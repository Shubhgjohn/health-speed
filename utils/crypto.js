const crypto = require('crypto')
const Bcrypt = require('bcrypt')

module.exports = {
// Function for generate randome string for password //
  genRandomString (length) {
    return crypto
      .randomBytes(Math.ceil(length / 2))
      .toString('hex')
      .slice(0, length)
  },
  // Function for encrypt the password //
  bcryptHash (value, salt) {
    const hash = Bcrypt.hashSync(value, salt)

    return hash
  },
  // Function for decrypt the password //
  bcryptVerify (hash, value) {
    const decrypt = Bcrypt.compareSync(hash, value)
    return decrypt
  }

}
