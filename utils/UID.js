/* eslint-disable camelcase */
/* eslint-disable prefer-const */
/* eslint-disable eqeqeq */
var auditModule = require('../models').Audit
const encounterModule = require('../models').Encounter
const orderModule = require('../models').Order
const patientProblemModule = require('../models').PatientProblem
const model = require('../models')
var randomize = require('randomatic')

module.exports = {
	async auditUid () {
		const audit = await auditModule.lastInserted()
		if (audit.length != 0) {
			let newUid = await getNextId()
			return newUid
		} else {
			let number = randomize('0', 20)
			let patternNumber = number.match(/.{1,4}/g)
			let id = patternNumber.join('-')
			let auditId = `AID-${id}`
			return auditId
		}
	},
	async encounterUID () {
		const encounter = await encounterModule.lastInserted()
		if (encounter.length != 0) {
			let newUid = await getNextEncounterId()
			return newUid
		} else {
			let number = randomize('0', 20)
			let patternNumber = number.match(/.{1,4}/g)
			let id = patternNumber.join('-')
			let encounterId = `EN-${id}`
			return encounterId
		}
	},
	async orderUID () {
		const order = await orderModule.lastInserted()
		if (order.length != 0) {
			let newUid = await getNextOrderId()
			return newUid
		} else {
			let number = randomize('0', 20)
			let patternNumber = number.match(/.{1,4}/g)
			let id = patternNumber.join('-')
			let orderId = `OID-${id}`
			return orderId
		}
	},
	async PT_PRO_UID () {
		const PT_Problem = await patientProblemModule.lastInserted()
		if (PT_Problem.length != 0) {
			let newUid = await getNextPatientProblemId()
			return newUid
		} else {
			let number = randomize('0', 8)
			let patternNumber = number.match(/.{1,4}/g)
			let id = patternNumber.join('-')
			let problemID = `PTPRO-${id}`
			return problemID
		}
	}
}

async function getNextId () {
	let number = randomize('0', 20)
	let patternNumber = number.match(/.{1,4}/g)
	let id = patternNumber.join('-')
	let audit_id = `AID-${id}`
	let audit = await auditModule.getAuditByAuditId(audit_id)
	if (audit.length > 0) {
		getNextId()
	} else {
		return audit_id
	}
}

async function getNextOrderId () {
	let number = randomize('0', 20)
	let patternNumber = number.match(/.{1,4}/g)
	let id = patternNumber.join('-')
	let Order_UID = `OID-${id}`
	let order = await model.Order.findOrderById(Order_UID)
	if (order.length > 0) {
		getNextOrderId()
	} else {
		return Order_UID
	}
}

async function getNextEncounterId () {
	let number = randomize('0', 20)
	let patternNumber = number.match(/.{1,4}/g)
	let id = patternNumber.join('-')
	let Encounter_UID = `EN-${id}`
	let encounter = await model.Encounter.getEncounterByEncounter_UID(Encounter_UID)
	if (encounter) {
		getNextEncounterId()
	} else {
		return Encounter_UID
	}
}

async function getNextPatientProblemId () {
	let number = randomize('0', 8)
	let patternNumber = number.match(/.{1,4}/g)
	let id = patternNumber.join('-')
	let patientProblem_UID = `PTPRO-${id}`
	let PTProblem = await model.PatientProblem.findPatientProblemByID(patientProblem_UID)
	if (PTProblem) {
		getNextPatientProblemId()
	} else {
		return patientProblem_UID
	}
}
