module.exports = {
// Function for check that phone number is valid or not //
  isAValidPhoneNumber (number) {
    return /^[0-9]{10}$/.test(number)
  }
}
