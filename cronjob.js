const { func } = require('joi')
/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
/* eslint-disable handle-callback-err */
const cron = require('node-cron')
const request = require('request')

module.exports = {
	cronScheduler () {
		const baseUrl = process.env.API_HOST
		/**
     * notify before 15 min of start encounter at every 15th minute.
     */
		cron.schedule('*/15 * * * *', async function () {
			const url = `${baseUrl}/notification/before-fifteen-min`
			request.put(url, function (err, response, body) {})
		})

		/**
     * notify before encounter and start encounter at every 30th minute.
     */
		cron.schedule('*/30 * * * *', async function () {
			const url = `${baseUrl}/notification/notify-before-encounter`
			request.put(url, function (err, response, body) {
			})
		})
		/**
     * Send notification to admin at provider update at every 60th minute.
     */
		cron.schedule('*/60 * * * *', async function () {
			const url = `${baseUrl}/notification/send_provider_update_profile_email`
			request.get(url, function (err, response, body) {
			})
		})

		/**
     * cron job for addendum reminder  at every 30th minute
     */
		cron.schedule('*/30 * * * *', async function () {
			const url = `${baseUrl}/notification/addendum-reminder`
			request.get(url, function (err, response, body) {})
		})

		/**
     * cron job for Permission Notifications Alert At 00:00.
     */
		cron.schedule('0 0 * * *', async function () {
			const url = `${baseUrl}/notification/turn-on-notifications`
			request.get(url, function (err, response, before) {})
		})

		/**
     * Notification on descharge instruction reminder at every 60th minute.
     */
		cron.schedule('*/30 * * * *', async function () {
			const url = `${baseUrl}/notification/discharge-instruction-reminder`
			request.get(url, function (err, response, body) {})
		})

		/**
     * Cron job for set encounter as timed-out at every 60th minute.
     */
		cron.schedule('0 0 * * *', async function () {
			const url1 = `${baseUrl}/encounter/reschedule-time-out-encounter`
			const url2 = `${baseUrl}/encounter/followup-time-out-encounter`
			request.put(url1, function (err, response, body) {})
			request.put(url2, function (err, response, body) {})
		})

		/**
     * cron job for set encounter as Active  At 00:00.
     */
		cron.schedule('*/30 * * * *', async function () {
			const url = `${baseUrl}/encounter/active-encounter`
			request.put(url, function (err, response, body) {})
		})

		/**
     * cron job order reminder to provider at every  At 00:00.
     */
		cron.schedule('0 0 * * *', async function () {
			const url = `${baseUrl}/order/order-reminder`
			request.get(url, function (err, response, body) {})
		})

		/**
     * cron job for monthly earning at 00:00 on day-of-month 1.
     */
		cron.schedule('0 0 1 * *', async function () {
			const url = `${baseUrl}/provider/pro-monthly-earning`
			request.get(url, function (err, response, body) {})
		})

		/**
     * cron job for chart reminder at every 60th minute.
     */
		cron.schedule('*/60 * * * *', async function () {
			const url = `${baseUrl}/chart/chart-reminder`
			request(url, function (err, response, body) {})
		})


		cron.schedule('*/30 * * * *', async function() {
			const url = `${baseUrl}/encounter/payment-failure`
			request.get(url, function (err, response, body) {})
		})
	}
}
