/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const model = require('../../models')
const errorModule = require('../../models').Errors
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
// const { auditUid } = require('../../utils/UID')
// const auditType = require('../../auditType.json')
const logger = require('../../models/errorlog')

// route for create order
router.get('/update-encounter-order/:Encounter_UID', authorize(Role.Provider), async (req, res) => {
  var { params: { Encounter_UID } } = req
  if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required', Error_Code: errorModule.Encounter_UID_required() })
  try {
    let orders = await model.Order.getOrderByEncounterId(Encounter_UID)
    if (orders.length > 0) {
      await model.Order.completeOrders(Encounter_UID)
      res.send({
        code: 1,
        status: 'sucess',
        message: 'Order completed successfully'
      })
    } else {
      res.send({
        code: 1,
        status: 'sucess',
        message: 'Order completed successfully'
      })
    }
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.send({ code: 0, status: 'failed', message: error.message })
  }
})

module.exports = router
