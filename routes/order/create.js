/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const moment = require('moment')
const model = require('../../models')
const encounterModule = require('../../models').Encounter
const errorModule = require('../../models').Errors
const orderModule = require('../../models').Order
const auditModule = require('../../models').Audit
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const { auditUid, orderUID } = require('../../utils/UID')
const auditType = require('../../auditType.json')
const logger = require('../../models/errorlog')

// route for create order
router.post('/create', authorize(Role.Provider), async (req, res) => {
  var { body: { Encounter_UID, Order_Text, Order_Type } } = req
  if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required', Error_Code: errorModule.Encounter_UID_required() })
  // if (!PT_Username) return res.send({ code: 0, status: 'failed', message: "PT_Username is required" , Error_Code: });
  if (!Order_Text) return res.send({ code: 0, status: 'failed', message: 'Order_Text is required', Error_Code: 'HS119' })
  if (!Order_Type) return res.send({ code: 0, status: 'failed', message: 'Order_Type is required', Error_Code: 'HS120' })
  req.body.PRO_UID = req.user.PRO_UID
  try {
    let encounter = await encounterModule.getEncounterByEncounter_UID(Encounter_UID)
    if (!encounter) return res.send({ code: 0, status: 'failed', message: `Encounter with id '${Encounter_UID}' not found!`, Error_Code: errorModule.Encounter_not_found() })
    let chart = await model.Chart.findChartByEncounterUID(Encounter_UID)
    if (chart.length > 0 && chart[0].Chart_is_Signed) {
      req.body.Order_As_Addendum = true
    } else {
      req.body.Order_As_Addendum = false
    }
    req.body.PT_HS_MRN = encounter.PT_HS_MRN
    req.body.PT_Username = encounter.PT_Username
    req.body.Order_UID = await orderUID()
    req.body.Order_Review_Status = 'Review'
    if (req.body.Order_Reminder == 'Checked') {
      req.body.Days_to_Order_Reminder = 6
      let date = new Date()
      date = date.setDate(date.getDate() + 7)
      req.body.Reminder_Start_Date = date
      req.body.Reminder_Date = date
    }
    req.body.Order_Created_Time = moment(new Date()).format('LT')
    req.body.Order_Created_Date = moment(new Date()).format('MM-DD-YYYY')

    await orderModule.create(req.body)
    const auditData = {
      Audit_UID: await auditUid(),
      Audit_Info: 'New- ' + await auditUid(),
      Audit_Type: auditType.add,
      Data_Point: 'PRO/create order',
      PRO_First_Name: req.user.PRO_First_Name,
      PRO_Last_Name: req.user.PRO_Last_Name,
      PRO_UID: req.user.PRO_UID,
      IP_Address: await auditModule.getIpAddress(req)
    }
    await auditModule.create(auditData)
    let order = await orderModule.getOrderByEncounterId(Encounter_UID)
    res.send({
      code: 1,
      status: 'sucess',
      message: 'Order created successfully',
      orders: order
    })
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.send({ code: 0, status: 'failed', message: error.message })
  }
})

module.exports = router
