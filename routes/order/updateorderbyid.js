/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const moment = require('moment')
const errorModule = require('../../models').Errors
const orderModule = require('../../models').Order
// const auditModule = require('../../models').Audit
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const logger = require('../../models/errorlog')
// const { encounterUID, auditUid, orderUID } = require('../../utils/UID')
// const auditType = require('../../auditType.json')

// route for create order
router.put('/update-order/:Order_UID', authorize(Role.Provider), async (req, res) => {
  var { params: { Order_UID }, body: { data } } = req
  if (!Order_UID) return res.send({ code: 0, status: 'failed', message: 'Order_UID is required', Error_Code: errorModule.Order_UID_required() })
  if (!data) return res.send({ code: 0, status: 'failed', message: 'data is required', Error_Code: errorModule.data_required() })
  try {
    let order = await orderModule.findOrderById(Order_UID)
    if (order.length > 0) {
      let updatedOrder = await orderModule.updateOrderById(Order_UID, data)
      updatedOrder.Order_Created_Date = moment(new Date(updatedOrder.Order_Created_Date)).format('MM-DD-YYYY')
      updatedOrder.Updated_At = moment(new Date(updatedOrder.Updated_At)).format('MM-DD-YYYY')
      let orders = await orderModule.getOrderByEncounterId(updatedOrder.Encounter_UID)
      return res.send({
        code: 1,
        status: 'sucess',
        data: updatedOrder,
        orders: orders
      })
    } else {
      return res.send({
        code: 0,
        status: 'failed',
        data: `No order found with ${Order_UID}`,
        Error_Code: errorModule.No_order_found()
      })
    }
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.send({ code: 0, status: 'failed', message: error.message })
  }
})

module.exports = router
