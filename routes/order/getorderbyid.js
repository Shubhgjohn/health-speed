/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const moment = require('moment')
const serviceModule = require('../../models').CommonServices
const encounterModule = require('../../models').Encounter
const errorModule = require('../../models').Errors
const orderModule = require('../../models').Order
// const auditModule = require('../../models').Audit
const model = require('../../models')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const logger = require('../../models/errorlog')
// const { encounterUID, auditUid, orderUID } = require('../../utils/UID')
// const auditType = require('../../auditType.json')

// route for create order
router.get('/get-order/:Order_UID', authorize([Role.Provider, Role.Patient]), async (req, res) => {
  var { params: { Order_UID } } = req
  if (!Order_UID) return res.send({ code: 0, status: 'failed', message: 'Order_UID is required', Error_Code: errorModule.Order_UID_required() })
  try {
    let order = await orderModule.findOrderById(Order_UID)
    if (order.length > 0) {
      let encounter = await encounterModule.getEncounterByEncounter_UID(order[0].Encounter_UID)
      let patient = await model.Patient.findById(encounter.PT_HS_MRN)
      order = JSON.parse(JSON.stringify(order[0]))
      // if(req.userType === 'Provider') {
      let provider = await model.Provider.findById(encounter.PRO_UID)
      order.PT_Profile_Picture = patient.PT_Profile_Picture ? await serviceModule.getImage(patient.PT_Profile_Picture) : null
      order.PRO_Profile_Picture = provider.PRO_Profile_Picture ? await serviceModule.getImage(provider.PRO_Profile_Picture) : null
      order.PRO_Last_Name = provider.PRO_Last_Name
      order.PT_Age_Now = new Date().getFullYear() - patient.PT_DOB.getFullYear()
      order.PT_Gender = patient.PT_Gender
      order.PT_First_Name = patient.PT_First_Name
      order.PT_Last_Name = patient.PT_Last_Name
      order.Encounter_Date = encounter.Encounter_Date
      order.Encounter_Time = encounter.Encounter_Time
      order.Encounter_Type = encounter.Encounter_Type
      order.Encounter_Chief_Complaint_PT = encounter.Encounter_Chief_Complaint_PT
      order.Encounter_Chief_Complaint_PRO = encounter.Encounter_Chief_Complaint_PRO
      // }
      order.Order_Created_Date = moment(new Date(order.Order_Created_Date)).format('MM-DD-YYYY')
      order.Updated_At = moment(new Date(order.Updated_At)).format('MM-DD-YYYY')

      await orderModule.viewOrder(Order_UID, req.user.PRO_UID || req.user.PT_HS_MRN)
      return res.send({
        code: 1,
        status: 'sucess',
        data: order
      })
    } else {
      return res.send({
        code: 0,
        status: 'failed',
        data: `No order found with ${Order_UID}`,
        Error_Code: errorModule.No_order_found()
      })
    }
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.send({ code: 0, status: 'failed', message: error.message })
  }
})

module.exports = router
