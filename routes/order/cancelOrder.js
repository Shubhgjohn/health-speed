/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const orderModule = require('../../models').Order
const auditModule = require('../../models').Audit
const patientModule = require('../../models').Patient
const { auditUid } = require('../../utils/UID')
const auditType = require('../../auditType')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_PRIVATE_KEY)
const logger = require('../../models/errorlog')

router.put('/cancel-order/:id', authorize(Role.Provider), async (req, res) => {
  try {
    const order_id = req.params.id
    if (!order_id) return res.send({ code: 0, status: 'failed', message: 'ORDER ID is required!!' })
    const orderData = await orderModule.getOrderById(order_id)
    if (orderData.length == 0) return res.send({ code: 0, status: 'failed', message: `order id '${order_id}' is invalid!` })
    if (orderData[0].Order_Status == 'Cancelled') return res.send({ code: 0, status: 'failed', message: `order id '${order_id}' is already cancelled!` })
    let data = {
      Order_Review_Status: 'Cancelled',
      Order_Status: 'Cancelled',
      Order_Reminder: orderData[0].Order_Reminder == 'Checked' ? 'Unchecked' : orderData[0].Order_Reminder
    }
    await orderModule.updateOrderById(order_id, data)
    let patient = await patientModule.findOneByEmail(orderData[0].PT_Username)
    if (!patient) return res.send({ code: 0, status: 'failed', message: 'No paient found for this order!' })
    if (patient.PT_Email_Notification == 'Enabled' && patient.PT_Account_Status == 'Active') {
      let message = {
        to: orderData[0].PT_Username,
        from: process.env.ADMIN_FROM_EMAIL,
        template_id: process.env.NOTIFY_PATIENT_ON_CANCEL_ORDER_TEMPLATE,
        dynamic_template_data: {
          subject: `${orderData[0].Order_Type}`,
          url: `${process.env.API_HOST}/patient/main/${orderData[0].Order_UID}/${orderData[0].Order_Type}`
        }
      }
      sgMail.send(message)
    }
    var auditData = {
      Audit_UID: await auditUid(),
      Audit_Type: auditType.change,
      Audit_Info: 'New- ' + await auditUid(),
      Data_Point: 'PRO/cancel order',
      IP_Address: await auditModule.getIpAddress(req)
    }
    await auditModule.create(auditData)
    res.send({ code: 1, status: 'success', message: 'Order has been cancelled.' })
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.send({ code: 0, status: 'failed', message: error.message })
  }
})

module.exports = router
