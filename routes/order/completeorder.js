/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const orderModule = require('../../models').Order
const auditModule = require('../../models').Audit
const errorModule = require('../../models').Errors
const providerModule = require('../../models').Provider
const { auditUid } = require('../../utils/UID')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const auditType = require('../../auditType.json')
const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_PRIVATE_KEY)
const logger = require('../../models/errorlog')
const moment = require('moment')

// route for create order
router.put('/complete-order/:Order_UID', authorize(Role.Provider), async (req, res) => {
  var { params: { Order_UID } } = req
  if (!Order_UID) return res.send({ code: 0, status: 'failed', message: 'Order_UID is required', Error_Code: errorModule.Order_UID_required() })
  try {
    let auditData = {
      Audit_UID: await auditUid(),
      Audit_Info: 'New- ' + await auditUid(),
      Audit_Type: auditType.change,
      Data_Point: 'PRO/complete order by id',
      PRO_First_Name: req.user.PRO_First_Name,
      PRO_Last_Name: req.user.PRO_Last_Name,
      PRO_UID: req.user.PRO_UID,
      IP_Address: await auditModule.getIpAddress(req)
    }
    await auditModule.create(auditData)

    // eslint-disable-next-line no-unneeded-ternary
    req.body.Order_Complete = req.body.Order_Complete == 'No' ? false : true

    let order = await orderModule.findOrderById(Order_UID)
    if (order.length > 0) {
      if (order[0].Order_Complete && req.body.Order_Complete == 'Yes') {
        res.send({ code: 0, status: 'failed', message: `Order ${Order_UID} is already completed`, Error_Code: 'HS117' })
      } else {
        let d = new Date()
        var newYork = moment.tz(d, process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm:ss')
        req.body.Order_Date_Marked_Complete = req.headers.host.includes('localhost') ? moment(Date.parse(newYork)).add(330, 'm').toDate() : moment(Date.parse(newYork)).toDate()
        await orderModule.completeOrder(Order_UID, req.body)
        res.send({ code: 1, status: 'sucess', message: 'Order complete successfully' })
      }
    } else {
      res.send({ code: 0, status: 'failed', message: `No order find with this ${Order_UID}`, Error_Code: errorModule.No_order_found() })
    }
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.send({ code: 0, status: 'failed', message: error.message })
  }
})

router.get('/order-reminder', async (req, res) => {
  let orders = await orderModule.find()
  let count = 0
  if (orders.length === 0) return res.send('no orders found')
  orders.map(async ele => {
    let provider = await providerModule.findById(ele.PRO_UID)
    if (provider.PRO_Notifications == 'Yes') {
      if (ele.Order_Status === 'Delivered' && ele.Order_Reminder === 'Checked') {
        let reminderDate = ele.Order_Date_Delivered.setDate(ele.Order_Date_Delivered.getDate() + ele.Days_to_Order_Reminder)
        reminderDate = new Date(reminderDate)
        if (reminderDate.getDate === new Date().getDate()) {
          let msg = {
            from: process.env.ADMIN_FROM_EMAIL,
            to: provider.PRO_Username,
            template_id: process.env.Notify_Provider_On_Order_Reminder,
            dynamic_template_data: {
              url: `${process.env.API_HOST}/provider/main/${ele.Order_UID}/${ele.Order_Type}`
            }
          }
          sgMail.send(msg)
        }
      }
    }
    count = count + 1
    if (count === orders.length) return res.send('success')
  })
})

module.exports = router
