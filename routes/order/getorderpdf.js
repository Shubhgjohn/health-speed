/* eslint-disable no-useless-escape */
/* eslint-disable no-unneeded-ternary */
/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const moment = require('moment')
const model = require('../../models')
const errorModule = require('../../models').Errors
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
var $ = require('jsrender')
const { auditUid } = require('../../utils/UID')
const auditType = require('../../auditType.json')
const logger = require('../../models/errorlog')
const puppeteer = require('puppeteer')
var AWS = require('aws-sdk')
AWS.config.update({
	accessKeyId: process.env.AWS_ACCESS_KEY,
	secretAccessKey: process.env.AWS_SECRET_KEY,
	region: 'us-east-1'
})

var s3 = new AWS.S3()

// route for create order
router.get('/get-pdf/:Order_UID', authorize([Role.Provider, Role.Patient]), async (req, responce) => {
	var { params: { Order_UID } } = req
	if (!Order_UID) return responce.send({ code: 0, status: 'failed', message: 'Order_UID is required', Error_Code: errorModule.Order_UID_required() })
	try {
		let order = await model.Order.getOrderAllDetails(Order_UID)
		let auditData = {
			Audit_UID: await auditUid(),
			Audit_Info: 'New- ' + await auditUid(),
			Audit_Type: auditType.view,
			Data_Point: 'PRO/Get order pdf',
			PRO_First_Name: req.user.PRO_First_Name,
			PRO_Last_Name: req.user.PRO_Last_Name,
			PRO_UID: req.user.PRO_UID,
			IP_Address: await model.Audit.getIpAddress(req)
		}
		await model.Audit.create(auditData)
		if (order.length != 0) {
			let encounter = await model.Encounter.getEncounterByEncounter_UID(order[0].Encounter_UID)
			if (!encounter) return responce.send({ code: 0, status: 'failed', message: 'somthing went wrong', Error_Code: 'HS420' })
			if (order[0].Order_Type == 'Image Order') {
				let ICD10_Code_Array = []
				if (order[0].Order_ICD10_Code.length > 0) {
					order[0].Order_ICD10_Code.map(ele => {
						ICD10_Code_Array.push({ ICD10_Code: ele })
					})
				}
				order[0].ICD10_Code_Array = ICD10_Code_Array
				let url = await createPDF(order, 'imagin_order.html', 'image_order', encounter)
				return responce.send({ code: 1, status: 'sucess', data: url })
			}
			if (order[0].Order_Type == 'Lab Order') {
				let ICD10_Code_Array = []
				if (order[0].Order_ICD10_Code.length > 0) {
					order[0].Order_ICD10_Code.map(ele => {
						ICD10_Code_Array.push({ ICD10_Code: ele })
					})
				}
				order[0].ICD10_Code_Array = ICD10_Code_Array
				let url = await createPDF(order, 'lab_order.html', 'lab_order', encounter)
				return responce.send({ code: 1, status: 'sucess', data: url })
			}
			if (order[0].Order_Type == 'Prescription Order') {
				let ICD10_Code_Array = []
				if (order[0].Order_ICD10_Code.length > 0) {
					order[0].Order_ICD10_Code.map(ele => {
						ICD10_Code_Array.push({ ICD10_Code: ele })
					})
				}
				order[0].ICD10_Code_Array = ICD10_Code_Array
				let url = await createPDF(order, 'prescription_order.html', 'prescription_order', encounter)
				return responce.send({ code: 1, status: 'sucess', data: url })
			}
			if (order[0].Order_Type == 'Other Order') {
				let ICD10_Code_Array = []
				if (order[0].Order_ICD10_Code.length > 0) {
					order[0].Order_ICD10_Code.map(ele => {
						ICD10_Code_Array.push({ ICD10_Code: ele })
					})
				}
				order[0].ICD10_Code_Array = ICD10_Code_Array
				let url = await createPDF(order, 'other_order.html', 'other_order', encounter)
				return responce.send({ code: 1, status: 'sucess', data: url })
			}
			if (order[0].Order_Type == 'Referral Order') {
				let ICD10_Code_Array = []
				if (order[0].Order_ICD10_Code.length > 0) {
					order[0].Order_ICD10_Code.map(ele => {
						ICD10_Code_Array.push({ ICD10_Code: ele })
					})
				}
				order[0].ICD10_Code_Array = ICD10_Code_Array
				let url = await createPDF(order, 'referal_order.html', 'referral_order', encounter)
				return responce.send({ code: 1, status: 'sucess', data: url })
			}
		} else {
			responce.send({
				code: 0,
				status: 'failed',
				message: 'No order find with this order id',
				Error_Code: errorModule.No_order_found()
			})
		}
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return responce.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS270' })
	}
})

async function createPDF (order, html, name, encounter) {
	order = JSON.parse(JSON.stringify(order))
	order[0].order_submit_date = moment(order[0].Created_At).format('M/D/YYYY')
	order[0].ICD10 = order[0].Order_ICD10_Code.length > 0 ? true : false
	order[0].Order_Stat = order[0].Order_Stat == 'Unchecked' ? false : true
	order[0].PT_DOB = moment(new Date(order[0].PT_DOB)).format('M/D/YYYY')
	// eslint-disable-next-line no-self-assign
	order[0].PT_Gender = order[0].PT_Gender ? order[0].PT_Gender[0] : null
	order[0].PT_Age_Now = moment(new Date()).format('YYYY') - moment(new Date(order[0].PT_DOB)).format('YYYY')
	order[0].Order_Created_Date = moment(new Date(order[0].Order_Created_Date)).format('M/D/YYYY')
	order[0].Order_Complete_Date = moment(new Date(order[0].Order_Date_Marked_Complete)).format('M/D/YY')
	order[0].Order_Complete_Time = moment(order[0].Order_Date_Marked_Complete).format('HH:mm')
	order[0].PT_Cell_Phone = order[0].PT_Cell_Phone ? order[0].PT_Cell_Phone.replace(/\D+/g, '')
		.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3') : null
	order[0].PRO_Office_Phone = order[0].PRO_Office_Phone ? order[0].PRO_Office_Phone.replace(/\D+/g, '')
		.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3') : null
	order[0].Encounter_End_Date = order[0].Order_As_Addendum ? order[0].Order_Date_Delivered ? moment.tz(order[0].Order_Date_Delivered, process.env.CURRENT_TIMEZONE).format('M/D/YYYY') : null : encounter.Encounter_Close_Date ? moment(encounter.Encounter_Close_Date).format('M/D/YYYY') : null
	order[0].Encounter_End_Time = order[0].Order_As_Addendum ? order[0].Order_Date_Delivered ? moment.tz(order[0].Order_Date_Delivered, process.env.CURRENT_TIMEZONE).format('HH:mm') : null : encounter.Encounter_Close_Date ? moment(encounter.Encounter_Close_Date).format('HH:mm') : null
	let lab_order_template = $.templates(`./download/${html}`)
	let order_html = lab_order_template.render(order[0])
	const opts = {
		headless: true,
		timeout: 15000,
		args: ['--no-sandbox']
	}
	const browser = await puppeteer.launch(opts)
	const page = await browser.newPage()
	await page.setContent(order_html)
	const pdf = await page.pdf({
		// path: 'order.pdf',
		format: 'A4',
		printBackground: true,
		displayHeaderFooter: true,
		footerTemplate: `<div  style="width:100%; font-size: 11px !important;  padding:0px 60px 25px 60px !important; page-break-after: always; font-family: 'Roboto', sans-serif;">
     
    <div style = "float: left; margin:0px 0px;">
    <div >${order[0].PT_First_Name} ${order[0].PT_Last_Name} &nbsp; DOB: <span> </span>${order[0].PT_DOB}</div>
    <div >${order[0].Encounter_UID}</div>
  </div>
  <div style = "float: right; margin:0px 0px;">
  <div class=\"page-footer\" style=\"width:100%; text-align:right; \">Page <span class=\"pageNumber\" "></span> of <span class=\"totalPages\"></span></div>
  </div></div>`,
		margin: {
			bottom: '30mm'
		}
	})
	await browser.close()
	return new Promise((resolve, reject) => {
		var imagename = `${name}_${Date.now()}.pdf`
		const params = {
			Bucket: process.env.AWS_BUCKET,
			Key: imagename,
			Body: pdf,
			ContentType: 'application/pdf',
			ACL: 'public-read'
		}
		s3.upload(params, function (s3Err, data) {
			if (s3Err) throw s3Err
			resolve(data.Location)
		})
	})
}

module.exports = router
