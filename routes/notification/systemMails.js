const Router = require('express').Router
const router = new Router()
// const providerModule = require('../../models').Provider
const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_PRIVATE_KEY)

module.exports = router
