/* eslint-disable camelcase */
/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
const Router = require('express').Router
const router = new Router()
const providerModule = require('../../models').Provider
const notificationModule = require('../../models').Notification
const fcm = require('../../_helpers/fcm')

router.get('/turn-on-notifications', async (req, res) => {
  try {
    let providerData = await providerModule.find()
    let count = 0
    providerData.map(async ele => {
      if (ele.PRO_Notifications == 'No') {
        let jsonData = {
          title: 'Notifications Alert'
        }
        let PRO_Token = await notificationModule.getToken('Provider', ele.PRO_Username)
        let arr = []
        PRO_Token.map(item => arr.push(item.Device_Token))
        let title = 'HealthSpeed Would Like to Send You Push Notifications'
        let body = 'Notifications may include alerts, sounds, and icon badges. These can be configured in Settings.'
        await sendNotification(arr, title, body, jsonData)
      }
      count = count + 1
      if (count == providerData.length)res.send('notifications send')
    })
  } catch (err) {
    res.send({ code: 0, status: 'failed', message: err })
  }
})

async function sendNotification (arr, title, body, jsonData) {
  if (arr.length != 0) {
    var payload = { // this may vary according to the message type (single recipient, multicast, topic, et cetera)
      registration_ids: arr,
      notification: {
        title: title,
        body: body
      },

      data: jsonData
    }
    fcm.send(payload, function (err, response) {
      if (err) {
      } else {
        console.log(response)
      }
    })
  }
}
module.exports = router
