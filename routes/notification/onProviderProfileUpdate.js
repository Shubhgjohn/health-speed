/* eslint-disable dot-notation */
/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
const Router = require('express').Router
const router = new Router()
const providerModule = require('../../models').Provider
const auditModule = require('../../models').Audit
const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_PRIVATE_KEY)
const $ = require('jsrender')

router.get('/send_provider_update_profile_email', async (req, res) => {
	let other = false
	let provider = await providerModule.find()
	let newDate = new Date()
	var d2 = new Date(newDate)
	d2.setHours(newDate.getHours() - 1)
	let auditData = await auditModule.getAuditBetweenTime(d2, newDate)
	for (var i = 0; i < provider.length; i++) {
		let data = auditData.filter(ele => ele.PRO_UID == provider[i].PRO_UID)
		let arr = []
		data.forEach(ele => {
			if (!ele.Field_Name) other = true
			if (arr.some(item => item.field_name == ele.Field_Name)) {
				arr.map(change => {
					if (change['old_value'] == ele.New_Value) {
						arr.shift(change)
					} else {
						change['new_value'] = ele.New_Value
					}
				})
			} else {
				if (ele.Old_Value != ele.New_Value) { arr.push({ field_name: ele.Field_Name, old_value: ele.Old_Value, new_value: ele.New_Value }) }
			}
		})
		if (arr.length > 0) {
			let list = { records: arr, PRO_First_Name: provider[i].PRO_First_Name, PRO_Last_Name: provider[i].PRO_Last_Name, other: other }
			let tmpl = $.templates('./download/record.html')
			const msg = {
				to: process.env.ADMIN_TO_EMAIL,
				from: process.env.ADMIN_FROM_EMAIL,
				subject: 'Provider Profile Updates Posted',
				html: tmpl.render(list)
			}
			await sgMail.send(msg)
		}
		if (i == provider.length - 1) res.send('all done')
	}
})
module.exports = router
