/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
const Router = require('express').Router
const router = new Router()
const notificationModel = require('../../models').Notification
const auditModule = require('../../models').Audit
const { auditUid } = require('../../utils/UID')
const auditType = require('../../auditType.json')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
// const fcm = require('../../_helpers/fcm')

router.post('/save-device-token', authorize([Role.Provider, Role.Patient]), async (req, res) => {
  let data = req.body
  if (!data.Device_Token) return res.send({ code: 0, status: 'failed', message: 'Device_Token required!', Error_Code: 'HS115' })
  if (!data.Device_Id) return res.send({ code: 0, status: 'failed', message: 'Device_Id required!', Error_Code: 'HS500' })
  data.Role = req.userType
  data.Email_ID = req.userType == 'Provider' ? req.user.PRO_Username : req.user.PT_Username
  await notificationModel.removeToken(data)
  await notificationModel.create(data)
  let UID = await auditUid()
  let auditData = {
    Audit_UID: UID,
    Audit_Info: 'New- ' + UID,
    Data_Point: req.userType == 'Provider' ? 'PRO/save device token' : 'PT/save device token',
    Audit_Type: auditType.add,
    PRO_First_Name: req.userType == 'Provider' ? req.user.PRO_First_Name : null,
    PRO_Last_Name: req.userType == 'Provider' ? req.user.PRO_Last_Name : null,
    PRO_UID: req.userType == 'Provider' ? req.user.PRO_UID : null,
    PT_First_Name: req.userType == 'Patient' ? req.user.PT_First_Name : null,
    PT_Last_Name: req.userType == 'Patient' ? req.user.PT_Last_Name : null,
    PT_HSID: req.userType == 'Patient' ? req.user.PT_HS_MRN : null,
    IP_Address: await auditModule.getIpAddress(req)
  }
  await auditModule.create(auditData)
  return res.send({ code: 1, status: 'success', message: 'token saved' })
})

router.delete('/remove-device-token', authorize([Role.Provider, Role.Patient]), async (req, res) => {
  let data = req.body
  if (!data.Device_Id) return res.send({ code: 0, status: 'failed', message: 'Device_Id required!', Error_Code: 'HS500' })
  data.Role = req.userType
  await notificationModel.removeToken(data)
  return res.send({ code: 1, status: 'success', message: 'token remove successfully.' })
})

module.exports = router
