/* eslint-disable camelcase */
/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
const Router = require('express').Router
const router = new Router()
const providerModule = require('../../models').Provider
const encounterModule = require('../../models').Encounter
const NotificationModule = require('../../models').Notification
const moment = require('moment-timezone')
const fcm = require('../../_helpers/fcm')
const momentTz = require('moment-timezone')

router.get('/discharge-instruction-reminder', async (req, res) => {
  let data = await encounterModule.getActiveEncounter()
  let count = 0
  data = data.filter(ele => (Math.abs(Date.parse(momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm:ss')) - Date.parse(moment(new Date(`${ele.Encounter_Date} ${ele.Encounter_Time}`)).format('YYYY-MM-DDTHH:mm:ss'))) / 36e5) < 25)
  if (data.length > 0) {
    data.forEach(async ele => {
      if (ele.Encounter_Time != null && !ele.Encounter_Time.includes(' ')) {
        if (ele.Encounter_Time.includes('a')) {
          ele.Encounter_Time = `${ele.Encounter_Time.split('a')[0]} am`
        }
        if (ele.Encounter_Time.includes('p')) {
          ele.Encounter_Time = `${ele.Encounter_Time.split('p')[0]} pm`
        }
      }
      let provider = await providerModule.findById(ele.PRO_UID)
      let hours = Math.abs(Date.parse(momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm:ss')) - Date.parse(moment(new Date(`${ele.Encounter_Date} ${ele.Encounter_Time}`)).format('YYYY-MM-DDTHH:mm:ss'))) / 36e5
      let jsonData = {
        Encounter_UID: ele.Encounter_UID,
        title: 'Discharge Instructions Reminder'
      }
      if (hours >= 4 && hours < 4.5) {
        await sendNotification(provider, jsonData)
        count = count + 1
        if (count == data.length) return res.send('success')
      } else if (hours >= 12 && hours < 12.5) {
        await sendNotification(provider, jsonData)
        count = count + 1
        if (count == data.length) return res.send('success')
      } else if (hours >= 24 && hours < 24.5) {
        await sendNotification(provider, jsonData)
        count = count + 1
        if (count == data.length) return res.send('success')
      } else {
        count = count + 1
        if (count == data.length) return res.send('success')
      }
    })
  } else {
    return res.send('success')
  }
})

let sendNotification = async (provider, jsonData) => {
  let PRO_Token = await NotificationModule.getToken('Provider', provider.PRO_Username)
  let title = 'Ready to Submit Your Discharge Instructions ?'
  let body = 'Your patient is waiting to receive their instructions. Send them now!'
  let arr = []
  PRO_Token.map(item => arr.push(item.Device_Token))
  if (arr.length != 0) {
    let payload = {
      registration_ids: arr,
      notification: {
        title: title,
        body: body
      },

      data: jsonData
    }
    fcm.send(payload, function (err, response) {
      if (err) {
      } else console.log(response)
    })
  }
}
module.exports = router
