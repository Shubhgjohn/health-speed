/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
/* eslint-disable prefer-const */
const Router = require('express').Router
const router = new Router()
const notificationModule = require('../../models').Notification
const encounterModule = require('../../models').Encounter
const providerModule = require('../../models').Provider
// const patientModule = require('../../models').Patient
const fcm = require('../../_helpers/fcm')
const moment = require('moment')
const momentTz = require('moment-timezone')

router.put('/before-fifteen-min', async (req, res) => {
	let encounterData = await encounterModule.beforeEncouterDate()
	let filterData = encounterData.filter(ele => Date.parse(moment(new Date(`${ele.Encounter_Date} ${ele.Encounter_Time}`)).format('YYYY-MM-DDTHH:mm')) >= Date.parse(momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm')))
	let count = 0
	let currentTime = Date.parse(momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm'))
	if (filterData.length == 0) return res.send('success')
	for (let i = 0; i < filterData.length; i++) {
		let encounterTime = Date.parse(moment(new Date(`${filterData[i].Encounter_Date} ${filterData[i].Encounter_Time}`)).format('YYYY-MM-DDTHH:mm'))
		let difference = diff_minutes(currentTime, encounterTime)
		// let hours = Math.round(Math.abs(currentTime - encounterTime) / 36e5)
		let provider = await providerModule.findById(filterData[i].PRO_UID)
		// let patient = await patientModule.findOneByEmail(filterData[i].PT_Username)
		let PRO_Token = await notificationModule.getToken('Provider', provider.PRO_Username)
		let PT_Token = await notificationModule.getToken('Patient', filterData[i].PT_Username)
		if (difference > 13 && difference <= 15) {
			if (!filterData[i].First_Notification) {
				let jsonData = {
					Encounter_UID: filterData[i].Encounter_UID,
					PRO_UID: filterData[i].PRO_UID,
					title: 'Encounter Reminder'
				}
				let arr = []
				PRO_Token.map(ele => arr.push(ele.Device_Token))
				PT_Token.map(ele => arr.push(ele.Device_Token))
				if (arr.length != 0) {
					let title = '15 Minutes Until Your Visit Starts!'
					let body = 'Review your visit details before it starts in 15 minutes.'
					await sendNotification(arr, title, body, jsonData)
					let data = {
						First_Notification: true
					}
					await encounterModule.updateEncounter(filterData[i].Encounter_UID, data)
				}
			}
		}
		count = count + 1
		if (count == filterData.length) res.send('success')
	}
})

router.put('/notify-before-encounter', async (req, res) => {
	let encounterData = await encounterModule.beforeEncouterDate()
	let filterData = encounterData.filter(ele => Date.parse(moment(new Date(`${ele.Encounter_Date} ${ele.Encounter_Time}`)).format('YYYY-MM-DDTHH:mm')) >= Date.parse(momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm')))
	let count = 0
	let currentTime = Date.parse(momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm'))
	if (filterData.length == 0) return res.send('success')
	for (let i = 0; i < filterData.length; i++) {
		let encounterTime = Date.parse(moment(new Date(`${filterData[i].Encounter_Date} ${filterData[i].Encounter_Time}`)).format('YYYY-MM-DDTHH:mm'))
		let difference = diff_minutes(currentTime, encounterTime)
		let hours = Math.round(Math.abs(currentTime - encounterTime) / 36e5)
		let provider = await providerModule.findById(filterData[i].PRO_UID)
		// let patient = await patientModule.findOneByEmail(filterData[i].PT_Username)
		let PRO_Token = await notificationModule.getToken('Provider', provider.PRO_Username)
		let PT_Token = await notificationModule.getToken('Patient', filterData[i].PT_Username)
		if (hours > 71 && hours < 73) {
			let jsonData = {
				Encounter_UID: filterData[i].Encounter_UID,
				PRO_UID: filterData[i].PRO_UID,
				title: 'Update PMH Pre Visit'
			}
			let arr = []
			PT_Token.map(ele => arr.push(ele.Device_Token))
			let title = 'Getting ready'
			let body = 'To get the most out of your visit, take a moment to update your medical history.'
			if (!filterData[i].PMH_Notification && arr.length > 0) {
				await sendNotification(arr, title, body, jsonData)
				let data = {
					PMH_Notification: true
				}
				await encounterModule.updateEncounter(filterData[i].Encounter_UID, data)
			}
		}
		if (difference < 1) {
			if (!filterData[i].Second_Notification) {
				let jsonData = {
					Encounter_UID: filterData[i].Encounter_UID,
					PRO_UID: filterData[i].PRO_UID,
					title: 'Encounter Reminder'
				}
				let PRO_arr = []
				let PT_arr = []
				PRO_Token.map(ele => PRO_arr.push(ele.Device_Token))
				PT_Token.map(ele => PT_arr.push(ele.Device_Token))
				if (PRO_arr.length != 0) {
					let title = 'Your Visit Is Starting Now'
					let body = 'Get ready for your visit with your patient.'
					await sendNotification(PRO_arr, title, body, jsonData)
				}
				if (PT_arr.length != 0) {
					let title = 'Your Visit Is Starting Now'
					let body = `Get ready for your visit with Dr. ${provider.PRO_Last_Name}`
					await sendNotification(PT_arr, title, body, jsonData)
				}
				let data = {
					Second_Notification: true
				}
				await encounterModule.updateEncounter(filterData[i].Encounter_UID, data)
			}
		}
		count = count + 1
		if (count == filterData.length) res.send('success')
		// res.send('success')
	}
})

function diff_minutes (dt2, dt1) {
	var diff = (dt2 - dt1) / 1000
	diff /= 60
	return Math.abs(Math.round(diff))
}

async function sendNotification (arr, title, body, jsonData) {
	var payload = { // this may vary according to the message type (single recipient, multicast, topic, et cetera)
		registration_ids: arr,
		notification: {
			title: title,
			body: body
		},

		data: jsonData
	}
	await fcm.send(payload, async function (err, response) {
		if (err) {
		} else {
			console.log(response)
		}
	})
	return true
}

module.exports = router
