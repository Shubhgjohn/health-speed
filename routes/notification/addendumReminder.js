/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
/* eslint-disable prefer-const */
const Router = require('express').Router
const router = new Router()
const notificationModule = require('../../models').Notification
const encounterModule = require('../../models').Encounter
const providerModule = require('../../models').Provider
const patientModule = require('../../models').Patient
const fcm = require('../../_helpers/fcm')
var AWS = require('aws-sdk')
const momentTz = require('moment-timezone')
AWS.config.update({
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET_KEY,
  region: 'us-east-1'
})
var s3 = new AWS.S3()

router.get('/addendum-reminder', async (req, res) => {
  let encounterData = await encounterModule.addendumReminder()
  let filterData = encounterData.filter(ele =>(ele.PRO_Addendums.length > 0 && ele.Chart_is_Signed) )
  let count = 0
  if ( filterData.length == 0 ) {
    return res.send({ code: 1, status: 'success', message: 'notification send' })
  } else {
    filterData.forEach(async ele => {
      let patient = await patientModule.findOneByEmail(ele.PT_Username)
      let provider = await providerModule.findById(ele.PRO_UID)
      let PRO_Token = await notificationModule.getToken('Provider', provider.PRO_Username)
      let arr = []
      PRO_Token.map(ele => arr.push(ele.Device_Token))
      if (ele.Chart_is_Signed) {
        ele.PRO_Addendums.map(async item => {
          if (!item.Addendum_Is_Signed) {
            var PRO_Addendum_Attachment = item.PRO_Addendum_Attachment.length > 0 ? await getImageUrls(item.PRO_Addendum_Attachment) : []
            let jsonData = {
              PT_Profile_Picture: patient.PT_Profile_Picture ? await getImage(patient.PT_Profile_Picture) : '',
              PT_First_Name: patient.PT_First_Name,
              PT_Last_Name: patient.PT_Last_Name,
              PT_Gender: patient.PT_Gender,
              PT_Age_Now: (new Date().getFullYear() - patient.PT_DOB.getFullYear()).toString(),
              Encounter_Type: ele.Encounter_Type,
              Encounter_Date: ele.Encounter_Date,
              Encounter_Time: ele.Encounter_Time,
              Encounter_UID: ele.Encounter_UID,
              Addendum_Id: item._id.toString(),
              Encounter_Chief_Complaint_PT: ele.Encounter_Chief_Complaint_PT ? ele.Encounter_Chief_Complaint_PT : '',
              Encounter_Chief_Complaint_PRO: ele.Encounter_Chief_Complaint_PRO ? ele.Encounter_Chief_Complaint_PRO : '',
              PRO_Addendum_Note: item.PRO_Addendum_Note ? item.PRO_Addendum_Note : '',
              ICD10_Code: JSON.stringify(item.ICD10_Code),
              PRO_Addendum_Attachment: JSON.stringify(PRO_Addendum_Attachment),
              title: 'Addendum Reminder'
            }
            if (item.Updated_At) {
              let hours = Math.abs(Date.parse(momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm:ss')) - Date.parse(momentTz.tz(item.Updated_At, process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm:ss'))) / 36e5
              if (hours >= 48  && hours <=49) {
                if (arr.length != 0) {
                  var payload = { // this may vary according to the message type (single recipient, multicast, topic, et cetera)
                    registration_ids: arr,
                    notification: {
                      title: `Hey, you remember this addendum for ${patient.PT_First_Name} ${patient.PT_Last_Name.charAt(0)} ?`,
                      body: 'It hasn’t been completed! Best take a look now.'
                    },
  
                    data: jsonData
                  }
                  fcm.send(payload, async function (err, response) {
                    if (err) {
                    } else {
                      // await encounterModule.updateEncounter(ele.Encounter_UID,{First_Notification:true})
                    }
                  })
                }
              }
            }
          }
        })
      }
      count = count + 1
      if (count == 1) return res.send({ code: 1, status: 'success', message: 'notification send' })
    })
  }
})

let getImage = async (imageKey) => {
  if (imageKey == null || imageKey == '') {
    return ''
  } else {
    if (imageKey.includes('pdf')) {
      const url = s3.getSignedUrl('getObject', {
        Bucket: process.env.AWS_BUCKET,
        Key: imageKey,
        ResponseContentType: 'application/pdf',
        Expires: 60 * 60 * 24 * 15 // time in seconds: e.g. 60 * 5 = 5 mins
      })
      return url
    } else {
      const url = s3.getSignedUrl('getObject', {
        Bucket: process.env.AWS_BUCKET,
        Key: imageKey,
        Expires: 60 * 60 * 24 * 15 // time in seconds: e.g. 60 * 5 = 5 mins
      })
      return url
    }
  }
}

var getImageUrls = async (arr) => {
  let newArray = []; let count = 0
  for (var i = 0; i < arr.length; i++) {
    let url = arr[i].Attachment_Name != null || arr[i].Attachment_Name != '' ? await getImage(arr[i].Attachment_Name) : null
    newArray.push({
      Attachment_Url: url,
      Is_Deleted: arr[i].Is_Deleted,
      _id: arr[i]._id,
      Attachment_Name: arr[i].Attachment_Name,
      Attachment_By: arr[i].Attachment_By,
      Attachment_User: arr[i].Attachment_User
    })
    count = count + 1
    if (count == arr.length) {
      return newArray
    }
  }
}

module.exports = router
