const express = require('express')
const app = express()

const saveDeviceToken = require('./saveDeviceID')
const fifteenMinBefore = require('./notifyBeforeEncounter')
const addendumReminder = require('./addendumReminder')
const permissionNotificationAlert = require('./PermissionNotificationsAlert')
const dischargeInstructionReminder = require('./dischargeInstructionReminder')
const systemMails = require('./systemMails')
const providerUpdateProfileNotification = require('./onProviderProfileUpdate')

app.use(saveDeviceToken)
app.use(fifteenMinBefore)
app.use(addendumReminder)
app.use(permissionNotificationAlert)
app.use(dischargeInstructionReminder)
app.use(systemMails)
app.use(providerUpdateProfileNotification)

module.exports = app
