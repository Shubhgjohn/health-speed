/* eslint-disable no-self-assign */
/* eslint-disable no-unneeded-ternary */
/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
/* eslint-disable prefer-const */
const Router = require('express')
const router = new Router()
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const encounterModule = require('../../models').Encounter
const patientHxModule = require('../../models').PatientHistory
const serviceModule = require('../../models').CommonServices
const patientModule = require('../../models').Patient
const providerModule = require('../../models').Provider
const stripeModule = require('../../models').Stripe
const _ = require('lodash')
const momentTz = require('moment-timezone')
const moment = require('moment')
var aws = require('aws-sdk')
aws.config.update({
	secretAccessKey: process.env.AWS_SECRET_KEY,
	accessKeyId: process.env.AWS_ACCESS_KEY,
	region: 'us-east-1'
})

var s3 = new aws.S3()
// patient upcomming visits and attn for dashboard screen in patient app //
router.get('/get-visit-attn', authorize(Role.Patient), async (req, res) => {
	try {
		var data = {}
		var attn = {}
		const minorPatient = await patientModule.findMinorIDByUserId(req.user.PT_HS_MRN)
		let patients = minorPatient.map(function (el) {
			return el.PT_HS_MRN
		})
		patients.push(req.user.PT_HS_MRN)
		const patientEncounter = await encounterModule.getPatientEncounterByUserID(patients)
		// const patientEncounter1 = await encounterModule.getPatientEncounterByUserID1(patients)

		if (patientEncounter.length == 0) {
			const PT_Profile_Picture = req.user.PT_Profile_Picture ? await serviceModule.getImage(req.user.PT_Profile_Picture) : null
			const patientHistory = await patientHxModule.findByUserId(req.user.PT_HS_MRN)
			let attn = {}
			let data = {}
			let GeneralMedicalHistory = await medicalHistory(PT_Profile_Picture, patientHistory, req)
			let Medications = await medicationHistory(PT_Profile_Picture, patientHistory, req)
			let other = await otherHistory(PT_Profile_Picture, patientHistory, req)
			let OB = await obgynHistory(PT_Profile_Picture, patientHistory, req, patientEncounter)
			let MedicalHistoryIncompleteCards = []

			data.upcommingVisit = []
			attn.DischargeInstructionsCards = []
			attn.AccessRequestCards = []
			attn.FollowupRequestCards = []
			attn.ChartSignedCards = []
			attn.AddendumAddedCards = []
			attn.Encounter_Reschedule_Request_Cards = []
			attn.EncounterCancelledCards = []
			attn.MedicalHistorySuggestedChangeCards = []
			attn.MedicalHistoryIncompleteCards = await obgynFilterData(GeneralMedicalHistory, Medications, other, OB, MedicalHistoryIncompleteCards)
			attn.Order_As_Addendum_Cards = []
			attn.PaymentFailureCard = []

			data.attn = attn
			return res.send({ code: 1, status: 'success', data: data })
		}
		patientEncounter.sort(compareDsc)
		const upcommingVisit = patientEncounter.filter(ele => (Date.parse(moment(new Date(`${ele.Encounter_Date} ${ele.Encounter_Time}`)).format('YYYY-MM-DDTHH:mm:ss')) >= Date.parse(momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm:ss')) && (ele.Encounter_Status == 'Scheduled' || ele.Encounter_Reschedule_Status == 'Scheduled')))
		const PT_Profile_Picture = req.user.PT_Profile_Picture ? await serviceModule.getImage(req.user.PT_Profile_Picture) : null

		let upcommingEncounter = upcommingVisit.length > 0 ? await getVisits(upcommingVisit) : []
		data.upcommingVisit = upcommingEncounter.length > 0 ? await parentDetails(upcommingEncounter) : []

		let DischargeInstructionsCards = []
		let AccessRequestCards = []
		let FollowupRequestCards = []
		let ChartSignedCards = []
		let AddendumAddedCards = []
		let EncounterCancelledCards = []
		let MedicalHistoryIncompleteCards = []
		let Order_As_Addendum_Cards = []
		let PaymentFailureCard = []
		let Encounter_Reschedule_Request_Cards = []
		let MedicalHistorySuggestedChangeCards = []
		let count = 0
		// let cards = []
		patientEncounter.map(async ele => {
			let shareChart = []
			if (ele.Encounter_Access_Provider.length == 0) {
				shareChart.push(ele)
			} else {
				if (!ele.Encounter_Access_Provider.find(find => (find.PRO_UID == ele.PRO_UID && find.Access_Status == 'Granted'))) shareChart.push(ele)
			}

			if (ele.Discharge_Instructions_Status == 'Delivered' && !ele.Discharge_Instructions_Viewed.includes(req.user.PT_HS_MRN)) {
				let dischargeData = await getUpdatedJson(ele)
				DischargeInstructionsCards.push(dischargeData)
			}
			if ((ele.Chart_is_Signed.length > 0 && ele.Chart_is_Signed[0] == true) && (ele.Chart_Viewed.length > 0 && !ele.Chart_Viewed[0].includes(req.user.PT_HS_MRN))) {
				let chartData = await getUpdatedJson(ele)
				chartData.PRO_Gender = ele.PRO_Gender
				ChartSignedCards.push(chartData)
			}
			if (ele.Encounter_Status == 'Active' && ele.PRO_Addendums.length > 0) {
				ele.PRO_Addendums.forEach(async item => {
					for (let index = 0; index < item.length; index++) {
						const element = item[index]

						if (element.Addendum_Is_Signed) {
							let viewAddedum = ele.Addendum_Viewed_By.find(ele => ele.Addendum_Id == element._id)
							if (!viewAddedum || viewAddedum.User != req.user.PT_HS_MRN) {
								let data = await getUpdatedJson(ele)
								data._id = element._id
								data.PRO_Gender = ele.PRO_Gender
								data.PRO_Addendum_Note = element.PRO_Addendum_Note
								data.ICD10_Code = element.ICD10_Code
								data.PRO_Addendum_Attachment = element.PRO_Addendum_Attachment.length > 0 ? await serviceModule.getFileUrl(element.PRO_Addendum_Attachment) : []
								AddendumAddedCards.push(data)
							}
						}
					}
				})
			}
			if (ele.order.length > 0) {
				ele.order.map(async (item) => {
					if (item.Order_As_Addendum == true) {
						if (!item.Order_Viewed_By.includes(req.user.PT_HS_MRN)) {
							let orderData = await getUpdatedJson(ele)
							orderData.orderType = item.Order_Type
							orderData.Order_Text = item.Order_Text
							orderData.PRO_Gender = ele.PRO_Gender
							orderData.Order_ICD10_Code = item.Order_ICD10_Code
							orderData.Order_UID = item.Order_UID
							orderData.Order_Stat = item.Order_Stat
							orderData.Order_Status = item.Order_Status
							orderData.Order_Reminder = item.Order_Reminder
							orderData.Order_Review_Status = item.Order_Review_Status
							Order_As_Addendum_Cards.push(orderData)
						}
					}
				})
			}
			if (ele.Encounter_Access_Provider && ele.Encounter_Access_Provider.length > 0 && ele.PT_Username == req.user.PT_Username) {
				ele.Encounter_Access_Provider.forEach(async item => {
					if (item.Access_Status == 'Requested') {
						let requestedBy = await providerModule.findById(item.PRO_UID)
						let cardData = await getUpdatedJson(ele)
						cardData.requestedBy = requestedBy.PRO_Last_Name
						cardData.requestedBy_UID = requestedBy.PRO_UID
						cardData.Encounter_Chief_Complaint_PT = ele.Encounter_Chief_Complaint_PT ? ele.Encounter_Chief_Complaint_PT : null
						AccessRequestCards.push(cardData)
					}
				})
			}
			let card = null
			if (ele.Encounter_Followup_Status == 'Requested' && ele.Followup_Created_By == 'Provider') {
				try {
					if (ele.Payment_Card_Id) {
						card = await stripeModule.getCardByCardId(req.user.stripeCustomerId, ele.Payment_Card_Id)
					}
				} catch (error) {
					if (error) card = null
				};
				let Encounter = patientEncounter.filter(a => a.Parent_Encounter == ele.Encounter_UID)
				Encounter.map(async item => {
					if (item.Encounter_Status == 'Requested' && (Date.parse(moment(new Date(`${item.Encounter_Date} ${item.Encounter_Time}`)).format('YYYY-MM-DDTHH:mm')) > Date.parse(momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm')))) {
						FollowupRequestCards.push(await getAttnVisit('followup', item, shareChart, card, ele))
						FollowupRequestCards = _.uniqBy(FollowupRequestCards, 'Encounter_UID')
					}
				})
			}
			if (ele.Encounter_Status == 'Cancelled' && ele.Encounter_Cancelled_By == 'Provider') {
				try {
					if (ele.Payment_Card_Id) {
						card = await stripeModule.getCardByCardId(req.user.stripeCustomerId, ele.Payment_Card_Id)
					}
				} catch (error) {
					if (error) card = null
				};
				EncounterCancelledCards.push(await getAttnVisit('cancel', ele, shareChart, card))
				EncounterCancelledCards = _.uniqBy(EncounterCancelledCards, 'Encounter_UID')
			}
			if (ele.Encounter_Reschedule_Status == 'Requested' && ele.Reschedule_Requested_By == 'Provider' && (Date.parse(moment(new Date(`${ele.Encounter_Date} ${ele.Encounter_Time}`)).format('YYYY-MM-DDTHH:mm')) > Date.parse(momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm')))) {
				try {
					if (ele.Payment_Card_Id) {
						card = await stripeModule.getCardByCardId(req.user.stripeCustomerId, ele.Payment_Card_Id)
					}
				} catch (error) {
					if (error) card = null
				};
				Encounter_Reschedule_Request_Cards.push(await getAttnVisit('reschedule', ele, shareChart, card))
				Encounter_Reschedule_Request_Cards = _.uniqBy(Encounter_Reschedule_Request_Cards, 'Encounter_UID')
			}
			if (ele.Transaction_Status == 'Failed') {
				try {
					if (ele.Payment_Card_Id) {
						card = await stripeModule.getCardByCardId(req.user.stripeCustomerId, ele.Payment_Card_Id)
					}
				} catch (error) {
					if (error) card = null
				};
				PaymentFailureCard.push(await getAttnVisit('payment', ele, shareChart, card))
			}

			if (ele.Encounter_Status == 'Active') {
				let diffrence = false
				let patientHistory = await patientHxModule.findPatientHistoryById(ele.PT_HS_MRN)
				let patient = await patientModule.findById(ele.PT_HS_MRN)
				let patientEncounterHistory = ele.PT_History
				let encounterData = {}
				if (patientHistory.Updated_At.getTime() < patientEncounterHistory.Updated_At.getTime()) {
					encounterData.PRO_Profile_Picture = ele.PRO_Profile_Picture != null ? await serviceModule.getImage(ele.PRO_Profile_Picture) : null
					encounterData.PRO_Last_Name = ele.PRO_Last_Name
					encounterData.Encounter_Date = ele.Encounter_Date
					encounterData.Encounter_UID = ele.Encounter_UID
					encounterData.Encounter_Time = ele.Encounter_Time
					encounterData.Encounter_Chief_Complaint_PRO = ele.Encounter_Chief_Complaint_PRO
					encounterData.Encounter_Chief_Complaint_PT = ele.Encounter_Chief_Complaint_PT
					encounterData.Encounter_PT_Chief_Complaint_More = ele.Encounter_PT_Chief_Complaint_More
					encounterData.Encounter_Type = ele.Encounter_Type
					encounterData.PT_First_Name = patient.PT_First_Name
					encounterData.PT_Last_Name = patient.PT_Last_Name
					encounterData.PT_Gender = patient.PT_Gender
					encounterData.PT_HS_MRN = patient.PT_HS_MRN
					encounterData.PT_Profile_Picture = patient.PT_Profile_Picture != null ? await serviceModule.getImage(patient.PT_Profile_Picture) : null
					encounterData.PT_Age_Now = patient.PT_DOB ? await calculateAge(patient.PT_DOB) : null
					encounterData.PT_DOB = patient.PT_DOB ? patient.PT_DOB : null
					var jsonObject1 = patientEncounterHistory
					var jsonObject2 = patientHistory
					let differanceArray = {}

					delete jsonObject1._id
					delete jsonObject2._id
					delete jsonObject1.Created_At
					delete jsonObject2.Created_At
					delete jsonObject1.Updated_At
					delete jsonObject2.Updated_At
					delete jsonObject1.__v
					delete jsonObject2.__v

					var keys = Object.keys(jsonObject1)
					for (var i = 0; i < keys.length; i++) {
						var key = keys[i]
						if (key != 'PRO_UID' && key != 'PT_Username' && key != 'PT_Organ_Donor' && key != 'PT_First_Name' && key != 'PT_Profile_Picture') {
							if (key == 'PT_Allergies' || key == 'PT_Family_History_Info' || key == 'PT_Medication' || key == 'PT_Medication_Attachment' || key == 'PT_Medical_Directive_Attachment' || key == 'PT_Medical_History_Attachment') {
								jsonObject1[key] = jsonObject1[key] == null ? [] : typeof (jsonObject1[key]) == 'string' ? jsonObject1[key] == '' ? [] : [jsonObject1[key]] : jsonObject1[key]
								jsonObject2[key] = jsonObject2[key] == null ? [] : typeof (jsonObject2[key]) == 'string' ? jsonObject1[key] == '' ? [] : [jsonObject2[key]] : jsonObject2[key]
							}
							if (jsonObject1[key] != null && typeof (jsonObject1[key]) == 'object') {
								diffrence = true
								differanceArray[key] = {
									newValue: jsonObject1[key] == null || jsonObject1[key] == undefined ? '' : typeof (jsonObject1[key]) == 'number' ? `${jsonObject1[key]}` : typeof (jsonObject1[key]) == 'object' ? await getImageUrls(jsonObject1[key]) : jsonObject1[key],
									oldValue: jsonObject2[key] == null || jsonObject2[key] == undefined ? '' : typeof (jsonObject2[key]) == 'number' ? `${jsonObject2[key]}` : typeof (jsonObject2[key]) == 'object' ? await getImageUrls(jsonObject2[key]) : jsonObject2[key]
								}
							} else {
								if (jsonObject1[key] !== jsonObject2[key]) {
									diffrence = true
									differanceArray[key] = {
										newValue: jsonObject1[key] == null || jsonObject1[key] == undefined ? '' : typeof (jsonObject1[key]) == 'number' ? `${jsonObject1[key]}` : typeof (jsonObject1[key]) == 'object' ? await getImageUrls(jsonObject1[key]) : jsonObject1[key],
										oldValue: jsonObject2[key] == null || jsonObject2[key] == undefined ? '' : typeof (jsonObject2[key]) == 'number' ? `${jsonObject2[key]}` : typeof (jsonObject2[key]) == 'object' ? await getImageUrls(jsonObject2[key]) : jsonObject2[key]
									}
								} else {
									differanceArray[key] = {
										newValue: jsonObject1[key] == null || jsonObject1[key] == undefined ? '' : typeof (jsonObject1[key]) == 'number' ? `${jsonObject1[key]}` : typeof (jsonObject1[key]) == 'object' ? await getImageUrls(jsonObject1[key]) : jsonObject1[key],
										oldValue: jsonObject1[key] == null || jsonObject1[key] == undefined ? '' : typeof (jsonObject1[key]) == 'number' ? `${jsonObject1[key]}` : typeof (jsonObject1[key]) == 'object' ? await getImageUrls(jsonObject1[key]) : jsonObject1[key]
									}
								}
							}
						}
						if (i == keys.length - 1) {
							encounterData.changes = differanceArray
							if (diffrence) MedicalHistorySuggestedChangeCards.push(encounterData)
						}
					}
				}
			}

			count = count + 1
			if (count == patientEncounter.length) {
				const patientHistory = await patientHxModule.findByUserId(req.user.PT_HS_MRN)
				if (patientHistory) {
					var GeneralMedicalHistory = await medicalHistory(PT_Profile_Picture, patientHistory, req)
					var Medications = await medicationHistory(PT_Profile_Picture, patientHistory, req)
					var other = await otherHistory(PT_Profile_Picture, patientHistory, req)
					var OB = await obgynHistory(PT_Profile_Picture, patientHistory, req, patientEncounter)
				}
				attn.DischargeInstructionsCards = Array.isArray(DischargeInstructionsCards) ? DischargeInstructionsCards.sort(compareDsc) : []
				attn.AccessRequestCards = Array.isArray(AccessRequestCards) ? AccessRequestCards.sort(compareDsc) : []
				attn.FollowupRequestCards = Array.isArray(FollowupRequestCards) ? FollowupRequestCards.sort(compareDsc) : []
				attn.ChartSignedCards = Array.isArray(ChartSignedCards) ? ChartSignedCards.sort(compareDsc) : []
				attn.AddendumAddedCards = Array.isArray(AddendumAddedCards) ? AddendumAddedCards.sort(compareDsc) : []
				attn.Encounter_Reschedule_Request_Cards = Array.isArray(Encounter_Reschedule_Request_Cards) ? Encounter_Reschedule_Request_Cards.sort(compareDsc) : []
				attn.EncounterCancelledCards = Array.isArray(EncounterCancelledCards) ? EncounterCancelledCards : []
				attn.MedicalHistorySuggestedChangeCards = Array.isArray(MedicalHistorySuggestedChangeCards) ? MedicalHistorySuggestedChangeCards.sort(compareDsc) : []
				attn.MedicalHistoryIncompleteCards = await obgynFilterData(GeneralMedicalHistory, Medications, other, OB, MedicalHistoryIncompleteCards)
				attn.Order_As_Addendum_Cards = Array.isArray(Order_As_Addendum_Cards) ? Order_As_Addendum_Cards.sort(compareDsc) : []
				attn.PaymentFailureCard = Array.isArray(PaymentFailureCard) ? PaymentFailureCard.sort(compareDsc) : []
				data.attn = attn
				res.send({ code: 1, status: 'success', data: data })
			}
		})
	} catch (err) {
		res.send({ code: 2, status: 'failed', message: err.message, Error_Code: 'HS149' })
	}
})

let getImageUrls = async (arr) => {
	let newArray = []; let count = 0
	if (arr == null || arr.length == 0) {
		return newArray
	}
	if (arr.length == undefined) {
		return arr
	}
	if (arr[0].Attachment_Name) {
		for (var i = 0; i < arr.length; i++) {
			let url = arr[i].Attachment_Name != null || arr[i].Attachment_Name != '' ? await serviceModule.getImage(arr[i].Attachment_Name) : null
			newArray.push({
				Attachment_Url: url,
				Is_Deleted: arr[i].Is_Deleted,
				_id: arr[i]._id,
				Attachment_Name: arr[i].Attachment_Name,
				Attachment_By: arr[i].Attachment_By,
				Attachment_User: arr[i].Attachment_User
			})
			count = count + 1
			if (count == arr.length) {
				return newArray
			}
		}
	} else {
		return arr
	}
}
// get patient upcomming and past visits for visit screen in patient app //
router.get('/get-all-visits', authorize(Role.Patient), async (req, res) => {
	var data = {}
	var user_ids = [req.user.PT_HS_MRN]
	const minorPatient = await patientModule.findMinorIDByUserId(req.user.PT_HS_MRN)
	minorPatient.map(ele => {
		user_ids.push(ele.PT_HS_MRN)
	})
	let patientEncounter = await encounterModule.getPatientEncounterByUserID(user_ids)
	if (patientEncounter.length > 0) {
		let upcommingVisit = patientEncounter.filter(ele => (Date.parse(moment(new Date(`${ele.Encounter_Date} ${ele.Encounter_Time}`)).format('YYYY-MM-DDTHH:mm:ss')) >= Date.parse(momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm:ss')) && (ele.Encounter_Status == 'Scheduled' || ele.Encounter_Reschedule_Status == 'Scheduled')))
		let pastVisit = patientEncounter.filter(ele => Date.parse(moment(new Date(`${ele.Encounter_Date} ${ele.Encounter_Time}`)).format('YYYY-MM-DDTHH:mm:ss')) < Date.parse(momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm:ss')))

		if (upcommingVisit.length > 0) {
			upcommingVisit.sort(compareDsc)
			upcommingVisit = await getVisits(upcommingVisit)
			data.upcommingVisit = await parentDetails(upcommingVisit)
		} else {
			data.upcommingVisit = []
		}
		if (pastVisit.length > 0) {
			pastVisit.sort(compareDsc)
			pastVisit = await getVisits(pastVisit)
			data.pastVisit = _.uniqBy(pastVisit, 'Encounter_UID')
		} else {
			data.pastVisit = []
		}

		return res.send({ code: 1, status: 'success', data: data })
	} else {
		data.upcommingVisit = []
		data.pastVisit = []
		return res.send({ code: 1, status: 'success', data: data })
	}
})

let parentDetails = async (encounterArray) => {
	let arr = []
	for (let index = 0; index < encounterArray.length; index++) {
		const element = encounterArray[index]
		var o = Object.assign({}, element)
		if (o.Encounter_Type == 'Followup Visit') {
			let parent = await encounterModule.getEncounterByEncounter_UID(o.Parent_Encounter)
			o.Parent_Encounter_Date = parent ? parent.Encounter_Date : null
			o.Parent_Encounter_Cheif_Complaint = parent ? parent.Encounter_Chief_Complaint_PRO : null
			arr.push(o)
		} else {
			o.Parent_Encounter_Date = null
			o.Parent_Encounter_Cheif_Complaint = null
			arr.push(o)
		}
		if (index == encounterArray.length - 1) {
			return arr
		}
	}
}

async function calculateAge(birthday) { // birthday is a date
    var ageDifMs = Date.now() - birthday.getTime();
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}
async function getVisits (arr) {
	var visit = arr.map(ele => ({
		PT_Profile_Picture: ele.PT_Profile_Picture ? s3.getSignedUrl('getObject', {
			Bucket: process.env.AWS_BUCKET,
			Key: ele.PT_Profile_Picture,
			Expires: 60 * 60 * 24 * 15 // time in seconds: e.g. 60 * 5 = 5 mins
		}) : null,
		PT_First_Name: ele.PT_First_Name,
		PT_Last_Name: ele.PT_Last_Name,
		PT_HS_MRN: ele.PT_HS_MRN,
		Encounter_UID: ele.Encounter_UID,
		Encounter_Type: ele.Encounter_Type,
		Encounter_Location_Type: ele.Encounter_Location_Type,
		Encounter_Date: ele.Encounter_Date,
		Encounter_Time: ele.Encounter_Time,
		Encounter_Status: ele.Encounter_Status,
		Parent_Encounter: ele.Parent_Encounter,
		Encounter_Reschedule_Status: ele.Encounter_Reschedule_Status,
		Cancellation_Reason: ele.Cancellation_Reason,
		Encounter_Chief_Complaint_PRO: ele.Encounter_Chief_Complaint_PRO,
		PRO_First_Name: ele.PRO_First_Name,
		PRO_Last_Name: ele.PRO_Last_Name,
		PRO_Profile_Picture: ele.PRO_Profile_Picture ? s3.getSignedUrl('getObject', {
			Bucket: process.env.AWS_BUCKET,
			Key: ele.PRO_Profile_Picture,
			Expires: 60 * 60 * 24 * 15 // time in seconds: e.g. 60 * 5 = 5 mins
		}) : null,
		PRO_Gender: ele.PRO_Gender,
		PRO_UID: ele.PRO_UID
	}))
	return visit
}

async function getAttnVisit (type, ele, shareChart, card, parentEncounter) {
	let day = moment(new Date(ele.Encounter_Date)).format('ddd')
	if (day == 'Thu') day = day + 'r'
	if (day == 'Tue') day = day + 's'
	let netDate = ele.Pro_Office_Hours_Time.find(ele => ele.day == day)
	let fromTime = netDate.fromTime.time.includes(':') ? netDate.fromTime.time + ' ' + netDate.fromTime.zone : netDate.fromTime.time + ':00' + ' ' + netDate.fromTime.zone
	let toTime = netDate.toTime.time.includes(':') ? netDate.toTime.time + ' ' + netDate.toTime.zone: netDate.toTime.time + ':00' +  ' ' + netDate.toTime.zone
	fromTime = Date.parse(moment(new Date(`${ele.Encounter_Date} ${fromTime}`)))
	toTime = Date.parse(moment(new Date(`${ele.Encounter_Date} ${toTime}`)))

	let encounterTime = type = 'reschedule' ? Date.parse(moment(new Date(`${ele.Reschedule_Request_Date} ${ele.Reschedule_Request_Time}`))) : Date.parse(moment(new Date(`${ele.Encounter_Date} ${ele.Encounter_Time}`)))
	let Pro_Office_Hours_Time = encounterTime >= fromTime && encounterTime <=toTime ? true : false

	var data = {
		Parent_Encounter_Date: parentEncounter ? parentEncounter.Encounter_Date : null,
		PT_Profile_Picture: ele.PT_Profile_Picture ? await serviceModule.getImage(ele.PT_Profile_Picture) : null,
		PT_First_Name: ele.PT_First_Name,
		PT_Last_Name: ele.PT_Last_Name,
		PT_HS_MRN: ele.PT_HS_MRN,
		PRO_Profile_Picture: ele.PRO_Profile_Picture ? await serviceModule.getImage(ele.PRO_Profile_Picture) : null,
		PRO_First_Name: ele.PRO_First_Name,
		PRO_Last_Name: ele.PRO_Last_Name,
		Encounter_UID: ele.Encounter_UID,
		Encounter_Location_Type: ele.Encounter_Location_Type,
		Encounter_Date: ele.Encounter_Date,
		Encounter_Time: ele.Encounter_Time,
		Encounter_Start_Time: ele.Encounter_Start_Time,
		Encounter_End_Time: ele.Encounter_End_Time,
		Encounter_Start_Time_Zone: ele.Encounter_Start_Time_Zone,
		Encounter_End_Time_Zone: ele.Encounter_End_Time_Zone,
		Encounter_Status: ele.Encounter_Status,
		Encounter_Reschedule_Status: ele.Encounter_Reschedule_Status,
		Encounter_Cost: ele.Encounter_Cost,
		Cancellation_Reason: ele.Cancellation_Reason,
		PRO_Gender: ele.PRO_Gender,
		PRO_UID: ele.PRO_UID,
		Order_Type: ele.orderType,
		Order_Text: ele.Order_Text,
		Order_ICD10_Code: ele.Order_ICD10_Code,
		Order_UID: ele.Order_UID,
		Order_Stat: ele.Order_Stat,
		Order_Status: ele.Order_Status,
		Order_Reminder: ele.Order_Reminder,
		Order_Review_Status: ele.Order_Review_Status,
		Encounter_Chief_Complaint_PRO: ele.Encounter_Chief_Complaint_PRO,
		Encounter_Chief_Complaint_PT: ele.Encounter_Chief_Complaint_PT,
		PT_Home_Address_Line_1: ele.PT_Home_Address_Line_1 ? ele.PT_Home_Address_Line_1 : null,
		PT_Home_Address_Line_2: ele.PT_Home_Address_Line_2 ? ele.PT_Home_Address_Line_2 : null,
		PT_Home_Address_Latitude: ele.PT_Home_Address_Latitude ? ele.PT_Home_Address_Latitude : null,
		PT_Home_Address_Longitude: ele.PT_Home_Address_Longitude ? ele.PT_Home_Address_Longitude : null,
		PT_Home_City: ele.PT_Home_City ? ele.PT_Home_City : null,
		PT_Home_State: ele.PT_Home_State ? ele.PT_Home_State : null,
		PT_Home_Zip: ele.PT_Home_Zip ? ele.PT_Home_Zip : null,
		PRO_Office_Address_Line_1: ele.PRO_Office_Address_Line_1 ? ele.PRO_Office_Address_Line_1 : null,
		PRO_Office_Address_Line_2: ele.PRO_Office_Address_Line_2 ? ele.PRO_Office_Address_Line_2 : null,
		PRO_Office_Address_Latitude: ele.PRO_Office_Address_Latitude ? ele.PRO_Office_Address_Latitude : null,
		PRO_Office_Address_Longitude: ele.PRO_Office_Address_Longitude ? ele.PRO_Office_Address_Longitude : null,
		PRO_Office_City: ele.PRO_Office_City ? ele.PRO_Office_City : null,
		PRO_Office_State: ele.PRO_Office_State ? ele.PRO_Office_State : null,
		PRO_Office_Zip: ele.PRO_Office_Zip ? ele.PRO_Office_Zip : null,
		Share_All_Chart: shareChart.length > 0 ? false : true,
		PT_Share_Records: ele.PT_Share_Records ? ele.PT_Share_Records : 'No',
		PT_Credit_Card_Type: card ? card.brand : null,
		PT_Credit_Card_Short_Number: card ? card.last4 : null,
		Reschedule_Request_Date: ele.Reschedule_Request_Date,
		Reschedule_Request_Time: ele.Reschedule_Request_Time,
		Pro_Office_Hours_Time: Pro_Office_Hours_Time
	}
	return data
}

function compareDsc (a, b) {
	if (new Date(b.Encounter_Date + ' ' + b.Encounter_Time) < new Date(a.Encounter_Date + ' ' + a.Encounter_Time)) {
		return -1
	}
	if (new Date(b.Encounter_Date + ' ' + b.Encounter_Time) > new Date(a.Encounter_Date + ' ' + a.Encounter_Time)) {
		return 1
	}
	return 0
}

// function compareAsc (a, b) {
//   if (new Date(a.Encounter_Date + ' ' + a.Encounter_Time) < new Date(b.Encounter_Date + ' ' + b.Encounter_Time)) {
//     return -1
//   }
//   if (new Date(a.Encounter_Date + ' ' + a.Encounter_Time) > new Date(b.Encounter_Date + ' ' + b.Encounter_Time)) {
//     return 1
//   }
//   return 0
// }

let getUpdatedJson = async (data) => {
	let json = {
		PT_Profile_Picture: data.PT_Profile_Picture ? await serviceModule.getImage(data.PT_Profile_Picture) : null,
		PRO_Profile_Picture: data.PRO_Profile_Picture ? await serviceModule.getImage(data.PRO_Profile_Picture) : null,
		PT_First_Name: data.PT_First_Name,
		PT_Last_Name: data.PT_Last_Name,
		PRO_First_Name: data.PRO_First_Name,
		PRO_UID: data.PRO_UID,
		PRO_Last_Name: data.PRO_Last_Name ? data.PRO_Last_Name : null,
		Encounter_Date: data.Encounter_Date,
		Encounter_Time: data.Encounter_Time.includes(' ') ? data.Encounter_Time : `${data.Encounter_Time.slice(0, -2)} ${data.Encounter_Time.slice(-2)}`,
		Encounter_UID: data.Encounter_UID,
		Encounter_Chief_Complaint_PRO: data.Encounter_Chief_Complaint_PRO ? data.Encounter_Chief_Complaint_PRO : null
	}
	return json
}

async function medicalHistory (PT_Profile_Picture, patientHistory, req) {
	let GeneralMedicalHistory = {
		PT_First_Name: req.user.PT_First_Name,
		PT_Last_Name: req.user.PT_Last_Name,
		PT_Profile_Picture: PT_Profile_Picture,
		PT_Primary_Care_Provider_Toggle: patientHistory.PT_Primary_Care_Provider_Toggle,
		PT_Past_Year_Medical_Changes: patientHistory.PT_Past_Year_Medical_Changes,
		PT_Heart_Disease: patientHistory.PT_Heart_Disease,
		PT_Hypertension: patientHistory.PT_Hypertension,
		PT_Diabetes: patientHistory.PT_Diabetes,
		PT_Cancer: patientHistory.PT_Cancer,
		PT_Immune_Diseases: patientHistory.PT_Immune_Diseases,
		PT_Surgery: patientHistory.PT_Surgery,
		PT_Neuro_Issues: patientHistory.PT_Neuro_Issues,
		PT_Other_Diseases: patientHistory.PT_Other_Diseases,
		PT_Lab_and_Image_Reports_Toggle: patientHistory.PT_Lab_and_Image_Reports_Toggle,
		PT_Medical_Directives_Toggle: patientHistory.PT_Medical_Directives_Toggle,
		PT_Hx_General_Confirmation: patientHistory.PT_Hx_General_Confirmation ? patientHistory.PT_Hx_General_Confirmation : 'Unchecked'
	}
	return GeneralMedicalHistory
}

async function medicationHistory (PT_Profile_Picture, patientHistory, req) {
	let Medications = {
		PT_First_Name: req.user.PT_First_Name,
		PT_Last_Name: req.user.PT_Last_Name,
		PT_Medications_Toggle: patientHistory.PT_Medications_Toggle,
		PT_Hx_Medications_Confirmation: patientHistory.PT_Hx_Medications_Confirmation ? patientHistory.PT_Hx_Medications_Confirmation : 'Unchecked',
		PT_Profile_Picture: PT_Profile_Picture
	}
	return Medications
}

async function otherHistory (PT_Profile_Picture, patientHistory, req) {
	let other = {
		PT_First_Name: req.user.PT_First_Name,
		PT_Last_Name: req.user.PT_Last_Name,
		PT_Allergies_Toggle: patientHistory.PT_Allergies_Toggle,
		PT_Occupation: patientHistory.PT_Occupation,
		PT_Hx_Other_Confirmation: patientHistory.PT_Hx_Other_Confirmation ? patientHistory.PT_Hx_Other_Confirmation : 'Unchecked',
		PT_Profile_Picture: PT_Profile_Picture,
		PT_Recreational_Drugs: patientHistory.PT_Recreational_Drugs,
		PT_Tobacco_Usage: patientHistory.PT_Tobacco_Usage,
		PT_Alcohol_Usage: patientHistory.PT_Alcohol_Usage
	}
	return other
}

async function obgynHistory (PT_Profile_Picture, patientHistory, req, patientEncounter) {
	let last_updated_ob = await getDays(req.user.PT_HX_Last_Updated_OB_Section, new Date())
	let OB = req.user.PT_Gender == 'Female' ? ((patientHistory.PT_Hx_OB_Confirmation == 'Unchecked' || patientEncounter.reduce((arr, ele) => {
		if (moment(new Date(ele.Encounter_Date)).fromNow().split(' ')[0] < 4) { arr.push(ele.Encounter_Date) } return arr
	}, []).length > 0) && last_updated_ob > 15) ? {
			PT_First_Name: req.user.PT_First_Name,
			PT_Last_Name: req.user.PT_Last_Name,
			Pregnancy_History: patientHistory.Pregnancy_History,
			PT_Pregnancy_Complications: patientHistory.PT_Pregnancy_Complications,
			Pregnant_Now: patientHistory.Pregnant_Now,
			Oral_Contraceptives: patientHistory.Oral_Contraceptives,
			PT_Hx_OB_Confirmation: patientHistory.PT_Hx_OB_Confirmation ? patientHistory.PT_Hx_OB_Confirmation : 'Unchecked',
			PT_Profile_Picture: PT_Profile_Picture
		} : {
			PT_First_Name: req.user.PT_First_Name,
			PT_Last_Name: req.user.PT_Last_Name,
			Pregnancy_History: patientHistory.Pregnancy_History,
			PT_Pregnancy_Complications: patientHistory.PT_Pregnancy_Complications,
			Pregnant_Now: patientHistory.Pregnant_Now,
			Oral_Contraceptives: patientHistory.Oral_Contraceptives,
			PT_Hx_OB_Confirmation: patientHistory.PT_Hx_OB_Confirmation ? patientHistory.PT_Hx_OB_Confirmation : 'Unchecked',
			PT_Profile_Picture: PT_Profile_Picture
		} : {
		PT_First_Name: req.user.PT_First_Name,
		PT_Last_Name: req.user.PT_Last_Name,
		Pregnancy_History: null,
		PT_Pregnancy_Complications: null,
		Pregnant_Now: null,
		Oral_Contraceptives: null,
		PT_Hx_OB_Confirmation: null,
		PT_Profile_Picture: PT_Profile_Picture
	}
	return OB
}

let getDays = async (date1, date2) => {
	var Difference_In_Time = date2.getTime() - new Date(date1).getTime()
	var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24)
	return Difference_In_Days
}
async function obgynFilterData (GeneralMedicalHistory, Medications, other, OB, MedicalHistoryIncompleteCards) {
	let value1 = Object.values(GeneralMedicalHistory)
	let value2 = Object.values(Medications)
	let value3 = Object.values(other)
	let value4 = Object.values(OB)
	value1 = value1.includes('No') ? true : false
	value2 = value2.includes('No') ? true : false
	value3 = value3.includes('No') ? true : false
	value4 = (value4.includes(null) || value4.includes('No')) ? true : false

	MedicalHistoryIncompleteCards.push({
		GeneralMedicalHistory,
		Medications,
		other,
		'OB/GYN': OB
	})
	MedicalHistoryIncompleteCards = value1 ? MedicalHistoryIncompleteCards : (MedicalHistoryIncompleteCards[0].GeneralMedicalHistory = {}, MedicalHistoryIncompleteCards = MedicalHistoryIncompleteCards)
	MedicalHistoryIncompleteCards = value2 ? MedicalHistoryIncompleteCards : (MedicalHistoryIncompleteCards[0].Medications = {}, MedicalHistoryIncompleteCards = MedicalHistoryIncompleteCards)
	MedicalHistoryIncompleteCards = value3 ? MedicalHistoryIncompleteCards : (MedicalHistoryIncompleteCards[0].other = {}, MedicalHistoryIncompleteCards = MedicalHistoryIncompleteCards)
	MedicalHistoryIncompleteCards = value4 ? MedicalHistoryIncompleteCards : (MedicalHistoryIncompleteCards[0]['OB/GYN'] = {}, MedicalHistoryIncompleteCards = MedicalHistoryIncompleteCards)
	return Array.isArray(MedicalHistoryIncompleteCards) ? MedicalHistoryIncompleteCards : []
}
module.exports = router
