/* eslint-disable no-unused-expressions */
/* eslint-disable no-self-assign */
/* eslint-disable camelcase */
/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
const Router = require('express').Router
const router = new Router()
const patientModule = require('../../models').Patient
const chartModule = require('../../models').Chart
const encounterModule = require('../../models').Encounter
const orderModule = require('../../models').Order
const errorModule = require('../../models').Errors
const patientHistoryModule = require('../../models').PatientHistory
const patientProblemModule = require('../../models').PatientProblem
const auditModule = require('../../models').Audit
var { auditUid } = require('../../utils/UID')
var auditType = require('../../auditType.json')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
var { isAValidPhoneNumber } = require('../../utils/validate')
const { bcryptHash } = require('../../utils/crypto')
const jwt = require('jsonwebtoken')
const logger = require('../../models/errorlog')

const moment = require('moment')
var aws = require('aws-sdk')
aws.config.update({
  secretAccessKey: process.env.AWS_SECRET_KEY,
  accessKeyId: process.env.AWS_ACCESS_KEY,
  region: 'us-east-1'
})

var s3 = new aws.S3()

// Update patient and patient history //
router.put('/update-patient-info', authorize(Role.Patient), async (req, res) => {
  try {
    const id = req.body.PT_HS_MRN ? req.body.PT_HS_MRN : req.user.PT_HS_MRN
    const data = req.body
    const patient = await patientModule.findById(id)
    if (!patient) return res.send({ code: 0, status: 'failed', message: `patient with id '${id}' is not found!`, Error_Code: errorModule.patient_not_found() })

    if (data.PT_Username) delete data.PT_Username
    if (data.PT_Password) delete data.PT_Password
    if (data.PT_Cell_Phone) {
      var validNumber = isAValidPhoneNumber(data.PT_Cell_Phone)
      if (!validNumber) return res.send({ code: 0, status: 'failed', message: 'Please make sure to enter a sequence of numbers. No letters or special characters are accepted.', Error_Code: 'HS185' })
    }
    if (data.PT_DOB) {
      data.PT_DOB = moment(new Date(data.PT_DOB)).format('MM/DD/YYYY')
      data.PT_Age_Now = new Date().getFullYear() - new Date(data.PT_DOB).getFullYear()
    }
    if (data.PT_Emergency_Contact_Phone) {
      let validNumber = isAValidPhoneNumber(data.PT_Emergency_Contact_Phone)
      if (!validNumber) return res.send({ code: 0, status: 'failed', message: 'Please make sure to enter a sequence of numbers. No letters or special characters are accepted.', Error_Code: 'HS185' })
    }
    if (data.PT_HX_Last_Updated_About_Section) data.PT_HX_Last_Updated_About_Section = data.PT_HX_Last_Updated_About_Section
    if (data.PT_HX_Last_Updated_General_Section) data.PT_HX_Last_Updated_General_Section = data.PT_HX_Last_Updated_General_Section
    if (data.PT_HX_Last_Updated_Medications_Section) data.PT_HX_Last_Updated_Medications_Section = data.PT_HX_Last_Updated_Medications_Section
    if (data.PT_HX_Last_Updated_Other_Section) data.PT_HX_Last_Updated_Other_Section = data.PT_HX_Last_Updated_Other_Section
    if (data.PT_HX_Last_Updated_OB_Section) data.PT_HX_Last_Updated_OB_Section = data.PT_HX_Last_Updated_OB_Section
    const patient_history = await patientHistoryModule.findByUserId(id)
    if (data.PT_Primary_Care_Provider_Toggle == 'No') data.PT_Primary_Care_Provider = null
    if (data.PT_Past_Year_Medical_Changes == 'No') data.PT_Past_Year_Medical_Changes_Note = null
    if (data.PT_Heart_Disease == 'No') data.PT_Heart_Disease_Note = null
    if (data.PT_Hypertension == 'No') data.PT_Hypertension_Note = null
    if (data.PT_Diabetes == 'No') data.PT_Diabetes_Note = null
    if (data.PT_Cancer == 'No') data.PT_Cancer_Note = null
    if (data.PT_Immune_Diseases == 'No') data.PT_Immune_Diseases_Note = null
    if (data.PT_Surgery == 'No') data.PT_Surgery_Note = null
    if (data.PT_Neuro_Issues == 'No') data.PT_Neuro_Issues_Note = null
    if (data.PT_Other_Diseases == 'No') data.PT_Other_Diseases_Note = null
    if (data.PT_Medical_Directives_Toggle == 'No') data.PT_Medical_Directive = null
    if (data.PT_Allergies_Toggle == 'No') data.PT_Allergies = []
    if (data.PT_Recreational_Drugs == 'No') data.PT_Recreational_Drugs_Note = null
    if (data.PT_Tobacco_Usage && data.PT_Tobacco_Usage == null) data.PT_Tobacco_Usage = 'No'
    if (data.PT_Tobacco_Usage == 'No') data.PT_Tobacco_Usage_Note = null
    if (data.PT_Alcohol_Usage == 'No') data.PT_Alcohol_Usage_Note = null
    if (data.PT_Lab_and_Image_Reports_Toggle == 'No') {
      data.PT_Lab_and_Image_Reports = []
      data.PT_Medical_History_Attachment = []
    }
    if (data.PT_Medical_Directives_Toggle == 'No') data.PT_Medical_Directive_Attachment = []
    if (data.PT_Medications_Toggle === 'No') {
      data.PT_Medication = []
      data.PT_Medication_Attachment = []
    }
    if (patient.PT_Gender == 'Male') {
      let obgynData = {
        Pregnancy_History: 'No',
        PT_Last_Menstrual_Cycle_Date: null,
        PT_Number_Of_Pregnancies: 0,
        PT_Number_Of_Deliveries: 0,
        PT_Pregnancy_Complications: 'No',
        PT_Pregnancy_Complications_Note: null,
        Pregnant_Now: 'No',
        Expected_Delivery_Date: null,
        Nursing_Now: 'No',
        Oral_Contraceptives_Info: null,
        Oral_Contraceptives: 'No'
      }
      await patientHistoryModule.updateById(id, obgynData)
      if (data.Pregnancy_History || data.Pregnant_Now || data.Oral_Contraceptives || data.PT_Last_Menstrual_Cycle_Date) {
        return res.send({ code: 0, status: 'failed', message: 'OB/GYN senction can not update for Male patient' })
      }
    }
    if (data.Pregnancy_History == 'No') {
      data.PT_Number_Of_Pregnancies = 0
      data.PT_Number_Of_Deliveries = 0
      data.PT_Pregnancy_Complications = 'No'
      data.PT_Pregnancy_Complications_Note = null
    }
    if (data.Pregnancy_History == 'Yes') {
      if (data.PT_Pregnancy_Complications == 'No') data.PT_Pregnancy_Complications_Note = null
    }
    if (data.Pregnant_Now == 'No') data.Expected_Delivery_Date = null
    if (data.Oral_Contraceptives == 'No') data.Oral_Contraceptives_Info = null
    const auditData = {
      Audit_UID: await auditUid(),
      Audit_Info: 'New- ' + await auditUid(),
      Audit_Type: auditType.change,
      Data_Point: 'PT/update info',
      PT_First_Name: req.user.PT_First_Name,
      PT_Last_Name: req.user.PT_Last_Name,
      PT_HSID: req.user.PT_HS_MRN,
      IP_Address: await auditModule.getIpAddress(req)
    }
    await auditModule.create(auditData)
    if (patient_history) {
      await patientHistoryModule.updateById(id, data)
      await updateUpcommingEncounterPatientHistory(id, data)
    } else {
      data.PT_HS_MRN = id
      await patientHistoryModule.create(data)
      await updateUpcommingEncounterPatientHistory(id, data)
    }
    delete data.PT_HS_MRN
    await patientModule.updatePatientById(id, data)
    if (patient.PT_Is_Minor == 'No') {
      delete data.PT_HX_Last_Updated_About_Section
      if (data.PT_Home_Address_Line_1 || data.PT_Home_Address_Line_2 || data.PT_Cell_Phone || data.PT_Home_Address_Latitude || data.PT_Home_Address_Longitude || data.PT_Home_City || data.PT_Home_State || data.PT_Home_Zip) {
        let minorData = {}
        data.PT_Cell_Phone ? minorData.PT_Cell_Phone = data.PT_Cell_Phone : null
        data.PT_Home_Address_Line_1 ? minorData.PT_Home_Address_Line_1 = data.PT_Home_Address_Line_1 : null
        data.PT_Home_Address_Line_2 ? minorData.PT_Home_Address_Line_2 = data.PT_Home_Address_Line_2 : null
        data.PT_Home_City ? minorData.PT_Home_City = data.PT_Home_City : null
        data.PT_Home_Zip ? minorData.PT_Home_Zip = data.PT_Home_Zip : null
        data.PT_Home_State ? minorData.PT_Home_State = data.PT_Home_State : null
        data.PT_Home_Address_Latitude ? minorData.PT_Home_Address_Latitude = data.PT_Home_Address_Latitude : null
        data.PT_Home_Address_Longitude ? minorData.PT_Home_Address_Longitude = data.PT_Home_Address_Longitude : null
        await patientModule.updateMinorById(id, minorData)
      }
    }
    let Encounter_UID = data.Encounter_UID
    delete data.Encounter_UID
    await encounterModule.updateEncounter(Encounter_UID, data)
    res.send({
      code: 1,
      status: 'success',
      message: 'patient info updated successfully!',
      PT_HS_MRN: id,
      PT_Profile_Picture: patient.PT_Profile_Picture ? s3.getSignedUrl('getObject', {
        Bucket: process.env.AWS_BUCKET,
        Key: req.body.PT_Profile_Picture ? req.body.PT_Profile_Picture : patient.PT_Profile_Picture,
        Expires: 60 * 60 * 24 * 15 // time in seconds: e.g. 60 * 5 = 5 mins
      }) : null
    })
    // }
  } catch (err) {
    logger.error(err.message)
    logger.fatal(err)
    console.log(err)
    res.send({ code: 2, status: 'failed', message: err.message, Error_Code: 'HS186' })
  }
})

router.put('/update-email', authorize(Role.Patient), async (req, res) => {
  try {
    var { body: { PT_Username } } = req
    if (!PT_Username) return res.send({ code: 0, status: 'failed', message: '*Pleease provide a valid email address', Error_Code: errorModule.patient_valid_email() })
    if (PT_Username == '') return res.send({ code: 0, status: 'failed', message: '*Pleease provide a valid email address', Error_Code: errorModule.patient_valid_email() })
    PT_Username = PT_Username.toLowerCase()
    let checkPatient = await patientModule.findOneByEmail(PT_Username)
    if (checkPatient) {
      return res.send({
        code: 0,
        status: 'failed',
        message: '*This email address is associated with another account',
        Error_Code: errorModule.username_already_associated()
      })
    } else {
      await chartModule.updatePatientEmail(req.user.PT_Username, PT_Username)
      await encounterModule.updatePatientEmail(req.user.PT_Username, PT_Username)
      await orderModule.updatePatientEmail(req.user.PT_Username, PT_Username)
      await patientHistoryModule.updatePatientEmail(req.user.PT_Username, PT_Username)
      await patientProblemModule.updatePatientEmail(req.user.PT_Username, PT_Username)
      await patientModule.updatePatientEmail(req.user.PT_Username, PT_Username)
      const tokenPayload = {
        PT_HS_MRN: req.user.PT_HS_MRN,
        PT_Username: PT_Username,
        roles: 'Patient'
      }
      const token = jwt.sign(tokenPayload, process.env.JWT_SECRET, { expiresIn: 60 * 60 * 24 })
      var auditData = {
        Audit_UID: await auditUid(),
        Audit_Type: auditType.change,
        Audit_Info: 'New- ' + await auditUid(),
        Data_Point: 'PT/Update email',
        PT_First_Name: req.user.PT_First_Name,
        PT_Last_Name: req.user.PT_Last_Name,
        PT_HSID: req.user.PT_HS_MRN,
        IP_Address: await auditModule.getIpAddress(req),
        New_Value: PT_Username,
        Old_Value: req.user.PT_Username
      }
      await auditModule.create(auditData)
      return res.send({
        code: 1,
        status: 'sucess',
        message: 'Patient email updated successfully',
        token: token
      })
    }
  } catch (err) {
    res.send({ code: 2, status: 'failed', message: err.message, Error_Code: 'HS187' })
  }
})

router.put('/update-password', authorize(Role.Patient), async (req, res) => {
  const { body: { PT_Password } } = req
  if (!PT_Password) return res.send({ code: 0, status: 'failed', message: '*What would you like a new password to be', Error_Code: 'HS188' })
  if (PT_Password == '') return res.send({ code: 0, status: 'failed', message: '*What would you like a new password to be', Error_Code: 'HS188' })
  var reg = /^(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{10,45}$/
  if (!reg.test(PT_Password)) {
    return res.send({ code: 0, status: 'failed', message: '*Please make sure the password meets all of the criteria below.', Error_Code: errorModule.make_sure_criteria() })
  } else {
    let hashPassword = await bcryptHash(PT_Password, 10)
    await patientModule.updatePatientById(req.user.PT_HS_MRN, { PT_Password: hashPassword })
    var auditData = {
      Audit_UID: await auditUid(),
      Audit_Type: auditType.change,
      Audit_Info: 'New- ' + await auditUid(),
      Data_Point: 'PT/Update password',
      PT_First_Name: req.user.PT_First_Name,
      PT_Last_Name: req.user.PT_Last_Name,
      PT_HSID: req.user.PT_HS_MRN,
      IP_Address: await auditModule.getIpAddress(req)
    }
    await auditModule.create(auditData)
    return res.send({ code: 1, status: 'success', message: 'Patient password updated successfully' })
  }
})

router.put('/update-vital-sign/:PT_HS_MRN', authorize([Role.Provider]), async (req, res) => {
  try {
    const { body: { data }, params: { PT_HS_MRN } } = req
    if (!PT_HS_MRN) return res.send({ code: 0, status: 'failed', message: 'PT_HS_MRN is required!', Error_Code: errorModule.PT_HS_MRN_required() })
    if (!data) return res.send({ code: 0, status: 'failed', message: 'data is required', Error_Code: errorModule.data_required() })
    if (!data.Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required', Error_Code: errorModule.Encounter_UID_required() })
    if (data.PT_Username) return res.send({ code: 0, status: 'failed', message: 'patient can not update email', Error_Code: 'HS189' })
    let patient = await patientModule.findById(PT_HS_MRN)
    let auditData = {
      Audit_UID: await auditUid(),
      Audit_Info: 'New- ' + await auditUid(),
      Audit_Type: auditType.view,
      Data_Point: req.userType == 'Patient' ? 'PT/update vital sign' : 'PRO/update vital sign',
      PT_First_Name: req.userType == 'Patient' ? req.user.PT_First_Name : null,
      PT_Last_Name: req.userType == 'Patient' ? req.user.PT_Last_Name : null,
      PT_HSID: req.userType == 'Patient' ? req.user.PT_HS_MRN : null,
      PRO_First_Name: req.userType == 'Provider' ? req.user.PRO_First_Name : null,
      PRO_Last_Name: req.userType == 'Provider' ? req.user.PRO_Last_Name : null,
      PRO_UID: req.userType == 'Provider' ? req.user.PRO_UID : null,
      IP_Address: await auditModule.getIpAddress(req)
    }
    await auditModule.create(auditData)
    if (!patient) {
      return res.send({
        code: 0,
        status: 'failed',
        message: `No patient found with ${PT_HS_MRN} in the system`,
        Error_Code: errorModule.patient_not_found()
      })
    } else {
      await patientModule.updatePatientById(PT_HS_MRN, data)
      let d = new Date()
      var newYork = moment.tz(d, process.env.CURRENT_TIMEZONE).format('MM-DD-YYYY HH:mm a')
      let chartData = {
        Vital_Signs_Last_Modified_Timestamp: req.headers.host.includes('localhost') ? moment(Date.parse(newYork)).add(330, 'm').toDate() : moment(Date.parse(newYork)).toDate()
      }
      let updatedAt = req.headers.host.includes('localhost') ? moment(Date.parse(newYork)).add(330, 'm').toDate() : moment(Date.parse(newYork)).toDate()
      await chartModule.updateChart(data.Encounter_UID, chartData, updatedAt)
      req.body.data.Vital_Signs_Last_Modified_Timestamp = newYork
      delete req.body.data.Updated_At
      return res.send({
        code: 1,
        status: 'sucess',
        message: 'Patient update successfully',
        data: req.body.data
      })
    }
  } catch (err) {
    res.send({ code: 2, status: 'failed', message: err.message, Error_Code: 'HS190' })
  }
})

async function updateUpcommingEncounterPatientHistory (id, data) {
  let patientAllEncounter = await encounterModule.getPatientsEncounterById(id)
  let history = await patientHistoryModule.findByUserId(id)
  let currentDate = new Date()
  let upcomingEncounter = patientAllEncounter.filter(ele => new Date(ele.Encounter_Date + ' ' + ele.Encounter_Time) >= currentDate)
  let encounter_ids = upcomingEncounter.map(ele => {
    return ele.Encounter_UID
  })
  if (upcomingEncounter.length == 0) {
    return true
  } else {
    await encounterModule.updateAllPatientComingEncounter(encounter_ids, history)
    return true
  }
}
module.exports = router
