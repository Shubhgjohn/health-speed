/* eslint-disable camelcase */
/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
var Router = require('express').Router
var router = new Router()
const auditModule = require('../../models').Audit
const errorModule = require('../../models').Errors
const stripeModule = require('../../models').Stripe
const encounterModule = require('../../models').Encounter
const providerModule = require('../../models').Provider
const transactionModule = require('../../models').Transactions
const patientModule = require('../../models').Patient
var { auditUid } = require('../../utils/UID')
// var auditType = require('../../auditType.json')
const logger = require('../../models/errorlog')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
var stripe = require('stripe')(process.env.SECRET_KEY)
const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_PRIVATE_KEY)

router.post('/add-payment-card', authorize(Role.Patient), async (req, res) => {
  if (!req.user.stripeCustomerId) return res.send({ code: 0, status: 'failed', message: 'this patient has no stripe account', Error_Code: errorModule.no_stripe_account() })
  try {
    var { body: { token } } = req
    if (!token) return res.send({ code: 0, status: 'failed', message: 'token is required!', Error_Code: 'HS176' })
    await createAudit(req, 'Add', 'add credit card')
    let cardDetails = await stripeModule.createCard(req.user.stripeCustomerId, token)
    if (req.body.isprimary) {
      await stripeModule.makeDefault(req.user.stripeCustomerId, cardDetails.id)
    }
    res.send({ code: 1, status: 'success', message: 'Card added', data: cardDetails })
  } catch (err) {
    return res.send({ code: 2, status: 'failed', message: err.message, Error_Code: 'HS177' })
  }
})

router.get('/get-all-card', authorize(Role.Patient), async (req, res) => {
  try {
    if (!req.user.stripeCustomerId) return res.send({ code: 0, status: 'failed', message: 'this patient has no stripe account', Error_Code: errorModule.no_stripe_account() })
    let customer = await stripe.customers.retrieve(req.user.stripeCustomerId)
    let customerDefaultSource = customer.default_source
    let cards = await stripeModule.getCardList(req.user.stripeCustomerId)
    cards.data.map(ele => {
      if (ele.id == customerDefaultSource) {
        ele.default = true
      } else {
        ele.default = false
      }
    })
    res.send({ code: 1, status: 'sucess', data: cards.data })
  } catch (err) {
    logger.error(err.message)
    logger.fatal(err)
    return res.send({ code: 0, status: 'failed', message: err.message, Error_Code: 'HS178' })
  }
})

router.delete('/delete-card', authorize(Role.Patient), async (req, res) => {
  try {
    if (!req.user.stripeCustomerId) return res.send({ code: 0, status: 'failed', message: 'this patient has no stripe account', Error_Code: errorModule.no_stripe_account() })
    let cardId = req.body.cardId
    if (!cardId) return res.send({ code: 0, status: 'failed', message: 'cardId is required!', Error_Code: errorModule.cardId_required() })
    await createAudit(req, 'Remove', 'remove credit card')
    await stripeModule.deleteCreditCard(req.user.stripeCustomerId, cardId)
    res.send({ code: 1, status: 'success', message: 'Card removed successfully' })
  } catch (err) {
    return res.send({ code: 2, status: 'failed', message: err.message, Error_Code: 'HS180' })
  }
})

router.put('/make-default-card', authorize(Role.Patient), async (req, res) => {
  try {
    if (!req.user.stripeCustomerId) return res.send({ code: 0, status: 'failed', message: 'this patient has no stripe account', Error_Code: errorModule.no_stripe_account() })
    let cardId = req.body.cardId
    if (!cardId) return res.send({ code: 0, status: 'failed', message: 'cardId is required!', Error_Code: errorModule.cardId_required() })
    await createAudit(req, 'Change', 'make default card')
    await stripeModule.makeDefault(req.user.stripeCustomerId, cardId)
    return res.send({ code: 1, status: 'success', message: 'Make card as default successfully' })
  } catch (err) {
    return res.send({ code: 2, status: 'failed', message: err.message, Error_Code: 'HS181' })
  }
})

router.put('/update-card', authorize(Role.Patient), async (req, res) => {
  try {
    if (!req.user.stripeCustomerId) return res.send({ code: 0, status: 'failed', message: 'this patient has no stripe account', Error_Code: errorModule.no_stripe_account() })
    let cardId = req.body.cardId
    if (!cardId) return res.send({ code: 0, status: 'failed', message: 'cardId is required!', Error_Code: errorModule.cardId_required() })
    let { body: { data } } = req
    if (!data) return res.send({ code: 0, status: 'failed', message: 'data is required!', Error_Code: errorModule.data_required() })
    await createAudit(req, 'Change', 'Update credit card')
    await stripeModule.updateCardDetail(req.user.stripeCustomerId, cardId, data)
    if (req.body.isprimary) {
      await stripeModule.makeDefault(req.user.stripeCustomerId, cardId)
    }
    return res.send({ code: 1, status: 'success', message: 'Card detail update successfully' })
  } catch (err) {
    return res.send({ code: 2, status: 'failed', message: err.message, Error_Code: 'HS182' })
  }
})

router.get('/create-account', async (req, res) => {
  stripe.accounts.create({
    type: 'custom',
    country: 'US',
    email: 'test20@tepia.co',
    requested_capabilities: [
      'transfers'
    ]
  }, (err, account) => {
    if (err) {
      console.log(err)
      res.send(err)
    } else {
      console.log('account successful' + account)
      res.send(account)
    }
  })
})

router.post('/retry-payment/:Encounter_UID', authorize(Role.Patient), async (req, res) => {
  try {
    let Encounter_UID = req.params.Encounter_UID
    let Payment_Card_Id = req.body.Payment_Card_Id
    if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID required!', Error_Code: errorModule.Encounter_UID_required() })
    if (!Payment_Card_Id) return res.send({ code: 0, status: 'failed', message: 'Payment_Card_Id required!', Error_Code: errorModule.Payment_Card_Id_required() })
    let encounter = await encounterModule.getEncounterByEncounter_UID(Encounter_UID)
    if (!encounter) return res.send({ code: 0, status: 'failed', message: `encounter with '${Encounter_UID}' not found!`, Error_Code: errorModule.Encounter_not_found() })
    let provider = await providerModule.findById(encounter.PRO_UID)
    if (encounter.Transaction_Status == 'Failed') {
      let providerAccount = provider.StripeAccountId
      let card = null
      try {
        await stripeModule.makeDefault(req.user.stripeCustomerId, Payment_Card_Id)
        card = await stripeModule.getCardByCardId(req.user.stripeCustomerId, Payment_Card_Id)
      } catch (error) {
        if (error) card = null
      }
      if (card == null) {
        let data = {
          Encounter_UID: Encounter_UID,
          Payment_Type: 'Retry Payment',
          Payment_Date: new Date(),
          Amount: encounter.Encounter_Cost,
          Charge_Id: null,
          Transaction_Status: 'Failed',
          Payment_By: req.user.PT_HS_MRN,
          PRO_UID: encounter.PRO_UID
        }
        await transactionModule.create(data)
        await encounterModule.updateEncounter(Encounter_UID, { Transaction_Status: 'Failed' })
        if (req.user.PT_Email_Notification === 'Enabled') {
          let PT_Msg = await patientMail(req, encounter)
          sgMail.send(PT_Msg)
        }
        let msg = await adminMail(encounter, req)
        sgMail.send(msg)
        return res.send({ code: 0, status: 'failed', message: 'Transaction failed!', Error_Code: 'HS232' })
      }
      stripe.charges.create(
        {
          amount: encounter.Encounter_Cost * 100,
          currency: 'usd',
          customer: req.user.stripeCustomerId,
          transfer_data: {
            amount: ((encounter.Encounter_Cost * 90) / 100) * 100,
            destination: providerAccount
          }
        },
        async function (err, charge) {
          if (err) {
            let data = {
              Encounter_UID: Encounter_UID,
              Payment_Type: 'Retry Payment',
              Payment_Date: new Date(),
              Amount: encounter.Encounter_Cost,
              Charge_Id: null,
              Transaction_Status: 'Failed',
              Payment_By: req.user.PT_HS_MRN,
              PRO_UID: encounter.PRO_UID
            }
            await transactionModule.create(data)
            await encounterModule.updateEncounter(Encounter_UID, { Transaction_Status: 'Failed' })
            if (req.user.PT_Email_Notification === 'Enabled') {
              let PT_Msg = await patientMail(req, encounter)
              sgMail.send(PT_Msg)
            }
            let msg = await adminMail(encounter, req)
            sgMail.send(msg)
            return res.send({ code: 0, status: 'failed', message: 'Transaction failed!', Error_Code: 'HS232' })
          }
          let data = {
            Encounter_UID: Encounter_UID,
            Payment_Type: 'Retry Payment',
            Payment_Date: new Date(),
            Amount: encounter.Encounter_Cost,
            receipt_url: charge.receipt_url,
            Transaction_Id: charge.balance_transaction,
            Charge_Id: charge.id,
            Transaction_Status: 'Paid',
            Payment_By: req.user.PT_HS_MRN,
            PRO_UID: encounter.PRO_UID
          }
          await transactionModule.create(data)
          let providerEarned = provider.PRO_Earnings_to_Date + ((encounter.Encounter_Cost * 90) / 100) * 100
          let totalAmmount = req.user.PT_Total_Amount_Paid + encounter.Encounter_Cost
          await providerModule.updateById(provider.PRO_UID, { PRO_Earnings_to_Date: providerEarned })
          await patientModule.updatePatientByUsername(req.user.PT_Username, { PT_Total_Amount_Paid: totalAmmount })
          await encounterModule.updateEncounter(Encounter_UID, { Encounter_Transaction_Status: 'Paid', Transaction_Status: 'Paid' })
          await createAudit(req, 'Confirm', 'retry payment')
          res.send({ code: 1, status: 'success', message: 'Payment processed successfully', data: charge })
        }
      )
    } else {
      return res.send({ code: 0, status: 'failed', message: 'Invalid encounter for retry-payment.', Error_Code: 'HS184' })
    }
  } catch (err) {
    return res.send({ code: 2, status: 'failed', message: err.message, Error_Code: 'HS265' })
  }
})

async function createAudit (req, type, data_point) {
  let auditData = {
    Audit_UID: await auditUid(),
    Audit_Info: 'New- ' + await auditUid(),
    Audit_Type: type,
    Data_Point: `PT/${data_point}`,
    PT_First_Name: req.user.PT_First_Name,
    PT_Last_Name: req.user.PT_Last_Name,
    PT_HSID: req.user.PT_HS_MRN,
    IP_Address: await auditModule.getIpAddress(req)
  }
  await auditModule.create(auditData)
}

function adminMail (encounter, req) {
  let message = {
    from: process.env.ADMIN_FROM_EMAIL,
    to: process.env.SENDGRID_TO_EMAIL,
    template_id: process.env.Notify_Admin_On_Payment_Failure,
    dynamic_template_data: {
      Encounter_Date: encounter.Encounter_Date,
      Encounter_Time: encounter.Encounter_Time,
      PT_First_Name: req.user.PT_First_Name,
      PT_Last_Name: req.user.PT_Last_Name,
      PT_Username: req.user.PT_Username,
      PT_Cell_Phone: req.user.PT_Cell_Phone
    }
  }
  return message
}

function patientMail (req, encounter) {
  let message = {
    from: process.env.ADMIN_FROM_EMAIL,
    to: req.user.PT_Username,
    template_id: process.env.Notify_Patient_On_Payment_Failure,
    dynamic_template_data: {
      url: `${process.env.API_HOST}/patient/main/payment/${encounter.Encounter_UID}/retry`
    }
  }
  return message
}

module.exports = router
