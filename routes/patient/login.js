/* eslint-disable quotes */
/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const jwt = require('jsonwebtoken')
const patientModule = require('../../models').Patient
const patientHXModule = require('../../models').PatientHistory
const errorModule = require('../../models').Errors
const auditModule = require('../../models').Audit
var { auditUid } = require('../../utils/UID')
var auditType = require('../../auditType.json')
const { bcryptVerify } = require('../../utils/crypto')
const moment = require('moment')
// Service for provider login //
router.post('/login', async (req, res) => {
	var { body: { PT_Username, PT_Password } } = req
	if (!PT_Username) return res.send({ code: 0, status: 'failed', message: `*Don't forget to enter your email! It’s required to login.`, Error_Code: errorModule.enter_your_email() })
	if (!PT_Password) return res.send({ code: 0, status: 'failed', message: `*Don't forget to enter your password! It’s required to login.`, Error_Code: errorModule.enter_your_password() })
	PT_Username = PT_Username.toLowerCase()
	try {
		let patient = await patientModule.findOneByEmail(PT_Username)
		if (!patient) {
			return res.send({
				code: 0,
				status: 'failed',
				message: `*Hum… We didn't find a record for this email address. Please check it and try again`,
				Error_Code: errorModule.did_not_find_record(),
				PT_Account_Status: null,
				key: 'Email'
			})
		} else {
			if (patient.PT_Account_Status == 'Banned') return res.send({ code: 0, status: 'failed', message: 'Please contact Healthspeed.', Error_Code: errorModule.account_banned(), PT_Account_Status: patient.PT_Account_Status })
			if (!patient.PT_Password) {
				return res.send({
					code: 0,
					status: 'failed',
					message: `*Your entries don't match our records. Please check them and try again.`,
					Error_Code: errorModule.entries_do_not_match(),
					PT_Account_Status: null,
					key: 'Password'
				})
			}
			var isPasswordTrue = bcryptVerify(PT_Password, patient.PT_Password)
			if (isPasswordTrue) {
				let patientHistory = await patientHXModule.findByUserId(patient.PT_HS_MRN)
				if (!patientHistory) {
					let data = {
						PT_HS_MRN: patient.PT_HS_MRN
					}
					await patientHXModule.create(data)
				}
				const tokenPayload = {
					PT_HS_MRN: patient.PT_HS_MRN,
					PT_Username: patient.PT_Username,
					roles: 'Patient'
				}
				let PT_Profile_Status
				if (patient.PT_First_Name != null && patient.PT_First_Name != '' && patient.PT_Last_Name != null && patient.PT_Last_Name != '' && patient.PT_Gender != null && patient.PT_Gender != '' && patient.PT_DOB != null && patient.PT_DOB != '' && patient.PT_Home_Zip != null && patient.PT_Home_Zip != '' && patient.PT_Home_State != null && patient.PT_Home_State != '' && patient.PT_Home_City != null && patient.PT_Home_City != '' && patient.PT_Home_Address_Line_1 != null && patient.PT_Home_Address_Line_1 != '' && patient.PT_Cell_Phone != null && patient.PT_Cell_Phone != '') {
					PT_Profile_Status = 'Completed'
				} else {
					PT_Profile_Status = 'Incompleted'
				}
				const token = jwt.sign(tokenPayload, process.env.JWT_SECRET, { expiresIn: 60 * 60 * 24 })
				await patientModule.updateLastLogin(patient.PT_Username)
				var ipadd = await auditModule.getIpAddress(req)
				const UID = await auditUid()
				const auditData = {
					Audit_UID: UID,
					Audit_Type: auditType.login,
					Data_Point: 'PT/Login',
					Audit_Info: 'New- ' + UID,
					PT_First_Name: patient.PT_First_Name,
					PT_Last_Name: patient.PT_Last_Name,
					PT_HSID: patient.PT_HS_MRN,
					IP_Address: ipadd
				}
				await auditModule.create(auditData)
				return res.status(200).send({
					code: 1,
					status: 'sucess',
					data: token,
					date: moment(new Date()).format('MM-DD-YYYY'),
					PT_Profile_Status: PT_Profile_Status,
					PT_Account_Status: patient.PT_Account_Status
				})
			} else {
				return res.send({
					code: 0,
					status: 'failed',
					message: `*Your entries don't match our records. Please check them and try again.`,
					Error_Code: errorModule.entries_do_not_match(),
					PT_Account_Status: null,
					key: 'Password'
				})
			}
		}
	} catch (err) {
		console.log(err)
		return res.send({ code: 2, status: 'failed', message: err.message, Error_Code: 'HS156' })
	}
})

module.exports = router
