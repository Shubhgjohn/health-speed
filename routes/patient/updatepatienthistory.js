/* eslint-disable prefer-const */
const Router = require('express').Router
const router = new Router()
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
var auditModule = require('../../models').Audit
const patientHistoryModule = require('../../models').PatientHistory
var { auditUid } = require('../../utils/UID')
var auditType = require('../../auditType.json')

// Update patient Hystory //
router.put('/updatehistory', authorize([Role.Patient]), async (req, res) => {
  const { body: { data } } = req
  if (!data) { return res.send({ code: 0, status: 'failed', message: 'patient history data is required' }) }
  let patient = await patientHistoryModule.findByUsername(req.user.PT_Username)
  if (patient) {
    await patientHistoryModule.updateByUsername(patient.PT_HS_MRN, data)
    const auditData = {
      Audit_UID: await auditUid(),
      Audit_Type: auditType.change,
      Data_Point: 'PT/Update Patient History',
      PT_First_Name: req.user.PT_First_Name,
      PT_Last_Name: req.user.PT_Last_Name,
      PT_HSID: req.user.PT_HS_MRN,
      Audit_Info: 'New- ' + await auditUid(),
      Old_Value: patient,
      New_Value: data,
      IP_Address: await auditModule.getIpAddress(req)
    }
    await auditModule.create(auditData)
    return res.send({ code: 1, status: 'sucess', message: 'patient history update successfully' })
  } else {
    data.PT_Username = req.user.PT_Username
    patientHistoryModule.create(data)
    const UID = await auditUid()
    const auditData = {
      Audit_UID: UID,
      Audit_Type: auditType.add,
      Data_Point: 'PT/Patient History Added',
      PT_First_Name: req.user.PT_First_Name,
      PT_Last_Name: req.user.PT_Last_Name,
      PT_HSID: req.user.PT_HS_MRN,
      Audit_Info: 'new- ' + UID,
      New_Value: data,
      IP_Address: await auditModule.getIpAddress(req)
    }
    await auditModule.create(auditData)
    return res.send({ code: 1, status: 'sucess', message: 'patient history update successfully' })
  }
})

module.exports = router
