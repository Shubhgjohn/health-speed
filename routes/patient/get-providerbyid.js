/* eslint-disable no-unneeded-ternary */
/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
/* eslint-disable prefer-const */
const Router = require('express')
const router = new Router()
const model = require('../../models')
const encounterModule = require('../../models').Encounter
const chartModule = require('../../models').Chart
const orderModule = require('../../models').Order
const otherModule = require('../../models').Other
const errorModule = require('../../models').Errors
const availabilityModule = require('../../models').Availability
const serviceModule = require('../../models').CommonServices
// const auditModule = require('../../models').Audit
const stripeModule = require('../../models').Stripe
// const { auditUid } = require('../../utils/UID')
// const auditType = require('../../auditType.json')
const authoreze = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const moment = require('moment')
const momentTz = require('moment-timezone')
const { Model } = require('mongoose')

router.get('/get-provider/:Encounter_UID/:key?', authoreze(Role.Patient), async (req, res) => {
  try {
    const Encounter_UID = req.params.Encounter_UID
    const encounter = await encounterModule.findEncounter(Encounter_UID)
    if (encounter.length == 0) return res.send({ code: 0, status: 'failed', message: `encounter with id '${Encounter_UID}' is not found!`, Error_Code: errorModule.Encounter_not_found() })
    if (!req.user.stripeCustomerId) return res.send({ code: 0, status: 'failed', message: 'this patient has no stripe account', Error_Code: errorModule.no_stripe_account() })
    let followupEncounter = await encounterModule.getFollowupEncounterByEncounter_UID(encounter[0].Encounter_UID)
    let myDate = new Date().setTime(new Date().getTime() + (24 * 60 * 60 * 1000) * 90) // date of next 90 days
    let card = null
    try {
      if (encounter[0].Payment_Card_Id) {
        card = await stripeModule.getCardByCardId(req.user.stripeCustomerId, encounter[0].Payment_Card_Id)
      }
    } catch (error) {
      if (error) card = null
    }
    var availability = await getAllAvailability(myDate, encounter[0].provider.PRO_UID)
    var travelTimeSlot = encounter[0].provider.PRO_Home_Visit_Buffer / 15
    var distanceMiles = distance(encounter[0].patient.PT_Home_Address_Latitude, encounter[0].patient.PT_Home_Address_Longitude, encounter[0].provider.PRO_Office_Address_Latitude, encounter[0].provider.PRO_Office_Address_Longitude)
    let day = moment(new Date(encounter[0].Encounter_Date)).format('ddd')
    if (day == 'Thu') day = day + 'r'
    if (day == 'Tue') day = day + 's'
    let netDate = encounter[0].provider.Pro_Office_Hours_Time.find(ele => ele.day == day)
    let fromTime = netDate.fromTime.time.includes(':') ? netDate.fromTime.time : netDate.fromTime.time + ':00'
    let toTime = netDate.toTime.time.includes(':') ? netDate.toTime.time : netDate.toTime.time + ':00'
    let Pro_Office_Hours_Time = ((new Date(`${encounter[0].Encounter_Date} ${fromTime} ${netDate.fromTime.zone}`) <= new Date(`${encounter[0].Encounter_Date} ${encounter[0].Encounter_Time}`)) && (new Date(`${encounter[0].Encounter_Date} ${encounter[0].Encounter_Time}`) <= new Date(`${encounter[0].Encounter_Date} ${toTime} ${netDate.toTime.zone}`))) ? true : false
    var temp = 0
    var newArr = []
    var finalList = []
    for (var i = 0; i < availability.length; i++) {
      var slotsIndex = availability[i]
      if (newArr.length == 0) {
        await getNextAvailableDay(slotsIndex, distanceMiles, encounter[0].provider, travelTimeSlot, newArr, finalList, temp)
      } else { break }
    }
    if (finalList.length == 0) finalList.push({ PRO_Next_Available_for_Home_Visit: false, PRO_Next_Available_for_Office_Visit: false })
    // return res.send(finalList)
    var provider_info = {
      PRO_Profile_Picture: encounter[0].provider.PRO_Profile_Picture ? await serviceModule.getImage(encounter[0].provider.PRO_Profile_Picture) : null,
      PT_Profile_Picture: encounter[0].patient.PT_Profile_Picture ? await serviceModule.getImage(encounter[0].patient.PT_Profile_Picture) : null,
      PT_First_Name: encounter[0].patient.PT_First_Name ? encounter[0].patient.PT_First_Name : '',
      PT_Last_Name: encounter[0].patient.PT_Last_Name ? encounter[0].patient.PT_Last_Name : '',
      PRO_First_Name: encounter[0].provider.PRO_First_Name,
      PRO_Last_Name: encounter[0].provider.PRO_Last_Name,
      PRO_Primary_Specialty: encounter[0].provider.PRO_Speciality.length > 0 ? encounter[0].provider.PRO_Speciality.find(ele => ele.PRO_Primary_Speciality == true) ? encounter[0].provider.PRO_Speciality.find(ele => ele.PRO_Primary_Speciality == true).PRO_Speciality : null : null,
      PRO_Office_Address_Line_1: encounter[0].provider.PRO_Office_Address_Line_1 ? encounter[0].provider.PRO_Office_Address_Line_1 : null,
      PRO_Office_Address_Line_2: encounter[0].provider.PRO_Office_Address_Line_2 ? encounter[0].provider.PRO_Office_Address_Line_2 : null,
      PRO_Office_Address_Latitude: encounter[0].provider.PRO_Office_Address_Latitude ? encounter[0].provider.PRO_Office_Address_Latitude : null,
      PRO_Office_Address_Longitude: encounter[0].provider.PRO_Office_Address_Longitude ? encounter[0].provider.PRO_Office_Address_Longitude : null,
      PRO_Office_City: encounter[0].provider.PRO_Office_City ? encounter[0].provider.PRO_Office_City : null,
      PRO_Office_State: encounter[0].provider.PRO_Office_State ? encounter[0].provider.PRO_Office_State : null,
      PRO_Office_Zip: encounter[0].provider.PRO_Office_Zip ? encounter[0].provider.PRO_Office_Zip : null,
      PRO_Office_Phone: encounter[0].provider.PRO_Office_Phone,
      PRO_UID: encounter[0].provider.PRO_UID,
      PRO_Bio: encounter[0].provider.PRO_Bio,
      PRO_Board_Cert: encounter[0].provider.PRO_Board_Cert,
      PRO_Educations: encounter[0].provider.PRO_Educations,
      PRO_Affliation: encounter[0].provider.PRO_Affliation,
      PRO_Experiences: encounter[0].provider.PRO_Experiences,
      PRO_Actions: encounter[0].provider.PRO_Actions,
      PRO_Speciality: encounter[0].provider.PRO_Speciality.length > 0 ? encounter[0].provider.PRO_Speciality : [],
      Encounter_Date: encounter[0].Encounter_Date,
      Encounter_Time: encounter[0].Encounter_Time,
      Encounter_Status: encounter[0].Encounter_Status,
      Encounter_Has_Followup: encounter[0].Encounter_Has_Followup,
      Encounter_Followup_Limit: encounter[0].Encounter_Followup_Limit,
      Encounter_Chief_Complaint_PT: encounter[0].Encounter_Chief_Complaint_PT,
      Reschedule_Request_Date: encounter[0].Reschedule_Request_Date,
      Reschedule_Request_Time: encounter[0].Reschedule_Request_Time,
      PT_Credit_Card_Short_Number: card ? card.last4 : null,
      PT_Credit_Card_Type: card ? card.brand : null,
      Pro_Office_Hours_Time: Pro_Office_Hours_Time,
      PRO_Account_Status: encounter[0].provider.PRO_Account_Status,
      PT_Account_Status: encounter[0].patient.PT_Account_Status,
      next_availability: (finalList[0].PRO_Next_Available_for_Home_Visit == true || finalList[0].PRO_Next_Available_for_Office_Visit == true) ? true : false
    }
    let encounters = await encounterModule.getPatientEncounter(req.user.PT_Username)
    if (encounters.length > 0) {
      var pastVisit = encounters.filter(ele => Date.parse(moment(new Date(`${ele.Encounter_Date} ${ele.Encounter_Time}`)).format('YYYY-MM-DDTHH:mm:ss')) < Date.parse(momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm:ss')))
    }
    let shareChart = []
    if (pastVisit.length > 0) {
      pastVisit.map(ele => {
        if (ele.Encounter_Access_Provider.length == 0) {
          shareChart.push(ele)
        } else {
          if (!ele.Encounter_Access_Provider.find(item => (item.PRO_UID == encounter[0].PRO_UID && item.Access_Status == 'Granted'))) shareChart.push(ele)
        }
      })
    }
    const get_ready = {
      PT_HS_MRN: encounter[0].patient.PT_HS_MRN,
      PRO_Last_Name: encounter[0].provider.PRO_Last_Name,
      Get_Ready_Info_Status_About: encounter[0].Get_Ready_Info_Status_About ? encounter[0].Get_Ready_Info_Status_About : 'Active',
      Get_Ready_Info_Status_General: encounter[0].Get_Ready_Info_Status_General ? encounter[0].Get_Ready_Info_Status_General : 'Active',
      Get_Ready_Info_Status_Medications: encounter[0].Get_Ready_Info_Status_Medications ? encounter[0].Get_Ready_Info_Status_Medications : 'Active',
      Get_Ready_Info_Status_Other: encounter[0].Get_Ready_Info_Status_Other ? encounter[0].Get_Ready_Info_Status_Other : 'Active',
      Get_Ready_Info_Status_OBGYN: encounter[0].Get_Ready_Info_Status_OBGYN ? encounter[0].Get_Ready_Info_Status_OBGYN : 'Active',
      PT_HX_Last_Updated_About_Section: encounter[0].patient.PT_HX_Last_Updated_About_Section ? (moment(new Date(encounter[0].patient.PT_HX_Last_Updated_About_Section)).format('MMM D, YYYY [at] h:mma')) : null,
      PT_HX_Last_Updated_General_Section: encounter[0].patient.PT_HX_Last_Updated_General_Section ? (moment(new Date(encounter[0].patient.PT_HX_Last_Updated_General_Section)).format('MMM D, YYYY [at] h:mma')) : null,
      PT_HX_Last_Updated_Medications_Section: encounter[0].patient.PT_HX_Last_Updated_Medications_Section ? (moment(new Date(encounter[0].patient.PT_HX_Last_Updated_Medications_Section)).format('MMM D, YYYY [at] h:mma')) : null,
      PT_HX_Last_Updated_Other_Section: encounter[0].patient.PT_HX_Last_Updated_Other_Section ? (moment(new Date(encounter[0].patient.PT_HX_Last_Updated_Other_Section)).format('MMM D, YYYY [at] h:mma')) : null,
      PT_HX_Last_Updated_OB_Section: encounter[0].patient.PT_HX_Last_Updated_OB_Section ? (moment(new Date(encounter[0].patient.PT_HX_Last_Updated_OB_Section)).format('MMM D, YYYY [at] h:mma')) : null,
      PT_Share_Records: encounter[0].patient.PT_Share_Records ? encounter[0].patient.PT_Share_Records : 'No',
      Share_All_Chart: shareChart.length > 0 ? false : true
    }
    let transactionDate = await model.Transactions.getTransactionDate(encounter[0].Encounter_UID)
    const visit_info = {
      Encounter_UID: encounter[0].Encounter_UID,
      Encounter_Date: encounter[0].Encounter_Date,
      Encounter_Time: encounter[0].Encounter_Time,
      Encounter_Type: encounter[0].Encounter_Type,
      PT_Account_Status: encounter[0].patient.PT_Account_Status,
      Encounter_Status: encounter[0].Encounter_Status,
      Encounter_Followup_Status: encounter[0].Encounter_Followup_Status,
      PRO_Account_Status: encounter[0].provider.PRO_Account_Status,
      Encounter_Location_Type: encounter[0].Encounter_Location_Type,
      PRO_Last_Name: encounter[0].provider.PRO_Last_Name,
      PRO_Office_Address_Line_1: encounter[0].provider.PRO_Office_Address_Line_1 ? encounter[0].provider.PRO_Office_Address_Line_1 : null,
      PRO_Office_Address_Line_2: encounter[0].provider.PRO_Office_Address_Line_2 ? encounter[0].provider.PRO_Office_Address_Line_2 : null,
      PRO_Office_Address_Latitude: encounter[0].provider.PRO_Office_Address_Latitude ? encounter[0].provider.PRO_Office_Address_Latitude : null,
      PRO_Office_Address_Longitude: encounter[0].provider.PRO_Office_Address_Longitude ? encounter[0].provider.PRO_Office_Address_Longitude : null,
      PRO_Office_City: encounter[0].provider.PRO_Office_City ? encounter[0].provider.PRO_Office_City : null,
      PRO_Office_State: encounter[0].provider.PRO_Office_State ? encounter[0].provider.PRO_Office_State : null,
      PRO_Office_Zip: encounter[0].provider.PRO_Office_Zip ? encounter[0].provider.PRO_Office_Zip : null,
      PT_Home_Address_Line_1: encounter[0].patient.PT_Home_Address_Line_1 ? encounter[0].patient.PT_Home_Address_Line_1 : null,
      PT_Home_Address_Line_2: encounter[0].patient.PT_Home_Address_Line_2 ? encounter[0].patient.PT_Home_Address_Line_2 : null,
      PT_Home_Address_Latitude: encounter[0].patient.PT_Home_Address_Latitude ? encounter[0].patient.PT_Home_Address_Latitude : null,
      PT_Home_Address_Longitude: encounter[0].patient.PT_Home_Address_Longitude ? encounter[0].patient.PT_Home_Address_Longitude : null,
      PT_Home_City: encounter[0].patient.PT_Home_City ? encounter[0].patient.PT_Home_City : null,
      PT_Home_State: encounter[0].patient.PT_Home_State ? encounter[0].patient.PT_Home_State : null,
      PT_Home_Zip: encounter[0].patient.PT_Home_Zip ? encounter[0].patient.PT_Home_Zip : null,
      Encounter_Transaction_Status: encounter[0].Encounter_Transaction_Status,
      Transaction_Date : transactionDate ? moment(transactionDate.Payment_Date).format('M/D/YY') : null,
      Encounter_Cost: encounter[0].Encounter_Cost,
      next_availability: (finalList[0].PRO_Next_Available_for_Home_Visit == true || finalList[0].PRO_Next_Available_for_Office_Visit == true) ? true : false
    }
    let chart = {}
    let chartData = await chartModule.findEncounterChartById(encounter[0].Encounter_UID, encounter[0].provider.PRO_UID, encounter[0].patient.PT_HS_MRN)
    chartData = JSON.parse(JSON.stringify(chartData))
    // return res.send(chartData)
    if (chartData && chartData.Discharge_Instructions_Status == 'Delivered') {
      chart.PRO_Diagnosis = chartData.PRO_Diagnosis
      chart.PRO_Diagnosis_ICD_Code = chartData.PRO_Diagnosis_ICD_Code
      chart.PRO_Patient_Instructions = chartData.PRO_Patient_Instructions
      let allEncounterOrder = await orderModule.findEncounterOrdersById(encounter[0].Encounter_UID, encounter[0].provider.PRO_UID, encounter[0].patient.PT_HS_MRN)
      let orders = allEncounterOrder.map(ele => ({
        Order_UID: ele.Order_UID ? ele.Order_UID : null,
        Order_Complete: ele.Order_Complete ? ele.Order_Complete : null,
        Order_Incomplete: ele.Order_Incomplete ? ele.Order_Incomplete : null,
        Order_Text: ele.Order_Text ? ele.Order_Text : null,
        Reminder_Start_Date: ele.Reminder_Start_Date ? moment(new Date(ele.Reminder_Start_Date)).format('MM-DD-YYYY') : null,
        Reminder_Date: ele.Reminder_Date ? moment(new Date(ele.Reminder_Date)).format('MM-DD-YYYY') : null,
        Order_Reminder: ele.Order_Reminder ? ele.Order_Reminder : null,
        Order_Type: ele.Order_Type ? ele.Order_Type : null,
        Order_Created_Date: ele.Order_Created_Date ? moment(new Date(ele.Order_Created_Date)).format('MM/DD/YYYY') : null,
        Order_Status: ele.Order_Status ? ele.Order_Status : null,
        Order_Stat: ele.Order_Stat ? ele.Order_Stat : 'Unchecked',
        Order_ICD10_Code: ele.Order_ICD10_Code ? ele.Order_ICD10_Code : null,
        Order_Review_Status: ele.Order_Review_Status ? ele.Order_Review_Status : 'Review'
      }))
      chart.orders = orders
      let card = null
      try {
        if (encounter[0].Payment_Card_Id) {
          card = await stripeModule.getCardByCardId(req.user.stripeCustomerId, encounter[0].Payment_Card_Id)
        }
      } catch (error) {
        if (error) card = null
      }
      // return res.send(cards)
      chart.follow_up = {
        Encounter_Followup_Status: followupEncounter ? encounter[0].Encounter_Followup_Status : null,
        Encounter_Location_Type: followupEncounter ? followupEncounter.Encounter_Location_Type : null,
        Encounter_Date: followupEncounter ? followupEncounter.Encounter_Date : null,
        Encounter_Time: followupEncounter ? followupEncounter.Encounter_Time : null,
        PT_Account_Status: encounter[0].patient.PT_Account_Status ? encounter[0].patient.PT_Account_Status : null,
        Encounter_Cost: followupEncounter ? followupEncounter.Encounter_Cost : null,
        PT_Credit_Card_Type: card ? card.brand : null,
        PT_Credit_Card_Short_Number: card ? card.last4 : null,
        Encounter_UID: followupEncounter ? followupEncounter.Encounter_UID : null,
        PRO_UID: followupEncounter ? followupEncounter.PRO_UID : null
      }
      let arr = []
      chartData.PRO_Addendums.map(ele => {
        arr.push({
          Addendum_Is_Signed: ele.Addendum_Is_Signed,
          PRO_Addendum_Completed_Timestamp: moment(ele.Updated_At).format('MM/DD/YYYY'),
          PRO_Addendum_Note: ele.PRO_Addendum_Note
        })
      })
      chart.Addendum = arr
    } else {
      chart = null
    }


    if (req.params.key == 'Discharge_Instruction') {
      await chartModule.viewDischargeInstruction(Encounter_UID, req.user.PT_HS_MRN)
    }
    if (req.params.key == 'Chart_Data') {
      await chartModule.viewChartData(Encounter_UID, req.user.PT_HS_MRN)
    }
    let settingData = await otherModule.find()
    res.send({
      code: 1,
      status: 'success',
      provider_info: provider_info,
      get_ready: get_ready,
      visit_info: visit_info,
      discharge_instruction: chart,
      AP_Phone_Number: settingData[0].HS_Support_Number,
      HS_Support_Email: settingData[0].HS_Support_Email
    })
  } catch (err) {
    res.send({ code: 2, status: 'failed', message: err.message, Error_Code: 'HS151' })
  }
})

async function getAllAvailability (date, PRO_UID) {
  var dateObj = new Date()
  dateObj.setDate(dateObj.getDate() - 1)
  var allAvailability = await availabilityModule.getProviderAvailabilityById(PRO_UID, dateObj, new Date(date))
  return allAvailability
}

function distance (lat1, lon1, lat2, lon2) {
  if ((lat1 == lat2) && (lon1 == lon2)) {
    return 0
  } else {
    var radlat1 = Math.PI * lat1 / 180
    var radlat2 = Math.PI * lat2 / 180
    var theta = lon1 - lon2
    var radtheta = Math.PI * theta / 180
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta)
    if (dist > 1) {
      dist = 1
    }
    dist = Math.acos(dist)
    dist = dist * 180 / Math.PI
    dist = dist * 60 * 1.1515
    return dist
  }
}

function getNextAvailableDay (slotsIndex, distanceMiles, ele, travelTimeSlot, newArr, finalList, temp) {
  for (var j = 0; j < slotsIndex.PRO_Slots.length; j++) {
    temp = 0
    var currentTime = moment(new Date()).format('HH:mm').split(':')
    currentTime = parseFloat(parseInt(currentTime[0]) + '.' + (parseInt(currentTime[1]) + 20))
    var slotTime = slotsIndex.PRO_Slots[j].Slot_Format
    if (slotsIndex.PRO_Availability_Date == moment(new Date()).format('MM-DD-YYYY')) {
      if (slotTime > currentTime) {
        let checkStatus = getNext_Available_Appointment(distanceMiles, ele, travelTimeSlot, newArr, finalList, slotsIndex, j, temp)
        if (!checkStatus) break
      }
    } else {
      let checkStatus = getNext_Available_Appointment(distanceMiles, ele, travelTimeSlot, newArr, finalList, slotsIndex, j, temp)
      if (!checkStatus) break
    }
  }
  return true
}

function getNext_Available_Appointment (distanceMiles, ele, travelTimeSlot, newArr, finalList, slotsIndex, j, temp) {
  if (slotsIndex.PRO_Availability_Type == 'Home') {
    if (distanceMiles <= ele.PRO_Miles_to_Travel) {
      let index = j
      for (let k = 1; k <= travelTimeSlot + 2; k++) {
        if (index <= 47 - travelTimeSlot) {
          if (slotsIndex.PRO_Slots[index].Is_Home_Available == true && slotsIndex.PRO_Slots[index].Is_Office_Available == true) temp++
        } else break
        index = index + 1
      }
      if (temp == travelTimeSlot + 2) {
        newArr.push(ele)
        finalList.push({ PRO_Next_Available_for_Home_Visit: true, PRO_Next_Available_for_Office_Visit: true })
        return false
      }
    }
    return true
  } else {
    if (distanceMiles <= ele.PRO_Miles_to_Travel) {
      let index = j
      for (let k = 1; k <= travelTimeSlot + 2; k++) {
        if (index <= 47 - travelTimeSlot) {
          if (slotsIndex.PRO_Slots[index].Is_Home_Available == true && slotsIndex.PRO_Slots[index].Is_Office_Available == true) temp++
        } else break
        index = index + 1
      }
      if (temp == travelTimeSlot + 2) {
        newArr.push(ele)
        finalList.push({ PRO_Next_Available_for_Home_Visit: true, PRO_Next_Available_for_Office_Visit: true })
        return false
      } else {
        if (j <= 46) {
          var c1 = slotsIndex.PRO_Slots[j].Is_Home_Available == true && slotsIndex.PRO_Slots[j].Is_Office_Available == true
          var c2 = slotsIndex.PRO_Slots[j + 1].Is_Home_Available == true && slotsIndex.PRO_Slots[j + 1].Is_Office_Available == true
        }
        if (c1 && c2) {
          newArr.push(ele)
          finalList.push({ PRO_Next_Available_for_Home_Visit: false, PRO_Next_Available_for_Office_Visit: true })
          return false
        }
      }
      return true
    } else {
      let c1 = slotsIndex.PRO_Slots[j].Is_Home_Available == true && slotsIndex.PRO_Slots[j].Is_Office_Available == true
      let c2 = j != slotsIndex.PRO_Slots.length - 1 ? slotsIndex.PRO_Slots[j + 1].Is_Home_Available == true && slotsIndex.PRO_Slots[j + 1].Is_Office_Available == true : false
      if (c1 && c2) {
        newArr.push(ele)
        finalList.push({ PRO_Next_Available_for_Home_Visit: false, PRO_Next_Available_for_Office_Visit: true })
        return false
      }
    }
    return true
  }
}
module.exports = router
