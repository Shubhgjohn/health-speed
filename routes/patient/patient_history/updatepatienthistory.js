/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const model = require('../../../models')
const errorModule = require('../../../models').Errors
const serviceModule = require('../../../models').CommonServices
const authorize = require('../../../_helpers/authorize')
var { auditUid } = require('../../../utils/UID')
var auditType = require('../../../auditType.json')
const Role = require('../../../_helpers/role')
var ObjectId = require('mongodb').ObjectID
const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_PRIVATE_KEY)
const logger = require('../../../models/errorlog')
var aws = require('aws-sdk')
aws.config.update({
  secretAccessKey: process.env.AWS_SECRET_KEY,
  accessKeyId: process.env.AWS_ACCESS_KEY,
  region: 'us-east-1'
})

var s3 = new aws.S3()

router.put('/update-history', authorize(Role.Provider), async (req, res) => {
  var { body: { Encounter_UID } } = req
  // if (!PT_Username) return res.send({ code: 0, status: 'failed', message: 'PT_Username is required', Error_Code: errorModule.PT_Username_required() })
  if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required', Error_Code: errorModule.Encounter_UID_required() })
  try {
    let encounter = await model.Encounter.getEncounterByEncounter_UID(Encounter_UID)
    let patient = await model.Patient.findById(encounter.PT_HS_MRN)
    if (!patient) {
      res.send({ code: 0, status: 'failed', message: `No patient found with ${encounter.PT_HS_MRN}`, Error_Code: errorModule.patient_not_found() })
    }
    if (encounter) {
      delete req.body.Encounter_UID
      delete req.body.PT_Username
      if (req.body.PT_Allergies && typeof (req.body.PT_Allergies) == 'string') return res.send({ code: 0, status: 'failed', message: 'PT_Allergies must be array.', Error_Code: 'HS351' })
      if (req.body.PT_Allergies && req.body.PT_Allergies == null) req.body.PT_Allergies = []
      if (req.body.PT_Family_History_Info && typeof (req.body.PT_Family_History_Info) == 'string') return res.send({ code: 0, status: 'failed', message: 'PT_Family_History_Info must be array.', Error_Code: 'HS352' })
      if (req.body.PT_Family_History_Info && req.body.PT_Family_History_Info == null) req.body.PT_Family_History_Info = []
      if (req.body.PT_Medication_Attachment && typeof (req.body.PT_Medication_Attachment) == 'string') return res.send({ code: 0, status: 'failed', message: 'PT_Medication_Attachment must be array.', Error_Code: 'HS354' })
      if (req.body.PT_Medication_Attachment && req.body.PT_Medication_Attachment == null) req.body.PT_Medication_Attachment = []
      if (req.body.PT_Medication && typeof (req.body.PT_Medication) == 'string') return res.send({ code: 0, status: 'failed', message: 'PT_Medication must be array.', Error_Code: 'HS353' })
      if (req.body.PT_Medication && req.body.PT_Medication == null) req.body.PT_Medication = []
      if (req.body.PT_Medical_Directive_Attachment && typeof (req.body.PT_Medical_Directive_Attachment) == 'string') return res.send({ code: 0, status: 'failed', message: 'PT_Medical_Directive_Attachment must be array.', Error_Code: 'HS355' })
      if (req.body.PT_Medical_Directive_Attachment && req.body.PT_Medical_Directive_Attachment == null) req.body.PT_Medical_Directive_Attachment = []
      if (req.body.PT_Medical_History_Attachment && typeof (req.body.PT_Medical_History_Attachment) == 'string') return res.send({ code: 0, status: 'failed', message: 'PT_Medical_History_Attachment must be array.', Error_Code: 'HS356' })
      if (req.body.PT_Medical_History_Attachment && req.body.PT_Medical_History_Attachment == null) req.body.PT_Medical_History_Attachment = []
      await model.Encounter.updateEncounterPatientHistory(Encounter_UID, req.body)
      await createAudit(req)
      // await sendMail(encounter)
      res.send({ code: 1, status: 'sucess', data: req.body, message: 'Patient history updated successfully' })
    } else {
      res.send({ code: 0, status: 'failed', message: `No encounter found with ${Encounter_UID}`, Error_Code: errorModule.Encounter_not_found() })
    }
  } catch (err) {
    logger.error(err.message)
    logger.fatal(err)
    res.send({ code: 2, status: 'failed', message: err.message, Error_Code: 'HS130' })
  }
})

router.put('/update-attachment', authorize(Role.Provider), async (req, res) => {
  var { body: { PT_Username, Encounter_UID } } = req
  if (!PT_Username) return res.send({ code: 0, status: 'failed', message: 'PT_Username is required', Error_Code: errorModule.PT_Username_required() })
  if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required', Error_Code: errorModule.Encounter_UID_required() })

  try {
    let patient = await model.Patient.findOneByEmail(PT_Username)
    if (!patient) {
      res.send({ code: 0, status: 'failed', message: `No patient found with ${PT_Username}`, Error_Code: errorModule.patient_not_found() })
    }
    let encounter = await model.Encounter.getEncounterByEncounter_UID(Encounter_UID)
    if (encounter) {
      delete req.body.Encounter_UID
      delete req.body.PT_Username
      var key = Object.keys(req.body)[0]
      var attachmentData = req.body[key]
      attachmentData[0].Attachment_By = req.userType
      attachmentData[0].Attachment_User = req.userType == 'Provider' ? req.user.PRO_UID : req.user.PT_HS_MRN
      attachmentData[0].Is_Deleted = false
      attachmentData[0].Attachment_Url = ''
      attachmentData[0]._id = await ObjectId()
      req.body[key] = attachmentData[0]
      await model.Encounter.updateEncounterPatientAttachment(Encounter_UID, req.body, key)
      await createAudit(req)
      // await sendMail(encounter)
      res.send({ code: 1, status: 'sucess', message: 'Patient history attachment updated successfully' })
    } else {
      res.send({ code: 0, status: 'failed', message: `No encounter found with ${Encounter_UID}`, Error_Code: errorModule.Encounter_not_found() })
    }
  } catch (err) {
    logger.error(err.message)
    logger.fatal(err)
    res.send({ code: 2, status: 'failed', message: err.message, Error_Code: 'HS131' })
  }
})

router.put('/update-medication/:id', authorize([Role.Provider]), async (req, res) => {
  var { body: { PT_Medication_Name, Encounter_UID }, params: { id } } = req
  if (!id) return res.send({ code: 0, status: 'failed', message: 'id is required', Error_Code: errorModule.id_required() })
  if (req.userType === 'Provider') {
    if (!PT_Medication_Name) return res.send({ code: 0, status: 'failed', message: 'PT_Medication_Name is required', Error_Code: errorModule.PT_Medication_Name_required() })
  }
  if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required', Error_Code: errorModule.Encounter_UID_required() })
  try {
    let medication = await model.PatientHistory.findMedicationById(id)
    const auditData = {
      Audit_UID: await auditUid(),
      Audit_Type: auditType.change,
      Data_Point: 'PRO/Update patient medication',
      PRO_First_Name: req.user.PRO_First_Name,
      PRO_Last_Name: req.user.PRO_Last_Name,
      PRO_UID: req.user.PRO_UID,
      Audit_Info: 'New- ' + await auditUid(),
      New_Value: req.body,
      Old_Value: medication,
      IP_Address: await model.Audit.getIpAddress(req)
    }
    let encounter = await model.Encounter.getEncounterByEncounter_UID(Encounter_UID)
    if (encounter) {
      let medication = await model.Encounter.getPatientMedication(Encounter_UID, id)
      if (medication) {
        await model.Encounter.updatePatientMedication(Encounter_UID, PT_Medication_Name, id)
        await model.Audit.create(auditData)
        // await sendMail(encounter)
        return res.status(200).send({
          code: 1,
          status: 'sucess',
          message: 'medication updated successfully'
        })
      } else {
        return res.send({ code: 0, status: 'failed', message: `Medication with id "${id}" is invalid`, Error_Code: errorModule.Medication_invalid() })
      }
    } else {
      res.send({ code: 0, status: 'failed', message: `No encounter found with ${Encounter_UID}`, Error_Code: errorModule.Encounter_not_found() })
    }
  } catch (err) {
    logger.error(err.message)
    logger.fatal(err)
    return res.send({
      code: 2,
      status: 'failed',
      message: err.message,
      Error_Code: 'HS132'
    })
  }
})

router.delete('/delete-medication/:id', authorize([Role.Provider, Role.Patient]), async (req, res) => {
  var { params: { id }, body: { Encounter_UID } } = req
  if (!id) return res.send({ code: 0, status: 'failed', message: 'id is required', Error_Code: errorModule.id_required() })
  try {
    if (req.userType === 'Provider') {
      if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required', Error_Code: errorModule.Encounter_UID_required() })
      let encounter = await model.Encounter.getEncounterByEncounter_UID(Encounter_UID)
      if (encounter) {
        let medication = await model.Encounter.getPatientMedication(Encounter_UID, id)
        if (medication) {
          await model.Encounter.deletePatientMedication(Encounter_UID, id)
          const auditData = {
            Audit_UID: await auditUid(),
            Audit_Type: auditType.remove,
            Data_Point: 'PRO/delete patient medication',
            PRO_First_Name: req.user.PRO_First_Name,
            PRO_Last_Name: req.user.PRO_Last_Name,
            PRO_UID: req.user.PRO_UID,
            Audit_Info: 'New- ' + await auditUid(),
            New_Value: req.body,
            Old_Value: medication,
            IP_Address: await model.Audit.getIpAddress(req)
          }
          await model.Audit.create(auditData)
          let encounter = await model.Encounter.getEncounterByEncounter_UID(Encounter_UID)
          // await sendMail(encounter)
          return res.status(200).send({
            code: 1,
            status: 'sucess',
            message: 'medication deleted successfully',
            data: encounter.PT_History.PT_Medication
          })
        } else {
          return res.send({ code: 0, status: 'failed', message: `Medication with id ${id} is invalid`, Error_Code: errorModule.Medication_invalid() })
        }
      } else {
        res.send({ code: 0, status: 'failed', message: `No encounter found with ${Encounter_UID}`, Error_Code: errorModule.Encounter_not_found() })
      }
    }
    let medication = await model.PatientHistory.findMedicationById(id)
    if (medication) {
      await model.PatientHistory.deleteMedicationById(id)
      let deletedData = await model.PatientHistory.findByUserId(medication.PT_HS_MRN)
      const auditData = {
        Audit_UID: await auditUid(),
        Audit_Type: auditType.remove,
        Data_Point: 'PT/Delete patient medication',
        PT_First_Name: req.user.PT_First_Name,
        PT_Last_Name: req.user.PT_Last_Name,
        PT_HS_MRN: req.user.PT_HS_MRN,
        Audit_Info: 'New- ' + await auditUid(),
        New_Value: deletedData,
        Old_Value: medication,
        IP_Address: await model.Audit.getIpAddress(req)
      }
      await model.Audit.create(auditData)
      return res.send({ code: 1, status: 'success', message: 'Medication deleted successfully', data: { PT_Medication: deletedData.PT_Medication } })
    } else {
      return res.send({ code: 0, status: 'failed', message: `Medication with id '${id}' is invalid`, Error_Code: errorModule.Medication_invalid() })
    }
  } catch (err) {
    logger.error(err.message)
    logger.fatal(err)
    return res.send({
      code: 2,
      status: 'failed',
      message: err.message,
      Error_Code: 'HS133'
    })
  }
})

router.put('/update-medical_directive/:id', authorize(Role.Provider), async (req, res) => {
  var { body: { PT_Medication_Name }, params: { id } } = req
  if (!id) return res.send({ code: 0, status: 'failed', message: 'id is required', Error_Code: errorModule.id_required() })
  if (!PT_Medication_Name) return res.send({ code: 0, status: 'failed', message: 'PT_Medication_Name is required', Error_Code: errorModule.PT_Medication_Name_required() })
  try {
    let medication = await model.PatientHistory.findMedicationById(id)
    if (!medication) return res.send({ code: 0, status: 'failed', message: `Medication with id "${id}" is invalid`, Error_Code: errorModule.Medication_invalid() })
    const auditData = {
      Audit_UID: await auditUid(),
      Audit_Type: auditType.change,
      Data_Point: 'PRO/Update patient medication',
      PRO_First_Name: req.user.PRO_First_Name,
      PRO_Last_Name: req.user.PRO_Last_Name,
      PRO_UID: req.user.PRO_UID,
      Audit_Info: 'New- ' + await auditUid(),
      New_Value: req.body,
      Old_Value: medication,
      IP_Address: await model.Audit.getIpAddress(req)
    }
    await model.PatientHistory.updateMedicationById(id, { PT_Medication_Name: PT_Medication_Name })
    await model.Audit.create(auditData)
    return res.status(200).send({
      code: 1,
      status: 'sucess',
      message: 'medication updated successfully'
    })
  } catch (err) {
    logger.error(err.message)
    logger.fatal(err)
    return res.send({
      code: 2,
      status: 'failed',
      message: err.message,
      Error_Code: 'HS134'
    })
  }
})

let createAudit = async (req) => {
  const auditData = {
    Audit_UID: await auditUid(),
    Audit_Info: 'New- ' + await auditUid(),
    Data_Point: 'PT_Problem/update',
    Audit_Type: auditType.view,
    PRO_First_Name: req.user.PRO_First_Name,
    PRO_Last_Name: req.user.PRO_Last_Name,
    PRO_UID: req.user.PRO_UID,
    IP_Address: await model.Audit.getIpAddress(req)
  }
  await model.Audit.create(auditData)
}


router.get('/history-suggestion/:Encounter_UID', authorize(Role.Patient), async (req, res) => {
  let Encounter_UID = req.params.Encounter_UID
  if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID required!', Error_Code: errorModule.Encounter_UID_required() })
  try {
    let encounter = await model.Encounter.getEncounterByEncounter_UID(Encounter_UID)
    if (!encounter) return res.send({ code: 0, status: 'failed', message: 'encounter not found!', Error_Code: errorModule.Encounter_not_found() })
    // eslint-disable-next-line no-unused-vars
    let diffrence = false
    let patientHistory = await model.PatientHistory.findPatientHistoryById(encounter.PT_HS_MRN)
    let patient = await model.Patient.findById(encounter.PT_HS_MRN)
    let provider = await model.Provider.findById(encounter.PRO_UID)
    let patientEncounterHistory = encounter.PT_History
    let encounterData = {}
    encounterData.PRO_Profile_Picture = provider.PRO_Profile_Picture != null ? await serviceModule.getImage(provider.PRO_Profile_Picture) : null
    encounterData.PRO_Last_Name = provider.PRO_Last_Name
    encounterData.Encounter_Date = encounter.Encounter_Date
    encounterData.Encounter_UID = encounter.Encounter_UID
    encounterData.Encounter_Time = encounter.Encounter_Time
    encounterData.Encounter_Chief_Complaint_PRO = encounter.Encounter_Chief_Complaint_PRO
    encounterData.Encounter_Chief_Complaint_PT = encounter.Encounter_Chief_Complaint_PT
    encounterData.Encounter_PT_Chief_Complaint_More = encounter.Encounter_PT_Chief_Complaint_More
    encounterData.Encounter_Type = encounter.Encounter_Type
    encounterData.PT_First_Name = patient.PT_First_Name
    encounterData.PT_Last_Name = patient.PT_Last_Name
    encounterData.PT_Gender = patient.PT_Gender
    encounterData.PT_HS_MRN = patient.PT_HS_MRN
    encounterData.PT_Profile_Picture = patient.PT_Profile_Picture != null ? await serviceModule.getImage(patient.PT_Profile_Picture) : null
    var jsonObject1 = patientEncounterHistory
    var jsonObject2 = patientHistory
    let differanceArray = {}

    delete jsonObject1._id
    delete jsonObject2._id
    delete jsonObject1.Created_At
    delete jsonObject2.Created_At
    delete jsonObject1.Updated_At
    delete jsonObject2.Updated_At
    delete jsonObject1.__v
    delete jsonObject2.__v
    var keys = Object.keys(jsonObject1)
    let count = 0
    for (var i = 0; i < keys.length; i++) {
      var key = keys[i]
      if (key != 'PRO_UID' && key != 'PT_Username' && key != 'PT_Organ_Donor' && key != 'PT_First_Name' && key != 'PT_Profile_Picture' && !key.includes('Confirmation')) {
        if (jsonObject1[key] != null && typeof (jsonObject1[key]) == 'object') {
          differanceArray[key] = {
            oldValue: jsonObject1[key] == null || jsonObject1[key] == undefined ? jsonObject1[key] : typeof (jsonObject1[key]) == 'number' ? `${jsonObject1[key]}` : typeof (jsonObject1[key]) == 'object' ? await getImageUrls(jsonObject1[key]) : jsonObject1[key],
            newValue: jsonObject2[key] == null || jsonObject2[key] == undefined ? jsonObject2[key] : typeof (jsonObject2[key]) == 'number' ? `${jsonObject2[key]}` : typeof (jsonObject2[key]) == 'object' ? await getImageUrls(jsonObject2[key]) : jsonObject2[key]
          }
          if (jsonObject1[key] != null && jsonObject1[key] != undefined && jsonObject1[key] && typeof (jsonObject1[key]) == 'object') {
            count = jsonObject1[key].length != jsonObject2[key].length ? count + 1 : count
          }
        } else {
          if (jsonObject1[key] != jsonObject2[key]) {
            differanceArray[key] = {
              oldValue: jsonObject1[key] == null || jsonObject1[key] == undefined ? jsonObject1[key] : typeof (jsonObject1[key]) == 'number' ? `${jsonObject1[key]}` : typeof (jsonObject1[key]) == 'object' ? await getImageUrls(jsonObject1[key]) : jsonObject1[key],
              newValue: jsonObject2[key] == null || jsonObject2[key] == undefined ? jsonObject2[key] : typeof (jsonObject2[key]) == 'number' ? `${jsonObject2[key]}` : typeof (jsonObject2[key]) == 'object' ? await getImageUrls(jsonObject2[key]) : jsonObject2[key]
            }
            count = count + 1
          } else {
            differanceArray[key] = {
              oldValue: jsonObject1[key] == null || jsonObject1[key] == undefined ? jsonObject1[key] : typeof (jsonObject1[key]) == 'number' ? `${jsonObject1[key]}` : typeof (jsonObject1[key]) == 'object' ? await getImageUrls(jsonObject1[key]) : jsonObject1[key],
              newValue: jsonObject1[key] == null || jsonObject1[key] == undefined ? jsonObject1[key] : typeof (jsonObject1[key]) == 'number' ? `${jsonObject1[key]}` : typeof (jsonObject1[key]) == 'object' ? await getImageUrls(jsonObject1[key]) : jsonObject1[key]
            }
          }
        }
      }
      if (i == keys.length - 1) {
        encounterData.changes = differanceArray
        return res.send({ code: 1, status: 'success', data: encounterData })
      }
    }
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.send({ code: 0, status: 'failed', message: error.message })
  }
})

let getImageUrls = async (arr) => {
  let newArray = []; let count = 0
  if (arr == null || arr.length == 0) {
    return newArray
  }
  if (arr.length == undefined) {
    return arr
  }
  if (arr[0].Attachment_Name) {
    for (var i = 0; i < arr.length; i++) {
      let url = arr[i].Attachment_Name != null || arr[i].Attachment_Name != '' ? await getImageUrl(arr[i].Attachment_Name) : null
      newArray.push({
        Attachment_Url: url,
        Is_Deleted: arr[i].Is_Deleted,
        _id: arr[i]._id,
        Attachment_Name: arr[i].Attachment_Name,
        Attachment_By: arr[i].Attachment_By,
        Attachment_User: arr[i].Attachment_User
      })
      count = count + 1
      if (count == arr.length) {
        return newArray
      }
    }
  } else {
    return arr
  }
}

let getImageUrl = async (imageKey) => {
  if (imageKey == null || imageKey == '') {
    return null
  } else {
    if (imageKey.includes('pdf')) {
      const url = s3.getSignedUrl('getObject', {
        Bucket: process.env.AWS_BUCKET,
        Key: imageKey,
        ResponseContentType: 'application/pdf',
        Expires: 60 * 60 * 24 * 15 // time in seconds: e.g. 60 * 5 = 5 mins
      })
      return url
    } else {
      const url = s3.getSignedUrl('getObject', {
        Bucket: process.env.AWS_BUCKET,
        Key: imageKey,
        Expires: 60 * 60 * 24 * 15 // time in seconds: e.g. 60 * 5 = 5 mins
      })
      return url
    }
  }
}

module.exports = router
