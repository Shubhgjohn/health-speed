/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
const Router = require('express').Router
const router = new Router()
const model = require('../../../models')
const serviceModule = require('../../../models').CommonServices
const authorize = require('../../../_helpers/authorize')
var { auditUid } = require('../../../utils/UID')
var auditType = require('../../../auditType.json')
const Role = require('../../../_helpers/role')
const logger = require('../../../models/errorlog')
var ObjectId = require('mongodb').ObjectID

// API for delete patient history attachment

router.delete('/:PT_HS_MRN/delete-attachment/:id', authorize([Role.Provider, Role.Patient]), async (req, res) => {
  var { params: { id, PT_HS_MRN } , body : { Encounter_UID } } = req
  if (!id) return res.send({ code: 0, status: 'failed', message: 'id is required', Error_Code: model.Errors.id_required() })
  try {
    if (req.userType == 'Provider') {
      let findInMedicalHistoryAttachment = await model.Encounter.findMedicalHistoryAttachment(Encounter_UID, id)
      if (findInMedicalHistoryAttachment && findInMedicalHistoryAttachment.PT_History.PT_Medical_History_Attachment.length > 0) {
       let data = findInMedicalHistoryAttachment.PT_History.PT_Medical_History_Attachment
       let attachmentName = data.find(ele=> ele._id == id)
        data = data.filter(ele=>{
          return ele._id != id
        })
        await model.Encounter.setMedicalHistoryAttachment(Encounter_UID, data)
        return res.send({ code: 1, status: 'sucess', message: `${attachmentName.Attachment_Name} was successfully deleted` })
      }

      let findInMedicalDirectiveAttachment = await model.Encounter.findMedicalDirectiveAttachment(Encounter_UID, id)
      if (findInMedicalDirectiveAttachment && findInMedicalDirectiveAttachment.PT_History.PT_Medical_Directive_Attachment.length > 0) {
        let data = findInMedicalDirectiveAttachment.PT_History.PT_Medical_Directive_Attachment
        let attachmentName = data.find(ele=> ele._id == id)
         data = data.filter(ele=>{
           return ele._id != id
         })
         await model.Encounter.setMedicalDirectiveAttchment(Encounter_UID, data)
         return res.send({ code: 1, status: 'sucess', message: `${attachmentName.Attachment_Name} was successfully deleted` })
      }

      let findInMedicationAttachment = await model.Encounter.findMedicationAttachment(Encounter_UID, id)
      if (findInMedicationAttachment && findInMedicationAttachment.PT_History.PT_Medication_Attachment.length > 0 ) {
        let data = findInMedicationAttachment.PT_History.PT_Medication_Attachment
        let attachmentName = data.find(ele=> ele._id == id)
         data = data.filter(ele=>{
           return ele._id != id
         })
         await model.Encounter.setMedicationAttachment(Encounter_UID, data)
         return res.send({ code: 1, status: 'sucess', message: `${attachmentName.Attachment_Name} was successfully deleted` })
      }
    } else {
      let findInMedicationAttachment = await model.PatientHistory.findMedicationAttachment(id)
      if (findInMedicationAttachment.length > 0) {
        await model.PatientHistory.deleteMedicationAttachment(id)
        let data = await getLatestHistory(PT_HS_MRN)
        let json = {
          PT_Medication_Attachment: await serviceModule.getFileUrl(data.PT_Medication_Attachment)
        }
        await updateHistory(PT_HS_MRN)
        await createAudit(req)
        return res.send({ code: 1, status: 'sucess', message: `${findInMedicationAttachment[0].PT_Medication_Attachment.Attachment_Name} was successfully deleted`, data: json })
        // }
      }
      let findInMedicalHistoryAttchment = await model.PatientHistory.findMedicalHistoryAttchment(id)
      if (findInMedicalHistoryAttchment.length > 0) {

        await model.PatientHistory.deleteMedicalHistoryAttachment(id)
        let data = await getLatestHistory(PT_HS_MRN)
        let json = {
          PT_Medical_History_Attachment: await serviceModule.getFileUrl(data.PT_Medical_History_Attachment)
        }
        await updateHistory(PT_HS_MRN)
        await createAudit(req)
        return res.send({ code: 1, status: 'sucess', message: `${findInMedicalHistoryAttchment[0].PT_Medical_History_Attachment.Attachment_Name} was successfully deleted`, data: json })
      }
      let findInMedicalDirectiveAttachment = await model.PatientHistory.findMedicalDirectiveAttachment(id)
      if (findInMedicalDirectiveAttachment.length > 0) {

        await model.PatientHistory.deleteMedicalDirectiveAttachment(id)
        let data = await getLatestHistory(PT_HS_MRN)
        let json = {
          PT_Medical_Directive_Attachment: await serviceModule.getFileUrl(data.PT_Medical_Directive_Attachment)
        }
        await updateHistory(PT_HS_MRN)
        await createAudit(req)
        return res.send({ code: 1, status: 'sucess', message: `${findInMedicalDirectiveAttachment[0].PT_Medical_Directive_Attachment.Attachment_Name} was successfully deleted`, data: json })
      }
      let LabAndImageAttachment = await model.PatientHistory.findLabAndImageAttachment(id)
      if (LabAndImageAttachment.length > 0) {

        await model.PatientHistory.deleteLabAndImageAttachment(id)
        let data = await getLatestHistory(PT_HS_MRN)
        let json = {
          PT_Lab_and_Image_Reports: await serviceModule.getFileUrl(data.PT_Lab_and_Image_Reports)
        }
        await updateHistory(PT_HS_MRN)
        await createAudit(req)
        return res.send({ code: 1, status: 'sucess', message: `${LabAndImageAttachment[0].PT_Lab_and_Image_Reports.Attachment_Name} was successfully deleted`, data: json })
      } else {
        return res.send({ code: 0, status: 'failed', message: 'No Attchment found for this id', Error_Code: 'HS128' })
      }
    }
  } catch (err) {
    console.log(err)
    logger.error(err.message)
    logger.fatal(err)
    res.send({ code: 2, status: 'failed', message: err.message, Error_Code: 'HS129' })
  }

  async function createAudit (req) {
    const auditData = {
      Audit_UID: await auditUid(),
      Audit_Info: 'New- ' + await auditUid(),
      Data_Point: req.userType == 'Provider' ? 'PRO/delete attachment' : 'PT/delete attachment',
      Audit_Type: auditType.remove,
      PRO_First_Name: req.userType == 'Provider' ? req.user.PRO_First_Name : null,
      PRO_Last_Name: req.userType == 'Provider' ? req.user.PRO_Last_Name : null,
      PRO_UID: req.userType == 'Provider' ? req.user.PRO_UID : null,
      PT_First_Name: req.userType == 'Patient' ? req.user.PT_First_Name : null,
      PT_Last_Name: req.userType == 'Patient' ? req.user.PT_Last_Name : null,
      PT_HSID: req.userType == 'Patient' ? req.user.PT_HS_MRN : null,
      IP_Address: await model.Audit.getIpAddress(req)
    }
    await model.Audit.create(auditData)
  }
})

let updateEncounterPatientHistory = async (encounters, history) => {
  let count = 0
  encounters.map(async ele => {
    await model.Encounter.updatePatientMedicalHistory(ele.Encounter_UID, history)
    count = count + 1
    if (count == encounters.length) {
      return true
    }
  })
}

let updateHistory = async (PT_HS_MRN) => {
  let encounters = await model.Encounter.getPatientsEncounterById(PT_HS_MRN)
  let currentDate = new Date()
  let upcomingEncounter = encounters.filter(ele => new Date(ele.Encounter_Date + ' ' + ele.Encounter_Time) >= currentDate)
  if (upcomingEncounter.length > 0) {
    let patientHx = await model.PatientHistory.findPatientHistoryById(PT_HS_MRN)
    await updateEncounterPatientHistory(upcomingEncounter, patientHx)
    return true
  } else {
    return true
  }
}

let getLatestHistory = async (PT_HS_MRN) => {
  return await model.PatientHistory.findPatientHistoryById(PT_HS_MRN)
}

module.exports = router
