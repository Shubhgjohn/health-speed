/* eslint-disable no-async-promise-executor */
/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const model = require('../../../models')
const errorModule = require('../../../models').Errors
const serviceModule = require('../../../models').CommonServices
const authorize = require('../../../_helpers/authorize')
var { auditUid } = require('../../../utils/UID')
var auditType = require('../../../auditType.json')
const Role = require('../../../_helpers/role')
var ObjectId = require('mongodb').ObjectID
const logger = require('../../../models/errorlog')

// API for delete patient history attachment

router.post('/add-attachment', authorize([Role.Provider, Role.Patient]), async (req, res) => {
  var { body: { PT_HS_MRN, Attachment_Name, type, Encounter_UID } } = req
  if (!PT_HS_MRN) return res.send({ code: 0, status: 'failed', message: 'PT_HS_MRN is required', Error_Code: errorModule.PT_HS_MRN_required() })
  if (!Attachment_Name) return res.send({ code: 0, status: 'failed', message: 'Attachment_Name is required', Error_Code: errorModule.Attachment_Name_required() })
  if (!type) return res.send({ code: 0, status: 'failed', message: 'type is required', Error_Code: 'HS122' })

  try {
    let patientHistory = await model.PatientHistory.findPatientHistoryById(PT_HS_MRN)
    let encounterData = {
      Attachment_Name: Attachment_Name,
      Attachment_By: req.userType,
      Attachment_User: req.userType == 'Provider' ? req.user.PRO_UID : req.user.PT_HS_MRN,
      Attachment_Url: null,
      Is_Deleted: false,
      _id: ObjectId()
    }
    await createAudit(req)
    if (patientHistory) {
      let attachmentData = {
        Attachment_Name: Attachment_Name,
        Attachment_By: req.userType,
        Attachment_User: req.userType == 'Provider' ? req.user.PRO_UID : req.user.PT_HS_MRN
      }
      if (type == 'PT_Medication_Attachment') {
        if (req.userType == 'Patient') {
          await model.PatientHistory.uploadMedicationAttachment(PT_HS_MRN, attachmentData)
        }

        let encounterHistory = await updateHistory(req.userType, PT_HS_MRN, encounterData, type, Encounter_UID)
        let latestHistoryData = await getLatestHistory(PT_HS_MRN)
        let data = req.userType == 'Patient' ? latestHistoryData.PT_Medication_Attachment : encounterHistory
        let json = {
          PT_Medication_Attachment: await serviceModule.getFileUrl(data)
        }
        return res.send({ code: 1, status: 'Sucess', data: json, message: `${Attachment_Name} was successfully uploaded` })
      }
      if (type == 'PT_Medical_Directive_Attachment') {
        if (req.userType == 'Patient') {
          await model.PatientHistory.uploadMedicalDirectiveAttachment(PT_HS_MRN, attachmentData)
        }
        let encounterHistory = await updateHistory(req.userType, PT_HS_MRN, encounterData, type, Encounter_UID)
        let latestHistoryData = await getLatestHistory(PT_HS_MRN)
        let data = req.userType == 'Patient' ? latestHistoryData.PT_Medical_Directive_Attachment : encounterHistory
        let json = {
          PT_Medical_Directive_Attachment: await serviceModule.getFileUrl(data)
        }
        return res.send({ code: 1, status: 'Sucess', data: json, message: `${Attachment_Name} was successfully uploaded` })
      }
      if (type == 'PT_Medical_History_Attachment') {
        if (req.userType == 'Patient') {
          await model.PatientHistory.uploadMedicalHistoryAttachment(PT_HS_MRN, attachmentData)
        }
        let encounterHistory = await updateHistory(req.userType, PT_HS_MRN, encounterData, type, Encounter_UID)
        let latestHistoryData = await getLatestHistory(PT_HS_MRN)
        let data = req.userType == 'Patient' ? latestHistoryData.PT_Medical_History_Attachment : encounterHistory
        let json = {
          PT_Medical_History_Attachment: await serviceModule.getFileUrl(data)
        }
        return res.send({ code: 1, status: 'Sucess', data: json, message: `${Attachment_Name} was successfully uploaded` })
      }
      if (type == 'PT_Lab_and_Image_Reports') {
        if (req.userType == 'Patient') {
          await model.PatientHistory.uploadLabAndImageReportAttachment(PT_HS_MRN, attachmentData)
        }
        let encounterHistory = await updateHistory(req.userType, PT_HS_MRN, encounterData, type, Encounter_UID)
        let latestHistoryData = await getLatestHistory(PT_HS_MRN)
        let data = req.userType == 'Patient' ? latestHistoryData.PT_Lab_and_Image_Reports : encounterHistory
        let json = {
          PT_Lab_and_Image_Reports: await serviceModule.getFileUrl(data)
        }
        return res.send({ code: 1, status: 'Sucess', data: json, message: `${Attachment_Name} was successfully uploaded` })
      }
    } else {
      let attachmentData = {
        Attachment_Name: Attachment_Name,
        Attachment_By: req.userType,
        Attachment_User: req.userType == 'Provider' ? req.user.PRO_UID : req.user.PT_HS_MRN
      }
      if (type == 'PT_Medication_Attachment') {
        let data = {
          PT_HS_MRN: PT_HS_MRN,
          PT_Medication_Attachment: attachmentData
        }
        let history = await model.PatientHistory.create(data)
        let json = {
          PT_Medication_Attachment: await serviceModule.getFileUrl(history.PT_Medication_Attachment)
        }
        await updateHistory(PT_HS_MRN, encounterData, type)
        return res.send({ code: 1, status: 'Sucess', data: json, message: `${Attachment_Name} was successfully uploaded` })
      }
      if (type == 'PT_Medical_Directive_Attachment') {
        let data = {
          PT_HS_MRN: PT_HS_MRN,
          PT_Medical_Directive_Attachment: attachmentData
        }
        let history = await model.PatientHistory.create(data)
        let json = {
          PT_Medical_Directive_Attachment: await serviceModule.getFileUrl(history.PT_Medical_Directive_Attachment)
        }
        await updateHistory(PT_HS_MRN, encounterData, type)
        return res.send({ code: 1, status: 'Sucess', data: json, message: `${Attachment_Name} was successfully uploaded` })
      }
      if (type == 'PT_Medical_History_Attachment') {
        let data = {
          PT_HS_MRN: PT_HS_MRN,
          PT_Medical_History_Attachment: attachmentData
        }
        let history = await model.PatientHistory.create(data)
        let json = {
          PT_Medical_History_Attachment: await serviceModule.getFileUrl(history.PT_Medical_History_Attachment)
        }
        await updateHistory(PT_HS_MRN, encounterData, type)
        return res.send({ code: 1, status: 'Sucess', data: json, message: `${Attachment_Name} was successfully uploaded` })
      }
      if (type == 'PT_Lab_and_Image_Reports') {
        let data = {
          PT_HS_MRN: PT_HS_MRN,
          PT_Lab_and_Image_Reports: attachmentData
        }
        let history = await model.PatientHistory.create(data)
        let json = {
          PT_Lab_and_Image_Reports: await serviceModule.getFileUrl(history.PT_Lab_and_Image_Reports)
        }
        await updateHistory(PT_HS_MRN, encounterData, type)
        return res.send({ code: 1, status: 'Sucess', data: json, message: `${Attachment_Name} was successfully uploaded` })
      }
    }
  } catch (err) {
    logger.error(err.message)
    logger.fatal(err)
    res.send({ code: 2, status: 'failed', message: err.message, Error_Code: 'HS123' })
  }
})

router.post('/add-medication', authorize([Role.Provider, Role.Patient]), async (req, res) => {
  var { body: { PT_HS_MRN, PT_Medication_Name, _id } } = req
  if (!PT_HS_MRN) return res.send({ code: 0, status: 'failed', message: 'PT_HS_MRN is required', Error_Code: errorModule.PT_HS_MRN_required() })
  if (!PT_Medication_Name) return res.send({ code: 0, status: 'failed', message: 'PT_Medication_Name is required', Error_Code: errorModule.PT_Medication_Name_required() })
  try {
    if (_id) {
      let medication = await model.PatientHistory.findMedicationById(_id)
      let PT_Username = medication.PT_Username
      if (!medication) return res.send({ code: 0, status: 'failed', message: `Medication with id "${_id}" is invalid`, Error_Code: errorModule.Medication_invalid() })
      const auditData = {
        Audit_UID: await auditUid(),
        Audit_Type: auditType.change,
        Data_Point: 'PRO/Update patient medication',
        PRO_First_Name: req.user.PRO_First_Name,
        PRO_Last_Name: req.user.PRO_Last_Name,
        PRO_UID: req.user.PRO_UID,
        Audit_Info: 'New- ' + await auditUid(),
        New_Value: req.body,
        Old_Value: medication,
        IP_Address: await model.Audit.getIpAddress(req)
      }
      await model.PatientHistory.updateMedicationById(_id, { PT_Medication_Name: PT_Medication_Name })
      let encounters = await model.Encounter.getPatientsEncounter(PT_Username)
      let currentDate = new Date()
      let patientHx = await model.PatientHistory.findByUsername(PT_Username)
      let upcomingEncounter = encounters.filter(ele => new Date(ele.Encounter_Date + ' ' + ele.Encounter_Time) >= currentDate)
      if (upcomingEncounter.length > 0) {
        await updateEncounterPatientHistory(upcomingEncounter, patientHx)
      }
      await model.Audit.create(auditData)
      let medicationData = await model.PatientHistory.findByUserId(PT_HS_MRN)
      return res.status(200).send({
        code: 1,
        status: 'sucess',
        message: 'medication updated successfully',
        data: { PT_Medication: medicationData.PT_Medication }
      })
    }
    let patientHistory = await model.PatientHistory.findPatientHistoryById(PT_HS_MRN)
    if (patientHistory) {
      let attachmentData = {
        PT_Medication_Name: PT_Medication_Name
      }
      await model.PatientHistory.addMedication(PT_HS_MRN, attachmentData)
      await updatePatientHistory(PT_HS_MRN, attachmentData)
      var data = await getLatestHistory(PT_HS_MRN)
      let json = {
        PT_Medication: data.PT_Medication
      }
      await createAudit(req)
      return res.send({ code: 1, status: 'Sucess', data: json, message: 'Medication addedd successfully' })
    } else {
      let attachmentData = {
        PT_Medication_Name: PT_Medication_Name
      }
      let data = {
        PT_HS_MRN: PT_HS_MRN,
        PT_Medication: attachmentData
      }
      let history = await model.PatientHistory.create(data)
      let json = {
        PT_Medication: history.PT_Medication
      }
      await createAudit(req)
      return res.send({ code: 1, status: 'Sucess', data: json, message: 'Medication addedd successfully' })
    }
  } catch (err) {
    logger.error(err.message)
    logger.fatal(err)
    res.send({ code: 2, status: 'failed', message: err.message, Error_Code: 'HS126' })
  }
})

let createAudit = async (req) => {
  const auditData = {
    Audit_UID: await auditUid(),
    Audit_Info: 'New- ' + await auditUid(),
    Data_Point: req.userType == 'Provider' ? 'PRO/add attachment' : 'PT/add attachment',
    Audit_Type: auditType.add,
    PRO_First_Name: req.userType == 'Provider' ? req.user.PRO_First_Name : null,
    PRO_Last_Name: req.userType == 'Provider' ? req.user.PRO_Last_Name : null,
    PRO_UID: req.userType == 'Provider' ? req.user.PRO_UID : null,
    PT_First_Name: req.userType == 'Patient' ? req.user.PT_First_Name : null,
    PT_Last_Name: req.userType == 'Patient' ? req.user.PT_Last_Name : null,
    PT_HSID: req.userType == 'Patient' ? req.user.PT_HS_MRN : null,
    IP_Address: await model.Audit.getIpAddress(req)
  }
  await model.Audit.create(auditData)
}

let getLatestHistory = async (PT_HS_MRN) => {
  return await model.PatientHistory.findPatientHistoryById(PT_HS_MRN)
}
let updateEncounterPatientHistory = async (encounters, historyData, type) => {
  let count = 0
  let history
  encounters.map(async ele => {
    if (type == 'PT_Medication_Attachment') {
      await model.Encounter.updateMedicationAttachment(ele.Encounter_UID, historyData)
    } else if (type == 'PT_Medical_Directive_Attachment') {
      history = await model.Encounter.updateMedicalDirectiveAttchment(ele.Encounter_UID, historyData)
    } else if (type == 'PT_Medical_History_Attachment') {
      history = await model.Encounter.updateMedicalHistoryAttachment(ele.Encounter_UID, historyData)
    } else if (type == 'PT_Lab_and_Image_Reports') {
      await model.Encounter.updateLabAttachment(ele.Encounter_UID, historyData)
    }
    count = count + 1
    if (count == encounters.length) {
      return history
    }
  })
}

let updatePatientHistory = async (PT_HS_MRN, data) => {
  let encounters = await model.Encounter.getPatientsEncounterById(PT_HS_MRN)
  let currentDate = new Date()
  let count = 0
  let upcomingEncounter = encounters.filter(ele => new Date(ele.Encounter_Date + ' ' + ele.Encounter_Time) >= currentDate)
  if (upcomingEncounter.length > 0) {
    upcomingEncounter.forEach(async element => {
      await model.Encounter.updatePatientMedication(element.Encounter_UID, data)
      count = count + 1
      if (count == upcomingEncounter.length) {
        return true
      }
    })
  } else {
    return true
  }
}

let updateHistory = async (user, PT_HS_MRN, data, type, EncounterUID) => {
  return new Promise(async (resolve, reject) => {
    if (user == 'Patient') {
      let encounters = await model.Encounter.getPatientsEncounterById(PT_HS_MRN)
      let currentDate = new Date()
      let upcomingEncounter = encounters.filter(ele => new Date(ele.Encounter_Date + ' ' + ele.Encounter_Time) >= currentDate)
      if (upcomingEncounter.length > 0) {
        let Encounter_UID = upcomingEncounter[0].Encounter_UID
        await updateEncounterPatientHistory(upcomingEncounter, data, type)
        let encounterData = await model.Encounter.getEncounterByEncounter_UID(Encounter_UID)
        let returnData = encounterData.PT_History[type]
        resolve(returnData)
      } else {
        resolve(true)
      }
    } else {
      let encounter = []
      let encounterData = await model.Encounter.getEncounterByEncounter_UID(EncounterUID)
      encounter.push({ Encounter_UID: EncounterUID })
      await updateEncounterPatientHistory(encounter, data, type)
      encounterData.PT_History[type].push(data)
      resolve(encounterData.PT_History[type])
    }
  })
}

module.exports = router
