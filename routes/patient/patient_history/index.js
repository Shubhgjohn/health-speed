const express = require('express')
const app = express()
const deleteAttachment = require('./deleteattachment')
const updatePatientHistory = require('./updatepatienthistory')
const addattachment = require('./addattachment')

app.use(deleteAttachment)
app.use(updatePatientHistory)
app.use(addattachment)
module.exports = app
