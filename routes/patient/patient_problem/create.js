/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const model = require('../../../models')
const errorModule = require('../../../models').Errors
const authorize = require('../../../_helpers/authorize')
var { auditUid, PT_PRO_UID } = require('../../../utils/UID')
var auditType = require('../../../auditType.json')
const moment = require('moment')
const Role = require('../../../_helpers/role')
const logger = require('../../../models/errorlog')

// API for create patient problem

router.post('/create', authorize(Role.Provider), async (req, res) => {
  var { body: { PT_Problem_Description, PT_HS_MRN } } = req
  if (!PT_Problem_Description) return res.send({ code: 0, status: 'failed', message: 'PT_Problem_Description is required', Error_Code: 'HS135' })
  if (!PT_HS_MRN) return res.send({ code: 0, status: 'failed', message: 'PT_Username is required', Error_Code: errorModule.PT_Username_required() })
  try {
    let patient = await model.Patient.findById(PT_HS_MRN)
    if (patient) {
      req.body.PT_PRO_UID = await PT_PRO_UID()
      req.body.PT_Problem_Created_By = `${req.user.PRO_First_Name}  ${req.user.PRO_Last_Name}`
      req.body.PT_Problem_Created_Timestamp = new Date()
      req.body.PRO_UID = req.user.PRO_UID
      req.body.PT_HS_MRN = patient.PT_HS_MRN
      let lastInsertedProblem = await model.PatientProblem.lastInserted()
      req.body.Problem_Index = lastInsertedProblem.length > 0 ? lastInsertedProblem[0].Problem_Index + 1 : 1
      let problemData = await model.PatientProblem.create(req.body)
      problemData.PT_Problem_Created_Timestamp = moment(new Date(problemData.PT_Problem_Created_Timestamp)).format('MM-DD-YYYY')
      await createAudit(req)
      res.send({ code: 1, status: 'sucess', message: 'Patient problem created successfully', data: problemData })
    } else {
      res.send({ code: 0, status: 'failed', message: `No patient found with ${PT_HS_MRN}`, Error_Code: errorModule.patient_not_found() })
    }
  } catch (err) {
    logger.error(err.message)
    logger.fatal(err)
    res.send({ code: 2, status: 'failed', message: err.message, Error_Code: 'HS136' })
  }
})
let createAudit = async (req) => {
  const auditData = {
    Audit_UID: await auditUid(),
    Audit_Info: 'New- ' + await auditUid(),
    Data_Point: 'PRO/create patient problem',
    Audit_Type: auditType.create,
    PRO_First_Name: req.user.PRO_First_Name,
    PRO_Last_Name: req.user.PRO_Last_Name,
    PRO_UID: req.user.PRO_UID,
    IP_Address: await model.Audit.getIpAddress(req)
  }
  await model.Audit.create(auditData)
}

module.exports = router
