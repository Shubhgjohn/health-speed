const express = require('express')
const app = express()
const createPatientProblem = require('./create')
const updatePatientProblem = require('./updatepatientproblem')
const updatePatientProblemIndex = require('./updateproblemindex')

app.use(createPatientProblem)
app.use(updatePatientProblem)
app.use(updatePatientProblemIndex)

module.exports = app
