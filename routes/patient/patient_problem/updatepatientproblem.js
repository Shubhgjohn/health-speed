/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
const Router = require('express').Router
const router = new Router()
const model = require('../../../models')
const authorize = require('../../../_helpers/authorize')
var { auditUid } = require('../../../utils/UID')
var auditType = require('../../../auditType.json')
const moment = require('moment')
const Role = require('../../../_helpers/role')
const logger = require('../../../models/errorlog')

// API for update patient problem

router.put('/update-problem/:PT_PRO_UID', authorize(Role.Provider), async (req, res) => {
  const { params: { PT_PRO_UID } } = req
  if (!PT_PRO_UID) return res.send({ code: 0, status: 'failed', message: 'PT_PRO_UID is required', Error_Code: 'HS137' })
  try {
    let patientProblem = await model.PatientProblem.findPatientProblemByID(PT_PRO_UID)
    if (patientProblem) {
      req.body.PT_Problem_Last_Modified_By = `${req.user.PRO_First_Name} ${req.user.PRO_Last_Name}`
      req.body.PT_Problem_Last_Modified_Timestamp = new Date()
      if (req.body.PT_Problem_Status == 'Resolved') {
        req.body.PT_Problem_Resolved_By = `${req.user.PRO_First_Name} ${req.user.PRO_Last_Name}`
        req.body.PT_Problem_Resolved_Timestamp = new Date()
      } else {
        req.body.PT_Problem_Resolved_By = null
        req.body.PT_Problem_Resolved_Timestamp = null
      }
      await model.PatientProblem.updatePatientProblem(PT_PRO_UID, req.body)
      req.body.PT_Problem_Last_Modified_Timestamp = moment(req.body.PT_Problem_Last_Modified_Timestamp).format('MM-DD-YYYY')
      req.body.PT_Problem_Resolved_Timestamp = req.body.PT_Problem_Status == 'Resolved' ? moment(req.body.PT_Problem_Resolved_Timestamp).format('MM-DD-YYYY') : null
      await createAudit(req)
      res.send({ code: 1, status: 'sucess', message: 'Patient problem updated successfully', data: req.body })
    } else {
      res.send({ code: 0, status: 'failed', message: `Patient problem with ${PT_PRO_UID} is not exist`, Error_Code: 'HS138' })
    }
  } catch (err) {
    logger.error(err.message)
    logger.fatal(err)
    res.send({ code: 2, status: 'failed', message: err.message, Error_Code: 'HS139' })
  }
})
let createAudit = async (req) => {
  const auditData = {
    Audit_UID: await auditUid(),
    Audit_Info: 'New- ' + await auditUid(),
    Data_Point: 'PRO/update Patient Problem',
    Audit_Type: auditType.change,
    PRO_First_Name: req.user.PRO_First_Name,
    PRO_Last_Name: req.user.PRO_Last_Name,
    PRO_UID: req.user.PRO_UID,
    IP_Address: await model.Audit.getIpAddress(req)
  }
  await model.Audit.create(auditData)
}

module.exports = router
