/* eslint-disable prefer-const */
/* eslint-disable eqeqeq */
const Router = require('express').Router
const router = new Router()
const model = require('../../../models')
const errorModule = require('../../../models').Errors
const authorize = require('../../../_helpers/authorize')
var { auditUid } = require('../../../utils/UID')
var auditType = require('../../../auditType.json')
const Role = require('../../../_helpers/role')
const logger = require('../../../models/errorlog')

// API for update patient problem

router.put('/update-problem', authorize(Role.Provider), async (req, res) => {
  const { body: { data } } = req
  if (!data) return res.send({ code: 0, status: 'failed', message: 'data is required', Error_Code: errorModule.data_required() })
  try {
    if (data.length > 0) {
      for (var index = 0; index < data.length; index++) {
        data[index].Problem_Index = index + 1
        data[index].PT_Problem_Last_Modified_By = `${req.user.PRO_First_Name} ${req.user.PRO_Last_Name}`
        data[index].PT_Problem_Last_Modified_Timestamp = new Date()
        await model.PatientProblem.updatePatientProblem(data[index].PT_PRO_UID, data[index])
        if (index == data.length - 1) {
          await createAudit(req)
          res.send({ code: 1, status: 'sucess', message: 'Patient problem updated successfully', data: req.body })
        }
      }
    } else {
      res.send({ code: 0, status: 'failed', message: 'No problem is provided', Error_Code: 'HS140' })
    }
  } catch (err) {
    logger.error(err.message)
    logger.fatal(err)
    res.send({ code: 2, status: 'failed', message: err.message, Error_Code: 'HS141' })
  }
})
let createAudit = async (req) => {
  const auditData = {
    Audit_UID: await auditUid(),
    Audit_Info: 'New- ' + await auditUid(),
    Data_Point: 'PRO/update patient problem',
    Audit_Type: auditType.change,
    PRO_First_Name: req.user.PRO_First_Name,
    PRO_Last_Name: req.user.PRO_Last_Name,
    PRO_UID: req.user.PRO_UID,
    IP_Address: await model.Audit.getIpAddress(req)
  }
  await model.Audit.create(auditData)
}

module.exports = router
