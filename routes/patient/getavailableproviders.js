/* eslint-disable no-self-assign */
/* eslint-disable no-unneeded-ternary */
/* eslint-disable no-async-promise-executor */
/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
/* eslint-disable prefer-const */
const Router = require('express')
const router = new Router()
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const model = require('../../models')
// const { auditUid } = require('../../utils/UID')
// const auditType = require('../../auditType.json')
const moment = require('moment-timezone')
const _ = require('lodash')
var aws = require('aws-sdk')
aws.config.update({
	secretAccessKey: process.env.AWS_SECRET_KEY,
	accessKeyId: process.env.AWS_ACCESS_KEY,
	region: 'us-east-1'
})

var s3 = new aws.S3()
let count = 0
let arr = []
router.get('/get-available-physicians/:PT_HS_MRN/:PRO_UID?', authorize(Role.Patient), async (req, res) => {
	var { params: { PRO_UID } } = req
	let current_date = await getTime()
	arr = []
	try {
		// var temp = 0
		let myDate = current_date + (24 * 60 * 60 * 1000) * 60 // date of next 60 days
		current_date = await getTime()
		let nextDate = current_date + (24 * 60 * 60 * 1000) * 365 // date of next 52 weeks
		const PT_HS_MRN = req.params.PT_HS_MRN
		var patient = await model.Patient.findById(PT_HS_MRN)
		if (!patient) return res.send({ code: 0, status: 'failed', message: `patient with id '${PT_HS_MRN}' is not in our system!` })
		if (!patient.PT_DOB) return res.send({ code: 0, status: 'failed', message: 'patient DOB is not set in our system!' })
		var PTDOB = new Date(`${patient.PT_DOB}`)
		var DOB_DIFF = new Date(new Date() - PTDOB)
		let currentAge = (DOB_DIFF.toISOString().slice(0, 4) - 1970)
		let providerList = await model.Provider.getAbailablePhysician(currentAge)
		// providerList = providerList.filter(a => a.PRO_UID == 'PRO-5844-1546')
		let user_ids = providerList.map(ele => {
			return ele.PRO_UID	
		})
		if (providerList.length == 0) {
			return res.send({ code: 1, status: 'success', data: [] })
		}
		const availability = await getAllAvailability(myDate, user_ids)
		const nextYearAvailability = await getAllAvailability(nextDate, user_ids)

		const providerAvailability = await getProviderAvailability(myDate, user_ids)
		const providerNextYearAvailability = await getProviderAvailability(nextDate, user_ids)
		getProviderData(providerList, PRO_UID, res, availability, patient, nextYearAvailability, providerAvailability, providerNextYearAvailability, req)
	} catch (err) {
		res.send({ code: 2, status: 'failed', message: err.message })
	}
})

let getProviderData = async (providers, PRO_UID, res, availability, patient, nextYearAvailability, providerAvailability, providerNextYearAvailability, req) => {
	for (var i = 0; i < providers.length; i++) {
		let availablePhysician = availability.filter(item => providers[i].PRO_UID == item.PRO_UID)
		if (availablePhysician.length > 0) {
			await getAvailabilityData(providers[i], PRO_UID, res, availablePhysician, providers.length, patient, nextYearAvailability, providerAvailability, providerNextYearAvailability, req)
			if (i == providers.length - 1) {
				if (PRO_UID) {
					arr = await removeProvider(arr, PRO_UID)
				}
				return res.send({ code: 1, status: 'success', data: arr })
			} else {
				continue
			}
		} else {
			if (i == providers.length - 1) {
				if (PRO_UID) {
					arr = await removeProvider(arr, PRO_UID)
				}
				return res.send({ code: 1, status: 'success', data: arr })
			}
		}
	}
}

let getAvailabilityData = async (provider, PRO_UID, res, availablePhysician, totalProvider, patient, nextYearAvailability, providerAvailability, providerNextYearAvailability, req) => {
	for (let i = 0; i < availablePhysician.length; i++) {
		provider.PRO_Profile_Picture = provider.PRO_Profile_Picture ? await getImage(provider.PRO_Profile_Picture) : ''
		provider.PRO_Primary_Speciality = provider.PRO_Speciality.filter(item => item.PRO_Primary_Speciality == true)[0] ? provider.PRO_Speciality.filter(item => item.PRO_Primary_Speciality == true) : []
		provider.PT_to_PRO_Distance = await distance(patient.PT_Home_Address_Latitude, patient.PT_Home_Address_Longitude, provider.PRO_Office_Address_Latitude, provider.PRO_Office_Address_Longitude)
		provider.PRO_Office_Rate_Status = provider.PRO_Office_Rate_Status
		provider.PRO_Home_Rate_Status = provider.PRO_Home_Rate_Status
		if (provider.PT_to_PRO_Distance > 5) {
			provider.PT_to_PRO_Distance = provider.PT_to_PRO_Distance.toFixed(0)
		} else {
			provider.PT_to_PRO_Distance = Number(parseFloat(provider.PT_to_PRO_Distance).toFixed(1)).toString()
		}
		provider.PRO_Special_Rate = provider.PRO_Special_Rate ? provider.PRO_Special_Rate : []
		let schedulingData = await getSchedulling(nextYearAvailability, provider, providerAvailability, providerNextYearAvailability, req, res)
		schedulingData = await getSortedData(schedulingData)
		provider.schedulingData = schedulingData
		let providerNextAvailabilityData = await getProviderNextAvailability(provider.schedulingData)
		provider.PRO_Next_Available_Appointment = providerNextAvailabilityData ? providerNextAvailabilityData[1] : null
		provider.PRO_Next_Available_Date = providerNextAvailabilityData ? providerNextAvailabilityData[0] : null
		provider.PRO_Next_Available_for_Home_Visit = providerNextAvailabilityData ? providerNextAvailabilityData[3] : false
		provider.PRO_Next_Available_for_Office_Visit = providerNextAvailabilityData ? providerNextAvailabilityData[2] : false
		provider.Pro_Office_Hours_Time = provider.Pro_Office_Hours_Time
		if (!arr.find(ele => ele.PRO_UID == provider.PRO_UID)) {
			arr.push(provider)
		}
		count = count + 1
		if (count == totalProvider) {
			if (PRO_UID) {
				arr = await removeProvider(arr, PRO_UID)
				arr = await removeUnavailableProvider(arr)
			}
			return
		} else {
			arr = await removeUnavailableProvider(arr)
			return
		}
	}
}

let removeUnavailableProvider = async (arr) => {
	arr = arr.filter(ele => {
		return ele.PRO_Next_Available_Appointment != null
	})
	return arr
}
let removeProvider = async (arr, PRO_UID) => {
	arr = arr.filter(ele => {
		return (ele.PRO_UID != PRO_UID && ele.PRO_Next_Available_Appointment != null)
	})
	return arr
}
let getSortedData = async (scheduling) => {
	let dates = scheduling.sort(function (a, b) {
		return new Date(a.Full_Date).getTime() - new Date(b.Full_Date).getTime()
	})
	return dates
}
let getProviderNextAvailability = async (schedulingData) => {
	let time1
	let time2
	let current_date = await getTime()
	let nextDate = current_date + (24 * 60 * 60 * 1000) * 365 // date of next 52 weeks
	current_date = await getTime()
	let dates = await getDatesArray(current_date, nextDate)
	for (let x = 0; x < dates.length; x++) {
		let data = schedulingData.find(element => element.Full_Date == dates[x])
		if (data && (data.Regular_Hour.length > 0 || data.Off_Hour.length > 0)) {
			data.Regular_Hour = data.Regular_Hour.filter((item) => { return item != undefined })
			let regular = data.Regular_Hour.map(ele => ({
				From_Time: parseFloat(moment(ele.From_Time, ['h:mma']).format('HH.mm')),
				To_Time: ele.To_Time,
				Availability_Type: ele.Availability_Type
			}))
			regular = regular.sort(compare)
			data.Off_Hour = data.Off_Hour.filter((item) => { return item != undefined })
			let off = data.Off_Hour.map(ele => ({
				From_Time: parseFloat(moment(ele.From_Time, ['h:mma']).format('HH.mm')),
				To_Time: ele.To_Time,
				Availability_Type: ele.Availability_Type
			}))
			off = off.sort(compare)
			let firstRegularData = regular.length > 0 ? regular[0] : null
			let firstOffData = off.length > 0 ? off[0] : null
			firstRegularData = data.Regular_Hour.find(ele => ele.To_Time == firstRegularData.To_Time)
			firstOffData = data.Off_Hour.length > 0 ? data.Off_Hour.find(ele => ele.To_Time == firstOffData.To_Time) : null

			if (firstRegularData) {
				time1 = moment(firstRegularData.From_Time, 'hh:mma').format('H.mm')
			}
			if (firstOffData) {
				time2 = moment(firstOffData.From_Time, 'hh:mma').format('H.mm')
			}
			if (time1 && time2) {
				if (parseFloat(time1) < parseFloat(time2)) {
					time1 = moment(`${time1}`, 'H.mm').format('hh:mma')
					return [dates[x], time1, true, true]
				} else {
					time2 = moment(`${time2}`, 'H.mm').format('hh:mma')
					return [dates[x], time2, true, true]
				}
			} else if (time1 && time2 == undefined) {
				time1 = moment(`${time1}`, 'H.mm').format('hh:mma')
				return [dates[x], time1, true, true]
			} else if (time2 && time1 == undefined) {
				time2 = moment(`${time2}`, 'H.mm').format('hh:mma')
				return [dates[x], time2, true, true]
			}
		}
	}
}
function compare (a, b) {
	return a.From_Time - b.From_Time
}
let getTime = () => {
	let d = new Date()
	var newYork = moment.tz(d, process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm:ss')
	return Date.parse(newYork)
}

let getSchedulling = async (availabilities, provider, providerAvailability, providerNextYearAvailability, req, res) => {
	let current_date = await getTime()
	let nextDate = current_date + (24 * 60 * 60 * 1000) * 365 // date of next 52 weeks
	current_date = await getTime()
	let dates = await getDatesArray(current_date, nextDate)
	return new Promise(async (resolve, reject) => {
		let totalArray = []
		let itemprocessed = 0
		dates.forEach(async ele => {
			let findAvailability = await availabilities.find(item => (item.PRO_Availability_Date == ele && item.PRO_UID == provider.PRO_UID))
			let providerAvailabiliy = await providerNextYearAvailability.find(item => (item.PRO_Availability_Date == ele && item.PRO_UID == provider.PRO_UID))
			if (findAvailability) {
					let	OfficeHourSlots
					let HomeHourSlots
					let mergeArray = {}
					let slotArrays = {}
					let current_date = await getTime()
					let currentDate = moment(current_date).format('MM-DD-YYYY')
					let time = moment(current_date).format('HH.mm')
				
					let officeTime = `${parseFloat(time) + parseFloat(provider.PRO_Notice_for_Office_Visit)}`
					let homeTime = `${parseFloat(time) + parseFloat(provider.PRO_Notice_for_Home_Visit)}`
					
					officeTime = `${parseFloat(officeTime).toFixed(2)}` 
					homeTime = `${parseFloat(homeTime).toFixed(2)}`
					
					let Regular_Home_Hour_Rates
					let Regular_Office_Hour_Rates
					let Off_Home_Hour_Rates
					let Off_Office_Hour_Rates
	
					let date = moment(new Date(ele)).format('M/D/YY')
					let specialRates = provider.PRO_Special_Rate.length > 0 ? provider.PRO_Special_Rate.filter(item => item.PRO_Special_Rate_Date == date) : []
					if (req.query.type == 'followup') {
						Regular_Home_Hour_Rates = specialRates.length == 0 ? `${provider.PRO_Home_Office_Hours_Followup_Rate}` : specialRates[0].PRO_Home_Followup_Special_Hourly_Rate ? `${specialRates[0].PRO_Home_Followup_Special_Hourly_Rate}` : `${provider.PRO_Home_Office_Hours_Followup_Rate}`
						Regular_Office_Hour_Rates = specialRates.length == 0 ? `${provider.PRO_Office_Office_Hours_Followup_Rate}` : specialRates[0].PRO_Office_Followup_Special_Hourly_Rate ? `${specialRates[0].PRO_Office_Followup_Special_Hourly_Rate}` : `${provider.PRO_Office_Office_Hours_Followup_Rate}`
						Off_Home_Hour_Rates = specialRates.length == 0 ? `${provider.PRO_Home_Off_Hours_Followup_Rate}` : specialRates[0].PRO_Home_Followup_Special_Off_Hourly_Rate ? `${specialRates[0].PRO_Home_Followup_Special_Off_Hourly_Rate}` : `${provider.PRO_Home_Off_Hours_Followup_Rate}`
						Off_Office_Hour_Rates = specialRates.length == 0 ? `${provider.PRO_Office_Off_Hours_Followup_Rate}` : specialRates[0].PRO_Office_Followup_Special_Off_Hourly_Rate ? `${specialRates[0].PRO_Office_Followup_Special_Off_Hourly_Rate}` : `${provider.PRO_Office_Off_Hours_Followup_Rate}`
					} else {
						Regular_Home_Hour_Rates = specialRates.length == 0 ? `${provider.PRO_Home_Office_Hours_New_Visit_Rate}` : specialRates[0].PRO_Home_New_Visit_Special_Hourly_Rate ? `${specialRates[0].PRO_Home_New_Visit_Special_Hourly_Rate}` : `${provider.PRO_Home_Office_Hours_New_Visit_Rate}`
						Regular_Office_Hour_Rates = specialRates.length == 0 ? `${provider.PRO_Office_Office_Hours_New_Visit_Rate}` : specialRates[0].PRO_Office_New_Visit_Special_Hourly_Rate ? `${specialRates[0].PRO_Office_New_Visit_Special_Hourly_Rate}` : `${provider.PRO_Office_Office_Hours_New_Visit_Rate}`
						Off_Home_Hour_Rates = specialRates.length == 0 ? `${provider.PRO_Home_Off_Hours_New_Visit_Rate}` : specialRates[0].PRO_Home_Followup_Special_Off_Hourly_Rate ? `${specialRates[0].PRO_Home_New_Visit_Special_Off_Hourly_Rate}` : `${provider.PRO_Home_Off_Hours_New_Visit_Rate}`
						Off_Office_Hour_Rates = specialRates.length == 0 ? `${provider.PRO_Office_Off_Hours_New_Visit_Rate}` : specialRates[0].PRO_Office_New_Visit_Special_Off_Hourly_Rate ? `${specialRates[0].PRO_Office_New_Visit_Special_Off_Hourly_Rate}` : `${provider.PRO_Office_Off_Hours_New_Visit_Rate}`
					}

					let availability_date = providerAvailabiliy.PRO_Availability_Date
					let day = moment(new Date(availability_date)).format('ddd')
					day = day == 'Tue' ? 'Tues' : day
					day = day == 'Thu' ? 'Thur' : day
					let hourArray = provider.Pro_Office_Hours_Time
					let actualDay = hourArray.find(ele => ele.day == day)

					let endTime = '23.5'
					let RegularStartTime = moment(`${actualDay.fromTime.time} ${actualDay.fromTime.zone}`, 'hh:mm A').format('H.mm')
					let RegularEndTime = moment(`${actualDay.toTime.time} ${actualDay.toTime.zone}`, 'hh:mm A').format('H.mm')

					RegularStartTime = RegularStartTime.includes('.30') ? `${RegularStartTime.split('.')[0]}.5` : RegularStartTime
					RegularEndTime = RegularEndTime.includes('.30') ? `${RegularEndTime.split('.')[0]}.5` : RegularEndTime
					if (findAvailability.PRO_Availability_Date == currentDate) {
						if (parseFloat(officeTime) > parseFloat(endTime) && parseFloat(homeTime) > parseFloat(endTime)) {
							itemprocessed = itemprocessed + 1
							slotArrays = {}
						} else {
							
								officeTime = officeTime.split('.')[1] > 50 ? `${parseFloat(officeTime.split('.')[0]) + 1}` : `${officeTime.split('.')[0]}.5`
								homeTime = homeTime.split('.')[1] > 50 ? `${parseFloat(homeTime.split('.')[0]) + 1}` : `${homeTime.split('.')[0]}.5`
								
								let startIndexOffice = findAvailability.PRO_Slots.findIndex(z => z.Slot_Format == officeTime)
								let startIndexHome = findAvailability.PRO_Slots.findIndex(z => z.Slot_Format == homeTime)
								let endIndex = findAvailability.PRO_Slots.findIndex(z => z.Slot_Format == endTime)
						
								let availabilitySlotsOffice = findAvailability.PRO_Slots.slice(startIndexOffice, endIndex)
								let providerAvailabilitySlotsOffice = providerAvailabiliy.PRO_Slots.slice(startIndexOffice, endIndex)

								let availabilitySlotsHome = findAvailability.PRO_Slots.slice(startIndexHome, endIndex)
								let providerAvailabilitySlotsHome = providerAvailabiliy.PRO_Slots.slice(startIndexHome, endIndex)

								OfficeHourSlots = (provider.PRO_Office_Rate_Status == 'Complete' && availabilitySlotsOffice.length > 0 ) ? await getOfficeHourSlots(availabilitySlotsOffice, providerAvailabilitySlotsOffice, provider, ele) : []
				    			HomeHourSlots = (provider.PRO_Home_Rate_Status == 'Complete' && availabilitySlotsHome.length > 0 )   ? await getHomeHourSlots(availabilitySlotsHome, providerAvailabilitySlotsHome, provider, ele) : []
								mergeArray = mergeArray && mergeArray.length > 0 ? mergeArray.concat(OfficeHourSlots) : OfficeHourSlots
								mergeArray = mergeArray && mergeArray.length > 0 ? mergeArray.concat(HomeHourSlots) : HomeHourSlots						
								slotArrays = await getHourArray(mergeArray, RegularStartTime, RegularEndTime)
								itemprocessed = itemprocessed + 1
							}
					} else {
						OfficeHourSlots = (provider.PRO_Office_Rate_Status == 'Complete') ? await getOfficeHourSlots(findAvailability.PRO_Slots, providerAvailabiliy.PRO_Slots, provider, ele) : []
				    	HomeHourSlots = (provider.PRO_Home_Rate_Status == 'Complete' )   ? await getHomeHourSlots(findAvailability.PRO_Slots, providerAvailabiliy.PRO_Slots, provider, ele) : []
						mergeArray = mergeArray && mergeArray.length > 0 ? mergeArray.concat(OfficeHourSlots) : OfficeHourSlots
						mergeArray = mergeArray && mergeArray.length > 0 ? mergeArray.concat(HomeHourSlots) : HomeHourSlots			
						slotArrays = await getHourArray(mergeArray, RegularStartTime , RegularEndTime)
						itemprocessed = itemprocessed + 1
					}
					let Is_Home_Special_Rates = false;
					let Is_Office_Special_Rates = false;

					if(specialRates.length > 0) {
						Is_Home_Special_Rates = await getHomeSpecialRates(specialRates[0])
						Is_Office_Special_Rates =  await getOfficeSpecialRates(specialRates[0])
					  } 
					totalArray.push({
						PRO_UID: provider.PRO_UID,
						year: ele.split('-')[2],
						month: ele.split('-')[0],
						Date: ele.split('-')[1],
						Full_Date: ele,
						Regular_Hour: (!_.isEmpty(slotArrays)  && slotArrays.Regular.length > 0) ? slotArrays.Regular : [],
						Off_Hour: (!_.isEmpty(slotArrays)  && slotArrays.Off.length > 0) ? slotArrays.Off : [],
						Regular_Home_Hour_Rates: Regular_Home_Hour_Rates,
						Regular_Office_Hour_Rates: Regular_Office_Hour_Rates,
						Off_Home_Hour_Rates: Off_Home_Hour_Rates,
						Off_Office_Hour_Rates: Off_Office_Hour_Rates,
						Is_Special_Rates: specialRates.length == 0 ? false : true,
						Is_Home_Special_Rates:  Is_Home_Special_Rates,
						Is_Office_Special_Rates:  Is_Office_Special_Rates,
						Is_Office_Availability: await getOfficeAvailability(availabilities, ele, provider),
						Is_Home_Availability: await getHomeAvailability(availabilities, ele, provider)
					})
				} else {
					totalArray.push({
						PRO_UID: provider.PRO_UID,
						year: ele.split('-')[2],
						month: ele.split('-')[0],
						Date: ele.split('-')[1],
						Full_Date: ele,
						Regular_Hour: [],
						Off_Hour: []
					})
				itemprocessed = itemprocessed + 1
			}
			if (itemprocessed == dates.length) {
				// console.log("aaa",totalArray.length);
				resolve(totalArray)
			}
		})
	})
}



let getHourArray = async (array, regularStart, regularEnd,date) => {
	return new Promise((resolve,reject)=>{
		regularEnd = regularEnd == '0.00' ? '24' : regularEnd
		let Regular_Array = []
		let Off_Array = []
		if(array.length==0){
			resolve( {
				Regular: [],
				Off: [],
			});
		}
		for (let index = 0; index < array.length; index++) {
			const ele = array[index];
			if (parseFloat(ele.Slot_Time) >= parseFloat(regularStart) && parseFloat(ele.Slot_Time) < parseFloat(regularEnd)) {
				delete ele.Slot_Time
				Regular_Array.push(ele)
			} else {
				delete ele.Slot_Time
				Off_Array.push(ele)
			}
			if (index == array.length - 1 ) {
				resolve( {
					Regular: Regular_Array,
					Off: Off_Array
						});
			}
		}
	})
}

let getHomeSpecialRates = async (specialRates) => {
		let home = []
		home.push(specialRates.PRO_Home_Followup_Special_Hourly_Rate,
			specialRates.PRO_Home_Followup_Special_Off_Hourly_Rate,
			specialRates.PRO_Home_New_Visit_Special_Hourly_Rate,
			specialRates.PRO_Home_New_Visit_Special_Off_Hourly_Rate)
		if (!home.includes(null)) {
			return true
		} else {
			return false
		}
}

let getOfficeSpecialRates = async (specialRates) => {
	// return new Promise ((resolve,reject)=>{
	let Office = []
	Office.push(specialRates.PRO_Office_Followup_Special_Hourly_Rate,
		specialRates.PRO_Office_Followup_Special_Off_Hourly_Rate,
		specialRates.PRO_Office_New_Visit_Special_Hourly_Rate,
		specialRates.PRO_Office_New_Visit_Special_Off_Hourly_Rate)
	if (!Office.includes(null)) {
		return true
		} else {
		return false
	}
// });
}

let getOfficeAvailability = async (availabilities, date, provider) => {
	let availability = await availabilities.find(item => (item.PRO_Availability_Date == date && item.PRO_UID == provider.PRO_UID))
	if (availability) {
		let officeAvailability = await availability.PRO_Slots.find(ele => ele.Is_Office_Availability == true)
		if (officeAvailability) {
			return true
		} else {
			return false
		}
	} else {
		return false
	}
}

let getHomeAvailability = async (availabilities, date, provider) => {
	let availability = await availabilities.find(item => (item.PRO_Availability_Date == date && item.PRO_UID == provider.PRO_UID))
	if (availability) {
		let homeAvailability = await availability.PRO_Slots.find(ele => ele.Is_Home_Availability == true)
		if (homeAvailability) {
			return true
		} else {
			return false
		}
	} else {
		return false
	}
}


let getOfficeHourSlots = async (slots, slots1, provider, data) => {

	let officeHour = []
	let buffer = parseFloat(provider.PRO_Office_Visit_Buffer) * 2
	for (var i = 0; i < slots.length;) {
		if (i >= slots.length - 1) break
		if ((slots[i].Is_Office_Availability && slots[i].Is_Office_Available && slots[i + 1].Is_Office_Availability && slots[i + 1].Is_Office_Available) &&
    slots1[i].Is_Office_Available && slots1[i + 1].Is_Office_Available) {
			officeHour.push({
				From_Time: `${slots[i].Slot_Time}${slots[i].Slot_Time_Zone}`,
				To_Time: `${slots[i + 1].Slot_Time.includes(30) ? `${parseInt(slots[i + 1].Slot_Time.split(':')[0]) == 12 ? 1 : parseInt(slots[i + 1].Slot_Time.split(':')[0]) + 1}:00` : `${parseInt(slots[i + 1].Slot_Time.split(':')[0])}:30`}${(slots[i + 1].Slot_Time == '11:30' && slots[i + 1].Slot_Time_Zone == 'am') ? 'pm' : slots[i + 1].Slot_Time_Zone}`,
				Availability_Type: 'Office',
				Slot_Time : slots[i].Slot_Format
			})
			i = i > slots.length - 2 ? i + 1 : i + 2
			i = i > slots.length - 2 ? i : i + buffer
		} else {
			i = i + 1
		}
		if (i >= slots.length - 1) {
			return officeHour
		}
	}
}



let getHomeHourSlots = async (slots, slots1, provider, date) => {
	
	let isPushed = false
	let neededTarvel = provider.PRO_Home_Visit_Buffer / 15
	let travelCount = neededTarvel / 2
	let count = neededTarvel + 2
	let homeSlotArray = []
	let homeArray = []

	for (var x = 0; x < slots.length;) {
		
		if (slots[x].Is_Home_Availability && slots[x].Is_Home_Available && slots1[x].Is_Home_Available) {
			for (let y = x; y < x + count; y++) {
				if (y == slots.length - 1) break
				homeArray.push(slots[y].Is_Home_Availability, slots[y].Is_Home_Available, slots1[y].Is_Home_Available)
				if (y == x + count - 1) {
					if (!homeArray.includes(false) && slots[x + 2 + travelCount]) {
						homeSlotArray.push({
							From_Time: x == 0 ? `${slots[x].Slot_Time}${slots[x].Slot_Time_Zone}` : `${slots[x + travelCount].Slot_Time}${slots[x + travelCount].Slot_Time_Zone}`,
							To_Time: x == 0 ? `${slots[x + 2].Slot_Time}${slots[x + 2].Slot_Time_Zone}` : `${slots[x + 2 + travelCount].Slot_Time}${slots[x + 2 + travelCount].Slot_Time_Zone}`,
							Availability_Type: 'Home',
							Slot_Time : x == 0 ? slots[x].Slot_Format : slots[x + travelCount].Slot_Format
						})

						isPushed = true
						homeArray = []
					} else {
						homeArray = []
						isPushed = false
					}
				}
			}
		}
		if (x == slots.length - 1 || slots.length - x <= 2 + neededTarvel) {
			return homeSlotArray
		}
		x = isPushed ? x == 0 ? x + 2 + travelCount : x + 2 + neededTarvel : x + 1
	}

}



let getImage = (imageKey) => {
	if (imageKey == null || imageKey == '') {
		return ''
	} else {
		const url = s3.getSignedUrl('getObject', {
			Bucket: process.env.AWS_BUCKET,
			Key: imageKey,
			Expires: 60 * 60 * 24 * 15
		})
		return url
	}
}

let getDatesArray = (startDate, stopDate) => {
	var dateArray = []
	var currentDate = moment(startDate)
	stopDate = moment(stopDate)
	while (currentDate <= stopDate) {
		dateArray.push(moment(currentDate).format('MM-DD-YYYY'))
		currentDate = moment(currentDate).add(1, 'days')
	}
	return dateArray
}

let distance = async (lat1, lon1, lat2, lon2) => {
	if ((lat1 == lat2) && (lon1 == lon2)) {
		return 0
	} else {
		var radlat1 = Math.PI * lat1 / 180
		var radlat2 = Math.PI * lat2 / 180
		var theta = lon1 - lon2
		var radtheta = Math.PI * theta / 180
		var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta)
		if (dist > 1) {
			dist = 1
		}
		dist = Math.acos(dist)
		dist = dist * 180 / Math.PI
		dist = dist * 60 * 1.1515
		return dist
	}
}

async function getAllAvailability (date, ids) {
	var dateObj = await getTime()
	dateObj = moment(dateObj).format('YYYY-MM-DD')
	dateObj = new Date(dateObj)
	let allAvailability = await model.ProviderAvailability.getAllAvailability(ids, dateObj, new Date(date))
	return allAvailability
}

async function getProviderAvailability (date, ids) {
	var dateObj = await getTime()
	dateObj = moment(dateObj).format('YYYY-MM-DD')
	dateObj = new Date(dateObj)
	let allAvailability = await model.Availability.getAllAvailability(ids, dateObj, new Date(date))
	return allAvailability
}
module.exports = router
