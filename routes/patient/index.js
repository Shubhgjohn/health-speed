const express = require('express')
const app = express()
const getPatient = require('./patient')
const registerPatient = require('./registration')
const loginPatient = require('./login')
const patientUpdateRoute = require('./updatepatient')
const updatepatienthistory = require('./updatepatienthistory')
const getPatientById = require('./getpatientbyid')
const patientProblem = require('./patient_problem')
const patientHistory = require('./patient_history')
const createEncounter = require('./createEncounter')
const dashboardAndVisit = require('./dashboardAndVisit')
const providerById = require('./get-providerbyid')
const checkPatientExist = require('./checkExistEmail')
const stripe = require('./stripe')
const sharePastChart = require('./shareAllPastChart')
const getAvailableProvider = require('./getavailableproviders')

app.use(getPatient)
app.use(registerPatient)
app.use(loginPatient)
app.use(patientUpdateRoute)
app.use(updatepatienthistory)
app.use(getPatientById)
app.use('/patient-problem', patientProblem)
app.use('/patient-history', patientHistory)
app.use(createEncounter)
app.use(dashboardAndVisit)
app.use(providerById)
app.use(checkPatientExist)
app.use(stripe)
app.use(sharePastChart)
app.use(getAvailableProvider)
module.exports = app
