/* eslint-disable no-unneeded-ternary */
/* eslint-disable camelcase */
/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
const Router = require('express').Router
const router = new Router()
const model = require('../../models')
const errorModule = require('../../models').Errors
const serviceModule = require('../../models').CommonServices
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const _ = require('lodash')
const moment = require('moment')

router.get('/get-patientbyid/:PT_HS_MRN', authorize(Role.Provider), async (req, res) => {
  var { params: { PT_HS_MRN } } = req
  if (!PT_HS_MRN) return res.send({ code: 0, status: 'failed', message: 'PT_HS_MRN is required', Error_Code: errorModule.PT_HS_MRN_required() })
  let data = {}
  data.PRO_UID = req.user.PRO_UID
  let patient = await model.Patient.getPatientByHSId(PT_HS_MRN)
  if (patient.length == 0) return res.send({ code: 0, status: 'failed', message: `Patient with ${PT_HS_MRN} is not exist in system`, Error_Code: errorModule.patient_not_found() })
  let patient_history = await model.PatientHistory.findByUserId(PT_HS_MRN)
  patient[0].patient_history = patient_history ? patient_history : null
  if (patient.length > 0) {
    var Patient_Info = patient.map(ele => ({
      PT_HS_MRN: ele.PT_HS_MRN,
      PT_First_Name: ele.PT_First_Name ? ele.PT_First_Name : null,
      PT_Username: ele.PT_Username ? ele.PT_Username : null,
      PT_Last_Name: ele.PT_Last_Name ? ele.PT_Last_Name : null,
      PT_Age_Now: ele.PT_DOB ? new Date().getFullYear() - ele.PT_DOB.getFullYear() : null,
      PT_Gender: ele.PT_Gender ? ele.PT_Gender : null,
      PT_Home_Address_Latitude: ele.PT_Home_Address_Latitude ? ele.PT_Home_Address_Latitude : null,
      PT_Home_Address_Longitude: ele.PT_Home_Address_Longitude ? ele.PT_Home_Address_Longitude : null,
      PT_Home_City: ele.PT_Home_City ? ele.PT_Home_City : null,
      PT_Home_State: ele.PT_Home_State ? ele.PT_Home_State : null,
      PT_Home_Zip: ele.PT_Home_Zip ? ele.PT_Home_Zip : null,
      PT_Home_Address_Line_1: ele.PT_Home_Address_Line_1 ? ele.PT_Home_Address_Line_1 : null,
      PT_Home_Address_Line_2: ele.PT_Home_Address_Line_2 ? ele.PT_Home_Address_Line_2 : null,
      PT_Cell_Phone: ele.PT_Cell_Phone ? ele.PT_Cell_Phone : null,
      PT_DOB: ele.PT_DOB ? moment(new Date(ele.PT_DOB)).format('MM-DD-YYYY') : null,
      PT_Is_Minor: ele.PT_Is_Minor ? ele.PT_Is_Minor : false,
      PT_Emergency_Contact_Name: ele.PT_Emergency_Contact_Name ? ele.PT_Emergency_Contact_Name : null,
      PT_Emergency_Contact_Phone: ele.PT_Emergency_Contact_Phone ? ele.PT_Emergency_Contact_Phone : null,
      PT_Medical_Directive: ele.patient_history ? ele.patient_history.PT_Medical_Directive : null,
      PT_Organ_Donor: ele.patient_history ? ele.patient_history.PT_Organ_Donor : 'No',
      PT_Height: ele.PT_Height ? ele.PT_Height : 0,
      PT_Weight: ele.PT_Weight ? ele.PT_Weight : 0,
      PT_Primary_Care_Provider: ele.patient_history ? ele.patient_history.PT_Primary_Care_Provider : null,
      PT_Allergies: ele.patient_history ? ele.patient_history.PT_Allergies : []
    }))
    if (Patient_Info[0].PT_Is_Minor == 'Yes') {
      let patientGuardian = await model.Patient.findById(patient[0].PT_Guardian_Name)
      Patient_Info[0].PT_Guardian_Name = `${patientGuardian.PT_First_Name} ${patientGuardian.PT_Last_Name}`
    } else {
      Patient_Info[0].PT_Guardian_Name = null
    }
    Patient_Info[0].PT_Profile_Picture = patient[0].PT_Profile_Picture ? await serviceModule.getImage(patient[0].PT_Profile_Picture) : null
    data.Patient_Info = Patient_Info[0]
    let patientProblem = await model.PatientProblem.findPatientProblemByPT_Id(patient[0].PT_HS_MRN)
    if (patientProblem.length > 0) {
      patientProblem = await sortJson(patientProblem)
      let PT_Problem = patientProblem.map(ele => ({
        PT_Problem_Created_By: ele.PT_Problem_Created_First_Name + ' ' + ele.PT_Problem_Created_Last_Name,
        PT_Problem_Created_Timestamp: ele.PT_Problem_Created_Timestamp ? moment(new Date(ele.PT_Problem_Created_Timestamp)).format('MM-DD-YYYY') : null,
        PT_Problem_Last_Modified_By: ele.PT_Problem_Last_Modified_By ? ele.PT_Problem_Last_Modified_By : null,
        PT_Problem_Resolved_By: ele.PT_Problem_Resolved_By ? ele.PT_Problem_Resolved_By : null,
        PT_Problem_Status: ele.PT_Problem_Status ? ele.PT_Problem_Status : null,
        PT_Username: ele.PT_Username ? ele.PT_Username : null,
        PRO_UID: ele.PRO_UID ? ele.PRO_UID : null,
        PT_PRO_UID: ele.PT_PRO_UID ? ele.PT_PRO_UID : null,
        PT_Problem_Last_Modified_Timestamp: ele.PT_Problem_Last_Modified_Timestamp ? moment(new Date(ele.PT_Problem_Last_Modified_Timestamp)).format('MM-DD-YYYY') : null,
        PT_Problem_Description: ele.PT_Problem_Description ? ele.PT_Problem_Description : null,
        PT_Problem_Resolved_Timestamp: ele.PT_Problem_Resolved_Timestamp ? moment(new Date(ele.PT_Problem_Resolved_Timestamp)).format('MM-DD-YYYY') : null,
        Problem_Index: ele.Problem_Index ? ele.Problem_Index : null
      }))
      data.Problem_List = PT_Problem
    } else {
      data.Problem_List = []
    }

    let patient_history = patient[0].patient_history
    if (patient_history) {
      patient_history = JSON.parse(JSON.stringify(patient_history))
      let medical_history = await model.PatientHistory.getPatientMedicalHistory(PT_HS_MRN)
      if (Object.values(medical_history[0]).includes('Yes')) {
        patient_history.PT_Medical_History_Toggle = 'Yes'
      } else {
        patient_history.PT_Medical_History_Toggle = 'No'
      }
      let social_history = await model.PatientHistory.getPatientSocialHistory(PT_HS_MRN)
      if (Object.values(social_history[0]).includes('Yes')) {
        patient_history.PT_Social_History_Toggle = 'Yes'
      } else {
        patient_history.PT_Social_History_Toggle = 'No'
      }
      if (patient_history.PT_Medication_Attachment.length > 0) {
        patient_history.PT_Medication_Attachment = await serviceModule.getImageUrls(patient_history.PT_Medication_Attachment, req.user)
      }
      if (patient_history.PT_Medical_History_Attachment.length > 0) {
        patient_history.PT_Medical_History_Attachment = await serviceModule.getImageUrls(patient_history.PT_Medical_History_Attachment, req.user)
      }
      if (patient_history.PT_Medical_Directive_Attachment.length > 0) {
        patient_history.PT_Medical_Directive_Attachment = await serviceModule.getImageUrls(patient_history.PT_Medical_Directive_Attachment, req.user)
      }
      data.patient_history = patient_history
    } else {
      data.patient_history = null
    }
    let patientEncounter = await model.Encounter.getPatientsAllEncounter(patient[0].PT_HS_MRN)
    let count = 0
    let encounterArray = []
    if (patientEncounter.length > 0) {
      patientEncounter.map(async ele => {
        let chart = await model.Chart.getChartData(ele.Encounter_UID)
        if (chart) {
          ele.Chart_is_Signed = chart.Chart_is_Signed ? chart.Chart_is_Signed : false
          ele.PRO_Addendum_Note = chart.PRO_Addendum_Note ? ele.PRO_Addendum_Note : null
          ele.Addendum_Is_Signed = chart.Addendum_Is_Signed ? chart.Addendum_Is_Signed : false
          ele.Encounter_Number_of_Attachments = chart.Encounter_Number_of_Attachments ? chart.Encounter_Number_of_Attachments : 0
          ele.Encounter_Cancelled_By = ele.Encounter_Cancelled_User ? ele.Encounter_Cancelled_User == req.user.PRO_UID ? 'You' : ele.Encounter_Cancelled_By == 'Patient' ? 'Patient' : 'Provider' : null
        } else {
          ele.Chart_is_Signed = false
          ele.PRO_Addendum_Note = null
          ele.Addendum_Is_Signed = false
          ele.Encounter_Number_of_Attachments = 0
          ele.Encounter_Cancelled_By = null
        }
        ele.Encounter_Provider = ele.PRO_UID == req.user.PRO_UID ? 'You' : ele.Encounter_Provider

        ele.Encounter_Access = ele.PRO_UID == req.user.PRO_UID ? true : (ele.Encounter_Access_Provider && ele.Encounter_Access_Provider.length > 0) ? ele.Encounter_Access_Provider.find(ele => (ele.PRO_UID == req.user.PRO_UID)) ? ele.Encounter_Access_Provider.find(ele => (ele.Access_Status == 'Requested')) ? false : true : false : false

        ele.Encounter_Access_Status = (ele.Encounter_Access_Provider && ele.Encounter_Access_Provider.length > 0) ? ele.Encounter_Access_Provider.find(ele => (ele.PRO_UID == req.user.PRO_UID)) ? ele.Encounter_Access_Provider.find(ele => (ele.Access_Status == 'Requested')) ? 'Requested' : 'Granted' : 'Request' : 'Request'
        let EncounterTime = new Date(`${ele.Encounter_Date} ${ele.Encounter_Time}`).getTime()
        ele.Encounter_Started = EncounterTime > await getTime() ? false : true

        ele.Visit_Type = ele.Encounter_Type == 'New Visit' ? '(NP)' : '(FU)'
        ele.PRO_Profile_Picture = ele.PRO_Profile_Picture != null ? await serviceModule.getImage(ele.PRO_Profile_Picture) : null
        if (ele.Encounter_End_Time.match(/[a-z]/i)) {
          if (ele.Encounter_End_Time.includes('a')) {
            ele.Encounter_End_Time = ele.Encounter_End_Time.split('a')[0]
            ele.Encounter_End_Time_Zone = 'am'
          }
          if (ele.Encounter_End_Time.includes('p')) {
            ele.Encounter_End_Time = ele.Encounter_End_Time.split('p')[0]
            ele.Encounter_End_Time_Zone = 'pm'
          }
        }
        delete ele.Encounter_Access_Provider
        encounterArray.push(ele)
        count = count + 1
        if (count == patientEncounter.length) {
          encounterArray.sort(compareAsc)
          encounterArray = _.uniqBy(encounterArray, 'Encounter_UID')
          data.Encounter = encounterArray
          await getOrderData(patient[0].PT_HS_MRN, data, req, res)
        }

      })
    } else {
      data.Encounter = []
      await getOrderData(patient[0].PT_HS_MRN, data, req, res)
    }
  } else {
    res.send({ code: 0, status: 'failed', data: [], Error_Code: 'HS152' })
  }
})

let getTime = () => {
  let d = new Date()
  var newYork = moment.tz(d, process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm:ss')
  return Date.parse(newYork)
}

let sortJson = (array) => {
  let arr = array.sort(function (a, b) {
    return (a.Problem_Index - b.Problem_Index)
  })
  return arr
}

function compareAsc (a, b) {
  if (a.Encounter_Time != null && !a.Encounter_Time.includes(':')) {
    let time = a.Encounter_Time.split(' ')
    a.Encounter_Time = `${time[0]}:00 ${time[1]}`
  }
  if (b.Encounter_Time != null && !b.Encounter_Time.includes(':')) {
    let time = b.Encounter_Time.split(' ')
    b.Encounter_Time = `${time[0]}:00 ${time[1]}`
  }
  if (a.Encounter_Time != null && !a.Encounter_Time.includes(' ')) {
    if (a.Encounter_Time.includes('a')) {
      a.Encounter_Time = `${a.Encounter_Time.split('a')[0]} am`
    }
    if (a.Encounter_Time.includes('p')) {
      a.Encounter_Time = `${a.Encounter_Time.split('p')[0]} pm`
    }
  }
  if (b.Encounter_Time != null && !b.Encounter_Time.includes(' ')) {
    if (b.Encounter_Time.includes('a')) {
      b.Encounter_Time = `${b.Encounter_Time.split('a')[0]} am`
    }
    if (b.Encounter_Time.includes('p')) {
      b.Encounter_Time = `${b.Encounter_Time.split('p')[0]} pm`
    }
  }
  return new Date(b.Encounter_Date + ' ' + b.Encounter_Time).getTime() - new Date(a.Encounter_Date + ' ' + a.Encounter_Time).getTime()
}
let getOrderData = async (PT_HS_MRN, data, req, res) => {
  let orders = await model.Order.getPatientOrder(PT_HS_MRN)
  if (orders.length > 0) {
    let updatedOrders = orders.map(ele => ({
      Order_UID: ele.Order_UID ? ele.Order_UID : null,
      Order_Complete: ele.Order_Complete ? ele.Order_Complete : null,
      Order_Reminder: ele.Order_Reminder ? ele.Order_Reminder == 'Checked' ? 'Checked' : 'Unchecked' : null,
      Order_Created_Time: ele.Order_Created_Time ? ele.Order_Created_Time : null,
      Order_Type: ele.Order_Type ? ele.Order_Type : null,
      Order_Created_Date: ele.Order_Created_Date ? moment(new Date(ele.Order_Created_Date)).format('MM-DD-YYYY') : null,
      Order_Review_Status: ele.Order_Review_Status ? ele.Order_Review_Status : 'Review',
      Order_As_Addendum: ele.Order_As_Addendum ? ele.Order_As_Addendum : false,
      Order_Text: ele.Order_Text ? ele.Order_Text : null,
      Order_Status: ele.Order_Status ? ele.Order_Status : null,
      Order_Date_Marked_Complete: ele.Order_Date_Marked_Complete ? ele.Order_Date_Marked_Complete : null,
      Reminder_Start_Date: ele.Reminder_Start_Date ? moment(new Date(ele.Reminder_Start_Date)).format('MM-DD-YYYY') : null,
      Reminder_Date: ele.Reminder_Date ? moment(new Date(ele.Reminder_Date)).format('MM-DD-YYYY') : null,
      Encounter_Time: ele.Encounter_Time ? ele.Encounter_Time.includes(' ') ? ele.Encounter_Time : ele.Encounter_Time.slice(0, -2) + ' ' + ele.Encounter_Time.slice(-2) : null,
      Encounter_Date: ele.Encounter_Date ? ele.Encounter_Date : null,
      Encounter_Provider: ele.PRO_UID == req.user.PRO_UID ? 'You' : ele.Encounter_Provider,
      Encounter_UID: ele.Encounter_UID ? ele.Encounter_UID : null,
      Encounter_Type: ele.Encounter_Type ? ele.Encounter_Type : null,
      Encounter_Chief_Complaint_PRO: ele.Encounter_Chief_Complaint_PRO ? ele.Encounter_Chief_Complaint_PRO : null,
      Encounter_Chief_Complaint_PT: ele.Encounter_Chief_Complaint_PT ? ele.Encounter_Chief_Complaint_PT : null,
      Chart_is_Signed: ele.Chart_is_Signed ? ele.Chart_is_Signed : false
    }))
    updatedOrders = _.uniqBy(updatedOrders, 'Order_UID')
    updatedOrders.sort(compareDsc)
    data.Orders = updatedOrders
  } else {
    data.Orders = []
  }

  res.send({ code: 1, status: 'sucess', data: data })
}

function compareDsc (a, b) {
  if (new Date(b.Encounter_Date + ' ' + b.Encounter_Time) < new Date(a.Encounter_Date + ' ' + a.Encounter_Time)) {
    return -1
  }
  if (new Date(b.Encounter_Date + ' ' + b.Encounter_Time) > new Date(a.Encounter_Date + ' ' + a.Encounter_Time)) {
    return 1
  }
  return 0
}
module.exports = router
