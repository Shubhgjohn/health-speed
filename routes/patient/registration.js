/* eslint-disable prefer-const */
/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const patientModule = require('../../models').Patient
const errorModule = require('../../models').Errors
const patientHistoryModule = require('../../models').PatientHistory
const auditModule = require('../../models').Audit
const stripeModule = require('../../models').Stripe
const { bcryptHash } = require('../../utils/crypto')
const { auditUid } = require('../../utils/UID')
var auditType = require('../../auditType.json')
const jwt = require('jsonwebtoken')
const moment = require('moment')
var randomize = require('randomatic')

// Service for register a new user as patient //
router.post('/register', async (req, res, next) => { 
  try {
    var { body: { PT_Username, PT_Password } } = req
    if (!req.body.PT_Is_Minor) return res.send({ code: 0, status: 'failed', message: 'PT_Is_Minor required!'})
    if (req.body.PT_Is_Minor != 'No' && req.body.PT_Is_Minor != 'Yes') return res.send({ code: 0, status: 'failed', message: 'PT_Is_Minor value is missing.'})
    if (req.body.PT_Is_Minor == 'No' && !PT_Username) return res.send({ code: 0, status: 'failed', message: 'Don’t forget to enter your email! It’s required to sign up.', Error_Code: errorModule.enter_your_email() })
    if (req.body.PT_Is_Minor == 'No' && !PT_Password) return res.send({ code: 0, status: 'failed', message: 'Don’t forget to enter your password! It’s required to sign up.', Error_Code: errorModule.enter_your_password() })
    if (req.body.PT_Is_Minor == 'No') req.body.PT_Username = PT_Username ? PT_Username.toLowerCase() : null
    var reg = /^(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$#!%^*?&]{10,45}$/
    if (PT_Password && !reg.test(PT_Password)) {
      return res.send({ code: 0, status: 'failed', message: '*Please make sure the password meets all of the criteria below.', Error_Code: errorModule.make_sure_criteria() })
    }
    req.body.PT_HX_Last_Updated_About_Section = moment(new Date()).format('lll')
    req.body.PT_HX_Last_Updated_General_Section = moment(new Date()).format('lll')
    req.body.PT_HX_Last_Updated_Medications_Section = moment(new Date()).format('lll')
    req.body.PT_HX_Last_Updated_Other_Section = moment(new Date()).format('lll')
    req.body.PT_HX_Last_Updated_OB_Section = moment(new Date()).format('lll')
    if (req.body.PT_Is_Minor == 'No') req.body.PT_Biometric_Login = req.body.isEnableBiomatric == true ? 'Enabled' : 'Disabled'
    var allPatient = await patientModule.find()
    if (req.body.PT_Is_Minor == 'Yes') {
      if (allPatient.length == 0) {
        let number = randomize('0', 12)
        let patternNumber = number.match(/.{1,4}/g)
        let id = patternNumber.join('-')
        req.body.PT_HS_MRN = `HS-${id}`
      } else {
        let nextId = await getNextId()
        req.body.PT_HS_MRN = nextId
      }
      req.body.Created_At = new Date()
      req.body.PT_Age_Now = req.body.PT_DOB ? parseInt(moment(new Date()).format('YYYY')) - parseInt(moment(new Date(req.body.PT_DOB)).format('YYYY')) : null
      if (!req.headers.authorization) return res.send({ code: 0, status: 'failed', message: 'authorization token is required!', Error_Code: 'HS171' })
      var decoded = jwt.verify(req.headers.authorization.split(' ')[1], process.env.JWT_SECRET)
      if (decoded) {
        let patient = await patientModule.findOneByEmail(decoded.PT_Username)
        if (req.body.PT_DOB) req.body.PT_DOB = new Date(req.body.PT_DOB)
        req.body.PT_Guardian_Name = patient.PT_HS_MRN
        req.body.PT_Cell_Phone = patient.PT_Cell_Phone
        req.body.PT_Home_Address_Line_1 = patient.PT_Home_Address_Line_1
        req.body.PT_Home_Address_Line_2 = patient.PT_Home_Address_Line_2
        req.body.PT_Home_Address_Latitude = patient.PT_Home_Address_Latitude
        req.body.PT_Home_Address_Longitude = patient.PT_Home_Address_Longitude
        req.body.PT_Home_City = patient.PT_Home_City
        req.body.PT_Home_State = patient.PT_Home_State
        req.body.PT_Home_Zip = patient.PT_Home_Zip
        delete req.body.PT_Password
      } else {
        return res.send({ code: 0, status: 'failed', message: 'Unauthorized User!', Error_Code: 'HS172' })
      }
    }
    if (!req.body.PT_Is_Minor || req.body.PT_Is_Minor == 'No') {

      var patient = await patientModule.findOneByEmail(req.body.PT_Username)
      if (!patient.stripeCustomerId) {
        let stripeCustomer = await stripeModule.createCutomer(req.body.PT_Username)
        req.body.stripeCustomerId = stripeCustomer.id
      }
      let hashPassword = await bcryptHash(PT_Password, 10)
      req.body.PT_Password = hashPassword
      await patientModule.updatePatientByUsername(req.body.PT_Username, req.body)
      await createAudit(req, patient, req.body.PT_Is_Minor)
    } else {
      var minorPatient = await patientModule.create(req.body)
      let patientHistory = await patientHistoryModule.findByUserId(req.body.PT_HS_MRN)
      if (!patientHistory) {
        let historyData = {
          PT_HS_MRN: req.body.PT_HS_MRN
        }
        await patientHistoryModule.create(historyData)
        await createAudit(req, minorPatient, req.body.PT_Is_Minor)
      }
    }
    if (!req.body.PT_Is_Minor || req.body.PT_Is_Minor == 'No') {
      let token = getToken(patient.PT_Username, patient.PT_HS_MRN)
      return res.send({
        code: 1,
        status: 'sucess',
        data: token,
        date: moment(new Date()).format('MM-DD-YYYY')
      })
    } else {
      return res.send({
        code: 1,
        status: 'sucess',
        message: 'Minor added successfully',
        PT_HS_MRN: req.body.PT_HS_MRN
      })
    }
  } catch (err) {
    return res.send({ code: 2, status: 'failed', message: err.message, Error_Code: 'HS174' })
  }
})
// Function for generate unique id for patient //
async function getNextId () {
  let number = randomize('0', 12)
  let patternNumber = number.match(/.{1,4}/g)
  let id = patternNumber.join('-')
  let PT_HS_MRN = `HS-${id}`
  let patient = await patientModule.getPatientByHSId(PT_HS_MRN)
  if (patient.length > 0) {
    getNextId()
  } else {
    return PT_HS_MRN
  }
}
// Function for generate token //
function getToken (email, id) {
  const tokenPayload = {
    PT_HS_MRN: id,
    PT_Username: email,
    roles: 'Patient'
  }
  const token = jwt.sign(tokenPayload, process.env.JWT_SECRET, { expiresIn: 60 * 60 * 24 })
  return token
}
// function for create audit log //
async function createAudit (req, newpatient, isMinor) {
  let auditId = await auditUid()
  const auditData = {
    Audit_UID: auditId,
    Audit_Type: auditType.add,
    Data_Point: isMinor == 'No' ? 'PT/Signup' : 'PT/add minor',
    Audit_Info: 'new- ' + auditId,
    New_Value: newpatient._doc,
    IP_Address: await auditModule.getIpAddress(req)
  }
  await auditModule.create(auditData)
}

module.exports = router
