/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
/* eslint-disable prefer-const */
const Router = require('express').Router
const router = new Router()
const errorModule = require('../../models').Errors
const patientModule = require('../../models').Patient
const encounterModule = require('../../models').Encounter
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const moment = require('moment')
const momentTz = require('moment-timezone')

router.put('/share-past-chart/:PT_HS_MRN', authorize(Role.Patient), async (req, res) => {
	let PT_HS_MRN = req.params.PT_HS_MRN
	let PRO_UID = req.body.PRO_UID
	let PT_Share_Records = req.body.PT_Share_Records
	if (!PT_Share_Records) return res.send({ code: 0, status: 'failed', message: 'PT_Share_Records required!', Error_Code: 'HS175' })
	if (!PRO_UID) return res.send({ code: 0, status: 'failed', message: 'PRO_UID required!', Error_Code: errorModule.PRO_UID_required() })
	if (!PT_HS_MRN) return res.send({ code: 0, status: 'failed', message: 'PT_HS_MRN required!', Error_Code: errorModule.PT_HS_MRN_required() })
	let patient = await patientModule.findById(PT_HS_MRN)
	if (!patient) return res.send({ code: 0, status: 'failed', message: `patient with id '${PT_HS_MRN}' not found!`, Error_Code: errorModule.patient_not_found() })
	// let charts = await chartModule.findChartByPT_HS_MRN(PT_HS_MRN)
	let encounters = await encounterModule.getPatientEncounter(patient.PT_Username)
	if (encounters.length > 0) {
		var pastVisit = encounters.filter(ele => Date.parse(moment(new Date(`${ele.Encounter_Date} ${ele.Encounter_Time}`)).format('YYYY-MM-DDTHH:mm:ss')) < Date.parse(momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm:ss')))
	}
	let total_encounter = []
	let requested_encounter = []
	if (pastVisit.length > 0) {
		pastVisit.map(ele => {
			if (ele.Encounter_Access_Provider.find(item => item.PRO_UID == PRO_UID && item.Access_Status == 'Requested')) {
				requested_encounter.push(ele.Encounter_UID)
			} else {
				total_encounter.push(ele.Encounter_UID)
			}
		})
	}
	if (total_encounter.length > 0) {
		let data = {
			Encounter_Access_Provider: {
				PRO_UID: PRO_UID,
				Access_Status: 'Granted'
			}
		}
		await encounterModule.shareMultiChart(requested_encounter, PRO_UID)
		await encounterModule.shareAllPastChart(total_encounter, data.Encounter_Access_Provider)
	}
	await patientModule.updatePatientById(PT_HS_MRN, { PT_Share_Records: PT_Share_Records })
	res.send({ code: 1, status: 'success', message: 'Charts shared successfully.' })
})

router.put('/share-chart/:PT_HS_MRN', authorize(Role.Patient), async (req, res) => {
	let PT_HS_MRN = req.params.PT_HS_MRN
	let PRO_UID = req.body.PRO_UID
	let Encounter_UID = req.body.Encounter_UID
	let chart_Access = req.body.chart_Access
	if (!PT_HS_MRN) return res.send({ code: 0, status: 'failed', message: 'PT_HS_MRN required!', Error_Code: errorModule.PT_HS_MRN_required() })
	if (!PRO_UID) return res.send({ code: 0, status: 'failed', message: 'PRO_UID required!', Error_Code: errorModule.PRO_UID_required() })
	if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID required!', Error_Code: errorModule.Encounter_UID_required() })
	if (chart_Access == false) {
		await encounterModule.deniedChartAccess(Encounter_UID, PRO_UID)
		return res.send({ code: 1, status: 'success', message: 'Chart access denied.' })
	}

	await encounterModule.shareChart(Encounter_UID, PRO_UID)
	// await chartModule.ChartAccess(Encounter_UID, PRO_UID)
	return res.send({ code: 1, status: 'success', message: 'Chart shared successfully.' })
})

module.exports = router
