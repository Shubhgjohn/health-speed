/* eslint-disable quotes */
/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
var Router = require('express').Router
var router = new Router()
var moment = require('moment')
const moments = require('moment-timezone')
const jwt = require('jsonwebtoken')
var sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_PRIVATE_KEY)
const patientModule = require('../../models').Patient
const patientHxModule = require('../../models').PatientHistory
const auditModule = require('../../models').Audit
const stripeModule = require('../../models').Stripe
const errorModule = require('../../models').Errors
const encounterModule = require('../../models').Encounter
const otherModule = require('../../models').Other
const serviceModule = require('../../models').CommonServices
var { auditUid } = require('../../utils/UID')
var auditType = require('../../auditType.json')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const { bcryptHash } = require('../../utils/crypto')

// Service for request reset password for patient //

router.post('/forgot_password', async (req, res) => {
	sgMail.setApiKey(process.env.SENDGRID_PRIVATE_KEY)
	const { body: { PT_Username } } = req


	if (!PT_Username) return res.send({ code: 0, status: 'failed', message: `Don't forget to enter your email address! It's required to reset your password`, Error_Code: errorModule.enter_your_email() })
	try {
		let patient = await patientModule.findOneByEmail(PT_Username)
		if (!patient) {
			const msg = {
				to: process.env.ADMIN_TO_EMAIL,
				from: process.env.ADMIN_FROM_EMAIL,
				template_id: process.env.PATIENT_FORGET_PASSWORD_FAILURE_TEMPLATE,
				dynamic_template_data: {
					date: moment(new Date()).format('MM/DD/YY'),
					time: moments.tz(new Date(), process.env.CURRENT_TIMEZONE).format('HH:mm:ss'),
					email: PT_Username
				}
			}
			sgMail.send(msg)
			return res.send({ code: 0, status: 'failed', message: `*Hum… We didn't find a record for this email address. Please check it and try again`, Error_Code: errorModule.patient_valid_email() })
		} else {
			try {
				const tokenPayload = {
					PT_Username: patient.PT_Username
				}
				const token = patient.PT_Password ? jwt.sign(tokenPayload, patient.PT_Password, { expiresIn: '24h' }) : jwt.sign(tokenPayload, process.env.JWT_SECRET, { expiresIn: '24h' })
				const url = `${process.env.API_HOST}/patient/reset/${patient.PT_HS_MRN}/${token}`
				const msg = {
					to: patient.PT_Username,
					from: process.env.ADMIN_FROM_EMAIL,
					template_id: process.env.PATIENT_FORGOT_PASSWORD_TEMPLATE,
					dynamic_template_data: {
						date: moment(new Date()).format('MM/DD/YY'),
						time: moments.tz(new Date(), process.env.CURRENT_TIMEZONE).format('HH:mm:ss'),
						email: patient.PT_Username,
						contact: process.env.HELPDESK_EMAIL,
						phone: process.env.CONTACT,
						url: url
					}
				}
				sgMail.send(msg)
				var auditData = {
					Audit_UID: await auditUid(),
					Audit_Type: auditType.confirm,
					Data_Point: 'PT/Forget Password',
					Audit_Info: 'new- ' + await auditUid(),
					New_Value: patient.PT_Username,
					PT_First_Name: patient.PT_First_Name,
					PT_Last_Name: patient.PT_Last_Name,
					PT_HSID: patient.PT_HS_MRN,
					IP_Address: await auditModule.getIpAddress(req)
				}
				await auditModule.create(auditData)
				return res.send({
					code: 1,
					status: 'sucess',
					message: `We've sent you an email with instructions.`
				})
			} catch (err) {
				return res.send({ code: 0, status: 'failed', message: err.message, Error_Code: 'HS158' })
			}
		}
	} catch (err) {
		return res.send({ code: 2, status: 'failed', message: err.message, Error_Code: 'HS159' })
	}
})

// Service for reset Patient password //
router.post('/reset_password', async (req, res) => {
	const { body: { PT_HS_MRN, token, PT_New_Password } } = req
	if (!PT_HS_MRN) return res.send({ code: 0, status: 'failed', message: 'PT_HS_MRN required!', Error_Code: errorModule.PT_HS_MRN_required() })
	if (!token) return res.send({ code: 0, status: 'failed', message: 'Reset token is missing with the request', Error_Code: 'HS160' })
	if (!PT_New_Password) return res.send({ code: 0, status: 'failed', message: 'PT_New_Password required', Error_Code: errorModule.PT_New_Password_required() })
	var reg = /^(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$#!%^*?&]{10,45}$/
	if (!reg.test(PT_New_Password)) {
		return res.send({ code: 0, status: 'failed', message: '*Please make sure the criteria below are met and try again', Error_Code: errorModule.make_sure_criteria() })
	}
	try {
		var patientData = await patientModule.findById(PT_HS_MRN)
		if (patientData) {
			let hashPassword = patientData.PT_Password ? patientData.PT_Password : process.env.JWT_SECRET
			jwt.verify(token, hashPassword, async function (err, decoded) {
				if (err) return res.send({ code: 0, status: 'failed', message: 'token is not valid!', Error_Code: 'HS163' })
				if (decoded) {
					var newHashPassword = bcryptHash(PT_New_Password, 10)
					await patientModule.updatePasswordByEmail(decoded.PT_Username, newHashPassword)
					var ip_add = await auditModule.getIpAddress(req)
					var UID = await auditUid()
					var auditData = {
						Audit_UID: UID,
						Audit_Type: auditType.change,
						Data_Point: 'PT/Reset Password',
						Audit_Info: 'new- ' + UID,
						New_Value: decoded.PT_Username,
						PT_First_Name: patientData.PT_First_Name,
						PT_Last_Name: patientData.PT_Last_Name,
						PT_HSID: patientData.PT_HS_MRN,
						IP_Address: ip_add
					}
					await auditModule.create(auditData)
					return res.send({
						code: 1,
						status: 'Sucess',
						PT_Username: patientData.PT_Username,
						message: 'PT_Password is changed sucessfull'
					})
				} else {
					return res.send({ code: 0, status: 'failed', message: 'Request token is wrong', Error_Code: 'HS164' })
				}
			})
		} else {
			return res.send({
				code: 0,
				status: 'failed',
				message: 'Patient with this PT_HS_MRN is not available in the system',
				Error_Code: 'HS165'
			})
		}
	} catch (err) {
		return res.send({ code: 2, status: 'failed', message: err.message, Error_Code: 'HS166' })
	}
})
// Get patient and patient history //
router.get('/get-patient/:PT_HS_MRN', authorize(Role.Patient), async (req, res) => {
	try {
		let PT_HS_MRN = req.params.PT_HS_MRN
		var data = {}
		if (!PT_HS_MRN) return res.send({ code: 0, status: 'failed', message: 'PT_HS_MRN is required', Error_Code: errorModule.PT_HS_MRN_required() })
		const patient = await patientModule.findById(PT_HS_MRN)
		const patient_history = await patientHxModule.findByUserId(PT_HS_MRN)
		if (!patient) return res.send({ code: 0, status: 'failed', message: `patient with id '${PT_HS_MRN}' is not found!`, Error_Code: errorModule.patient_not_found() })
		data.about = {
			PT_HS_MRN: patient.PT_HS_MRN,
			PT_Is_Minor: patient.PT_Is_Minor ? patient.PT_Is_Minor : null,
			PT_Profile_Picture: patient.PT_Profile_Picture ? await serviceModule.getImage(patient.PT_Profile_Picture) : null,
			PT_First_Name: patient.PT_First_Name ? patient.PT_First_Name : null,
			PT_Last_Name: patient.PT_Last_Name ? patient.PT_Last_Name : null,
			PT_Gender: patient.PT_Gender ? patient.PT_Gender : null,
			PT_Home_Address_Latitude: patient.PT_Home_Address_Latitude ? patient.PT_Home_Address_Latitude : null,
			PT_Home_Address_Longitude: patient.PT_Home_Address_Longitude ? patient.PT_Home_Address_Longitude : null,
			PT_Home_City: patient.PT_Home_City ? patient.PT_Home_City : null,
			PT_Home_State: patient.PT_Home_State ? patient.PT_Home_State : null,
			PT_Home_Zip: patient.PT_Home_Zip ? patient.PT_Home_Zip : null,
			PT_Home_Address_Line_1: patient.PT_Home_Address_Line_1 ? patient.PT_Home_Address_Line_1 : null,
			PT_Home_Address_Line_2: patient.PT_Home_Address_Line_2 ? patient.PT_Home_Address_Line_2 : null,
			PT_Cell_Phone: patient.PT_Cell_Phone ? patient.PT_Cell_Phone : null,
			PT_DOB: patient.PT_DOB ? moment(new Date(patient.PT_DOB)).format('MM-DD-YYYY') : null,
			PT_Emergency_Contact_Name: patient.PT_Emergency_Contact_Name ? patient.PT_Emergency_Contact_Name : null,
			PT_Emergency_Contact_Phone: patient.PT_Emergency_Contact_Phone ? patient.PT_Emergency_Contact_Phone : null,
			PT_Medical_Directive: patient.patient_history ? patient.patient_history.PT_Medical_Directive : null,
			PT_Organ_Donor: patient.PT_Organ_Donor ? patient.PT_Organ_Donor : 'No',
			PT_Height: patient.PT_Height ? patient.PT_Height : null,
			PT_Weight: patient.PT_Weight ? patient.PT_Weight : null,
			PT_HX_Last_Updated_About_Section: patient.PT_HX_Last_Updated_About_Section ? moment(new Date(patient.PT_HX_Last_Updated_About_Section)).format('M/D/YY') : null,
			PT_About_Update_Toggle: patient.PT_About_Update_Toggle ? patient.PT_About_Update_Toggle : null,
			PT_Hx_About_Confirmation: patient_history ? patient_history.PT_Hx_About_Confirmation : 'Unchecked',
			Created_At: moment(patient.Created_At).format('MM/DD/YYYY')
		}
		data.general = {
			PT_Primary_Care_Provider_Toggle: patient_history ? patient_history.PT_Primary_Care_Provider_Toggle : 'No',
			PT_Primary_Care_Provider: patient_history ? patient_history.PT_Primary_Care_Provider : null,
			PT_Past_Year_Medical_Changes: patient_history ? patient_history.PT_Past_Year_Medical_Changes : 'No',
			PT_Past_Year_Medical_Changes_Note: patient_history ? patient_history.PT_Past_Year_Medical_Changes_Note : null,
			PT_Heart_Disease: patient_history ? patient_history.PT_Heart_Disease : 'No',
			PT_Heart_Disease_Note: patient_history ? patient_history.PT_Heart_Disease_Note : null,
			PT_Hypertension: patient_history ? patient_history.PT_Hypertension : 'No',
			PT_Hypertension_Note: patient_history ? patient_history.PT_Hypertension_Note : null,
			PT_Diabetes: patient_history ? patient_history.PT_Diabetes : 'No',
			PT_Diabetes_Note: patient_history ? patient_history.PT_Diabetes_Note : null,
			PT_Cancer: patient_history ? patient_history.PT_Cancer : 'No',
			PT_Cancer_Note: patient_history ? patient_history.PT_Cancer_Note : null,
			PT_Immune_Diseases: patient_history ? patient_history.PT_Immune_Diseases : 'No',
			PT_Immune_Diseases_Note: patient_history ? patient_history.PT_Immune_Diseases_Note : null,
			PT_Surgery: patient_history ? patient_history.PT_Surgery : 'No',
			PT_Surgery_Note: patient_history ? patient_history.PT_Surgery_Note : null,
			PT_Neuro_Issues: patient_history ? patient_history.PT_Neuro_Issues : 'No',
			PT_Neuro_Issues_Note: patient_history ? patient_history.PT_Neuro_Issues_Note : null,
			PT_Other_Diseases: patient_history ? patient_history.PT_Other_Diseases : 'No',
			PT_Other_Diseases_Note: patient_history ? patient_history.PT_Other_Diseases_Note : null,
			PT_Family_History_Info: patient_history ? patient_history.PT_Family_History_Info : null,
			PT_Lab_and_Image_Reports_Toggle: patient_history ? patient_history.PT_Lab_and_Image_Reports_Toggle : 'No',
			PT_Lab_and_Image_Reports: patient_history ? (patient_history.PT_Lab_and_Image_Reports != null && patient_history.PT_Lab_and_Image_Reports.length > 0) ? await serviceModule.getFileUrl(patient_history.PT_Lab_and_Image_Reports) : [] : [],
			PT_Medical_Directives_Toggle: patient_history ? patient_history.PT_Medical_Directives_Toggle : 'No',
			PT_Medical_Directive: patient_history ? patient_history.PT_Medical_Directive : null,
			PT_Medical_Directive_Attachment: patient_history ? (patient_history.PT_Medical_Directive_Attachment != null && patient_history.PT_Medical_Directive_Attachment.length > 0) ? await serviceModule.getFileUrl(patient_history.PT_Medical_Directive_Attachment) : [] : [],
			PT_HX_Last_Updated_General_Section: patient.PT_HX_Last_Updated_General_Section ? moment(new Date(patient.PT_HX_Last_Updated_General_Section)).format('M/D/YY') : null,
			PT_General_Update_Toggle: patient.PT_General_Update_Toggle ? patient.PT_General_Update_Toggle : null,
			PT_Hx_General_Confirmation: patient_history ? patient_history.PT_Hx_General_Confirmation : 'Unchecked',
			PT_Medical_History_Attachment: patient_history ? (patient_history.PT_Medical_History_Attachment != null && patient_history.PT_Medical_History_Attachment.length > 0) ? await serviceModule.getFileUrl(patient_history.PT_Medical_History_Attachment) : [] : []
		}
		data.medications = {
			PT_Medications_Toggle: patient_history ? patient_history.PT_Medications_Toggle : 'No',
			PT_Medication: patient_history ? patient_history.PT_Medication : [],
			PT_Medication_Attachment: patient_history ? (patient_history.PT_Medication_Attachment != null && patient_history.PT_Medication_Attachment.length > 0) ? await serviceModule.getFileUrl(patient_history.PT_Medication_Attachment) : [] : [],
			PT_HX_Last_Updated_Medications_Section: patient.PT_HX_Last_Updated_Medications_Section ? moment(new Date(patient.PT_HX_Last_Updated_Medications_Section)).format('M/D/YY') : null,
			PT_Medicaitons_Update_Toggle: patient.PT_Medicaitons_Update_Toggle ? patient.PT_Medicaitons_Update_Toggle : null,
			PT_Hx_Medications_Confirmation: patient_history ? patient_history.PT_Hx_Medications_Confirmation : 'Unchecked'
		}
		data.other = {
			PT_Allergies_Toggle: patient_history ? patient_history.PT_Allergies_Toggle : 'No',
			PT_Allergies: patient_history ? patient_history.PT_Allergies : null,
			PT_First_Name: patient.PT_First_Name,
			PT_Recreational_Drugs: patient_history ? patient_history.PT_Recreational_Drugs : 'No',
			PT_Recreational_Drugs_Note: patient_history ? patient_history.PT_Recreational_Drugs_Note : null,
			PT_Tobacco_Usage: patient_history ? patient_history.PT_Tobacco_Usage : 'No',
			PT_Tobacco_Usage_Note: patient_history ? patient_history.PT_Tobacco_Usage_Note : null,
			PT_Alcohol_Usage: patient_history ? patient_history.PT_Alcohol_Usage : 'No',
			PT_Alcohol_Usage_Note: patient_history ? patient_history.PT_Alcohol_Usage_Note : null,
			PT_Occupation: patient_history ? patient_history.PT_Occupation : null,
			PT_HX_Last_Updated_Other_Section: patient.PT_HX_Last_Updated_Other_Section ? moment(new Date(patient.PT_HX_Last_Updated_Other_Section)).format('M/D/YY') : null,
			PT_Other_Update_Toggle: patient.PT_Other_Update_Toggle ? patient.PT_Other_Update_Toggle : null,
			PT_Hx_Other_Confirmation: patient_history ? patient_history.PT_Hx_Other_Confirmation : 'Unchecked'
		}
		if (req.user.PT_Gender == 'Female') {
			data.obgyn = {
				PT_Last_Menstrual_Cycle_Date: patient_history ? patient_history.PT_Last_Menstrual_Cycle_Date : null,
				Pregnancy_History: patient_history ? patient_history.Pregnancy_History : 'No',
				PT_Pregnancy_Complications: patient_history ? patient_history.PT_Pregnancy_Complications : 'No',
				PT_Pregnancy_Complications_Note: patient_history ? patient_history.PT_Pregnancy_Complications_Note : null,
				PT_Number_Of_Pregnancies: patient_history ? patient_history.PT_Number_Of_Pregnancies : 0,
				PT_Number_Of_Deliveries: patient_history ? patient_history.PT_Number_Of_Deliveries : 0,
				Pregnant_Now: patient_history ? patient_history.Pregnant_Now : 'No',
				Expected_Delivery_Date: patient_history ? patient_history.Expected_Delivery_Date : null,
				Nursing_Now: patient_history ? patient_history.Nursing_Now : 'No',
				Oral_Contraceptives: patient_history ? patient_history.Oral_Contraceptives : 'No',
				Oral_Contraceptives_Info: patient_history ? patient_history.Oral_Contraceptives_Info : null,
				PT_HX_Last_Updated_OB_Section: patient.PT_HX_Last_Updated_OB_Section ? moment(new Date(patient.PT_HX_Last_Updated_OB_Section)).format('M/D/YY') : null,
				PT_OBGYN_Update_Toggle: patient.PT_OBGYN_Update_Toggle ? patient.PT_OBGYN_Update_Toggle : null,
				PT_Hx_OB_Confirmation: patient_history ? patient_history.PT_Hx_OB_Confirmation : 'Unchecked'
			}
		} else {
			data.obgyn = {
				PT_Last_Menstrual_Cycle_Date: null,
				Pregnancy_History: null,
				PT_Pregnancy_Complications: null,
				PT_Pregnancy_Complications_Note: null,
				PT_Number_Of_Pregnancies: null,
				PT_Number_Of_Deliveries: null,
				Pregnant_Now: null,
				Expected_Delivery_Date: null,
				Nursing_Now: null,
				Oral_Contraceptives: null,
				Oral_Contraceptives_Info: null,
				PT_HX_Last_Updated_OB_Section: null,
				PT_OBGYN_Update_Toggle: null,
				PT_Hx_OB_Confirmation: null
			}
		}
		res.send({ code: 1, status: 'success', data: data })
	} catch (err) {
		res.send({ code: 2, status: 'failed', message: err.message, Error_Code: 'HS167' })
	}
})

router.get('/get-patient', authorize(Role.Patient), async (req, res) => {
	try {
		let PT_HS_MRN = req.user.PT_HS_MRN
		var data = {}
		if (!PT_HS_MRN) return res.send({ code: 0, status: 'failed', message: 'PT_HS_MRN is required', Error_Code: errorModule.PT_HS_MRN_required() })
		const patient = await patientModule.findById(PT_HS_MRN)
		if (!patient) return res.send({ code: 0, status: 'failed', message: `patient with id '${PT_HS_MRN}' is not found!`, Error_Code: errorModule.patient_not_found() })
		const settingData = await otherModule.find()
		data = {
			PT_HS_MRN: patient.PT_HS_MRN,
			PT_Is_Minor: patient.PT_Is_Minor ? patient.PT_Is_Minor : 'No',
			PT_Profile_Picture: patient.PT_Profile_Picture ? await serviceModule.getImage(patient.PT_Profile_Picture) : null,
			PT_First_Name: patient.PT_First_Name ? patient.PT_First_Name : null,
			PT_Last_Name: patient.PT_Last_Name ? patient.PT_Last_Name : null,
			PT_Gender: patient.PT_Gender ? patient.PT_Gender : null,
			PT_Home_Address_Latitude: patient.PT_Home_Address_Latitude ? patient.PT_Home_Address_Latitude : null,
			PT_Home_Address_Longitude: patient.PT_Home_Address_Longitude ? patient.PT_Home_Address_Longitude : null,
			PT_Home_City: patient.PT_Home_City ? patient.PT_Home_City : null,
			PT_Home_State: patient.PT_Home_State ? patient.PT_Home_State : null,
			PT_Home_Zip: patient.PT_Home_Zip ? patient.PT_Home_Zip : null,
			PT_Home_Address_Line_1: patient.PT_Home_Address_Line_1 ? patient.PT_Home_Address_Line_1 : null,
			PT_Home_Address_Line_2: patient.PT_Home_Address_Line_2 ? patient.PT_Home_Address_Line_2 : null,
			PT_Cell_Phone: patient.PT_Cell_Phone ? patient.PT_Cell_Phone : null,
			PT_DOB: patient.PT_DOB ? moment(new Date(patient.PT_DOB)).format('MM-DD-YYYY') : null,
			PT_Emergency_Contact_Name: patient.PT_Emergency_Contact_Name ? patient.PT_Emergency_Contact_Name : null,
			PT_Emergency_Contact_Phone: patient.PT_Emergency_Contact_Phone ? patient.PT_Emergency_Contact_Phone : null,
			PT_Medical_Directive: patient.patient_history ? patient.patient_history.PT_Medical_Directive : null,
			PT_Organ_Donor: patient.patient_history ? patient.patient_history.PT_Organ_Donor : 'No',
			PT_Height: patient.PT_Height ? patient.PT_Height : 0,
			PT_Weight: patient.PT_Weight ? patient.PT_Weight : 0,
			PT_Email_Notification: patient.PT_Email_Notification ? patient.PT_Email_Notification : 'Disabled',
			PT_Biometric_Login: patient.PT_Biometric_Login ? patient.PT_Biometric_Login : 'Disabled',
			PT_HX_Last_Updated_About_Section: patient.PT_HX_Last_Updated_About_Section ? patient.PT_HX_Last_Updated_About_Section : null,
			AP_Phone_Number: settingData[0].HS_Support_Number
		}
		let minor = await minorsDetail(req)
		data.Minors = minor
		res.send({ code: 1, status: 'sucess', data: data })
	} catch (err) {
		res.send({ code: 2, status: 'failed', data: err.message, Error_Code: 'HS168' })
	}
})

router.get('/get-account-info', authorize(Role.Patient), async (req, res) => {
	try {
		const settingData = await otherModule.find()
		let setting = {
			PT_Email_Notification: req.user.PT_Email_Notification,
			PT_Biometric_Login: req.user.PT_Biometric_Login,
			PT_Username: req.user.PT_Username,
			HS_Support_Email: settingData[0].HS_Support_Email,
			AP_Phone_Number: settingData[0].HS_Support_Number,
			PT_Privacy_Policy: settingData[0].PT_Privacy_Policy,
			PT_Privacy_Policy_PDF_Name: settingData[0].PT_Privacy_Policy_PDF_Name,
			PT_Privacy_Policy_PDF_Url: settingData[0].PT_Privacy_Policy_PDF_Name ? await serviceModule.getImage(settingData[0].PT_Privacy_Policy_PDF_Name) : null,
			PT_Terms_and_Conditions: settingData[0].PT_Terms_and_Conditions,
			PT_Terms_and_Conditions_PDF_Name: settingData[0].PT_Terms_and_Conditions_PDF_Name,
			PT_Terms_and_Conditions_PDF_Url: settingData[0].PT_Terms_and_Conditions_PDF_Url ? await serviceModule.getImage(settingData[0].PT_Terms_and_Conditions_PDF_Name) : null,
			HS_Address: settingData[0].HS_Address,
			HS_911_Disclaimer: settingData[0].HS_911_Disclaimer
		}
		let minors = await minorsDetail(req)
		if (req.user.stripeCustomerId) var paymentMethod = await stripeModule.getCardList(req.user.stripeCustomerId)
		let encounters = await encounterModule.getPatientTransactionStatus(req.user.PT_Username)
		let paidStatus = encounters.filter(ele => ele.Encounter_Transaction_Status == 'Paid')
		let unpaidStatus = encounters.filter(ele => (ele.Encounter_Status == 'Active' && ele.Encounter_Transaction_Status == 'Unpaid'))
		let paidTransaction = await encounterData(paidStatus)
		let unpaidTransaction = await encounterData(unpaidStatus)
		let data = {}
		let transaction = {}

		paidTransaction.sort(sortTransaction)
		unpaidTransaction.sort(sortTransaction)
		transaction.paidTransaction = paidTransaction
		transaction.unpaidTransaction = unpaidTransaction
		data.setting = setting
		data.fimaly = minors
		data.paymentMethod = paymentMethod ? paymentMethod.data : []
		data.transaction = transaction
		res.send({ code: 1, status: 'success', data: data })
	} catch (err) {
		return res.send({ code: 0, status: 'failed', message: err.message, Error_Code: 'HS169' })
	}
})

function sortTransaction (a, b) {
	return new Date(b.Encounter_Date + ' ' + b.Encounter_Time).getTime() - new Date(a.Encounter_Date + ' ' + a.Encounter_Time).getTime()
}

router.get('/get-privacy-policy-term-condition', async (req, res) => {
	let settingData = await otherModule.find()
	if (settingData.length == 0) return res.send({ code: 1, status: 'success', message: 'no privacy policy and term & condition define' })
	let Privacy_Policy = {
		PT_Privacy_Policy: settingData[0].PT_Privacy_Policy,
		PT_Privacy_Policy_PDF_Url: settingData[0].PT_Privacy_Policy_PDF_Name ? await serviceModule.getImage(settingData[0].PT_Privacy_Policy_PDF_Name) : null,
		PT_Terms_and_Conditions: settingData[0].PT_Terms_and_Conditions,
		PT_Terms_and_Conditions_PDF_Url: settingData[0].PT_Terms_and_Conditions_PDF_Name ? await serviceModule.getImage(settingData[0].PT_Terms_and_Conditions_PDF_Name) : null,
		HS_Support_Email: settingData[0].HS_Support_Email,
		AP_Phone_Number: settingData[0].HS_Support_Number,
		HS_Address: settingData[0].HS_Address,
		HS_911_Disclaimer: settingData[0].HS_911_Disclaimer
	}
	res.send({ code: 1, status: 'success', data: Privacy_Policy })
})

async function minorsDetail (req) {
	let minors = await patientModule.getPatientAllMinors(req.user.PT_HS_MRN)
	if (minors.length > 0) {
		return new Promise((resolve, reject) => {
			let dataArray = []
			let count = 0
			minors.map(async ele => {
				dataArray.push({
					PT_HS_MRN: ele.PT_HS_MRN,
					PT_First_Name: ele.PT_First_Name,
					PT_Last_Name: ele.PT_Last_Name,
					PT_Profile_Picture: ele.PT_Profile_Picture ? await serviceModule.getImage(ele.PT_Profile_Picture) : null,
					PT_Is_Minor: ele.PT_Is_Minor,
					Created_At: ele.Created_At,
					PT_HX_Last_Updated_About_Section: moments.tz(new Date(ele.PT_HX_Last_Updated_About_Section), 'America/Los_Angeles').format('M/D/YY hh:mm a')
				})
				count = count + 1
				if (count == minors.length) {
					dataArray.sort(compareAsc)
					resolve(dataArray)
				}
			})
		})
	} else return []
}

async function encounterData (encounters) {
	if (encounters.length > 0) {
		return new Promise((resolve, reject) => {
			let arrData = []
			let count = 0
			encounters.map(async ele => {
				arrData.push({
					Encounter_UID: ele.Encounter_UID,
					Encounter_Type: ele.Encounter_Type,
					Encounter_Date: ele.Encounter_Date,
					Encounter_Time: ele.Encounter_Time,
					Encounter_Location_Type: ele.Encounter_Location_Type,
					Encounter_Chief_Complaint_PRO: ele.Encounter_Chief_Complaint_PRO,
					Encounter_Cost: ele.Encounter_Cost,
					PRO_UID: ele.provider.PRO_UID,
					PRO_First_Name: ele.provider.PRO_First_Name,
					PRO_Last_Name: ele.provider.PRO_Last_Name,
					PRO_Gender: ele.provider.PRO_Gender,
					Payment_Card_Id: ele.Payment_Card_Id,
					PRO_Profile_Picture: ele.provider.PRO_Profile_Picture ? await serviceModule.getImage(ele.provider.PRO_Profile_Picture) : null,
					PT_First_Name: ele.patients.PT_First_Name,
					PT_Last_Name: ele.patients.PT_Last_Name,
					PT_HS_MRN: ele.patients.PT_HS_MRN,
					PT_Profile_Picture: ele.patients.PT_Profile_Picture ? await serviceModule.getImage(ele.patients.PT_Profile_Picture) : null
				})
				count = count + 1
				if (count == encounters.length) resolve(arrData)
			})
		})
	} else return []
}

function compareAsc (a, b) {
	return new Date(a.Created_At).getTime() - new Date(b.Created_At).getTime()
}

module.exports = router
