/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
var auditType = require('../../auditType.json')
const model = require('../../models')
const auditModule = require('../../models').Audit
const errorModule = require('../../models').Errors
var { auditUid } = require('../../utils/UID')
const stripeModule = require('../../models').Stripe
const patientModule = require('../../models').Patient
const patientHxModule = require('../../models').PatientHistory
var randomize = require('randomatic')
const logger = require('../../models/errorlog')

// Service for check patient exist or not at the time of registration //
router.post('/check-patient-exist', async (req, res) => {
  var { body: { PT_Username } } = req
  if (!PT_Username) return res.send({ code: 0, status: 'failed', message: '*Don’t forget to enter your email! It’s required.', Error_Code: errorModule.enter_your_email() })
  try {
    PT_Username = PT_Username.toLowerCase()
    let patient = await patientModule.findOneByEmail(PT_Username)
    const auditData = {
      Audit_UID: await auditUid(),
      Audit_Type: auditType.confirm,
      Data_Point: 'PT/Signup',
      Audit_Info: 'New- ' + await auditUid(),
      New_Value: PT_Username,
      IP_Address: await auditModule.getIpAddress(req)
    }
    await auditModule.create(auditData)
    if (patient) {
      return res.send({
        code: 1,
        status: 'success',
        isExist: true,
        message: '*This username is already associated with another account. Please log in.'
      })
    } else {
      var allPatient = await patientModule.find()
      if (allPatient.length === 0) {
        let number = randomize('0', 12)
        let patternNumber = number.match(/.{1,4}/g)
        let id = patternNumber.join('-')
        req.body.PT_HS_MRN = `HS-${id}`
      } else {
        let nextId = await getNextId()
        req.body.PT_HS_MRN = nextId
      }
      req.body.PT_Username = PT_Username.toLowerCase()
      let stripeCustomer = await stripeModule.createCutomer(req.body.PT_Username)
      req.body.stripeCustomerId = stripeCustomer.id
      await model.Patient.create(req.body)
      let patientHistory = await patientHxModule.findByUsername(PT_Username)
      if (!patientHistory) {
        let historyData = {
          PT_HS_MRN: req.body.PT_HS_MRN
        }
        await patientHxModule.create(historyData)
      }
      return res.send({
        code: 0,
        status: 'failed',
        isExist: false,
        message: `*Hum… We didn't find a record for this email address. Please check it and try again.`,
        Error_Code: errorModule.did_not_find_record()
      })
    }
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.send({ code: 0, status: 'failed', message: error.message })
  }
})

async function getNextId () {
  let number = randomize('0', 12)
  let patternNumber = number.match(/.{1,4}/g)
  let id = patternNumber.join('-')
  let PT_HS_MRN = `HS-${id}`
  let patient = await patientModule.getPatientByHSId(PT_HS_MRN)
  if (patient.length > 0) {
    getNextId()
  } else {
    return PT_HS_MRN
  }
}
module.exports = router
