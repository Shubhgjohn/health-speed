/* eslint-disable no-unneeded-ternary */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
/* eslint-disable eqeqeq */
const Router = require('express').Router
const router = new Router()
const model = require('../../models')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const { auditUid, encounterUID } = require('../../utils/UID')
const auditType = require('../../auditType.json')
const moment = require('moment')
const momentTz = require('moment-timezone')
let isAvailable = true
const fcm = require('../../_helpers/fcm')
const logger = require('../../models/errorlog')
const availabilityCheck = require('../../service/checkProviderAvailability')

router.post('/create-encounter', authorize(Role.Patient), async (req, res) => {
	try {
		var data = req.body
		if (!data.Encounter_PT_Chief_Complaint_More) return res.send({ code: 0, status: 'failed', message: '*Don’t forget to explain your symptoms.', Error_Code: 'HS144' })
		if (!data.Encounter_Location_Type) return res.send({ code: 0, status: 'failed', message: 'Encounter_Location_Type is required', Error_Code: model.Errors.Encounter_Location_Type_required() })
		if (!data.Encounter_Type) return res.send({ code: 0, status: 'failed', message: 'Encounter_Type is required', Error_Code: model.Errors.Encounter_Type_required() })
		if (!data.Encounter_Date) return res.send({ code: 0, status: 'failed', message: 'Encounter_Date is required', Error_Code: model.Errors.Encounter_Date_required() })
		if (!data.Encounter_Cost) return res.send({ code: 0, status: 'failed', message: 'Encounter_Cost is required', Error_Code: model.Errors.Encounter_Cost_required() })
		if (!data.Encounter_Start_Time) return res.send({ code: 0, status: 'failed', message: 'Encounter_Start_Time is required', Error_Code: model.Errors.Encounter_Start_Time_required() })
		if (!data.Encounter_Start_Time.includes(':') && data.Encounter_Start_Time.match(/[a-z]/i)) return res.send({ code: 0, status: 'failed', message: 'Encounter_Start_Time format is not correct' })
		if (!data.Encounter_End_Time) return res.send({ code: 0, status: 'failed', message: 'Encounter_End_Time is required', Error_Code: model.Errors.Encounter_End_Time_required() })
		if (!data.Encounter_End_Time.includes(':') && data.Encounter_End_Time.match(/[a-z]/i)) return res.send({ code: 0, status: 'failed', message: 'Encounter_End_Time format is not correct' })
		if (!data.Encounter_Start_Time_Zone) return res.send({ code: 0, status: 'failed', message: 'Encounter_Start_Time_Zone is required', Error_Code: model.Errors.Encounter_Start_Time_Zone_required() })
		if (data.Encounter_Start_Time_Zone != 'am' && data.Encounter_Start_Time_Zone != 'pm') return res.send({ code: 0, status: 'failed', message: 'Encounter_Start_Time_Zone can contain only am/pm' })
		if (!data.Encounter_End_Time_Zone) return res.send({ code: 0, status: 'failed', message: 'Encounter_End_Time_Zone is required', Error_Code: model.Errors.Encounter_End_Time_Zone_required() })
		if (data.Encounter_End_Time_Zone != 'am' && data.Encounter_End_Time_Zone != 'pm') return res.send({ code: 0, status: 'failed', message: 'Encounter_End_Time_Zone can contain only am/pm' })
		if (!data.PRO_UID) return res.send({ code: 0, status: 'failed', message: 'PRO_UID is required', Error_Code: model.Errors.PRO_UID_required() })
		if (!data.PT_HS_MRN) return res.send({ code: 0, status: 'failed', message: 'PT_HS_MRN is required!', Error_Code: model.Errors.PT_HS_MRN_required() })
		if (!data.Payment_Card_Id) return res.send({ code: 0, status: 'failed', message: 'Payment_Card_Id required!', Error_Code: model.Errors.Payment_Card_Id_required() })
		data.Encounter_Chief_Complaint_PRO = data.Encounter_PT_Chief_Complaint_More
		const provider = await model.Provider.findById(data.PRO_UID)
		let patient = await model.Patient.findById(data.PT_HS_MRN)
		if (!provider) return res.send({ code: 0, status: 'failed', message: `provider with id '${data.PRO_UID}' is not found`, Error_Code: model.Errors.provider_not_found() })
		if (req.user.PT_Account_Status == 'Suspended' || provider.PRO_Account_Status == 'Suspended') return res.send({ code: 0, status: 'failed', message: 'Please contact Healthspeed.', Error_Code: model.Errors.you_can_not_schedule() })
		if (provider.PRO_Account_Status == 'New' || provider.PRO_Account_Status === 'Pending Approval') return res.send({ code: 0, status: 'failed', message: 'Please contact Healthspeed.', Error_Code: model.Errors.provider_can_not_accept() })
		if (provider.PRO_Account_Status === 'Banned') return res.send({ code: 0, status: 'failed', message: 'Please contact Healthspeed.', Error_Code: 'HS146' })
		data.Encounter_Start_Time = data.Encounter_Start_Time.charAt(0) == '0' ? data.Encounter_Start_Time.slice(1) : data.Encounter_Start_Time
		data.Encounter_End_Time = data.Encounter_End_Time.charAt(0) == '0' ? data.Encounter_End_Time.slice(1) : data.Encounter_End_Time
		let difference = await diff_hours(new Date(`${data.Encounter_Date} ${data.Encounter_Start_Time} ${data.Encounter_Start_Time_Zone}`), new Date(`${data.Encounter_Date} ${data.Encounter_End_Time} ${data.Encounter_End_Time_Zone}`))
		if (difference != 1) return res.send({ code: 0, status: 'failed', message: 'There somthing is wrong in your Encounter Start and End time.', Error_Code: 'HS402' })
		data.Encounter_Time = data.Encounter_Start_Time + ' ' + data.Encounter_Start_Time_Zone
		if (data.Triage_Q1_Answer == 'Pain' || data.Triage_Q1_Answer == 'Injured') {
			data.Encounter_Chief_Complaint_PT = data.Triage_Q2_Answer
		} else {
			data.Encounter_Chief_Complaint_PT = data.Triage_Q1_Answer
		}
		data.Encounter_Travel_Time = provider.PRO_Home_Visit_Buffer
		data.Encounter_Status = 'Scheduled'
		data.Encounter_Provider = `${provider.PRO_First_Name ? provider.PRO_First_Name : ''} ${provider.PRO_Last_Name ? provider.PRO_Last_Name : ''}`
		data.Encounter_UID = await encounterUID()
		data.PT_Username = req.user.PT_Username
		var travelTimeSlot = provider.PRO_Home_Visit_Buffer / 60
		var sTime, eTime
		const availability = await model.Availability.getProviderAvailability(data.PRO_UID, data.Encounter_Date)
		const providerAvailability = await model.ProviderAvailability.getProviderAvailability(req.body.PRO_UID, data.Encounter_Date)
		if (availability) {
			var Start_Time = moment(`${data.Encounter_Start_Time} ${data.Encounter_Start_Time_Zone}`, 'hh:mm A').format('HH.mm')
			var End_Time = moment(`${data.Encounter_End_Time} ${data.Encounter_End_Time_Zone}`, 'hh:mm A').format('HH.mm')

			Start_Time = Start_Time.includes(30) ? parseFloat(Start_Time) + 0.20 : parseFloat(Start_Time)
			End_Time = End_Time.includes(30) ? parseFloat(End_Time) + 0.20 : parseFloat(End_Time)

			if (data.Encounter_Location_Type == 'Home') {
				sTime = Start_Time - travelTimeSlot
				eTime = End_Time + travelTimeSlot
			} else {
				sTime = Start_Time
				eTime = End_Time
			}
			if (Date.parse(moment(new Date(`${data.Encounter_Date} ${data.Encounter_Time}`)).format('YYYY-MM-DDTHH:mm')) < Date.parse(momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm'))) return res.send({ code: 0, status: 'failed', message: 'Encounter_Start_Time can not be less than Current Time', Error_Code: 'HS147' })
			if (End_Time < Start_Time) return res.send({ code: 0, status: 'failed', message: 'Encounter_end_time cannot be less than Encounter_start_time', Error_Code: model.Errors.Encounter_End_Time_less_than_Encounter_Start_Time() })
			var arr = availability.PRO_Slots.filter(ele => ((ele.Slot_Format >= sTime) && (ele.Slot_Format < eTime)))
			var filterProviderAvailability = providerAvailability.PRO_Slots.filter(ele => ((ele.Slot_Format >= sTime) && (ele.Slot_Format < eTime)))
			let newdata = await availabilityCheck.checkProviderAvailability(filterProviderAvailability, data.Encounter_Location_Type, isAvailable)
			isAvailable = newdata == 'Available' ? true : false
			await checkAvailability(arr)
			if (isAvailable) {
				var newarr = availability.PRO_Slots.map(element => ({
					Is_Home_Available: ((element.Slot_Format == sTime || (element.Slot_Format >= sTime && element.Slot_Format < eTime))) ? false : element.Is_Home_Available,
					Is_Office_Available: ((element.Slot_Format == sTime || (element.Slot_Format >= sTime && element.Slot_Format < eTime))) ? false : element.Is_Office_Available,
					Is_Travel_Time: ((element.Slot_Format >= sTime && element.Slot_Format < Start_Time) || (element.Slot_Format >= End_Time && element.Slot_Format < eTime)) ? true : element.Is_Travel_Time,
					_id: element._id,
					Slot_Time: element.Slot_Time,
					Slot_Time_Zone: element.Slot_Time_Zone,
					Slot_Format: element.Slot_Format
				}))
				var newProviderAvailabilityArray = providerAvailability.PRO_Slots.map(element => ({
					Is_Home_Available: ((element.Slot_Format == sTime || (element.Slot_Format >= sTime && element.Slot_Format < eTime))) ? false : element.Is_Home_Available,
					Is_Office_Available: (element.Slot_Format == sTime || (element.Slot_Format >= sTime && element.Slot_Format < eTime)) ? false : element.Is_Office_Available,
					_id: element._id,
					Slot_Time: element.Slot_Time,
					Slot_Time_Zone: element.Slot_Time_Zone,
					Slot_Format: element.Slot_Format,
					Is_Home_Availability: element.Is_Home_Availability,
					Is_Office_Availability: element.Is_Office_Availability
				}))
				let patientHistory = await model.PatientHistory.findByUserId(data.PT_HS_MRN)
				patientHistory.PRO_UID = data.PRO_UID
				patientHistory.PT_Username = req.user.PT_Username
				data.PT_History = patientHistory
				let encounter = await model.Encounter.create(data)
				await createChart(encounter)
				await model.Availability.updateAvailabilityByTime(data.PRO_UID, data.Encounter_Date, newarr)
				await model.ProviderAvailability.updateAvailability(req.body.PRO_UID, data.Encounter_Date, newProviderAvailabilityArray)
				let jsonData = {
					PRO_UID: encounter.PRO_UID,
					Encounter_UID: encounter.Encounter_UID,
					title: 'Encounter Scheduled'
				}
				let PRO_Token = await model.Notification.getToken('Provider', provider.PRO_Username)
				let arr = []
				PRO_Token.map(ele => arr.push(ele.Device_Token))
				let title = `Visit Scheduled: ${moment(new Date(encounter.Encounter_Date)).format('M/D/YY')}`
				let body = 'A new visit has been scheduled. It’s been added to your calendar.'
				await sendNotification(arr, title, body, jsonData)
				await createAudit(req, data)
				isAvailable = true
				let encounterData = {
					PT_Profile_Picture: patient.PT_Profile_Picture ? await model.CommonServices.getImage(patient.PT_Profile_Picture) : null,
					PT_First_Name: patient.PT_First_Name,
					PT_Last_Name: patient.PT_Last_Name,
					PT_HS_MRN: patient.PT_HS_MRN,
					Encounter_UID: encounter.Encounter_UID,
					Encounter_Type: encounter.Encounter_Type,
					Encounter_Location_Type: encounter.Encounter_Location_Type,
					Encounter_Date: encounter.Encounter_Date,
					Encounter_Time: encounter.Encounter_Time,
					Encounter_Status: encounter.Encounter_Status,
					Encounter_Reschedule_Status: encounter.Encounter_Reschedule_Status,
					Cancellation_Reason: encounter.Cancellation_Reason,
					Encounter_Chief_Complaint_PRO: encounter.Encounter_Chief_Complaint_PRO,
					PRO_First_Name: provider.PRO_First_Name,
					PRO_Last_Name: provider.PRO_Last_Name,
					PRO_Profile_Picture: provider.PRO_Profile_Picture ? await model.CommonServices.getImage(provider.PRO_Profile_Picture) : null,
					PRO_Gender: provider.PRO_Gender,
					PRO_UID: provider.PRO_UID,
					PRO_Home_Address_Latitude: provider.PRO_Home_Address_Latitude,
					PRO_Home_Address_Longitude: provider.PRO_Home_Address_Longitude,
					PRO_Office_Address_Latitude: provider.PRO_Office_Address_Latitude,
					PRO_Office_Address_Longitude: provider.PRO_Office_Address_Longitude
				}
				res.send({ code: 1, status: 'sucess', message: 'encounter created sucessfull', Encounter_UID: encounter.Encounter_UID, data: encounterData })
			} else {
				isAvailable = true
				res.send({ code: 3, status: 'failed', message: 'Apologies, This timeslot is no longer available', Error_Code: model.Errors.You_can_not_set_encounter() })
			}
		} else {
			isAvailable = true
			res.send({ code: 0, status: 'failed', message: `No availability set for ${data.Encounter_Date} of Dr. ${provider.PRO_Last_Name}`, Error_Code: model.Errors.No_availability_available() })
		}
	} catch (err) {
		logger.error(err.message)
		logger.fatal(err)
		return res.send({ code: 2, status: 'failed', message: err.message, Error_Code: 'HS148' })
	}
})

var checkAvailability = async (availabilityArray) => {
	var count = 0
	availabilityArray.map(ele => {
		if ((ele.Is_Home_Available == true && ele.Is_Office_Available == true) || ele.Is_Travel_Time == true) {
			count = count + 1
			if (count == availabilityArray.length) {
				return true
			}
		} else {
			isAvailable = false
			count = count + 1
			if (count == availabilityArray.length) {
				return false
			}
		}
	})
}

async function createAudit (req, data) {
	var auditData = {
		Audit_UID: await auditUid(),
		Audit_Info: 'New- ' + await auditUid(),
		Audit_Type: auditType.add,
		Data_Point: 'PT/create encounter',
		New_Value: data,
		PT_First_Name: req.user.PT_First_Name,
		PT_Last_Name: req.user.PT_Last_Name,
		PT_HSID: req.user.PT_HS_MRN,
		IP_Address: await model.Audit.getIpAddress(req)
	}
	await model.Audit.create(auditData)
}

async function sendNotification (arr, title, body, jsonData) {
	if (arr.length != 0) {
		var payload = { // this may vary according to the message type (single recipient, multicast, topic, et cetera)
			registration_ids: arr,
			notification: {
				title: title,
				body: body
			},

			data: jsonData
		}
		await fcm.send(payload, function (err, response) {
			if (err) {
			} else console.log(response)
		})
	}
}

async function createChart (encounter) {
	let data = {
		PT_HS_MRN: encounter.PT_HS_MRN,
		PRO_Chart_Access: { PRO_UID: encounter.PRO_UID, Access_Status: true },
		PRO_UID: encounter.PRO_UID,
		Encounter_UID: encounter.Encounter_UID
	}
	await model.Chart.create(data)
	let unSignedDoc = await model.Chart.getProviderUnsignedDocCount(encounter.PRO_UID)
	await model.Provider.updateById(encounter.PRO_UID, { PRO_Number_of_Unsigned_Docs: unSignedDoc })
}

module.exports = router

function diff_hours (dt2, dt1) {
	var diff = (dt2.getTime() - dt1.getTime()) / 1000
	diff /= (60 * 60)
	return Math.abs(diff)
}
