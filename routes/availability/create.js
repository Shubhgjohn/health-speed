/* eslint-disable eqeqeq */
/* eslint-disable no-async-promise-executor */
/* eslint-disable no-unneeded-ternary */
/* eslint-disable no-self-assign */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
var Router = require('express').Router
var router = new Router()
const model = require('../../models')
var moment = require('moment')
const logger = require('../../models/errorlog')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const _ = require('lodash')

let officeAvailable = []
let officeUnavailable = []
let homeAvailable = []
let homeUnavalale = []
let officeFromTime = 0
let officeToTime = 0
let homeFromTime = 0
let homeToTime = 0
let availabilityType = ''

// service for set availability from provider
router.post('/set-availability', authorize(Role.Provider), async (req, res, next) => {
	var { body: { PRO_Availability_Date, PRO_Slots } } = req
	let status = false
	if (!PRO_Availability_Date) return res.send({ code: 0, status: 'failed', message: 'PRO_Availability_Date is required', Error_Code: 'HS015' })
	if (!PRO_Slots) return res.send({ code: 0, status: 'failed', message: 'PRO_Slots is required', Error_Code: 'HS016' })
	try {
		let PRO_UID = req.body.PRO_UID ? req.body.PRO_UID : req.user.PRO_UID
		if (PRO_Slots.length == 0) {
			await model.ProviderAvailability.deleteDayAvailability(PRO_UID, PRO_Availability_Date)
			await model.Availability.deleteDayAvailability(PRO_UID, PRO_Availability_Date)
			return res.send({
				code: 1,
				status: 'sucess',
				message: 'availability set successfully',
				office: [],
				home: []
			})
		}
		if (!req.user.StripeAccountId) {
			return res.send({
				code: 0,
				status: 'failed',
				message: `*provider with ${PRO_UID} has not setup deposit account`,
				Error_Code: 'HS017'
			})
		}


		// Get availability for the given date
		let provider_availability = await model.ProviderAvailability.getProviderAvailability(PRO_UID, PRO_Availability_Date)
		let officeFinalAvailableArray = []
		let officeFinalUnavailableArray = []
		let homeFinalAvailableArray = []
		let homeFinalUnavailableArray = []

		// if availability is set in system for the given date
		if (provider_availability) {
			if (PRO_Slots.length == 0) {
				let slotArray = [{
					From_Time: 0,
					To_Time: 24,
					Is_available: false
				}]

				// divide array in four-four hour parts
				let fourPartsArray = slotArray.length > 0 ? await getPartsArray(slotArray) : []
				let slotData = await getSlotArray()

				// update avaiability for date
				await model.ProviderAvailability.updateAvailability(PRO_UID, PRO_Availability_Date, slotData)
				res.send({
					code: 1,
					status: 'sucess',
					office: fourPartsArray,
					home: fourPartsArray
				})
			} else {
				let slotData = await getSlotArray()
				for (var i = 0; i < PRO_Slots.length; i++) {
					let To_Time = PRO_Slots[i].To_Time == 24 ? 23.5 : PRO_Slots[i].To_Time
					let startIndex = await getIndex(slotData, PRO_Slots[i].From_Time)
					let EndIndex = await getIndex(slotData, To_Time)
					for (var j = 0; j < slotData.length; j++) {
						if (j >= startIndex && j <= (PRO_Slots[i].To_Time == 24 ? EndIndex : EndIndex - 1)) {
							let slotJsonData = {
								Slot_Time: slotData[j].Slot_Time,
								Slot_Time_Zone: slotData[j].Slot_Time_Zone,
								Slot_Format: slotData[j].Slot_Format,
								Is_Home_Availability: slotData[j].Is_Home_Availability ? true : PRO_Slots[i].Is_Office_Availability ? false : true,
								Is_Office_Availability: slotData[j].Is_Office_Availability ? true : PRO_Slots[i].Is_Home_Availability ? false : true,
								Is_Home_Available: slotData[j].Is_Home_Availability ? true : PRO_Slots[i].Is_Office_Availability ? false : true,
								Is_Office_Available: slotData[j].Is_Office_Availability ? true : PRO_Slots[i].Is_Home_Availability ? false : true
							}
							slotData[j] = slotJsonData
						} else {
							slotData[j] = slotData[j]
						}
					}
					if (i == PRO_Slots.length - 1) {
						await model.ProviderAvailability.updateAvailability(PRO_UID, PRO_Availability_Date, slotData)
						await getAvailableOfficeSlotArray(slotData, status)
						await getAvailableHomeSlotArray(slotData, status)

						// get available hours for office
						officeAvailable.map(ele => {
							if (ele.length == 1) {
								officeFinalAvailableArray.push({
									From_Time: ele[0].Slot_Format,
									To_Time: ele[0].Slot_Format + 0.5,
									Is_available: true
								})
							} else {
								officeFinalAvailableArray.push({
									From_Time: ele[0].Slot_Format == 23.5 ? 24 : ele[0].Slot_Format,
									To_Time: (ele[ele.length - 1].Slot_Format == 23.5 && ele[ele.length - 1].Is_Office_Availability == true) ? 24 : ele[ele.length - 1].Slot_Format,
									Is_available: true
								})
							}
						})

						// get unavailable hours for office
						officeUnavailable.map(ele => {
							if (ele.length == 1) {
								officeFinalUnavailableArray.push({
									From_Time: ele[0].Slot_Format,
									To_Time: ele[0].Slot_Format + 0.5,
									Is_available: false
								})
							} else {
								officeFinalUnavailableArray.push({
									From_Time: ele[0].Slot_Format == 23.5 ? 24 : ele[0].Slot_Format,
									To_Time: (ele[ele.length - 1].Slot_Format == 23.5 && ele[ele.length - 1].Is_Office_Availability == false) ? 24 : ele[ele.length - 1].Slot_Format,
									Is_available: false
								})
							}
						})
						let officePartsArray = officeFinalUnavailableArray.length > 0 ? await getPartsArray(officeFinalUnavailableArray) : []
						officeFinalUnavailableArray = officePartsArray

						// get available hours for home
						homeAvailable.map(ele => {
							if (ele.length == 1) {
								homeFinalAvailableArray.push({
									From_Time: ele[0].Slot_Format,
									To_Time: ele[0].Slot_Format + 0.5,
									Is_available: true
								})
							} else {
								homeFinalAvailableArray.push({
									From_Time: ele[0].Slot_Format == 23.5 ? 24 : ele[0].Slot_Format,
									To_Time: (ele[ele.length - 1].Slot_Format == 23.5 && ele[ele.length - 1].Is_Home_Availability == true) ? 24 : ele[ele.length - 1].Slot_Format,
									Is_available: true
								})
							}
						})

						// get unavailable hours for home
						homeUnavalale.map(ele => {
							if (ele.length == 1) {
								homeFinalUnavailableArray.push({
									From_Time: ele[0].Slot_Format,
									To_Time: ele[0].Slot_Format + 0.5,
									Is_available: false
								})
							} else {
								homeFinalUnavailableArray.push({
									From_Time: ele[0].Slot_Format == 23.5 ? 24 : ele[0].Slot_Format,
									To_Time: (ele[ele.length - 1].Slot_Format == 23.5 && ele[ele.length - 1].Is_Home_Availability == false) ? 24 : ele[ele.length - 1].Slot_Format,
									Is_available: false
								})
							}
						})
						let homePartsArray = homeFinalUnavailableArray.length > 0 ? await getPartsArray(homeFinalUnavailableArray) : []
						homeFinalUnavailableArray = homePartsArray
						let officeArray = officeFinalAvailableArray.concat(officeFinalUnavailableArray)
						let homeArray = homeFinalAvailableArray.concat(homeFinalUnavailableArray)
						officeFinalUnavailableArray = []
						officeFinalAvailableArray = []
						officeAvailable = []
						officeUnavailable = []
						homeFinalUnavailableArray = []
						homeFinalAvailableArray = []
						homeAvailable = []
						homeUnavalale = []
						officeArray = _.sortBy(officeArray, 'From_Time')
						homeArray = _.sortBy(homeArray, 'From_Time')

						// Get availability for date
						let availability = await model.Availability.getProviderAvailability(req.user.PRO_UID, PRO_Availability_Date)
						if (!availability) {
							let data = await getProviderAvailabilityData(req, PRO_Availability_Date, PRO_Slots)
							await model.Availability.create(data)
						} else {
							await getUpdatedSlots(PRO_Slots)
							let data = {
								PRO_UID: req.user.PRO_UID,
								PRO_Availability_Date: availability.PRO_Availability_Date,
								PRO_Availability_Day: availability.PRO_Availability_Day,
								PRO_Availability_From_Time: availability.PRO_Availability_From_Time,
								PRO_Availability_From_Time_Zone: availability.PRO_Availability_From_Time_Zone,
								PRO_Availability_To_Time: availability.PRO_Availability_To_Time,
								PRO_Availability_To_Time_Zone: availability.PRO_Availability_To_Time_Zone,
								PRO_Availability_Type: availabilityType,
								PRO_Availability_Set_By: req.userType,
								PRO_Slots: availability.PRO_Slots
							}

							// update availability
							await model.Availability.updateAvailability(req.user.PRO_UID, data)
						}
						res.send({
							code: 1,
							status: 'sucess',
							message: 'availability set successfully',
							office: officeArray,
							home: homeArray
						})
					}
				}
			}
		} else {
			let slotData = await getSlotArray(PRO_Slots[0])
			slotData = slotData.map(ele => ({
				Slot_Time: ele.Slot_Time,
				Slot_Time_Zone: ele.Slot_Time_Zone,
				Slot_Format: ele.Slot_Format,
				Is_Home_Availability: PRO_Slots[0].Is_Office_Availability ? false : (ele.Slot_Format >= PRO_Slots[0].From_Time && ele.Slot_Format < PRO_Slots[0].To_Time) ? true : false,
				Is_Office_Availability: PRO_Slots[0].Is_Home_Availability ? false : (ele.Slot_Format >= PRO_Slots[0].From_Time && ele.Slot_Format < PRO_Slots[0].To_Time) ? true : false,
				Is_Home_Available: PRO_Slots[0].Is_Office_Availability ? false : (ele.Slot_Format >= PRO_Slots[0].From_Time && ele.Slot_Format < PRO_Slots[0].To_Time) ? true : false,
				Is_Office_Available: PRO_Slots[0].Is_Home_Availability ? false : (ele.Slot_Format >= PRO_Slots[0].From_Time && ele.Slot_Format < PRO_Slots[0].To_Time) ? true : false
			}))
			let json = {
				PRO_UID: PRO_UID,
				PRO_Availability_Date: PRO_Availability_Date,
				PRO_Availability_Day: moment(new Date(PRO_Availability_Date)).format('ddd'),
				PRO_Availability_Set_By: req.userType,
				PRO_Slots: slotData
			}

			// Get availability data
			let data = await getProviderAvailabilityData(req, PRO_Availability_Date, PRO_Slots)

			// Create availability
			await model.Availability.create(data)
			await model.ProviderAvailability.create(json)
			await getAvailableOfficeSlotArray(slotData, status)
			await getAvailableHomeSlotArray(slotData, status)

			// get available hours for office
			officeAvailable.map(ele => {
				officeFinalAvailableArray.push({
					From_Time: ele[0].Slot_Format == 23.5 ? 24 : ele[0].Slot_Format,
					To_Time: ele[ele.length - 1].Slot_Format,
					Is_available: true
				})
			})

			// get unavailable hours for office
			officeUnavailable.map(ele => {
				officeFinalUnavailableArray.push({
					From_Time: ele[0].Slot_Format == 23.5 ? 24 : ele[0].Slot_Format,
					To_Time: ele[ele.length - 1].Slot_Format,
					Is_available: false
				})
			})

			let officePartsArray = officeFinalUnavailableArray.length > 0 ? await getPartsArray(officeFinalUnavailableArray) : []
			officeFinalUnavailableArray = officePartsArray

			// get available hours for home
			homeAvailable.map(ele => {
				homeFinalAvailableArray.push({
					From_Time: ele[0].Slot_Format == 23.5 ? 24 : ele[0].Slot_Format,
					To_Time: ele[ele.length - 1].Slot_Format,
					Is_available: true
				})
			})

			// get unavailable hours for home
			homeUnavalale.map(ele => {
				homeFinalUnavailableArray.push({
					From_Time: ele[0].Slot_Format == 23.5 ? 24 : ele[0].Slot_Format,
					To_Time: ele[ele.length - 1].Slot_Format,
					Is_available: false
				})
			})

			let homePartsArray = homeFinalUnavailableArray.length > 0 ? await getPartsArray(homeFinalUnavailableArray) : []
			homeFinalUnavailableArray = homePartsArray
			let officeArray = officeFinalAvailableArray.concat(officeFinalUnavailableArray)
			let homeArray = homeFinalAvailableArray.concat(homeFinalUnavailableArray)
			officeFinalUnavailableArray = []
			officeFinalAvailableArray = []
			officeAvailable = []
			officeUnavailable = []
			homeFinalUnavailableArray = []
			homeFinalAvailableArray = []
			homeAvailable = []
			homeUnavalale = []
			officeArray = _.sortBy(officeArray, 'From_Time')
			homeArray = _.sortBy(homeArray, 'From_Time')
			res.send({
				code: 1,
				status: 'sucess',
				message: 'availability set successfully',
				office: officeArray,
				home: homeArray
			})
		}
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS018' })
	}
})

// Get office available slots
let getAvailableOfficeSlotArray = async (slotData, status) => {
	if (status || slotData.length == 1) {
		return
	}
	let startStatus = slotData[0].Is_Office_Available
	await nextFunctionOffice(startStatus, slotData)
}


// function for get provider availabity data
let getProviderAvailabilityData = async (req, PRO_Availability_Date, PRO_Slots) => {
	let availabilityData = await getAvailabilitySlotArray()
	await getUpdatedSlots(PRO_Slots)
	let From_Time, From_Time_Zone, To_Time, To_Time_Zone
	if (availabilityType == 'Both') {
		From_Time = officeFromTime <= homeFromTime ? officeFromTime : homeFromTime
		To_Time = officeToTime >= homeToTime ? officeToTime : homeToTime
	} else {
		From_Time = availabilityType == 'home' ? homeFromTime : officeFromTime
		To_Time = availabilityType == 'home' ? homeToTime : officeToTime
	}
	From_Time_Zone = moment(From_Time, 'HH.mm').format('h:mm a')
	To_Time_Zone = moment(To_Time, 'HH.mm').format('h:mm a')
	let data = {
		PRO_UID: req.user.PRO_UID,
		PRO_Availability_Date: PRO_Availability_Date,
		PRO_Availability_Day: moment(new Date(PRO_Availability_Date)).format('ddd'),
		PRO_Availability_From_Time: From_Time_Zone.split(' ')[0],
		PRO_Availability_From_Time_Zone: From_Time_Zone.split(' ')[1],
		PRO_Availability_To_Time: To_Time_Zone.split(' ')[0],
		PRO_Availability_To_Time_Zone: To_Time_Zone.split(' ')[1],
		PRO_Availability_Type: availabilityType,
		PRO_Availability_Set_By: req.userType,
		PRO_Slots: availabilityData
	}
	return data
}

// function for get slot array //
let nextFunctionOffice = async (status, slotData) => {
	let nextSlot = slotData.find(ele => ele.Is_Office_Available != status)
	let stat = !nextSlot ? true : false
	if (!nextSlot) {
		nextSlot = slotData[slotData.length - 1]
	}
	let index = await getIndex(slotData, nextSlot.Slot_Format)
	let data = slotData.slice(0, index + 1)
	if (status) {
		officeAvailable.push(data)
		let nextArray = slotData.slice(index, slotData.length)
		await getAvailableOfficeSlotArray(nextArray, stat)
	} else {
		officeUnavailable.push(data)
		let nextArray = slotData.slice(index, slotData.length)
		await getAvailableOfficeSlotArray(nextArray, stat)
	}
}


// Get home available slots
let getAvailableHomeSlotArray = async (slotData, status) => {
	if (status) {
		return
	}
	let startStatus = slotData[0].Is_Home_Available
	await nextFunctionHome(startStatus, slotData)
}


// function for get slot array //
let nextFunctionHome = async (status, slotData) => {
	let nextSlot = slotData.find(ele => ele.Is_Home_Available != status)
	let stat = !nextSlot ? true : false
	if (!nextSlot) {
		nextSlot = slotData[slotData.length - 1]
	}
	let index = await getIndex(slotData, nextSlot.Slot_Format)
	let data = slotData.slice(0, index + 1)
	if (status) {
		homeAvailable.push(data)
		let nextArray = slotData.slice(index, slotData.length)
		await getAvailableHomeSlotArray(nextArray, stat)
	} else {
		homeUnavalale.push(data)
		let nextArray = slotData.slice(index, slotData.length)
		await getAvailableHomeSlotArray(nextArray, stat)
	}
}

// Get slot array
var getSlotArray = async (slot) => {
	return new Promise(async function (resolve, reject) {
		var arr = []; var i; var j
		for (i = 0; i < 24; i++) {
			for (j = 0; j < 2; j++) {
				arr.push({
					Slot_Time: `${(i > 12 ? i - 12 : i) + ':' + (j == 0 ? '00' : 30)}`,
					Slot_Time_Zone: `${i >= 12 ? 'pm' : 'am'}`,
					Slot_Format: j == 0 ? i : i + 0.50,
					Is_Home_Availability: false,
					Is_Office_Availability: false,
					Is_Home_Available: false,
					Is_Office_Available: false
				})
			}
		}
		resolve(arr)
	})
}

// Function for get index for given value from array
let getIndex = (array, value) => {
	let index = -1
	array.find(function (item, i) {
		if (item.Slot_Format == value) {
			index = i
			return i
		}
	})
	return index
}


// Function for divide array in four-four hour parts
let getPartsArray = (arr) => {
	let partArray = []
	for (var i = 0; i < arr.length; i++) {
		for (let j = arr[i].From_Time; j < arr[i].To_Time; j += 4) {
			let json = {
				From_Time: j,
				To_Time: j + 4 > arr[i].To_Time ? arr[i].To_Time : j + 4,
				Is_available: false
			}
			partArray.push(json)
		}
		if (i == arr.length - 1) {
			return partArray
		}
	}
}

// Function for get availability slot array
var getAvailabilitySlotArray = async () => {
	return new Promise(async function (resolve, reject) {
		var arr = []; var i; var j
		for (i = 0; i < 24; i++) {
			for (j = 0; j < 2; j++) {
				arr.push({
					Slot_Time: `${(i > 12 ? i - 12 : i) + ':' + (j == 0 ? '00' : 30)}`,
					Slot_Time_Zone: `${i >= 12 ? 'pm' : 'am'}`,
					Slot_Format: `${j == 0 ? i : i + 0.50}`,
					Is_Home_Available: true,
					Is_Office_Available: true,
					Is_Travel_Time: false
				})
			}
		}
		resolve(arr)
	})
}

// function for get updated slots
let getUpdatedSlots = async (PRO_Slots) => {
	let type = []
	for (let i = 0; i < PRO_Slots.length; i++) {
		if (PRO_Slots[i].Is_Office_Availability) {
			officeFromTime = officeFromTime > PRO_Slots[i].From_Time ? PRO_Slots[i].From_Time : officeFromTime
			officeToTime = officeToTime < PRO_Slots[i].To_Time ? PRO_Slots[i].To_Time : officeToTime
			if (!type.includes('Office')) {
				type.push('Office')
			}
		}
		if (PRO_Slots[i].Is_Home_Availability) {
			homeFromTime = homeFromTime > PRO_Slots[i].From_Time ? PRO_Slots[i].From_Time : homeFromTime
			homeToTime = homeToTime < PRO_Slots[i].To_Time ? PRO_Slots[i].To_Time : homeToTime
			if (!type.includes('Home')) {
				type.push('Home')
			}
		}
		if (i == PRO_Slots.length - 1) {
			if (type.length > 1) {
				availabilityType = 'Both'
			} else {
				availabilityType = type[0]
			}
			return
		}
	}
}
module.exports = router
