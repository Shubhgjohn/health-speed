/* eslint-disable eqeqeq */
/* eslint-disable no-unneeded-ternary */
/* eslint-disable camelcase */
/* eslint-disable prefer-const */
var Router = require('express').Router
var router = new Router()
const model = require('../../models')
const logger = require('../../models/errorlog')
var _ = require('lodash')
const moment = require('moment')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
let { getTravelTimeArray , getAvailableSlotArray, getAvailableSlotHomeArray } = require('../../service/getAvailability')
let available = []
let homeAvailable = []

// Get provider availability by date //
router.get('/get_availability/:date', authorize(Role.Provider), async (req, res) => {
  var { params: { date } } = req
  if (!date) return res.send({ code: 0, sttaus: 'failed', message: 'date is required', Error_Code: model.Errors.date_required() })
  try {
    // Check availability is set in the system or not
    let availability = await model.Availability.getProviderAvailability(req.user.PRO_UID, date)
    let provider_availability = await model.ProviderAvailability.getProviderAvailability(req.user.PRO_UID, date)
    let status = false

    // if availability is not set in the system 
    if (!availability) {
      res.send({ code: 0, status: 'failed', message: `No availability available for ${req.user.PRO_UID} in ${date} date `, Error_Code: model.Errors.No_availability_available() })
    } else {
      let filteredEncounter = []

      // Get encounter of the provider on the given date
      var encounters = await model.Encounter.getProviderEncounters(req.user.PRO_UID, date)

      let slots1 = provider_availability.PRO_Slots

      let d = new Date()
      let current_date = moment.tz(d, process.env.CURRENT_TIMEZONE).format('MM-DD-YYYY')
      var newYork = moment.tz(d, process.env.CURRENT_TIMEZONE).format('H:mm')
      newYork = newYork.split(':')[1] < 30 ? parseFloat(newYork.split(':')[0]) + .5 : parseFloat(newYork.split(':')[0]) + 1

      newYork = current_date == date ? newYork : 0
      let startIndex = availability.PRO_Slots.findIndex(z => z.Slot_Format == newYork)
      let endIndex = 48
      slots1 = slots1.slice(startIndex, endIndex)
        let arr = []
        let arr1 = []
       available = await getAvailableSlotArray(slots1, status, arr)
        
       homeAvailable =  await getAvailableSlotHomeArray(slots1, status, arr1)

      let availableData = []
      if (available.length > 0 && available[0].length > 0) {
        available.map(ele => {
          if (ele.length > 0) {
            availableData.push({
              From_Time: ele[0].Slot_Format == 23.5 ? 24 : ele[0].Slot_Format,
              To_Time: ele[ele.length - 1].Slot_Format == 23.5 ? 24 : ele[ele.length - 1].Slot_Format,
              Is_available: false,
              Type: 'Office'
            })
          }
        })
      }
      if (homeAvailable.length > 0 && homeAvailable[0].length > 0) {
        homeAvailable.map(ele => {
          if (ele.length > 0) {
            availableData.push({
              From_Time: ele[0].Slot_Format == 23.5 ? 24 : ele[0].Slot_Format,
              To_Time: ele[ele.length - 1].Slot_Format == 23.5 ? 24 : ele[ele.length - 1].Slot_Format,
              Is_available: false, 
              Type: 'Home'
            })
          }
        })
      }

      // if provider have encounter on the given date
      if (encounters.length == 0) {
        filteredEncounter = []
        var data = JSON.parse(JSON.stringify(availability))
        data.encounters = filteredEncounter
        let travelTime = filteredEncounter.length > 0 ? await getTravelTimeArray(req.user, encounters, availability) : []
        travelTime = travelTime.length > 0 ? _.uniqWith(travelTime, _.isEqual) : []
        data.travelTime = travelTime
        availableData = _.sortBy(availableData, 'From_Time')
        data.availability = availableData
        availableData = []
        available = []
        homeAvailable = []
        res.send({ code: 1, status: 'sucess', data: data })
      } else {
        for (let index = 0; index < encounters.length; index++) {
          const element = encounters[index]
          if (element.Encounter_Start_Time.match(/[a-z]/i)) {
            if (element.Encounter_Start_Time.includes('a')) {
              element.Encounter_Start_Time = element.Encounter_Start_Time.split('a')[0]
              element.Encounter_Start_Time_Zone = 'am'
            }
            if (element.Encounter_Start_Time.includes('p')) {
              element.Encounter_Start_Time = element.Encounter_Start_Time.split('p')[0]
              element.Encounter_Start_Time_Zone = 'pm'
            }
          }
          if (element.Encounter_End_Time.match(/[a-z]/i)) {
            if (element.Encounter_End_Time.includes('a')) {
              element.Encounter_End_Time = element.Encounter_End_Time.split('a')[0]
              element.Encounter_End_Time_Zone = 'am'
            }
            if (element.Encounter_End_Time.includes('p')) {
              element.Encounter_End_Time = element.Encounter_End_Time.split('p')[0]
              element.Encounter_End_Time_Zone = 'pm'
            }
          }
          if (element.Encounter_Status != 'Cancelled') {
            filteredEncounter.push({
              Encounter_UID: element.Encounter_UID,
              Encounter_Date: element.Encounter_Date,
              Encounter_Start_Time: element.Encounter_Start_Time,
              Encounter_Start_Time_Zone: element.Encounter_Start_Time_Zone,
              Encounter_End_Time: element.Encounter_End_Time,
              Encounter_End_Time_Zone: element.Encounter_End_Time_Zone,
              Encounter_Location_Type: element.Encounter_Location_Type,
              Encounter_Type: element.Encounter_Type,
              Encounter_Cost: element.Encounter_Cost,
              Encounter_Chief_Complaint_PT: element.Encounter_Chief_Complaint_PT,
              Encounter_PT_Chief_Complaint_More: element.Encounter_PT_Chief_Complaint_More ? element.Encounter_PT_Chief_Complaint_More : '',
              Encounter_Chief_Complaint_PRO: element.Encounter_Chief_Complaint_PRO,
              Encounter_Provider: element.Encounter_Provider,
              Patient_Username: element.patient[0].PT_Username ? element.patient[0].PT_Username : null,
              Patient_Name: `${element.patient[0].PT_First_Name ? element.patient[0].PT_First_Name : ''} ${element.patient[0].PT_Last_Name ? element.patient[0].PT_Last_Name : ''}`,
              Patient_Age: element.patient[0].PT_DOB ? new Date().getFullYear() - element.patient[0].PT_DOB.getFullYear() : null,
              Patient_Gender: element.patient[0].PT_Gender
            })
          }
          if (index == encounters.length - 1) {
            // eslint-disable-next-line no-redeclare
            var data = JSON.parse(JSON.stringify(availability))
            data.encounters = filteredEncounter
            let travelTime = filteredEncounter.length > 0 ? await getTravelTimeArray(req.user, encounters, availability) : []
            travelTime = travelTime.length > 0 ? _.uniqWith(travelTime, _.isEqual) : []
            data.travelTime = travelTime
            availableData = _.sortBy(availableData, 'From_Time')
            data.availability = availableData
            availableData = []
            available = []
            homeAvailable = []
            res.send({ code: 1, status: 'sucess', data: data })
          }
        }
      }
    }
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    available = []
    return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS021' })
  }
})

module.exports = router
