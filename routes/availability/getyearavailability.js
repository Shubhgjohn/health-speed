/* eslint-disable eqeqeq */
/* eslint-disable no-unneeded-ternary */
/* eslint-disable prefer-const */
var Router = require('express').Router
var router = new Router()
const model = require('../../models')
const logger = require('../../models/errorlog')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const moment = require('moment')


// Service for get availability for whole year
router.get('/get_year_availability/:id?/:year', authorize([Role.Admin, Role.Provider]), async (req, res) => {
  var { params: { year } } = req
  var resultData = []
  if (!year) return res.send({ code: 0, status: 'failed', message: 'year is required', Error_Code: model.Errors.year_required() })
  try {
    let PRO_UID = req.userType == 'Admin' ? req.params.id : req.user.PRO_UID

    // Get all year availability 
    var availabilities = await model.Availability.getProviderAllAvailability(PRO_UID)
    for (var i = 1; i <= 12; i++) {
      let monthData = await getMonthData(year, i, availabilities, PRO_UID)
      resultData.push({
        year: year,
        month: monthData
      })
      if (i == 12) {
        res.send({ code: 1, status: 'sucess', data: resultData })
      }
    }
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS024' })
  }
})
// function for get month data of year //
let getMonthData = async (year, month, availabilityArray, PRO_UID) => {
  var monthArray = []
  let totalDays = await daysInMonth(month, year)
  for (var i = 1; i <= totalDays; i++) {
    var date = `${month < 10 ? '0' + month : month}-${i < 10 ? '0' + i : i}-${year}`
    var data = availabilityArray.find(ele => ele.PRO_Availability_Date == date)
    var isEncounter = await model.Encounter.getEncounterForDate(PRO_UID, date)
    if (data) {
      let homeSpecial = false
      let officeSpecial = false
      let specialRateDate = moment(new Date(date)).format('M/D/YY')
      var specialRates = await model.Provider.getSpecialRatesByDate(PRO_UID, specialRateDate)
      if (specialRates.length > 0) {
        homeSpecial = specialRates[0].special_rate.PRO_Home_Followup_Special_Hourly_Rate ? true : false
        officeSpecial = specialRates[0].special_rate.PRO_Office_Followup_Special_Hourly_Rate ? true : false
      }
      monthArray.push({
        date: date,
        month: month,
        PRO_Availability_Set_By: data.PRO_Availability_Set_By,
        Is_Office_Availability: data.PRO_Availability_Type == 'Both' ? true : data.PRO_Availability_Type == 'Office' ? true : false,
        Is_Home_Availability: data.PRO_Availability_Type == 'Both' ? true : data.PRO_Availability_Type == 'Home' ? true : false,
        Is_Office_Special_Rates: officeSpecial,
        Is_Home_Special_Rates: homeSpecial,
        Is_Encounter: isEncounter ? true : false
      })
    } else {
      monthArray.push({
        date: date,
        month: month,
        PRO_Availability_Set_By: '',
        Is_Office_Availability: false,
        Is_Home_Availability: false,
        Is_Office_Special_Rates: false,
        Is_Home_Special_Rates: false,
        Is_Encounter: false
      })
    }

    if (i == totalDays) {
      return monthArray
    }
  }
}

// function for get total days in month
function daysInMonth (month, year) {
  return new Date(year, month, 0).getDate()
}

module.exports = router
