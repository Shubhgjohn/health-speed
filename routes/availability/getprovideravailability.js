/* eslint-disable eqeqeq */
/* eslint-disable no-unneeded-ternary */
/* eslint-disable camelcase */
/* eslint-disable prefer-const */
var Router = require('express').Router
var router = new Router()
const model = require('../../models')
const logger = require('../../models/errorlog')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const _ = require('lodash')

let officeAvailable = []
let officeUnavailable = []
let homeAvailable = []
let homeUnavalale = []

// service for get the availability for the given date 
router.get('/get-provider-availability/:date', authorize(Role.Provider), async (req, res, next) => {
  var { params: { date } } = req
  if (!date) return res.send({ code: 0, status: 'failed', message: 'date is required', Error_Code: model.Errors.date_required() })
  try {
    let status = false

    // check availability is set in the system or not
    let provider_availability = await model.ProviderAvailability.getProviderAvailability(req.user.PRO_UID, date)
    let officeFinalAvailableArray = []
    let officeFinalUnavailableArray = []
    let homeFinalAvailableArray = []
    let homeFinalUnavailableArray = []

    // if availability is set in the system
    if (provider_availability) {
      let slotData = provider_availability.PRO_Slots
      await getAvailableOfficeSlotArray(slotData, status)
      await getAvailableHomeSlotArray(slotData, status)

      // Get available hour array for office
      officeAvailable.map(ele => {
        if (ele.length == 1) {
          officeFinalAvailableArray.push({
            From_Time: ele[0].Slot_Format,
            To_Time: ele[0].Slot_Format + 0.5,
            Is_available: true
          })
        } else {
          officeFinalAvailableArray.push({
            From_Time: ele[0].Slot_Format == 23.5 ? 24 : ele[0].Slot_Format,
            To_Time: (ele[ele.length - 1].Slot_Format == 23.5 && ele[ele.length - 1].Is_Office_Availability == true) ? 24 : ele[ele.length - 1].Slot_Format,
            Is_available: true
          })
        }
      })

      // Get unavailable hour array for office
      officeUnavailable.map(ele => {
        if (ele.length == 1) {
          officeFinalUnavailableArray.push({
            From_Time: ele[0].Slot_Format,
            To_Time: ele[0].Slot_Format + 0.5,
            Is_available: false
          })
        } else {
          officeFinalUnavailableArray.push({
            From_Time: ele[0].Slot_Format == 23.5 ? 24 : ele[0].Slot_Format,
            To_Time: (ele[ele.length - 1].Slot_Format == 23.5 && ele[ele.length - 1].Is_Office_Availability == false) ? 24 : ele[ele.length - 1].Slot_Format,
            Is_available: false
          })
        }
      })
      let officePartsArray = officeFinalUnavailableArray.length > 0 ? await getPartsArray(officeFinalUnavailableArray) : []
      officeFinalUnavailableArray = officePartsArray

      // Get available hour array for home
      homeAvailable.map(ele => {
        if (ele.length == 1) {
          homeFinalAvailableArray.push({
            From_Time: ele[0].Slot_Format,
            To_Time: ele[0].Slot_Format + 0.5,
            Is_available: true
          })
        } else {
          homeFinalAvailableArray.push({
            From_Time: ele[0].Slot_Format == 23.5 ? 24 : ele[0].Slot_Format,
            To_Time: (ele[ele.length - 1].Slot_Format == 23.5 && ele[ele.length - 1].Is_Home_Availability == true) ? 24 : ele[ele.length - 1].Slot_Format,
            Is_available: true
          })
        }
      })

      // Get unavailable hour array for home
      homeUnavalale.map(ele => {
        if (ele.length == 1) {
          homeFinalUnavailableArray.push({
            From_Time: ele[0].Slot_Format,
            To_Time: ele[0].Slot_Format + 0.5,
            Is_available: false
          })
        } else {
          homeFinalUnavailableArray.push({
            From_Time: ele[0].Slot_Format == 23.5 ? 24 : ele[0].Slot_Format,
            To_Time: (ele[ele.length - 1].Slot_Format == 23.5 && ele[ele.length - 1].Is_Home_Availability == false) ? 24 : ele[ele.length - 1].Slot_Format,
            Is_available: false
          })
        }
      })
      var homePartsArray = homeFinalUnavailableArray.length > 0 ? await getPartsArray(homeFinalUnavailableArray) : []
      homeFinalUnavailableArray = homePartsArray

      let officeArray = officeFinalAvailableArray.concat(officeFinalUnavailableArray)
      let homeArray = homeFinalAvailableArray.concat(homeFinalUnavailableArray)
      officeFinalUnavailableArray = []
      officeFinalAvailableArray = []
      officeAvailable = []
      officeUnavailable = []
      homeFinalUnavailableArray = []
      homeFinalAvailableArray = []
      homeAvailable = []
      homeUnavalale = []
      officeArray = _.sortBy(officeArray, 'From_Time')
      homeArray = _.sortBy(homeArray, 'From_Time')
      res.send({
        code: 1,
        status: 'sucess',
        office: officeArray,
        home: homeArray
      })
    } else {
      let slotArray = [{
        From_Time: 0,
        To_Time: 24,
        Is_available: false
      }]
      var fourPartsArray = await getPartsArray(slotArray)
      res.send({
        code: 1,
        status: 'sucess',
        office: fourPartsArray,
        home: fourPartsArray
      })
    }
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS022' })
  }
})


// Get availability slot array 
let getAvailableOfficeSlotArray = async (slotData, status) => {
  if (status) {
    return
  }
  let startStatus = slotData[0].Is_Office_Available
  await nextFunctionOffice(startStatus, slotData)
}


// function for get slot array //
let nextFunctionOffice = async (status, slotData) => {
  let nextSlot = slotData.find(ele => ele.Is_Office_Available != status)
  let stat = !nextSlot ? true : false
  if (!nextSlot) {
    nextSlot = slotData[slotData.length - 1]
  }
  let index = await getIndex(slotData, nextSlot.Slot_Format)
  let data = slotData.slice(0, index + 1)
  if (status) {
    officeAvailable.push(data)
    let nextArray = slotData.slice(index, slotData.length)
    await getAvailableOfficeSlotArray(nextArray, stat)
  } else {
    officeUnavailable.push(data)
    let nextArray = slotData.slice(index, slotData.length)
    await getAvailableOfficeSlotArray(nextArray, stat)
  }
}


// Function for get availabile slot array for home
let getAvailableHomeSlotArray = async (slotData, status) => {
  if (status) {
    return
  }
  let startStatus = slotData[0].Is_Home_Available
  await nextFunctionHome(startStatus, slotData)
}


// function for get slot array //
let nextFunctionHome = async (status, slotData) => {
  let nextSlot = slotData.find(ele => ele.Is_Home_Available != status)
  let stat = !nextSlot ? true : false
  if (!nextSlot) {
    nextSlot = slotData[slotData.length - 1]
  }
  let index = await getIndex(slotData, nextSlot.Slot_Format)
  let data = slotData.slice(0, index + 1)
  if (status) {
    homeAvailable.push(data)
    let nextArray = slotData.slice(index, slotData.length)
    await getAvailableHomeSlotArray(nextArray, stat)
  } else {
    homeUnavalale.push(data)
    let nextArray = slotData.slice(index, slotData.length)
    await getAvailableHomeSlotArray(nextArray, stat)
  }
}

// function for get index from value of an array
let getIndex = (array, value) => {
  let index = -1
  array.find(function (item, i) {
    if (item.Slot_Format == value) {
      index = i
      return i
    }
  })
  return index
}

// Function for get array in four-four hour parts 
let getPartsArray = (arr) => {
  let partArray = []
  for (var i = 0; i < arr.length; i++) {
    for (let j = arr[i].From_Time; j < arr[i].To_Time; j += 4) {
      let json = {
        From_Time: j,
        To_Time: j + 4 > arr[i].To_Time ? arr[i].To_Time : j + 4,
        Is_available: false
      }
      partArray.push(json)
    }
    if (i == arr.length - 1) {
      return partArray
    }
  }
}

module.exports = router
