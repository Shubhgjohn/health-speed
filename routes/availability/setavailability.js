/* eslint-disable eqeqeq */
/* eslint-disable no-unneeded-ternary */
/* eslint-disable no-async-promise-executor */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
var Router = require('express').Router
var router = new Router()
const model = require('../../models')
var moment = require('moment')
var { auditUid } = require('../../utils/UID')
var auditType = require('../../auditType.json')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const _ = require('lodash')

// route for set availability //
router.post('/create-avaivility', authorize([Role.Admin, Role.Provider]), async (req, res, next) => {
  var { body: { PRO_UID, PRO_Availability_From_Date, PRO_Availability_To_Date } } = req
  if (!PRO_UID) return res.send({ code: 0, status: 'failed', message: 'PRO_UID is required', Error_Code: model.Errors.PRO_UID_required() })
  if (!PRO_Availability_From_Date) return res.send({ code: 0, status: 'failed', message: 'PRO_Availability_From_Date is required', Error_Code: model.Errors.PRO_Availability_From_Date() })
  if (!PRO_Availability_To_Date) return res.send({ code: 0, status: 'failed', message: 'PRO_Availability_To_Date is required', Error_Code: model.Errors.PRO_Availability_To_Date() })
  var totalDays = await getTotalDays(PRO_Availability_From_Date, PRO_Availability_To_Date)
  if (totalDays.length == 0) return res.send({ code: 0, status: 'failed', message: 'Date is not selected correctly', Error_Code: 'HS013' })
  var count = 0
  var createArray = []
  var updateArray = []
  totalDays.map(async ele => {
    let date = new Date(ele)
    var weekDayName = moment(date).format('ddd')
    var providerAvailability = await getAvailability(req.body.PRO_UID, ele)
    if (!providerAvailability) {
      let slotData = await getSlotArray()
      let data = {
        PRO_UID: req.body.PRO_UID,
        PRO_Availability_Date: ele,
        PRO_Availability_Day: weekDayName,
        PRO_Availability_From_Time: req.body[weekDayName] && !_.isEmpty(req.body[weekDayName]) ? req.body[weekDayName].PRO_Availability_From_Time.split(' ')[0] : '9:00',
        PRO_Availability_From_Time_Zone: req.body[weekDayName] && !_.isEmpty(req.body[weekDayName]) ? req.body[weekDayName].PRO_Availability_From_Time.split(' ')[1] : 'am',
        PRO_Availability_To_Time: req.body[weekDayName] && !_.isEmpty(req.body[weekDayName]) ? req.body[weekDayName].PRO_Availability_To_Time.split(' ')[0] : '5:00',
        PRO_Availability_To_Time_Zone: req.body[weekDayName] && !_.isEmpty(req.body[weekDayName]) ? req.body[weekDayName].PRO_Availability_To_Time.split(' ')[1] : 'pm',
        PRO_Availability_Type: req.body[weekDayName] && !_.isEmpty(req.body[weekDayName]) ? req.body[weekDayName].PRO_Availability_Type : 'Office',
        PRO_Availability_Set_By: req.userType,
        PRO_Slots: slotData
      }
      createArray.push({
        data: data,
        date: ele
      })
      count = count + 1
      if (count == totalDays.length) {
        await createAudit(req, providerAvailability)
        await createAvailability(createArray, updateArray, res)
      }
    } else if (req.userType == 'Provider' || (req.userType == 'Admin' && providerAvailability.PRO_Availability_Set_By == 'Admin')) {
      let slotData = await getSlotArray()
      let data = {
        PRO_UID: req.body.PRO_UID,
        PRO_Availability_Date: ele,
        PRO_Availability_Day: weekDayName,
        PRO_Availability_From_Time: req.body[weekDayName] && !_.isEmpty(req.body[weekDayName]) ? req.body[weekDayName].PRO_Availability_From_Time.split(' ')[0] : providerAvailability.PRO_Availability_From_Time,
        PRO_Availability_From_Time_Zone: req.body[weekDayName] && !_.isEmpty(req.body[weekDayName]) ? req.body[weekDayName].PRO_Availability_From_Time.split(' ')[1] : providerAvailability.PRO_Availability_From_Time_Zone,
        PRO_Availability_To_Time: req.body[weekDayName] && !_.isEmpty(req.body[weekDayName]) ? req.body[weekDayName].PRO_Availability_To_Time.split(' ')[0] : providerAvailability.PRO_Availability_To_Time,
        PRO_Availability_To_Time_Zone: req.body[weekDayName] && !_.isEmpty(req.body[weekDayName]) ? req.body[weekDayName].PRO_Availability_To_Time.split(' ')[1] : providerAvailability.PRO_Availability_To_Time_Zone,
        PRO_Availability_Type: req.body[weekDayName] && !_.isEmpty(req.body[weekDayName]) ? req.body[weekDayName].PRO_Availability_Type : 'Office',
        PRO_Availability_Set_By: req.userType,
        PRO_Slots: slotData
      }
      count = count + 1
      updateArray.push({
        data: data,
        PRO_UID: PRO_UID
      })
      if (count == totalDays.length) {
        await createAudit(req, providerAvailability)
        await createAvailability(createArray, updateArray, res)
      }
    } else {
      count = count + 1
      if (count == totalDays.length) {
        return next()
      }
    }
  })
})
// function for create availability //
async function createAvailability (createData, updateData, res) {
  let createCount = 0
  if (createData.length == 0) {
    await updateAvalability(updateData, res)
  } else {
    createData.map(async ele => {
      await model.Availability.create(ele.data)
      createCount = createCount + 1
      if (createCount == createData.length) {
        await updateAvalability(updateData, res)
      }
    })
  }
}
// function for update availability //
async function updateAvalability (updateArray, res) {
  let updateCount = 0
  if (updateArray.length == 0) {
    res.send({ code: 1, status: 'sucess', message: 'Availability set successfully' })
  } else {
    updateArray.map(async ele => {
      await model.Availability.updateAvailability(ele.PRO_UID, ele.data)
      updateCount = updateCount + 1
      if (updateCount == updateArray.length) {
        res.send({ code: 1, status: 'sucess', message: 'Availability set successfully' })
      }
    })
  }
}
// function for get total days between start date to end date //
var getTotalDays = async (fromDate, toDate) => {
  return new Promise(async function (resolve, reject) {
    var dateArray = []
    var currentDate = moment(fromDate)
    var stopDate = moment(toDate)
    while (currentDate <= stopDate) {
      dateArray.push(moment(currentDate).format('MM-DD-YYYY'))
      currentDate = moment(currentDate).add(1, 'days')
    }
    resolve(dateArray)
  })
}
// function for get slot array //
var getSlotArray = async () => {
  return new Promise(async function (resolve, reject) {
    var arr = []; var i; var j
    for (i = 0; i < 24; i++) {
      for (j = 0; j < 2; j++) {
        arr.push({
          Slot_Time: `${(i > 12 ? i - 12 : i) + ':' + (j == 0 ? '00' : 30)}`,
          Slot_Time_Zone: `${i >= 12 ? 'pm' : 'am'}`,
          Slot_Format: `${j == 0 ? i : i + 0.50}`
        })
      }
    }
    resolve(arr)
  })
}
// function for get provider availability for date //
function getAvailability (PRO_UID, date) {
  return new Promise(async function (resolve, reject) {
    var availability = model.Availability.getProviderAvailability(PRO_UID, date)
    resolve(availability)
  }, 3000)
}
// Create audit log function //
async function createAudit (req, oldvalue) {
  var auditData = {
    Audit_UID: await auditUid(),
    Audit_Type: auditType.add,
    Audit_Info: 'New- ' + await auditUid(),
    Data_Point: req.userType == 'Admin' ? 'AP/create availibility' : 'PRO/create availibility ',
    Old_Value: oldvalue ? oldvalue : null,
    New_Value: req.body,
    IP_Address: await model.Audit.getIpAddress(req)
  }
  await model.Audit.create(auditData)
}

module.exports = router
