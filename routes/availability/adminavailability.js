/* eslint-disable eqeqeq */
/* eslint-disable no-async-promise-executor */
/* eslint-disable no-unneeded-ternary */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
var Router = require('express').Router
var router = new Router()
const model = require('../../models')
var moment = require('moment')
const logger = require('../../models/errorlog')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const _ = require('lodash')


// Service for create availability from admin side
router.post('/create', authorize(Role.Admin), async (req, res, next) => {
  var { body: { PRO_Availability_From_Date, PRO_Availability_To_Date } } = req
  if (!PRO_Availability_From_Date) return res.send({ code: 0, status: 'failed', message: 'PRO_Availability_From_Date is required', Error_Code: model.Errors.PRO_Availability_From_Date() })
  if (!PRO_Availability_To_Date) return res.send({ code: 0, status: 'failed', message: 'PRO_Availability_To_Date is required', Error_Code: model.Errors.PRO_Availability_To_Date() })
  let PRO_UID = req.body.PRO_UID ? req.body.PRO_UID : req.user.PRO_UID
  try {

    // Check provider account status
    const checkAccountStatus = await model.Provider.getProviderByPRO_UID(PRO_UID)
    if (!checkAccountStatus.StripeAccountId) {
      return res.status(400).send({
        status: 'failed',
        message: 'First you must setup a deposit account',
        Error_Code: 'HS012'
      })
    }

    // Get total days from dates
    let totalDays = await getTotalDays(PRO_Availability_From_Date, PRO_Availability_To_Date)
    if (totalDays.length == 0) return res.send({ code: 0, status: 'failed', message: 'Date is not selected correctly', Error_Code: 'HS013' })
    let count = 0
    totalDays.map(async ele => {
      let date = new Date(ele)

      // Get availability for the given date
      var providerAvailability = await getAvailability(req.body.PRO_UID, ele)
      var weekDayName = moment(date).format('ddd')

      let From_Time = req.body[weekDayName] && !_.isEmpty(req.body[weekDayName]) ? moment(`${req.body[weekDayName].PRO_Availability_From_Time}`, 'hh:mm A').format('HH.mm')
        : null
      let To_Time = req.body[weekDayName] && !_.isEmpty(req.body[weekDayName]) ? moment(`${req.body[weekDayName].PRO_Availability_To_Time}`, 'hh:mm A').format('HH.mm')
        : null
      To_Time = (To_Time != null && To_Time == '00.00') ? '24.00' : To_Time
      // if availability is not set in system then
      if ( From_Time != null && !providerAvailability ) {
        let slotData = await getSlotArray()
        slotData = slotData.map(ele => ({
          Slot_Time: ele.Slot_Time,
          Slot_Time_Zone: ele.Slot_Time_Zone,
          Slot_Format: ele.Slot_Format,
          Is_Home_Availability: req.body[weekDayName].PRO_Availability_Type == 'Home' || req.body[weekDayName].PRO_Availability_Type == 'Both' ? (ele.Slot_Format >= From_Time && ele.Slot_Format < To_Time) ? true : false : false,
          Is_Office_Availability: req.body[weekDayName].PRO_Availability_Type == 'Office' || req.body[weekDayName].PRO_Availability_Type == 'Both' ? (ele.Slot_Format >= From_Time && ele.Slot_Format < To_Time) ? true : false : false,
          Is_Home_Available: req.body[weekDayName].PRO_Availability_Type == 'Home' || req.body[weekDayName].PRO_Availability_Type == 'Both' ? (ele.Slot_Format >= From_Time && ele.Slot_Format < To_Time) ? true : false : false,
          Is_Office_Available: req.body[weekDayName].PRO_Availability_Type == 'Office' || req.body[weekDayName].PRO_Availability_Type == 'Both' ? (ele.Slot_Format >= From_Time && ele.Slot_Format < To_Time) ? true : false : false
        }))
        let json = {
          PRO_UID: PRO_UID,
          PRO_Availability_Date: ele,
          PRO_Availability_Day: moment(new Date(ele)).format('ddd'),
          PRO_Availability_Set_By: req.userType,
          PRO_Slots: slotData
        }

        // Create availability 
        await model.ProviderAvailability.create(json)
        let availability = await model.Availability.getProviderAvailability(req.body.PRO_UID, ele)
        if (!availability) {
          let data = await getProviderAvailabilityData(req, ele, req.body[weekDayName])
          await model.Availability.create(data)
        }
        count = count + 1
        if (count == totalDays.length) {
          res.send({ code: 1, status: 'sucess', message: 'Availability set successfully' })
        }
      } else {
        // if availability is set and it is set by admin
        if (From_Time != null && providerAvailability.PRO_Availability_Set_By == 'Admin') {
          let slotData = await getSlotArray()
          slotData = slotData.map(ele => ({
            Slot_Time: ele.Slot_Time,
            Slot_Time_Zone: ele.Slot_Time_Zone,
            Slot_Format: ele.Slot_Format,
            Is_Home_Availability: req.body[weekDayName].PRO_Availability_Type == 'Home' || req.body[weekDayName].PRO_Availability_Type == 'Both' ? (ele.Slot_Format >= From_Time && ele.Slot_Format < To_Time) ? true : false : false,
            Is_Office_Availability: req.body[weekDayName].PRO_Availability_Type == 'Office' || req.body[weekDayName].PRO_Availability_Type == 'Both' ? (ele.Slot_Format >= From_Time && ele.Slot_Format < To_Time) ? true : false : false,
            Is_Home_Available: req.body[weekDayName].PRO_Availability_Type == 'Home' || req.body[weekDayName].PRO_Availability_Type == 'Both' ? (ele.Slot_Format >= From_Time && ele.Slot_Format < To_Time) ? true : false : false,
            Is_Office_Available: req.body[weekDayName].PRO_Availability_Type == 'Office' || req.body[weekDayName].PRO_Availability_Type == 'Both' ? (ele.Slot_Format >= From_Time && ele.Slot_Format < To_Time) ? true : false : false
          }))

          await model.ProviderAvailability.updateAvailability(PRO_UID, ele, slotData)
          let availability = await model.Availability.getProviderAvailability(req.body.PRO_UID, ele)
          if (!availability) {
            let data = await getProviderAvailabilityData(req, ele, req.body[weekDayName])
            await model.Availability.create(data)
          }
          count = count + 1
          if (count == totalDays.length) {
            res.send({ code: 1, status: 'sucess', message: 'Availability set successfully' })
          }
        } else {
          count = count + 1
          if (count == totalDays.length) {
            res.send({ code: 1, status: 'sucess', message: 'Availability set successfully' })
          }
        }
      }
    })
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.status(422).send({ code: 422, status: 'failed', message: error.message, Error_Code: 'HS014' })
  }
})

// function for get availability for the date
function getAvailability (PRO_UID, date) {
  return new Promise(async function (resolve, reject) {
    var availability = model.ProviderAvailability.getProviderAvailability(PRO_UID, date)
    resolve(availability)
  }, 3000)
}

// function for get default slot array
var getSlotArray = async () => {
  return new Promise(async function (resolve, reject) {
    var arr = []; var i; var j
    for (i = 0; i < 24; i++) {
      for (j = 0; j < 2; j++) {
        arr.push({
          Slot_Time: `${(i > 12 ? i - 12 : i) + ':' + (j == 0 ? '00' : 30)}`,
          Slot_Time_Zone: `${i >= 12 ? 'pm' : 'am'}`,
          Slot_Format: j == 0 ? i : i + 0.50,
          Is_Home_Availability: false,
          Is_Office_Availability: false,
          Is_Home_Available: false,
          Is_Office_Available: false
        })
      }
    }
    resolve(arr)
  })
}

// function for get total days between start date to end date //
var getTotalDays = async (fromDate, toDate) => {
  return new Promise(async function (resolve, reject) {
    var dateArray = []
    var currentDate = moment(fromDate)
    var stopDate = moment(toDate)
    while (currentDate <= stopDate) {
      dateArray.push(moment(currentDate).format('MM-DD-YYYY'))
      currentDate = moment(currentDate).add(1, 'days')
    }
    resolve(dateArray)
  })
}

// Get availability data fro create
let getProviderAvailabilityData = async (req, PRO_Availability_Date, PRO_Slots) => {
  PRO_Slots.PRO_Availability_From_Time = PRO_Slots.PRO_Availability_From_Time ? PRO_Slots.PRO_Availability_From_Time : '9:00 am'
  PRO_Slots.PRO_Availability_To_Time = PRO_Slots.PRO_Availability_To_Time ? PRO_Slots.PRO_Availability_To_Time : '5:00 pm'
  PRO_Slots.PRO_Availability_Type = PRO_Slots.PRO_Availability_Type ? PRO_Slots.PRO_Availability_Type : 'Both'
  let availabilityData = await getAvailabilitySlotArray()
  let data = {
    PRO_UID: req.body.PRO_UID,
    PRO_Availability_Date: PRO_Availability_Date,
    PRO_Availability_Day: moment(new Date(PRO_Availability_Date)).format('ddd'),
    PRO_Availability_From_Time: PRO_Slots.PRO_Availability_From_Time.split(' ')[0],
    PRO_Availability_From_Time_Zone: PRO_Slots.PRO_Availability_From_Time.split(' ')[1],
    PRO_Availability_To_Time: PRO_Slots.PRO_Availability_To_Time.split(' ')[0],
    PRO_Availability_To_Time_Zone: PRO_Slots.PRO_Availability_To_Time.split(' ')[1],
    PRO_Availability_Type: PRO_Slots.PRO_Availability_Type,
    PRO_Availability_Set_By: req.userType,
    PRO_Slots: availabilityData
  }
  return data
}


// function for get slots
var getAvailabilitySlotArray = async () => {
  return new Promise(async function (resolve, reject) {
    var arr = []; var i; var j
    for (i = 0; i < 24; i++) {
      for (j = 0; j < 2; j++) {
        arr.push({
          Slot_Time: `${(i > 12 ? i - 12 : i) + ':' + (j == 0 ? '00' : 30)}`,
          Slot_Time_Zone: `${i >= 12 ? 'pm' : 'am'}`,
          Slot_Format: `${j == 0 ? i : i + 0.50}`,
          Is_Home_Available: true,
          Is_Office_Available: true,
          Is_Travel_Time: false
        })
      }
    }
    resolve(arr)
  })
}
module.exports = router
