/* eslint-disable prefer-const */
/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const model = require('../../models')
const authorize = require('../../_helpers/authorize')
const logger = require('../../models/errorlog')
const Role = require('../../_helpers/role')
const moment = require('moment')

// Get the list of audits log with all filters, sorting and search funcationality //
router.post('/get-audit-logs', authorize(Role.Admin), async (req, res) => {
  var filters = req.body
  filters.start_date = moment(filters.start_date).format('YYYY-MM-DD')
  filters.end_date = moment(filters.end_date).format('YYYY-MM-DD')
  try {

    // Get all audit logs with filter data
    var data = await model.Audit.getAuditLogs(filters)
    if (!data) return res.status(200).send({ code: 200, status: 'success', message: 'No record found.' })
    res.status(200).send({ code: 200, status: 'success', data: data })
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.status(422).send({ code: 422, status: 'failed', message: error.message })
  }
})
// Get the all data of audits log for export //
router.post('/export-csv', authorize(Role.Admin), async (req, res) => {
  let filters = req.body

  // Get CSV data
  var export_audit = await model.Audit.getExportCSV(filters)
  if (export_audit.length == 0) return res.status(200).send({ code: 200, status: 'success', message: 'No Result' })
  res.status(200).send({ code: 200, status: 'success', data: export_audit })
})

// Get all provider for listing in audit logs //
router.get('/get-provider/:AP_Audit_Providers_Search?', authorize(Role.Admin), async (req, res) => {
  try {
    const AP_Audit_Providers_Search = req.params.AP_Audit_Providers_Search
    var provider = await model.Provider.providerForAuditLogs(AP_Audit_Providers_Search)
    res.status(200).send({ code: 200, status: 'success', data: provider })
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.status(422).send({ code: 422, status: 'failed', message: error.message })
  }
})
// Get all patient for listing in audit logs //
router.get('/get-patient/:AP_Audit_Patients_Search?', authorize(Role.Admin), async (req, res) => {
  const AP_Audit_Patients_Search = req.params.AP_Audit_Patients_Search
  try {
    var patient = await model.Patient.patientForAuditLog(AP_Audit_Patients_Search)
    res.status(200).send({ code: 200, status: 'success', data: patient })
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.status(422).send({ code: 422, status: 'failed', message: error.message })
  }
})

module.exports = router
