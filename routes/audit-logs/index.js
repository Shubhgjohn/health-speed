var express = require('express')
var app = express()
var auditLogs = require('./auditlogs')
app.use(auditLogs)

module.exports = app
