/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const moment = require('moment')
const model = require('../../models')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const logger = require('../../models/errorlog')
// const auditType = require('../../auditType.json')
const momentTz = require('moment-timezone')

// Update provider travel time //
router.put('/update_travel', authorize(Role.Provider), async (req, res) => {
	const { body: { travel_time } } = req
	try {
		var disableCount = 0; var enableCount = 0
		if (travel_time >= req.user.PRO_Home_Visit_Buffer) {
			await model.Provider.updateById(req.user.PRO_UID, { PRO_Home_Visit_Buffer: travel_time })
			res.send({ code: 1, status: 'sucess', message: 'provider update successfully' })
		} else {
			let disableSlotCount = req.user.PRO_Home_Visit_Buffer / 60
			let currentSlotCount = travel_time / 30
			let allEncounters = await model.Encounter.findScheduledEncounter(req.user.PRO_UID)
			if (allEncounters.length > 0) {
				var upcomingEncounter = allEncounters[0].encounter.filter(ele => Date.parse(moment(new Date(`${ele.Encounter_Date} ${ele.Encounter_Time}`)).format('YYYY-MM-DDTHH:mm:ss')) >= Date.parse(momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm:ss')))
				const dateFromStr = str => new Date('1970/01/01 ' + str)
				let updatedEncounter = upcomingEncounter.sort((a, b) => dateFromStr(a.Encounter_Complete_Time) - dateFromStr(b.Encounter_Complete_Time))
				if (updatedEncounter.length == 0) {
					await model.Provider.updateById(req.user.PRO_UID, { PRO_Home_Visit_Buffer: travel_time })
					return res.send({ code: 1, status: 'success', message: 'No upcomming encounter for you to update travel time.' })
				}
				updatedEncounter.map(async ele => {
					await disabledSlots(req.user.PRO_UID, ele, disableSlotCount)
					disableCount = disableCount + 1
					if (disableCount == updatedEncounter.length) {
						updatedEncounter.map(async element => {
							await enableSlots(req.user.PRO_UID, element, currentSlotCount)
							enableCount = enableCount + 1
							if (enableCount == updatedEncounter.length) {
								await model.Provider.updateById(req.user.PRO_UID, { PRO_Home_Visit_Buffer: travel_time })
								res.send({ code: 1, status: 'sucess', message: 'provider update successfully' })
							}
						})
					}
				})
			} else {
				await model.Provider.updateById(req.user.PRO_UID, { PRO_Home_Visit_Buffer: travel_time })
				res.send({ code: 1, status: 'sucess', message: 'provider update successfully' })
			}
		}
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({
			code: 2,
			status: 'failed',
			message: error.message,
			Error_Code: 'HS311'
		})
	}
})

var disabledSlots = async (id, data, slotCount) => {
	let availability = await model.Availability.getProviderAvailability(id, data.Encounter_Date)
	if (data.Encounter_Location_Type != availability.PRO_Availability_Type) {
		var Start_Time = moment(`${data.Encounter_startComplete_Time}`, 'hh:mm A').format('HH.mm')
		var End_Time = moment(`${data.Encounter_endComplete_Time}`, 'hh:mm A').format('HH.mm')
		Start_Time = Start_Time.includes(30) ? parseFloat(Start_Time) + 0.20 : parseFloat(Start_Time)
		End_Time = End_Time.includes(30) ? parseFloat(End_Time) + 0.20 : parseFloat(End_Time)
		Start_Time = Start_Time - slotCount
		End_Time = End_Time + slotCount

		var slots = await availability.PRO_Slots.filter(ele => ((ele.Slot_Format >= Start_Time) && (ele.Slot_Format < End_Time)))
		var startIndex = slots.findIndex(function (item, i) {
			return (item.Slot_Time === data.Encounter_Start_Time && item.Slot_Time_Zone === data.Encounter_Start_Time_Zone)
		})
		var endIndex = slots.findIndex(function (item, i) {
			return (item.Slot_Time === data.Encounter_End_Time && item.Slot_Time_Zone === data.Encounter_End_Time_Zone)
		})
		for (var i = 0; i < slots.length; i++) {
			if ((i >= 0 && i < startIndex) || (i >= endIndex && i < slots.length)) {
				var time = moment(`${slots[i].Slot_Time} ${slots[i].Slot_Time_Zone}`, 'hh:mm A').format('HH.mm')
				time = time.includes(30) ? parseFloat(time) + 0.20 : parseFloat(time)
				await model.Availability.updateSlot(id, data.Encounter_Date, time)
				if (i == slots.length - 1) {
					return
				}
			} else {
				if (i == slots.length - 1) {
					return
				}
			}
		}
	} else {
		return true
	}
}

var enableSlots = async (id, data, slotCount) => {
	let availability = await model.Availability.getProviderAvailability(id, data.Encounter_Date)

	if (data.Encounter_Location_Type != availability.PRO_Availability_Type) {
		var Start_Time = moment(`${data.Encounter_startComplete_Time}`, 'hh:mm A').format('HH.mm')
		var End_Time = moment(`${data.Encounter_endComplete_Time}`, 'hh:mm A').format('HH.mm')
		Start_Time = Start_Time.includes(30) ? parseFloat(Start_Time) + 0.20 : parseFloat(Start_Time)
		End_Time = End_Time.includes(30) ? parseFloat(End_Time) + 0.20 : parseFloat(End_Time)
		Start_Time = Start_Time - slotCount
		End_Time = End_Time + slotCount

		var slots = await availability.PRO_Slots.filter(ele => ((ele.Slot_Format >= Start_Time) && (ele.Slot_Format < End_Time)))
		let startIndex = slots.findIndex(function (item, i) {
			return (item.Slot_Time === data.Encounter_Start_Time && item.Slot_Time_Zone === data.Encounter_Start_Time_Zone)
		})
		let endIndex = slots.findIndex(function (item, i) {
			return (item.Slot_Time === data.Encounter_End_Time && item.Slot_Time_Zone === data.Encounter_End_Time_Zone)
		})
		for (var i = 0; i < slots.length; i++) {
			if ((i >= 0 + slotCount && i < startIndex) || (i >= endIndex && i < slots.length - slotCount)) {
				var time = moment(`${slots[i].Slot_Time} ${slots[i].Slot_Time_Zone}`, 'hh:mm A').format('HH.mm')
				time = time.includes(30) ? parseFloat(time) + 0.20 : parseFloat(time)
				await model.Availability.updateAvailableSlot(id, data.Encounter_Date, time)
				if (i == slots.length - 1) {
					return
				}
			} else {
				if (i == slots.length - 1) {
					return
				}
			}
		}
	} else {
		return true
	}
}
module.exports = router
