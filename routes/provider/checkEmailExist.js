/* eslint-disable no-unneeded-ternary */
/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const model = require('../../models')
const errorModule = require('../../models').Errors
var auditType = require('../../auditType.json')
var { auditUid } = require('../../utils/UID')
const logger = require('../../models/errorlog')

// Service for check provider exist or not at the time of registration //
router.post('/check-provider-exist', async (req, res) => {
	var { body: { PRO_Username } } = req
	if (!PRO_Username) return res.send({ code: 422, status: 'failed', message: '*Don’t forget to enter your email! It’s required.', Error_Code: errorModule.enter_provider_email() })

	PRO_Username = PRO_Username.toLowerCase()
	try {
		let provider = await model.Provider.findOneByEmail(PRO_Username)
		const auditData = {
			Audit_UID: await auditUid(),
			Audit_Type: auditType.confirm,
			Data_Point: 'PRO/Signup',
			Audit_Info: 'New- ' + await auditUid(),
			New_Value: PRO_Username,
			IP_Address: await model.Audit.getIpAddress(req)
		}
		await model.Audit.create(auditData)
		if (provider) {
			return res.status(200).send({
				code: 1,
				status: 'sucess',
				isExist: true,
				isBiometricEnable: provider.PRO_Biometric_Login == 'Disabled' ? false : true,
				message: '*This username is already associated with another account. Please log in.'
			})
		} else {
			return res.status(200).send({
				code: 0,
				status: 'failed',
				isExist: false,
				message: '*Provider with this email is not found in the system.',
				Error_Code: errorModule.provider_not_found()
			})
		}
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS300' })
	}
})

module.exports = router
