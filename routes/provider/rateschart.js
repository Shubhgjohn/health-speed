/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
/* eslint-disable prefer-const */
const Router = require('express').Router
const router = new Router()
const model = require('../../models')
const moment = require('moment')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
// const auditModule = require('../../models').Audit
// const { auditUid } = require('../../utils/UID')
// const auditType = require('../../auditType.json')
const fcm = require('../../_helpers/fcm')
const logger = require('../../models/errorlog')
const _ = require('lodash')

// get data for the monthly earning chart
router.post('/get_monthly_earning', authorize(Role.Provider), async (req, res) => {
  const { body: { month, year } } = req
  const dataArray = []
  if (!month) return res.send({ code: 1, status: 'failed', message: 'Month is required for display chart', Error_Code: 'HS206' })
  if (!year) return res.send({ code: 1, status: 'failed', message: 'Year is required for display chart', Error_Code: 'HS207' })
  try {
    let providerCreatedMonth = req.user.Created_At.getMonth() + 1
    let providerCreatedYear = req.user.Created_At.getUTCFullYear()
    let currentDate = new Date()
    var my_date = new Date(`${year}, ${month}`)
    var first_date = `${year}-${month}-01`
    var last_date = new Date(my_date.getFullYear(), my_date.getMonth() + 1, 0)
    if (req.headers.host.includes('localhost')) {
      last_date = last_date.setDate(last_date.getDate() + 1)
    }
    if (providerCreatedYear <= year && year <= currentDate.getUTCFullYear() && (year >= providerCreatedYear || providerCreatedMonth > month)) {
      let total_earning = await model.Transactions.getTotalEarning(req.user.PRO_UID)
      total_earning = await getTotalEarning(total_earning)
      if (total_earning > 0) {
        first_date = moment(new Date(first_date)).format('YYYY-MM-DD')
        last_date = new Date(last_date)
        last_date.setUTCHours(23)
        last_date.setUTCMinutes(59)
        last_date.setUTCSeconds(59)
        let monthEarningData = await model.Transactions.getMonthData(req.user.PRO_UID, new Date(first_date), new Date(last_date))
        let homeVisits = monthEarningData.filter(ele => ele.encounter.Encounter_Location_Type == 'Home')
        let officeVisits = monthEarningData.filter(ele => ele.encounter.Encounter_Location_Type == 'Office')
        if (monthEarningData.length != 0) {
          let totalMonthEarning = 0
          let totalHomeEarning = 0
          let totalOfficeEarning = 0
          let totalDays = getDaysInMonth(month, year)
          let currentMonth = new Date().getUTCMonth() + 1
          let currentDay = new Date().getDate()
          totalDays = month == currentMonth ? currentDay : totalDays
          let providerCreatedDate = req.user.Created_At.getDate()
          let startIndex = month == providerCreatedMonth ? providerCreatedDate : 1
          for (let i = startIndex; i <= totalDays; i++) {
            let date = `${month < 10 ? `0${month}` : month}-${i < 10 ? `0${i}` : i}-${year}`
            let value = monthEarningData.filter(ele => moment(ele.Created_At).format('MM-DD-YYYY') == date)
            if (value.length > 0) {
              let EarnValue = await getEarnValue(value)
              totalMonthEarning = totalMonthEarning + EarnValue[0] + EarnValue[1]
              totalHomeEarning = totalHomeEarning + EarnValue[0]
              totalOfficeEarning = totalOfficeEarning + EarnValue[1]
              dataArray.push({
                date: `${i}/${month}/${year}`,
                earning: totalMonthEarning,
                home_earn_value: EarnValue[0],
                office_earn_value: EarnValue[1]
              })
            } else {
              dataArray.push({
                date: `${i}/${month}/${year}`,
                earning: totalMonthEarning,
                home_earn_value: 0,
                office_earn_value: 0
              })
            }

            if (i == totalDays) {
              return res.status(200).send({
                code: 1,
                status: 'sucess',
                data: dataArray,
                total_this_month_Earning: totalMonthEarning,
                total_earning: total_earning,
                home_earning: totalHomeEarning,
                office_earning: totalOfficeEarning,
                home_visit: homeVisits.length,
                office_visit: officeVisits.length,
                total_visit: homeVisits.length + officeVisits.length,
                Is_Earning: true
              })
            }
          }
        } else {
          let totalDays = getDaysInMonth(month, year)
          let currentMonth = new Date().getUTCMonth() + 1
          let currentDay = new Date().getDate()
          totalDays = month == currentMonth ? currentDay : totalDays
          let providerCreatedDate = req.user.Created_At.getDate()
          let startIndex = month == providerCreatedMonth ? providerCreatedDate : 1
          for (let i = startIndex; i <= totalDays; i++) {
            dataArray.push({
              date: `${i}/${month}/${year}`,
              earning: 0,
              home_earn_value: 0,
              office_earn_value: 0
            })
          }
          return res.status(200).send({
            code: 1,
            status: 'sucess',
            data: dataArray,
            total_this_month_Earning: 0,
            total_earning: total_earning,
            home_earning: 0,
            office_earning: 0,
            home_visit: homeVisits.length,
            office_visit: officeVisits.length,
            total_visit: homeVisits.length + officeVisits.length,
            Is_Earning: false
          })
        }
      } else {
        const totalDays = getDaysInMonth(month, year)
        for (let i = 1; i <= totalDays; i++) {
          dataArray.push({
            date: `${i}/${month}/${year}`,
            earning: 0,
            home_earn_value: 0,
            office_earn_value: 0
          })
        }
        return res.status(200).send({
          code: 1,
          status: 'sucess',
          data: dataArray,
          total_this_month_Earning: 0,
          total_earning: 0,
          home_earning: 0,
          office_earning: 0,
          home_visit: 0,
          office_visit: 0,
          total_visit: 0,
          Is_Earning: false
        })
      }
    } else {
      return res.send({ code: 0, status: 'failed', message: 'No data found', Error_Code: 'HS208' })
    }
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS308' })
  }
})

let getTotalEarning = async (earningArray) => {
  let total = 0
  for (let index = 0; index < earningArray.length; index++) {
    const element = earningArray[index]
    if (element.encounter.length > 0) {
      total = total + element.Amount
    }
    if (index == earningArray.length - 1) {
      return total
    }
  }
}

router.get('/pro-monthly-earning', async (req, res) => {
  try {
    let month = new Date().getMonth() + 1
    let year = month === 1 ? new Date().getFullYear() - 1 : new Date().getFullYear()
    let filterData = await model.Provider.getActiveProvider()
    if (filterData.length === 0) return res.send('success')
    let count = 0
    filterData.map(async ele => {
      let providerCreatedMonth = ele.Created_At.getMonth() + 1
      let providerCreatedYear = ele.Created_At.getUTCFullYear()
      let currentDate = new Date()
      var my_date = new Date(`${year}, ${month}`)
      var first_date = new Date(my_date.getFullYear(), my_date.getMonth() - 1 , 1)
      var last_date = new Date(my_date.getFullYear(), my_date.getMonth(), 0)
      first_date = first_date.setDate(first_date.getDate() + 1)
      last_date = last_date.setDate(last_date.getDate() + 1)

      if (providerCreatedYear <= year && year <= currentDate.getUTCFullYear() && (year > providerCreatedYear || providerCreatedMonth > month)) {
        let monthEarningData = await model.Transactions.getMonthData(ele.PRO_UID, new Date(first_date), new Date(last_date))

        if (monthEarningData.length !== 0) {
          let totalMonthEarning = 0
          const totalDays = getDaysInMonth(month, year)
          for (var i = 1; i <= totalDays; i++) {
            let date = `${month < 10 ? `0${month}` : month}-${i < 10 ? `0${i}` : i}-${year}`
            let value = monthEarningData.filter(item => moment(item.Created_At).format('MM-DD-YYYY') == date)
            if (value.length > 0) {
              let EarnValue = await getEarnValue(value)
              totalMonthEarning = totalMonthEarning + EarnValue[0] + EarnValue[1]
            }
            if (i == totalDays) {
              let arr = []
              let title = 'Your Monthly Earnings Have Been Updated'
              let body = `You earned ${totalMonthEarning} this month. Check out how that compares to previous months.`
              let PRO_Token = await model.Notification.getToken('Provider', ele.PRO_Username)
              PRO_Token.map(item => arr.push(item.Device_Token))
              let josnData = {
                month: month.toString(),
                year: year.toString(),
                title: 'Monthly Earnings'
              }
              console.log('zzzz', totalMonthEarning)

              if (totalMonthEarning > 0 && arr.length > 0) {
                let payload = {
                  registration_ids: arr,
                  notification: {
                    title: title,
                    body: body
                  },
                  data: josnData
                }
                fcm.send(payload, function (error, response) {
                  if (error) {
                    console.log(error)
                  } else {
                    console.log(response)
                  }
                })
              }
            }
          }
        }
      }
      count = count + 1
      if (count === filterData.length) return res.send('success')
    })
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS309' })
  }
})

async function getEarnValue (value) {
  let home = 0
  let office = 0
  for (var i = 0; i < value.length; i++) {
    if (value[i].encounter.Encounter_Location_Type == 'Home' && value[i].Transaction_Status == 'Paid') {
      home = home + value[i].Amount
    }
    if (value[i].encounter.Encounter_Location_Type == 'Office' && value[i].Transaction_Status == 'Paid') {
      office = office + value[i].Amount
    }
    if (i == value.length - 1) {
      return [home, office]
    }
  }
}
// function for get total days in month //
var getDaysInMonth = (month, year) => {
  return new Date(year, month, 0).getDate()
}
// service for get transaction //
router.get('/get-transaction', authorize([Role.Provider]), async (req, res) => {
  try {
    let data = await model.Encounter.getEncounters(req.user.PRO_UID)
    if (data.length == 0) {
      return res.send({ code: 0, status: 'failed', data: [], message: 'No Transaction found for this encounter', Error_Code: 'HS209' })
    } else {
      data = _.uniqBy(data, function (e) {
        return e.Encounter_UID
      })
      return res.send({ code: 1, status: 'sucess', data: data })
    }
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS310' })
  }
})
module.exports = router
