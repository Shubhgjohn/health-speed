/* eslint-disable quotes */
/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const jwt = require('jsonwebtoken')
const model = require('../../models')
const errorModule = require('../../models').Errors
var { auditUid } = require('../../utils/UID')
var auditType = require('../../auditType.json')
const { bcryptVerify } = require('../../utils/crypto')
const moment = require('moment')
const logger = require('../../models/errorlog')
// Service for provider login //
router.post('/login', async (req, res) => {
	var { body: { PRO_Username, PRO_Password } } = req
	if (!PRO_Username) return res.send({ code: 0, status: 'failed', message: '*Don’t forget to enter your email! It’s required to login.', Error_Code: errorModule.enter_your_email() })
	if (!PRO_Password) return res.send({ code: 0, status: 'failed', message: '*Don’t forget to enter your password! It’s required to login.', Error_Code: errorModule.enter_your_password() })

	PRO_Username = PRO_Username.toLowerCase()

	try {
		let provider = await model.Provider.findOneByEmail(PRO_Username)
		if (!provider) {
			return res.send({ code: 0, status: 'failed', message: `*Are you sure that's the correct email? Try again or sign up!`, Error_Code: 'HS193', PRO_Account_Status: null })
		} else {
			if (provider.PRO_Account_Status == 'Banned') return res.send({ code: 0, status: 'failed', message: 'Please contact HealthSpeed.', Error_Code: 'HS194', PRO_Account_Status: null })
			if (!provider.PRO_Password) {
				return res.send({
					code: 0,
					status: 'failed',
					message: `*Are you sure that's the correct password? Try again or try resetting it.`,
					PRO_Account_Status: null
				})
			}
			var isPasswordTrue = bcryptVerify(PRO_Password, provider.PRO_Password)
			if (isPasswordTrue) {
				const tokenPayload = {
					PRO_Id: provider.PRO_UID,
					PRO_Username: provider.PRO_Username,
					roles: 'Provider'
				}
				const token = jwt.sign(tokenPayload, process.env.JWT_SECRET, { expiresIn: 60 * 60 * 24 })
				await model.Provider.updateLastLogin(provider.PRO_Username)
				const auditData = {
					Audit_UID: await auditUid(),
					Audit_Type: auditType.login,
					Data_Point: 'PRO/Login',
					PRO_First_Name: provider.PRO_First_Name,
					PRO_Last_Name: provider.PRO_Last_Name,
					PRO_UID: provider.PRO_UID,
					Audit_Info: 'New- ' + await auditUid(),
					IP_Address: await model.Audit.getIpAddress(req)
				}
				await model.Audit.create(auditData)
				return res.status(200).send({
					code: 1,
					status: 'sucess',
					data: token,
					date: moment(new Date()).format('MM-DD-YYYY'),
					PRO_Account_Status: provider.PRO_Account_Status
				})
			} else {
				return res.send({ code: 0, status: 'failed', message: `*Are you sure that's the correct password? Try again or try resetting it.`, Error_Code: 'HS195', PRO_Account_Status: provider.PRO_Account_Status })
			}
		}
	} catch (err) {
		logger.error(err.message)
		logger.fatal(err)
		return res.send({ code: 2, status: 'failed', message: err.message, Error_Code: 'HS196' })
	}
})

module.exports = router
