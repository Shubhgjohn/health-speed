/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
const Router = require('express').Router
const router = new Router()
const model = require('../../models')
const moment = require('moment')
const authorize = require('../../_helpers/authorize')
// var { auditUid } = require('../../utils/UID')
// var auditType = require('../../auditType.json')
const Role = require('../../_helpers/role')
const logger = require('../../models/errorlog')
// const momentTz = require('moment-timezone')

// service for get patient encounter by provider UID //
router.get('/get_encounter_patient', authorize(Role.Provider), async (req, res, next) => {
  try {
    let patients = await model.Encounter.getEncounterPatients(req.user.PRO_UID)
    if (patients.length > 0) {
      await sendPatient(patients, res)
    } else {
      res.send({ code: 0, status: 'failed', data: [], Error_Code: 'HS192' })
    }
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS301' })
  }
})

let sendPatient = async (array, res) => {
  if (array.length > 0) {
    let patientDetails = array.map(ele => ({
      Encounter_UID: ele.Encounter_UID,
      Encounter_Date: ele.Encounter_Date,
      Encounter_Followup_Limit: ele.Encounter_Followup_Limit,
      PT_Username: ele.patient.PT_Username,
      PT_HS_MRN: ele.patient.PT_HS_MRN,
      PT_First_Name: ele.patient.PT_First_Name ? ele.patient.PT_First_Name : '',
      PT_Last_Name: ele.patient.PT_Last_Name ? ele.patient.PT_Last_Name : '',
      PT_Age: ele.patient.PT_DOB ? new Date().getFullYear() - ele.patient.PT_DOB.getFullYear() : null,
      PT_DOB: moment(new Date(ele.patient.PT_DOB)).format('MM-DD-YYYY'),
      PT_Gender: ele.patient.PT_Gender
    }))
    let uniqueData = []; let arr = []
    patientDetails.map(ele => {
      if (!arr.includes(ele.PT_HS_MRN)) {
        uniqueData.push(ele)
      }
      arr.push(ele.PT_HS_MRN)
    })
    res.send({ code: 1, status: 'sucess', data: uniqueData })
  } else {
    res.send({ code: 0, status: 'failed', data: [], Error_Code: 'HS192' })
  }
}

module.exports = router
