/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const model = require('../../models')
const { bcryptHash } = require('../../utils/crypto')
const { auditUid } = require('../../utils/UID')
var auditType = require('../../auditType.json')
const jwt = require('jsonwebtoken')
const errorModule = require('../../models').Errors
const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_PRIVATE_KEY)
const moment = require('moment')
var randomize = require('randomatic')
const logger = require('../../models/errorlog')

// Service for register a new user as provider //
router.post('/register', async (req, res, next) => {
	var { body: { PRO_Username, PRO_Office_Zip, PRO_Password, isEnableBiomatric } } = req
	if (!PRO_Username) return res.status(422).send({ code: 0, status: 'failed', message: '*Don’t forget to enter your email! It’s required to sign up.', Error_Code: errorModule.enter_your_email() })
	if (!PRO_Office_Zip) return res.status(422).send({ code: 0, status: 'failed', message: '*Don’t forget to enter your zip code! It’s required to sign up.', Error_Code: 'HS210' })
	if (!PRO_Password) return res.status(422).send({ code: 0, status: 'failed', message: '*Don’t forget to enter your password! It’s required to sign up.', Error_Code: errorModule.enter_your_password() })
	// if (!PRO_Private_Key) return res.status(422).send({ code: 0, status: 'failed', message: 'PRO_Private_Key is required' , Error_Code: });
	// if (!isEnableBiomatric) return res.status(422).send({ code: 422, status: 'failed', message: 'isEnableBiomatric is required' , Error_Code: });
	req.body.PRO_Username = PRO_Username.toLowerCase()
	var reg = /^(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$#!%^*?&]{10,45}$/
	if (!reg.test(PRO_Password)) {
		return res.send({ code: 0, status: 'failed', message: '*Please make sure the password meets all of the criteria below.', Error_Code: errorModule.make_sure_criteria() })
	}
	try {
		let provider = await model.Provider.findOneByEmail(req.body.PRO_Username)
		if (provider) {
			return res.send({ code: 0, status: 'failed', message: '*This username is already associated with another account. Please log in.', Error_Code: errorModule.username_already_associated() })
		} else {
			let hashPassword = await bcryptHash(PRO_Password, 10)
			req.body.PRO_Password = hashPassword
			req.body.PRO_Biometric_Login = isEnableBiomatric ? 'Enabled' : 'Disabled'
			var allProvider = await model.Provider.lastInserted()
			let Stripecustomer = await model.Stripe.createCutomer(req.body.PRO_Username)
			// let account = await model.Stripe.createStripeAccount(req.body.PRO_Username);
			// req.body.StripeAccountId = account.id;
			req.body.stripeCustomerId = Stripecustomer.id
			if (allProvider.length == 0) {
				let number = randomize('0', 8)
				let patternNumber = number.match(/.{1,4}/g)
				let id = patternNumber.join('-')
				req.body.PRO_UID = `PRO-${id}`
				let newProvider = await model.Provider.create(req.body)
				let token = getToken(req.body.PRO_Username, req.body.PRO_UID)
				delete newProvider._doc.PRO_Password
				await createAudit(req, newProvider._doc)
				await sendMail(req.body.PRO_Username, newProvider.PRO_UID, PRO_Office_Zip)
				return res.status(200).send({
					code: 1,
					status: 'sucess',
					data: token,
					date: moment(new Date()).format('MM-DD-YYYY')
				})
			} else {
				let nextId = await getNextId()
				req.body.PRO_UID = nextId
				var newProvider = await model.Provider.create(req.body)
				let token = getToken(req.body.PRO_Username, req.body.PRO_UID)
				delete newProvider._doc.PRO_Password
				await createAudit(req, newProvider._doc)
				await sendMail(req.body.PRO_Username, newProvider.PRO_UID, PRO_Office_Zip)
				return res.status(200).send({
					code: 1,
					status: 'sucess',
					data: token,
					date: moment(new Date()).format('MM-DD-YYYY')
				})
			}
		}
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS211' })
	}
})
// Function for generate unique id for provider //
async function getNextId () {
	let number = randomize('0', 8)
	let patternNumber = number.match(/.{1,4}/g)
	let id = patternNumber.join('-')
	let PRO_UID = `PRO-${id}`
	let provider = await model.Provider.findById(PRO_UID)
	if (provider) {
		getNextId()
	} else {
		return PRO_UID
	}
}
// Function for generate token //
function getToken (email, id) {
	const tokenPayload = {
		PRO_Id: id,
		PRO_Username: email,
		roles: 'Provider'
	}
	const token = jwt.sign(tokenPayload, process.env.JWT_SECRET, { expiresIn: 60 * 60 * 24 })
	return token
}
// Create audit for provider //
async function createAudit (req, data) {
	let auditId = await auditUid()
	const auditData = {
		Audit_UID: auditId,
		Audit_Type: auditType.add,
		Data_Point: 'PRO/Signup',
		Audit_Info: 'New- ' + auditId,
		New_Value: data,
		IP_Address: await model.Audit.getIpAddress(req)
	}
	await model.Audit.create(auditData)
}
// function for send email to the user //
async function sendMail (PRO_Username, PRO_UID, PRO_Office_Zip) {
	const message = {
		from: process.env.ADMIN_FROM_EMAIL,
		to: process.env.ADMIN_TO_EMAIL,
		template_id: process.env.PROVIDER_SIGNUP_TEMPLATE,
		dynamic_template_data: {
			PRO_Office_Zip: PRO_Office_Zip,
			PRO_Username: PRO_Username,
			url: `${process.env.ADMIN_PORTAL}/providers/${PRO_UID}`
		}
	}
	sgMail.send(message)
}
module.exports = router
