/* eslint-disable no-unneeded-ternary */
/* eslint-disable camelcase */
/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
const Router = require('express').Router
const router = new Router()
const model = require('../../models')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
var { auditUid } = require('../../utils/UID')
var auditType = require('../../auditType.json')
const errorModule = require('../../models').Errors
const serviceModule = require('../../models').CommonServices
const logger = require('../../models/errorlog')
const moment = require('moment')

// const Joi = require('joi')
// const providerValidation = require('../../provider validation/updateProvider')

// Service for update provider details //
router.put('/updateprovider', authorize([Role.Provider]), async (req, res) => {
	const { body: { data } } = req
	if (!data) return res.send({ code: 0, status: 'failed', message: 'data is required ', Error_Code: errorModule.data_required() })
	if (data.PRO_Password) return res.send({ code: 0, status: 'failed', message: 'provider can not update password', Error_Code: 'HS212' })
	try {
		let provider = await model.Provider.findById(req.user.PRO_UID)
		if (provider) {
			delete data._id
			if (data.PRO_Speciality) {
				data.PRO_Speciality = await RemoveSpeciality(data.PRO_Speciality)
				let isPrimary = data.PRO_Speciality.find(ele => ele.PRO_Primary_Speciality == true)
				if (!isPrimary) {
					if (data.PRO_Speciality.length > 0) {
						data.PRO_Speciality[0].PRO_Primary_Speciality = true
					}
				}
			}

			if (data.Pro_Office_Hours_Time) {
				data.Pro_Office_Hours_Time = await getOfficeHours(data.Pro_Office_Hours_Time)
			}
			var updateProvider = await model.Provider.updateById(req.user.PRO_UID, data)

			const auditData = {
				Audit_UID: await auditUid(),
				Audit_Type: auditType.change,
				Data_Point: 'PRO/Update Provider',
				PRO_First_Name: req.user.PRO_First_Name,
				PRO_Last_Name: req.user.PRO_Last_Name,
				PRO_UID: req.user.PRO_UID,
				Audit_Info: 'New- ' + await auditUid(),
				Field_Name: typeof (data[Object.keys(data)[0]]) == 'object' ? null : Object.keys(data)[0],
				Old_Value: provider[Object.keys(data)[0]],
				New_Value: data[Object.keys(data)[0]],
				IP_Address: await model.Audit.getIpAddress(req)
			}
			await model.Audit.create(auditData)
		
			let newProvider
			if (updateProvider.PRO_Home_Rate_Status != 'Complete') {
				let providerHomeRatesValue = []
				providerHomeRatesValue.push(updateProvider.PRO_Home_Office_Hours_New_Visit_Rate, updateProvider.PRO_Home_Office_Hours_Followup_Rate,
					updateProvider.PRO_Home_Off_Hours_New_Visit_Rate, updateProvider.PRO_Home_Off_Hours_Followup_Rate)
				if (providerHomeRatesValue.includes(null) || providerHomeRatesValue.includes(0)) {
					let statusData = { PRO_Home_Rate_Status: 'Incomplete' }
					newProvider = await model.Provider.updateById(updateProvider.PRO_UID, statusData)
				} else {
					let statusData = { PRO_Home_Rate_Status: 'Complete' }
					newProvider = await model.Provider.updateById(updateProvider.PRO_UID, statusData)
				}
			}
			if (updateProvider.PRO_Home_Rate_Status == 'Complete') {
				let providerHomeRatesValue = []
				providerHomeRatesValue.push(updateProvider.PRO_Home_Office_Hours_New_Visit_Rate, updateProvider.PRO_Home_Office_Hours_Followup_Rate,
					updateProvider.PRO_Home_Off_Hours_New_Visit_Rate, updateProvider.PRO_Home_Off_Hours_Followup_Rate)
				if (providerHomeRatesValue.includes(null) || providerHomeRatesValue.includes(0)) {
					let statusData = { PRO_Home_Rate_Status: 'Incomplete' }
					newProvider = await model.Provider.updateById(updateProvider.PRO_UID, statusData)
				} else {
					let statusData = { PRO_Home_Rate_Status: 'Complete' }
					newProvider = await model.Provider.updateById(updateProvider.PRO_UID, statusData)
				}
			}
			if (updateProvider.PRO_Office_Rate_Status != 'Complete') {
				let providerOfficeRatesValue = []
				providerOfficeRatesValue.push(updateProvider.PRO_Office_Office_Hours_New_Visit_Rate, updateProvider.PRO_Office_Office_Hours_Followup_Rate,
					updateProvider.PRO_Office_Off_Hours_New_Visit_Rate, updateProvider.PRO_Office_Off_Hours_Followup_Rate)
				if (providerOfficeRatesValue.includes(null) || providerOfficeRatesValue.includes(0)) {
					let statusData = { PRO_Office_Rate_Status: 'Incomplete' }
					newProvider = await model.Provider.updateById(updateProvider.PRO_UID, statusData)
				} else {
					let statusData = { PRO_Office_Rate_Status: 'Complete' }
					newProvider = await model.Provider.updateById(updateProvider.PRO_UID, statusData)
				}
			}
			if (updateProvider.PRO_Office_Rate_Status == 'Complete') {
				let providerOfficeRatesValue = []
				providerOfficeRatesValue.push(updateProvider.PRO_Office_Office_Hours_New_Visit_Rate, updateProvider.PRO_Office_Office_Hours_Followup_Rate,
					updateProvider.PRO_Office_Off_Hours_New_Visit_Rate, updateProvider.PRO_Office_Off_Hours_Followup_Rate)
				if (providerOfficeRatesValue.includes(null) || providerOfficeRatesValue.includes(0)) {
					let statusData = { PRO_Office_Rate_Status: 'Incomplete' }
					newProvider = await model.Provider.updateById(updateProvider.PRO_UID, statusData)
				} else {
					let statusData = { PRO_Office_Rate_Status: 'Complete' }
					newProvider = await model.Provider.updateById(updateProvider.PRO_UID, statusData)
				}
			}

			if (newProvider.PRO_Govt_ID_Front) {
				let url = await serviceModule.getImage(newProvider.PRO_Govt_ID_Front)
				newProvider.PRO_Govt_ID_Front = url
			}
			if (newProvider.PRO_Govt_ID_Back) {
				let url = await serviceModule.getImage(newProvider.PRO_Govt_ID_Back)
				newProvider.PRO_Govt_ID_Back = url
			}
			if (newProvider.PRO_Driving_Licence_Front != null && newProvider.PRO_Driving_Licence_Front != '') {
				newProvider.PRO_Driving_Licence_Front = await serviceModule.getImage(newProvider.PRO_Driving_Licence_Front)
			}
			if (newProvider.PRO_Driving_Licence_Back != null && newProvider.PRO_Driving_Licence_Back != '') {
				newProvider.PRO_Driving_Licence_Back = await serviceModule.getImage(newProvider.PRO_Driving_Licence_Back)
			}

			if (newProvider.PRO_State_ID_Front != null && newProvider.PRO_State_ID_Front != '') {
				newProvider.PRO_State_ID_Front = await serviceModule.getImage(newProvider.PRO_State_ID_Front)
			}
			if (newProvider.PRO_State_ID_Back != null && newProvider.PRO_State_ID_Back != '') {
				newProvider.PRO_State_ID_Back = await serviceModule.getImage(newProvider.PRO_State_ID_Back)
			}

			if (newProvider.PRO_Passport_Front != null && newProvider.PRO_Passport_Front != '') {
				newProvider.PRO_Passport_Front = await serviceModule.getImage(newProvider.PRO_Passport_Front)
			}

			newProvider.PRO_Speciality.sort(function (a, b) {
				return b.PRO_Primary_Speciality - a.PRO_Primary_Speciality
			})
			return res.send({ code: 1, status: 'sucess', data: newProvider, message: 'provider update successfully' })
		} else {
			return res.send({
				code: 0,
				status: 'failed',
				message: `provider with ${req.user.PRO_UID} is not available in the system`,
				Error_Code: errorModule.Provider_not_available()
			})
		}
	} catch (err) {
		logger.error(err.message)
		logger.fatal(err)
		return res.send({
			code: 2,
			status: 'failed',
			message: err.message,
			Error_Code: 'HS213'
		})
	}
})
function RemoveSpeciality (arr) {
	return arr.filter(function (emp) {
		if (emp.Is_Deleted == true) {
			return false
		}
		return true
	})
}

function getOfficeHours (hourArray) {
	var arr = []
	for (let index = 0; index < hourArray.length; index++) {
		const element = hourArray[index]
		if (element.fromTime.time.includes(' ')) {
			element.fromTime.time = element.fromTime.time.replace(/ /g, '')
		}
		if (element.toTime.time.includes(' ')) {
			element.toTime.time = element.toTime.time.replace(/ /g, '')
		}
		arr.push(element)
		if (index == hourArray.length - 1) {
			return arr
		}
	}
}
// Service for save provider's special rate //
router.post('/save-specialrate', authorize([Role.Provider]), async (req, res) => {
	const { body: { PRO_Special_Rate_Date } } = req
	req.body.PRO_Special_Rate_Date = moment(req.body.PRO_Special_Rate_Date, 'M/DD/YY').format('M/D/YY')
	if (!PRO_Special_Rate_Date) return res.send({ code: 0, status: 'failed', message: '*Please choose a date to apply your special rate to.', Error_Code: 'HS214' })
	try {
		const auditData = {
			Audit_UID: await auditUid(),
			Audit_Type: auditType.add,
			Data_Point: 'PRO/Provide Special Rate',
			PRO_First_Name: req.user.PRO_First_Name,
			PRO_Last_Name: req.user.PRO_Last_Name,
			PRO_UID: req.user.PRO_UID,
			Audit_Info: 'New- ' + await auditUid(),
			New_Value: req.body,
			IP_Address: await model.Audit.getIpAddress(req)
		}
		await model.Audit.create(auditData)
		let specialRate = await model.Provider.getSpecialRatesByDate(req.user.PRO_UID, req.body.PRO_Special_Rate_Date)
		if (specialRate.length == 0) {
			let provider = await model.Provider.saveSpecialRate(req.user.PRO_UID, req.body)
			if (provider) {
				return res.status(200).send({
					code: 1,
					status: 'sucess',
					message: 'provider Special Rate added successfully'
				})
			} else {
				return res.send({
					code: 0,
					status: 'failed',
					message: `provider with ${req.user.PRO_UID} is not available in the system`,
					Error_Code: errorModule.Provider_not_available()
				})
			}
		} else {
			return res.send({
				code: 0,
				status: 'failed',
				message: '*special rate is already set for the choosen date. please select another date'
			})
		}
	} catch (err) {
		logger.error(err.message)
		logger.fatal(err)
		return res.send({
			code: 2,
			status: 'failed',
			message: err.message,
			Error_Code: 'HS215'
		})
	}
})
// Service for delete provider's special rate by special rate id //
router.delete('/delete-specialrate/:speccialrateid', authorize([Role.Provider]), async (req, res) => {
	const { params: { speccialrateid } } = req
	if (!speccialrateid) return res.send({ code: 0, status: 'failed', message: 'Special Rate id is required', Error_Code: errorModule.special_Rate_id_required() })
	try {
		let specialRate = await model.Provider.findSpecialRateById(speccialrateid)
		if (specialRate.length == 0) return res.send({ code: 0, status: 'failed', message: `special rate with id "${speccialrateid}" is not in our system!`, Error_Code: errorModule.special_rate_not_in_system() })
		let provider = await model.Provider.delteSpecialRate(req.user.PRO_UID, speccialrateid)
		const auditData = {
			Audit_UID: await auditUid(),
			Audit_Type: auditType.remove,
			Data_Point: 'PRO/Provider Delete Special Rate',
			PRO_First_Name: req.user.PRO_First_Name,
			PRO_Last_Name: req.user.PRO_Last_Name,
			PRO_UID: req.user.PRO_UID,
			Audit_Info: 'New- ' + await auditUid(),
			Old_Value: specialRate,
			IP_Address: await model.Audit.getIpAddress(req)
		}
		await model.Audit.create(auditData)
		if (provider) {
			return res.status(200).send({
				code: 1,
				status: 'sucess',
				message: 'Special Rate removed successfully'
			})
		} else {
			return res.send({
				code: 0,
				status: 'failed',
				message: `provider with ${req.user.PRO_UID} is not available in the system`,
				Error_Code: errorModule.Provider_not_available()
			})
		}
	} catch (err) {
		logger.error(err.message)
		logger.fatal(err)
		return res.send({
			code: 2,
			status: 'failed',
			message: err.message,
			Error_Code: 'HS218'
		})
	}
})
// Service for get provider's special rate by id //
router.get('/get-specialrate', authorize([Role.Provider]), async (req, res) => {
	try {
		let provider = await model.Provider.getSpecialRateById(req.user.PRO_UID)
		if (provider) {
			return res.status(200).send({
				code: 1,
				status: 'sucess',
				data: provider
			})
		} else {
			return res.send({
				code: 0,
				status: 'failed',
				message: `provider with ${req.user.PRO_UID} is not available in the system`,
				Error_Code: errorModule.Provider_not_available()
			})
		}
	} catch (err) {
		logger.error(err.message)
		logger.fatal(err)
		return res.send({
			code: 2,
			status: 'failed',
			message: err.message,
			Error_Code: 'HS219'
		})
	}
})

// Service for fetch provider's bank account details //
router.get('/fetch-bank-account', authorize([Role.Provider]), async (req, res) => {
	try {
		let providerBankAccount = await model.Stripe.getBankAccount(req.user.StripeAccountId)
		// if(! providerBankAccount.external_accounts) return res.send({code: 0,status: 'failed',message: "Stripe Account Not Exist, Error_Code: "})
		var result = providerBankAccount.external_accounts && providerBankAccount.external_accounts.data.length > 0 ? providerBankAccount.external_accounts.data.map(ele => ({ accountNumber: ele.last4 })) : []
		var PRO_SSN = providerBankAccount.business_type == 'individual' ? providerBankAccount[providerBankAccount.business_type].ssn_last_4_provided : null
		var PRO_CompanyName = providerBankAccount.business_type == 'company' ? providerBankAccount[providerBankAccount.business_type].name : null
		var PRO_EIN = providerBankAccount.business_type == 'company' ? providerBankAccount[providerBankAccount.business_type].tax_id_provided : null

		var bannkAccount = {
			id: providerBankAccount.id,
			PRO_Entity_TYPE: providerBankAccount.business_type,
			PRO_SSN,
			PRO_CompanyName,
			PRO_EIN

		}
		return res.status(200).send({
			code: 1,
			status: 'sucess',
			data: result,
			bannkAccount: bannkAccount
		})
	} catch (err) {
		logger.error(err.message)
		logger.fatal(err)
		return res.send({
			code: 2,
			status: 'failed',
			message: err.message,
			Error_Code: 'HS220'
		})
	}
})
// Service for update provider's special rate by id //
router.put('/update-specialrate/:id', authorize([Role.Provider]), async (req, res) => {
	const {
		body: { PRO_Special_Rate_Date },
		params: { id }
	} = req
	if (!PRO_Special_Rate_Date) return res.send({ code: 0, status: 'failed', message: 'PRO_Special_Rate_Date is required ', Error_Code: 'HS221' })
	if (!id) return res.send({ code: 0, status: 'failed', message: 'Special Rate id is required', Error_Code: errorModule.special_Rate_id_required() })
	let specialRate = await model.Provider.findSpecialRateById(id)
	if (specialRate.length == 0) return res.send({ code: 0, status: 'failed', message: `special rate with id "${id}" is not in our system!`, Error_Code: errorModule.special_rate_not_in_system() })
	let rate = await model.Provider.getSpecialRatesByDate(req.user.PRO_UID, PRO_Special_Rate_Date)
	if (rate.length > 0 && rate[0]._id != req.params.id) {
		return res.send({
			code: 0,
			status: 'failed',
			message: '*special rate is already set for the choosen date. please select another date'
		})
	}
	try {
		let provider = await model.Provider.updateSpecialDataById(id, req.body)
		const auditData = {
			Audit_UID: await auditUid(),
			Audit_Type: auditType.change,
			Data_Point: 'PRO/Update Special Rate',
			PRO_First_Name: req.user.PRO_First_Name,
			PRO_Last_Name: req.user.PRO_Last_Name,
			PRO_UID: req.user.PRO_UID,
			Audit_Info: 'New- ' + await auditUid(),
			New_Value: req.body,
			Old_Value: specialRate,
			IP_Address: await model.Audit.getIpAddress(req)
		}
		await model.Audit.create(auditData)
		if (provider) {
			return res.status(200).send({
				code: 1,
				status: 'sucess',
				message: 'provider Special Rate updated successfully'
			})
		} else {
			return res.send({
				code: 0,
				status: 'failed',
				message: `provider with ${req.user.PRO_UID} is not available in the system`,
				Error_Code: errorModule.Provider_not_available()
			})
		}
	} catch (err) {
		logger.error(err.message)
		logger.fatal(err)
		return res.send({
			code: 2,
			status: 'failed',
			message: err.message,
			Error_Code: 'HS222'
		})
	}
})
router.post('/add-specialty', authorize([Role.Provider]), async (req, res) => {
	const { body: { PRO_Speciality } } = req
	if (!PRO_Speciality) return res.send({ code: 0, status: 'failed', message: 'PRO_Speciality is required', Error_Code: errorModule.PRO_Speciality_required() })
	try {
		req.body.PRO_Primary_Speciality = req.user.PRO_Speciality.length > 0 ? false : true
		let provider = await model.Provider.addSpeciality(req.user.PRO_UID, req.body)
		const auditData = {
			Audit_UID: await auditUid(),
			Audit_Type: auditType.add,
			Data_Point: 'PRO/Add speciality',
			PRO_First_Name: req.user.PRO_First_Name,
			PRO_Last_Name: req.user.PRO_Last_Name,
			PRO_UID: req.user.PRO_UID,
			Audit_Info: 'New- ' + await auditUid(),
			New_Value: req.body,
			IP_Address: await model.Audit.getIpAddress(req)
		}
		await model.Audit.create(auditData)
		if (provider) {
			let providerData = await model.Provider.findById(req.user.PRO_UID)
			providerData.PRO_Password = undefined
			return res.status(200).send({
				code: 1,
				status: 'sucess',
				message: 'provider Speciality  added successfully',
				data: providerData
			})
		} else {
			return res.send({
				code: 0,
				status: 'failed',
				message: `provider with ${req.user.PRO_UID} is not available in the system`,
				Error_Code: errorModule.Provider_not_available()
			})
		}
	} catch (err) {
		logger.error(err.message)
		logger.fatal(err)
		return res.send({
			code: 2,
			status: 'failed',
			message: err.message,
			Error_Code: 'HS224'
		})
	}
})
router.delete('/delete-speciality/:specialityid', authorize([Role.Provider]), async (req, res) => {
	const { params: { specialityid } } = req
	if (!specialityid) return res.send({ code: 0, status: 'failed', message: 'Speciality Id is required ', Error_Code: errorModule.Speciality_Id_required() })
	try {
		let providerData = await model.Provider.findSpecilityById(specialityid)
		if (providerData.length == 0) return res.send({ code: 0, status: 'failed', message: `Speciality with id "${specialityid}" is invalid`, Error_Code: errorModule.Speciality_invalid() })
		let provider
		if (providerData[0].PRO_Primary_Speciality) {
			provider = await model.Provider.deleteSpeciality(req.user.PRO_UID, specialityid)
			await model.Provider.CreatePrimarySpeciality(req.user.PRO_UID)
		} else {
			provider = await model.Provider.deleteSpeciality(req.user.PRO_UID, specialityid)
		}
		const auditData = {
			Audit_UID: await auditUid(),
			Audit_Type: auditType.remove,
			Data_Point: 'PRO/Provider Delete Special Rate',
			PRO_First_Name: req.user.PRO_First_Name,
			PRO_Last_Name: req.user.PRO_Last_Name,
			PRO_UID: req.user.PRO_UID,
			Audit_Info: 'New- ' + await auditUid(),
			Old_Value: providerData,
			IP_Address: await model.Audit.getIpAddress(req)
		}
		await model.Audit.create(auditData)
		if (provider) {
			let providerData = await model.Provider.findById(req.user.PRO_UID)
			providerData.PRO_Password = undefined
			return res.status(200).send({
				code: 1,
				status: 'sucess',
				message: 'Speciality  removed successfully',
				data: providerData
			})
		} else {
			return res.send({
				code: 0,
				status: 'failed',
				message: `provider with ${req.user.PRO_UID} is not available in the system`,
				Error_Code: errorModule.Provider_not_available()
			})
		}
	} catch (err) {
		logger.error(err.message)
		logger.fatal(err)
		return res.send({
			code: 2,
			status: 'failed',
			message: err.message,
			Error_Code: 'HS227'
		})
	}
})
router.put('/update-speciality/:specialityid', authorize([Role.Provider]), async (req, res) => {
	const {
		body: { PRO_Speciality },
		params: { specialityid }
	} = req
	if (!PRO_Speciality) return res.send({ code: 0, status: 'failed', message: 'PRO_Speciality is required ', Error_Code: errorModule.PRO_Speciality_required() })
	if (!specialityid) return res.send({ code: 0, status: 'failed', message: 'Speciality id is required ', Error_Code: errorModule.Speciality_Id_required() })
	try {
		var speciality = await model.Provider.findSpecilityById(specialityid)
		if (speciality.length == 0) return res.send({ code: 0, status: 'failed', message: `Speciality with id "${specialityid}" is invalid`, Error_Code: errorModule.Speciality_invalid() })
		const auditData = {
			Audit_UID: await auditUid(),
			Audit_Type: auditType.change,
			Data_Point: 'PRO/Update Speciality',
			PRO_First_Name: req.user.PRO_First_Name,
			PRO_Last_Name: req.user.PRO_Last_Name,
			PRO_UID: req.user.PRO_UID,
			Audit_Info: 'New- ' + await auditUid(),
			New_Value: req.body,
			Old_Value: speciality,
			IP_Address: await model.Audit.getIpAddress(req)
		}
		await model.Audit.create(auditData)
		let provider = await model.Provider.updateSpecialityById(specialityid, { PRO_Speciality: PRO_Speciality })
		if (provider) {
			let providerData = await model.Provider.findById(req.user.PRO_UID)
			providerData.PRO_Password = undefined
			return res.status(200).send({
				code: 1,
				status: 'sucess',
				message: 'provider Speciality updated successfully',
				data: providerData
			})
		} else {
			return res.send({
				code: 0,
				status: 'failed',
				message: `provider with ${req.user.PRO_UID} is not available in the system`,
				Error_Code: errorModule.Provider_not_available()
			})
		}
	} catch (err) {
		logger.error(err.message)
		logger.fatal(err)
		return res.send({
			code: 2,
			status: 'failed',
			message: err.message,
			Error_Code: 'HS228'
		})
	}
})
// Add Affiliation of Provider
router.post('/add-affiliation', authorize([Role.Provider]), async (req, res) => {
	const { body: { PRO_Affiliations } } = req
	if (!PRO_Affiliations) return res.send({ code: 0, status: 'failed', message: 'PRO_Affiliations is required ', Error_Code: errorModule.PRO_Affiliations_required() })
	try {
		let provider = await model.Provider.addAffiliation(req.user.PRO_UID, { PRO_Affiliations: PRO_Affiliations })
		const auditData = {
			Audit_UID: await auditUid(),
			Audit_Type: auditType.add,
			Data_Point: 'PRO/Provider add affiliation',
			PRO_First_Name: req.user.PRO_First_Name,
			PRO_Last_Name: req.user.PRO_Last_Name,
			PRO_UID: req.user.PRO_UID,
			Audit_Info: 'New- ' + await auditUid(),
			New_Value: req.body,
			IP_Address: await model.Audit.getIpAddress(req)
		}
		await model.Audit.create(auditData)
		let providerData = await model.Provider.findById(req.user.PRO_UID)
		providerData.PRO_Password = undefined
		if (provider) {
			return res.status(200).send({
				code: 1,
				status: 'sucess',
				message: 'Provider Affiliation  added successfully',
				data: providerData
			})
		} else {
			return res.send({
				code: 0,
				status: 'failed',
				message: `provider with ${req.user.PRO_UID} is not available in the system`,
				Error_Code: errorModule.Provider_not_available()
			})
		}
	} catch (err) {
		logger.error(err.message)
		logger.fatal(err)
		return res.send({
			code: 2,
			status: 'failed',
			message: err.message,
			Error_Code: 'HS230'
		})
	}
})
// Delete Affiliation of provider By ID
router.delete('/delete-affiliation/:affiliationid', authorize([Role.Provider]), async (req, res) => {
	const { params: { affiliationid } } = req
	if (!affiliationid) return res.send({ code: 0, status: 'failed', message: 'affliation Id is required ', Error_Code: errorModule.affliation_Id_required() })
	try {
		let affliation = await model.Provider.findAffliationById(affiliationid)
		if (affliation.length == 0) return res.send({ code: 0, status: 'failed', message: `provider affliation with id "${affiliationid}" is not in our system!`, Error_Code: errorModule.affliation_invalid() })
		let provider = await model.Provider.deleteAffiliationById(req.user.PRO_UID, affiliationid)
		const auditData = {
			Audit_UID: await auditUid(),
			Audit_Type: auditType.remove,
			Data_Point: 'PRO/Provider Delete affliation',
			PRO_First_Name: req.user.PRO_First_Name,
			PRO_Last_Name: req.user.PRO_Last_Name,
			PRO_UID: req.user.PRO_UID,
			Audit_Info: 'New- ' + await auditUid(),
			Old_Value: affliation,
			IP_Address: await model.Audit.getIpAddress(req)
		}
		await model.Audit.create(auditData)
		if (provider) {
			let providerData = await model.Provider.findById(req.user.PRO_UID)
			providerData.PRO_Password = undefined
			return res.status(200).send({
				code: 1,
				status: 'sucess',
				message: 'Affiliation removed successfully',
				data: providerData
			})
		} else {
			return res.send({
				code: 0,
				status: 'failed',
				message: `provider with ${req.user.PRO_UID} is not available in the system`,
				Error_Code: errorModule.Provider_not_available()
			})
		}
	} catch (err) {
		logger.error(err.message)
		logger.fatal(err)
		return res.send({
			code: 2,
			status: 'failed',
			message: err.message,
			Error_Code: 'HS233'
		})
	}
})
// Update Affiliation of provider By ID
router.put('/update-affiliation/:affiliationid', authorize([Role.Provider]), async (req, res) => {
	const {
		body: { PRO_Affiliations },
		params: { affiliationid }
	} = req
	if (!PRO_Affiliations) return res.send({ code: 0, status: 'failed', message: 'PRO_Affiliations is required ', Error_Code: errorModule.PRO_Affiliations_required() })
	if (!affiliationid) return res.send({ code: 0, status: 'failed', message: 'Affiliation id is required ', Error_Code: errorModule.affliation_Id_required() })
	let affliation = await model.Provider.findAffliationById(affiliationid)
	if (affliation.length == 0) return res.send({ code: 0, status: 'failed', message: `provider's affliation with id "${affiliationid}" is not in our system!`, Error_Code: errorModule.affliation_invalid() })
	try {
		let provider = await model.Provider.updateAffiliationById(affiliationid, { PRO_Affiliations: PRO_Affiliations })
		const auditData = {
			Audit_UID: await auditUid(),
			Audit_Type: auditType.change,
			Data_Point: 'PRO/Update affliation',
			PRO_First_Name: req.user.PRO_First_Name,
			PRO_Last_Name: req.user.PRO_Last_Name,
			PRO_UID: req.user.PRO_UID,
			Audit_Info: 'New- ' + await auditUid(),
			New_Value: req.body,
			Old_Value: affliation,
			IP_Address: await model.Audit.getIpAddress(req)
		}
		await model.Audit.create(auditData)
		if (provider) {
			let providerData = await model.Provider.findById(req.user.PRO_UID)
			providerData.PRO_Password = undefined
			return res.status(200).send({
				code: 1,
				status: 'sucess',
				message: 'provider Affiliation updated successfully',
				data: providerData
			})
		} else {
			return res.send({
				code: 0,
				status: 'failed',
				message: `provider with ${req.user.PRO_UID} is not available in the system`,
				Error_Code: errorModule.Provider_not_available()
			})
		}
	} catch (err) {
		logger.error(err.message)
		logger.fatal(err)
		return res.send({
			code: 2,
			status: 'failed',
			message: err.message,
			Error_Code: 'HS234'
		})
	}
})
// Add Education of Provider //
router.post('/add-education', authorize([Role.Provider]), async (req, res) => {
	const { body: { PRO_Education } } = req
	if (!PRO_Education) return res.send({ code: 0, status: 'failed', message: 'PRO_Education is required ', Error_Code: errorModule.PRO_Education_required() })
	try {
		let provider = await model.Provider.addEducation(req.user.PRO_UID, { PRO_Education: PRO_Education })
		const auditData = {
			Audit_UID: await auditUid(),
			Audit_Type: auditType.add,
			Data_Point: 'PRO/Provider add education',
			PRO_First_Name: req.user.PRO_First_Name,
			PRO_Last_Name: req.user.PRO_Last_Name,
			PRO_UID: req.user.PRO_UID,
			Audit_Info: 'New- ' + await auditUid(),
			New_Value: req.body,
			IP_Address: await model.Audit.getIpAddress(req)
		}
		await model.Audit.create(auditData)
		if (provider) {
			let providerData = await model.Provider.findById(req.user.PRO_UID)
			providerData.PRO_Password = undefined
			return res.status(200).send({
				code: 1,
				status: 'sucess',
				message: 'Provider Education  added successfully',
				data: providerData
			})
		} else {
			return res.send({
				code: 0,
				status: 'failed',
				message: `provider with ${req.user.PRO_UID} is not available in the system`,
				Error_Code: errorModule.Provider_not_available()
			})
		}
	} catch (err) {
		logger.error(err.message)
		logger.fatal(err)
		return res.send({
			code: 2,
			status: 'failed',
			message: err.message,
			Error_Code: 'HS236'
		})
	}
})
// Delete Education of provider By ID
router.delete('/delete-education/:educationid', authorize([Role.Provider]), async (req, res) => {
	const { params: { educationid } } = req
	if (!educationid) return res.send({ code: 0, status: 'failed', message: 'Education Id is required ', Error_Code: errorModule.Education_Id_required() })
	try {
		let education = await model.Provider.findEducationById(educationid)
		if (education.length == 0) return res.send({ code: 0, status: 'failed', message: `provider's education with id "${educationid}" is not in our system!`, Error_Code: errorModule.education_invalid() })
		let provider = await model.Provider.deleteEducationById(req.user.PRO_UID, educationid)
		const auditData = {
			Audit_UID: await auditUid(),
			Audit_Type: auditType.remove,
			Data_Point: 'PRO/Provider Delete education',
			PRO_First_Name: req.user.PRO_First_Name,
			PRO_Last_Name: req.user.PRO_Last_Name,
			PRO_UID: req.user.PRO_UID,
			Audit_Info: 'New- ' + await auditUid(),
			Old_Value: education,
			IP_Address: await model.Audit.getIpAddress(req)
		}
		await model.Audit.create(auditData)
		if (provider) {
			let providerData = await model.Provider.findById(req.user.PRO_UID)
			providerData.PRO_Password = undefined
			return res.status(200).send({
				code: 1,
				status: 'sucess',
				message: 'Education removed successfully',
				data: providerData
			})
		} else {
			return res.send({
				code: 0,
				status: 'failed',
				message: `provider with ${req.user.PRO_UID} is not available in the system`,
				Error_Code: errorModule.Provider_not_available()
			})
		}
	} catch (err) {
		logger.error(err.message)
		logger.fatal(err)
		return res.send({
			code: 2,
			status: 'failed',
			message: err.message,
			Error_Code: 'HS239'
		})
	}
})
// Update Education of provider By ID
router.put('/update-education/:educationid', authorize([Role.Provider]), async (req, res) => {
	const {
		body: { PRO_Education },
		params: { educationid }
	} = req
	if (!PRO_Education) return res.send({ code: 0, status: 'failed', message: 'PRO_Education is required ', Error_Code: errorModule.PRO_Education_required() })
	if (!educationid) return res.send({ code: 0, status: 'failed', message: 'Education id is required ', Error_Code: errorModule.Education_Id_required() })
	let education = await model.Provider.findEducationById(educationid)
	if (education.length == 0) return res.send({ code: 0, status: 'failed', message: `provider's education with id "${educationid}" is not in our system!`, Error_Code: errorModule.education_invalid() })
	try {
		let provider = await model.Provider.updateEducationById(educationid, { PRO_Education: PRO_Education })
		const auditData = {
			Audit_UID: await auditUid(),
			Audit_Type: auditType.change,
			Data_Point: 'PRO/Update education',
			PRO_First_Name: req.user.PRO_First_Name,
			PRO_Last_Name: req.user.PRO_Last_Name,
			PRO_UID: req.user.PRO_UID,
			Audit_Info: 'New- ' + await auditUid(),
			New_Value: req.body,
			Old_Value: education,
			IP_Address: await model.Audit.getIpAddress(req)
		}
		await model.Audit.create(auditData)
		if (provider) {
			let providerData = await model.Provider.findById(req.user.PRO_UID)
			providerData.PRO_Password = undefined
			return res.status(200).send({
				code: 1,
				status: 'sucess',
				message: 'provider Education updated successfully',
				data: providerData
			})
		} else {
			return res.send({
				code: 0,
				status: 'failed',
				message: `provider with ${req.user.PRO_UID} is not available in the system`,
				Error_Code: errorModule.Provider_not_available()
			})
		}
	} catch (err) {
		logger.error(err.message)
		logger.fatal(err)
		return res.send({
			code: 2,
			status: 'failed',
			message: err.message,
			Error_Code: 'HS240'
		})
	}
})
// Add Experience of Provider //
router.post('/add-experience', authorize([Role.Provider]), async (req, res) => {
	const { body: { PRO_Experience } } = req
	if (!PRO_Experience) return res.send({ code: 0, status: 'failed', message: 'PRO_Experience is required ', Error_Code: errorModule.PRO_Experience_required() })
	try {
		let provider = await model.Provider.addExperience(req.user.PRO_UID, { PRO_Experience: PRO_Experience })
		const auditData = {
			Audit_UID: await auditUid(),
			Audit_Type: auditType.add,
			Data_Point: 'PRO/Provider add experience',
			PRO_First_Name: req.user.PRO_First_Name,
			PRO_Last_Name: req.user.PRO_Last_Name,
			PRO_UID: req.user.PRO_UID,
			Audit_Info: 'New- ' + await auditUid(),
			New_Value: req.body,
			IP_Address: await model.Audit.getIpAddress(req)
		}
		await model.Audit.create(auditData)
		if (provider) {
			let providerData = await model.Provider.findById(req.user.PRO_UID)
			providerData.PRO_Password = undefined
			return res.status(200).send({
				code: 1,
				status: 'sucess',
				message: 'Provider Experience  added successfully',
				data: providerData
			})
		} else {
			return res.send({
				code: 0,
				status: 'failed',
				message: `provider with ${req.user.PRO_UID} is not available in the system`,
				Error_Code: errorModule.Provider_not_available()
			})
		}
	} catch (err) {
		logger.error(err.message)
		logger.fatal(err)
		return res.send({
			code: 2,
			status: 'failed',
			message: err.message,
			Error_Code: 'HS242'
		})
	}
})
// Delete Experience of provider By ID
router.delete('/delete-experience/:experienceid', authorize([Role.Provider]), async (req, res) => {
	const { params: { experienceid } } = req
	if (!experienceid) return res.send({ code: 0, status: 'failed', message: 'Experience Id is required ', Error_Code: errorModule.Experience_Id_required() })
	try {
		let experience = await model.Provider.findExperienceById(experienceid)
		if (experience.length == 0) return res.send({ code: 0, status: 'failed', message: `provider's experience with id "${experienceid}" is not in our system!`, Error_Code: errorModule.experience_invalid() })
		let provider = await model.Provider.deleteExperienceById(req.user.PRO_UID, experienceid)
		const auditData = {
			Audit_UID: await auditUid(),
			Audit_Type: auditType.remove,
			Data_Point: 'PRO/Provider Delete experience',
			PRO_First_Name: req.user.PRO_First_Name,
			PRO_Last_Name: req.user.PRO_Last_Name,
			PRO_UID: req.user.PRO_UID,
			Audit_Info: 'New- ' + await auditUid(),
			Old_Value: experience,
			IP_Address: await model.Audit.getIpAddress(req)
		}
		await model.Audit.create(auditData)
		if (provider) {
			let providerData = await model.Provider.findById(req.user.PRO_UID)
			providerData.PRO_Password = undefined
			return res.status(200).send({
				code: 1,
				status: 'sucess',
				message: 'Experience removed successfully',
				data: providerData
			})
		} else {
			return res.send({
				code: 0,
				status: 'failed',
				message: `provider with ${req.user.PRO_UID} is not available in the system`,
				Error_Code: errorModule.Provider_not_available()
			})
		}
	} catch (err) {
		logger.error(err.message)
		logger.fatal(err)
		return res.send({
			code: 2,
			status: 'failed',
			message: err.message,
			Error_Code: 'HS245'
		})
	}
})
// Update Experience of provider By ID
router.put('/update-experience/:experienceid', authorize([Role.Provider]), async (req, res) => {
	const {
		body: { PRO_Experience },
		params: { experienceid }
	} = req
	if (!PRO_Experience) return res.send({ code: 0, status: 'failed', message: 'PRO_Experience is required ', Error_Code: errorModule.PRO_Experience_required() })
	if (!experienceid) return res.send({ code: 0, status: 'failed', message: 'Experience id is required ', Error_Code: errorModule.Experience_Id_required() })
	try {
		let experience = await model.Provider.findExperienceById(experienceid)
		if (experience.length == 0) return res.send({ code: 0, status: 'failed', message: `provider's experience with id "${experienceid}" is not in our system!`, Error_Code: errorModule.experience_invalid() })
		let provider = await model.Provider.updateExperienceById(experienceid, { PRO_Experience: PRO_Experience })
		const auditData = {
			Audit_UID: await auditUid(),
			Audit_Type: auditType.change,
			Data_Point: 'PRO/Update experience',
			PRO_First_Name: req.user.PRO_First_Name,
			PRO_Last_Name: req.user.PRO_Last_Name,
			PRO_UID: req.user.PRO_UID,
			Audit_Info: 'New- ' + await auditUid(),
			New_Value: req.body,
			Old_Value: experience,
			IP_Address: await model.Audit.getIpAddress(req)
		}
		await model.Audit.create(auditData)
		if (provider) {
			let providerData = await model.Provider.findById(req.user.PRO_UID)
			providerData.PRO_Password = undefined
			return res.status(200).send({
				code: 1,
				status: 'sucess',
				message: 'provider Experience updated successfully',
				data: providerData
			})
		} else {
			return res.send({
				code: 0,
				status: 'failed',
				message: `provider with ${req.user.PRO_UID} is not available in the system`,
				Error_Code: errorModule.Provider_not_available()
			})
		}
	} catch (err) {
		logger.error(err.message)
		logger.fatal(err)
		return res.send({
			code: 2,
			status: 'failed',
			message: err.message,
			Error_Code: 'HS246'
		})
	}
})
// Add Action of Provider
router.post('/add-action', authorize([Role.Provider]), async (req, res) => {
	const { body: { PRO_Actions_Against } } = req
	if (!PRO_Actions_Against) return res.send({ code: 0, status: 'failed', message: 'PRO_Actions_Against is required ', Error_Code: errorModule.PRO_Actions_Against_required() })
	try {
		let provider = await model.Provider.addAction(req.user.PRO_UID, { PRO_Actions_Against: PRO_Actions_Against })
		const auditData = {
			Audit_UID: await auditUid(),
			Audit_Type: auditType.add,
			Data_Point: 'PRO/Provider add action',
			PRO_First_Name: req.user.PRO_First_Name,
			PRO_Last_Name: req.user.PRO_Last_Name,
			PRO_UID: req.user.PRO_UID,
			Audit_Info: 'New- ' + await auditUid(),
			New_Value: req.body,
			IP_Address: await model.Audit.getIpAddress(req)
		}
		await model.Audit.create(auditData)
		if (provider) {
			let providerData = await model.Provider.findById(req.user.PRO_UID)
			providerData.PRO_Password = undefined
			return res.status(200).send({
				code: 1,
				status: 'sucess',
				message: 'Provider Action  added successfully',
				data: providerData
			})
		} else {
			return res.send({
				code: 0,
				status: 'failed',
				message: `provider with ${req.user.PRO_UID} is not available in the system`,
				Error_Code: errorModule.Provider_not_available()
			})
		}
	} catch (err) {
		logger.error(err.message)
		logger.fatal(err)
		return res.send({
			code: 2,
			status: 'failed',
			message: err.message,
			Error_Code: 'HS248'
		})
	}
})
// Delete Action of provider By ID
router.delete('/delete-action/:actionid', authorize([Role.Provider]), async (req, res) => {
	const { params: { actionid } } = req
	if (!actionid) return res.send({ code: 0, status: 'failed', message: 'Action Id is required ', Error_Code: errorModule.Action_Id_required() })
	try {
		let action = await model.Provider.findActionById(actionid)
		if (action.length == 0) return res.send({ code: 0, status: 'failed', message: `provider's acrtion with id "${actionid}" is not in our system!`, Error_Code: errorModule.acrtion_invalid() })
		let provider = await model.Provider.deleteActionById(req.user.PRO_UID, actionid)
		const auditData = {
			Audit_UID: await auditUid(),
			Audit_Type: auditType.remove,
			Data_Point: 'PRO/Delete action',
			PRO_First_Name: req.user.PRO_First_Name,
			PRO_Last_Name: req.user.PRO_Last_Name,
			PRO_UID: req.user.PRO_UID,
			Audit_Info: 'New- ' + await auditUid(),
			Old_Value: action,
			IP_Address: await model.Audit.getIpAddress(req)
		}
		await model.Audit.create(auditData)
		if (provider) {
			let providerData = await model.Provider.findById(req.user.PRO_UID)
			providerData.PRO_Password = undefined
			return res.status(200).send({
				code: 1,
				status: 'sucess',
				message: 'Action removed successfully',
				data: providerData
			})
		} else {
			return res.send({
				code: 0,
				status: 'failed',
				message: `provider with ${req.user.PRO_UID} is not available in the system`,
				Error_Code: errorModule.Provider_not_available()
			})
		}
	} catch (err) {
		logger.error(err.message)
		logger.fatal(err)
		return res.send({
			code: 2,
			status: 'failed',
			message: err.message,
			Error_Code: 'HS251'
		})
	}
})
// Update Action of provider By ID
router.put('/update-action/:actionid', authorize([Role.Provider]), async (req, res) => {
	const {
		body: { PRO_Actions_Against },
		params: { actionid }
	} = req
	if (!PRO_Actions_Against) return res.send({ code: 0, status: 'failed', message: 'PRO_Actions_Against is required ', Error_Code: errorModule.PRO_Actions_Against_required() })
	if (!actionid) return res.send({ code: 0, status: 'failed', message: 'Action id is required ', Error_Code: errorModule.Action_Id_required() })
	try {
		let action = await model.Provider.findActionById(actionid)
		if (action.length == 0) return res.send({ code: 0, status: 'failed', message: `provider's acrtion with id "${actionid}" is not in our system!`, Error_Code: errorModule.acrtion_invalid() })
		const auditData = {
			Audit_UID: await auditUid(),
			Audit_Type: auditType.change,
			Data_Point: 'PRO/Update action',
			PRO_First_Name: req.user.PRO_First_Name,
			PRO_Last_Name: req.user.PRO_Last_Name,
			PRO_UID: req.user.PRO_UID,
			Audit_Info: 'New- ' + await auditUid(),
			New_Value: req.body,
			Old_Value: action,
			IP_Address: await model.Audit.getIpAddress(req)
		}
		await model.Audit.create(auditData)
		let provider = await model.Provider.updateActionById(actionid, { PRO_Actions_Against: PRO_Actions_Against })
		if (provider) {
			let providerData = await model.Provider.findById(req.user.PRO_UID)
			providerData.PRO_Password = undefined
			return res.send({
				code: 1,
				status: 'sucess',
				message: 'provider Action updated successfully',
				data: providerData
			})
		} else {
			return res.send({
				code: 0,
				status: 'failed',
				message: `provider with ${req.user.PRO_UID} is not available in the system`,
				Error_Code: errorModule.Provider_not_available()
			})
		}
	} catch (err) {
		logger.error(err.message)
		logger.fatal(err)
		return res.send({
			code: 2,
			status: 'failed',
			message: err.message,
			Error_Code: 'HS252'
		})
	}
})

// craete bank account
router.post('/save-deposite', authorize([Role.Provider]), async (req, res) => {
	try {
		if (!req.body.PRO_Entity_TYPE) return res.send({ code: 0, status: 'failed', message: 'PRO_Entity_TYPE  is required', Error_Code: 'HS253' })
		if (!req.body.PRO_Routing_Number) return res.send({ code: 0, status: 'failed', message: 'PRO_Routing_Number   is required', Error_Code: 'HS254' })
		if (!req.body.PRO_Account_Number) return res.send({ code: 0, status: 'failed', message: 'PRO_Account_Number  is required', Error_Code: 'HS255' })
		// req.body.PRO_WebAddress = "https://www.tepia.co";
		if (req.user.PRO_First_Name && req.user.PRO_Last_Name && req.user.PRO_DOB) {
			let businessData
			if (req.body.PRO_Entity_TYPE == 'individual') {
				if (!req.body.PRO_SSN) return res.send({ code: 0, status: 'failed', message: 'PRO_SSN   is required', Error_Code: 'HS257' })
				businessData = {
					first_name: req.user.PRO_First_Name,
					last_name: req.user.PRO_Last_Name,
					ssn_last_4: req.body.PRO_SSN
				}
			} else if (req.body.PRO_Entity_TYPE == 'llc') {
				if (!req.body.PRO_EIN) return res.send({ code: 0, status: 'failed', message: 'PRO_EIN   is required', Error_Code: 'HS258' })
				businessData = {
					name: req.body.PRO_Routing_CompanyName,
					tax_id: req.body.PRO_EIN
				}
			} else if (req.body.PRO_Entity_TYPE == 'company') {
				if (!req.body.PRO_WebAddress) return res.send({ code: 0, status: 'failed', message: 'PRO_WebAddress is required', Error_Code: 'HS256' })
				if (!req.body.PRO_CompanyName) return res.send({ code: 0, status: 'failed', message: 'PRO_CompanyName   is required', Error_Code: 'HS259' })
				if (!req.body.PRO_EIN) return res.send({ code: 0, status: 'failed', message: 'PRO_EIN   is required', Error_Code: 'HS260' })
				businessData = {
					name: req.body.PRO_CompanyName,
					tax_id: req.body.PRO_EIN
				}
			} else {
				return res.send({ code: 0, status: 'failed', message: 'Invalid PRO_Entity_TYPE: must be one of individual, company, non_profit, or government_entity', Error_Code: 'HS261' })
			}
			if (!req.user.StripeAccountId) {
				let accountData = await model.Stripe.craeteAccount(req.user, req.body, req.connection.remoteAddress, businessData)
				const data = { PRO_Billing_Status: 'Complete', Updated_At: Date.now(), StripeAccountId: accountData.id }
				await model.Provider.updateById(req.user.PRO_UID, data)
			} else {
				delete businessData.tax_id
				delete businessData.ssn_last_4
				await model.Stripe.updateAccount(req.user, req.body, req.connection.remoteAddress, businessData)
				const data = { PRO_Billing_Status: 'Complete', Updated_At: Date.now() }
				await model.Provider.updateById(req.user.PRO_UID, data)
			}
			return res.send({
				code: 1,
				status: 'sucess',
				message: 'Account detail added successfully'

			})
		} else {
			return res.send({
				code: 0,
				status: 'failed',
				Error_Code: 'HS264',
				message: `*provider with ${req.user.PRO_UID} is not completed profile`
			})
		}
	} catch (err) {
		logger.error(err.message)
		logger.fatal(err)
		return res.send({
			code: 0,
			status: 'failed',
			message: err.message,
			Error_Code: 'HS263'
		})
	}
})

module.exports = router
