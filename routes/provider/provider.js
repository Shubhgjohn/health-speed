/* eslint-disable quotes */
/* eslint-disable eqeqeq */
/* eslint-disable dot-notation */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const jwt = require('jsonwebtoken')
const model = require('../../models')
const errorModule = require('../../models').Errors
const serviceModule = require('../../models').CommonServices
var sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_PRIVATE_KEY)
const { bcryptHash } = require('../../utils/crypto')
const { auditUid } = require('../../utils/UID')
var auditType = require('../../auditType.json')
const moment = require('moment-timezone')
const moments = require('moment')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const logger = require('../../models/errorlog')

// Service for request to reset password //
router.post('/forgot_password', async (req, res) => {
	sgMail.setApiKey(process.env.SENDGRID_PRIVATE_KEY)
	const { body: { PRO_Username } } = req
	if (!PRO_Username) return res.send({ code: 0, status: 'failed', message: `*Don't forget to enter your email address! It's required to reset your password.`, Error_Code: errorModule.enter_your_email() })
	try {
		let provider = await model.Provider.findOneByEmail(PRO_Username)
		if (!provider) {
			let time = moments.tz(new Date(), process.env.CURRENT_TIMEZONE).format('HH:mm:ss')
			const msg = {
				to: process.env.ADMIN_TO_EMAIL,
				from: process.env.ADMIN_FROM_EMAIL,
				template_id: process.env.PROVIDER_FORGOT_PASSWORD_FAILURE_TEMPLATE,
				dynamic_template_data: {
					date: moment.tz(new Date(), process.env.CURRENT_TIMEZONE).format('MM/DD/YY'),
					time: time,
					email: PRO_Username,
					contact: process.env.HELPDESK_EMAIL,
					phone: process.env.CONTACT
				}
			}
			sgMail.send(msg)
				.then(() => {
					return res.send({ code: 0, status: 'failed', message: '*Humm... Please check this email address and try again.', Error_Code: 'HS197' })
				}).catch(err => {
					return res.send({ code: 0, status: 'failed', message: err.message, Error_Code: 'HS198' })
				})
		} else {
			const tokenPayload = {
				PRO_Username: provider.PRO_Username
			}
			let verify = provider.PRO_Password ? provider.PRO_Password : process.env.JWT_SECRET
			const token = jwt.sign(tokenPayload, verify, { expiresIn: '24h' })
			const url = `${process.env.API_HOST}/provider/reset/${provider.PRO_UID}/${token}`
			let time = moments.tz(new Date(), process.env.CURRENT_TIMEZONE).format('HH:mm:ss')
			const msg = {
				to: provider.PRO_Username,
				from: process.env.ADMIN_FROM_EMAIL,
				template_id: process.env.PROVIDER_FORGOT_PASSWORD_TEMPLATE,
				dynamic_template_data: {
					date: moment(new Date()).format('MM/DD/YY'),
					time: time,
					email: provider.PRO_Username,
					contact: process.env.HELPDESK_EMAIL,
					phone: process.env.CONTACT,
					url: url
				}
			}
			sgMail.send(msg)
				.then(async () => {
					return res.send({
						code: 1,
						status: 'sucess',
						message: 'Please check your email for reseting your password'
					})
				}).catch(err => {
					return res.send({ code: 0, status: 'failed', message: err.message, Error_Code: 'HS199' })
				})
		}
	} catch (err) {
		logger.error(err.message)
		logger.fatal(err)
		return res.send({ code: 2, status: 'failed', message: err.message, Error_Code: 'HS200' })
	}
})

router.get('/:id/:token', (req, res) => {
	if (req.headers['user-agent'].includes('Android')) {
		res.redirect('https://play.google.com/')
	} else if (req.headers['user-agent'].includes('iPhone')) {
		res.redirect('https://www.apple.com/ios/app-store/')
	} else {
		res.redirect('https://play.google.com/')
	}
})
// Service for reset provider password //
router.post('/reset_password', async (req, res) => {
	const { body: { PRO_UID, token, PRO_New_Password } } = req
	if (!PRO_UID) return res.status(422).send({ code: 0, status: 'failed', message: 'PRO_UID is required', Error_Code: errorModule.PRO_UID_required() })
	if (!token) return res.status(422).send({ code: 0, status: 'failed', message: 'Reset token is missing with the request', Error_Code: 'HS201' })
	if (!PRO_New_Password) return res.status(422).send({ code: 0, status: 'failed', message: '*Please make sure the criteria below are met and try again', Error_Code: errorModule.make_sure_criteria() })
	var reg = /^(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$#!%^*?&]{10,45}$/
	if (!reg.test(PRO_New_Password)) {
		return res.send({ code: 0, status: 'failed', message: '*Please make sure the criteria below are met and try again', Error_Code: errorModule.make_sure_criteria() })
	}
	try {
		var providerData = await model.Provider.findById(PRO_UID)
		if (providerData) {
			let verify = providerData.PRO_Password ? providerData.PRO_Password : process.env.JWT_SECRET
			jwt.verify(token, verify, async function (err, decoded) {
				if (err) return res.send({ code: 0, status: 'failed', message: 'You provided wrong token!', Error_Code: 'HS202' })
				if (decoded) {
					var newHashPassword = bcryptHash(PRO_New_Password, 10)
					await model.Provider.updateByEmail(decoded.PRO_Username, newHashPassword)
					const tokenPayload = {
						PRO_Id: PRO_UID,
						PRO_Username: decoded.PRO_Username,
						roles: 'Provider'
					}
					const token = jwt.sign(tokenPayload, process.env.JWT_SECRET, { expiresIn: 60 * 60 * 24 })
					var auditData = {
						Audit_UID: await auditUid(),
						Audit_Type: auditType.change,
						Data_Point: 'PRO/Reset Password',
						PRO_First_Name: providerData.PRO_First_Name,
						PRO_Last_Name: providerData.PRO_Last_Name,
						PRO_UID: providerData.PRO_UID,
						Audit_Info: 'New- ' + await auditUid(),
						New_Value: decoded.PRO_Username,
						IP_Address: await model.Audit.getIpAddress(req)
					}
					await model.Audit.create(auditData)
					return res.send({
						code: 1,
						status: 'Sucess',
						data: token,
						date: moment(new Date()).format('MM-DD-YYYY')
					})
				}
			})
		} else {
			return res.send({
				code: 0,
				status: 'failed',
				message: 'Provider with this PRO_UID is not available in the system',
				Error_Code: errorModule.Provider_not_available()
			})
		}
	} catch (err) {
		logger.error(err.message)
		logger.fatal(err)
		return res.send({ code: 2, status: 'failed', message: err.message, Error_Code: 'HS205' })
	}
})
// Get provider by id //
router.get('/get-providerbyid', authorize([Role.Provider]), async (req, res) => {
	try {
		let provider = await model.Provider.findById(req.user.PRO_UID)
		var created_date = moment(provider.Created_At).format('MM-DD-YYYY')
		var newProvider = JSON.parse(JSON.stringify(provider))
		newProvider['Signup_Date'] = created_date
		newProvider['PRO_DOB'] = provider.PRO_DOB ? moment(provider.PRO_DOB).format('MM-DD-YYYY') : null

		if (newProvider.PRO_Govt_ID_Front != null && newProvider.PRO_Govt_ID_Front != '') {
			newProvider.PRO_Govt_ID_Front = await serviceModule.getImage(newProvider.PRO_Govt_ID_Front)
		}
		if (newProvider.PRO_Govt_ID_Back != null && newProvider.PRO_Govt_ID_Back != '') {
			newProvider.PRO_Govt_ID_Back = await serviceModule.getImage(newProvider.PRO_Govt_ID_Back)
		}
		if (newProvider.PRO_Driving_Licence_Front != null && newProvider.PRO_Driving_Licence_Front != '') {
			newProvider.PRO_Driving_Licence_Front = await serviceModule.getImage(newProvider.PRO_Driving_Licence_Front)
		}
		if (newProvider.PRO_Driving_Licence_Back != null && newProvider.PRO_Driving_Licence_Back != '') {
			newProvider.PRO_Driving_Licence_Back = await serviceModule.getImage(newProvider.PRO_Driving_Licence_Back)
		}

		if (newProvider.PRO_State_ID_Front != null && newProvider.PRO_State_ID_Front != '') {
			newProvider.PRO_State_ID_Front = await serviceModule.getImage(newProvider.PRO_State_ID_Front)
		}
		if (newProvider.PRO_State_ID_Back != null && newProvider.PRO_State_ID_Back != '') {
			newProvider.PRO_State_ID_Back = await serviceModule.getImage(newProvider.PRO_State_ID_Back)
		}

		if (newProvider.PRO_Passport_Front != null && newProvider.PRO_Passport_Front != '') {
			newProvider.PRO_Passport_Front = await serviceModule.getImage(newProvider.PRO_Passport_Front)
		}

		if (newProvider.PRO_Profile_Picture != null && newProvider.PRO_Profile_Picture != '') {
			newProvider.PRO_Profile_Picture = await serviceModule.getImage(newProvider.PRO_Profile_Picture)
		}
		if (newProvider.PRO_Special_Rate.length > 0) {
			newProvider.PRO_Special_Rate = newProvider.PRO_Special_Rate.filter(ele => {
				return new Date(ele.PRO_Special_Rate_Date) > new Date()
			})
		}
		var settingData = await model.Other.find()
		newProvider.PRO_Office_Office_Hours_New_Visit_Rate_AVG = settingData[0].PRO_Office_Office_Hours_New_Visit_Rate_AVG
		newProvider.PRO_Office_Office_Hours_Followup_Rate_AVG = settingData[0].PRO_Office_Office_Hours_Followup_Rate_AVG
		newProvider.PRO_Office_Off_Hours_New_Visit_Rate_AVG = settingData[0].PRO_Office_Off_Hours_New_Visit_Rate_AVG
		newProvider.PRO_Office_Off_Hours_Followup_Rate_AVG = settingData[0].PRO_Office_Off_Hours_Followup_Rate_AVG
		newProvider.PRO_Home_Office_Hours_New_Visit_Rate_AVG = settingData[0].PRO_Home_Office_Hours_New_Visit_Rate_AVG
		newProvider.PRO_Home_Office_Hours_Followup_Rate_AVG = settingData[0].PRO_Home_Office_Hours_Followup_Rate_AVG
		newProvider.PRO_Home_Off_Hours_New_Visit_Rate_AVG = settingData[0].PRO_Home_Off_Hours_New_Visit_Rate_AVG
		newProvider.PRO_Home_Off_Hours_Followup_Rate_AVG = settingData[0].PRO_Home_Off_Hours_Followup_Rate_AVG
		newProvider.PRO_Office_Office_Hours_New_Visit_Min = settingData[0].PRO_Office_Office_Hours_New_Visit_Min
		newProvider.PRO_Office_Office_Hours_Followup_Min = settingData[0].PRO_Office_Office_Hours_Followup_Min
		newProvider.PRO_Office_Off_Hours_New_Visit_Min = settingData[0].PRO_Office_Off_Hours_New_Visit_Min
		newProvider.PRO_Office_Off_Hours_Followup_Min = settingData[0].PRO_Office_Off_Hours_Followup_Min
		newProvider.PRO_Home_Office_Hours_New_Visit_Min = settingData[0].PRO_Home_Office_Hours_New_Visit_Min
		newProvider.PRO_Home_Office_Hours_Followup_Min = settingData[0].PRO_Home_Office_Hours_Followup_Min
		newProvider.PRO_Home_Off_Hours_New_Visit_Min = settingData[0].PRO_Home_Off_Hours_New_Visit_Min
		newProvider.PRO_Home_Off_Hours_Followup_Min = settingData[0].PRO_Home_Off_Hours_Followup_Min
		newProvider.AP_Phone_Number = settingData[0].HS_Support_Number
		let data = {}
		let attachment = {}
		if (settingData.length > 0) {
			data = JSON.parse(JSON.stringify(settingData[0]))
			data.AP_Phone_Number = settingData[0].HS_Support_Number
			if (data.PRO_Privacy_Policy_PDF_Name) {
				let url = await serviceModule.getImage(data.PRO_Privacy_Policy_PDF_Name)
				data.PRO_Privacy_Policy_PDF_Url = url
			}
			if (data.PRO_Terms_and_Conditions_PDF_Name) {
				let url = await serviceModule.getImage(data.PRO_Terms_and_Conditions_PDF_Name)
				data.PRO_Terms_and_Conditions_PDF_Url = url
			}
			if (data.PT_Privacy_Policy_PDF_Name) {
				let url = await serviceModule.getImage(data.PT_Privacy_Policy_PDF_Name)
				data.PT_Privacy_Policy_PDF_Url = url
			}
			if (data.PT_Terms_and_Conditions_PDF_Name) {
				let url = await serviceModule.getImage(data.PT_Terms_and_Conditions_PDF_Name)
				data.PT_Terms_and_Conditions_PDF_Name = url
			}
		}
		if (newProvider.PRO_CV.length > 0) {
			attachment = await getImageUrls(newProvider.PRO_CV)
			newProvider.PRO_CV = attachment
		}
		if (newProvider.PRO_State_Medical_License.length > 0) {
			attachment = await getImageUrls(newProvider.PRO_State_Medical_License)
			newProvider.PRO_State_Medical_License = attachment
		}
		if (newProvider.PRO_DEA_Cert.length > 0) {
			attachment = await getImageUrls(newProvider.PRO_DEA_Cert)
			newProvider.PRO_DEA_Cert = attachment
		}
		if (newProvider.PRO_Board_Cert.length > 0) {
			attachment = await getImageUrls(newProvider.PRO_Board_Cert)
			newProvider.PRO_Board_Cert = attachment
		}
		if (newProvider.PRO_Liability_Cert.length > 0) {
			attachment = await getImageUrls(newProvider.PRO_Liability_Cert)
			newProvider.PRO_Liability_Cert = attachment
		}

		newProvider.PRO_Speciality.sort(function (a, b) {
			return b.PRO_Primary_Speciality - a.PRO_Primary_Speciality
		})
		return res.status(200).send({
			code: 1,
			status: 'sucess',
			provider: newProvider,
			settingData: data
		})
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS303' })
	}
})

// Get file urls for documents //
var getImageUrls = async (arr) => {
	let newArray = []; let count = 0
	for (var i = 0; i < arr.length; i++) {
		let url = arr[i].Attachment_Name != null || arr[i].Attachment_Name != '' ? await serviceModule.getImage(arr[i].Attachment_Name) : null
		if (arr[i].Is_Deleted == false) {
			newArray.push({
				Attachment_Url: url,
				Is_Deleted: arr[i].Is_Deleted,
				_id: arr[i]._id,
				Attachment_Name: arr[i].Attachment_Name
			})
		}
		count = count + 1
		if (count == arr.length) {
			return newArray
		}
	}
}
// Get provider apecility list //
router.get('/get-specialitylist', authorize([Role.Provider]), async (req, res) => {
	try {
		let specialitylist = await model.Specialty.find()
		return res.send({ code: 1, status: 'success', data: specialitylist })
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS304' })
	}
})
// Get provider affliation list //
router.get('/get-affliationlist', authorize([Role.Provider]), async (req, res) => {
	try {
		let affliationlist = await model.Affiliation.find()
		return res.send({ code: 1, status: 'success', data: affliationlist })
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS305' })
	}
})
// Update provider emailId //
router.put('/update-email', authorize([Role.Provider]), async (req, res) => {
	const { body: { PRO_Username } } = req
	if (!PRO_Username) return res.send({ code: 0, status: 'failed', message: `Don't forget to enter your email address! It's required to reset your email`, Error_Code: errorModule.enter_your_email() })
	try {
		let provider = await model.Provider.findOneByEmail(PRO_Username)
		if (provider) {
			return res.status(200).send({
				code: 0,
				status: 'failed',
				message: 'This username is already associated with another account. Please log in',
				Error_Code: errorModule.username_already_associated()
			})
		} else {
			await model.Provider.updateById(req.user.PRO_UID, { PRO_Username: PRO_Username })
			const tokenPayload = {
				PRO_Id: req.user.PRO_UID,
				PRO_Username: PRO_Username,
				roles: 'Provider'
			}
			const token = jwt.sign(tokenPayload, process.env.JWT_SECRET, { expiresIn: 60 * 60 * 24 })
			var auditData = {
				Audit_UID: await auditUid(),
				Audit_Type: auditType.change,
				Audit_Info: 'New- ' + await auditUid(),
				Data_Point: 'PRO/Update email',
				PRO_First_Name: req.user.PRO_First_Name,
				PRO_Last_Name: req.user.PRO_Last_Name,
				PRO_UID: req.user.PRO_UID,
				IP_Address: await model.Audit.getIpAddress(req),
				New_Value: PRO_Username,
				Old_Value: req.user.PRO_Username
			}
			await model.Audit.create(auditData)
			if (req.user.PRO_Notifications == 'Yes') {
				const newDate = new Date()
				let msg = {
					from: process.env.ADMIN_FROM_EMAIL,
					to: req.user.PRO_Username,
					template_id: process.env.Notify_Provider_On_Account_Email_Updated,
					dynamic_template_data: {
						date: moment(newDate).format('MM/DD/YYYY'),
						time: moment(newDate).format('HH:MM:SS'),
						email: PRO_Username
					}
				}
				sgMail.send(msg)
			}
			return res.status(200).send({
				code: 1,
				status: 'success',
				message: 'Provider  email updated successfuly',
				token: token
			})
		}
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS306' })
	}
})
// Update provider password //
router.put('/update-password', authorize([Role.Provider]), async (req, res) => {
	const { body: { PRO_Password } } = req
	if (!PRO_Password) return res.send({ code: 0, status: 'failed', message: `Don't forget to enter your password ! It's required to reset your password`, Error_Code: errorModule.enter_your_password() })
	try {
		var reg = /^(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{10,45}$/
		if (!reg.test(PRO_Password)) {
			return res.send({ code: 0, status: 'failed', message: '*Please make sure the criteria below are met and try again', Error_Code: errorModule.make_sure_criteria() })
		} else {
			let hashPassword = await bcryptHash(PRO_Password, 10)
			await model.Provider.updateById(req.user.PRO_UID, { PRO_Password: hashPassword })
			var auditData = {
				Audit_UID: await auditUid(),
				Audit_Type: auditType.change,
				Audit_Info: 'New- ' + await auditUid(),
				Data_Point: 'PRO/Update password',
				PRO_First_Name: req.user.PRO_First_Name,
				PRO_Last_Name: req.user.PRO_Last_Name,
				PRO_UID: req.user.PRO_UID,
				IP_Address: await model.Audit.getIpAddress(req)
			}
			await model.Audit.create(auditData)
			return res.send({ code: 1, status: 'success', message: 'Provider password updated successfully' })
		}
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS307' })
	}
})
// service for send mail to admin if any question value is no //
router.post('/send-attestation-mail', authorize(Role.Provider), async (req, res) => {
	try {
		const PRO_Username = req.user.PRO_Username
		const provider = await model.Provider.findOneByEmail(PRO_Username)
		if (provider.PRO_Attestation_QA == 'No' || provider.PRO_Attestation_QB == 'No' || provider.PRO_Attestation_QC == 'No' || provider.PRO_Attestation_QD == 'No' || provider.PRO_Attestation_QJ == 'No' || provider.PRO_Attestation_QK == 'No' ||
			provider.PRO_Attestation_QE == 'No' || provider.PRO_Attestation_QF == 'No' || provider.PRO_Attestation_QG == 'No' || provider.PRO_Attestation_QH == 'No' || provider.PRO_Attestation_QI == 'No' || provider.PRO_Attestation_QL == 'No') {
			let ADMIN_message = {
				from: process.env.ADMIN_FROM_EMAIL,
				to: process.env.ADMIN_TO_EMAIL,
				template_id: process.env.NOTIFY_ADMIN_ON_ATTESTATION_NO,
				dynamic_template_data: {
					url: `${process.env.ADMIN_PORTAL}providers/${req.user.PRO_UID}?type=ATTESTATIONS`,
					date: moment(new Date()).format('MM/DD/YY'),
					time: moment(new Date()).format('HH:MM:SS'),
					PRO_Last_Name: provider.PRO_Last_Name
				}
			}
			sgMail.send(ADMIN_message)
		}
		return res.send({ code: 1, status: 'success', message: 'Mail send to admin for attestation question.' })
	} catch (err) {
		res.send({ code: 0, status: 'failed', message: err.message })
	}
})

module.exports = router
