const Router = require('express').Router
const router = new Router()
var aws = require('aws-sdk')
var multer = require('multer')
const fs = require('fs')
var upload = multer({ dest: 'uploads/' })
aws.config.update({
  secretAccessKey: process.env.AWS_SECRET_KEY,
  accessKeyId: process.env.AWS_ACCESS_KEY,
  region: 'us-east-1'
})

var s3 = new aws.S3()

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads')
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + file.originalname)
  }
})
upload = multer({ storage: storage })
// Service for upload provider's documents front image on aws server //
router.post('/uploadfrontimage', upload.single('front_image'), (req, res) => {
  var fileName = req.file.path
  var imagename = req.file.originalname

  fs.readFile(fileName, (err, data) => {
    if (err) throw err
    const params = {
      Bucket: process.env.AWS_BUCKET,
      Key: imagename,
      ContentType: 'image/png',
      Body: data
    }

    s3.upload(params, function (s3Err, data) {
      if (s3Err) throw s3Err
      res.image = data.Location
      console.log(`File uploaded successfully at ${data.Location}`)

      fs.unlink(fileName, err => {
        if (err) throw err
        res.status(200).send({ code: 1, status: 'success', data: data.Location })
      })
    })
  })
})
// Service for upload provider's documents back image on aws server //
router.post('/uploadbackimage', upload.single('back_image'), (req, res) => {
  var fileName = req.file.path
  var imagename = req.file.originalname

  fs.readFile(fileName, (err, data) => {
    if (err) throw err
    const params = {
      Bucket: process.env.AWS_BUCKET,
      Key: imagename,
      ContentType: 'image/png',
      Body: data
    }

    s3.upload(params, function (s3Err, data) {
      if (s3Err) throw s3Err
      res.image = data.Location
      console.log(`File uploaded successfully at ${data.Location}`)

      fs.unlink(fileName, err => {
        if (err) throw err
        res.status(200).send({ code: 1, status: 'success', data: data.Location })
      })
    })
  })
})
// Service for upload provider's image on aws server //
router.post('/upload-image', upload.single('image'), (req, res) => {
  var fileName = req.file.path
  var imagename = Date.now() + req.file.originalname

  fs.readFile(fileName, (err, data) => {
    if (err) throw err
    const params = {
      Bucket: process.env.AWS_BUCKET,
      Key: imagename,
      Body: data,
      ContentType: 'application/pdf',
      ACL: 'public-read'
    }
    s3.upload(params, function (s3Err, data) {
      if (s3Err) throw s3Err
      res.image = data.Location
      fs.unlink(fileName, err => {
        if (err) throw err
        res.status(200).send({ code: 1, status: 'success', data: data.Location })
      })
    })
  })
})

// Service for upload provider's image on aws server //
router.get('/get-image/:imagekey', (req, res) => {
  if (!req.params.imagekey) return res.status(422).send({ code: 422, status: 'failed', message: 'image key is required to get image!!' })
  const url = s3.getSignedUrl('getObject', {
    Bucket: process.env.AWS_BUCKET,
    Key: req.params.imagekey,
    Expires: 60 * 5 // time in seconds: e.g. 60 * 5 = 5 mins
  })
  return res.send(url)
})

// Service for upload provider's image on aws server //
router.get('/gnrate-token', (req, res) => {
  var CryptoJS = require('crypto-js')

  // var accessKeyID = process.env.AWS_ACCESS_KEY
  var secretAccessKey = process.env.AWS_SECRET_KEY

  // var bucket = process.env.AWS_BUCKET
  var region = 'us-east-1' // overwrite with your region
  // var expiration = '2019-11-30T12:00:00.000Z' // overwrite date
  var date = '20191030' // overwrite date
  var serviceName = 's3'

  function getSignatureKey (key, dateStamp, regionName, serviceName) {
    var kDate = CryptoJS.HmacSHA256(dateStamp, 'AWS4' + key)
    var kRegion = CryptoJS.HmacSHA256(regionName, kDate)
    var kService = CryptoJS.HmacSHA256(serviceName, kRegion)
    var kSigning = CryptoJS.HmacSHA256('aws4_request', kService)

    return kSigning
  }

  var s3Policy = {
    expiration: '2015-12-30T12:00:00.000Z',
    conditions: [
      { bucket: 'sigv4examplebucket' },
      ['starts-with', '$key', 'user/user1/'],
      { acl: 'public-read' },
      { success_action_redirect: 'http://storagehs.s3.amazonaws.com/successful_upload.html' },
      ['starts-with', '$Content-Type', 'image/'],
      { 'x-amz-meta-uuid': '14365123651274' },
      { 'x-amz-server-side-encryption': 'AES256' },
      ['starts-with', '$x-amz-meta-tag', ''],

      { 'x-amz-credential': 'AKIAYE4IFI3U3INXIDFT/20151229/us-east-1/s3/aws4_request' },
      { 'x-amz-algorithm': 'AWS4-HMAC-SHA256' },
      { 'x-amz-date': '20191129T115104Z' }
    ]
  }

  // eslint-disable-next-line node/no-deprecated-api
  var base64Policy = new Buffer(JSON.stringify(s3Policy), 'utf-8').toString('base64')
  console.log('base64Policy:', base64Policy)

  var signatureKey = getSignatureKey(secretAccessKey, date, region, serviceName)
  var s3Signature = CryptoJS.HmacSHA256(base64Policy, signatureKey).toString(CryptoJS.enc.Hex)
  return res.send({ s3Signature: s3Signature, base64Policy: base64Policy })
})

module.exports = router
