/* eslint-disable eqeqeq */
/* eslint-disable no-self-assign */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const model = require('../../models')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const logger = require('../../models/errorlog')
const moment = require('moment')
const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_PRIVATE_KEY)
const fcm = require('../../_helpers/fcm')
var AWS = require('aws-sdk')
const { mode } = require('crypto-js')
AWS.config.update({
	accessKeyId: process.env.AWS_ACCESS_KEY,
	secretAccessKey: process.env.AWS_SECRET_KEY,
	region: 'us-east-1'
})
var s3 = new AWS.S3()


// service for cancel encounter UID
router.put('/cancel-encounter/:Encounter_UID', authorize([Role.Provider, Role.Patient]), async (req, res) => {
	var { body: { data }, params: { Encounter_UID } } = req
	if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required', Error_Code: model.Errors.Encounter_UID_required() })
	if (!data) return res.send({ code: 0, status: 'failed', message: 'data is required', Error_Code: model.Errors.data_required() })
	try {

		// Check encounter with this encounter UID is available in the system or not
		let encounter = await model.Encounter.getEncounterByEncounter_UID(Encounter_UID)
		if (encounter) {

			// Find the encounter patient and provider
			var provider = await model.Provider.findById(encounter.PRO_UID)
			var patient = await model.Patient.findOneByEmail(encounter.PT_Username)

			if (encounter.Encounter_Status == 'Cancelled') return res.send({ code: 0, status: 'failed', message: 'encounter already Cancelled', Error_Code: 'HS056' })
			
			// Get the availablity for the encounter date
			const availability = await model.Availability.getProviderAvailability(encounter.PRO_UID, encounter.Encounter_Date)
			const providerAvailability = await model.ProviderAvailability.getProviderAvailability(encounter.PRO_UID, encounter.Encounter_Date)
			var Start_Time = moment(`${encounter.Encounter_Start_Time} ${encounter.Encounter_Start_Time_Zone}`, 'hh:mm A').format('HH.mm')
			var End_Time = moment(`${encounter.Encounter_End_Time} ${encounter.Encounter_End_Time_Zone}`, 'hh:mm A').format('HH.mm')
			Start_Time = Start_Time.includes(30) ? parseFloat(Start_Time) + 0.20 : parseFloat(Start_Time)
			End_Time = End_Time.includes(30) ? parseFloat(End_Time) + 0.20 : parseFloat(End_Time)
			var sIndex = await getSlotsTime(availability.PRO_Slots, Start_Time)
			var eIndex = await getSlotsTime(availability.PRO_Slots, End_Time)
			var count = 0
			var temp = 0
			if (sIndex != 0) {
				for (let i = sIndex - 1; ; i--) {
					if (availability.PRO_Slots[i].Is_Travel_Time == false) {
						if (availability.PRO_Slots[i].Is_Home_Available == true && availability.PRO_Slots[i].Is_Office_Available == true) {
							sIndex = i + 1
							break
						} else {
							sIndex = sIndex
							break
						}
					}
					count = count + 1
				}
			}
			if (eIndex != 47) {
				for (let i = eIndex; ; i++) {
					if (availability.PRO_Slots[i].Is_Travel_Time == false) {
						if (availability.PRO_Slots[i].Is_Home_Available == true && availability.PRO_Slots[i].Is_Office_Available == true) {
							eIndex = i - 1
							break
						} else {
							if (count == temp) {
								eIndex = eIndex - 1
							} else {
								eIndex = eIndex + count > 0 ? (count - 1) : count
							}
							break
						}
					}
					if (i == 47) break
					temp = temp + 1
				}
			}
			for (var i = sIndex; i <= eIndex; i++) {
				availability.PRO_Slots[i].Is_Home_Available = true
				availability.PRO_Slots[i].Is_Office_Available = true
				availability.PRO_Slots[i].Is_Travel_Time = false
				providerAvailability.PRO_Slots[i].Is_Office_Available = true
				providerAvailability.PRO_Slots[i].Is_Home_Available = true
				// encounter.Encounter_Location_Type == 'Office' ? providerAvailability.PRO_Slots[i].Is_Office_Available = true : providerAvailability.PRO_Slots[i].Is_Home_Available = true
			}

			// update the availability for the provider
			await model.Availability.updateAvailability(encounter.PRO_UID, availability)
			await model.ProviderAvailability.updateAvailability(encounter.PRO_UID, providerAvailability.PRO_Availability_Date, providerAvailability.PRO_Slots)
			data.Encounter_Cancelled_By = req.userType
			data.Encounter_Cancelled_User = req.userType == 'Provider' ? req.user.PRO_UID : req.user.PT_HS_MRN
			data.Encounter_Status = 'Cancelled'
			data.Encounter_Reschedule_Status = encounter.Encounter_Reschedule_Status == 'Requested' ? null : encounter.Encounter_Reschedule_Status
			
			// update encounter with the data
			await model.Encounter.updateEncounter(Encounter_UID, data)
			if (encounter.Encounter_Type == 'Followup Visit') {
				await model.Encounter.updateEncounter(encounter.Parent_Encounter, { Encounter_Followup_Status: 'Cancelled', Encounter_Has_Followup: false })
			}
			const message = {
				from: process.env.ADMIN_FROM_EMAIL,
				to: process.env.ADMIN_TO_EMAIL,
				template_id: process.env.NOTIFY_ADMIN_ON_CANCEL_ENCOUNTER_TEMPLATE,
				dynamic_template_data: {
					PRO_First_Name: provider.PRO_First_Name,
					PRO_Last_Name: provider.PRO_Last_Name,
					PRO_Username: provider.PRO_Username,
					PRO_Home_Phone: provider.PRO_Home_Phone,
					PRO_Office_Phone: provider.PRO_Office_Phone,
					PT_First_Name: patient.PT_First_Name,
					PT_Last_Name: patient.PT_Last_Name,
					PT_Username: patient.PT_Username,
					PT_Cell_Phone: patient.PT_Cell_Phone == '0000000000' ? null : patient.PT_Cell_Phone,
					Encounter_Date: moment(new Date(encounter.Encounter_Date)).format('MM/DD/YY'),
					Encounter_Time: encounter.Encounter_Time
				}
			}

			if (req.userType == 'Patient') {
				let jsonData = await NotificationData(encounter, patient, provider, data)
				let arr = []
				let token = await model.Notification.getToken('Provider', provider.PRO_Username)
				token.map(ele => arr.push(ele.Device_Token))

				// send notification to the provider
				if (arr.length != 0) {
					if (encounter.Encounter_Type == 'New Visit') {
						let title = `Visit Cancelled: ${moment(new Date(encounter.Encounter_Date)).format('M/D/YY')}`
						let body = 'You can review or reschedule this visit in the mobile app.'
						await sendNotification(arr, title, body, jsonData)
					} else {
						let title = `Followup Cancelled: ${moment(new Date(encounter.Encounter_Date)).format('M/D/YY')}`
						let body = `${patient.PT_First_Name} ${patient.PT_Last_Name.charAt(0)} has cancelled a followup visit with you. The visit has been removed from your calendar.`
						await sendNotification(arr, title, body, jsonData)
					}
				}
			} else {
				let jsonData = await NotificationData(encounter, patient, provider, data)
				let arr = []
				let token = await model.Notification.getToken('Patient', patient.PT_Username)
				token.map(ele => arr.push(ele.Device_Token))

				// send notification to the patient
				if (arr.length != 0) {
					let title = encounter.Encounter_Type == 'New Visit' ? 'Visit Cancelled' : 'Followup visit cancelled'
					let body = encounter.Encounter_Type == 'New Visit' ? 'An upcoming visit has been cancelled.' : 'An upcoming followup visit has been cancelled.'
					await sendNotification(arr, title, body, jsonData)
				}
			}

			// send mail to the admin for cancel encounter
			sgMail.send(message)
			// await createAudit(req);
			res.send({ code: 1, status: 'sucess', message: 'Encounter cancelled successfully' })
		} else {
			// await createAudit(req);
			res.send({ code: 0, status: 'failed', message: `No encounter find with ${Encounter_UID}`, Error_Code: model.Errors.Encounter_not_found() })
		}
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS057' })
	}
})


// Function for get slot time
function getSlotsTime (arr, time) {
	var index = -1
	arr.find((ele, i) => {
		if (ele.Slot_Format == time) {
			index = i
			return i
		}
	})
	return index
}


// Function for the send notification
async function sendNotification (arr, title, body, jsonData) {
	var payload = { // this may vary according to the message type (single recipient, multicast, topic, et cetera)
		registration_ids: arr,
		notification: {
			title: title,
			body: body
		},

		data: jsonData
	}
	fcm.send(payload, function (err, response) {
		if (err) {
		} else console.log(response)
	})
}


// Function for the get image url from the image key
function getImageUrl (imageKey) {
	if (imageKey == null || imageKey == '') {
		return ''
	} else {
		return new Promise((resolve, reject) => {
			const url = s3.getSignedUrl('getObject', {
				Bucket: process.env.AWS_BUCKET,
				Key: imageKey,
				Expires: 60 * 60 * 24 * 15
			})
			resolve(url)
		})
	}
}


// Function for get the notification data
async function NotificationData (encounter, patient, provider, data) {
	let encounterPatient = await model.Patient.findById(encounter.PT_HS_MRN)
	let jsonData = {
		PT_Profile_Picture: encounterPatient.PT_Profile_Picture ? await getImageUrl(encounterPatient.PT_Profile_Picture) : '',
		PRO_Profile_Picture: provider.PRO_Profile_Picture ? await getImageUrl(provider.PRO_Profile_Picture) : '',
		PT_HS_MRN: encounterPatient.PT_HS_MRN,
		PT_First_Name: encounterPatient.PT_First_Name,
		PT_Last_Name: encounterPatient.PT_Last_Name,
		PT_Gender: encounterPatient.PT_Gender,
		PT_Age_Now: (new Date().getFullYear() - encounterPatient.PT_DOB.getFullYear()).toString(),
		PRO_Last_Name: provider.PRO_Last_Name,
		PRO_UID: provider.PRO_UID,
		PRO_Gender: provider.PRO_Gender,
		Encounter_UID: encounter.Encounter_UID,
		Encounter_Date: encounter.Encounter_Date,
		Encounter_Time: encounter.Encounter_Time,
		Encounter_Type: encounter.Encounter_Type,
		Encounter_Chief_Complaint_PRO: encounter.Encounter_Chief_Complaint_PRO ? encounter.Encounter_Chief_Complaint_PRO : '',
		Encounter_Chief_Complaint_PT: encounter.Encounter_Chief_Complaint_PT ? encounter.Encounter_Chief_Complaint_PT : '',
		Encounter_PT_Chief_Complaint_More: encounter.Encounter_PT_Chief_Complaint_More ? encounter.Encounter_PT_Chief_Complaint_More : '',
		Cancellation_Reason: data.Cancellation_Reason,
		title: 'Encounter_Cancelled'
	}
	return jsonData
}

module.exports = router
