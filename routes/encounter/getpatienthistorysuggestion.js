/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const model = require('../../models')
const errorModule = require('../../models').Errors
const serviceModule = require('../../models').CommonServices
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const logger = require('../../models/errorlog')
var aws = require('aws-sdk')
aws.config.update({
  secretAccessKey: process.env.AWS_SECRET_KEY,
  accessKeyId: process.env.AWS_ACCESS_KEY,
  region: 'us-east-1'
})

var s3 = new aws.S3()

router.get('/get-suggestion/:Encounter_UID/:PT_HS_MRN', authorize(Role.Provider), async (req, res) => {
  var { params: { Encounter_UID, PT_HS_MRN } } = req
  if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required!', Error_Code: errorModule.Encounter_UID_required() })
  if (!PT_HS_MRN) return res.send({ code: 0, status: 'failed', message: 'PT_HS_MRN is required!', Error_Code: errorModule.PT_HS_MRN_required() })
  try {
    let patientHistory = await model.PatientHistory.findPatientHistoryById(PT_HS_MRN)
    let patient = await model.Patient.getPatientByHSId(PT_HS_MRN)
    let encounter = await model.Encounter.getEncounterByEncounter_UID(Encounter_UID)
    if (!encounter) return res.send({ code: 0, status: 'failed', message: `encounter with id '${Encounter_UID}' not found!`, Error_Code: errorModule.Encounter_not_found() })
    let patientEncounterHistory = encounter.PT_History
    let encounterData = {}

    encounterData.Encounter_Date = encounter.Encounter_Date
    encounterData.Encounter_UID = encounter.Encounter_UID
    encounterData.Encounter_Time = encounter.Encounter_Time
    encounterData.Encounter_Chief_Complaint_PRO = encounter.Encounter_Chief_Complaint_PRO
    encounterData.Encounter_Chief_Complaint_PT = encounter.Encounter_Chief_Complaint_PT
    encounterData.Encounter_PT_Chief_Complaint_More = encounter.Encounter_PT_Chief_Complaint_More
    encounterData.Encounter_Type = encounter.Encounter_Type
    encounterData.PT_First_Name = patient[0].PT_First_Name
    encounterData.PT_Last_Name = patient[0].PT_Last_Name
    encounterData.PT_Gender = patient[0].PT_Gender
    encounterData.PT_Age_Now = new Date().getFullYear() - patient[0].PT_DOB.getFullYear()
    encounterData.PT_HS_MRN = patient[0].PT_HS_MRN
    encounterData.PT_Profile_Picture = patient[0].PT_Profile_Picture != null ? await serviceModule.getImage(patient[0].PT_Profile_Picture) : null

    var jsonObject1 = patientEncounterHistory
    var jsonObject2 = patientHistory
    let differanceArray = {}

    delete jsonObject1._id
    delete jsonObject2._id
    delete jsonObject1.Created_At
    delete jsonObject2.Created_At
    delete jsonObject1.Updated_At
    delete jsonObject2.Updated_At
    delete jsonObject1.__v
    delete jsonObject2.__v
    var keys = Object.keys(jsonObject1)
    let count = 0
    for (var i = 0; i < keys.length; i++) {
      var key = keys[i]
      if (
        key != 'PT_HX_Last_Updated_About_Section' &&
        key != 'PT_General_Update_Toggle' &&
        key != 'PT_HX_Last_Updated_General_Section' &&
        key != 'PT_Medicaitons_Update_Toggle' &&
        key != 'PT_HX_Last_Updated_Medications_Section' &&
        key != 'PT_Other_Update_Toggle' &&
        key != 'PT_HX_Last_Updated_Other_Section' &&
        key != 'PT_OBGYN_Update_Toggle' &&
        key != 'PT_HX_Last_Updated_OB_Section' &&
         key != 'PT_Cell_Phone' && key != 'PT_About_Update_Toggle' && key != 'PRO_UID' && key != 'PT_Username' && key != 'PT_Organ_Donor' && key != 'PT_First_Name' && key != 'PT_Profile_Picture' && !key.includes('Confirmation')) {
        if (jsonObject1[key] != null && typeof (jsonObject1[key]) == 'object') {
          differanceArray[key] = {
            oldValue: jsonObject1[key] == null || jsonObject1[key] == undefined ? jsonObject1[key] : typeof (jsonObject1[key]) == 'number' ? `${jsonObject1[key]}` : typeof (jsonObject1[key]) == 'object' ? await getImageUrls(jsonObject1[key]) : jsonObject1[key],
            newValue: jsonObject2[key] == null || jsonObject2[key] == undefined ? jsonObject2[key] : typeof (jsonObject2[key]) == 'number' ? `${jsonObject2[key]}` : typeof (jsonObject2[key]) == 'object' ? await getImageUrls(jsonObject2[key]) : jsonObject2[key]
          }
          if (jsonObject1[key] != null && jsonObject1[key] != undefined && jsonObject1[key] && typeof (jsonObject1[key]) == 'object') {
            count = jsonObject1[key].length != jsonObject2[key].length ? count + 1 : count
          }
        } else {
          if (jsonObject1[key] != jsonObject2[key]) {
            differanceArray[key] = {
              oldValue: jsonObject1[key] == null || jsonObject1[key] == undefined ? jsonObject1[key] : typeof (jsonObject1[key]) == 'number' ? `${jsonObject1[key]}` : typeof (jsonObject1[key]) == 'object' ? await getImageUrls(jsonObject1[key]) : jsonObject1[key],
              newValue: jsonObject2[key] == null || jsonObject2[key] == undefined ? jsonObject2[key] : typeof (jsonObject2[key]) == 'number' ? `${jsonObject2[key]}` : typeof (jsonObject2[key]) == 'object' ? await getImageUrls(jsonObject2[key]) : jsonObject2[key]
            }
            if (differanceArray[key].newValue) {
              count = count + 1
            }
          } else {
            differanceArray[key] = {
              oldValue: jsonObject1[key] == null || jsonObject1[key] == undefined ? jsonObject1[key] : typeof (jsonObject1[key]) == 'number' ? `${jsonObject1[key]}` : typeof (jsonObject1[key]) == 'object' ? await getImageUrls(jsonObject1[key]) : jsonObject1[key],
              newValue: jsonObject1[key] == null || jsonObject1[key] == undefined ? jsonObject1[key] : typeof (jsonObject1[key]) == 'number' ? `${jsonObject1[key]}` : typeof (jsonObject1[key]) == 'object' ? await getImageUrls(jsonObject1[key]) : jsonObject1[key]
            }
          }
        }
      }
      if (i == keys.length - 1) {
        // eslint-disable-next-line no-unneeded-ternary
        let differance = count != 0 ? true : false
        res.send({ code: 1, status: 'sucess', Encounter_Data: encounterData, Changes: differanceArray, differance: differance })
      }
    }
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS093' })
  }
})

let getImageUrls = async (arr) => {
  let newArray = []; let count = 0
  if (arr == null || arr.length == 0) {
    return newArray
  }
  if (arr.length == undefined) {
    return arr
  }
  if (arr[0].Attachment_Name) {
    for (var i = 0; i < arr.length; i++) {
      let url = arr[i].Attachment_Name != null || arr[i].Attachment_Name != '' ? await getImageUrl(arr[i].Attachment_Name) : null
      newArray.push({
        Attachment_Url: url,
        Is_Deleted: arr[i].Is_Deleted,
        _id: arr[i]._id,
        Attachment_Name: arr[i].Attachment_Name,
        Attachment_By: arr[i].Attachment_By,
        Attachment_User: arr[i].Attachment_User
      })
      count = count + 1
      if (count == arr.length) {
        return newArray
      }
    }
  } else {
    return arr
  }
}

let getImageUrl = async (imageKey) => {
  if (imageKey == null || imageKey == '') {
    return null
  } else {
    if (imageKey.includes('pdf')) {
      const url = s3.getSignedUrl('getObject', {
        Bucket: process.env.AWS_BUCKET,
        Key: imageKey,
        ResponseContentType: 'application/pdf',
        Expires: 60 * 60 * 24 * 15 // time in seconds: e.g. 60 * 5 = 5 mins
      })
      return url
    } else {
      const url = s3.getSignedUrl('getObject', {
        Bucket: process.env.AWS_BUCKET,
        Key: imageKey,
        Expires: 60 * 60 * 24 * 15 // time in seconds: e.g. 60 * 5 = 5 mins
      })
      return url
    }
  }
}
module.exports = router
