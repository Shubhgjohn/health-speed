/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const model = require('../../models')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const { auditUid } = require('../../utils/UID')
const auditType = require('../../auditType.json')
const logger = require('../../models/errorlog')
var stripe = require('stripe')(process.env.SECRET_KEY)
const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_PRIVATE_KEY)
const request = require('request')
const moment = require('moment')


// service for close encounter from encounter uid
router.put('/close-encounter/:Encounter_UID', authorize(Role.Provider), async (req, res) => {
	var { body: { data }, params: { Encounter_UID } } = req
	if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required', Error_Code: model.Errors.Encounter_UID_required() })
	if (!data) return res.send({ code: 0, status: 'failed', message: 'data is required', Error_Code: model.Errors.data_required() })
	try {

		// Find encounter is exixt with this encounter uid or not
		let encounter = await model.Encounter.getEncounterByEncounter_UID(Encounter_UID)
		if (encounter) {
			let encounter_data = {
				Exam_Start_Time: data.Encounter_Time.includes(' ') ? `${data.Encounter_Time.split(' ')[0]} ${data.Encounter_Time.split(' ')[1]}` : `${data.Encounter_Time.slice(0, -2)} ${data.Encounter_Time.slice(-2).toLowerCase()}`,
				Exam_End_Time: data.Encounter_End_Time.includes(' ') ? `${data.Encounter_End_Time.split(' ')[0]} ${data.Encounter_End_Time.split(' ')[1]}` : `${data.Encounter_End_Time.slice(0, -2)} ${data.Encounter_End_Time.slice(-2).toLowerCase()}`,
				Encounter_Status: 'Active'
			}

			// Get chart data of this encounter
			let chart = await model.Chart.getChartData(Encounter_UID)
			if (chart) {

				// find encounter patient and provider of the encounter
				let patient = await model.Patient.findOneByEmail(encounter.PT_Username)
				let provider = await model.Provider.findById(encounter.PRO_UID)

				// Get complete encounter , signed and unsigned charts of the provider
				let completeEncounter = await model.Chart.getPatientSignedDocCount(patient.PT_Username)
				let completeChart = await model.Chart.getProviderSignedDocCount(provider.PRO_UID)
				let unsignedChart = await model.Chart.getProviderUnsignedDocCount(provider.PRO_UID)

				if (encounter.Encounter_Transaction_Status == 'Unpaid' && encounter.Transaction_Status == 'Unpaid') {
					let card = null
					if (encounter.Payment_Card_Id) {
						try {
							await model.Stripe.makeDefault(patient.stripeCustomerId, encounter.Payment_Card_Id)
							card = await model.Stripe.getCardByCardId(patient.stripeCustomerId, encounter.Payment_Card_Id)
						} catch (error) {
							if (error) card = null
						}
					}
					let providerAccount = provider.StripeAccountId

					// Create payment
					let result = await makePayment(encounter, patient, card, providerAccount)
					const closeDate = moment(Date.parse(moment.tz(new Date(), process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm'))).toDate()
					
					// if payment is failed then we have to send email
					if (result == 'Failed') {
						await model.Provider.updateById(provider.PRO_UID, { PRO_Number_of_Complete_Charts: completeChart, PRO_Number_of_Unsigned_Docs: unsignedChart })
						await model.Patient.updatePatientByUsername(patient.PT_Username, { PT_Total_Number_of_Encounters: completeEncounter })
						await model.Encounter.updateEncounter(Encounter_UID, { Transaction_Status: 'Failed', Encounter_Close_Date: closeDate })
						if (patient.PT_Email_Notification == 'Enabled') {
							let PT_Msg = await patientMail(patient, encounter)
							sgMail.send(PT_Msg)
						}
						let msg = await adminMail(encounter, patient)

						// send mail to the patient
						sgMail.send(msg)
					} else if (result == 'Paid') {
						let providerEarned = provider.PRO_Earnings_to_Date + ((encounter.Encounter_Cost * 90) / 100) * 100
						let totalAmmount = patient.PT_Total_Amount_Paid + encounter.Encounter_Cost
						await model.Provider.updateById(provider.PRO_UID, { PRO_Number_of_Complete_Charts: completeChart, PRO_Number_of_Unsigned_Docs: unsignedChart, PRO_Earnings_to_Date: providerEarned })
						await model.Patient.updatePatientByUsername(patient.PT_Username, { PT_Total_Number_of_Encounters: completeEncounter, PT_Total_Amount_Paid: totalAmmount })
						await model.Encounter.updateEncounter(Encounter_UID, { Encounter_Transaction_Status: 'Paid', Transaction_Status: 'Paid', Encounter_Close_Date: closeDate })
					}
				}
				if (patient.PT_Email_Notification == 'Enabled') {
					request.get({
						url: `${process.env.API_HOST}/encounter/get-suggestion/${encounter.Encounter_UID}/${encounter.PT_HS_MRN}`,
						headers: { authorization: req.headers.authorization }
						// eslint-disable-next-line handle-callback-err
					}, async function (err, response, body) {
						let data = JSON.parse(body)
						if (data.differance) {
							await sendMail(encounter)
						}
					})
				}

				// update the encounter data
				await model.Encounter.updateEncounter(Encounter_UID, encounter_data)
				let d = new Date()
				var newYork = moment.tz(d, process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm:ss')
				let chartData = {
					Chart_is_Signed: true,
					PRO_Private_Note: data.PRO_Private_Note,
					Updated_At: req.headers.host.includes('localhost') ? moment(Date.parse(newYork)).add(330, 'm').toDate() : moment(Date.parse(newYork)).toDate()
				}

				// update the encounter chart
				await model.Chart.updateEncounterChartById(Encounter_UID, chartData)
			} else {
				res.send({
					code: 0,
					status: 'failed',
					message: `No chart data found for encounter UID '${Encounter_UID}'`,
					Error_Code: model.Errors.No_chart_found()
				})
			}
			await createAudit(req)
			res.send({ code: 1, status: 'sucess', message: 'Encounter close successfully' })
		} else {
			// await createAudit(req);
			res.send({ code: 0, status: 'failed', message: `No encounter find with ${Encounter_UID}`, Error_Code: model.Errors.Encounter_not_found() })
		}
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS058' })
	}
})


// Function for the create audit
let createAudit = async (req) => {
	const auditData = {
		Audit_UID: await auditUid(),
		Audit_Info: 'New- ' + await auditUid(),
		Data_Point: 'PRO/Close Encounter ',
		Audit_Type: auditType.change,
		PRO_First_Name: req.user.PRO_First_Name,
		PRO_Last_Name: req.user.PRO_Last_Name,
		PRO_UID: req.user.PRO_UID,
		IP_Address: await model.Audit.getIpAddress(req)
	}
	await model.Audit.create(auditData)
}


// Function for make payment
async function makePayment (encounter, patient, card, providerAccount) {
	if (card == null) {
		let data = {
			Encounter_UID: encounter.Encounter_UID,
			Payment_Type: 'New Payment',
			Payment_Date: new Date(),
			Amount: encounter.Encounter_Cost,
			Charge_Id: null,
			Transaction_Status: 'Failed',
			Payment_By: patient.PT_HS_MRN,
			PRO_UID: encounter.PRO_UID
		}
		await model.Transactions.create(data)
		return 'Failed'
	}
	else {
		stripe.charges.create(
			{
				amount: encounter.Encounter_Cost * 100,
				currency: 'usd',
				customer: patient.stripeCustomerId,
				transfer_data: {
					amount: ((encounter.Encounter_Cost * 90) / 100) * 100,
					destination: providerAccount
				}
			},
			async function (err, charge) {
				if (err) {
					let data = {
						Encounter_UID: encounter.Encounter_UID,
						Payment_Type: 'New Payment',
						Payment_Date: new Date(),
						Amount: encounter.Encounter_Cost,
						Charge_Id: null,
						Transaction_Status: 'Failed',
						Payment_By: patient.PT_HS_MRN,
						PRO_UID: encounter.PRO_UID
					}
					await model.Transactions.create(data)
					return 'Failed'
				}
				let data = {
					Encounter_UID: encounter.Encounter_UID,
					Payment_Type: 'New Payment',
					Payment_Date: new Date(),
					Amount: encounter.Encounter_Cost,
					receipt_url: charge.receipt_url,
					Transaction_Id: charge.balance_transaction,
					Charge_Id: charge.id,
					Transaction_Status: 'Paid',
					Payment_By: patient.PT_HS_MRN,
					PRO_UID: encounter.PRO_UID
				}
				await model.Transactions.create(data)
				return 'Paid'
			}
		)
	}
	
}
// Function for send email to the patient for payment failure
function patientMail (patient, encounter) {
	let message = {
		from: process.env.ADMIN_FROM_EMAIL,
		to: patient.PT_Username,
		template_id: process.env.Notify_Patient_On_Payment_Failure,
		dynamic_template_data: {
			url: `${process.env.API_HOST}/patient/main/payment/${encounter.Encounter_UID}/retry`
		}
	}
	return message
}

// Function for send mail to admin for payment failure
function adminMail (encounter, patient) {
	let message = {
		from: process.env.ADMIN_FROM_EMAIL,
		to: process.env.ADMIN_TO_EMAIL,
		template_id: process.env.Notify_Admin_On_Payment_Failure,
		dynamic_template_data: {
			Encounter_Date: encounter.Encounter_Date,
			Encounter_Time: encounter.Encounter_Time,
			PT_First_Name: patient.PT_First_Name,
			PT_Last_Name: patient.PT_Last_Name,
			PT_Username: patient.PT_Username,
			PT_Cell_Phone: patient.PT_Cell_Phone
		}
	}
	return message
}

// Function for send mail to admin for suggestion cards
async function sendMail (encounter) {
	let msg = {
		to: encounter.PT_Username,
		from: process.env.ADMIN_FROM_EMAIL,
		template_id: process.env.Notify_Patient_On_Medical_History_Change_Suggestions,
		dynamic_template_data: {
			url: `${process.env.API_HOST}/patient/main/editsuggest/${encounter.Encounter_UID}/history`
		}
	}
	sgMail.send(msg)
}

module.exports = router
