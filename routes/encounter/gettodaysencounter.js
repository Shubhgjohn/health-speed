/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
/* eslint-disable prefer-const */
const Router = require('express').Router
const router = new Router()
const moment = require('moment')
const model = require('../../models')
const _ = require('lodash')
const encounterModule = require('../../models').Encounter
const orderModule = require('../../models').Order
const chartModule = require('../../models').Chart
const serviceModule = require('../../models').CommonServices
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const logger = require('../../models/errorlog')
const errorModule = require('../../models').Errors
const momentTz = require('moment-timezone')
var aws = require('aws-sdk')
aws.config.update({
  secretAccessKey: process.env.AWS_SECRET_KEY,
  accessKeyId: process.env.AWS_ACCESS_KEY,
  region: 'us-east-1'
})

var s3 = new aws.S3()


router.get('/get-todays-visits', authorize(Role.Provider), async (req, res) => {
  let today = momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('MM-DD-YYYY')
  let attention = {}; let totalEncounter = []
  try {
    let allEncounters = await encounterModule.getProviderTodaysEncounter(req.user.PRO_UID, today)
    allEncounters = allEncounters.filter(ele => (Date.parse(moment(new Date(`${ele.Encounter_Date} ${ele.Encounter_Time}`)).format('YYYY-MM-DDTHH:mm')) >= Date.parse(momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm'))) && ele.Encounter_Status != 'Cancelled' && ele.Encounter_Status != 'Abandoned' && ele.Encounter_Status != 'Requested')
    if (allEncounters.length > 0) {
      let filteredEncounter = allEncounters.map(ele => ({
        Encounter_Type: ele.Encounter_Type,
        Encounter_UID: ele.Encounter_UID,
        PT_First_Name: ele.patient[0].PT_First_Name,
        PT_Last_Name: ele.patient[0].PT_Last_Name,
        PT_Age_Now: ele.patient[0].PT_Age_Now,
        Encounter_Transaction_Status: ele.Encounter_Transaction_Status,
        Encounter_Location_Type: ele.Encounter_Location_Type,
        Encounter_Date: ele.Encounter_Date,
        Encounter_Time: ele.Encounter_Start_Time + ' ' + ele.Encounter_Start_Time_Zone,
        Encounter_Chief_Complaint_PRO: ele.Encounter_Chief_Complaint_PRO,
        Encounter_Cost: ele.Encounter_Cost,
        PT_Gender: ele.patient[0].PT_Gender,
        PT_HS_MRN: ele.patient[0].PT_HS_MRN,
        PT_DOB: ele.patient[0].PT_DOB
      }))
      // await createAudit(req);
      filteredEncounter = filteredEncounter.sort(compareDsc)
      totalEncounter = filteredEncounter
    } else {
      totalEncounter = []
    }
    let orders = await orderModule.getAllProviderOrders(req.user.PRO_UID)
    let orderArray = []
    if (orders.length > 0) {
      orders.sort(compareAsc)
      orders.map(async ele => {
        if (Date.parse(momentTz.tz(ele.Reminder_Date, process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm')) > Date.parse(momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm'))) {
          if (!ele.Encounter_Time.includes(' ')) {
            if (ele.Encounter_Time.includes('a')) {
              ele.Encounter_Time = `${ele.Encounter_Time.split('a')[0]} am`
            } else {
              ele.Encounter_Time = `${ele.Encounter_Time.split('a')[0]} pm`
            }
          }
          if (!ele.Order_Complete) {
            orderArray.push({
              Order_UID: ele.Order_UID,
              Order_Complete: ele.Order_Complete ? ele.Order_Complete : false,
              Order_Reminder: ele.Order_Reminder ? ele.Order_Reminder : 'Unchecked',
              Order_Type: ele.Order_Type ? ele.Order_Type : null,
              Order_Review_Status: ele.Order_Review_Status ? ele.Order_Review_Status : 'Review',
              Order_Text: ele.Order_Text ? ele.Order_Text : null,
              Order_Stat: ele.Order_Stat ? ele.Order_Stat : 'Unchecked',
              Order_Status: ele.Order_Status ? ele.Order_Status : 'Draft',
              Reminder_Date: ele.Reminder_Date ? momentTz.tz(ele.Reminder_Date, process.env.CURRENT_TIMEZONE).format('MM-DD-YYYY') : null,
              Encounter_Date: ele.Encounter_Date ? ele.Encounter_Date : null,
              Encounter_Type: ele.Encounter_Type ? ele.Encounter_Type : null,
              Encounter_Time: ele.Encounter_Time ? ele.Encounter_Time : null,
              Encounter_UID: ele.Encounter_UID ? ele.Encounter_UID : null,
              Encounter_Chief_Complaint_PRO: ele.Encounter_Chief_Complaint_PRO ? ele.Encounter_Chief_Complaint_PRO : null,
              PT_First_Name: ele.PT_First_Name ? ele.PT_First_Name : null,
              PT_Last_Name: ele.PT_Last_Name ? ele.PT_Last_Name : null,
              PT_Age_Now: ele.PT_Age_Now ? ele.PT_Age_Now : null,
              PT_Gender: ele.PT_Gender ? ele.PT_Gender : null,
              PT_DOB: moment(new Date(ele.PT_DOB)).format('MM-DD-YYYY'),
              PT_Profile_Picture: ele.PT_Profile_Picture ? await serviceModule.getImage(ele.PT_Profile_Picture) : null,
              PT_Username: ele.PT_Username ? ele.PT_Username : null,
              PT_HS_MRN: ele.PT_HS_MRN ? ele.PT_HS_MRN : null,
              Chart_is_Signed: ele.Chart_is_Signed ? ele.Chart_is_Signed : false
            })
          }
        }
      })
      attention.Order = orderArray
    } else {
      attention.Order = orderArray
    }

    let mainData = await chartModule.getProviderChart(req.user.PRO_UID)
    let data = []
    if (mainData.length == 0) {
      attention.addendum = []
      attention.Discharge = []
      attention.Chart = []
    } else {
      data = await createArray(mainData)
      let addendumData = data.addendumData
      addendumData = addendumData.map(({ Discharge_Instructions_Status, ...rest }) => rest)
      addendumData = addendumData.map(({ Chart_is_Signed, ...rest }) => rest)
      attention.addendum = addendumData

      let dischargeCardData = data.dischargeData
      dischargeCardData = _.uniqBy(dischargeCardData, 'Encounter_UID')
      attention.Discharge = dischargeCardData.filter(item => (Math.abs(Date.parse(momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm:ss')) - Date.parse(moment(new Date(`${item.Encounter_Date} ${item.Encounter_Time}`)).format('YYYY-MM-DDTHH:mm:ss'))) / 36e5) > 4)

      let chartData = mainData
      chartData = await getChartData(chartData)
      if (chartData) {
        chartData = chartData.map(({ Addendum_Is_Signed, ...rest }) => rest)
        dischargeCardData = dischargeCardData.map(({ PRO_Addendum_Attchment, ...rest }) => rest)
        chartData = chartData.map(({ Discharge_Instructions_Status, ...rest }) => rest)
        chartData = chartData.map(({ PRO_Addendum_Note, ...rest }) => rest)
        chartData = _.uniqBy(chartData, 'Encounter_UID')
        attention.Chart = chartData.filter(item => item.Encounter_Status != 'Cancelled')
      } else {
        attention.Chart = []
      }
    }
    let encounters = await encounterModule.getProvidersAllEncounter(req.user.PRO_UID)
    if (encounters.length == 0) {
      attention.Encounter_Cancelled_Card = []
      attention.Encounter_Reschedule_Request_Accepted = []
      attention.Encounter_Reschedule_Request_Declined = []
    } else {
      let encounterData = encounters.map(ele => ({
        Encounter_UID: ele.Encounter_UID ? ele.Encounter_UID : null,
        PRO_UID: ele.PRO_UID ? ele.PRO_UID : null,
        Encounter_Chief_Complaint_PRO: ele.Encounter_Chief_Complaint_PRO ? ele.Encounter_Chief_Complaint_PRO : null,
        Encounter_Type: ele.Encounter_Type ? ele.Encounter_Type : null,
        Encounter_Location_Type: ele.Encounter_Location_Type ? ele.Encounter_Location_Type : null,
        Encounter_Time: ele.Encounter_Time ? ele.Encounter_Time : null,
        Encounter_Date: ele.Encounter_Date ? ele.Encounter_Date : null,
        Encounter_Start_Time: ele.Encounter_Start_Time ? ele.Encounter_Start_Time : null,
        Encounter_Start_Time_Zone: ele.Encounter_Start_Time_Zone ? ele.Encounter_Start_Time_Zone.toLowerCase() : null,
        Encounter_Status: ele.Encounter_Status ? ele.Encounter_Status : null,
        Cancellation_Reason: ele.Cancellation_Reason ? ele.Cancellation_Reason : null,
        Encounter_Provider: ele.Encounter_Provider ? ele.Encounter_Provider : null,
        Primary_DX: ele.Primary_DX ? ele.Primary_DX : null,
        Encounter_Reschedule_Status: ele.Encounter_Reschedule_Status ? ele.Encounter_Reschedule_Status : null,
        Encounter_Access_Provider: ele.Encounter_Access_Provider ? ele.Encounter_Access_Provider : null,
        PT_HS_MRN: ele.PT_HS_MRN ? ele.PT_HS_MRN : null,
        PT_First_Name: ele.PT_First_Name ? ele.PT_First_Name : null,
        PT_Last_Name: ele.PT_Last_Name ? ele.PT_Last_Name : null,
        PT_Age_Now: ele.PT_Age_Now ? ele.PT_Age_Now : null,
        PT_Gender: ele.PT_Gender ? ele.PT_Gender : null,
        PT_DOB: ele.PT_DOB ? moment(new Date(ele.PT_DOB)).format('MM-DD-YYYY') : null,
        PT_Profile_Picture: ele.PT_Profile_Picture ?  s3.getSignedUrl('getObject', {
          Bucket: process.env.AWS_BUCKET,
          Key: ele.PT_Profile_Picture,
          Expires: 60 * 60 * 24 * 15 
        }) : null
      }))
      attention.Encounter_Cancelled_Card = encounterData.filter(ele => ele.Encounter_Status == 'Cancelled')
      attention.Encounter_Cancelled_Card = attention.Encounter_Cancelled_Card.length > 0 ? attention.Encounter_Cancelled_Card.sort(compareAsc) : []
      attention.Encounter_Reschedule_Request_Accepted = encounterData.filter(ele => ele.Encounter_Reschedule_Status == 'Scheduled')
      attention.Encounter_Reschedule_Request_Accepted = attention.Encounter_Reschedule_Request_Accepted.length > 0 ? attention.Encounter_Reschedule_Request_Accepted.sort(compareAsc) : []
      attention.Encounter_Reschedule_Request_Declined = encounterData.filter(ele => ele.Encounter_Reschedule_Status == 'Declined')
      attention.Encounter_Reschedule_Request_Declined = attention.Encounter_Reschedule_Request_Declined.length > 0 ? attention.Encounter_Reschedule_Request_Declined.sort(compareAsc) : []
    }
    let followupEncounter = await model.Encounter.getFollowup(req.user.PRO_UID)
    if (followupEncounter.length == 0) {
      attention.Followup_Cards = []
    } else {
      let count = 0
      let followupArray = []
      followupEncounter.map(ele => {
        if (ele.Encounter_Followup_Status != 'Cancelled') {
          if (!ele.Encounter_Time.includes(' ')) {
            if (ele.Encounter_Time.includes('a')) {
              ele.Encounter_Time = `${ele.Encounter_Time.split('a')[0]} am`
            } else {
              ele.Encounter_Time = `${ele.Encounter_Time.split('a')[0]} pm`
            }
          }
          followupArray.push({
            Encounter_UID: ele.Encounter_UID ? ele.Encounter_UID : null,
            Encounter_Chief_Complaint_PRO: ele.Encounter_Chief_Complaint_PRO ? ele.Encounter_Chief_Complaint_PRO : null,
            Encounter_Type: ele.Encounter_Type ? ele.Encounter_Type : null,
            Encounter_Location_Type: ele.Encounter_Location_Type ? ele.Encounter_Location_Type : null,
            Encounter_Time: ele.Encounter_Time ? ele.Encounter_Time : null,
            Encounter_Date: ele.Encounter_Date ? ele.Encounter_Date : null,
            Encounter_Start_Time: ele.Encounter_Start_Time ? ele.Encounter_Start_Time : null,
            Encounter_Start_Time_Zone: ele.Encounter_Start_Time_Zone ? ele.Encounter_Start_Time_Zone.toLowerCase() : null,
            Encounter_Status: ele.Encounter_Status ? ele.Encounter_Status : null,
            Encounter_Followup_Status: ele.Encounter_Followup_Status ? ele.Encounter_Followup_Status : null,
            Encounter_Provider: ele.Encounter_Provider ? ele.Encounter_Provider : null,
            Parent_Encounter: ele.Parent_Encounter ? ele.Parent_Encounter : null,
            Encounter_Has_Followup: ele.Encounter_Has_Followup ? ele.Encounter_Has_Followup : false
          })
        }
        count = count + 1
        if (count == followupEncounter.length) {
          followupArray.sort(compareAsc)
          attention.Followup_Cards = followupArray
        }
      })
    }

    let encounter = await model.Encounter.getEncounter(req.user.PRO_UID)
    let patientMedicalHistory = await updatedHistory(req, encounter, res)
    patientMedicalHistory = patientMedicalHistory ? patientMedicalHistory.sort(compareAsc) : []
    attention.Patient_Medical_History_Card = patientMedicalHistory
    res.send({
      code: 1,
      status: 'sucess',
      data: totalEncounter,
      attention: attention
    })
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS094' })
  }
})

// function sortOrders(a, b) {
//   return new Date(b.Created_At).getTime() - new Date(a.Created_At).getTime()
// }

let getChartData = async (chartData) => {
  let arr = []
  for (let index = 0; index < chartData.length; index++) {
    const ele = chartData[index]
    if (ele.Chart_is_Signed != true && ele.Encounter_Status != 'Abandoned' && Date.parse(moment(new Date(`${ele.Encounter_Date} ${ele.Encounter_Time}`)).format('YYYY-MM-DDTHH:mm:ss')) < Date.parse(momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm:ss'))) {
      if (!ele.Encounter_Time.includes(' ')) {
        if (ele.Encounter_Time.includes('a')) {
          ele.Encounter_Time = `${ele.Encounter_Time.split('a')[0]} am`
        } else {
          ele.Encounter_Time = `${ele.Encounter_Time.split('a')[0]} pm`
        }
      }
      if (ele.PT_Gender == '') {
        ele.PT_Gender = null
      }
      arr.push(ele)
      if (index == chartData.length - 1) {
        arr.sort(compareAsc)
        return arr
      }
    } else {
      if (index == chartData.length - 1) {
        arr.sort(compareAsc)
        return arr
      }
    }
  }
}
function compareDsc (a, b) {
  if (new Date(a.Encounter_Date + ' ' + a.Encounter_Time) < new Date(b.Encounter_Date + ' ' + b.Encounter_Time)) {
    return -1
  }
  if (new Date(a.Encounter_Date + ' ' + a.Encounter_Time) > new Date(b.Encounter_Date + ' ' + b.Encounter_Time)) {
    return 1
  }
  return 0
}

let createArray = async (arr) => {
  let count = 0
  let addendumData = []
  let dischargeData = []
  let d = new Date()
  var newYork = moment.tz(d, process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm:ss')
  let currentDate = moment(new Date(newYork)).add(330, 'm').toDate()
  for (var i = 0; i < arr.length; i++) {

    let encounterDate = moment(new Date(arr[i].Encounter_Date + ' ' + arr[i].Encounter_Time)).add(330, 'm').toDate()
    if(encounterDate < currentDate) {
      if (arr[i].PRO_Addendums && arr[i].PRO_Addendums.length != 0) {
        if (!arr[i].Encounter_Time.includes(' ')) {
          if (arr[i].Encounter_Time.includes('a')) {
            arr[i].Encounter_Time = `${arr[i].Encounter_Time.split('a')[0]} am`
          } else {
            arr[i].Encounter_Time = `${arr[i].Encounter_Time.split('a')[0]} pm`
          }
        }
        for (let j = 0; j < arr[i].PRO_Addendums.length; j++) {
          addendumData.push({
            Encounter_Time: arr[i].Encounter_Time ? arr[i].Encounter_Time : null,
            Encounter_Date: arr[i].Encounter_Date ? arr[i].Encounter_Date : null,
            Encounter_Type: arr[i].Encounter_Type ? arr[i].Encounter_Type : null,
            Encounter_Status: arr[i].Encounter_Status ? arr[i].Encounter_Status : null,
            Encounter_Chief_Complaint_PRO: arr[i].Encounter_Chief_Complaint_PRO ? arr[i].Encounter_Chief_Complaint_PRO : null,
            Encounter_UID: arr[i].Encounter_UID ? arr[i].Encounter_UID : null,
            PRO_UID: arr[i].PRO_UID ? arr[i].PRO_UID : null,
            Primary_DX: arr[i].Primary_DX ? arr[i].Primary_DX : null,
            Addendum_Is_Signed: arr[i].PRO_Addendums[j].Addendum_Is_Signed ? arr[i].PRO_Addendums[j].Addendum_Is_Signed : false,
            Addendum_Id: arr[i].PRO_Addendums[j]._id,
            PRO_Addendum_Attchment: arr[i].PRO_Addendums[j].PRO_Addendum_Attachment.length > 0 ? await serviceModule.getFileUrl(arr[i].PRO_Addendums[j].PRO_Addendum_Attachment) : [],
            PT_First_Name: arr[i].PT_First_Name ? arr[i].PT_First_Name : null,
            PT_Last_Name: arr[i].PT_Last_Name ? arr[i].PT_Last_Name : null,
            PT_Age_Now: arr[i].PT_Age_Now ? arr[i].PT_Age_Now : null,
            PT_Gender: (arr[i].PT_Gender || arr[i].PT_Gender == '') ? arr[i].PT_Gender : null,
            PT_DOB: arr[i].PT_DOB ? moment(new Date(arr[i].PT_DOB)).format('MM-DD-YYYY') : null,
            PRO_Addendum_Note: arr[i].PRO_Addendums[j].PRO_Addendum_Note ? arr[i].PRO_Addendums[j].PRO_Addendum_Note : null,
            ICD10_Code: arr[i].PRO_Addendums[j].ICD10_Code ? arr[i].PRO_Addendums[j].ICD10_Code : [],
            Discharge_Instructions_Status: arr[i].Discharge_Instructions_Status ? arr[i].Discharge_Instructions_Status : 'Draft',
            Chart_is_Signed: arr[i].Chart_is_Signed ? arr[i].Chart_is_Signed : false,
            PT_Profile_Picture: arr[i].PT_Profile_Picture ? await serviceModule.getImage(arr[i].PT_Profile_Picture) : null
          })
        }
      } else if (!arr[i].Chart_is_Signed && arr[i].Discharge_Instructions_Status == 'Draft') {
        let hours = Math.abs(Date.parse(momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm:ss')) - Date.parse(moment(new Date(`${arr[i].Encounter_Date} ${arr[i].Encounter_Time}`)).format('YYYY-MM-DDTHH:mm:ss'))) / 36e5
        if (hours >= 4) {
          dischargeData.push({
            Encounter_Time: arr[i].Encounter_Time ? arr[i].Encounter_Time : null,
            Encounter_Date: arr[i].Encounter_Date ? arr[i].Encounter_Date : null,
            Encounter_Type: arr[i].Encounter_Type ? arr[i].Encounter_Type : null,
            Encounter_Status: arr[i].Encounter_Status ? arr[i].Encounter_Status : null,
            Encounter_Chief_Complaint_PRO: arr[i].Encounter_Chief_Complaint_PRO ? arr[i].Encounter_Chief_Complaint_PRO : null,
            Encounter_UID: arr[i].Encounter_UID ? arr[i].Encounter_UID : null,
            PRO_UID: arr[i].PRO_UID ? arr[i].PRO_UID : null,
            Primary_DX: arr[i].Primary_DX ? arr[i].Primary_DX : null,
            PT_First_Name: arr[i].PT_First_Name ? arr[i].PT_First_Name : null,
            PT_Last_Name: arr[i].PT_Last_Name ? arr[i].PT_Last_Name : null,
            PT_Age_Now: arr[i].PT_Age_Now ? arr[i].PT_Age_Now : null,
            PT_Gender: (arr[i].PT_Gender || arr[i].PT_Gender == '') ? arr[i].PT_Gender : null,
            PT_DOB: arr[i].PT_DOB ? moment(new Date(arr[i].PT_DOB)).format('MM-DD-YYYY') : null,
            Discharge_Instructions_Status: arr[i].Discharge_Instructions_Status ? arr[i].Discharge_Instructions_Status : 'Draft',
            Chart_is_Signed: arr[i].Chart_is_Signed ? arr[i].Chart_is_Signed : false,
            PT_Profile_Picture: arr[i].PT_Profile_Picture ? await serviceModule.getImage(arr[i].PT_Profile_Picture) : null
          })
        }
      }
      count = count + 1
      if (count == arr.length) {
        addendumData.sort(compareAsc)
        dischargeData.sort(compareAsc)
        let data = {
          addendumData: addendumData,
          dischargeData: dischargeData
        }
        return data
      }
    } else {
      count = count + 1
      if (count == arr.length) {
        addendumData.sort(compareAsc)
        dischargeData.sort(compareAsc)
        let data = {
          addendumData: addendumData,
          dischargeData: dischargeData
        }
        return data
      }
    }

  }
}

let updatedHistory = async (req, encounterArray, res) => {
  let updatedEncounterArray = []
  for (let index = 0; index < encounterArray.length; index++) {
    const element = encounterArray[index]
    let Encounter_UID = element.Encounter_UID
    let PT_HS_MRN = element.PT_HS_MRN
    let patientHistory = await model.PatientHistory.findPatientHistoryById(PT_HS_MRN)
    let encounter = await model.Encounter.getEncounterByEncounter_UID(Encounter_UID)
    if (!encounter) return res.send({ code: 0, status: 'failed', message: `encounter with id '${Encounter_UID}' not found!`, Error_Code: errorModule.Encounter_not_found() })
    let encounterPatientHistory = encounter.PT_History
    // let diffrence = await getDiffrence(req, Encounter_UID, PT_HS_MRN)
    let diffrence = await serviceModule.getChanges(encounterPatientHistory, patientHistory, encounter.PRO_UID)
    if (diffrence) {
      if (!element.Encounter_Time.includes(' ')) {
        if (element.Encounter_Time.includes('a')) {
          element.Encounter_Time = `${element.Encounter_Time.split('a')[0]} am`
        } else {
          element.Encounter_Time = `${element.Encounter_Time.split('a')[0]} pm`
        }
      }
      let currentAge = await calculateAge(element.patients.PT_DOB)
      updatedEncounterArray.push({
        Encounter_UID: element.Encounter_UID,
        Encounter_Chief_Complaint_PRO: element.Encounter_Chief_Complaint_PRO,
        Encounter_Type: element.Encounter_Type,
        Encounter_Start_Time: element.Encounter_Start_Time ? element.Encounter_Start_Time : null,
        Encounter_Start_Time_Zone: element.Encounter_Start_Time_Zone ? element.Encounter_Start_Time_Zone.toLowerCase() : null,
        Encounter_Location_Type: element.Encounter_Location_Type,
        Encounter_Time: element.Encounter_Time,
        Encounter_Date: element.Encounter_Date,
        PT_First_Name: element.patients.PT_First_Name,
        PT_Last_Name: element.patients.PT_Last_Name,
        PT_Gender: element.patients.PT_Gender,
        PT_HS_MRN: element.patients.PT_HS_MRN,
        PT_Username: element.patients.PT_Username,
        PT_DOB: element.patients.PT_DOB,
        PT_Age_Now: currentAge
      })
      if (index == encounterArray.length - 1) {
        return updatedEncounterArray
      }
    } else {
      if (index == encounterArray.length - 1) {
        return updatedEncounterArray
      }
    }
  }
}

function compareAsc (a, b) {
  if (a.Encounter_Time != null && !a.Encounter_Time.includes(':')) {
    let time = a.Encounter_Time.split(' ')
    a.Encounter_Time = `${time[0]}:00 ${time[1]}`
  }
  if (b.Encounter_Time != null && !b.Encounter_Time.includes(':')) {
    let time = b.Encounter_Time.split(' ')
    b.Encounter_Time = `${time[0]}:00 ${time[1]}`
  }
  return new Date(b.Encounter_Date + ' ' + b.Encounter_Time).getTime() - new Date(a.Encounter_Date + ' ' + a.Encounter_Time).getTime()
}
async function calculateAge(birthday) { // birthday is a date
  var ageDifMs = Date.now() - birthday.getTime();
  var ageDate = new Date(ageDifMs); // miliseconds from epoch
  return Math.abs(ageDate.getUTCFullYear() - 1970);
}
module.exports = router
