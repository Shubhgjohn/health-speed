/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')

router.get('/view-encounter/:Encounter_UID', authorize([Role.Provider, Role.Patient]), async (req, res) => {
  var { params: { Encounter_UID } } = req
  if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required' })
})

module.exports = router
