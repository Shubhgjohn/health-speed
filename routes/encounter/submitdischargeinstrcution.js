/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const chartModule = require('../../models').Chart
const orderModule = require('../../models').Order
// const auditModule = require('../../models').Audit
const errorModule = require('../../models').Errors
const patientModule = require('../../models').Patient
const providerModule = require('../../models').Provider
const encounterModule = require('../../models').Encounter
const notificationModule = require('../../models').Notification
const moment = require('moment-timezone')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const logger = require('../../models/errorlog')
const fcm = require('../../_helpers/fcm')

router.put('/submit-discharge-instruction/:Encounter_UID', authorize(Role.Provider), async (req, res) => {
	var { params: { Encounter_UID } } = req
	if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required', Error_Code: errorModule.Encounter_UID_required() })
	try {
		let encounter = await encounterModule.getEncounterByEncounter_UID(Encounter_UID)
		if (!encounter) return res.send({ code: 0, status: 'failed', message: `No encounter found with ${Encounter_UID}`, Error_Code: errorModule.Encounter_not_found() })
		if (encounter.Encounter_Status == 'Scheduled') return res.send({ code: 4, status: 'failed', message: 'This encounter has not been started yet. So you can not submit the discharge instruction for this patient.' })
		if (encounter.Encounter_Status == 'Cancelled') return res.send({ code: 4, status: 'failed', message: 'This Encounter has been cancelled. So you can not submit the discharge instruction for this patient.' })
		let chartData = await chartModule.getChartData(Encounter_UID)
		if (chartData) {
			let patient = await patientModule.findOneByEmail(encounter.PT_Username)
			let provider = await providerModule.findById(encounter.PRO_UID)
			let PT_Token = await notificationModule.getToken('Patient', patient.PT_Username)
			let arr = []
			PT_Token.map(ele => arr.push(ele.Device_Token))
			let orderData = {
				Order_Status: 'Delivered',
				Order_Date_Delivered: new Date()
			}
			await orderModule.updateAllOrders(Encounter_UID, orderData)
				let data = {
				Discharge_Instructions_Status: 'Delivered'
			}
			let d = new Date()
			var newYork = moment.tz(d, process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm:ss')
			let updatedAt = req.headers.host.includes('localhost') ? moment(Date.parse(newYork)).add(330, 'm').toDate() : moment(Date.parse(newYork)).toDate()
			await chartModule.updateChart(Encounter_UID, data, updatedAt)
			if (chartData.Discharge_Instructions_Status == 'Draft') {
				if (arr.length != 0) {
					let payload = {
						registration_ids: arr,
						notification: {
							title: 'Visit Instructions Available',
							body: `Review what Dr. ${provider.PRO_Last_Name} has instructed for your recent visit.`
						},

						data: {
							Encounter_UID: Encounter_UID,
							PRO_UID: encounter.PRO_UID,
							title: 'Discharge Instructions'
						}
					}
					fcm.send(payload, function (err, response) {
						if (err) {} else console.log(response)
					})
				}
			} else {
				if (arr.length != 0 && chartData.Discharge_Instructions_Status == 'Delivered') {
					let payload = {
						registration_ids: arr,
						notification: {
							title: 'Visit Instructions Updated',
							body: 'Checkout the changes made to your visit instructions.'
						},

						data: {
							Encounter_UID: Encounter_UID,
							PRO_UID: encounter.PRO_UID,
							title: 'Discharge Instructions'
						}
					}
					fcm.send(payload, function (err, response) {
						if (err) {
						} else console.log(response)
					})
				}
			}
			// await createAudit(req);
			res.send({ code: 1, status: 'sucess', message: 'Discharge instruction submit successfully' })
		} else {
			let data = {
				Discharge_Instructions_Status: 'Delivered',
				PRO_UID: req.user.PRO_UID,
				PT_Username: encounter.PT_Username,
				PT_HS_MRN: encounter.PT_HS_MRN,
				Encounter_UID: Encounter_UID
			}
			await chartModule.create(data)
			res.send({ code: 1, status: 'sucess', message: 'Discharge instruction submit successfully' })
		}
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS099' })
	}
})


module.exports = router
