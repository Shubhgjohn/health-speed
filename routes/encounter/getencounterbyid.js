/* eslint-disable eqeqeq */
/* eslint-disable no-unneeded-ternary */
/* eslint-disable camelcase */
/* eslint-disable prefer-const */
const Router = require('express').Router
const router = new Router()
const model = require('../../models')
const errroModule = require('../../models').Errors
const serviceModule = require('../../models').CommonServices
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const logger = require('../../models/errorlog')
const moment = require('moment')
const _ = require('lodash')

// Get the encounter by id
router.get('/get-encounter/:id', authorize(Role.Provider), async (req, res) => {
	var { params: { id } } = req
	try {
		let patients_details = {}
		let encounter = await model.Encounter.findEncounter(id)
		if (encounter.length == 0) return res.send({ code: 0, status: 'failed', message: `No encounter found with ${id}`, Error_Code: errroModule.Encounter_not_found() })
		let EncounterClosed = encounter[0].Exam_Start_Time && encounter[0].Exam_End_Time ? true : false

		let followupEncounter = []
		let patient_history = encounter[0].PT_History
		encounter[0].patient_history = patient_history ? patient_history : null
		if (encounter.length) {
			let PT_Username = encounter[0].patient.PT_Username
			let PT_HS_MRN = encounter[0].patient.PT_HS_MRN
			let patientAllEncounter = await model.Encounter.getPatientsAllEncounter(PT_HS_MRN)
			let pastEncounters = []
			let upcomingEncounter = []

			let d = new Date()
			var newYork = moment.tz(d, process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm:ss')
			let currentDate = moment(new Date(newYork)).add(330, 'm').toDate()

			for (let index = 0; index < patientAllEncounter.length; index++) {
				const ele = patientAllEncounter[index]
				if (!ele.Encounter_Time.includes(':')) {
					ele.Encounter_Time = `${ele.Encounter_Time.split(' ')[0]}:00 ${ele.Encounter_Time.split(' ')[1]}`
				}
				if (!ele.Encounter_Time.includes(' ') && (ele.Encounter_Time.includes('p') || ele.Encounter_Time.includes('P'))) {
					ele.Encounter_Time = ele.Encounter_Time.includes('P') ? ele.Encounter_Time.split('P')[0] : ele.Encounter_Time.split('P')[0]
					ele.Encounter_Time = `${ele.Encounter_Time} pm`
				}
				if (!ele.Encounter_Time.includes(' ') && (ele.Encounter_Time.includes('a') || ele.Encounter_Time.includes('A'))) {
					ele.Encounter_Time = ele.Encounter_Time.includes('a') ? ele.Encounter_Time.split('a')[0] : ele.Encounter_Time.split('A')[0]
					ele.Encounter_Time = `${ele.Encounter_Time} am`
				}
				let encounterDate = moment(new Date(ele.Encounter_Date + ' ' + ele.Encounter_Time)).add(330, 'm').toDate()
				
				if (encounterDate < currentDate) {
					pastEncounters.push(ele)
				} else {
					upcomingEncounter.push(ele)
				}
			}
			pastEncounters = pastEncounters.filter(ele => { return ele.Encounter_UID != id })
			upcomingEncounter = upcomingEncounter.filter(ele => { return ele.Encounter_UID != id })

			pastEncounters.sort(compareAsc)
			upcomingEncounter.sort(compareDsc)
			pastEncounters = pastEncounters.length > 0 ? await updateEncounterJson(pastEncounters, req.user, encounter[0].patient, true) : []
			upcomingEncounter = upcomingEncounter.length > 0 ? await updateEncounterJson(upcomingEncounter, req.user, encounter[0].patient, false) : []

			if (encounter[0].Encounter_Has_Followup) {
				let followup = await model.Encounter.getParentEncounter(id)

				if (followup.length > 0) {
					followupEncounter = await updateEncounterJson(followup, req.user, encounter[0].patient)
					if (followupEncounter.length != 0) {
						followupEncounter[0].Encounter_Followup_Status = followup[0].Encounter_Followup_Status ? followup[0].Encounter_Followup_Status : null
						followupEncounter[0].Parent_Encounter = id
						followupEncounter[0].Followup_rules = true
						followupEncounter[0].Encounter_Has_Followup = followup[0].Encounter_Has_Followup
					} else {
						followupEncounter = []
					}
				}
			}
			var provider_info = {}

			provider_info.PRO_UID = encounter[0].provider.PRO_UID
			provider_info.PRO_First_Name = encounter[0].provider.PRO_First_Name ? encounter[0].provider.PRO_First_Name : null
			provider_info.PRO_Last_Name = encounter[0].provider.PRO_Last_Name ? encounter[0].provider.PRO_Last_Name : null
			provider_info.PRO_Home_City = encounter[0].provider.PRO_Home_City ? encounter[0].provider.PRO_Home_City : null
			provider_info.PRO_Home_State = encounter[0].provider.PRO_Home_State ? encounter[0].provider.PRO_Home_State : null
			provider_info.PRO_Profile_Picture = encounter[0].provider.PRO_Profile_Picture ? await serviceModule.getImage(encounter[0].provider.PRO_Profile_Picture) : null
			provider_info.PRO_Primary_Speciality = await encounter[0].provider.PRO_Speciality.find(function (item) {
				// eslint-disable-next-line no-return-assign
				return item.PRO_Primary_Speciality = true
			})

			provider_info.PRO_Primary_Speciality = provider_info.PRO_Primary_Speciality ? provider_info.PRO_Primary_Speciality.PRO_Speciality : null
			provider_info.PRO_Office_Address_Line_1 = encounter[0].provider.PRO_Office_Address_Line_1 ? encounter[0].provider.PRO_Office_Address_Line_1 : null
			provider_info.PRO_Office_Address_Line_2 = encounter[0].provider.PRO_Office_Address_Line_2 ? encounter[0].provider.PRO_Office_Address_Line_2 : null
			provider_info.PRO_Office_City = encounter[0].provider.PRO_Office_City ? encounter[0].provider.PRO_Office_City : null
			provider_info.PRO_Office_State = encounter[0].provider.PRO_Office_State ? encounter[0].provider.PRO_Office_State : null
			provider_info.PRO_Office_Zip = encounter[0].provider.PRO_Office_Zip ? encounter[0].provider.PRO_Office_Zip : null
			provider_info.PRO_Home_Zip = encounter[0].provider.PRO_Home_Zip ? encounter[0].provider.PRO_Home_Zip : null
			provider_info.PRO_Office_Phone = encounter[0].provider.PRO_Office_Phone ? encounter[0].provider.PRO_Office_Phone : null
			provider_info.PRO_Home_Phone = encounter[0].provider.PRO_Home_Phone ? encounter[0].provider.PRO_Home_Phone : null
			provider_info.PRO_Profile_Status = encounter[0].provider.PRO_Profile_Status ? encounter[0].provider.PRO_Profile_Status : null
			provider_info.PRO_Home_Rate_Status = encounter[0].provider.PRO_Home_Rate_Status ? encounter[0].provider.PRO_Home_Rate_Status : null
			provider_info.PRO_Office_Rate_Status = encounter[0].provider.PRO_Office_Rate_Status ? encounter[0].provider.PRO_Office_Rate_Status : null
			provider_info.PRO_Billing_Status = encounter[0].provider.PRO_Billing_Status ? encounter[0].provider.PRO_Billing_Status : null
			var patientInfo = encounter.map(ele => ({
				PT_HS_MRN: ele.PT_HS_MRN,
				PT_Username: ele.patient.PT_Username ? ele.patient.PT_Username : null,
				PT_First_Name: ele.patient.PT_First_Name ? ele.patient.PT_First_Name : null,
				PT_Last_Name: ele.patient.PT_Last_Name ? ele.patient.PT_Last_Name : null,
				PT_Age_Now: ele.patient.PT_DOB ? new Date().getFullYear() - ele.patient.PT_DOB.getFullYear() : null,
				PT_Gender: ele.patient.PT_Gender ? ele.patient.PT_Gender : null,
				PT_Home_City: ele.patient.PT_Home_City ? ele.patient.PT_Home_City : null,
				PT_Home_State: ele.patient.PT_Home_State ? ele.patient.PT_Home_State : null,
				PT_Home_Zip: ele.patient.PT_Home_Zip ? ele.patient.PT_Home_Zip : null,
				PT_Home_Address_Line_1: ele.patient.PT_Home_Address_Line_1 ? ele.patient.PT_Home_Address_Line_1 : null,
				PT_Home_Address_Line_2: ele.patient.PT_Home_Address_Line_2 ? ele.patient.PT_Home_Address_Line_2 : null,
				PT_Home_Address_Latitude: ele.patient.PT_Home_Address_Latitude ? ele.patient.PT_Home_Address_Latitude : null,
				PT_Home_Address_Longitude: ele.patient.PT_Home_Address_Longitude ? ele.patient.PT_Home_Address_Longitude : null,
				PT_Cell_Phone: ele.patient.PT_Cell_Phone ? ele.patient.PT_Cell_Phone : null,
				PT_DOB: ele.patient.PT_DOB ? moment(ele.patient.PT_DOB).format('MM-DD-YYYY') : null,
				PT_Medical_Directive: ele.patient_history ? ele.patient_history.PT_Medical_Directive : null,
				PT_Organ_Donor: ele.patient_history ? ele.patient_history.PT_Organ_Donor : 'No',
				PT_Is_Minor: ele.patient.PT_Is_Minor ? ele.patient.PT_Is_Minor : 'No',
				PT_Height: ele.patient.PT_Height ? ele.patient.PT_Height : 0,
				PT_Weight: ele.patient.PT_Weight ? ele.patient.PT_Weight : 0,
				PT_BMI: ele.patient.PT_BMI ? ele.patient.PT_BMI : 0,
				PT_Blood_Glucose: ele.patient.PT_Blood_Glucose ? ele.patient.PT_Blood_Glucose : 0,
				PT_O2_Sat: ele.patient.PT_O2_Sat ? ele.patient.PT_O2_Sat : 0,
				PT_Temp: ele.patient.PT_Temp ? ele.patient.PT_Temp : 0,
				PT_Heart_Rt: ele.patient.PT_Heart_Rt ? ele.patient.PT_Heart_Rt : 0,
				PT_Resp: ele.patient.PT_Resp ? ele.patient.PT_Resp : 0,
				PT_Blood_Pressure: ele.patient.PT_Blood_Pressure ? ele.patient.PT_Blood_Pressure : 0,
				PT_Systolic_BP: ele.patient.PT_Systolic_BP ? ele.patient.PT_Systolic_BP : 0,
				PT_Diastolic_BP: ele.patient.PT_Diastolic_BP ? ele.patient.PT_Diastolic_BP : 0,
				PT_Primary_Care_Provider: ele.patient_history ? ele.patient_history.PT_Primary_Care_Provider : null,
				PT_Allergies: ele.patient_history ? ele.patient_history.PT_Allergies : []
			}))
			patientInfo[0].PT_Profile_Picture = encounter[0].patient.PT_Profile_Picture ? await serviceModule.getImage(encounter[0].patient.PT_Profile_Picture) : null
			patients_details.patient_info = patientInfo
			if (patientInfo[0].PT_Is_Minor == 'Yes') {
				let guardian = await model.Patient.getPatientByHSId(encounter[0].patient.PT_Guardian_Name)
				patientInfo[0].PT_Guardian_Name = `${guardian[0].PT_First_Name} ${guardian[0].PT_Last_Name}`
			} else {
				patientInfo[0].PT_Guardian_Name = null
			}

			patients_details.past_visits = _.uniqBy(pastEncounters, 'Encounter_UID')
			patients_details.upcoming_visit = _.uniqBy(upcomingEncounter, 'Encounter_UID')
			patients_details.followup_visit = followupEncounter

			let encounterInfo = encounter.map(ele => ({
				Encounter_Chief_Complaint_PRO: ele.Encounter_Chief_Complaint_PRO ? ele.Encounter_Chief_Complaint_PRO : null,
				Encounter_Chief_Complaint_PT: ele.Encounter_Chief_Complaint_PT ? ele.Encounter_Chief_Complaint_PT : null,
				Encounter_UID: ele.Encounter_UID ? ele.Encounter_UID : null,
				Encounter_PT_Chief_Complaint_More: ele.Encounter_PT_Chief_Complaint_More ? ele.Encounter_PT_Chief_Complaint_More : null,
				Encounter_Type: ele.Encounter_Type ? ele.Encounter_Type : null,
				Encounter_Location_Type: ele.Encounter_Location_Type ? ele.Encounter_Location_Type : null,
				Encounter_Time: ele.Encounter_Time ? ele.Encounter_Time : null,
				Encounter_End_Time: ele.Encounter_End_Time ? `${ele.Encounter_End_Time} ${ele.Encounter_End_Time_Zone}` : null,
				Encounter_Date: ele.Encounter_Date ? ele.Encounter_Date : null,
				Encounter_Day: ele.Encounter_Date ? moment(new Date(ele.Encounter_Date)).format('ddd') : null,
				Encounter_Address_1: ele.Encounter_Location_Type == 'Office' ? ele.provider.PRO_Office_Address_Line_1 : ele.patient.PT_Home_Address_Line_1,
				Encounter_Address_2: ele.Encounter_Location_Type == 'Office' ? ele.provider.PRO_Office_Address_Line_2 : ele.patient.PT_Home_Address_Line_2,
				Encounter_Address_Latitude: ele.Encounter_Location_Type == 'Office' ? ele.provider.PRO_Office_Address_Latitude : ele.patient.PT_Home_Address_Latitude,
				Encounter_Address_Longitute: ele.Encounter_Location_Type == 'Office' ? ele.provider.PRO_Office_Address_Longitude : ele.patient.PT_Home_Address_Longitude,
				Encounter_Cost: ele.Encounter_Cost ? ele.Encounter_Cost : null,
				Encounter_Status: ele.Encounter_Status ? ele.Encounter_Status : null,
				Encounter_Has_Followup: ele.Encounter_Has_Followup ? ele.Encounter_Has_Followup : false,
				Encounter_Transaction_Status: ele.Encounter_Transaction_Status ? ele.Encounter_Transaction_Status : null,
				Encounter_Reschedule_Status: ele.Encounter_Reschedule_Status ? ele.Encounter_Reschedule_Status : null,
				Reschedule_Request_Date: ele.Reschedule_Request_Date ? ele.Reschedule_Request_Date : null,
				Reschedule_Request_Time: ele.Reschedule_Request_Time ? ele.Reschedule_Request_Time : null,
				Primary_DX: ele.Primary_DX ? ele.Primary_DX : null,
				Encounter_Followup_Limit: ele.Encounter_Followup_Limit ? ele.Encounter_Followup_Limit : null,
				Exam_Start_Time: ele.Exam_Start_Time ? ele.Exam_Start_Time : ele.Encounter_Time,
				Exam_End_Time: ele.Exam_End_Time ? ele.Exam_End_Time : `${ele.Encounter_End_Time} ${ele.Encounter_End_Time_Zone}`
			}))
			if (encounterInfo[0].Encounter_Time != null && !encounterInfo[0].Encounter_Time.includes(':')) {
				let time = encounterInfo[0].Encounter_Time.split(' ')
				encounterInfo[0].Encounter_Time = `${time[0]}:00 ${time[1]}`
			}
			if (encounterInfo[0].Encounter_End_Time != null && !encounterInfo[0].Encounter_End_Time.includes(':')) {
				let time = encounterInfo[0].Encounter_End_Time.split(' ')
				encounterInfo[0].Encounter_End_Time = `${time[0]}:00 ${time[1]}`
			}
			if (encounterInfo[0].Encounter_End_Time.match(/[a-z]/i)) {
				if (encounterInfo[0].Encounter_End_Time.includes('a')) {
					encounterInfo[0].Encounter_End_Time = `${encounterInfo[0].Encounter_End_Time.split('a')[0]}am`
				}
				if (encounterInfo[0].Encounter_End_Time.includes('p')) {
					encounterInfo[0].Encounter_End_Time = `${encounterInfo[0].Encounter_End_Time.split('p')[0]}pm`
				}
			}
			let chart = {}
			let chartData = await model.Chart.findEncounterChart(encounter[0].Encounter_UID, encounter[0].PRO_UID)
			if (!chartData) {
				let data = {
					Encounter_UID: encounter[0].Encounter_UID,
					PRO_UID: req.user.PRO_UID,
					PT_HS_MRN: patientInfo[0].PT_HS_MRN,
					PT_Username: patientInfo[0].PT_Username
				}
				chartData = await model.Chart.create(data)
			}
			chartData = JSON.parse(JSON.stringify(chartData))
			let allEncounterOrder = await model.Order.findEncounterOrders(encounter[0].Encounter_UID, req.user.PRO_UID, PT_Username)
			if (chartData && chartData.Discharge_Instructions_Status == 'Delivered') {
				chart.PRO_Diagnosis = chartData.PRO_Diagnosis
				chart.PRO_Diagnosis_ICD_Code = chartData.PRO_Diagnosis_ICD_Code
				chart.PRO_Patient_Instructions = chartData.PRO_Patient_Instructions
				chart.orders = allEncounterOrder.length > 0 ? await getOrders(allEncounterOrder) : []
			} else {
				chart = null
			}
			var Open_Chart = chartData
			if (Open_Chart) {
				if (allEncounterOrder.length == 0) {
					Open_Chart.Orders = []
				} else {
					Open_Chart.Orders = await getOrders(allEncounterOrder)
					Open_Chart.followup_visit = followupEncounter
				}
			}

			if (Open_Chart && Open_Chart.PRO_Addendums.length > 0) {
				Open_Chart.PRO_Addendums = await addendumAttachmentURL(Open_Chart.PRO_Addendums)
			}
			if (Open_Chart && Open_Chart.PRO_HPI_Attchment.length > 0) {
				Open_Chart.PRO_HPI_Attchment = await serviceModule.getFileUrl(Open_Chart.PRO_HPI_Attchment)
			}
			if (Open_Chart && Open_Chart.PRO_Review_of_Systems_Attchment.length > 0) {
				Open_Chart.PRO_Review_of_Systems_Attchment = await serviceModule.getFileUrl(Open_Chart.PRO_Review_of_Systems_Attchment)
			}
			if (Open_Chart && Open_Chart.PRO_Exam_Notes_Attchment.length > 0) {
				Open_Chart.PRO_Exam_Notes_Attchment = await serviceModule.getFileUrl(Open_Chart.PRO_Exam_Notes_Attchment)
			}
			if (Open_Chart && Open_Chart.PRO_Diagnosis_ICD_Code.length > 0 && !Open_Chart.PRO_Diagnosis_ICD_Code.includes(null)) {
				Open_Chart.PRO_Diagnosis_ICD_Code = Open_Chart.PRO_Diagnosis_ICD_Code.filter(ele => {
					return ele.Is_Deleted != true
				})
			}
			if (Open_Chart && Open_Chart.PRO_Results_Note_Attachment.length > 0) {
				Open_Chart.PRO_Results_Note_Attachment = await serviceModule.getFileUrl(Open_Chart.PRO_Results_Note_Attachment)
			}
			if (Open_Chart && Open_Chart.PRO_Diagnosis.length > 0) {
				// eslint-disable-next-line no-self-assign
				Open_Chart.PRO_Diagnosis = Open_Chart.PRO_Diagnosis
			}
			if (Open_Chart && Open_Chart.PRO_Plan_Attchment.length > 0) {
				Open_Chart.PRO_Plan_Attchment = await serviceModule.getFileUrl(Open_Chart.PRO_Plan_Attchment)
			}
			if (Open_Chart && Open_Chart.PRO_Diagnosis_Attchment.length > 0) {
				Open_Chart.PRO_Diagnosis_Attchment = await serviceModule.getFileUrl(Open_Chart.PRO_Diagnosis_Attchment)
			}

			if (Open_Chart) {
				Open_Chart.Vital_Signs_Last_Modified_Timestamp = Open_Chart && Open_Chart.Vital_Signs_Last_Modified_Timestamp != null ? moment(new Date(Open_Chart.Vital_Signs_Last_Modified_Timestamp)).format('MM-DD-YYYY hh:mm A') : null
				patients_details.patient_info[0].Vital_Signs_Last_Modified_Timestamp = Open_Chart && Open_Chart.Vital_Signs_Last_Modified_Timestamp != null ? moment(new Date(Open_Chart.Vital_Signs_Last_Modified_Timestamp)).format('MM-DD-YYYY hh:mm A') : null
			} else {
				patients_details.patient_info[0].Vital_Signs_Last_Modified_Timestamp = null
			}
			let patient_history = encounter[0].patient_history
			if (patient_history) {
				patient_history = JSON.parse(JSON.stringify(patient_history))
				patient_history.PT_Tobacco_Usage = patient_history.PT_Tobacco_Usage == null ? 'No' : patient_history.PT_Tobacco_Usage
				let medicalHistoryArray = []
				medicalHistoryArray.push(patient_history.PT_Heart_Disease, patient_history.PT_Diabetes, patient_history.PT_Cancer,
					patient_history.PT_Immune_Diseases, patient_history.PT_Surgery, patient_history.PT_Neuro_Issues, patient_history.PT_Other_Diseases)
				if (medicalHistoryArray.length > 0) {
					if (medicalHistoryArray.includes('Yes')) {
						patient_history.PT_Medical_History_Toggle = 'Yes'
					} else {
						patient_history.PT_Medical_History_Toggle = 'No'
					}
				} else {
					patient_history.PT_Medical_History_Toggle = 'No'
				}
				let socaialHistoryArray = []
				socaialHistoryArray.push(patient_history.PT_Tobacco_Usage,
					patient_history.PT_Recreational_Drugs, patient_history.PT_Alcohol_Usage)
				if (socaialHistoryArray.length > 0) {
					if (socaialHistoryArray.includes('Yes')) {
						patient_history.PT_Social_History_Toggle = 'Yes'
					} else {
						patient_history.PT_Social_History_Toggle = 'No'
					}
				} else {
					patient_history.PT_Social_History_Toggle = 'No'
				}

				if (patient_history.PT_Medication_Attachment.length > 0) {
					patient_history.PT_Medication_Attachment = await serviceModule.getFileUrl(patient_history.PT_Medication_Attachment)
				}
				if (patient_history.PT_Medical_History_Attachment.length > 0) {
					patient_history.PT_Medical_History_Attachment = await serviceModule.getFileUrl(patient_history.PT_Medical_History_Attachment)
				}
				if (patient_history.PT_Medical_Directive_Attachment.length > 0) {
					patient_history.PT_Medical_Directive_Attachment = await serviceModule.getFileUrl(patient_history.PT_Medical_Directive_Attachment)
				}
				if (patient_history.Expected_Delivery_Date == '' || patient_history.Expected_Delivery_Date == null) {
					patient_history.Pregnant_Now = 'No'
				}
			} else {
				let json = {
					PT_HS_MRN: patients_details.patient_info[0].PT_HS_MRN,
					PT_Username: patients_details.patient_info[0].PT_Username
				}
				let PatientHistory = await model.PatientHistory.findPatientHistoryById(json.PT_HS_MRN)
				patient_history = PatientHistory ? PatientHistory : await model.PatientHistory.create(json)
				await model.Encounter.updatePatientMedicalHistory(req.params.id, patient_history)
				patient_history.PT_Medical_History_Toggle = 'No'
				patient_history.PT_Social_History_Toggle = 'No'
			}
			// await createAudit(req);

			let chatMessage = await model.Message.getEncounterMessages(id)
			let message = []
			if (chatMessage.length > 0) {
				message = chatMessage.map(ele => ({
					_id: ele._id,
					body: ele.body,
					sentBy: ele.sentBy,
					Message_Date: moment(ele.Created_At).tz(process.env.CURRENT_TIMEZONE).format('MM-DD-YYYY'),
					Message_Time: moment(ele.Created_At).tz(process.env.CURRENT_TIMEZONE).format('hh:mm a')
				}))
			}
			let settingData = await model.Other.find()
			patient_history.PRO_UID = provider_info.PRO_UID
			patient_history.PT_Username = patients_details.patient_info[0].PT_Username
			if (Open_Chart) {
				Open_Chart.Updated_At = Open_Chart ? moment(Open_Chart.Updated_At).format('MM-DD-YYYY hh:mm a') : null
				Open_Chart.Created_At = Open_Chart ? moment(Open_Chart.Created_At).format('MM-DD-YYYY hh:mm a') : null
			}
			let encounterTime = moment(new Date(encounterInfo[0].Encounter_Date + ' ' + encounterInfo[0].Encounter_Time)).add(330, 'm').toDate()
			res.send({
				code: 1,
				status: 'sucess',
				PRO_Account_Status: req.user.PRO_Account_Status,
				PRO_Private_Note: Open_Chart.PRO_Private_Note ? Open_Chart.PRO_Private_Note : null,
				AP_Phone_Number: settingData[0].HS_Support_Number,
				Encounter_Started: currentDate.getTime() > encounterTime.getTime() ? true : false,
				Encounter_closed: EncounterClosed,
				Provider_Details: provider_info,
				Patients_Details: patients_details,
				visit_info: encounterInfo,
				messages: message,
				Discharge_Instruction: chart,
				Chart_is_Signed: chartData ? chartData.Chart_is_Signed : false,
				Patient_History: patient_history,
				Open_Chart: Open_Chart
			})
		} else {
			// await createAudit(req);
			res.send({ code: 0, status: 'failed', message: `No encounter found for ${id}`, Error_Code: errroModule.Encounter_not_found() })
		}
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS090' })
	}
})


let getOrders = (encounters) => {
	let orders = encounters.map(ele => ({
		Order_UID: ele.Order_UID ? ele.Order_UID : null,
		Order_Complete: ele.Order_Complete ? ele.Order_Complete : null,
		Order_Incomplete: ele.Order_Incomplete ? ele.Order_Incomplete : null,
		Order_Text: ele.Order_Text ? ele.Order_Text : null,
		Order_As_Addendum: ele.Order_As_Addendum ? ele.Order_As_Addendum : false,
		Reminder_Start_Date: ele.Reminder_Start_Date ? moment(new Date(ele.Reminder_Start_Date)).format('MM-DD-YYYY') : null,
		Reminder_Date: ele.Reminder_Date ? moment(new Date(ele.Reminder_Date)).format('MM-DD-YYYY') : null,
		Order_Reminder: ele.Order_Reminder ? ele.Order_Reminder : null,
		Order_Type: ele.Order_Type ? ele.Order_Type : null,
		Order_Created_Date: ele.Order_Created_Date ? moment(new Date(ele.Order_Created_Date)).format('MM-DD-YYYY') : null,
		Order_Created_Time: ele.Order_Created_Time ? ele.Order_Created_Time : null,
		Order_Status: ele.Order_Status ? ele.Order_Status : null,
		Order_Stat: ele.Order_Stat ? ele.Order_Stat : 'Unchecked',
		Order_ICD10_Code: ele.Order_ICD10_Code ? ele.Order_ICD10_Code : []
	}))
	return orders
}

function compareAsc (a, b) {
	if (a.Encounter_Time != null && !a.Encounter_Time.includes(':')) {
		let time = a.Encounter_Time.split(' ')
		a.Encounter_Time = `${time[0]}:00 ${time[1]}`
	}
	if (b.Encounter_Time != null && !b.Encounter_Time.includes(':')) {
		let time = b.Encounter_Time.split(' ')
		b.Encounter_Time = `${time[0]}:00 ${time[1]}`
	}
	if (a.Encounter_Time != null && !a.Encounter_Time.includes(' ')) {
		if (a.Encounter_Time.includes('a')) {
			a.Encounter_Time = `${a.Encounter_Time.split('a')[0]} am`
		}
		if (a.Encounter_Time.includes('p')) {
			a.Encounter_Time = `${a.Encounter_Time.split('p')[0]} pm`
		}
	}
	if (b.Encounter_Time != null && !b.Encounter_Time.includes(' ')) {
		if (b.Encounter_Time.includes('a')) {
			b.Encounter_Time = `${b.Encounter_Time.split('a')[0]} am`
		}
		if (b.Encounter_Time.includes('p')) {
			b.Encounter_Time = `${b.Encounter_Time.split('p')[0]} pm`
		}
	}
	return new Date(b.Encounter_Date + ' ' + b.Encounter_Time).getTime() - new Date(a.Encounter_Date + ' ' + a.Encounter_Time).getTime()
}

function compareDsc (a, b) {
	if (new Date(a.Encounter_Date + ' ' + a.Encounter_Time) < new Date(b.Encounter_Date + ' ' + b.Encounter_Time)) {
		return -1
	}
	if (new Date(a.Encounter_Date + ' ' + a.Encounter_Time) > new Date(b.Encounter_Date + ' ' + b.Encounter_Time)) {
		return 1
	}
	return 0
}

let updateEncounterJson = async (encounters, Provider, patient, status) => {
	return new Promise(function (resolve, reject) {
		let dataArray = []
		let count = 0
		encounters.map(async ele => {
			if (ele.Encounter_Status == 'Requested' || ele.Encounter_Status == 'Scheduled' || ele.Encounter_Status == 'Active') {
				if (ele.Encounter_Time != null && !ele.Encounter_Time.includes(':')) {
					let time = ele.Encounter_Time.split(' ')
					ele.Encounter_Time = `${time[0]}:00 ${time[1]}`
				}
				if (ele.Encounter_End_Time != null && !ele.Encounter_End_Time.includes(':')) {
					let time = ele.Encounter_End_Time.split(' ')
					ele.Encounter_End_Time = `${time[0]}:00 ${time[1]}`
				}
				if (ele.Encounter_End_Time.match(/[a-z]/i)) {
					if (ele.Encounter_End_Time.includes('a')) {
						ele.Encounter_End_Time = ele.Encounter_End_Time.split('a')[0]
						ele.Encounter_End_Time_Zone = 'am'
					}
					if (ele.Encounter_End_Time.includes('p')) {
						ele.Encounter_End_Time = ele.Encounter_End_Time.split('p')[0]
						ele.Encounter_End_Time_Zone = 'pm'
					}
				}
				dataArray.push({
					Encounter_UID: ele.Encounter_UID,
					Encounter_Chief_Complaint_PRO: ele.Encounter_Chief_Complaint_PRO,
					Encounter_Type: ele.Encounter_Type,
					Encounter_Location_Type: ele.Encounter_Location_Type,
					Encounter_Time: ele.Encounter_Time,
					Encounter_Started: (status == true || status == false) ? status : null,
					Encounter_Date: ele.Encounter_Date,
					Encounter_End_Time: `${ele.Encounter_End_Time} ${ele.Encounter_End_Time_Zone}`,
					Encounter_Status: ele.Encounter_Status,
					Encounter_Provider: ele.PRO_UID == Provider.PRO_UID ? 'You' : ele.Encounter_Provider,
					Encounter_Followup_Status: ele.Encounter_Followup_Status ? ele.Encounter_Followup_Status : null,
					PRO_UID: ele.PRO_UID ? ele.PRO_UID : null,
					PRO_Profile_Picture: ele.PRO_Profile_Picture != null ? await serviceModule.getImage(ele.PRO_Profile_Picture) : null,
					PRO_Home_Address_Latitude: Provider.PRO_Home_Address_Latitude ? Provider.PRO_Home_Address_Latitude : null,
					PRO_Home_Address_Longitude: Provider.PRO_Home_Address_Longitude ? Provider.PRO_Home_Address_Longitude : null,
					PRO_Office_Address_Latitude: Provider.PRO_Office_Address_Latitude ? Provider.PRO_Office_Address_Latitude : null,
					PRO_Office_Address_Longitude: Provider.PRO_Office_Address_Longitude ? Provider.PRO_Office_Address_Longitude : null,
					Encounter_Cancelled_By: ele.Encounter_Status == 'Cancelled' ? ele.Encounter_Cancelled_By == 'Patient' ? 'Patient'
						: ele.Encounter_Cancelled_User == Provider.PRO_UID ? 'You' : 'Provider' : null,
					PT_First_Name: patient.PT_First_Name,
					PT_Last_Name: patient.PT_Last_Name,
					PT_Gender: patient.PT_Gender,
					PT_Age_Now: patient.PT_DOB ? new Date().getFullYear() - patient.PT_DOB.getFullYear() : null,
					Primary_DX: ele.Primary_DX ? ele.Primary_DX : null,
					Chart_is_Signed: ele.Chart_is_Signed,
					Addendum_Is_Signed: ele.Addendum_Is_Signed,
					Encounter_Number_of_Attachments: ele.Encounter_Number_of_Attachments,
					Encounter_Access: ele.PRO_UID == Provider.PRO_UID ? true : (ele.Encounter_Access_Provider && ele.Encounter_Access_Provider.length > 0) ? ele.Encounter_Access_Provider.find(ele => (ele.PRO_UID == Provider.PRO_UID && ele.Access_Status == 'Granted')) ? true : false : false,
					Encounter_Access_Status: ele.PRO_UID == Provider.PRO_UID ? 'Granted' : (ele.Encounter_Access_Provider && ele.Encounter_Access_Provider.length > 0) ? ele.Encounter_Access_Provider.find(ele => (ele.PRO_UID == Provider.PRO_UID)) ? ele.Encounter_Access_Provider.find(ele => (ele.PRO_UID == Provider.PRO_UID && ele.Access_Status == 'Granted')) ? 'Granted' : 'Requested' : 'Request' : 'Request'
				})
				count = count + 1
				if (count == encounters.length) {
					resolve(dataArray)
				}
			} else {
				count = count + 1
				if (count == encounters.length) {
					resolve(dataArray)
				}
			}
		})
	})
}

let addendumAttachmentURL = async (PRO_Addendums) => {
	return new Promise((resolve, reject) => {
		let count = 0
		let dataArray = []
		PRO_Addendums.map(async ele => {
			let updated_date = moment(ele.Updated_At).format('MM-DD-YYYY')
			let updated_time = moment(ele.Updated_At).format('h:mm a')
			dataArray.push({
				PRO_Addendum_Note: ele.PRO_Addendum_Note,
				ICD10_Code: ele.ICD10_Code,
				Addendum_Is_Signed: ele.Addendum_Is_Signed,
				Is_Deleted: ele.Is_Deleted,
				_id: ele._id,
				updated_date : updated_date,
				updated_time : updated_time,
				Updated_At: ele.Updated_At,
				PRO_Addendum_Attachment: ele.PRO_Addendum_Attachment.length > 0 ? await serviceModule.getFileUrl(ele.PRO_Addendum_Attachment) : []
			})
			count = count + 1
			if (count == PRO_Addendums.length) resolve(dataArray)
		})
	})
}

module.exports = router
