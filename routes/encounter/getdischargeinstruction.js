/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const moment = require('moment')
const chartModule = require('../../models').Chart
const orderModule = require('../../models').Order
const errorModule = require('../../models').Errors
const encounterModule = require('../../models').Encounter
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const logger = require('../../models/errorlog')


// route for get discharge instruction data
router.post('/get-discharge-instruction', authorize(Role.Provider), async (req, res) => {
  var { body: { Encounter_UID, PT_Username } } = req
  let chart = {}
  if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required', Error_Code: errorModule.Encounter_UID_required() })
  let encounter = await encounterModule.getEncounterByEncounter_UID(Encounter_UID)
  if (!encounter) return res.send({ code: 0, status: 'failed', message: `encounter with id '${Encounter_UID}' not found!`, Error_Code: errorModule.Encounter_not_found() })
  if (!PT_Username) return res.send({ code: 0, status: 'failed', message: 'PT_Username is required', Error_Code: errorModule.PT_Username_required() })

  try {

    //Get the chart data
    let chartData = await chartModule.findEncounterChart(Encounter_UID, req.user.PRO_UID, PT_Username)
    if (chartData) {
      chart.diagnosis = chartData.PRO_Diagnosis
      chart.recommendations = chartData.PRO_Patient_Instructions
    } else {
      chart.diagnosis = []
      chart.recommendations = ''
    }
    let allEncounterOrder = await orderModule.findEncounterOrders(Encounter_UID, req.user.PRO_UID, PT_Username)
    let orders = allEncounterOrder.map(ele => ({
      Order_UID: ele.Order_UID,
      Order_Complete: ele.Order_Complete,
      Order_Incomplete: ele.Order_Incomplete,
      Order_Text: ele.Order_Text,
      Reminder_Start_Date: moment(new Date(ele.Reminder_Start_Date)).format('MM-DD-YYYY'),
      Reminder_Date: moment(new Date(ele.Reminder_Date)).format('MM-DD-YYYY'),
      Order_Reminder: ele.Order_Reminder,
      Order_Type: ele.Order_Type,
      Order_Created_Date: moment(new Date(ele.Order_Created_Date)).format('MM-DD-YYYY'),
      Order_Status: ele.Order_Status
    }))
    chart.orders = orders.length > 0 ? orders : []

    res.send({ code: 1, status: 'sucess', data: chart })
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS089' })
  }
})

module.exports = router
