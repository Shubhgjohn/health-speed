/* eslint-disable eqeqeq */
/* eslint-disable no-unneeded-ternary */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const moment = require('moment')
const momentTz = require('moment-timezone')
const model = require('../../models')
const availabilityCheck = require('../../service/checkProviderAvailability')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const { encounterUID, auditUid } = require('../../utils/UID')
const auditType = require('../../auditType.json')
var isAvailable = true
const fcm = require('../../_helpers/fcm')
const logger = require('../../models/errorlog')
var AWS = require('aws-sdk')
// const { date } = require('joi')
AWS.config.update({
	accessKeyId: process.env.AWS_ACCESS_KEY,
	secretAccessKey: process.env.AWS_SECRET_KEY,
	region: 'us-east-1'
})
var s3 = new AWS.S3()

// route for create encounter followup
router.post('/create-followup', authorize([Role.Provider, Role.Patient]), async (req, res) => {
	try {
		var { body: { Encounter_Location_Type, Parent_Encounter, Encounter_Date, Encounter_Cost, Encounter_Start_Time, Encounter_Start_Time_Zone } } = req
		if (!Encounter_Location_Type) return res.send({ code: 0, status: 'failed', message: 'Encounter_Location_Type is required', Error_Code: model.Errors.Encounter_Location_Type_required() })
		if (!Parent_Encounter) return res.send({ code: 0, status: 'failed', message: 'Parent_Encounter is required', Error_Code: 'HS074' })

		// find the parent encounter data
		let parentEncounter = await model.Encounter.getEncounterByEncounter_UID(Parent_Encounter)
		if (!parentEncounter) {
			return res.send({ code: 0, status: 'failed', message: `There are no encounter with ${Parent_Encounter}`, Error_Code: model.Errors.Encounter_not_found() })
		}
		if (Date.parse(moment(new Date(`${parentEncounter.Encounter_Date} ${parentEncounter.Encounter_Time}`)).format('YYYY-MM-DDTHH:mm')) > Date.parse(momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm'))) return res.send({ code: 0, status: 'failed', message: 'Followup request can\'t be requested since this Encounter has not been started.' })
		var diffDays = parseInt((Date.parse(momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm')) - Date.parse(moment(new Date(`${parentEncounter.Encounter_Date}`)).format('YYYY-MM-DDTHH:mm'))) / (1000 * 60 * 60 * 24), 10)
		if (diffDays > parentEncounter.Encounter_Followup_Limit) return res.send({ code: 0, status: 'failed', message: 'This visit is too old to accept followups. Please schedule a new encounter.', Error_Code: 'HS401' })
		if (!Encounter_Date) return res.send({ code: 0, status: 'failed', message: 'Encounter_Date is required', Error_Code: model.Errors.Encounter_Date_required() })

		if (!Encounter_Cost) return res.send({ code: 0, status: 'failed', message: 'Encounter_Cost is required', Error_Code: model.Errors.Encounter_Cost_required() })
		if (!Encounter_Start_Time) return res.send({ code: 0, status: 'failed', message: 'Encounter_Start_Time is required', Error_Code: model.Errors.Encounter_Start_Time_required() })
		if (!Encounter_Start_Time.includes(':') && Encounter_Start_Time.match(/[a-z]/i)) return res.send({ code: 0, status: 'failed', message: 'Encounter_Start_Time format is not correct' })

		if (!Encounter_Start_Time_Zone) return res.send({ code: 0, status: 'failed', message: 'Encounter_Start_Time_Zone is required', Error_Code: model.Errors.Encounter_Start_Time_Zone_required() })
		if (Encounter_Start_Time_Zone != 'am' && Encounter_Start_Time_Zone != 'pm') return res.send({ code: 0, status: 'failed', message: 'Encounter_Start_Time_Zone can contain only am/pm' })

		req.body.Encounter_Chief_Complaint_PRO = req.body.Encounter_Chief_Complaint_PRO || parentEncounter.Encounter_Chief_Complaint_PRO
		req.body.Encounter_PT_Chief_Complaint_More = req.body.Encounter_PT_Chief_Complaint_More || parentEncounter.Encounter_PT_Chief_Complaint_More

		if (req.body.Encounter_Chief_Complaint_PRO) {
			req.body.Encounter_Chief_Complaint_PT = req.body.Encounter_Chief_Complaint_PRO
		}
		if (!Encounter_Start_Time.includes(':')) {
			req.body.Encounter_Start_Time = `${Encounter_Start_Time}:00`
		}
		let E_Time = moment(new Date(`${Encounter_Date}` + ' ' + `${req.body.Encounter_Start_Time + ' ' + Encounter_Start_Time_Zone}`)).add(1, 'h').format('hh:mm a')

		req.body.Encounter_End_Time = E_Time.split(' ')[0][0] == '0' ? E_Time.split(' ')[0].slice(1) : E_Time.split(' ')[0]
		req.body.Encounter_End_Time_Zone = E_Time.split(' ')[1]
		if (parentEncounter.Encounter_Status == 'Requested' || parentEncounter.Encounter_Status == 'Cancelled') {
			return res.send({ code: 0, status: 'failed', message: 'You cat not create followup due to parent encounter may be Requested or Cancelled', Error_Code: 'HS076' })
		}
		if (parentEncounter && parentEncounter.Encounter_Has_Followup) {
			return res.send({ code: 0, status: 'failed', message: 'This encounter has already followup', Error_Code: 'HS077' })
		}
		if (parentEncounter.Encounter_Type == 'Followup Visit') {
			return res.send({ code: 0, status: 'failed', message: 'This encounter is already a followup of another encounter', Error_Code: 'HS078' })
		}
		// req.body.Encounter_Start_Time = !Encounter_Start_Time.includes(':') ? Encounter_Start_Time + ':00' : Encounter_Start_Time
		req.body.Encounter_Start_Time = req.body.Encounter_Start_Time.charAt(0) == '0' ? req.body.Encounter_Start_Time.slice(1) : req.body.Encounter_Start_Time
		req.body.Encounter_End_Time = req.body.Encounter_End_Time.charAt(0) == '0' ? req.body.Encounter_End_Time.slice(1) : req.body.Encounter_End_Time
		let difference = await diff_hours(new Date(`${Encounter_Date} ${req.body.Encounter_Start_Time} ${Encounter_Start_Time_Zone}`), new Date(`${Encounter_Date} ${req.body.Encounter_End_Time} ${req.body.Encounter_End_Time_Zone}`))
		if (difference != 1) return res.send({ code: 0, status: 'failed', message: 'There somthing is wrong in your Encounter Start and End time.', Error_Code: 'HS402' })
		req.body.Encounter_Time = req.body.Encounter_Start_Time + ' ' + Encounter_Start_Time_Zone

		// block for calculate encounter cost is user is provider
		if (req.userType == 'Provider') {
			let regularHour = req.user.Pro_Office_Hours_Time
			let day = moment(new Date(Encounter_Date)).format('ddd')
			if (day == 'Thu') day = day + 'r'
			if (day == 'Tue') day = day + 's'
			let netDate = regularHour.find(ele => ele.day == day)
			let fromTime = netDate.fromTime.time.includes(':') ? netDate.fromTime.time : netDate.fromTime.time + ':00'
			let toTime = netDate.toTime.time.includes(':') ? netDate.toTime.time : netDate.toTime.time + ':00'
			let regularTime = ((Date.parse(moment(new Date(`${Encounter_Date} ${fromTime} ${netDate.fromTime.zone}`)).format('YYYY-MM-DDTHH:mm:ss')) <= Date.parse(moment(new Date(`${Encounter_Date} ${req.body.Encounter_Time}`)).format('YYYY-MM-DDTHH:mm:ss'))) && (Date.parse(moment(new Date(`${Encounter_Date} ${req.body.Encounter_Time}`)).format('YYYY-MM-DDTHH:mm:ss')) <= Date.parse(moment(new Date(`${Encounter_Date} ${toTime} ${netDate.toTime.zone}`)).format('YYYY-MM-DDTHH:mm:ss')))) ? true : false
			let specialRate = req.user.PRO_Special_Rate.filter(item => item.PRO_Special_Rate_Date == moment(new Date(Encounter_Date)).format('M/D/YY'))
			if (regularTime) {
				if (Encounter_Location_Type == 'Office' || Encounter_Location_Type == 'office') {
					req.body.Encounter_Cost = specialRate.length > 0 ? specialRate[0].PRO_Office_Followup_Special_Hourly_Rate ? specialRate[0].PRO_Office_Followup_Special_Hourly_Rate : req.user.PRO_Office_Office_Hours_Followup_Rate : req.user.PRO_Office_Office_Hours_Followup_Rate
				} else {
					req.body.Encounter_Cost = specialRate.length > 0 ? specialRate[0].PRO_Home_Followup_Special_Hourly_Rate ? specialRate[0].PRO_Home_Followup_Special_Hourly_Rate : req.user.PRO_Home_Office_Hours_Followup_Rate : req.user.PRO_Home_Office_Hours_Followup_Rate
				}
			} else {
				if (Encounter_Location_Type == 'Office' || Encounter_Location_Type == 'office') {
					req.body.Encounter_Cost = specialRate.length > 0 ? specialRate[0].PRO_Office_Followup_Special_Off_Hourly_Rate ? specialRate[0].PRO_Office_Followup_Special_Off_Hourly_Rate : req.user.PRO_Office_Off_Hours_Followup_Rate : req.user.PRO_Office_Off_Hours_Followup_Rate
				} else {
					req.body.Encounter_Cost = specialRate.length > 0 ? specialRate[0].PRO_Home_Followup_Special_Off_Hourly_Rate ? specialRate[0].PRO_Home_Followup_Special_Off_Hourly_Rate : req.user.PRO_Home_Off_Hours_Followup_Rate : req.user.PRO_Home_Off_Hours_Followup_Rate
				}
			}
		}

		// block for return error is requested date and time is in past
		if (Encounter_Date == momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('MM-DD-YYYY')) {
			if (Date.parse(moment(new Date(`${Encounter_Date} ${Encounter_Start_Time} ${Encounter_Start_Time_Zone}`)).format('YYYY-MM-DDTHH:mm:ss')) < Date.parse(momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm:ss'))) {
				return res.send({
					code: 0,
					status: 'failed',
					message: 'You can\'t set encounter for past time',
					Error_Code: model.Errors.can_not_set_past_time()
				})
			}
		}

		// Get the patient details from patient email
		let patient = await model.Patient.findOneByEmail(parentEncounter.PT_Username)
		if (req.userType == 'Patient' && patient.PT_Account_Status == 'Suspended') return res.send({ code: 0, status: 'failed', message: 'Please contact Healthspeed.', Error_Code: model.Errors.patient_can_not_accept() })
		let provider = await model.Provider.findById(parentEncounter.PRO_UID)
		if (req.userType == 'Patient' && provider.PRO_Account_Status != 'Active') return res.send({ code: 0, status: 'failed', message: 'Please contact Healthspeed.', Error_Code: model.Errors.provider_can_not_accept() })
		if (req.userType == 'Provider' && provider.PRO_Account_Status == 'Suspended') return res.send({ code: 0, status: 'failed', message: 'Please contact Healthspeed.', Error_Code: model.Errors.provider_can_not_accept() })
		if (req.userType == 'Provider' && patient.PT_Account_Status != 'Active') return res.send({ code: 0, status: 'failed', message: 'Please contact Healthspeed.', Error_Code: model.Errors.patient_can_not_accept() })
		
		req.body.Encounter_Travel_Time = provider.PRO_Home_Visit_Buffer
		req.body.Encounter_Type = 'Followup Visit'
		req.body.Encounter_Provider = `${provider.PRO_First_Name ? provider.PRO_First_Name : ''} ${provider.PRO_Last_Name ? provider.PRO_Last_Name : ''}`
		req.body.Encounter_Transaction_Status = 'Unpaid'
		req.body.Encounter_Status = req.userType == 'Provider' ? 'Requested' : 'Scheduled'
		req.body.Encounter_UID = await encounterUID()
		req.body.PRO_UID = provider.PRO_UID
		req.body.PT_Username = parentEncounter.PT_Username
		req.body.Payment_Card_Id = req.body.Payment_Card_Id ? req.body.Payment_Card_Id : parentEncounter.Payment_Card_Id

		var travelTimeSlot = provider.PRO_Home_Visit_Buffer / 60
		var sTime, eTime

		// check availability for date
		const availability = await model.Availability.getProviderAvailability(provider.PRO_UID, Encounter_Date)
		const providerAvailability = await model.ProviderAvailability.getProviderAvailability(req.body.PRO_UID, Encounter_Date)

		// if availability is set for given date
		if (availability) {
			var Start_Time = moment(`${Encounter_Start_Time} ${Encounter_Start_Time_Zone}`, 'hh:mm A').format('HH.mm')
			var End_Time = moment(`${req.body.Encounter_End_Time} ${req.body.Encounter_End_Time_Zone}`, 'hh:mm A').format('HH.mm')
			Start_Time = Start_Time.includes(30) ? parseFloat(Start_Time) + 0.20 : parseFloat(Start_Time)
			End_Time = End_Time.includes(30) ? parseFloat(End_Time) + 0.20 : parseFloat(End_Time)

			if (Encounter_Location_Type == 'Home') {
				sTime = Start_Time - travelTimeSlot
				eTime = End_Time + travelTimeSlot
			} else {
				sTime = Start_Time
				eTime = End_Time
			}
			if (End_Time < Start_Time) return res.send({ code: 0, status: 'failed', message: 'Encounter_End_Time cannot less than Encounter_Start_Time', Error_Code: model.Errors.Encounter_End_Time_less_than_Encounter_Start_Time() })
			var arr = availability.PRO_Slots.filter(ele => ((ele.Slot_Format >= sTime) && (ele.Slot_Format < eTime)))
			var filterProviderAvailability = providerAvailability.PRO_Slots.filter(ele => ((ele.Slot_Format >= sTime) && (ele.Slot_Format < eTime)))
			let data = await availabilityCheck.checkProviderAvailability(filterProviderAvailability, Encounter_Location_Type, isAvailable)
			isAvailable = data == 'Available' ? true : false
			await checkAvailability(arr, travelTimeSlot * 2)
			if (isAvailable) {
				var newarr = availability.PRO_Slots.map(element => ({
					Is_Home_Available: ((element.Slot_Format == sTime || (element.Slot_Format >= sTime && element.Slot_Format < eTime))) ? false : element.Is_Home_Available,
					Is_Office_Available: (element.Slot_Format == sTime || (element.Slot_Format >= sTime && element.Slot_Format < eTime)) ? false : element.Is_Office_Available,
					Is_Travel_Time: (element.Slot_Format >= sTime && element.Slot_Format < Start_Time) || (element.Slot_Format >= End_Time && element.Slot_Format < eTime) ? true : element.Is_Travel_Time,
					_id: element._id,
					Slot_Time: element.Slot_Time,
					Slot_Time_Zone: element.Slot_Time_Zone,
					Slot_Format: element.Slot_Format
				}))
				var newProviderAvailabilityArray = providerAvailability.PRO_Slots.map(element => ({
					Is_Home_Available: ((element.Slot_Format == sTime || (element.Slot_Format >= sTime && element.Slot_Format < eTime))) ? false : element.Is_Home_Available,
					Is_Office_Available: (element.Slot_Format == sTime || (element.Slot_Format >= sTime && element.Slot_Format < eTime)) ? false : element.Is_Office_Available,
					_id: element._id,
					Slot_Time: element.Slot_Time,
					Slot_Time_Zone: element.Slot_Time_Zone,
					Slot_Format: element.Slot_Format,
					Is_Home_Availability: element.Is_Home_Availability,
					Is_Office_Availability: element.Is_Office_Availability
				}))
				// let patient = await model.Patient.findOneByEmail(PT_Username)
				// if (patient) {
				req.body.PT_HS_MRN = parentEncounter.PT_HS_MRN
				let patientHistory = await model.PatientHistory.findByUserId(parentEncounter.PT_HS_MRN)
				req.body.PT_History = patientHistory
				let patientData = await model.Patient.findById(parentEncounter.PT_HS_MRN)
				let followupEncounter = await model.Encounter.create(req.body)
				let encounterData = {
					PT_Profile_Picture: patientData.PT_Profile_Picture ? await model.CommonServices.getImage(patientData.PT_Profile_Picture) : null,
					PT_First_Name: patientData.PT_First_Name,
					PT_Last_Name: patientData.PT_Last_Name,
					PT_HS_MRN: patientData.PT_HS_MRN,
					Encounter_UID: followupEncounter.Encounter_UID,
					Encounter_Type: followupEncounter.Encounter_Type,
					Encounter_Location_Type: followupEncounter.Encounter_Location_Type,
					Encounter_Date: followupEncounter.Encounter_Date,
					Encounter_Time: followupEncounter.Encounter_Time,
					Encounter_Status: followupEncounter.Encounter_Status,
					Encounter_Reschedule_Status: followupEncounter.Encounter_Reschedule_Status,
					Cancellation_Reason: followupEncounter.Cancellation_Reason,
					Encounter_Chief_Complaint_PRO: followupEncounter.Encounter_Chief_Complaint_PRO,
					PRO_First_Name: provider.PRO_First_Name,
					PRO_Last_Name: provider.PRO_Last_Name,
					PRO_Profile_Picture: provider.PRO_Profile_Picture ? await model.CommonServices.getImage(provider.PRO_Profile_Picture) : null,
					PRO_Gender: provider.PRO_Gender,
					PRO_UID: provider.PRO_UID,
					PRO_Home_Address_Latitude: provider.PRO_Home_Address_Latitude,
					PRO_Home_Address_Longitude: provider.PRO_Home_Address_Longitude,
					PRO_Office_Address_Latitude: provider.PRO_Office_Address_Latitude,
					PRO_Office_Address_Longitude: provider.PRO_Office_Address_Longitude
				}
				await model.Availability.updateAvailabilityByTime(provider.PRO_UID, Encounter_Date, newarr)
				await model.ProviderAvailability.updateAvailability(req.body.PRO_UID, Encounter_Date, newProviderAvailabilityArray)
				let data = {
					Encounter_Followup_Status: req.userType == 'Provider' ? 'Requested' : 'Scheduled',
					Encounter_Has_Followup: true,
					Followup_Created_By: req.userType,
					Followup_Requested_Time: new Date()
				}
				await model.Encounter.updateEncounter(Parent_Encounter, data)
				if (req.userType == 'Patient') {
					await createChart(followupEncounter)
					let jsonData = {
						Encounter_UID: followupEncounter.Encounter_UID,
						PRO_UID: followupEncounter.PRO_UID,
						title: 'Followup Scheduled'
					}
					let PRO_Token = await model.Notification.getToken('Provider', provider.PRO_Username)
					let arr = []
					PRO_Token.map(ele => arr.push(ele.Device_Token))
					let title = `Followup Scheduled: ${moment(new Date(followupEncounter.Encounter_Date)).format('M/D')}`
					let body = `A followup has been scheduled with ${patient.PT_First_Name} ${patient.PT_Last_Name.charAt(0)} and added to your calendar.`
					await sendNotification(arr, title, body, jsonData)
					let PT_Token = await model.Notification.getToken('Patient', req.user.PT_Username)
					arr = []
					PT_Token.map(ele => arr.push(ele.Device_Token))
					title = 'Followup Scheduled'
					body = `Get ready for your followup visit with Dr. ${provider.PRO_Last_Name}`
					await sendNotification(arr, title, body, jsonData)
				}
				if (req.userType == 'Provider') {
					let jsonData = await NotificationData(followupEncounter, patient, provider)
					let token = await model.Notification.getToken('Patient', parentEncounter.PT_Username)
					let arr = []
					token.map(ele => arr.push(ele.Device_Token))
					if (arr.length != 0) {
						var payload = { // this may vary according to the message type (single recipient, multicast, topic, et cetera)
							registration_ids: arr,
							notification: {
								title: 'Followup Requested',
								body: 'A followup visit has been requested, time to review it.'
							},

							data: jsonData
						}
						await fcm.send(payload, function (err, response) {
							if (err) {
							} else console.log(response)
						})
					}
				}
				await createAudit(req, provider)
				isAvailable = true
				res.send({
					code: 1,
					status: 'sucess',
					message: 'encounter created sucessfull',
					Encounter_UID: followupEncounter.Encounter_UID,
					data: encounterData
				})

			} else {
				isAvailable = true
				res.send({ code: 3, status: 'failed', message: 'Apologies, This timeslot is no longer available', Error_Code: model.Errors.You_can_not_set_encounter() })
			}
		} else {
			isAvailable = true
			res.send({ code: 0, status: 'failed', message: `No availability set for ${Encounter_Date} of Dr. ${provider.PRO_Last_Name}`, Error_Code: model.Errors.No_availability_available() })
		}
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS083' })
	}
})

async function createChart (encounter) {
	let data = {
		PT_HS_MRN: encounter.PT_HS_MRN,
		PRO_Chart_Access: { PRO_UID: encounter.PRO_UID },
		PRO_UID: encounter.PRO_UID,
		Encounter_UID: encounter.Encounter_UID
	}
	await model.Chart.create(data)
}

async function sendNotification (arr, title, body, jsonData) {
	if (arr.length != 0) {
		var payload = { // this may vary according to the message type (single recipient, multicast, topic, et cetera)
			registration_ids: arr,
			notification: {
				title: title,
				body: body
			},

			data: jsonData
		}
		await fcm.send(payload, function (err, response) {
			if (err) {
			} else console.log(response)
		})
	}
}

var checkAvailability = async (availabilityArray, ignoreIndex) => {
	var count = 0
	availabilityArray.map(ele => {
		if ((ele.Is_Home_Available == true && ele.Is_Office_Available == true) && !ele.Is_Travel_Time) {
			count = count + 1
			if (count == availabilityArray.length) {
				return true
			}
		} else {
			isAvailable = false
			count = count + 1
			if (count == availabilityArray.length) {
				return true
			}
		}
	})
}

async function createAudit (req, provider) {
	var auditData = {
		Audit_UID: await auditUid(),
		Audit_Info: 'New- ' + await auditUid(),
		Audit_Type: auditType.add,
		Data_Point: 'EN/create followup',
		New_Value: req.body,
		IP_Address: await model.Audit.getIpAddress(req),
		PRO_First_Name: provider.PRO_First_Name,
		PRO_Last_Name: provider.PRO_Last_Name,
		PRO_UID: provider.PRO_UID
	}
	await model.Audit.create(auditData)
}

async function NotificationData (encounter, patient, provider) {
	let encounterPatient = await model.Patient.findById(encounter.PT_HS_MRN)
	let day = moment(new Date(encounter.Encounter_Date)).format('ddd')
	if (day == 'Thu') day = day + 'r'
	if (day == 'Tue') day = day + 's'
	let netDate = provider.Pro_Office_Hours_Time.find(ele => ele.day == day)
	let fromTime = netDate.fromTime.time.includes(':') ? netDate.fromTime.time : netDate.fromTime.time + ':00'
	let toTime = netDate.toTime.time.includes(':') ? netDate.toTime.time : netDate.toTime.time + ':00'
	let Pro_Office_Hours_Time = ((Date.parse(moment(new Date(`${encounter.Encounter_Date} ${fromTime} ${netDate.fromTime.zone}`)).format('YYYY-MM-DDTHH:mm:ss')) <= Date.parse(moment(new Date(`${encounter.Encounter_Date} ${encounter.Encounter_Time}`)).format('YYYY-MM-DDTHH:mm:ss'))) && (Date.parse(moment(new Date(`${encounter.Encounter_Date} ${encounter.Encounter_Time}`)).format('YYYY-MM-DDTHH:mm:ss')) <= Date.parse(moment(new Date(`${encounter.Encounter_Date} ${toTime} ${netDate.toTime.zone}`)).format('YYYY-MM-DDTHH:mm:ss')))) ? true : false
	let card = null
	try {
		card = await model.Stripe.getCardByCardId(patient.stripeCustomerId, encounter.Payment_Card_Id)
	} catch (err) {
		card = null
	}
	let patientChart = await model.Chart.findChartByPT_HS_MRN(encounter.PT_HS_MRN)
	let shareChart = []
	patientChart.map(item => {
		if (item.PRO_Chart_Access.length == 0) {
			shareChart.push(item)
		} else {
			if (!item.PRO_Chart_Access.find(find => (find.PRO_UID == provider.PRO_UID) || (find.PRO_UID == provider.PRO_UID && find.Access_Status == true))) shareChart.push(item)
		}
	})
	let jsonData = {
		Encounter_UID: encounter.Encounter_UID,
		Encounter_Location_Type: encounter.Encounter_Location_Type,
		Encounter_Date: encounter.Encounter_Date,
		Encounter_Time: encounter.Encounter_Time,
		Encounter_Start_Time: encounter.Encounter_Start_Time,
		Encounter_End_Time: encounter.Encounter_End_Time,
		Encounter_Start_Time_Zone: encounter.Encounter_Start_Time_Zone,
		Encounter_End_Time_Zone: encounter.Encounter_End_Time_Zone,
		Encounter_Cost: encounter.Encounter_Cost.toString(),
		Encounter_Chief_Complaint_PT: encounter.Encounter_Chief_Complaint_PT,
		PRO_Profile_Picture: provider.PRO_Profile_Picture ? await getImageUrl(provider.PRO_Profile_Picture) : '',
		PRO_Last_Name: provider.PRO_Last_Name,
		PRO_UID: provider.PRO_UID,
		PRO_Office_Address_Line_1: provider.PRO_Office_Address_Line_1 ? provider.PRO_Office_Address_Line_1 : '',
		PRO_Office_Address_Line_2: provider.PRO_Office_Address_Line_2 ? provider.PRO_Office_Address_Line_2 : '',
		PRO_Office_City: provider.PRO_Office_City ? provider.PRO_Office_City : '',
		PRO_Office_State: provider.PRO_Office_State ? provider.PRO_Office_State : '',
		PRO_Office_Zip: provider.PRO_Office_Zip ? provider.PRO_Office_Zip.toString() : '',
		Pro_Office_Hours_Time: Pro_Office_Hours_Time.toString(),
		PT_Profile_Picture: encounterPatient.PT_Profile_Picture ? await getImageUrl(encounterPatient.PT_Profile_Picture) : '',
		PT_Home_Address_Line_1: patient.PT_Home_Address_Line_1 ? patient.PT_Home_Address_Line_1 : '',
		PT_Home_Address_Line_2: patient.PT_Home_Address_Line_2 ? patient.PT_Home_Address_Line_2 : '',
		PT_Home_City: patient.PT_Home_City ? patient.PT_Home_City : '',
		PT_Home_State: patient.PT_Home_State ? patient.PT_Home_State : '',
		PT_Home_Zip: patient.PT_Home_Zip ? patient.PT_Home_Zip.toString() : '',
		PT_Credit_Card_Type: card ? card.brand : '',
		PT_Credit_Card_Short_Number: card ? card.last4.toString() : '',
		Share_All_Chart: shareChart.length > 0 ? 'false' : 'true',
		title: 'Followup Request'
	}
	return jsonData
}

function getImageUrl (imageKey) {
	if (imageKey == null || imageKey == '') {
		return ''
	} else {
		return new Promise((resolve, reject) => {
			const url = s3.getSignedUrl('getObject', {
				Bucket: process.env.AWS_BUCKET,
				Key: imageKey,
				Expires: 60 * 60 * 24 * 15
			})
			resolve(url)
		})
	}
}
module.exports = router

function diff_hours (dt2, dt1) {
	var diff = (dt2.getTime() - dt1.getTime()) / 1000
	diff /= (60 * 60)
	return Math.abs(diff)
}
