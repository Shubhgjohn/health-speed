/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
// const auditModule = require('../../models').Audit
const errorModule = require('../../models').Errors
const patientModule = require('../../models').Patient
const providerModule = require('../../models').Provider
const encounterModule = require('../../models').Encounter
const serviceModule = require('../../models').CommonServices
const notificationModule = require('../../models').Notification
const chartModule = require('../../models').Chart
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const logger = require('../../models/errorlog')
const fcm = require('../../_helpers/fcm')

router.put('/update-encounter-access/:Encounter_UID', authorize(Role.Provider), async (req, res) => {
	var { body: { data }, params: { Encounter_UID } } = req
	if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required', Error_Code: errorModule.Encounter_UID_required() })
	if (!data) return res.send({ code: 0, status: 'failed', message: 'data is required', Error_Code: errorModule.data_required() })
	let provider = await providerModule.findById(data.Encounter_Access_Provider.PRO_UID)
	if (!provider) return res.send({ code: 0, status: 'failed', message: `provider with UID '${data.Encounter_Access_Provider.PRO_UID}' not found!`, Error_Code: errorModule.provider_not_found() })
	try {
		let encounter = await encounterModule.getEncounterByEncounter_UID(Encounter_UID)
		if (encounter) {
			let patient = await patientModule.findOneByEmail(encounter.PT_Username)
			if (encounter.Encounter_Access_Provider) {
				var access = encounter.Encounter_Access_Provider.find(ele => (ele.PRO_UID == data.Encounter_Access_Provider.PRO_UID))
				if (access) {
					return res.send({ code: 0, status: 'failed', message: 'You already request for this request', Error_Code: 'HS110' })
				} else {
					let encounterPatient = await patientModule.findById(encounter.PT_HS_MRN)
					let encounterProvider = await providerModule.findById(encounter.PRO_UID)
					let jsonData = {
						PRO_Profile_Picture: provider.PRO_Profile_Picture ? await serviceModule.getImage(provider.PRO_Profile_Picture) : '',
						PT_Profile_Picture: encounterPatient.PT_Profile_Picture ? await serviceModule.getImage(encounterPatient.PT_Profile_Picture) : '',
						Encounter_Date: encounter.Encounter_Date,
						Encounter_Time: encounter.Encounter_Time,
						Encounter_UID: Encounter_UID,
						PT_HS_MRN: encounterPatient.PT_HS_MRN,
						Encounter_Chief_Complaint_PT: encounter.Encounter_Chief_Complaint_PT ? encounter.Encounter_Chief_Complaint_PT : '',
						Encounter_Chief_Complaint_PRO: encounter.Encounter_Chief_Complaint_PRO ? encounter.Encounter_Chief_Complaint_PRO : null,
						requestedBy: req.user.PRO_Last_Name,
						requestedBy_UID: req.user.PRO_UID,
						PRO_UID: encounterProvider.PRO_UID,
						PRO_Last_Name: encounterProvider.PRO_Last_Name,
						title: 'Encounter Access'
					}
					data.Encounter_Access_Provider.Access_Status = 'Requested'
					await encounterModule.updateEncounterAcesss(Encounter_UID, data.Encounter_Access_Provider)
					await chartModule.UpdatePRO_Access_Chart(Encounter_UID, { PRO_UID: data.Encounter_Access_Provider.PRO_UID, Access_Status: false })
					let token = await notificationModule.getToken('Patient', patient.PT_Username)
					let arr = []
					token.map(ele => arr.push(ele.Device_Token))
					if (arr.length != 0) {
						var payload = { // this may vary according to the message type (single recipient, multicast, topic, et cetera)
							registration_ids: arr,
							notification: {
								title: 'Access Requested For Your Encounter',
								body: `Dr. ${req.user.PRO_First_Name} ${req.user.PRO_Last_Name} is requesting access to an encounter. Ready to give them access?`
							},

							data: jsonData
						}
						fcm.send(payload, function (err, response) {
							if (err) {
							} else {
								console.log(response)
							}
						})
					}
					// await createAudit(req);
					res.send({ code: 1, status: 'sucess', message: 'We have sent a request to the patient.' })
				}
			} else {
				data.Encounter_Access_Provider.Access_Status = 'Requested'
				await encounterModule.updateEncounterAcesss(Encounter_UID, data.Encounter_Access_Provider)
				// await createAudit(req);
				res.send({ code: 1, status: 'sucess', message: 'Encounter update successfully' })
			}
		} else {
			// await createAudit(req);
			res.send({ code: 0, status: 'failed', message: `No encounter find with ${Encounter_UID}`, Error_Code: errorModule.Encounter_not_found() })
		}
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS111' })
	}
})


module.exports = router
