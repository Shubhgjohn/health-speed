/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const moment = require('moment')
const model = require('../../models')
const errorModule = require('../../models').Errors
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const { encounterUID, auditUid } = require('../../utils/UID')
const auditType = require('../../auditType.json')
const logger = require('../../models/errorlog')
var isAvailable = true

// route for create encounter
router.post('/create_encounter', authorize(Role.Provider), async (req, res) => {
  var {
    body: {
      Encounter_Location_Type, Encounter_Type, Encounter_Date, Encounter_Chief_Complaint_PT, Encounter_Chief_Complaint_PRO,
      Encounter_Cost, Encounter_Start_Time, Encounter_End_Time, Encounter_Start_Time_Zone, Encounter_End_Time_Zone, PT_Username
    }
  } = req
  if (!Encounter_Location_Type) return res.send({ code: 0, status: 'failed', message: 'Encounter_Location_Type is required', Error_Code: errorModule.Encounter_UID_required() })
  if (!Encounter_Type) return res.send({ code: 0, status: 'failed', message: 'Encounter_Type is required', Error_Code: errorModule.Encounter_Type_required() })
  if (!Encounter_Date) return res.send({ code: 0, status: 'failed', message: 'Encounter_Date is required', Error_Code: errorModule.Encounter_Date_required() })
  if (!Encounter_Chief_Complaint_PT) return res.send({ code: 0, status: 'failed', message: 'Encounter_Chief_Complaint_PT is required', Error_Code: errorModule.Encounter_Chief_Complaint_PT_required() })
  if (!Encounter_Chief_Complaint_PRO) return res.send({ code: 0, status: 'failed', message: 'Encounter_Chief_Complaint_PRO is required', Error_Code: errorModule.Encounter_Chief_Complaint_PRO_required() })
  if (!Encounter_Cost) return res.send({ code: 0, status: 'failed', message: 'Encounter_Cost is required', Error_Code: errorModule.Encounter_Cost_required() })
  if (!Encounter_Start_Time) return res.send({ code: 0, status: 'failed', message: 'Encounter_Start_Time is required', Error_Code: errorModule.Encounter_Start_Time_required() })
  if (!Encounter_End_Time) return res.send({ code: 0, status: 'failed', message: 'Encounter_End_Time is required', Error_Code: errorModule.Encounter_End_Time_required() })
  if (!Encounter_Start_Time_Zone) return res.send({ code: 0, status: 'failed', message: 'Encounter_Start_Time_Zone is required', Error_Code: errorModule.Encounter_Start_Time_Zone_required() })
  if (!Encounter_End_Time_Zone) return res.send({ code: 0, status: 'failed', message: 'Encounter_End_Time_Zone is required', Error_Code: errorModule.Encounter_End_Time_Zone_required() })
  if (!PT_Username) return res.send({ code: 0, status: 'failed', message: 'PT_Username is required', Error_Code: errorModule.PT_Username_required() })
  req.body.Encounter_Time = Encounter_Start_Time + ' ' + Encounter_Start_Time_Zone
  req.body.Encounter_Provider = `${req.user.PRO_First_Name ? req.user.PRO_First_Name : ''} ${req.user.PRO_Last_Name ? req.user.PRO_Last_Name : ''}`
  req.body.Encounter_Transaction_Status = 'Unpaid'
  req.body.Encounter_Status = 'Requested'
  req.body.Primary_DX = `${req.user.PRO_First_Name ? req.user.PRO_First_Name : ''} ${req.user.PRO_Last_Name ? req.user.PRO_Last_Name : ''}`
  req.body.Encounter_UID = await encounterUID()
  req.body.PRO_UID = req.user.PRO_UID
  var travelTimeSlot = req.user.PRO_Home_Visit_Buffer / 60
  var sTime, eTime
  try {
    if (Encounter_Date == moment(new Date()).format('MM-DD-YYYY')) {
      let time = moment(new Date()).format('H.mm')
      time = parseInt(time.split('.')[1]) > 30 ? `${parseInt(time.split('.')[0])}.5` : parseInt(time.split('.')[0])
      let encounterTime = moment(`${Encounter_Start_Time} ${Encounter_Start_Time_Zone}`, 'hh:mm A').format('HH.mm')
      encounterTime = parseInt(encounterTime.split('.')[1]) > 30 ? `${parseInt(encounterTime.split('.')[0])}.5` : parseInt(encounterTime.split('.')[0])
      if (encounterTime < time) {
        return res.send({
          code: 0,
          status: 'failed',
          message: 'You can\'t set encounter for past time'
        })
      }
    }
    const availability = await model.Availability.getProviderAvailability(req.user.PRO_UID, Encounter_Date)
    const providerAvailability = await model.ProviderAvailability.getProviderAvailability(req.user.PRO_UID, Encounter_Date)
    if (availability && (availability.PRO_Availability_Type != Encounter_Location_Type && availability.PRO_Availability_Type != 'Both')) {
      return res.send({
        code: 0,
        status: 'failed',
        message: 'You can not schedule office visit for this day',
        Error_Code: 'HS069'
      })
    }
    let availabilityStartTime, availabilityEndTime, Start_Time, End_Time

    if (availability) {
      availabilityStartTime = moment(`${availability.PRO_Availability_From_Time} ${availability.PRO_Availability_From_Time_Zone}`, 'hh:mm A').format('HH.mm')
      availabilityEndTime = moment(`${availability.PRO_Availability_To_Time} ${availability.PRO_Availability_To_Time_Zone}`, 'hh:mm A').format('HH.mm')
      Start_Time = moment(`${Encounter_Start_Time} ${Encounter_Start_Time_Zone}`, 'hh:mm A').format('HH.mm')
      End_Time = moment(`${Encounter_End_Time} ${Encounter_End_Time_Zone}`, 'hh:mm A').format('HH.mm')
    }
    if (availability && Encounter_Location_Type == 'Office' && (Start_Time < availabilityStartTime || Start_Time > availabilityEndTime)) {
      return res.send({
        code: 0,
        status: 'failed',
        message: 'Your encounter time is out of provider availability hours',
        Error_Code: 'HS070'
      })
    }

    if (availability) {
      Start_Time = Start_Time.includes(30) ? parseFloat(Start_Time) + 0.20 : parseFloat(Start_Time)
      End_Time = End_Time.includes(30) ? parseFloat(End_Time) + 0.20 : parseFloat(End_Time)

      if (Encounter_Location_Type == 'Home') {
        sTime = Start_Time - travelTimeSlot
        eTime = End_Time + travelTimeSlot
      } else {
        sTime = Start_Time
        eTime = End_Time
      }
      if (End_Time < Start_Time) return res.send({ code: 0, status: 'failed', message: 'Encounter_End_Time cannot less than Encounter_Start_Time', Error_Code: errorModule.Encounter_End_Time_less_than_Encounter_Start_Time() })
      var arr = availability.PRO_Slots.filter(ele => ((ele.Slot_Format >= sTime) && (ele.Slot_Format < eTime)))
      var filterProviderAvailability = providerAvailability.PRO_Slots.filter(ele => ((ele.Slot_Format >= sTime) && (ele.Slot_Format < eTime)))
      await checkAvailability(arr, travelTimeSlot * 2)
      await checkProviderAvailability(filterProviderAvailability)
      if (isAvailable) {
        var newarr = availability.PRO_Slots.map(element => ({
          Is_Home_Available: ((element.Slot_Format == sTime || (element.Slot_Format >= sTime && element.Slot_Format < eTime))) ? false : element.Is_Home_Available,
          Is_Office_Available: (element.Slot_Format == sTime || (element.Slot_Format >= sTime && element.Slot_Format < eTime)) ? false : element.Is_Office_Available,
          Is_Travel_Time: (element.Slot_Format >= sTime && element.Slot_Format < Start_Time) || (element.Slot_Format >= End_Time && element.Slot_Format < eTime) ? true : element.Is_Travel_Time,
          _id: element._id,
          Slot_Time: element.Slot_Time,
          Slot_Time_Zone: element.Slot_Time_Zone,
          Slot_Format: element.Slot_Format
        }))
        var newProviderAvailabilityArray = providerAvailability.PRO_Slots.map(element => ({
          Is_Home_Available: ((element.Slot_Format == sTime || (element.Slot_Format >= sTime && element.Slot_Format < eTime))) ? false : element.Is_Home_Available,
          Is_Office_Available: (element.Slot_Format == sTime || (element.Slot_Format >= sTime && element.Slot_Format < eTime)) ? false : element.Is_Office_Available,
          _id: element._id,
          Slot_Time: element.Slot_Time,
          Slot_Time_Zone: element.Slot_Time_Zone,
          Slot_Format: element.Slot_Format,
          Is_Home_Availability: element.Is_Home_Availability,
          Is_Office_Availability: element.Is_Office_Availability
        }))
        let patient = await model.Patient.findOneByEmail(PT_Username)
        if (patient) {
          req.body.PT_HS_MRN = patient.PT_HS_MRN
          let patientHistory = await model.PatientHistory.findByUsername(PT_Username)
          if (patientHistory) {
            patientHistory.PRO_UID = req.user.PRO_UID
            patientHistory.PT_Username = PT_Username
            req.body.PT_History = patientHistory
          } else {
            let PatientHistory = {
              PRO_UID: req.user.PRO_UID,
              PT_Username: PT_Username
            }
            req.body.PT_History = PatientHistory
          }
          await model.Encounter.create(req.body)
          await model.Availability.updateAvailabilityByTime(req.user.PRO_UID, Encounter_Date, newarr)
          await model.ProviderAvailability.updateAvailability(req.user.PRO_UID, Encounter_Date, newProviderAvailabilityArray)
          let chartData = {
            PRO_UID: req.user.PRO_UID,
            PT_HS_MRN: patient.PT_HS_MRN,
            PT_Username: PT_Username,
            Encounter_UID: req.body.Encounter_UID
          }
          await model.Chart.create(chartData)
          await createAudit(req)
          isAvailable = true
          res.send({ code: 1, status: 'sucess', message: 'encounter created sucessfull' })
        } else {
          isAvailable = true
          res.send({ code: 0, status: 'failed', message: `Patient with ${PT_Username} is not available`, Error_Code: errorModule.patient_not_found() })
        }
      } else {
        isAvailable = true
        res.send({ code: 3, status: 'failed', message: 'Apologies, This timeslot is no longer available', Error_Code: errorModule.You_can_not_set_encounter() })
      }
    } else {
      isAvailable = true
      res.send({ code: 0, status: 'failed', message: `No availability set for ${Encounter_Date} of Dr. ${req.user.PRO_Last_Name}`, Error_Code: errorModule.No_availability_available() })
    }
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS073' })
  }
})

var checkAvailability = async (availabilityArray, ignoreIndex) => {
  var count = 0
  availabilityArray.map(ele => {
    if (ele.Is_Travel_Time || (ele.Is_Home_Available == true && ele.Is_Office_Available == true)) {
      count = count + 1
      if (count == availabilityArray.length) {
        return true
      }
    } else {
      isAvailable = false
      count = count + 1
      if (count == availabilityArray.length) {
        return true
      }
    }
  })
}

let checkProviderAvailability = async (availabilityArray) => {
  var count = 0
  availabilityArray.map(ele => {
    if (ele.Is_Home_Available == true && ele.Is_Office_Available == true) {
      count = count + 1
      if (count == availabilityArray.length) {
        return true
      }
    } else {
      isAvailable = false
      count = count + 1
      if (count == availabilityArray.length) {
        return true
      }
    }
  })
}

async function createAudit (req) {
  var auditData = {
    Audit_UID: await auditUid(),
    Audit_Info: 'New- ' + await auditUid(),
    Audit_Type: auditType.add,
    Data_Point: 'EN/create',
    New_Value: req.body,
    IP_Address: await model.Audit.getIpAddress(req)
  }
  await model.Audit.create(auditData)
}
module.exports = router
