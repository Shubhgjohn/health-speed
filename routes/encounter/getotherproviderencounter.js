/* eslint-disable eqeqeq */
/* eslint-disable no-unneeded-ternary */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const model = require('../../models')
const errorModules = require('../../models').Errors
const serviceModule = require('../../models').CommonServices
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const moment = require('moment')
const logger = require('../../models/errorlog')

router.post('/get-provider-encounter/:Encounter_UID', authorize(Role.Provider), async (req, res) => {
  var { params: { Encounter_UID }, body: { PRO_UID, PT_HS_MRN } } = req
  try {
    let encounter = await model.Encounter.getPatientProviderEncounter(PRO_UID, PT_HS_MRN)
    if (encounter.length == 0) return res.send({ code: 0, status: 'failed', message: `No encounter found with ${Encounter_UID}`, Error_Code: errorModules.Encounter_not_found() })
    let provider_info = {}
    provider_info.PRO_UID = encounter[0].provider.PRO_UID
    provider_info.PRO_First_Name = encounter[0].provider.PRO_First_Name ? encounter[0].provider.PRO_First_Name : null
    provider_info.PRO_Last_Name = encounter[0].provider.PRO_Last_Name ? encounter[0].provider.PRO_Last_Name : null
    provider_info.PRO_Home_City = encounter[0].provider.PRO_Home_City ? encounter[0].provider.PRO_Home_City : null
    provider_info.PRO_Home_State = encounter[0].provider.PRO_Home_State ? encounter[0].provider.PRO_Home_State : null
    provider_info.PRO_Profile_Picture = encounter[0].provider.PRO_Profile_Picture ? await serviceModule.getImage(encounter[0].provider.PRO_Profile_Picture) : null
    provider_info.PRO_Primary_Speciality = await encounter[0].provider.PRO_Speciality.find(function (item) {
      // eslint-disable-next-line no-return-assign
      return item.PRO_Primary_Speciality = true
    })
    provider_info.PRO_Primary_Speciality = provider_info.PRO_Primary_Speciality ? provider_info.PRO_Primary_Speciality.PRO_Speciality : null
    provider_info.PRO_Office_Address_Line_1 = encounter[0].provider.PRO_Office_Address_Line_1 ? encounter[0].provider.PRO_Office_Address_Line_1 : null
    provider_info.PRO_Office_Address_Line_2 = encounter[0].provider.PRO_Office_Address_Line_2 ? encounter[0].provider.PRO_Office_Address_Line_2 : null
    provider_info.PRO_Office_City = encounter[0].provider.PRO_Office_City ? encounter[0].provider.PRO_Office_City : null
    provider_info.PRO_Office_State = encounter[0].provider.PRO_Office_State ? encounter[0].provider.PRO_Office_State : null
    provider_info.PRO_Office_Zip = encounter[0].provider.PRO_Office_Zip ? encounter[0].provider.PRO_Office_Zip : null
    provider_info.PRO_Home_Zip = encounter[0].provider.PRO_Home_Zip ? encounter[0].provider.PRO_Home_Zip : null
    provider_info.PRO_Office_Phone = encounter[0].provider.PRO_Office_Phone ? encounter[0].provider.PRO_Office_Phone : null
    provider_info.PRO_Home_Phone = encounter[0].provider.PRO_Home_Phone ? encounter[0].provider.PRO_Home_Phone : null
    provider_info.PRO_BIO = encounter[0].provider.PRO_BIO ? encounter[0].provider.PRO_BIO : null
    provider_info.PRO_Speciality = encounter[0].provider.PRO_Speciality ? encounter[0].provider.PRO_Speciality : []
    provider_info.PRO_Board_Cert = encounter[0].provider.PRO_Board_Cert ? encounter[0].provider.PRO_Board_Cert : []
    provider_info.PRO_Educations = encounter[0].provider.PRO_Educations ? encounter[0].provider.PRO_Educations.map(ele => ({ PRO_Education: ele.PRO_Education })) : []
    provider_info.PRO_Experiences = encounter[0].provider.PRO_Experiences ? encounter[0].provider.PRO_Experiences.map(ele => ({ PRO_Experience: ele.PRO_Experience })) : []
    provider_info.PRO_Affliation = encounter[0].provider.PRO_Affliation ? encounter[0].provider.PRO_Affliation.map(ele => ({ PRO_Affiliations: ele.PRO_Affiliations })) : []
    provider_info.PRO_Actions = encounter[0].provider.PRO_Actions ? encounter[0].provider.PRO_Actions.map(ele => ({ PRO_Actions_Against: ele.PRO_Actions_Against })) : []

    var patientInfo = {}
    patientInfo.PT_HS_MRN = encounter[0].patient.PT_HS_MRN
    patientInfo.PT_Username = encounter[0].patient.PT_Username ? encounter[0].patient.PT_Username : null
    patientInfo.PT_First_Name = encounter[0].patient.PT_First_Name ? encounter[0].patient.PT_First_Name : null
    patientInfo.PT_Last_Name = encounter[0].patient.PT_Last_Name ? encounter[0].patient.PT_Last_Name : null
    patientInfo.PT_Age_Now = encounter[0].patient.PT_DOB ? new Date().getFullYear() - encounter[0].patient.PT_DOB.getFullYear() : null
    patientInfo.PT_Gender = encounter[0].patient.PT_Gender ? encounter[0].patient.PT_Gender : null
    patientInfo.PT_Home_City = encounter[0].patient.PT_Home_City ? encounter[0].patient.PT_Home_City : null
    patientInfo.PT_Home_State = encounter[0].patient.PT_Home_State ? encounter[0].patient.PT_Home_State : null
    patientInfo.PT_Home_Zip = encounter[0].patient.PT_Home_Zip ? encounter[0].patient.PT_Home_Zip : null
    patientInfo.PT_Home_Address_Line_1 = encounter[0].patient.PT_Home_Address_Line_1 ? encounter[0].patient.PT_Home_Address_Line_1 : null
    patientInfo.PT_Home_Address_Line_2 = encounter[0].patient.PT_Home_Address_Line_2 ? encounter[0].patient.PT_Home_Address_Line_2 : null
    patientInfo.PT_Home_Address_Latitude = encounter[0].patient.PT_Home_Address_Latitude ? encounter[0].patient.PT_Home_Address_Latitude : null
    patientInfo.PT_Home_Address_Longitude = encounter[0].patient.PT_Home_Address_Longitude ? encounter[0].patient.PT_Home_Address_Longitude : null
    patientInfo.PT_Cell_Phone = encounter[0].patient.PT_Cell_Phone ? encounter[0].patient.PT_Cell_Phone : null
    patientInfo.PT_DOB = encounter[0].patient.PT_DOB ? moment(encounter[0].patient.PT_DOB).format('MM-DD-YYYY') : null

    patientInfo.PT_Profile_Picture = encounter[0].patient.PT_Profile_Picture ? await serviceModule.getImage(encounter[0].patient.PT_Profile_Picture) : null

    let encounter_data = await model.Encounter.findEncounter(Encounter_UID)
    let encounterInfo = encounter_data.map(ele => ({
      Encounter_Chief_Complaint_PRO: ele.Encounter_Chief_Complaint_PRO ? ele.Encounter_Chief_Complaint_PRO : null,
      Encounter_Chief_Complaint_PT: ele.Encounter_Chief_Complaint_PT ? ele.Encounter_Chief_Complaint_PT : null,
      Encounter_PT_Chief_Complaint_More: ele.Encounter_PT_Chief_Complaint_More ? ele.Encounter_PT_Chief_Complaint_More : null,
      Encounter_Type: ele.Encounter_Type ? ele.Encounter_Type : null,
      Encounter_Location_Type: ele.Encounter_Location_Type ? ele.Encounter_Location_Type : null,
      Encounter_Time: ele.Encounter_Time ? ele.Encounter_Time : null,
      Encounter_End_Time: ele.Encounter_End_Time ? `${ele.Encounter_End_Time} ${ele.Encounter_End_Time_Zone}` : null,
      Encounter_Date: ele.Encounter_Date ? ele.Encounter_Date : null,
      Encounter_Day: ele.Encounter_Date ? moment(new Date(ele.Encounter_Date)).format('ddd') : null,
      Encounter_Address_1: ele.Encounter_Location_Type == 'Office' ? ele.provider.PRO_Office_Address_Line_1 : ele.patient.PT_Home_Address_Line_1,
      Encounter_Address_2: ele.Encounter_Location_Type == 'Office' ? ele.provider.PRO_Office_Address_Line_2 : ele.patient.PT_Home_Address_Line_2,
      Encounter_Address_Latitude: ele.Encounter_Location_Type == 'Office' ? ele.provider.PRO_Office_Address_Latitude : ele.patient.PT_Home_Address_Latitude,
      Encounter_Address_Longitute: ele.Encounter_Location_Type == 'Office' ? ele.provider.PRO_Office_Address_Longitude : ele.patient.PT_Home_Address_Longitude,
      Encounter_Cost: ele.Encounter_Cost ? ele.Encounter_Cost : null,
      Encounter_Status: ele.Encounter_Status ? ele.Encounter_Status : null,
      Encounter_Has_Followup: ele.Encounter_Has_Followup ? ele.Encounter_Has_Followup : false,
      Encounter_Transaction_Status: ele.Encounter_Transaction_Status ? ele.Encounter_Transaction_Status : null,
      Encounter_Reschedule_Status: ele.Encounter_Reschedule_Status ? ele.Encounter_Reschedule_Status : null,
      Reschedule_Request_Date: ele.Reschedule_Request_Date ? ele.Reschedule_Request_Date : null,
      Reschedule_Request_Time: ele.Reschedule_Request_Time ? ele.Reschedule_Request_Time : null,
      Primary_DX: ele.Primary_DX ? ele.Primary_DX : null
    }))

    let encounter_array = []
    encounter.map(ele => {
      encounter_array.push({
        Encounter_UID: ele.Encounter_UID ? ele.Encounter_UID : null,
        Encounter_Chief_Complaint_PRO: ele.Encounter_Chief_Complaint_PRO ? ele.Encounter_Chief_Complaint_PRO : null,
        Encounter_Chief_Complaint_PT: ele.Encounter_Chief_Complaint_PT ? ele.Encounter_Chief_Complaint_PT : null,
        Encounter_PT_Chief_Complaint_More: ele.Encounter_PT_Chief_Complaint_More ? ele.Encounter_PT_Chief_Complaint_More : null,
        Encounter_Type: ele.Encounter_Type ? ele.Encounter_Type : null,
        Encounter_Location_Type: ele.Encounter_Location_Type ? ele.Encounter_Location_Type : null,
        Encounter_Time: ele.Encounter_Time ? ele.Encounter_Time : null,
        Encounter_End_Time: ele.Encounter_End_Time ? `${ele.Encounter_End_Time} ${ele.Encounter_End_Time_Zone}` : null,
        Encounter_Date: ele.Encounter_Date ? ele.Encounter_Date : null,
        Encounter_Day: ele.Encounter_Date ? moment(new Date(ele.Encounter_Date)).format('ddd') : null,
        Encounter_Address_1: ele.Encounter_Location_Type == 'Office' ? ele.provider.PRO_Office_Address_Line_1 : ele.patient.PT_Home_Address_Line_1,
        Encounter_Address_2: ele.Encounter_Location_Type == 'Office' ? ele.provider.PRO_Office_Address_Line_2 : ele.patient.PT_Home_Address_Line_2,
        Encounter_Address_Latitude: ele.Encounter_Location_Type == 'Office' ? ele.provider.PRO_Office_Address_Latitude : ele.patient.PT_Home_Address_Latitude,
        Encounter_Address_Longitute: ele.Encounter_Location_Type == 'Office' ? ele.provider.PRO_Office_Address_Longitude : ele.patient.PT_Home_Address_Longitude,
        Encounter_Cost: ele.Encounter_Cost ? ele.Encounter_Cost : null,
        Encounter_Status: ele.Encounter_Status ? ele.Encounter_Status : null,
        Encounter_Has_Followup: ele.Encounter_Has_Followup ? ele.Encounter_Has_Followup : false,
        Encounter_Transaction_Status: ele.Encounter_Transaction_Status ? ele.Encounter_Transaction_Status : null,
        Encounter_Reschedule_Status: ele.Encounter_Reschedule_Status ? ele.Encounter_Reschedule_Status : null,
        Reschedule_Request_Date: ele.Reschedule_Request_Date ? ele.Reschedule_Request_Date : null,
        Reschedule_Request_Time: ele.Reschedule_Request_Time ? ele.Reschedule_Request_Time : null,
        Primary_DX: ele.Primary_DX ? ele.Primary_DX : null,
        Encounter_Access: (ele.Encounter_Access_Provider && ele.Encounter_Access_Provider.length > 0) ? ele.Encounter_Access_Provider.find(ele => (ele.PRO_UID == req.user.PRO_UID)) ? ele.Encounter_Access_Provider.find(ele => (ele.Access_Status == 'Requested')) ? true : false : false : false,
        Encounter_Access_Status: (ele.Encounter_Access_Provider && ele.Encounter_Access_Provider.length > 0) ? ele.Encounter_Access_Provider.find(ele => (ele.PRO_UID == req.user.PRO_UID)) ? ele.Encounter_Access_Provider.find(ele => (ele.Access_Status == 'Requested')) ? 'Requested' : 'Granted' : 'Request' : 'Request',
        PRO_First_Name: ele.provider.PRO_First_Name ? ele.provider.PRO_First_Name : '',
        PRO_Last_Name: ele.provider.PRO_Last_Name ? ele.provider.PRO_Last_Name : ''
      })
    })
    res.send({
      code: 1,
      status: 'sucess',
      Provider_Details: provider_info,
      Patients_Details: patientInfo,
      Visit_Info: encounterInfo,
      Other_visit: encounter_array
    })
  } catch (error) {
    console.log(error)
    logger.error(error.message)
    logger.fatal(error)
    return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS091' })
  }
})

module.exports = router
