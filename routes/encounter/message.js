/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const moment = require('moment')
const chartModule = require('../../models').Chart
const errorModule = require('../../models').Errors
const messageModule = require('../../models').Message
const patientModule = require('../../models').Patient
const providerModule = require('../../models').Provider
const encounterModule = require('../../models').Encounter
const notificationModule = require('../../models').Notification
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const logger = require('../../models/errorlog')
const fcm = require('../../_helpers/fcm')

router.post('/message/:Encounter_UID', authorize([Role.Provider, Role.Patient]), async (req, res) => {
  var {
    params: { Encounter_UID },
    body: { body }
  } = req
  if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required', Error_Code: errorModule.Encounter_UID_required() })
  if (!body) return res.send({ code: 0, status: 'failed', message: 'body is required', Error_Code: errorModule.body_required() })
  try {
    let encounter = await encounterModule.getEncounterByEncounter_UID(Encounter_UID)
    let patient = await patientModule.findOneByEmail(encounter.PT_Username)
    let provider = await providerModule.findById(encounter.PRO_UID)
    if (!encounter) return res.send({ code: 0, status: 'failed', message: `encounter with UID '${Encounter_UID}' not found!`, Error_Code: errorModule.Encounter_not_found() })
    let chartData = await chartModule.getChartData(Encounter_UID)
    if (chartData) {
      let messageData = {
        sentBy: req.user.PT_Username ? req.user.PT_HS_MRN : req.user.PRO_UID,
        body,
        Encounter_UID,
        Created_At: new Date()
      }
      await messageModule.create(messageData)
      // await createAudit(req);
      let jsonData = {
        Encounter_UID: Encounter_UID,
        title: 'chat'
      }
      if (req.userType == 'Patient') {
        let PRO_Token = await notificationModule.getToken('Provider', provider.PRO_Username)
        let title = `New HealthSpeed Message from ${patient.PT_First_Name} ${patient.PT_Last_Name.charAt(0)}`
        let body = 'This will show one line of the chat message and end in an ellipses'
        await sendNotification(PRO_Token, title, body, jsonData)
      } else {
        let PT_Token = await notificationModule.getToken('Patient', patient.PT_Username)
        let title = 'New Message'
        let body = `Login to view your message from Dr. ${provider.PRO_Last_Name}`
        await sendNotification(PT_Token, title, body, jsonData)
      }
      return res.send({ code: 1, status: 'sucess', message: 'message sent successfully' })
    } else {
      return res.send({ code: 0, status: 'failed', message: `No encounter found with ${Encounter_UID}`, Error_Code: errorModule.Encounter_not_found() })
    }
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS096' })
  }
})

router.get('/get-message/:Encounter_UID', authorize([Role.Provider, Role.Patient]), async (req, res) => {
  var { params: { Encounter_UID } } = req
  if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required', Error_Code: errorModule.Encounter_UID_required() })
  try {
    let encounter = await encounterModule.getEncounterByEncounter_UID(Encounter_UID)
    if (!encounter) return res.send({ code: 0, status: 'failed', message: `encounter with id '${Encounter_UID}' not found!`, Error_Code: errorModule.Encounter_not_found() })
    let messages = await messageModule.getEncounterMessages(Encounter_UID)
    if (messages) {
      // res.send(messages)
      let message = messages.map(ele => ({
        _id: ele._id,
        body: ele.body,
        sentBy: ele.sentBy,
        Message_Date: moment(ele.Created_At).tz(process.env.CURRENT_TIMEZONE).format('MM-DD-YYYY'),
        Message_Time: moment(ele.Created_At).tz(process.env.CURRENT_TIMEZONE).format('hh:mm a')
      }))
      return res.send({ code: 1, status: 'sucess', message: message })
    } else {
      return res.send({ code: 0, status: 'failed', message: [], Error_Code: 'HS097' })
    }
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.send({ code: 0, status: 'failed', message: error.message, Error_Code: 'HS098' })
  }
})


async function sendNotification (token, title, body, jsonData) {
  let arr = []
  token.map(ele => arr.push(ele.Device_Token))
  if (arr.length != 0) {
    var payload = { // this may vary according to the message type (single recipient, multicast, topic, et cetera)
      registration_ids: arr,
      notification: {
        title: title,
        body: body
      },

      data: jsonData
    }
    fcm.send(payload, function (err, response) {
      if (err) {
      } else console.log(response)
    })
  }
}
module.exports = router
