/* eslint-disable eqeqeq */
/* eslint-disable no-unneeded-ternary */
/* eslint-disable no-self-assign */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const moment = require('moment')
const model = require('../../models')
// const auditModule = require('../../models').Audit
const errorModule = require('../../models').Errors
const encounterModule = require('../../models').Encounter
const providerAvailabilityModule = require('../../models').Availability
const providerModule = require('../../models').Provider
const patientModule = require('../../models').Patient
const notificationModel = require('../../models').Notification
const availabilityCheck = require('../../service/checkProviderAvailability')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const logger = require('../../models/errorlog')
var isAvailable = true
let isConfirmed = true
const fcm = require('../../_helpers/fcm')
var AWS = require('aws-sdk')
AWS.config.update({
	accessKeyId: process.env.AWS_ACCESS_KEY,
	secretAccessKey: process.env.AWS_SECRET_KEY,
	region: 'us-east-1'
})
var s3 = new AWS.S3()

let count = 0

// route for the rescheduling encounter
router.put('/reschedule-encounter/:Encounter_UID', authorize([Role.Provider, Role.Patient]), async (req, res) => {
	try {
		var { body: { data }, params: { Encounter_UID } } = req
		if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required', Error_Code: errorModule.Encounter_UID_required() })
		if (!data) return res.send({ code: 0, status: 'failed', message: 'data is required', Error_Code: errorModule.data_required() })
		let encounter = await encounterModule.getEncounterByEncounter_UID(Encounter_UID)
		if (encounter) {
			// Get patient and provider details
			var provider = await providerModule.findById(encounter.PRO_UID)
			var patient = await patientModule.findOneByEmail(encounter.PT_Username)
			if (req.userType == 'Patient' && patient.PT_Account_Status == 'Suspended') return res.send({ code: 0, status: 'failed', message: 'Please contact Healthspeed.', Error_Code: errorModule.patient_can_not_accept() })
			if (req.userType == 'Patient' && provider.PRO_Account_Status != 'Active') return res.send({ code: 0, status: 'failed', message: 'Please contact Healthspeed.', Error_Code: errorModule.provider_can_not_accept() })
			if (req.userType == 'Provider' && provider.PRO_Account_Status == 'Suspended') return res.send({ code: 0, status: 'failed', message: 'Please contact Healthspeed.', Error_Code: errorModule.provider_can_not_accept() })
			if (req.userType == 'Provider' && patient.PT_Account_Status != 'Active') return res.send({ code: 0, status: 'failed', message: 'Please contact Healthspeed.', Error_Code: errorModule.patient_can_not_accept() })

			var travelTimeSlot = req.userType == 'Patient' ? provider.PRO_Home_Visit_Buffer / 60 : req.user.PRO_Home_Visit_Buffer / 60
			let encounterOldTime = moment(`${encounter.Encounter_Start_Time} ${encounter.Encounter_Start_Time_Zone}`, 'hh:mm A').format('HH.mm')
			let encounterOldEndTime = moment(`${encounter.Encounter_End_Time} ${encounter.Encounter_End_Time_Zone}`, 'hh:mm A').format('HH.mm')
			encounterOldTime = encounterOldTime.includes(30) ? parseFloat(encounterOldTime) + 0.50 : parseFloat(encounterOldTime)
			encounterOldEndTime = encounterOldEndTime.includes(30) ? parseFloat(encounterOldEndTime) + 0.50 : parseFloat(encounterOldEndTime)
			let differance = encounterOldEndTime - encounterOldTime
			let providerAvailability = await providerAvailabilityModule.getProviderAvailability(encounter.PRO_UID, data.Reschedule_Request_Date)
			const providerAvailable = await model.ProviderAvailability.getProviderAvailability(encounter.PRO_UID, data.Reschedule_Request_Date)
			data.Reschedule_Request_Time = data.Reschedule_Request_Time.charAt(0) == '0' ? data.Reschedule_Request_Time.slice(1) : data.Reschedule_Request_Time
			if (providerAvailability) {
				var Start_Time = parseFloat(moment(`${data.Reschedule_Request_Time}`, 'hh:mm A').format('HH.mm'))
				Start_Time = Start_Time % 1 != 0 ? Start_Time + 0.2 : Start_Time
				var End_Time = Start_Time + differance
				if (data.Encounter_Location_Type == 'Home') {
					Start_Time = Start_Time - travelTimeSlot
					End_Time = End_Time + travelTimeSlot
				} else {
					Start_Time = Start_Time
					End_Time = End_Time
				}

				// check the availability from system
				if (End_Time < Start_Time) return res.send({ code: 0, status: 'failed', message: 'Encounter_End_Time cannot less than Encounter_Start_Time', Error_Code: errorModule.Encounter_End_Time_less_than_Encounter_Start_Time() })
				var arr = providerAvailability.PRO_Slots.filter(ele => ((ele.Slot_Format >= Start_Time) && (ele.Slot_Format < End_Time)))
				var filterProviderAvailability = providerAvailable.PRO_Slots.filter(ele => ((ele.Slot_Format >= Start_Time) && (ele.Slot_Format < End_Time)))
				let newData = await availabilityCheck.checkProviderAvailability(filterProviderAvailability, data.Encounter_Location_Type, isAvailable)
				isAvailable = newData == 'Available' ? true : false
				await checkAvailability(arr)
				// if provider is available
				if (isAvailable) {
					if (req.userType == 'Provider') {
						let regularHour = req.user.Pro_Office_Hours_Time
						let day = moment(new Date(data.Reschedule_Request_Date)).format('ddd')
						if (day == 'Thu') day = day + 'r'
						if (day == 'Tue') day = day + 's'
						let netDate = regularHour.find(ele => ele.day == day)

						let fromTime = netDate.fromTime.time.includes(':') ? netDate.fromTime.time : netDate.fromTime.time + ':00'
						let toTime = netDate.toTime.time.includes(':') ? netDate.toTime.time : netDate.toTime.time + ':00'
						fromTime = Date.parse(moment(new Date(`${data.Reschedule_Request_Date} ${fromTime} ${netDate.fromTime.zone}`)))
						toTime = Date.parse(moment(new Date(`${data.Reschedule_Request_Date} ${toTime} ${netDate.toTime.zone}`)))
						req.body.data.Reschedule_Request_Time = req.body.data.Reschedule_Request_Time.includes(':') ? req.body.data.Reschedule_Request_Time : req.body.data.Reschedule_Request_Time.split(' ')[0] + ':00' + ' ' + req.body.data.Reschedule_Request_Time.split(' ')[1]

						let encounter_time = Date.parse(moment(new Date(`${data.Reschedule_Request_Date} ${req.body.data.Reschedule_Request_Time}`)))

						let regularTime = encounter_time >= fromTime && encounter_time <= toTime ? true : false

						let specialRate = req.user.PRO_Special_Rate.filter(item => item.PRO_Special_Rate_Date == moment(new Date(data.Reschedule_Request_Date)).format('M/D/YY'))

						let location = data.Encounter_Location_Type.toLowerCase()

						// calculate the encounter cost
						if (regularTime) {
							if (location == 'office') {
								if (encounter.Encounter_Type == 'Followup Visit') {
									req.body.data.Encounter_Cost = specialRate.length > 0 ? specialRate[0].PRO_Office_Followup_Special_Hourly_Rate ? specialRate[0].PRO_Office_Followup_Special_Hourly_Rate : req.user.PRO_Office_Office_Hours_Followup_Rate : req.user.PRO_Office_Office_Hours_Followup_Rate
								} else {
									req.body.data.Encounter_Cost = specialRate.length > 0 ? specialRate[0].PRO_Office_New_Visit_Special_Hourly_Rate ? specialRate[0].PRO_Office_New_Visit_Special_Hourly_Rate : req.user.PRO_Office_Office_Hours_New_Visit_Rate : req.user.PRO_Office_Office_Hours_New_Visit_Rate
								}
							} else {
								if (encounter.Encounter_Type == 'Followup Visit') {
									req.body.data.Encounter_Cost = specialRate.length > 0 ? specialRate[0].PRO_Home_Followup_Special_Hourly_Rate ? specialRate[0].PRO_Home_Followup_Special_Hourly_Rate : req.user.PRO_Home_Office_Hours_Followup_Rate : req.user.PRO_Home_Office_Hours_Followup_Rate
								} else {
									req.body.data.Encounter_Cost = specialRate.length > 0 ? specialRate[0].PRO_Home_New_Visit_Special_Hourly_Rate ? specialRate[0].PRO_Home_New_Visit_Special_Hourly_Rate : req.user.PRO_Home_Office_Hours_New_Visit_Rate : req.user.PRO_Home_Office_Hours_New_Visit_Rate
								}
							}
						} else {
							if (location == 'office') {
								if (encounter.Encounter_Type == 'Followup Visit') {
									req.body.data.Encounter_Cost = specialRate.length > 0 ? specialRate[0].PRO_Office_Followup_Special_Off_Hourly_Rate ? specialRate[0].PRO_Office_Followup_Special_Off_Hourly_Rate : req.user.PRO_Office_Off_Hours_Followup_Rate : req.user.PRO_Office_Off_Hours_Followup_Rate
								} else {
									req.body.data.Encounter_Cost = specialRate.length > 0 ? specialRate[0].PRO_Office_New_Visit_Special_Off_Hourly_Rate ? specialRate[0].PRO_Office_New_Visit_Special_Off_Hourly_Rate : req.user.PRO_Office_Off_Hours_Followup_Rate : req.user.PRO_Office_Off_Hours_Followup_Rate
								}
							} else {
								if (encounter.Encounter_Type == 'Followup Visit') {
									req.body.data.Encounter_Cost = specialRate.length > 0 ? specialRate[0].PRO_Home_Followup_Special_Off_Hourly_Rate ? specialRate[0].PRO_Home_Followup_Special_Off_Hourly_Rate : req.user.PRO_Home_Off_Hours_Followup_Rate : req.user.PRO_Home_Off_Hours_Followup_Rate
								} else {
									req.body.data.Encounter_Cost = specialRate.length > 0 ? specialRate[0].PRO_Home_New_Visit_Special_Off_Hourly_Rate ? specialRate[0].PRO_Home_New_Visit_Special_Off_Hourly_Rate : req.user.PRO_Home_Off_Hours_Followup_Rate : req.user.PRO_Home_Off_Hours_Followup_Rate
								}
							}
						}
					}
					// eslint-disable-next-line no-redeclare
					var data = {
						Reschedule_Request_Date: data.Reschedule_Request_Date,
						Reschedule_Request_Time: data.Reschedule_Request_Time.includes(':') ? data.Reschedule_Request_Time : data.Reschedule_Request_Time.split(' ')[0] + ':00' + ' ' + data.Reschedule_Request_Time.split(' ')[1],
						Encounter_Reschedule_Status: req.userType == 'Provider' ? 'Requested' : 'Scheduled',
						Reschedule_Requested_By: req.userType,
						Reschedule_Requested_Time: new Date(),
						Encounter_Cost: req.body.data.Encounter_Cost ? req.body.data.Encounter_Cost : encounter.Encounter_Cost,
						Encounter_Location_Type: data.Encounter_Location_Type ? data.Encounter_Location_Type : encounter.Encounter_Location_Type,
						Encounter_Travel_Time: provider.PRO_Home_Visit_Buffer
					}
					await encounterModule.updateEncounter(Encounter_UID, data)
					encounter = await encounterModule.getEncounterByEncounter_UID(Encounter_UID)
					
					// send notification to the patient for encounter reschedule
					if (encounter.Encounter_Type == 'New Visit') {
						if (encounter.Encounter_Status == 'Scheduled' || encounter.Encounter_Status == 'Cancelled') {
							if (req.userType == 'Patient') {
								let SetData = await makeData(encounter)
								let confirmed = await setEncounter(SetData, provider, encounter, res)
								if (!isConfirmed) return res.send({ code: 3, status: 'failed', message: confirmed, Error_Code: count == 1 ? errorModule.Encounter_End_Time_less_than_Encounter_Start_Time() : count == 2 ? errorModule.You_can_not_set_encounter() : errorModule.You_can_not_set_encounter() })
								await encounterModule.updateEncounter(encounter.Encounter_UID, SetData)
								let jsonData = {
									Encounter_UID: encounter.Encounter_UID,
									PRO_UID: encounter.PRO_UID,
									title: 'Encounter Rechedule'
								}
								let arr = await getToken('Provider', provider.PRO_Username)
								let title = `Visit Rescheduled: ${moment(new Date(encounter.Encounter_Date)).format('M/D/YY')}`
								let body = 'Don’t forget to review this rescheduled visit.'
								await sendNotification(arr, title, body, jsonData)
							} else {
								let jsonData = await NotificationData(encounter, patient, provider, data)
								let arr = await getToken('Patient', patient.PT_Username)
								let title = 'Your Visit Has Been Rescheduled'
								let body = 'Check out your visit that has been rescheduled.'
								await sendNotification(arr, title, body, jsonData)
							}
						}
					} else {
						let parentEncounter = await encounterModule.getEncounterByEncounter_UID(encounter.Parent_Encounter)
						if (req.userType == 'Patient') {
							let SetData = await makeData(encounter)
							let confirmed = await setEncounter(SetData, provider, encounter, res)
							if (!isConfirmed) return res.send({ code: 0, status: 'failed', message: confirmed, Error_Code: count == 1 ? errorModule.Encounter_End_Time_less_than_Encounter_Start_Time() : count == 2 ? errorModule.You_can_not_set_encounter() : errorModule.No_availability_available() })
							await encounterModule.updateEncounter(encounter.Encounter_UID, SetData)
							let jsonData = {
								Encounter_UID: encounter.Encounter_UID,
								PRO_UID: encounter.PRO_UID,
								title: 'Encounter Rechedule'
							}
							if (parentEncounter.Encounter_Followup_Status == 'Scheduled') {
								let arr = await getToken('Provider', provider.PRO_Username)
								let title = `Followup Reschedule Request: ${moment(new Date(encounter.Encounter_Date)).format('M/D/YY')}`
								let body = `${req.user.PT_First_Name} ${req.user.PT_Last_Name.charAt(0)} has rescheduled a followup visit. Open the app to see the changes.`
								await sendNotification(arr, title, body, jsonData)
							}
						} else {
							let jsonData = await NotificationData(encounter, patient, provider, data)
							let arr = await getToken('Patient', patient.PT_Username)
							let title = 'A Followup Visit Has Been Rescheduled'
							let body = 'An upcoming followup visit has been rescheduled.'
							await sendNotification(arr, title, body, jsonData)
						}
					}
					// await createAudit(req);
					res.send({
						code: 1,
						status: 'sucess',
						message: 'Encounter reschedule successfully',
						Reschedule_Request_Date: data.Reschedule_Request_Date,
						Reschedule_Request_Time: data.Reschedule_Request_Time,
						Encounter_Cost: data.Encounter_Cost,
						Encounter_Location_Type: data.Encounter_Location_Type
					})
				} else {
					isAvailable = true
					res.send({ code: 3, status: 'failed', message: 'Apologies, This timeslot is no longer available', Error_Code: errorModule.You_can_not_set_encounter() })
				}
			} else {
				isAvailable = true
				res.send({ code: 0, status: 'failed', message: `No availability set for ${data.Reschedule_Request_Date} of Dr. ${provider.PRO_Last_Name}`, Error_Code: errorModule.No_availability_available() })
			}
		} else {
			// await createAudit(req);
			res.send({ code: 0, status: 'failed', message: `No encounter find with ${Encounter_UID}`, Error_Code: errorModule.Encounter_not_found() })
		}
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS088' })
	}
})


async function makeData (encounter) {
	let diff = Math.abs(new Date(`${encounter.Encounter_Date} ${encounter.Encounter_Start_Time} ${encounter.Encounter_Start_Time_Zone}`) - new Date(`${encounter.Encounter_Date} ${encounter.Encounter_End_Time} ${encounter.Encounter_End_Time_Zone}`)) / 36e5
	let Reschedule_End_Time = await getAfterDiffTime(new Date(`${encounter.Reschedule_Request_Date} ${encounter.Reschedule_Request_Time}`), diff)
	let data = {
		Encounter_Date: encounter.Reschedule_Request_Date,
		Encounter_Time: encounter.Reschedule_Request_Time,
		Encounter_Start_Time: encounter.Reschedule_Request_Time.split(' ')[0],
		Encounter_Start_Time_Zone: encounter.Reschedule_Request_Time.split(' ')[1].toLowerCase(),
		Encounter_End_Time: moment(Reschedule_End_Time).format('LT').split(' ')[0],
		Encounter_End_Time_Zone: moment(Reschedule_End_Time).format('LT').split(' ')[1].toLowerCase(),
		Encounter_Reschedule_Status: 'Scheduled',
		Encounter_Status: 'Scheduled',
		First_Notification: false,
		Second_Notification: false,
		PMH_Notification: false
	}
	return data
}

function getAfterDiffTime (date, diff) {
	date.setTime(date.getTime() + (diff * 60 * 60 * 1000))
	return date
}

async function setEncounter (data, provider, encounter, res) {
	data.Encounter_Time = data.Encounter_Start_Time + ' ' + data.Encounter_Start_Time_Zone
	var travelTimeSlot = provider.PRO_Home_Visit_Buffer / 60
	var sTime, eTime
	var availability = await model.Availability.getProviderAvailability(provider.PRO_UID, data.Encounter_Date)
	var providerAvailability = await model.ProviderAvailability.getProviderAvailability(encounter.PRO_UID, data.Encounter_Date)
	if (availability) {
		await resetEncounter(encounter, res)
		availability = await model.Availability.getProviderAvailability(provider.PRO_UID, data.Encounter_Date)
		providerAvailability = await model.ProviderAvailability.getProviderAvailability(encounter.PRO_UID, data.Encounter_Date)
		var Start_Time = moment(`${data.Encounter_Start_Time} ${data.Encounter_Start_Time_Zone}`, 'hh:mm A').format('HH.mm')
		var End_Time = moment(`${data.Encounter_End_Time} ${data.Encounter_End_Time_Zone}`, 'hh:mm A').format('HH.mm')

		Start_Time = Start_Time.includes(30) ? parseFloat(Start_Time) + 0.20 : parseFloat(Start_Time)
		End_Time = End_Time.includes(30) ? parseFloat(End_Time) + 0.20 : parseFloat(End_Time)
		if (data.Encounter_Location_Type == 'Home') {
			sTime = Start_Time - travelTimeSlot
			eTime = End_Time + travelTimeSlot
		} else {
			sTime = Start_Time
			eTime = End_Time
		}
		if (End_Time < Start_Time) {
			isConfirmed = false
			count = 1
			return 'Encounter_End_Time cannot less than Encounter_Start_Time'
		}
		var arr = availability.PRO_Slots.filter(ele => ((ele.Slot_Format >= sTime) && (ele.Slot_Format < eTime)))
		var filterProviderAvailability = providerAvailability.PRO_Slots.filter(ele => ((ele.Slot_Format >= sTime) && (ele.Slot_Format < eTime)))
		await availabilityCheck.checkProviderAvailability(filterProviderAvailability, encounter.Encounter_Location_Type, isAvailable)
		await checkAvailability(arr, travelTimeSlot * 2)
		if (isAvailable) {
			var newarr = availability.PRO_Slots.map(element => ({
				Is_Home_Available: ((element.Slot_Format == sTime || (element.Slot_Format >= sTime && element.Slot_Format < eTime))) ? false : element.Is_Home_Available,
				Is_Office_Available: (element.Slot_Format == sTime || (element.Slot_Format >= sTime && element.Slot_Format < eTime)) ? false : element.Is_Office_Available,
				Is_Travel_Time: (element.Slot_Format >= sTime && element.Slot_Format < Start_Time) || (element.Slot_Format >= End_Time && element.Slot_Format < eTime) ? true : element.Is_Travel_Time,
				_id: element._id,
				Slot_Time: element.Slot_Time,
				Slot_Time_Zone: element.Slot_Time_Zone,
				Slot_Format: element.Slot_Format
			}))
			var newProviderAvailabilityArray = providerAvailability.PRO_Slots.map(element => ({
				Is_Home_Available: ((element.Slot_Format == sTime || (element.Slot_Format >= sTime && element.Slot_Format < eTime))) ? false : element.Is_Home_Available,
				Is_Office_Available: (element.Slot_Format == sTime || (element.Slot_Format >= sTime && element.Slot_Format < eTime)) ? false : element.Is_Office_Available,
				_id: element._id,
				Slot_Time: element.Slot_Time,
				Slot_Time_Zone: element.Slot_Time_Zone,
				Slot_Format: element.Slot_Format,
				Is_Home_Availability: element.Is_Home_Availability,
				Is_Office_Availability: element.Is_Office_Availability
			}))
			await model.Availability.updateAvailabilityByTime(provider.PRO_UID, data.Encounter_Date, newarr)
			await model.ProviderAvailability.updateAvailability(encounter.PRO_UID, data.Encounter_Date, newProviderAvailabilityArray)
			isAvailable = true
		} else {
			isAvailable = true
			isConfirmed = false
			count = 3
			return 'Apologies, This timeslot is no longer available'
		}
	} else {
		isAvailable = true
		isConfirmed = false
		count = 3
		return `No availability set for ${data.Encounter_Date} of Dr. ${provider.PRO_Last_Name}`
	}
}

async function resetEncounter (encounter, res) {
	const availability = await model.Availability.getProviderAvailability(encounter.PRO_UID, encounter.Encounter_Date)
	const providerAvailability = await model.ProviderAvailability.getProviderAvailability(encounter.PRO_UID, encounter.Encounter_Date)
	if (availability) {
		var Start_Time = moment(`${encounter.Encounter_Start_Time} ${encounter.Encounter_Start_Time_Zone}`, 'hh:mm A').format('HH.mm')
		var End_Time = moment(`${encounter.Encounter_End_Time} ${encounter.Encounter_End_Time_Zone}`, 'hh:mm A').format('HH.mm')
		Start_Time = Start_Time.includes(30) ? parseFloat(Start_Time) + 0.20 : parseFloat(Start_Time)
		End_Time = End_Time.includes(30) ? parseFloat(End_Time) + 0.20 : parseFloat(End_Time)
		var sIndex = await getSlotsTime(availability.PRO_Slots, Start_Time)
		var eIndex = await getSlotsTime(availability.PRO_Slots, End_Time)
		var count = 0
		var temp = 0
		if (sIndex != 0) {
			for (let i = sIndex - 1; ; i--) {
				if (availability.PRO_Slots[i].Is_Travel_Time == false) {
					if (availability.PRO_Slots[i].Is_Home_Available == true && availability.PRO_Slots[i].Is_Office_Available == true) {
						sIndex = i + 1
						break
					} else {
						sIndex = sIndex
						break
					}
				}
				if (i == 0) break
				count = count + 1
			}
		}

		if (eIndex != 47) {
			for (let i = eIndex; ; i++) {
				if (availability.PRO_Slots[i].Is_Travel_Time == false) {
					if (availability.PRO_Slots[i].Is_Home_Available == true && availability.PRO_Slots[i].Is_Office_Available == true) {
						eIndex = i - 1
						break
					} else {
						if (count == temp) {
							eIndex = eIndex - 1
						} else {
							eIndex = eIndex + count > 0 ? (count - 1) : count
						}
						break
					}
				}
				if (i == 47) break
				temp = temp + 1
			}
		}

		for (var i = sIndex; i <= eIndex; i++) {
			availability.PRO_Slots[i].Is_Home_Available = true
			availability.PRO_Slots[i].Is_Office_Available = true
			availability.PRO_Slots[i].Is_Travel_Time = false
			encounter.Encounter_Location_Type == 'Office' ? providerAvailability.PRO_Slots[i].Is_Office_Available = true : providerAvailability.PRO_Slots[i].Is_Home_Available = true
		}
		await model.Availability.updateAvailability(encounter.PRO_UID, availability)
		await model.ProviderAvailability.updateAvailability(encounter.PRO_UID, providerAvailability.PRO_Availability_Date, providerAvailability.PRO_Slots)
	}
}

function getSlotsTime (arr, time) {
	var index = -1
	arr.find((ele, i) => {
		if (ele.Slot_Format == time) {
			index = i
			return i
		}
	})
	return index
}

var checkAvailability = async (availabilityArray) => {
	var count = 0
	availabilityArray.map(ele => {
		if ((ele.Is_Home_Available == true && ele.Is_Office_Available == true) || ele.Is_Travel_Time == true) {
			count = count + 1
			if (count == availabilityArray.length) {
				return true
			}
		} else {
			isAvailable = false
			count = count + 1
			if (count == availabilityArray.length) {
				return true
			}
		}
	})
}

async function getToken (role, email) {
	let token = await notificationModel.getToken(role, email)
	let arr = []
	token.map(ele => arr.push(ele.Device_Token))
	return arr
}

async function sendNotification (arr, title, body, jsonData) {
	if (arr.length != 0) {
		var payload = { // this may vary according to the message type (single recipient, multicast, topic, et cetera)
			registration_ids: arr,
			notification: {
				title: title,
				body: body
			},

			data: jsonData
		}
		await fcm.send(payload, function (err, response) {
			if (err) {
			} else console.log(response)
		})
	}
}

function getImageUrl (imageKey) {
	if (imageKey == null || imageKey == '') {
		return ''
	} else {
		return new Promise((resolve, reject) => {
			const url = s3.getSignedUrl('getObject', {
				Bucket: process.env.AWS_BUCKET,
				Key: imageKey,
				Expires: 60 * 60 * 24 * 15
			})
			resolve(url)
		})
	}
}

async function NotificationData (encounter, patient, provider, data) {
	let encounterPatient = await patientModule.findById(encounter.PT_HS_MRN)
	let day = moment(new Date(encounter.Encounter_Date)).format('ddd')
	if (day == 'Thu') day = day + 'r'
	if (day == 'Tue') day = day + 's'
	let netDate = provider.Pro_Office_Hours_Time.find(ele => ele.day == day)
	let fromTime = netDate.fromTime.time.includes(':') ? netDate.fromTime.time : netDate.fromTime.time + ':00'
	let toTime = netDate.toTime.time.includes(':') ? netDate.toTime.time : netDate.toTime.time + ':00'
	let Pro_Office_Hours_Time = ((new Date(`${encounter.Encounter_Date} ${fromTime} ${netDate.fromTime.zone}`) <= new Date(`${encounter.Encounter_Date} ${encounter.Encounter_Time}`)) && (new Date(`${encounter.Encounter_Date} ${encounter.Encounter_Time}`) <= new Date(`${encounter.Encounter_Date} ${toTime} ${netDate.toTime.zone}`))) ? true : false
	let card = null
	try {
		card = await model.Stripe.getCardByCardId(patient.stripeCustomerId, encounter.Payment_Card_Id)
	} catch (err) {
		card = null
	}
	let patientChart = await model.Chart.findChartByPT_HS_MRN(encounter.PT_HS_MRN)
	let shareChart = []
	patientChart.map(item => {
		if (item.PRO_Chart_Access.length == 0) {
			shareChart.push(item)
		} else {
			if (!item.PRO_Chart_Access.find(find => (find.PRO_UID == provider.PRO_UID) || (find.PRO_UID == provider.PRO_UID && find.Access_Status == true))) shareChart.push(item)
		}
	})
	let jsonData = {
		Encounter_UID: encounter.Encounter_UID,
		Encounter_Location_Type: encounter.Encounter_Location_Type,
		Encounter_Date: encounter.Encounter_Date,
		Encounter_Time: encounter.Encounter_Time,
		Reschedule_Request_Date: data.Reschedule_Request_Date,
		Reschedule_Request_Time: data.Reschedule_Request_Time,
		Encounter_Cost: encounter.Encounter_Cost.toString(),
		Encounter_Chief_Complaint_PT: encounter.Encounter_Chief_Complaint_PT,
		PRO_Profile_Picture: provider.PRO_Profile_Picture ? await getImageUrl(provider.PRO_Profile_Picture) : '',
		PRO_Last_Name: provider.PRO_Last_Name,
		PRO_First_Name: provider.PRO_First_Name,
		PRO_UID: provider.PRO_UID,
		PRO_Office_Address_Line_1: provider.PRO_Office_Address_Line_1 ? provider.PRO_Office_Address_Line_1 : '',
		PRO_Office_Address_Line_2: provider.PRO_Office_Address_Line_2 ? provider.PRO_Office_Address_Line_2 : '',
		PRO_Office_City: provider.PRO_Office_City ? provider.PRO_Office_City : '',
		PRO_Office_State: provider.PRO_Office_State ? provider.PRO_Office_State : '',
		PRO_Office_Zip: provider.PRO_Office_Zip ? provider.PRO_Office_Zip.toString() : '',
		Pro_Office_Hours_Time: Pro_Office_Hours_Time.toString(),
		PT_Profile_Picture: encounterPatient.PT_Profile_Picture ? await getImageUrl(encounterPatient.PT_Profile_Picture) : '',
		PT_HS_MRN: encounterPatient.PT_HS_MRN,
		PT_First_Name: patient.PT_First_Name,
		PT_Last_Name: patient.PT_Last_Name,
		PT_Home_Address_Line_1: patient.PT_Home_Address_Line_1 ? patient.PT_Home_Address_Line_1 : '',
		PT_Home_Address_Line_2: patient.PT_Home_Address_Line_2 ? patient.PT_Home_Address_Line_2 : '',
		PT_Home_City: patient.PT_Home_City ? patient.PT_Home_City : '',
		PT_Home_State: patient.PT_Home_State ? patient.PT_Home_State : '',
		PT_Home_Zip: patient.PT_Home_Zip ? patient.PT_Home_Zip.toString() : '',
		PT_Credit_Card_Type: card ? card.brand : '',
		PT_Credit_Card_Short_Number: card ? card.last4.toString() : '',
		Share_All_Chart: shareChart.length > 0 ? 'false' : 'true',
		title: encounter.Encounter_Type == 'New Visit' ? 'Encounter Rechedule' : 'Followup Rescheduled'
	}
	return jsonData
}
module.exports = router
