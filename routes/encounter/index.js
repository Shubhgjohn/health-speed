/* eslint-disable camelcase */
const express = require('express')
const app = express()
const model = require('../../models')
const createEncounterRoute = require('./create')
const upcomingVisitRoute = require('./gettodaysencounter')
const getEncounterById = require('./getencounterbyid')
const getDischargeInstruction = require('./getdischargeinstruction')
const updateEncounter = require('./updateencounter')
const updateEncounterAccess = require('./updateencounteraccress')
const submitDischargeInstruction = require('./submitdischargeinstrcution')
const rescheduleEncounter = require('./encounterreschedule')
const cancelEncounter = require('./cancelencounter')
const closeEncounter = require('./closeencounter')
const createFollowup = require('./createfollowup')
const patientHistorySuggestions = require('./getpatienthistorysuggestion')
const updatePatientHistory = require('./updatepatienthistory')
const message = require('./message')
const otherProviderEncounter = require('./getotherproviderencounter')
const viewEncounter = require('./viewencounter')

app.get('/remove-cancel-notification/:Encounter_UID', async (req, res) => {
  var { params: { Encounter_UID } } = req

  if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required' })

  await model.Encounter.updateEncounter(Encounter_UID, { Encounter_Status: 'Abandoned' })
  res.send({
    code: 1,
    status: 'sucess',
    message: 'Encounter close sucessfully'
  })
})

app.use(createEncounterRoute)
app.use(upcomingVisitRoute)
app.use(getEncounterById)
app.use(getDischargeInstruction)
app.use(updateEncounter)
app.use(updateEncounterAccess)
app.use(submitDischargeInstruction)
app.use(rescheduleEncounter)
app.use(cancelEncounter)
app.use(closeEncounter)
app.use(createFollowup)
app.use(patientHistorySuggestions)
app.use(updatePatientHistory)
app.use(message)
app.use(otherProviderEncounter)
app.use(viewEncounter)

module.exports = app
