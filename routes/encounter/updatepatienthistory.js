/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const model = require('../../models')
const errorModule = require('../../models').Errors
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const logger = require('../../models/errorlog')
const moment = require('moment')
const momentTz = require('moment-timezone')
const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_PRIVATE_KEY)

router.put('/update-history/:Encounter_UID', authorize(Role.Provider), async (req, res) => {
	var { body: { data }, params: { Encounter_UID } } = req
	if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required', Error_Code: errorModule.Encounter_UID_required() })
	if (!data) return res.send({ code: 0, status: 'failed', message: 'data is required', Error_Code: errorModule.data_required() })
	try {
		if (data.Pregnancy_History) {
			data.PT_Number_Of_Pregnancies = true
			data.PT_Number_Of_Deliveries = true
			data.Expected_Delivery_Date = true
			data.PT_Pregnancy_Complications = true
			data.PT_Pregnancy_Complications_Note = true
		}
		if (data.Oral_Contraceptives_Info) {
			data.Oral_Contraceptives = true
		}
		if (data.PT_Primary_Care_Physician_Toggle) {
			data.PT_Primary_Care_Provider = true
		}
		if (data.PT_Past_Year_Medical_Changes) {
			data.PT_Past_Year_Medical_Changes_Note = true
		}
		if (data.PT_Allergies_Toggle) {
			data.PT_Allergies = true
		}
		if (data.PT_Heart_Disease) {
			data.PT_Heart_Disease_Note = true
		}
		if (data.PT_Hypertension) {
			data.PT_Hypertension_Note = true
		}
		if (data.PT_Diabetes) {
			data.PT_Diabetes_Note = true
		}
		if (data.PT_Cancer) {
			data.PT_Cancer_Note = true
		}
		if (data.PT_Immune_Diseases) {
			data.PT_Immune_Diseases_Note = true
		}
		if (data.PT_Surgery) {
			data.PT_Surgery_Note = true
		}
		if (data.PT_Neuro_Issues) {
			data.PT_Neuro_Issues_Note = true
		}
		if (data.PT_Other_Diseases) {
			data.PT_Other_Diseases_Note = true
		}
		if (data.PT_Recreational_Drugs) {
			data.PT_Recreational_Drugs_Note = true
		}
		if (data.PT_Tobacco_Usage) {
			data.PT_Tobacco_Usage_Note = true
		}
		if (data.PT_Alcohol_Usage) {
			data.PT_Alcohol_Usage_Note = true
		}
		if (data.PT_Primary_Care_Physician_Toggle) {
			data.PT_Primary_Care_Provider_Toggle = true
		}
		if (data.PT_Medications_Toggle) {
			data.PT_Medication_Attachment = true
			data.PT_Medication = true
		}
		if (data.PT_Lab_and_Image_Reports_Toggle) {
			data.PT_Lab_and_Image_Reports = true
		}
		if (data.PT_Medical_Directives_Toggle) {
			data.PT_Medical_Directive_Attachment = true
			data.PT_Medical_History_Attachment = true
			data.PT_Medical_Directive = true
		}
		let encounter = await model.Encounter.getEncounterByEncounter_UID(Encounter_UID)
		let patientLatestHistory = await model.PatientHistory.findPatientHistoryById(data.PT_HS_MRN)
		if (encounter) {
			let json = {}
			var dataKeys = Object.keys(data)
			for (let a = 0; a < dataKeys.length; a++) {
				json[`PT_History.${dataKeys[a]}`] = patientLatestHistory[dataKeys[a]]
				if (a == dataKeys.length - 1) {
					await model.Encounter.updateEncounter(Encounter_UID, json)
					res.send({
						code: 1,
						status: 'sucess',
						message: 'Medical history update successfully'
					})
				}
			}
		} else {
			// await createAudit(req);
			res.send({ code: 0, status: 'failed', message: `No encounter find with ${Encounter_UID}`, Error_Code: errorModule.Encounter_not_found() })
		}
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS112' })
	}
})

router.put('/update-patient-history/:Encounter_UID', authorize(Role.Patient), async (req, res) => {
	var { body: { data }, params: { Encounter_UID } } = req
	if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required', Error_Code: errorModule.Encounter_UID_required() })
	if (!data) return res.send({ code: 0, status: 'failed', message: 'data is required', Error_Code: errorModule.data_required() })
	try {
		if (data.Pregnancy_History) {
			data.PT_Number_Of_Pregnancies = true
			data.PT_Number_Of_Deliveries = true
			data.Expected_Delivery_Date = true
			data.PT_Pregnancy_Complications = true
			data.PT_Pregnancy_Complications_Note = true
		}
		if (data.Oral_Contraceptives_Info) {
			data.Oral_Contraceptives = true
		}
		if (data.PT_Primary_Care_Physician_Toggle) {
			data.PT_Primary_Care_Provider = true
		}
		if (data.PT_Past_Year_Medical_Changes) {
			data.PT_Past_Year_Medical_Changes_Note = true
		}
		if (data.PT_Allergies_Toggle) {
			data.PT_Allergies = true
		}
		if (data.PT_Heart_Disease) {
			data.PT_Heart_Disease_Note = true
		}
		if (data.PT_Hypertension) {
			data.PT_Hypertension_Note = true
		}
		if (data.PT_Diabetes) {
			data.PT_Diabetes_Note = true
		}
		if (data.PT_Cancer) {
			data.PT_Cancer_Note = true
		}
		if (data.PT_Immune_Diseases) {
			data.PT_Immune_Diseases_Note = true
		}
		if (data.PT_Surgery) {
			data.PT_Surgery_Note = true
		}
		if (data.PT_Neuro_Issues) {
			data.PT_Neuro_Issues_Note = true
		}
		if (data.PT_Other_Diseases) {
			data.PT_Other_Diseases_Note = true
		}
		if (data.PT_Recreational_Drugs) {
			data.PT_Recreational_Drugs_Note = true
		}
		if (data.PT_Tobacco_Usage) {
			data.PT_Tobacco_Usage_Note = true
		}
		if (data.PT_Alcohol_Usage) {
			data.PT_Alcohol_Usage_Note = true
		}
		if (data.PT_Primary_Care_Physician_Toggle) {
			data.PT_Primary_Care_Provider_Toggle = true
		}
		if (data.PT_Medications_Toggle) {
			data.PT_Medication_Attachment = true
			data.PT_Medication = true
		}
		if (data.PT_Lab_and_Image_Reports_Toggle) {
			data.PT_Lab_and_Image_Reports = true
		}
		if (data.PT_Medical_Directives_Toggle) {
			data.PT_Medical_Directive_Attachment = true
			data.PT_Medical_History_Attachment = true
			data.PT_Medical_Directive = true
		}
		let encounter = await model.Encounter.getEncounterByEncounter_UID(Encounter_UID)
		if (encounter) {
			let json = {}
			var dataKeys = Object.keys(data)
			for (let a = 0; a < dataKeys.length; a++) {
				json[`${dataKeys[a]}`] = encounter.PT_History[dataKeys[a]]
				if (a == dataKeys.length - 1) {
					await model.PatientHistory.updateById(data.PT_HS_MRN, json)
					res.send({
						code: 1,
						status: 'sucess',
						message: 'History updated successfully'
					})
				}
			}
		} else {
			// await createAudit(req);
			res.send({ code: 0, status: 'failed', message: `No encounter find with ${Encounter_UID}`, Error_Code: errorModule.Encounter_not_found() })
		}
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS113' })
	}
})


router.put('/active-encounter', async (req, res) => {
	try {
		let filterEncounter = await model.Encounter.getScheduledEncounter()
		let count = 0
		if (filterEncounter.length == 0) return res.send({ code: 1, status: 'success', message: 'no scheduled encounter found.' })
		filterEncounter.forEach(async element => {
			if (Date.parse(moment(new Date(`${element.Encounter_Date} ${element.Encounter_Time}`)).format('YYYY-MM-DDTHH:mm:ss')) < Date.parse(momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm:ss'))) {
				await model.Encounter.updateEncounter(element.Encounter_UID, { Encounter_Status: 'Active', Updated_At: new Date() })
			}
			count = count + 1
			if (count == filterEncounter.length) return res.send({ code: 1, status: 'success', message: 'encounters change to Active status.' })
		})
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message })
	}
})

router.get('/payment-failure', async(req, res) => {
	try {
		let failurePaymentEncounter = await model.Transactions.getFailedTransaction()
		
		let filterEncounter = failurePaymentEncounter.filter(ele => (ele.Transaction_Status == 'Failed' && ele.Encounter_Transaction_Status == 'Unpaid') )
		let count = 0
		if (filterEncounter.length > 0 ) {
			filterEncounter.forEach(async ele => {
				let dateOneObj = new Date(ele.Created_At)
				let dateTwoObj = new Date()
				const milliseconds = Math.abs(dateTwoObj - dateOneObj);
				const hours = milliseconds / 36e5;	
					if (ele.Encounter_Payment_Failure_Mail == false && hours > 48) {
					const message = {
						from: process.env.ADMIN_FROM_EMAIL,
						to: ele.PT_Username,
						template_id: process.env.Notify_Patient_On_Payment_Failure,
						dynamic_template_data: {
							url: `${process.env.API_HOST}/patient/main/payment/${ele.Encounter_UID}/retry`
						}
					}
					await sgMail.send(message)
					let data = {
						Encounter_Payment_Failure_Mail : true
					}
					await model.Encounter.updateEncounter(ele.Encounter_UID, data)
					count = count + 1;
					if (count == filterEncounter.length) {
						res.send({ code : 1, status : 'sucess', message : 'email sent'})
					}
				} else {
					count = count + 1
					if (count == filterEncounter.length) {
						res.send({ code : 1, status : 'sucess', message : 'email sent'})
					}
				}
			})
		} else {
			res.send({ code : 1, status : 'sucess', message : 'No transaction found'})
		}
	} catch (error) {
	}
})

module.exports = router
