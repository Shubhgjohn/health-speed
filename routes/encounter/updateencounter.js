/* eslint-disable eqeqeq */
/* eslint-disable no-self-assign */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const moment = require('moment')
const model = require('../../models')
const chartModule = require('../../models').Chart
const errorModule = require('../../models').Errors
const patientModule = require('../../models').Patient
const providerModule = require('../../models').Provider
const encounterModule = require('../../models').Encounter
const notificationModule = require('../../models').Notification
const availabilityModule = require('../../models').Availability
const availabilityCheck = require('../../service/checkProviderAvailability')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const logger = require('../../models/errorlog')
const fcm = require('../../_helpers/fcm')
var isAvailable = true
var isConfirmed = true
let msg = 0

var AWS = require('aws-sdk')
const { mode } = require('crypto-js')
AWS.config.update({
	accessKeyId: process.env.AWS_ACCESS_KEY,
	secretAccessKey: process.env.AWS_SECRET_KEY,
	region: 'us-east-1'
})
var s3 = new AWS.S3()

router.put('/update-encounter/:Encounter_UID', authorize([Role.Provider]), async (req, res) => {
	var { body: { data }, params: { Encounter_UID } } = req
	if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required', Error_Code: errorModule.Encounter_UID_required() })
	if (!data) return res.send({ code: 0, status: 'failed', message: 'data is required', Error_Code: errorModule.data_required() })
	try {
		let encounter = await encounterModule.getEncounterByEncounter_UID(Encounter_UID)
		if (encounter) {
			if (data.PT_History) {
				if (data.PT_History == null) {
					data.PT_History = encounter.PT_History
				}
			}
			if (data.Encounter_Chief_Complaint_PRO) {
				let complaintData = data.Encounter_Chief_Complaint_PRO.split('-')
				data.Encounter_Chief_Complaint_PT = complaintData[0] ? complaintData[0] : encounter.Encounter_Chief_Complaint_PT
				data.Encounter_PT_Chief_Complaint_More = complaintData[1] ? complaintData[1] : encounter.Encounter_PT_Chief_Complaint_More
			}
			if (data.Encounter_Has_Followup == false) {
				let followupEncounter = await encounterModule.getEncounterFollowup(Encounter_UID)
				await resetEncounter(followupEncounter, res)
				await encounterModule.removeFollowupEncounter(Encounter_UID)
			}
			await encounterModule.updateEncounter(Encounter_UID, data)
			if (data.Encounter_Followup_Status && data.Encounter_Followup_Status == 'Cancelled') {
				let followupEncounter = await encounterModule.getEncounterFollowup(Encounter_UID)
				if (followupEncounter) {
					await encounterModule.removeFollowupEncounter(Encounter_UID)
				}
			}
			res.send({ code: 1, status: 'sucess', message: 'Encounter update successfully' })
		} else {
			res.send({ code: 0, status: 'failed', message: `No encounter find with ${Encounter_UID}`, Error_Code: errorModule.Encounter_not_found() })
		}
	} catch (error) {
		console.error(error)
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS100' })
	}
})

router.put('/confirm-encounter-followup/:Encounter_UID', authorize([Role.Patient, Role.Provider]), async (req, res) => {
	try {
		isConfirmed = true
		var { params: { Encounter_UID }, body: { data } } = req
		if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID required!', Error_Code: errorModule.Encounter_UID_required() })
		let encounter = await encounterModule.getEncounterByEncounter_UID(Encounter_UID)
		if (!encounter) return res.send({ code: 0, status: 'failed', message: `Encounter with UID '${Encounter_UID}' not found!`, Error_Code: errorModule.Encounter_not_found() })
		let patient = await patientModule.findOneByEmail(encounter.PT_Username)
		let provider = await providerModule.findById(encounter.PRO_UID)
		if (encounter.Encounter_Type == 'New Visit') {
			if (encounter.Encounter_Status == 'Requested') {
				await encounterModule.updateEncounter(Encounter_UID, { Encounter_Status: 'Scheduled' })
				await createChart(encounter)
				if (req.userType == 'Provider') {
					let jsonData = {
						Encounter_UID: Encounter_UID,
						PRO_UID: encounter.PRO_UID,
						title: 'Encounter Scheduled'
					}
					let PT_Token = await notificationModule.getToken('Patient', patient.PT_Username)
					let arr = []
					PT_Token.map(ele => arr.push(ele.Device_Token))
					let title = 'Your Visit Has Been Scheduled'
					let body = 'Review your information before your visit to make sure everything is up to date.'
					await sendNotification(arr, title, body, jsonData)
				}
				return res.send({ code: 1, status: 'success', message: 'encounter scheduled' })
			} else if ((encounter.Encounter_Status == 'Scheduled' || encounter.Encounter_Status == 'Cancelled') && encounter.Encounter_Reschedule_Status == 'Requested') {
				if (!data.Encounter_Reschedule_Status) return res.send({ code: 0, status: 'failed', message: 'Encounter_Reschedule_Status required!', Error_Code: errorModule.Encounter_Reschedule_Status_required() })
				if (data.Encounter_Reschedule_Status == 'Declined') {
					await encounterModule.updateEncounter(encounter.Encounter_UID, data)
					if (req.userType == 'Patient') {
						let jsonData = {
							Encounter_UID: Encounter_UID,
							title: 'Reschedule Declined'
						}
						let PRO_Token = await notificationModule.getToken('Provider', provider.PRO_Username)
						let arr = []
						PRO_Token.map(ele => arr.push(ele.Device_Token))
						let title = `Visit Reschedule Request Declined: ${moment(new Date(encounter.Reschedule_Request_Date)).format('M/D')}`
						let body = `${req.user.PT_First_Name} ${req.user.PT_Last_Name.charAt(0)} has declined your reschedule request.`
						await sendNotification(arr, title, body, jsonData)
					}
					return res.send({ code: 1, status: 'success', message: 'Rechedule Request Declined.' })
				}
				if (!data.Reschedule_Request_Date) return res.send({ code: 0, status: 'failed', message: 'Reschedule_Request_Date required!', Error_Code: errorModule.Reschedule_Request_Date_required() })
				if (!data.Reschedule_Request_Time) return res.send({ code: 0, status: 'failed', message: 'Reschedule_Request_Time required!', Error_Code: errorModule.Reschedule_Request_Time_required() })
				encounter.Reschedule_Request_Date = data.Reschedule_Request_Date
				encounter.Reschedule_Request_Time = data.Reschedule_Request_Time
				let SetData = await makeData(encounter)
				await setEncounter(SetData, provider, encounter, res)
				await encounterModule.updateEncounter(encounter.Encounter_UID, SetData)
				if (req.userType == 'Patient') {
					let jsonData = {
						Encounter_UID: encounter.Encounter_UID,
						PRO_UID: encounter.PRO_UID,
						title: 'Encounter Rechedule'
					}
					let arr = await getToken('Provider', provider.PRO_Username)
					let title = `Visit Rescheduled: ${moment(new Date(encounter.Encounter_Date)).format('M/D/YY')}`
					let body = 'Don’t forget to review this rescheduled visit.'
					await sendNotification(arr, title, body, jsonData)
				}
				return res.send({ code: 1, status: 'success', message: 'encounter successfully rescheduled.' })
			} else return res.send({ code: 0, status: 'failed', message: 'This request has no longer exist. Please contact HealthSpeed.', Error_Code: 'HS104' })
		} else {
			if (encounter.Encounter_Status == 'Requested') {
				if (!data.Encounter_Date) return res.send({ code: 0, status: 'failed', message: 'Encounter_Date is required!', Error_Code: errorModule.Encounter_Date_required() })
				if (!data.Encounter_Start_Time) return res.send({ code: 0, status: 'failed', message: 'Encounter_Start_Time is required', Error_Code: errorModule.Encounter_Start_Time_required() })
				if (!data.Encounter_End_Time) return res.send({ code: 0, status: 'failed', message: 'Encounter_End_Time is required', Error_Code: errorModule.Encounter_End_Time_required() })
				if (!data.Encounter_Start_Time_Zone) return res.send({ code: 0, status: 'failed', message: 'Encounter_Start_Time_Zone is required', Error_Code: errorModule.Encounter_Start_Time_Zone_required() })
				if (!data.Encounter_End_Time_Zone) return res.send({ code: 0, status: 'failed', message: 'Encounter_End_Time_Zone is required', Error_Code: errorModule.Encounter_End_Time_Zone_required() })
				let preDate = new Date(`${encounter.Encounter_Date} ${encounter.Encounter_Time}`)
				let newDate = new Date(`${data.Encounter_Date} ${data.Encounter_Start_Time} ${data.Encounter_Start_Time_Zone}`)
				if (preDate.getTime() != newDate.getTime()) {
					let confirmed = await setEncounter(data, provider, encounter, res)
					if (!isConfirmed) return res.send({ code: 0, status: 'failed', message: confirmed, Error_Code: msg == 1 ? errorModule.Encounter_End_Time_less_than_Encounter_Start_Time() : msg == 2 ? errorModule.You_can_not_set_encounter() : errorModule.No_availability_available() })
				}
				data.Encounter_Status = 'Scheduled'
				await encounterModule.updateEncounter(Encounter_UID, data)
				await encounterModule.updateEncounter(encounter.Parent_Encounter, { Encounter_Followup_Status: 'Scheduled' })
				await createChart(encounter)
				let jsonData = {
					Encounter_UID: Encounter_UID,
					PRO_UID: encounter.PRO_UID,
					title: 'Followup Scheduled'
				}
				if (req.userType == 'Patient') {
					let PRO_Token = await notificationModule.getToken('Provider', provider.PRO_Username)
					let arr = []
					PRO_Token.map(ele => arr.push(ele.Device_Token))
					let title = `Followup Scheduled: ${moment(new Date(encounter.Encounter_Date)).format('M/D')}`
					let body = `A followup has been scheduled with ${patient.PT_First_Name} ${patient.PT_Last_Name.charAt(0)} and added to your calendar.`
					await sendNotification(arr, title, body, jsonData)
					let PT_Token = await notificationModule.getToken('Patient', patient.PT_Username)
					arr = []
					PT_Token.map(ele => arr.push(ele.Device_Token))
					title = 'Followup Scheduled'
					body = `Get ready for your followup visit with Dr. ${provider.PRO_Last_Name}`
					await sendNotification(arr, title, body, jsonData)
				}
				return res.send({ code: 1, status: 'success', message: 'follow visit scheduled' })
			} else if ((encounter.Encounter_Status == 'Scheduled' || encounter.Encounter_Status == 'Cancelled') && encounter.Encounter_Reschedule_Status == 'Requested') {
				if (!data.Encounter_Reschedule_Status) return res.send({ code: 0, status: 'failed', message: 'Encounter_Reschedule_Status required!', Error_Code: errorModule.Encounter_Reschedule_Status_required() })
				if (data.Encounter_Reschedule_Status == 'Declined') {
					await encounterModule.updateEncounter(encounter.Encounter_UID, data)
					if (req.userType == 'Patient') {
						let jsonData = {
							Encounter_UID: Encounter_UID,
							title: 'Reschedule Declined'
						}
						let PRO_Token = await notificationModule.getToken('Provider', provider.PRO_Username)
						let arr = []
						PRO_Token.map(ele => arr.push(ele.Device_Token))
						let title = `Visit Reschedule Request Declined: ${moment(new Date(encounter.Reschedule_Request_Date)).format('M/D')}`
						let body = `${req.user.PT_First_Name} ${req.user.PT_Last_Name.charAt(0)} has declined your reschedule request.`
						await sendNotification(arr, title, body, jsonData)
					}
					return res.send({ code: 1, status: 'success', message: 'Rechedule Request Declined.' })
				}
				if (!data.Reschedule_Request_Date) return res.send({ code: 0, status: 'failed', message: 'Reschedule_Request_Date required!', Error_Code: errorModule.Reschedule_Request_Date_required() })
				if (!data.Reschedule_Request_Time) return res.send({ code: 0, status: 'failed', message: 'Reschedule_Request_Time required!', Error_Code: errorModule.Reschedule_Request_Time_required() })
				encounter.Reschedule_Request_Date = data.Reschedule_Request_Date
				encounter.Reschedule_Request_Time = data.Reschedule_Request_Time
				let SetData = await makeData(encounter)
				await setEncounter(SetData, provider, encounter, res)
				await encounterModule.updateEncounter(encounter.Encounter_UID, SetData)
				return res.send({ code: 1, status: 'success', message: 'followup successfully reschedule.' })
			} else return res.send({ code: 0, status: 'failed', message: 'This request has no longer exist. Please contact HealthSpeed.', Error_Code: 'HS104' })
		}
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS108' })
	}
})

router.put('/followup-time-out-encounter', async (req, res) => {
	try {
		let filterData = await encounterModule.getFollowupRequestedEncounter()
		if (filterData.length == 0) return 'no encounter to timed out.'
		let count = 0
		filterData.map(async ele => {
			let requestDay = moment(ele.Followup_Requested_Time).startOf('day').fromNow().split(' ')[0]
			if (requestDay > 7) {
				let childEncounter = await encounterModule.getEncounterFollowup(ele.Encounter_UID)
				await encounterModule.updateEncounter(childEncounter.Encounter_UID, { Encounter_Status: 'Cancelled' })
				await encounterModule.updateEncounter(ele.Encounter_UID, { Encounter_Followup_Status: 'Timed-out', Encounter_Has_Followup: false })
			}
			count = count + 1
			if (count == filterData.length) return res.send('timed out successfully.')
		})
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message })
	}
})

router.put('/reschedule-time-out-encounter', async (req, res) => {
	try {
		let filterData = await encounterModule.getEncounterRescheduleRequested()
		if (filterData.length == 0) return 'no encounter to timed out.'
		let count = 0
		filterData.map(async ele => {
			let requestDay = moment(ele.Reschedule_Requested_Time).startOf('day').fromNow().split(' ')[0]
			if (requestDay > 7) {
				await encounterModule.updateEncounter(ele.Encounter_UID, { Encounter_Reschedule_Status: 'Timed-out' })
			}
			count = count + 1
			if (count == filterData.length) return res.send('timed out successfully.')
		})
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message })
	}
})

router.put('/update-payment-method', authorize(Role.Patient), async (req, res) => {
	try {
		let data = req.body.data
		let count = 0
		data.map(async ele => {
			await encounterModule.updateEncounter(ele.Encounter_UID, { Payment_Card_Id: ele.Payment_Card_Id })
			count = count + 1
			if (count == data.length) return res.send({ code: 1, status: 'success', message: 'payment method chenged successfully.' })
		})
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS106' })
	}
})

router.put('/decline-encounter/:Encounter_UID', authorize([Role.Provider, Role.Patient]), async (req, res) => {
	let Encounter_UID = req.params.Encounter_UID
	try {
		if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID required!', Error_Code: errorModule.Encounter_UID_required() })
		let encounter = await encounterModule.getEncounterByEncounter_UID(Encounter_UID)
		if (!encounter) return res.send({ code: 0, status: 'failed', message: `encounter with UID '${Encounter_UID}' not found!`, Error_Code: errorModule.Encounter_not_found() })
		await resetEncounter(encounter, res)
		if (encounter.Encounter_Type == 'New Visit') {
			await encounterModule.updateEncounter(Encounter_UID, { Encounter_Status: 'Cancelled' })
			return res.send({ code: 1, status: 'success', message: 'Encounter Declined.' })
		} else {
			await encounterModule.updateEncounter(encounter.Parent_Encounter, { Encounter_Followup_Status: 'Declined', Encounter_Has_Followup: false })
			await encounterModule.updateEncounter(Encounter_UID, { Encounter_Status: 'Cancelled' })
			
			var provider = await model.Provider.findById(encounter.PRO_UID)
			var patient = await model.Patient.findOneByEmail(encounter.PT_Username)

			let jsonData = await NotificationData(encounter, patient, provider)
			let arr = []
			let token = await model.Notification.getToken('Provider', provider.PRO_Username)
			token.map(ele => arr.push(ele.Device_Token))

			// send notification to the provider
			if (arr.length != 0) {
				let title = `Followup visit cancelled: ${encounter.Encounter_Date}` 
				let body = `${patient.PT_First_Name} ${patient.PT_Last_Name ? patient.PT_Last_Name.charAt(0): ''} has cancelled followup`
				await sendNotification(arr, title, body, jsonData)
			}
			return res.send({ code: 1, status: 'success', message: 'Followup Declined.' })
		}
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS107' })
	}
})

function getAfterDiffTime (date, diff) {
	date.setTime(date.getTime() + (diff * 60 * 60 * 1000))
	return date
}

async function getToken (role, email) {
	let token = await notificationModule.getToken(role, email)
	let arr = []
	token.map(ele => arr.push(ele.Device_Token))
	return arr
}

async function makeData (encounter) {
	let diff = Math.abs(new Date(`${encounter.Encounter_Date} ${encounter.Encounter_Start_Time} ${encounter.Encounter_Start_Time_Zone}`) - new Date(`${encounter.Encounter_Date} ${encounter.Encounter_End_Time} ${encounter.Encounter_End_Time_Zone}`)) / 36e5
	let Reschedule_End_Time = await getAfterDiffTime(new Date(`${encounter.Reschedule_Request_Date} ${encounter.Reschedule_Request_Time}`), diff)
	let data = {
		Encounter_Date: encounter.Reschedule_Request_Date,
		Encounter_Time: encounter.Reschedule_Request_Time,
		Encounter_Start_Time: encounter.Reschedule_Request_Time.split(' ')[0],
		Encounter_Start_Time_Zone: encounter.Reschedule_Request_Time.split(' ')[1].toLowerCase(),
		Encounter_End_Time: moment(Reschedule_End_Time).format('LT').split(' ')[0],
		Encounter_End_Time_Zone: moment(Reschedule_End_Time).format('LT').split(' ')[1].toLowerCase(),
		Encounter_Reschedule_Status: 'Scheduled',
		Encounter_Status: 'Scheduled',
		First_Notification: false,
		Second_Notification: false,
		PMH_Notification: false
	}
	return data
}

async function createChart (encounter) {
	let data = {
		PT_HS_MRN: encounter.PT_HS_MRN,
		PRO_Chart_Access: { PRO_UID: encounter.PRO_UID },
		PRO_UID: encounter.PRO_UID,
		Encounter_UID: encounter.Encounter_UID
	}
	await chartModule.create(data)
	let unSignedDoc = await chartModule.getProviderUnsignedDocCount(encounter.PRO_UID)
	await providerModule.updateById(encounter.PRO_UID, { PRO_Number_of_Unsigned_Docs: unSignedDoc })
}

async function sendNotification (arr, title, body, jsonData) {
	if (arr.length != 0) {
		var payload = { // this may vary according to the message type (single recipient, multicast, topic, et cetera)
			registration_ids: arr,
			notification: {
				title: title,
				body: body
			},

			data: jsonData
		}
		await fcm.send(payload, function (err, response) {
			if (err) {
			} else console.log(response)
		})
	}
}

var checkAvailability = async (availabilityArray, ignoreIndex) => {
	var count = 0
	availabilityArray.map(ele => {
		if (ele.Is_Travel_Time || (ele.Is_Home_Available == true && ele.Is_Office_Available == true)) {
			count = count + 1
			if (count == availabilityArray.length) {
				return true
			}
		} else {
			isAvailable = false
			count = count + 1
			if (count == availabilityArray.length) {
				return true
			}
		}
	})
}

async function resetEncounter (encounter, res) {
	const availability = await availabilityModule.getProviderAvailability(encounter.PRO_UID, encounter.Encounter_Date)
	const providerAvailability = await model.ProviderAvailability.getProviderAvailability(encounter.PRO_UID, encounter.Encounter_Date)
	if (availability) {
		var Start_Time = moment(`${encounter.Encounter_Start_Time} ${encounter.Encounter_Start_Time_Zone}`, 'hh:mm A').format('HH.mm')
		var End_Time = moment(`${encounter.Encounter_End_Time} ${encounter.Encounter_End_Time_Zone}`, 'hh:mm A').format('HH.mm')
		Start_Time = Start_Time.includes(30) ? parseFloat(Start_Time) + 0.20 : parseFloat(Start_Time)
		End_Time = End_Time.includes(30) ? parseFloat(End_Time) + 0.20 : parseFloat(End_Time)
		var sIndex = await getSlotsTime(availability.PRO_Slots, Start_Time)
		var eIndex = await getSlotsTime(availability.PRO_Slots, End_Time)
		var count = 0
		var temp = 0
		if (sIndex != 0) {
			for (let i = sIndex - 1; ; i--) {
				if (availability.PRO_Slots[i].Is_Travel_Time == false) {
					if (availability.PRO_Slots[i].Is_Home_Available == true && availability.PRO_Slots[i].Is_Office_Available == true) {
						sIndex = i + 1
						break
					} else {
						sIndex = sIndex
						break
					}
				}
				if (i == 0) break
				count = count + 1
			}
		}
		if (eIndex != 47) {
			for (let i = eIndex; ; i++) {
				if (availability.PRO_Slots[i].Is_Travel_Time == false) {
					if (availability.PRO_Slots[i].Is_Home_Available == true && availability.PRO_Slots[i].Is_Office_Available == true) {
						eIndex = i - 1
						break
					} else {
						if (count == temp) {
							eIndex = eIndex - 1
						} else {
							eIndex = eIndex + count > 0 ? (count - 1) : count
						}
						break
					}
				}
				if (i == 47) break
				temp = temp + 1
			}
		}
		for (var i = sIndex; i <= eIndex; i++) {
			availability.PRO_Slots[i].Is_Home_Available = true
			availability.PRO_Slots[i].Is_Office_Available = true
			availability.PRO_Slots[i].Is_Travel_Time = false
			providerAvailability.PRO_Slots[i].Is_Office_Available = true
			providerAvailability.PRO_Slots[i].Is_Home_Available = true
			// encounter.Encounter_Location_Type == 'Office' ? providerAvailability.PRO_Slots[i].Is_Office_Available = true : providerAvailability.PRO_Slots[i].Is_Home_Available = true
		}
		await model.Availability.updateAvailability(encounter.PRO_UID, availability)
		await model.ProviderAvailability.updateAvailability(encounter.PRO_UID, providerAvailability.PRO_Availability_Date, providerAvailability.PRO_Slots)
	}
}

function getSlotsTime (arr, time) {
	var index = -1
	arr.find((ele, i) => {
		if (ele.Slot_Format == time) {
			index = i
			return i
		}
	})
	return index
}

async function setEncounter (data, provider, encounter, res) {
	data.Encounter_Time = data.Encounter_Start_Time + ' ' + data.Encounter_Start_Time_Zone
	var travelTimeSlot = provider.PRO_Home_Visit_Buffer / 60
	var sTime, eTime
	var availability = await model.Availability.getProviderAvailability(provider.PRO_UID, data.Encounter_Date)
	var providerAvailability = await model.ProviderAvailability.getProviderAvailability(encounter.PRO_UID, data.Encounter_Date)
	if (availability) {
		await resetEncounter(encounter, res)
		availability = await model.Availability.getProviderAvailability(provider.PRO_UID, data.Encounter_Date)
		providerAvailability = await model.ProviderAvailability.getProviderAvailability(encounter.PRO_UID, data.Encounter_Date)
		var Start_Time = moment(`${data.Encounter_Start_Time} ${data.Encounter_Start_Time_Zone}`, 'hh:mm A').format('HH.mm')
		var End_Time = moment(`${data.Encounter_End_Time} ${data.Encounter_End_Time_Zone}`, 'hh:mm A').format('HH.mm')

		Start_Time = Start_Time.includes(30) ? parseFloat(Start_Time) + 0.20 : parseFloat(Start_Time)
		End_Time = End_Time.includes(30) ? parseFloat(End_Time) + 0.20 : parseFloat(End_Time)
		if (encounter.Encounter_Location_Type == availability.PRO_Availability_Type) {
			sTime = Start_Time
			eTime = End_Time
		} else {
			sTime = Start_Time - travelTimeSlot
			eTime = End_Time + travelTimeSlot
		}
		if (End_Time < Start_Time) {
			isConfirmed = false
			msg = 1
			return 'Encounter_End_Time cannot less than Encounter_Start_Time'
		}
		var arr = availability.PRO_Slots.filter(ele => ((ele.Slot_Format >= sTime) && (ele.Slot_Format < eTime)))
		var filterProviderAvailability = providerAvailability.PRO_Slots.filter(ele => ((ele.Slot_Format >= sTime) && (ele.Slot_Format < eTime)))
		await availabilityCheck.checkProviderAvailability(filterProviderAvailability, encounter.Encounter_Location_Type, isAvailable)
		await checkAvailability(arr, travelTimeSlot * 2)
		if (isAvailable) {
			var newarr = availability.PRO_Slots.map(element => ({
				Is_Home_Available: ((element.Slot_Format == sTime || (element.Slot_Format >= sTime && element.Slot_Format < eTime))) ? false : element.Is_Home_Available,
				Is_Office_Available: (element.Slot_Format == sTime || (element.Slot_Format >= sTime && element.Slot_Format < eTime)) ? false : element.Is_Office_Available,
				Is_Travel_Time: (element.Slot_Format >= sTime && element.Slot_Format < Start_Time) || (element.Slot_Format >= End_Time && element.Slot_Format < eTime) ? true : element.Is_Travel_Time,
				_id: element._id,
				Slot_Time: element.Slot_Time,
				Slot_Time_Zone: element.Slot_Time_Zone,
				Slot_Format: element.Slot_Format
			}))
			var newProviderAvailabilityArray = providerAvailability.PRO_Slots.map(element => ({
				Is_Home_Available: ((element.Slot_Format == sTime || (element.Slot_Format >= sTime && element.Slot_Format < eTime))) ? false : element.Is_Home_Available,
				Is_Office_Available: (element.Slot_Format == sTime || (element.Slot_Format >= sTime && element.Slot_Format < eTime)) ? false : element.Is_Office_Available,
				_id: element._id,
				Slot_Time: element.Slot_Time,
				Slot_Time_Zone: element.Slot_Time_Zone,
				Slot_Format: element.Slot_Format,
				Is_Home_Availability: element.Is_Home_Availability,
				Is_Office_Availability: element.Is_Office_Availability
			}))
			await model.Availability.updateAvailabilityByTime(provider.PRO_UID, data.Encounter_Date, newarr)
			await model.ProviderAvailability.updateAvailability(encounter.PRO_UID, data.Encounter_Date, newProviderAvailabilityArray)
			isAvailable = true
		} else {
			isAvailable = true
			isConfirmed = false
			msg = 2
			return `You can't set encounter between ${data.Encounter_Start_Time} and ${data.Encounter_End_Time}`
		}
	} else {
		isAvailable = true
		isConfirmed = false
		msg = 3
		return `No availability set for ${data.Encounter_Date} of Dr. ${provider.PRO_Last_Name}`
	}
}


// Function for get the notification data
async function NotificationData (encounter, patient, provider, data) {
	let encounterPatient = await model.Patient.findById(encounter.PT_HS_MRN)
	let jsonData = {
		PT_Profile_Picture: encounterPatient.PT_Profile_Picture ? await getImageUrl(encounterPatient.PT_Profile_Picture) : '',
		PRO_Profile_Picture: provider.PRO_Profile_Picture ? await getImageUrl(provider.PRO_Profile_Picture) : '',
		PT_HS_MRN: encounterPatient.PT_HS_MRN,
		PT_First_Name: encounterPatient.PT_First_Name,
		PT_Last_Name: encounterPatient.PT_Last_Name,
		PT_Gender: encounterPatient.PT_Gender,
		PT_Age_Now: (new Date().getFullYear() - encounterPatient.PT_DOB.getFullYear()).toString(),
		PRO_Last_Name: provider.PRO_Last_Name,
		PRO_UID: provider.PRO_UID,
		PRO_Gender: provider.PRO_Gender,
		Encounter_UID: encounter.Encounter_UID,
		Encounter_Date: encounter.Encounter_Date,
		Encounter_Time: encounter.Encounter_Time,
		Encounter_Type: encounter.Encounter_Type,
		Encounter_Chief_Complaint_PRO: encounter.Encounter_Chief_Complaint_PRO ? encounter.Encounter_Chief_Complaint_PRO : '',
		Encounter_Chief_Complaint_PT: encounter.Encounter_Chief_Complaint_PT ? encounter.Encounter_Chief_Complaint_PT : '',
		Encounter_PT_Chief_Complaint_More: encounter.Encounter_PT_Chief_Complaint_More ? encounter.Encounter_PT_Chief_Complaint_More : '',
		title: `Encounter_Cancelled ${encounter.Encounter_Date}`
	}
	return jsonData
}
// Function for the get image url from the image key
function getImageUrl (imageKey) {
	if (imageKey == null || imageKey == '') {
		return ''
	} else {
		return new Promise((resolve, reject) => {
			const url = s3.getSignedUrl('getObject', {
				Bucket: process.env.AWS_BUCKET,
				Key: imageKey,
				Expires: 60 * 60 * 24 * 15
			})
			resolve(url)
		})
	}
}
module.exports = router
