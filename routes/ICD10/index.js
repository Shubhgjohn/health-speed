/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const model = require('../../models')
const { auditUid } = require('../../utils/UID')
var auditType = require('../../auditType.json')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
var multer = require('multer')
const fs = require('fs')
var upload = multer({ dest: 'uploads/' })
const csv = require('csv-parser')

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './')
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname)
  }
})
upload = multer({ storage: storage })
router.post('/add', async (req, res) => {
  const { PRO_Diagnosis_ICD10_Code, PRO_Diagnosis_ICD10_Code_Order } = req.body
  if (!PRO_Diagnosis_ICD10_Code) return res.send({ code: 0, status: 'failed', message: 'PRO_Diagnosis_ICD10_Code is required ' })
  if (!PRO_Diagnosis_ICD10_Code_Order) return res.send({ code: 0, status: 'failed', message: 'PRO_Diagnosis_ICD10_Code_Order is required ' })

  await model.ICD10.create({ PRO_Diagnosis_ICD10_Code, PRO_Diagnosis_ICD10_Code_Order })

  return res.send({ code: 1, status: 'success', message: 'added success' })
})
router.get('/get/:searchText?', authorize([Role.Provider, Role.Admin]), async (req, res) => {
  if (!req.params.searchText) {
    return res.send({ code: 1, status: 'success', data: [] })
  }
  let ICD10List = await model.ICD10.getICDCode(req.params.searchText)
  var auditData = {
    Audit_UID: await auditUid(),
    Audit_Type: auditType.view,
    Audit_Info: 'New- ' + await auditUid(),
    Data_Point: 'PRO/Get ICD10 list',
    IP_Address: await model.Audit.getIpAddress(req)
  }
  await model.Audit.create(auditData)
  return res.send({ code: 1, status: 'success', data: ICD10List })
})

router.get('/add-new-codes', upload.single('ICD'), async (req, res) => {
  let data = await readFile()
  await model.ICD10.insert(data)
  fs.unlink('ICD10.csv', err => {
    if (err) throw err
    return res.send({ code: 1, status: 'success', data: 'ICD added sucessfully' })
  })
})

router.get('/get-new-codes', async (req, res) => {
  let data = await model.NewICD.find()
  return res.send({ code: 1, status: 'success', data: data })
})
module.exports = router

let readFile = async () => {
  let result = []
  let count = 0
  return new Promise((resolve, reject) => {
    fs.createReadStream('ICD10.csv')
      .pipe(csv())
      .on('data', function (row) {
        let data = {
          PRO_Diagnosis_ICD10_Code: row.CODE,
          PRO_Diagnosis_ICD10_Code_Order: count,
          PRO_Diagnosis_ICD10_Code_Description: row.DESCRIPTION
        }
        result.push(data)
        count = count + 1
      })
      .on('end', function () {
        resolve(result)
      })
  })
}
