/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
/* eslint-disable prefer-const */
var Router = require('express').Router
var router = new Router()
var sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_PRIVATE_KEY)
const model = require('../../models')
var { auditUid } = require('../../utils/UID')
var auditType = require('../../auditType.json')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const { isAValidPhoneNumber } = require('../../utils/validate')
const moment = require('moment')
const logger = require('../../models/errorlog')
const _ = require('lodash')

// Get patient list with pagination and search funcationality //
router.post('/get-patient', authorize(Role.Admin), async (req, res) => {
  try {
    const data = req.body
    // get all patient from the system
    let patientData = await model.Patient.getPatientList(data)
    patientData.result.map(ele => {
      if (ele.PT_DOB) {
        var sdt = ele.PT_DOB
        var difdt = new Date(new Date() - sdt)
        ele.PT_Age_Now = ((difdt.toISOString().slice(0, 4) - 1970) + 'y ' + (difdt.getMonth()) + 'm')
      } else {
        ele.PT_Age_Now = 0
      }
      ele.lastActive = ele.lastActive ? ele.lastActive : null
    })
    return res.status(200).send({ code: 200, status: 'success', data: patientData })
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.status(422).send({ code: 422, status: 'failed', message: error.message })
  }
})
// Export patient csv //
router.post('/export-patient-csv', authorize(Role.Admin), async (req, res) => {
  let data = req.body
  const exportData = await model.Patient.exportPatientCSV(data)

  exportData.map(ele => {
    if (ele.PT_DOB) {
      var sdt = ele.PT_DOB
      var difdt = new Date(new Date() - sdt)
      ele.PT_Age_Now = ((difdt.toISOString().slice(0, 4) - 1970) + 'y ' + (difdt.getMonth()) + 'm')
    } else {
      ele.PT_Age_Now = 0
    }
    ele.lastActive = ele.lastActive ? ele.lastActive : null
  })
  res.status(200).send({ code: 200, status: 'success', data: exportData })
})
// get patient by patient id //
router.get('/get-patient/:id', authorize(Role.Admin), async (req, res) => {
  const id = req.params.id
  if (!id) return res.status(422).send({ code: 422, status: 'failed', message: `patient id is not provided! it's required to get patient details` })
  try {
    const patient = await model.Patient.getPatientById(id)

    if (patient.length == 0) return res.status(422).send({ code: 422, status: 'failed', message: `patient with id '${id}' is not in our system!` })
    
    if (patient[0].PT_Profile_Picture) {
      let url = await model.CommonServices.getImage(patient[0].PT_Profile_Picture)
      patient[0].PT_Profile_Picture = url
    }
    if (patient[0].PT_Govt_ID_Front) {
      let url = await model.CommonServices.getImage(patient[0].PT_Govt_ID_Front)
      patient[0].PT_Govt_ID_Front = url
    }
    if (patient[0].PT_Govt_ID_Back) {
      let url = await model.CommonServices.getImage(patient[0].PT_Govt_ID_Back)
      patient[0].PT_Govt_ID_Back = url
    }
    return res.status(200).send({ code: 200, status: 'success', data: patient[0] })
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.status(422).send({ code: 422, status: 'failed', message: error.message })
  }
})
// update patient by admin by id //
router.put('/update-patient/:id', authorize(Role.Admin), async (req, res) => {
  var data = req.body
  var id = req.params.id
  data.PT_Age_Now = (new Date().getFullYear() - new Date(data.PT_DOB).getFullYear())

  if (data.PT_Is_Minor == 'No') data.PT_Username = data.PT_Username.toLowerCase()
  if (!id) return res.status(422).send({ code: 422, status: 'failed', message: 'patient id is not provided!it\'s required to update patient!' })
  if (!data.PT_First_Name) return res.status(422).send({ code: 422, status: 'failed', message: 'Don’t forget to enter your first name!' })
  if (!data.PT_Last_Name) return res.status(422).send({ code: 422, status: 'failed', message: 'Don’t forget to enter your first name! It’s required for your profile.' })
  if (!data.PT_DOB) return res.status(422).send({ code: 422, status: 'failed', message: '*Don’t forget to enter your birth date.' })
  
  if (data.PT_Is_Minor == 'No') {
    // check phone no is valid or not
    if (!data.PT_Cell_Phone) return res.status(422).send({ code: 422, status: 'failed', message: '*Don’t forget to enter your home phone number.' })
    data.PT_Cell_Phone = data.PT_Cell_Phone.replace(/\D/g, '')
    let isValid = isAValidPhoneNumber(data.PT_Cell_Phone)
    if (!isValid) return res.status(422).send({ code: 422, status: 'failed', message: 'Please make sure to enter a sequence of numbers. No letters or special characters are accepted.' })
    if (!data.PT_Username) return res.status(422).send({ code: 422, status: 'failed', message: 'Please provide a valid email address' })
    if (!data.PT_Home_Address_Line_1) return res.status(422).send({ code: 422, status: 'failed', message: '*Don’t forget to fill in your home address! It’s required for your profile.' })
    if (!data.PT_Home_City) return res.status(422).send({ code: 422, status: 'failed', message: '*Don’t forget to fill in your home city! It’s required for your profile.' })
    if (!data.PT_Home_Zip) return res.status(422).send({ code: 422, status: 'failed', message: '*Don’t forget to fill in your home zip code! It’s required for your profile.' })
    if (!data.PT_Home_State) return res.status(422).send({ code: 422, status: 'failed', message: '*Don’t forget to fill in your home state! It’s required for your profile.' })
  }
  if (!data.PT_Gender) return res.status(422).send({ code: 422, status: 'failed', message: '*Don’t forget to pick your gender.' })
  if (!data.PT_Height) return res.status(422).send({ code: 422, status: 'failed', message: '*Please enter your height.' })
  if (!data.PT_Weight) return res.status(422).send({ code: 422, status: 'failed', message: '*Please enter your weight.' })

  try {
    // find patient by id
    const patientById = await model.Patient.findById(id)
    const patientByEmail = await model.Patient.findOneByEmail(data.PT_Username)
    if (!patientById) return res.status(422).send({ code: 422, status: 'failed', message: `patient with id '${id}' is not in our system!` })
    patientById.PT_Password = undefined
    if (!patientByEmail) {
      await model.Patient.updatePatientById(id, data)
      await createAudit(req, patientById, data)
    } else {
      if (patientById.PT_Username != patientByEmail.PT_Username) return res.status(422).send({ code: 422, status: 'failed', message: '*This username is already associated with another account. Please log in.' })
      await model.Patient.updatePatientById(id, data)
      await createAudit(req, patientById, data)
    }
    if (patientById.PT_Is_Minor == 'No') {
      let minorData = {
        PT_Cell_Phone: data.PT_Cell_Phone ? data.PT_Cell_Phone : null,
        PT_Home_Address_Line_1: data.PT_Home_Address_Line_1 ? data.PT_Home_Address_Line_1 : null,
        PT_Home_Address_Line_2: data.PT_Home_Address_Line_2 ? data.PT_Home_Address_Line_2 : null,
        PT_Home_City: data.PT_Home_City ? data.PT_Home_City : null,
        PT_Home_Zip: data.PT_Home_Zip ? data.PT_Home_Zip : null,
        PT_Home_State: data.PT_Home_State ? data.PT_Home_State : null,
        PT_Home_Address_Latitude: data.PT_Home_Address_Latitude ? data.PT_Home_Address_Latitude : null,
        PT_Home_Address_Longitude: data.PT_Home_Address_Longitude ? data.PT_Home_Address_Longitude : null
      }
      await model.Patient.updateMinorById(patientById.PT_HS_MRN, minorData)
    }
    res.status(200).send({ code: 200, status: 'success', message: 'patient updated successfully!' })
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.status(422).send({ code: 422, status: 'failed', message: error.message })
  }
})
// Get patient encounter by id//
router.post('/get-patient-encounter/:id', authorize(Role.Admin), async (req, res) => {
  var data = req.body
  data.PT_HSID = req.params.id
  if (!data.PT_HSID) return res.status(422).send({ code: 422, status: 'failed', message: 'patient user id is required to get encounter!!' })
  const patient = await model.Patient.findById(data.PT_HSID)
  if (!patient) return res.status(422).send({ code: 422, status: 'failed', message: `patient with id '${data.PT_HSID}' is not in our system!!` })
  data.PT_Username = patient.PT_Username
  try {
    // get patient all encounter
    const encounter = await model.Encounter.getPatientEncounterByEmail(data)
    if (data.order == 'asc') encounter.result.sort(compareAsc)
    else encounter.result.sort(compareDsc)

    encounter.result = _(encounter.result).slice(data.skip ? data.skip : 0).take(data.limit ? data.limit : 10).value()
    const auditData = {
      Audit_UID: await auditUid(),
      Audit_Type: auditType.view,
      Audit_Info: 'New- ' + await auditUid(),
      Data_Point: 'AP/Get patient encounter',
      IP_Address: await model.Audit.getIpAddress(req)
    }
    await model.Audit.create(auditData)
    if (encounter.total == 0) return res.status(200).send({ code: 200, status: 'success', data: encounter })
    var encounterData = encounter.result.map(ele => ({
      Encounter_Date: moment(moment(new Date(ele.Encounter_Date)).format('YYYY-MM-DD') + ele.Encounter_Time, 'YYYY-MM-DDLT').format('MM/DD/YYYY hh:mma'),
      Encounter_Location_Type: ele.Encounter_Location_Type,
      PRO_First_Name: ele.patient_encounter.length > 0 ? ele.patient_encounter[0].PRO_First_Name ? ele.patient_encounter[0].PRO_First_Name : '' : '',
      PRO_Last_Name: ele.patient_encounter.length > 0 ? ele.patient_encounter[0].PRO_Last_Name ? ele.patient_encounter[0].PRO_Last_Name : '' : '',
      PRO_UID: ele.patient_encounter[0].PRO_UID,
      Encounter_UID: ele.Encounter_UID,
      Encounter_Chief_Complaint_PRO: ele.Encounter_Chief_Complaint_PRO,
      Chart_is_Signed: ele.chart.length > 0 ? ele.chart[0].Chart_is_Signed : false,
      Encounter_Status: ele.Encounter_Status == 'Cancelled' ? 'Cancelled' : ele.Encounter_Status == 'Abandoned' ? 'Abandoned': ele.Encounter_Status == 'Requested' ? 'Requested'  : ele.Encounter_Status == 'Scheduled' ? 'Scheduled' : new Date() < new Date(ele.Encounter_Date + ' ' + ele.Encounter_Time) ? 'Scheduled' : (ele.chart.length > 0 ? ele.chart[0].Chart_is_Signed : false) == true ? 'Signed' : ele.Encounter_Status == 'Active' ? 'Active' : moment(ele.Encounter_Time, 'hh:mm A').format('HH:mm') < moment(new Date()).format('HH:mm') < moment(ele.Encounter_End_Time + ' ' + ele.Encounter_End_Time_Zone, 'hh:mm A').format('HH:mm') ? 'Active' : (moment(new Date()).format('HH:mm') > moment(ele.Encounter_End_Time + ' ' + ele.Encounter_End_Time_Zone, 'hh:mm A').format('HH:mm') && (ele.chart.length > 0 ? ele.chart[0].Chart_is_Signed : false) == false) ? 'Unsigned' : 'Requested'
    }))
    encounter.result = encounterData
    res.status(200).send({ code: 200, status: 'success', data: encounter })
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.status(422).send({ code: 422, status: 'failed', message: error.message })
  }
})
// Get patient order by id //
router.post('/get-patient-order/:id', authorize(Role.Admin), async (req, res) => {
  try {
    let PT_HS_MRN = req.params.id
    var data = req.body
    if (!PT_HS_MRN) return res.status(422).send({ code: 422, status: 'failed', message: 'PT_HS_MRN is required!!' })

    // get patient from patient id
    const patient = await model.Patient.findById(PT_HS_MRN)
    if (!patient) return res.status(422).send({ code: 422, status: 'failed', message: `patient with id '${PT_HS_MRN}' is not in our system!!` })
    data.PT_Username = patient.PT_Username
    data.PT_HS_MRN = PT_HS_MRN
    
    // get patients all orders
    const patient_order = await model.Order.getPatientOrderByEmail(data)
    if (data.order == 'asc') patient_order.result.sort(orderCompareAsc)
    else patient_order.result.sort(orderCompareDsc)

    patient_order.result = _(patient_order.result).slice(data.skip ? data.skip : 0).take(data.limit ? data.limit : 10).value()
    const auditData = {
      Audit_UID: await auditUid(),
      Audit_Type: auditType.view,
      Audit_Info: 'New- ' + await auditUid(),
      Data_Point: 'AP/Get patient order',
      IP_Address: await model.Audit.getIpAddress(req)
    }
    await model.Audit.create(auditData)
    if (patient_order.total == 0) return res.status(200).send({ code: 200, status: 'success', data: patient_order })
    var order_data = patient_order.result.map(ele => ({
      Order_Created_Date: moment(moment(new Date(ele.Order_Created_Date)).format('YYYY-MM-DD') + ele.Order_Created_Time, 'YYYY-MM-DDLT').format('MM/DD/YYYY hh:mma'),
      Order_Type: ele.Order_Type,
      Order_Text: ele.Order_Text,
      Encounter_UID: ele.Encounter_UID,
      Order_Status: ele.Order_Status,
      PRO_First_Name: ele.provider[0].PRO_First_Name,
      PRO_Last_Name: ele.provider[0].PRO_Last_Name,
      PRO_UID: ele.provider[0].PRO_UID,
      Order_UID: ele.Order_UID
    }))
    patient_order.result = order_data
    res.status(200).send({ code: 200, status: 'success', data: patient_order })
  } catch (err) {
    return res.status(422).send({ code: 422, status: 'failed', message: err.message })
  }
})
// Create audit for updated patient //
async function createAudit (req, oldPatient, data) {
  const auditData = {
    Audit_UID: await auditUid(),
    Audit_Type: auditType.change,
    Audit_Info: 'New- ' + await auditUid(),
    Data_Point: 'AP/Update patient',
    PT_First_Name: data.PRO_First_Name,
    PT_Last_Name: data.PRO_Last_Name,
    PT_HS_MRN: oldPatient.PT_HS_MRN,
    New_Value: data,
    Old_Value: oldPatient,
    IP_Address: await model.Audit.getIpAddress(req)
  }
  await model.Audit.create(auditData)
}

// function for get data in decending order
function compareDsc (a, b) {
  let bTime = b.Encounter_Time.includes(' ') ? b.Encounter_Time : b.Encounter_Time.slice(0, -2) + ' ' + b.Encounter_Time.slice(-2)
  let aTime = a.Encounter_Time.includes(' ') ? a.Encounter_Time : a.Encounter_Time.slice(0, -2) + ' ' + a.Encounter_Time.slice(-2)
  if (new Date(b.Encounter_Date + ' ' + bTime) < new Date(a.Encounter_Date + ' ' + aTime)) {
    return -1
  }
  if (new Date(b.Encounter_Date + ' ' + bTime) > new Date(a.Encounter_Date + ' ' + aTime)) {
    return 1
  }
  return 0
}
// function for get data in ascending order
function compareAsc (a, b) {
  let bTime = b.Encounter_Time.includes(' ') ? b.Encounter_Time : b.Encounter_Time.slice(0, -2) + ' ' + b.Encounter_Time.slice(-2)
  let aTime = a.Encounter_Time.includes(' ') ? a.Encounter_Time : a.Encounter_Time.slice(0, -2) + ' ' + a.Encounter_Time.slice(-2)
  if (new Date(a.Encounter_Date + ' ' + aTime) < new Date(b.Encounter_Date + ' ' + bTime)) {
    return -1
  }
  if (new Date(a.Encounter_Date + ' ' + aTime) > new Date(b.Encounter_Date + ' ' + bTime)) {
    return 1
  }
  return 0
}

// function for get orders in decending order
function orderCompareDsc (a, b) {
  if (new Date(b.Order_Created_Date + ' ' + b.Order_Created_Time) < new Date(a.Order_Created_Date + ' ' + a.Order_Created_Time)) {
    return -1
  }
  if (new Date(b.Order_Created_Date + ' ' + b.Order_Created_Time) > new Date(a.Order_Created_Date + ' ' + a.Order_Created_Time)) {
    return 1
  }
  return 0
}

// function for get orders in ascending order
function orderCompareAsc (a, b) {
  if (new Date(a.Order_Created_Date + ' ' + a.Order_Created_Time) < new Date(b.Order_Created_Date + ' ' + b.Order_Created_Time)) {
    return -1
  }
  if (new Date(a.Order_Created_Date + ' ' + a.Order_Created_Time) > new Date(b.Order_Created_Date + ' ' + b.Order_Created_Time)) {
    return 1
  }
  return 0
}

module.exports = router
