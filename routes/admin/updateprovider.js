/* eslint-disable eqeqeq */
/* eslint-disable dot-notation */
/* eslint-disable prefer-const */
var Router = require('express').Router
var router = new Router()
var model = require('../../models')
var auditType = require('../../auditType.json')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
var moment = require('moment')
var { auditUid } = require('../../utils/UID')
var { isAValidPhoneNumber } = require('../../utils/validate')
const logger = require('../../models/errorlog')
const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_PRIVATE_KEY)

// Update provider by id from admin side //
router.put('/update-provider/:id', authorize(Role.Admin), async (req, res, next) => {
  const { body: data, params: { id } } = req
  if (data.PRO_Password) return res.send({ code: 422, status: 'failed', message: 'provider can not update password' })
  if (!id) return res.status(422).send({ code: 422, status: 'failed', message: `provider UID is not provided! it's required to update provider` })
  var dateValidation = /^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/

  if (data.PRO_DOB) {
    data.PRO_DOB = moment(data.PRO_DOB).format('MM/DD/YYYY')
    if (!dateValidation.test(data.PRO_DOB)) return res.status(422).send({ code: 422, status: 'failed', message: 'Please make sure to format your date of birth as MM/DD/YYYY.' })
  }
  if (data.PRO_Home_Phone) {
    data.PRO_Home_Phone = data.PRO_Home_Phone.replace(/\D/g, '')
    let validNumber = isAValidPhoneNumber(data.PRO_Home_Phone)
    if (!validNumber) return res.status(422).send({ code: 422, status: 'failed', message: 'Please make sure to enter a sequence of numbers. No letters or special characters are accepted.' })
  }
  if (data.PT_Age_Min && data.PT_Age_Max) {
    if (data.PT_Age_Min > data.PT_Age_Max) {
      return res.status(422).send({ code: 422, status: 'failed', message: 'Patient min age cannot greater than patient max age.' })
    }
  }
  if (data.PRO_Office_Phone) {
    data.PRO_Office_Phone = data.PRO_Office_Phone.replace(/\D/g, '')
    let validNumber = isAValidPhoneNumber(data.PRO_Office_Phone)
    if (!validNumber) return res.status(422).send({ code: 422, status: 'failed', message: 'Please make sure to enter a sequence of numbers. No letters or special characters are accepted.' })
  }
  if (data.PRO_Fax_Number) {
    data.PRO_Fax_Number = data.PRO_Fax_Number.replace(/\D/g, '')
    let validNumber = isAValidPhoneNumber(data.PRO_Fax_Number)
    if (!validNumber) return res.status(422).send({ code: 422, status: 'failed', message: 'Please make sure to enter a sequence of numbers. No letters or special characters are accepted.' })
  }
  if (data.PT_Age_Min) {
    if (data.PT_Age_Min % 1 != 0) return res.status(422).send({ code: 422, status: 'failed', message: '*Must be at whole number entered.' })
  }
  if (data.PT_Age_Max) {
    if (data.PT_Age_Max % 1 != 0) return res.status(422).send({ code: 422, status: 'failed', message: '*Must be at whole number entered.' })
  }
  if (data.PRO_Account_Status == 'Active') {
    data.PRO_Profile_Status = 'Complete'
  }
  try {

    // Get provider by id
    var provider = await model.Provider.findById(id)
    if (!provider) return res.status(422).send({ code: 422, status: 'failed', message: 'this is not a valid user!' })
    provider.PRO_Password = undefined
    if (data.PRO_Username) {
      var userProvider = await model.Provider.findOneByEmail(data.PRO_Username)
      if (!userProvider) {
        await updateData(provider, data, res, req)
      } else {
        if (provider.PRO_Username == userProvider.PRO_Username) {
          await updateData(provider, data, res, req)
        } else {
          return res.status(422).send({ code: 422, status: 'failed', message: '*This username is already associated with another account. Please log in.' })
        }
      }
    } else {
      await updateData(provider, data, res, req)
    }
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.status(422).send({ code: 422, status: 'failed', message: error.message })
  }
})

// function for update provider data  and create audid //
var updateData = async (provider, data, res, req) => {

  // update provider by id
  await model.Provider.updateById(provider.PRO_UID, data)
  const auditData = {
    Audit_UID: await auditUid(),
    Audit_Type: auditType.change,
    Data_Point: 'AP/Update Provider',
    PRO_First_Name: provider.PRO_First_Name,
    PRO_Last_Name: provider.PRO_Last_Name,
    PRO_UID: provider.PRO_UID,
    Audit_Info: 'New- ' + await auditUid(),
    Old_Value: provider,
    New_Value: data,
    IP_Address: await model.Audit.getIpAddress(req)
  }
  await model.Audit.create(auditData)
  return res.status(200).send({
    code: 1,
    status: 'sucess',
    message: 'provider update successfully'
  })
}

module.exports = router
