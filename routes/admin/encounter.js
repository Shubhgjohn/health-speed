/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const model = require('../../models')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const { auditUid } = require('../../utils/UID')
const auditType = require('../../auditType.json')
const fcm = require('../../_helpers/fcm')
const logger = require('../../models/errorlog')


// service for the update encounter from encounter id
router.put('/update-encounter/:Encounter_UID', authorize(Role.Admin), async (req, res) => {
  var { body: { data }, params: { Encounter_UID } } = req
  if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required' })
  if (!data) return res.send({ code: 0, status: 'failed', message: 'data is required' })
  try {
    // find requested encounter in the system
    let encounter = await model.Encounter.getEncounterByEncounter_UID(Encounter_UID)
    
    // if encounter is available in the system 
    if (encounter) {
      // update encounter
      await model.Encounter.updateEncounter(Encounter_UID, data)
      if (data.Encounter_Status == 'Abandoned' && encounter.Encounter_Status == 'Scheduled') {
        let jsonData = {
          Encounter_UID: Encounter_UID,
          PRO_UID: encounter.PRO_UID,
          title: 'Abandoned Encounter'
        }
        let arr = []
        let token = await model.Notification.getToken('Patient', encounter.PT_Username)
        token.map(ele => arr.push(ele.Device_Token))
        if (arr.length != 0) {
          var payload = { // this may vary according to the message type (single recipient, multicast, topic, et cetera)
            registration_ids: arr,
            notification: {
              title: 'Something happened',
              body: `There's a problem with your paperwork that we want to bring to your attention.`
            },
            data: jsonData
          }
          // send notification
          fcm.send(payload, function (err, response) {
            if (err) {
            } else console.log(response)
          })
        }
      }
      if (data.Encounter_Status == 'Active') await model.Chart.updateEncounterChartById(Encounter_UID, { Chart_is_Signed: false })
      await createAudit(req)
      res.status(200).send({ code: 200, status: 'success', message: 'Encounter update successfully' })
    } else {
      await createAudit(req)
      res.status(200).send({ code: 200, status: 'success', message: `No encounter find with ${Encounter_UID}` })
    }
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.status(422).send({ code: 422, status: 'failed', message: error.message })
  }
})


// function for create audits
let createAudit = async (req) => {
  const auditData = {
    Audit_UID: await auditUid(),
    Audit_Info: 'New- ' + await auditUid(),
    Data_Point: 'AP/Update Encounter ',
    Audit_Type: auditType.change,
    IP_Address: await model.Audit.getIpAddress(req)
  }
  await model.Audit.create(auditData)
}

module.exports = router
