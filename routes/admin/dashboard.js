const Router = require('express').Router
const router = new Router()
const model = require('../../models')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const logger = require('../../models/errorlog')

// service for admin dashboard //
router.get('/dashboard', authorize(Role.Admin), async (req, res) => {
  try {
    var response = []
    const date = new Date(new Date() - 1000 * 60 * 60 * 24 * 30)
    const provider = await model.Provider.getLastThirtyDaysActiveUser(date)
    const patient = await model.Patient.getLastThirtyDaysActiveUser(date)
    const encounter = await model.Encounter.getLastThirtyDaysEncounter(date)
    const transaction = await model.Transactions.getLastThirtyDaysTransaction(date)
    response.push({ provider: provider, patient: patient, encounter: encounter, transaction: transaction })
    
    return res.status(200).send({ code: 200, status: 'success', data: response[0] })
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.status(422).send({ code: 422, status: 'failed', message: error.message })
  }
})

module.exports = router
