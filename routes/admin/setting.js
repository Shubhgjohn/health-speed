/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
var Router = require('express').Router
var router = new Router()
var model = require('../../models')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
var { auditUid } = require('../../utils/UID')
var auditType = require('../../auditType.json')
const logger = require('../../models/errorlog')

// Service for admin setting //
router.post('/update-setting', authorize(Role.Admin), async (req, res) => {
  var data = req.body
  if (!data.HS_Address) return res.status(422).send({ code: 422, status: 'failed', message: '*HS_Address is required' })
  if (!data.HS_Support_Email) return res.status(422).send({ code: 422, status: 'failed', message: '*Please enter an email address' })
  if (!data.HS_Support_Number) return res.status(422).send({ code: 422, status: 'failed', message: '*Please enter a phone number' })
  if (data.HS_Notification_Emails.length == 0) return res.status(422).send({ code: 422, status: 'failed', message: '*Please enter at least one valid email address for notification emails to be sent to.' })
  if (!data.PRO_Office_Office_Hours_New_Visit_Rate_AVG) {
    return res.status(422).send({
      code: 422,
      status: 'failed',
      message: '*We need an Office New Visit During Office Hours average rate so the providers know how their rates compare to other providers.'
    })
  }
  if (!data.PRO_Office_Office_Hours_Followup_Rate_AVG) {
    return res.status(422).send({
      code: 422,
      status: 'failed',
      message: '*We need an Office Followup During Office Hours average rate so the providers know how their rates compare to other providers.'
    })
  }
  if (!data.PRO_Office_Off_Hours_New_Visit_Rate_AVG) {
    return res.status(422).send({
      code: 422,
      status: 'failed',
      message: '*We need an Office New Visit During Off Hours average rate so the providers know how their rates compare to other providers.'
    })
  }
  if (!data.PRO_Office_Off_Hours_Followup_Rate_AVG) {
    return res.status(422).send({
      code: 422,
      status: 'failed',
      message: '*We need an Office Followup Visit During Office Hours average rate so the providers know how their rates compare to other providers.'
    })
  }
  if (!data.PRO_Home_Office_Hours_New_Visit_Rate_AVG) {
    return res.status(422).send({
      code: 422,
      status: 'failed',
      message: '*We need a Home New Visit During Office Hours average rate so the providers know how their rates compare to other providers.'
    })
  }
  if (!data.PRO_Home_Office_Hours_Followup_Rate_AVG) {
    return res.status(422).send({
      code: 422,
      status: 'failed',
      message: '*We need a Home Followup During Office Hours average rate so the providers know how their rates compare to other providers.'
    })
  }
  if (!data.PRO_Home_Off_Hours_New_Visit_Rate_AVG) {
    return res.status(422).send({
      code: 422,
      status: 'failed',
      message: '*We need a Home New Visit During After Hours average rate so the providers know how their rates compare to other providers.'
    })
  }
  if (!data.PRO_Home_Off_Hours_Followup_Rate_AVG) {
    return res.status(422).send({
      code: 422,
      status: 'failed',
      message: '*We need a Home Followup During After Hours average rate so the providers know how their rates compare to other providers.'
    })
  }
  if (!data.PRO_Office_Office_Hours_New_Visit_Min) {
    return res.status(422).send({
      code: 422,
      status: 'failed',
      message: '*We need an Office New Visit During Office Hours minimum rate so the providers know how low their starting rates can be.'
    })
  }
  if (!data.PRO_Office_Office_Hours_Followup_Min) {
    return res.status(422).send({
      code: 422,
      status: 'failed',
      message: '*We need an Office Followup During Office Hours minimum rate so the providers know how low their starting rates can be.'
    })
  }
  if (!data.PRO_Office_Off_Hours_New_Visit_Min) {
    return res.status(422).send({
      code: 422,
      status: 'failed',
      message: '*We need an Office New Visit During Off Hours average rate so the providers know how low their starting rates can be.'
    })
  }
  if (!data.PRO_Office_Off_Hours_Followup_Min) {
    return res.status(422).send({
      code: 422,
      status: 'failed',
      message: '*We need an Office Followup Visit During Office Hours average rate so the providers know how low their starting rates can be.'
    })
  }
  if (!data.PRO_Home_Office_Hours_New_Visit_Min) {
    return res.status(422).send({
      code: 422,
      status: 'failed',
      message: '*We need a Home New Visit During Office Hours average rate so the providers know how low their starting rates can be.'
    })
  }
  if (!data.PRO_Home_Office_Hours_Followup_Min) {
    return res.status(422).send({
      code: 422,
      status: 'failed',
      message: '*We need a Home Followup During Office Hours average rate so the providers know how low their starting rates can be.'
    })
  }
  if (!data.PRO_Home_Off_Hours_New_Visit_Min) {
    return res.status(422).send({
      code: 422,
      status: 'failed',
      message: '*We need a Home New Visit During After Hours average rate so the providers know how low their starting rates can be.'
    })
  }
  if (!data.PRO_Home_Off_Hours_Followup_Min) {
    return res.status(422).send({
      code: 422,
      status: 'failed',
      message: '*We need a Home Followup During After Hours average rate so the providers know how low their starting rates can be.'
    })
  }
  if (!data.HS_911_Disclaimer) {
    return res.status(422).send({
      code: 422,
      status: 'failed',
      message: '*Make sure you let the HealthSpeed users know what they should do in case of an emergency.'
    })
  }
  try {
    var settingData = await model.Other.find()
    if (settingData.length == 0) {
      settingData = await model.Other.create(data)
      await createAudit(req, settingData)
      res.status(200).send({ code: 200, status: 'success', message: 'save other data successfull' })
    } else {
      await model.Other.updateById(settingData[0]._id, data)
      await createAudit(req, data, settingData)
      res.status(200).send({ code: 200, status: 'success', message: 'update other data successfull' })
    }
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.status(422).send({ code: 422, status: 'failed', message: error.message })
  }
})
// Get setting details //
router.get('/get-setting', authorize(Role.Admin), async (req, res) => {
  try {
    var settingData = await model.Other.find()
    if (settingData.length > 0) {
      let data = JSON.parse(JSON.stringify(settingData[0]))
      if (data.PT_Privacy_Policy_PDF_Name) {
        let url = await model.CommonServices.getImage(data.PT_Privacy_Policy_PDF_Name)
        data.PT_Privacy_Policy_PDF_Url = url
      }
      if (data.PRO_Privacy_Policy_PDF_Name) {
        let url = await model.CommonServices.getImage(data.PRO_Privacy_Policy_PDF_Name)
        data.PRO_Privacy_Policy_PDF_Url = url
      }
      if (data.PT_Terms_and_Conditions_PDF_Name) {
        let url = await model.CommonServices.getImage(data.PT_Terms_and_Conditions_PDF_Name)
        data.PT_Terms_and_Conditions_PDF_Url = url
      }
      if (data.PRO_Terms_and_Conditions_PDF_Name) {
        let url = await model.CommonServices.getImage(data.PRO_Terms_and_Conditions_PDF_Name)
        data.PRO_Terms_and_Conditions_PDF_Url = url
      }

      res.status(200).send({ code: 200, status: 'success', data: data })
    } else {
      res.status(200).send({ code: 200, status: 'success', data: {} })
    }
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.status(422).send({ code: 422, status: 'failed', message: error.message })
  }
})
// Create audit for update provider //
async function createAudit (req, newValue, oldValue) {
  var auditData = {
    Audit_UID: await auditUid(),
    Audit_Type: oldValue ? auditType.change : auditType.add,
    Audit_Info: 'New- ' + await auditUid(),
    Data_Point: oldValue ? 'AP/Update setting' : 'AP/New setting',
    New_Value: newValue,
    Old_Value: oldValue,
    IP_Address: await model.Audit.getIpAddress(req)
  }
  await model.Audit.create(auditData)
}

module.exports = router
