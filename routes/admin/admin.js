/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
var Router = require('express').Router
var router = new Router()
var model = require('../../models')
var { bcryptVerify, bcryptHash } = require('../../utils/crypto')
var { auditUid } = require('../../utils/UID')
var auditType = require('../../auditType.json')
var jwt = require('jsonwebtoken')
var sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_PRIVATE_KEY)
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const momentTz = require('moment-timezone')
const logger = require('../../models/errorlog')

// Service for login admin user //
router.post('/login', async (req, res) => {
	const loginData = req.body
	if (!loginData.AP_Username) return res.status(422).send({ code: 422, status: 'failure', message: '*Don’t forget to enter your email! It’s required to sign in.' })
	if (!loginData.AP_Password) return res.status(422).send({ code: 422, status: 'failure', message: '*Don’t forget to enter your password! It’s required to sign in.' })
	loginData.AP_Username = loginData.AP_Username.toLowerCase()
	try {
		//find admin in the system
		var admin = await model.Admin.findAdminByUsername(loginData.AP_Username)
		if (!admin) {
			return res.status(422).send({
				code: 422,
				status: 'failure',
				message: '*This email isn’t associated with an account. Please make sure you entered it correctly.',
				key: 'Email'
			})
		} else {
			if (admin.AP_Status == 'Active') {
				const verifyPassword = bcryptVerify(loginData.AP_Password, admin.AP_Password)
				// if password matches in the system
				if (verifyPassword) {
					await model.Admin.updateAgentLastLogin(admin.AP_Username)
					const auditData = {
						Audit_UID: await auditUid(),
						Audit_Type: auditType.login,
						Data_Point: 'AP/Login',
						Audit_Info: 'New- ' + await auditUid(),
						IP_Address: await model.Audit.getIpAddress(req)
					}
					await model.Audit.create(auditData)
					var payload = {
						AP_Id: admin._id,
						AP_Username: admin.AP_Username,
						AP_First_Name: admin.AP_First_Name,
						AP_Last_Name: admin.AP_Last_Name,
						roles: 'Admin'
					}
					// create token
					var token = jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: 60 * 60 * 24 })
					return res.status(200).send({
						code: 200,
						status: 'success',
						data: token
					})
				} else {
					return res.status(422).send({
						code: 422,
						status: 'failure',
						message: '*Looks like your password was entered incorrectly. Try again!',
						key: 'Password'
					})
				}
			} else {
				return res.status(422).send({ code: 422, status: 'failure', message: '*This email isn’t associated with an account. Please make sure you entered it correctly.' })
			}
		}
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.status(422).send({ code: 422, status: 'failed', message: error.message })
	}
})
// Service for request to password reset //
router.post('/forget-password', async (req, res) => {
	var AP_Username = req.body.AP_Username.toLowerCase()
	// find the admin in the system
	const admin = await model.Admin.findAdminByUsername(AP_Username)
	if (!AP_Username) return res.status(422).send({ code: 422, status: 'failure', message: '*Don’t forget to enter your email address! It’s required to reset your password.' })
	try {
		if (!admin) {
			var currentdate = new Date()
			var datetime = momentTz.tz(currentdate, process.env.CURRENT_TIMEZONE).format('MM/DD/YYYY @ HH:mm:ss')
			let message = {
				to: process.env.ADMIN_TO_EMAIL,
				from: process.env.ADMIN_FROM_EMAIL,
				template_id: process.env.ADMIN_RESET_ALERT_TEMPLATE_ID,
				dynamic_template_data: {
					datetime: datetime,
					AP_Username: AP_Username
				}
			}
			// send mail to admin for forgot password
			await sgMail.send(message)
			return res.status(422).send({
				code: 422,
				status: 'failure',
				message: '*Hmm… Please check this email address and try again.'
			})
		} else {
			var auditData = {
				Audit_UID: await auditUid(),
				Audit_Type: auditType.change,
				Data_Point: 'AP/Forget Password',
				Audit_Info: 'New- ' + await auditUid(),
				New_Value: AP_Username,
				IP_Address: await model.Audit.getIpAddress(req)
			}
			// create audit
			await model.Audit.create(auditData)
			var payload = {
				Admin_Id: admin._id
			}
			var token = jwt.sign(payload, admin.AP_Password, { expiresIn: 60 * 60 * 24 })
			let message = {
				to: AP_Username,
				from: process.env.ADMIN_FROM_EMAIL,
				subject: 'reset password',
				templateId: process.env.ADMIN_FORGET_PASSWORD_TEMPLET_ID,
				dynamic_template_data: {
					auth_token: `${process.env.ADMIN_PORTAL}/reset/${admin._id}/${token}`,
					AP_Username: admin.AP_Username,
					date: momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('MM/DD/YY'),
					time: momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('HH:mm:ss')
				}
			}
			await sgMail.send(message)
			res.status(200).send({ code: 200, status: 'success', message: 'We’ve sent you an email with instructions.' })
		}
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.status(422).send({ code: 422, status: 'failed', message: error.message })
	}
})
// Put service to reset admin password //
router.put('/reset-password/:id/:token', async (req, res) => {
	var reges = /(?=^.{8,}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&amp;*()_+}{&quot;:;'?/&gt;.&lt;,])(?!.*\s)(?=.*[$&+_,:;=?@#|'<>.^*()%!-]).*$/
	var AP_Password = req.body.AP_Password
	var id = req.params.id
	var token = req.params.token
	try {
		// find admin by admin id
		var admin = await model.Admin.findById(id)
		if (!admin) return res.status(422).send({ code: 422, status: 'failed', message: '*This id isn’t associated with an account. Please make sure you entered it correctly.' })
		if (!id) return res.status(422).send({ code: 422, status: 'failure', message: 'this is not a valid user.try again to reset your password' })
		if (!token) return res.status(422).send({ code: 422, status: 'failure', message: 'auth_token is not provided.try again to reset your password' })
		if (!AP_Password) return res.status(422).send({ code: 422, status: 'failure', message: '*Don’t forget to enter your password! It’s required to reset your password.' })
		jwt.verify(token, admin[0].AP_Password, async (err, decoded) => {
			if (err) return res.status(422).send({ code: 422, status: 'failure', message: 'This link has been expired or invalid.try again to reset password request.' })
			if (reges.test(AP_Password)) {
				var data = {
					AP_Password: await bcryptHash(AP_Password, 10),
					Updated_At: Date.now()
				}
				await model.Admin.updateAP_PasswordById(id, data)
				var auditData = {
					Audit_UID: await auditUid(),
					Audit_Type: auditType.change,
					Data_Point: 'AP/Reset Password',
					Audit_Info: 'New- ' + await auditUid(),
					IP_Address: await model.Audit.getIpAddress(req)
				}
				await model.Audit.create(auditData)
				res.status(200).send({ code: 200, status: 'success', message: 'password updated successfully!' })
			} else {
				return res.status(422).send({ code: 422, status: 'failure', message: '*Please make sure the password meets all of the criteria below.' })
			}
		})
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.status(422).send({ code: 422, status: 'failed', message: error.message })
	}
})
// Get service for check token validation before password reset //
router.get('/check-token-validation/:id/:token', async (req, res) => {
	var id = req.params.id
	var token = req.params.token
	if (!id) return res.status(422).send({ code: 422, status: 'failed', message: 'user id is not provided!' })
	if (!token) return res.status(422).send({ code: 422, status: 'failed', message: 'no token provided!' })

	try {
		var user = await model.Admin.findById(id)
		if (user.length == 0) return res.status(422).send({ code: 422, status: 'failed', message: 'invalid user!' })
		const auditData = {
			Audit_UID: await auditUid(),
			Audit_Type: auditType.confirm,
			Audit_Info: 'New- ' + await auditUid(),
			Data_Point: 'AP/check token validation',
			IP_Address: await model.Audit.getIpAddress(req)
		}
		await model.Audit.create(auditData)
		jwt.verify(token, user[0].AP_Password, function (err, decoded) {
			if (err) {
				return res.status(401).json({
					code: 401,
					status: 'failure',
					message: 'Unauthorized'
				})
			} else {
				return res.status(200).json({
					code: 200,
					status: 'success',
					message: 'Authorized'
				})
			}
		})
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.status(422).send({ code: 422, status: 'failed', message: error.message })
	}
})
// Service for get user list(!logged in) with search funcationality //
router.post('/get-userlist', authorize([Role.Admin]), async (req, res) => {
	var data = req.body
	try {
		// get all admin list
		var allAdmin = await model.Admin.getNotLoggedinAdmin(req.user.AP_Username, data)
		return res.status(200).send({
			code: 200,
			status: 'success',
			data: allAdmin
		})
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.status(422).send({ code: 422, status: 'failed', message: error.message })
	}
})
// Delete service for delete user by id //
router.delete('/delete-user/:id', authorize(Role.Admin), async (req, res) => {
	var id = req.params.id
	if (!id) return res.status(422).send({ code: 422, status: 'failed', message: 'no id is provided to delete user!' })
	// find admin from system
	var user = await model.Admin.findById(id)
	if (user.length != 0) {
		user[0].AP_Password = undefined
		await model.Admin.updateById(id, { is_Deleted: true, Deleted_At: new Date() })
		var auditData = {
			Audit_UID: await auditUid(),
			Audit_Type: auditType.remove,
			Data_Point: 'AP/Delete User',
			Audit_Info: 'New- ' + await auditUid(),
			Old_Value: user,
			IP_Address: await model.Audit.getIpAddress(req)
		}
		await model.Audit.create(auditData)
		return res.status(200).send({ code: 200, status: 'success', message: 'user deleted successfully!' })
	} else {
		return res.status(422).send({ code: 422, status: 'failed', message: 'invalid user!!!' })
	}
})

module.exports = router
