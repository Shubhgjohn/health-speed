/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const model = require('../../models')
const { auditUid } = require('../../utils/UID')
const auditType = require('../../auditType')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_PRIVATE_KEY)
const logger = require('../../models/errorlog')


// service for the cancel order from order id
router.put('/cancel-order/:id', authorize([Role.Admin, Role.Provider]), async (req, res) => {
  try {
    const order_id = req.params.id
    if (!order_id) return res.status(422).send({ code: 422, status: 'failed', message: 'ORDER ID is required!!' })
    
    // find order in the system with order id
    const orderData = await model.Order.getOrderById(order_id)
   
    // if order with this id is not available
    if (orderData.length == 0) return res.status(422).send({ code: 422, status: 'failed', message: `order id '${order_id}' is invalid!` })
    
    // update order
    await model.Order.updateOrderById(order_id, { Order_Review_Status: 'Cancelled', Order_Status: 'Cancelled' })
    
    // find patient in the system
    let patient = await model.Patient.findOneByEmail(orderData[0].PT_Username)
    
    // if patient is not present in the system
    if (!patient) return res.send({ code: 0, status: 'failed', message: 'No paient found for this order!' })
    if (patient.PT_Email_Notification == 'Enabled' && patient.PT_Account_Status == 'Active') {
      let message = {
        to: orderData[0].PT_Username,
        from: process.env.ADMIN_FROM_EMAIL,
        template_id: process.env.NOTIFY_PATIENT_ON_CANCEL_ORDER_TEMPLATE,
        dynamic_template_data: {
          subject: `${orderData[0].Order_Type}`,
          url: `${process.env.API_HOST}/patient/main/${orderData[0].Order_UID}/${orderData[0].Order_Type}`
        }
      }
      // send mail
      sgMail.send(message)
    }
    var auditData = {
      Audit_UID: await auditUid(),
      Audit_Type: auditType.change,
      Audit_Info: 'New- ' + await auditUid(),
      Data_Point: 'AP/cancel order',
      IP_Address: await model.Audit.getIpAddress(req)
    }
    await model.Audit.create(auditData)
    res.status(200).send({ code: 200, status: 'success', message: 'Order has been cancelled.' })
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.status(422).send({ code: 422, status: 'failed', message: error.message })
  }
})

module.exports = router
