/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
var Router = require('express').Router
var router = new Router()
var model = require('../../models')
var { genRandomString, bcryptHash } = require('../../utils/crypto')
var { auditUid } = require('../../utils/UID')
var auditType = require('../../auditType.json')
var sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_PRIVATE_KEY)
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const jwt = require('jsonwebtoken')
const logger = require('../../models/errorlog')
const momentTz = require('moment-timezone')

// Post service for register a new user as admin //
router.post('/signup', async (req, res) => {
	const data = req.body
	if (!data.AP_Username) return res.status(422).send({ code: 422, status: 'failure', message: 'Don’t forget to enter your username! It’s required for your profile.' })
	if (!data.AP_First_Name) return res.status(422).send({ code: 422, status: 'failure', message: 'Don’t forget to enter your first name! It’s required for your profile.' })
	if (!data.AP_Last_Name) return res.status(422).send({ code: 422, status: 'failure', message: 'Don’t forget to enter your last name! It’s required for your profile.' })
	data.AP_Username = data.AP_Username.toLowerCase()
	try {
		// find admin in the system from the username
		const admin = await model.Admin.findAdminByUsername(data.AP_Username)
		// if admin with this email is already exist in system
		if (admin) {
			return res.status(422).send({
				code: 422,
				status: 'failure',
				message: '*This username is already associated with another account. Please log in.'
			})
		} else {
			const password = genRandomString(10)
			data.AP_Password = bcryptHash(password, 10)
			// create admin 
			const saveAdmin = await model.Admin.create(data)
			var payload = {
				Admin_Id: saveAdmin._id
			}
			var token = jwt.sign(payload, saveAdmin.AP_Password, { expiresIn: 60 * 60 * 24 })
			var message = {
				to: data.AP_Username,
				from: process.env.ADMIN_FROM_EMAIL,
				subject: 'reset password',
				templateId: process.env.ADMIN_FORGET_PASSWORD_TEMPLET_ID,
				dynamic_template_data: {
					auth_token: `${process.env.ADMIN_PORTAL}/reset/${saveAdmin._id}/${token}`,
					AP_Username: saveAdmin.AP_Username,
					date: momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('DD/MM/YY'),
					time: momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('HH:mm:ss')
				}
			}
			// send mail for reset password
			await sgMail.send(message)
			saveAdmin.AP_Password = undefined
			var ip_add = await model.Audit.getIpAddress(req)
			const UID = await auditUid()
			const auditData = {
				Audit_UID: UID,
				Audit_Type: auditType.add,
				Data_Point: 'AP/Signup',
				Audit_Info: 'New- ' + UID,
				New_Value: saveAdmin,
				IP_Address: ip_add
			}
			await model.Audit.create(auditData)
			res.status(200).send({ code: 200, status: 'success', message: 'signup successfull.check your mail for password and login.' })
		}
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.status(422).send({ code: 422, status: 'failed', message: error.message })
	}
})
// Put service for update admin by id //
router.put('/update-admin/:id', authorize(Role.Admin), async (req, res) => {
	const data = req.body
	data.AP_Id = req.params.id
	if (!data.AP_Username) return res.status(422).send({ code: 422, status: 'failure', message: 'Don’t forget to enter your username! It’s required for your profile.' })
	if (!data.AP_First_Name) return res.status(422).send({ code: 422, status: 'failure', message: 'Don’t forget to enter your first name! It’s required for your profile.' })
	if (!data.AP_Last_Name) return res.status(422).send({ code: 422, status: 'failure', message: 'Don’t forget to enter your last name! It’s required for your profile.' })
	if (!data.AP_Id) return res.status(422).send({ code: 422, status: 'failed', message: 'User id is not provided! it\'s required to update user.' })
	data.AP_Username = data.AP_Username.toLowerCase()
	try {
		// find the admin in the system
		const admin = await model.Admin.findAdminByUsername(data.AP_Username)
		data.Updated_At = Date.now()
		var user = await model.Admin.findById(data.AP_Id)
		if (user.length == 0) return res.status(422).send({ code: 422, status: 'failed', message: 'this is not a valid user!' })
		user[0].AP_Password = undefined

		if (user[0].AP_Username == data.AP_Username) {
			await updateUser(req, data, user)
			const token = await generateToken(data, user._id)
			return res.status(200).send({ code: 200, status: 'success', data: token })
		} else {
			if (admin) return res.status(422).send({ code: 422, status: 'failure', message: '*This username is already associated with another account. Please log in.' })
			await updateUser(req, data, user)
			const token = await generateToken(data, user._id)
			return res.status(200).send({ code: 200, status: 'success', data: token })
		}
		
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.status(422).send({ code: 422, status: 'failed', message: error.message })
	}
})
// Function for update admin user and create audit //
async function updateUser (req, data, user) {
	data.AP_Username = data.AP_Username.toLowerCase()
	await model.Admin.updateById(data.AP_Id, data)
	const auditData = {
		Audit_UID: await auditUid(),
		Audit_Type: auditType.change,
		Data_Point: 'AP/Update',
		Audit_Info: 'New- ' + await auditUid(),
		Old_Value: user,
		New_Value: data,
		IP_Address: await model.Audit.getIpAddress(req)
	}
	await model.Audit.create(auditData)
}
// Generate token for updtae on loggedin user //
async function generateToken (data, id) {
	let token
	if (data.isLggedinUser) {
		const payload = {
			AP_Id: id,
			AP_Username: data.AP_Username,
			AP_First_Name: data.AP_First_Name,
			AP_Last_Name: data.AP_Last_Name,
			roles: 'Admin'
		}
		token = jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: 60 * 60 * 24 })
	} else {
		token = jwt.sign({}, process.env.JWT_SECRET, { expiresIn: 60 * 60 * 24 })
	}
	return token
}

module.exports = router
