/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const model = require('../../models')
const auditType = require('../../auditType.json')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const { auditUid } = require('../../utils/UID')
const { isAValidPhoneNumber } = require('../../utils/validate')
const moment = require('moment')
const logger = require('../../models/errorlog')
const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_PRIVATE_KEY)
var jwt = require('jsonwebtoken')
const _ = require('lodash')
const momentTz = require('moment-timezone')

// Get provider list with search funcationality //
router.post('/get-providerlist', authorize(Role.Admin), async (req, res) => {
  try {
    var data = req.body

    // find the provider list
    var providerData = await model.Provider.getProviderList(data)

    return res.status(200).send({
      code: 200,
      status: 'success',
      data: providerData
    })
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.status(422).send({ code: 422, status: 'failed', message: error.message })
  }
})
// Get provider list for export csv //
router.post('/export-provider-csv', authorize(Role.Admin), async (req, res) => {
  let data = req.body

  // get providers list
  var providerData = await model.Provider.exportProviderCSV(data)

  providerData.map(ele => {
    ele.Last_Active = ele.Last_Active ? ele.Last_Active : null
  })
  return res.status(200).send({
    code: 200,
    status: 'success',
    data: providerData
  })
})
// Get provider by id //
router.get('/get-provider/:id', authorize(Role.Admin), async (req, res) => {
  try {
    var pro_id = req.params.id
    if (!pro_id) return res.status(422).send({ code: 422, status: 'failed', message: 'provider id is not provided!it\'s required to get provider details' })
    
    // Get provider data from provider id
    var providerData = await model.Provider.getProviderByPRO_UID(pro_id)
    if (!providerData) return res.status(422).send({ code: 422, status: 'failed', message: `provider with id '${pro_id}' is not in our system!!` })

    delete providerData._doc.PRO_Password
    let data
    if (providerData.PRO_Profile_Picture) {
      let url = await model.CommonServices.getImage(providerData.PRO_Profile_Picture)
      providerData.PRO_Profile_Picture = url
    }
    if (providerData.PRO_Govt_ID_Front) {
      let url = await model.CommonServices.getImage(providerData.PRO_Govt_ID_Front)
      providerData.PRO_Govt_ID_Front = url
    }
    if (providerData.PRO_Govt_ID_Back) {
      let url = await model.CommonServices.getImage(providerData.PRO_Govt_ID_Back)
      providerData.PRO_Govt_ID_Back = url
    }
    if (providerData.PRO_Driving_Licence_Front != null && providerData.PRO_Driving_Licence_Front != '') {
      providerData.PRO_Driving_Licence_Front = await model.CommonServices.getImage(providerData.PRO_Driving_Licence_Front)
    }
    if (providerData.PRO_Driving_Licence_Back != null && providerData.PRO_Driving_Licence_Back != '') {
      providerData.PRO_Driving_Licence_Back = await model.CommonServices.getImage(providerData.PRO_Driving_Licence_Back)
    }

    if (providerData.PRO_State_ID_Front != null && providerData.PRO_State_ID_Front != '') {
      providerData.PRO_State_ID_Front = await model.CommonServices.getImage(providerData.PRO_State_ID_Front)
    }
    if (providerData.PRO_State_ID_Back != null && providerData.PRO_State_ID_Back != '') {
      providerData.PRO_State_ID_Back = await model.CommonServices.getImage(providerData.PRO_State_ID_Back)
    }

    if (providerData.PRO_Passport_Front != null && providerData.PRO_Passport_Front != '') {
      providerData.PRO_Passport_Front = await model.CommonServices.getImage(providerData.PRO_Passport_Front)
    }
    if (providerData.PRO_CV.length > 0) {
      data = await getImageUrls(providerData.PRO_CV)
      providerData.PRO_CV = data
    }
    if (providerData.PRO_State_Medical_License.length > 0) {
      data = await getImageUrls(providerData.PRO_State_Medical_License)
      providerData.PRO_State_Medical_License = data
    }
    if (providerData.PRO_DEA_Cert.length > 0) {
      data = await getImageUrls(providerData.PRO_DEA_Cert)
      providerData.PRO_DEA_Cert = data
    }
    if (providerData.PRO_Board_Cert.length > 0) {
      data = await getImageUrls(providerData.PRO_Board_Cert)
      providerData.PRO_Board_Cert = data
    }
    if (providerData.PRO_Liability_Cert.length > 0) {
      data = await getImageUrls(providerData.PRO_Liability_Cert)
      providerData.PRO_Liability_Cert = data
    }
    return res.status(200).send({ code: 200, status: 'success', data: providerData })
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.status(422).send({ code: 422, status: 'failed', message: error.message })
  }
})
// Add new provider by admin //
router.post('/add-provider', authorize(Role.Admin), async (req, res) => {
  try {
    var data = req.body
    var dateValidation = /^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/
    if (!data.PRO_Gender) return res.status(422).send({ code: 422, status: 'failed', message: 'Don’t forget to choose a gender! It’s required for your profile.' })
    if (data.PRO_DOB) {
      data.PRO_DOB = moment(new Date(data.PRO_DOB)).format('MM/DD/YYYY')
      if (!dateValidation.test(data.PRO_DOB)) return res.status(422).send({ code: 422, status: 'failed', message: 'Please make sure to format your date of birth as MM/DD/YYYY.' })
    }
    if (!data.PRO_Username) return res.status(422).send({ code: 422, status: 'failed', message: '*Don’t forget to enter your email! It’s required to sign up.' })
    if (data.PRO_Home_Phone) {
      data.PRO_Home_Phone = data.PRO_Home_Phone.replace(/\D/g, '')
      let validNumber = isAValidPhoneNumber(data.PRO_Home_Phone)
      if (!validNumber) return res.status(422).send({ code: 422, status: 'failed', message: 'Please make sure to enter a sequence of numbers. No letters or special characters are accepted.' })
    }
    if (data.PRO_Office_Phone) {
      data.PRO_Office_Phone = data.PRO_Office_Phone.replace(/\D/g, '')
      let validNumber = isAValidPhoneNumber(data.PRO_Office_Phone)
      if (!validNumber) return res.status(422).send({ code: 422, status: 'failed', message: 'Please make sure to enter a sequence of numbers. No letters or special characters are accepted.' })
    }
    if (data.PRO_Fax_Number) {
      data.PRO_Fax_Number = data.PRO_Fax_Number.replace(/\D/g, '')
      var validNumber = isAValidPhoneNumber(data.PRO_Fax_Number)
      if (!validNumber) return res.status(422).send({ code: 422, status: 'failed', message: 'Please make sure to enter a sequence of numbers. No letters or special characters are accepted.' })
    }
    if (data.PT_Age_Min) {
      if (data.PT_Age_Min % 1 != 0) return res.status(422).send({ code: 422, status: 'failed', message: '*Must be at whole number entered.' })
    }
    if (data.PT_Age_Max) {
      if (data.PT_Age_Max % 1 != 0) return res.status(422).send({ code: 422, status: 'failed', message: '*Must be at whole number entered.' })
    }
    var provider = await model.Provider.findOneByEmail(data.PRO_Username)
    if (provider) return res.status(422).send({ code: 422, status: 'failed', message: '*This username is already associated with another account. Please log in.' })
    var allProvider = await model.Provider.lastInserted()
    if (allProvider.length == 0) {
      data.PRO_UID = 'PRO-0000-0001'
      const provider = await model.Provider.create(data)
      var providerData = await model.Provider.getProviderByPRO_UID(provider.PRO_UID)
      await createAudit(req, provider)
      await sendEmail(data, res)
      return res.status(200).send({
        code: 1,
        status: 'sucess',
        data: providerData
      })
    } else {
      let maxId = allProvider[0].PRO_UID
      let nextId = await getNextId(maxId)
      data.PRO_UID = nextId
      const provider = await model.Provider.create(data)
      // delete provider._doc.PRO_Password
      await createAudit(req, provider)
      await sendEmail(data, res)
      return res.status(200).send({ code: 200, status: 'success', data: provider })
    }
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.status(422).send({ code: 422, status: 'failed', message: error.message })
  }
})

//get provider encounter by id
router.post('/get-provider-encounter/:id', authorize(Role.Admin), async (req, res) => {
  try {
    var data = req.body
    data.PRO_UID = req.params.id
    if (!data.PRO_UID) return res.status(422).send({ code: 422, status: 'failed', message: 'provider id is required to get encounter!!' })
    const provider = await model.Provider.getProviderByPRO_UID(data.PRO_UID)
    if (!provider) return res.status(422).send({ code: 422, status: 'failed', message: `provider with id '${data.PRO_UID}' is not in our system` })
    const encounter = await model.Encounter.getProviderEncounterById(data)
    if (data.order == 'asc') encounter.result.sort(compareAsc)
    else encounter.result.sort(compareDsc)
    encounter.result = _(encounter.result).slice(data.skip ? data.skip : 0).take(data.limit ? data.limit : 10).value()
    var auditData = {
      Audit_UID: await auditUid(),
      Audit_Type: auditType.view,
      Audit_Info: 'New- ' + await auditUid(),
      Data_Point: 'AP/Get provider encounter',
      IP_Address: await model.Audit.getIpAddress(req)
    }
    await model.Audit.create(auditData)

    if (encounter.total == 0) return res.status(200).send({ code: 200, status: 'success', message: encounter })
    var encounterData = encounter.result.map(ele => ({
      Encounter_Date: moment(moment(new Date(ele.Encounter_Date)).format('YYYY-MM-DD') + ele.Encounter_Time, 'YYYY-MM-DDLT').format('MM/DD/YYYY hh:mma'),
      Encounter_Location_Type: ele.Encounter_Location_Type,
      PT_First_Name: ele.provider_encounter.length > 0 ? ele.provider_encounter[0].PT_First_Name : null,
      PT_Last_Name: ele.provider_encounter.length > 0 ? ele.provider_encounter[0].PT_Last_Name : null,
      PT_Gender: ele.provider_encounter.length > 0 ? ele.provider_encounter[0].PT_Gender : null,
      PT_DOB: ele.provider_encounter.length > 0 ? ele.provider_encounter[0].PT_DOB : null,
      Encounter_UID: ele.Encounter_UID,
      Encounter_Chief_Complaint_PRO: ele.Encounter_Chief_Complaint_PRO,
      Encounter_Status: ele.Encounter_Status == 'Cancelled' ? 'Cancelled' : ele.Encounter_Status == 'Abandoned' ? 'Abandoned' : ele.Encounter_Status == 'Scheduled' ? 'Scheduled' : new Date() < new Date(ele.Encounter_Date + ' ' + ele.Encounter_Time) ? 'Scheduled' : (ele.chart.length > 0 ? ele.chart[0].Chart_is_Signed : false) == true ? 'Signed' : ele.Encounter_Status == 'Active' ? 'Active' : moment(ele.Encounter_Time, 'hh:mm A').format('HH:mm') < moment(new Date()).format('HH:mm') < moment(ele.Encounter_End_Time + ' ' + ele.Encounter_End_Time_Zone, 'hh:mm A').format('HH:mm') ? 'Active' : (moment(new Date()).format('HH:mm') > moment(ele.Encounter_End_Time + ' ' + ele.Encounter_End_Time_Zone, 'hh:mm A').format('HH:mm') && (ele.chart.length > 0 ? ele.chart[0].Chart_is_Signed : false) == false) ? 'Unsigned' : 'Requested',
      Chart_is_Signed: ele.chart.length > 0 ? ele.chart[0].Chart_is_Signed : false
    }))

    encounter.result = encounterData
    res.status(200).send({ code: 200, status: 'success', data: encounter })
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.status(422).send({ code: 422, status: 'failed', message: error.message })
  }
})
// service for send onboarding email to provider //
router.post('/send-onboarding-mail', authorize(Role.Admin), async (req, res) => {
  let PRO_UID = req.body.PRO_UID
  if (!PRO_UID) return res.status(422).send({ code: 422, status: 'failed', message: 'PRO_UID is required!' })
  try {
    let settingData = await model.Other.find()
    let provider = await model.Provider.findById(PRO_UID)
    if (!provider) return res.status(422).send({ code: 422, status: 'failed', message: `provider with id '${PRO_UID}' not found` })
    var PRO_message = {
      from: settingData[0].HS_Support_Email,
      to: provider.PRO_Username,
      template_id: process.env.On_Boarding_Email,
      dynamic_template_data: {
        url: `${process.env.API_HOST}/provider/main/dashboard`
      }
    }
    await sgMail.send(PRO_message)
    return res.status(200).send({ code: 200, status: 'success', message: 'mail send to provider!' })
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.status(422).send({ code: 422, status: 'failed', message: error })
  }
})
// Function for generate unique id for provider //
function getNextId (id) {
  let splitedId = id.split('-')
  let lastNumericValue = parseInt(splitedId[2])
  let middleNumericValue = parseInt(splitedId[1])
  let lastValue = `${lastNumericValue != 9999 ? pad(lastNumericValue + 1, 4) : pad(0, 4)}`
  let middleValue = `${lastNumericValue != 9999 ? splitedId[1] : pad(middleNumericValue + 1, 4)}`
  let newId = `PRO-${middleValue}-${lastValue}`
  return newId
  function pad (n, width, z) {
    z = z || '0'
    n = n + ''
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n
  }
}
// Send confirmation email to provider //
async function sendEmail (data, res) {
  let verify = data.PRO_Password ? data.PRO_Password : process.env.JWT_SECRET
  let token = jwt.sign({ PRO_Username: data.PRO_Username }, verify, { expiresIn: '24h' })
  const url = `${process.env.API_HOST}/provider/reset/${data.PRO_UID}/${token}`
  let ADMIN_message = {
    from: process.env.ADMIN_FROM_EMAIL,
    to: data.PRO_Username,
    template_id: process.env.PROVIDER_FORGOT_PASSWORD_TEMPLATE,
    dynamic_template_data: {
      date: moment(new Date()).format('MM/DD/YY'),
      time: momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('HH:mm:ss'),
      PRO_Username: data.PRO_Username,
      url: url
    }
  }
  try {
    // await sgMail.send(PRO_message);
    await sgMail.send(ADMIN_message)
  } catch (err) {
    return res.send(err.message)
  }
}
// Create audit log for provider //
async function createAudit (req, data) {
  const auditData = {
    Audit_UID: await auditUid(),
    Audit_Type: auditType.add,
    Audit_Info: 'New- ' + await auditUid(),
    Data_Point: 'AP/Add provider',
    PRO_First_Name: data.PRO_First_Name,
    PRO_Last_Name: data.PRO_Last_Name,
    PRO_UID: data.PRO_UID,
    New_Value: data,
    IP_Address: await model.Audit.getIpAddress(req)
  }
  await model.Audit.create(auditData)
}

// Get file urls for documents //
var getImageUrls = async (arr) => {
  let newArray = []; let count = 0
  for (var i = 0; i < arr.length; i++) {
    let url = arr[i].Attachment_Name != null || arr[i].Attachment_Name != '' ? await model.CommonServices.getImage(arr[i].Attachment_Name) : null
    if (arr[i].Is_Deleted == false) {
      newArray.push({
        Attachment_Url: url,
        Is_Deleted: arr[i].Is_Deleted,
        _id: arr[i]._id,
        Attachment_Name: arr[i].Attachment_Name
      })
    }
    count = count + 1
    if (count == arr.length) {
      return newArray
    }
  }
}

// function for get data in decending order
function compareDsc (a, b) {
  if (new Date(b.Encounter_Date + ' ' + b.Encounter_Time) < new Date(a.Encounter_Date + ' ' + a.Encounter_Time)) {
    return -1
  }
  if (new Date(b.Encounter_Date + ' ' + b.Encounter_Time) > new Date(a.Encounter_Date + ' ' + a.Encounter_Time)) {
    return 1
  }
  return 0
}

// function for get data in ascending order
function compareAsc (a, b) {
  if (new Date(a.Encounter_Date + ' ' + a.Encounter_Time) < new Date(b.Encounter_Date + ' ' + b.Encounter_Time)) {
    return -1
  }
  if (new Date(a.Encounter_Date + ' ' + a.Encounter_Time) > new Date(b.Encounter_Date + ' ' + b.Encounter_Time)) {
    return 1
  }
  return 0
}

module.exports = router
