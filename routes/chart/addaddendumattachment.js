/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const chartModule = require('../../models').Chart
const auditModule = require('../../models').Audit
const errorModule = require('../../models').Errors
const encounterModule = require('../../models').Encounter
const serviceModule = require('../../models').CommonServices
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const { auditUid } = require('../../utils/UID')
const logger = require('../../models/errorlog')
const auditType = require('../../auditType.json')

// route for create chart
router.post('/add-addendum-attchment', authorize(Role.Provider), async (req, res) => {
	var { body: { Encounter_UID, Attachment_Name } } = req
	if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required', Error_Code: errorModule.Encounter_UID_required() })
	if (!Attachment_Name) return res.send({ code: 0, status: 'failed', message: 'Attachment_Name is required', Error_Code: errorModule.Attachment_Name_required() })

	try {
		// Get encounter by encounter by encounter uid
		let encounter = await encounterModule.getEncounterByEncounter_UID(Encounter_UID)
		if (!encounter) return res.send({ code: 0, status: 'failed', message: `encounter with id '${Encounter_UID}' not found!`, Error_Code: errorModule.Encounter_not_found() })
		if (!req.body.Addendum_Id) return res.send({ code: 0, status: 'failed', message: 'Addendum_Id is required!', Error_Code: errorModule.Addendum_Id_required() })
		
		// Get chart data from encounter UID
		let chart = await chartModule.getChartData(Encounter_UID)
		if (chart) {
			let attachmantData = {
				Attachment_Name: Attachment_Name,
				Attachment_By: req.userType,
				Attachment_User: req.userType == 'Provider' ? req.user.PRO_UID : req.user.PT_HS_MRN
			}

			// upload addendum attachment
			await chartModule.uploadAddendumAttachment(Encounter_UID, req.body.Addendum_Id, attachmantData)
			const auditData = {
				Audit_UID: await auditUid(),
				Audit_Info: 'New- ' + await auditUid(),
				Audit_Type: auditType.add,
				Data_Point: 'PRO/add addendum attachment',
				PRO_First_Name: req.user.PRO_First_Name,
				PRO_Last_Name: req.user.PRO_Last_Name,
				PRO_UID: req.user.PRO_UID,
				IP_Address: await auditModule.getIpAddress(req)
			}
			await auditModule.create(auditData)

			// Get chart data from encounter UID
			let updatedChart = await chartModule.getChartData(Encounter_UID)
			// eslint-disable-next-line eqeqeq
			updatedChart = updatedChart.PRO_Addendums.find(ele => ele._id == req.body.Addendum_Id)
			let addendumAttachment = updatedChart.PRO_Addendum_Attachment.length > 0 ? await serviceModule.getFileUrl(updatedChart.PRO_Addendum_Attachment) : []
			res.send({
				code: 1,
				status: 'sucess',
				data: addendumAttachment
			})
		} else {
			res.send({
				code: 0,
				status: 'failed',
				message: `No chart found for encounter UID ${Encounter_UID}`,
				Error_Code: errorModule.No_chart_found()
			})
		}
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS029' })
	}
})

module.exports = router
