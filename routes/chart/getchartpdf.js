/* eslint-disable no-useless-escape */
/* eslint-disable eqeqeq */
/* eslint-disable no-async-promise-executor */
/* eslint-disable dot-notation */
/* eslint-disable camelcase */
/* eslint-disable prefer-const */
const Router = require('express').Router
const router = new Router()
const moment = require('moment')
const model = require('../../models')
const serviceModule = require('../../models').CommonServices
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const logger = require('../../models/errorlog')
var $ = require('jsrender')
var AWS = require('aws-sdk')
const puppeteer = require('puppeteer')
AWS.config.update({
	accessKeyId: process.env.AWS_ACCESS_KEY,
	secretAccessKey: process.env.AWS_SECRET_KEY,
	region: 'ap-south-1'
})

var s3 = new AWS.S3({ params: { Bucket: `${process.env.AWS_BUCKET}/Order` } })

// route for get summery chart PDF
router.get('/get-summery-chart/:Encounter_UID', authorize([Role.Provider, Role.Patient]), async (req, res) => {
	try {
		let Encounter_UID = req.params.Encounter_UID
		if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required!', Error_Code: model.Errors.Encounter_UID_required() })
		
		// Check encounter with this encounter UID is available in the system or not
		let encounter = await model.Encounter.getSummaryByEncounter_UID(Encounter_UID)
		if (encounter.length == 0) return res.send({ code: 0, status: 'failed', message: `Encounter with id '${Encounter_UID}' not found!`, Error_Code: model.Errors.Encounter_not_found() })
		
		// Check followup of this encounter is available or not
		let followupEncounter = await model.Encounter.getFollowupEncounterByEncounter_UID(encounter[0].Encounter_UID)
		let arr = []; let all_url = []
		if (encounter[0].charts.length != 0) {
			encounter[0].charts[0].PRO_Addendums.map(ele => {
				ele.signDate = ele.Addendum_Sign_Date ? moment(ele.Addendum_Sign_Date).format('D/M/YYYY') : null
				ele.signTime = ele.Addendum_Sign_Date ? moment.tz(ele.Addendum_Sign_Date, process.env.CURRENT_TIMEZONE).format('HH:mm') : null
				ele.PRO_Addendum_Attachment.forEach(item => {
					item['key'] = 'Chart ' + '/ ' + 'Addendum'
					arr.push(item)
				})
			})
			// all_url = []
			all_url = await addendumArray(arr)
		} else {
			all_url = []
		}
		let summaryData = {
			chart_title: encounter[0].Encounter_Type == 'New Visit' ? 'New Visit Summary' : 'Visit Summary',
			PRO_First_Name: encounter[0].providers.PRO_First_Name,
			PRO_Last_Name: encounter[0].providers.PRO_Last_Name,
			PRO_Office_Address_Line_1: encounter[0].providers.PRO_Office_Address_Line_1,
			PRO_Office_Address_Line_2: encounter[0].providers.PRO_Office_Address_Line_2,
			PRO_Office_State: encounter[0].providers.PRO_Office_State,
			PRO_Office_Zip: encounter[0].providers.PRO_Office_Zip,
			PRO_Office_City: encounter[0].providers.PRO_Office_City,
			PRO_Office_Phone: encounter[0].providers.PRO_Office_Phone.replace(/\D+/g, '')
				.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3'),
			Encounter_Day: moment(new Date(encounter[0].Encounter_Date)).format('dddd'),
			Encounter_Date: moment(new Date(encounter[0].Encounter_Date)).format('M/D/YYYY'),
			Encounter_Time: moment(encounter[0].Encounter_Time, 'hh:mm a').format('HH:mm'),
			Encounter_Location_Type: encounter[0].Encounter_Location_Type,
			PT_First_Name: encounter[0].patients.PT_First_Name ? encounter[0].patients.PT_First_Name : null,
			PT_Last_Name: encounter[0].patients.PT_Last_Name ? encounter[0].patients.PT_Last_Name : null,
			PT_Cell_Phone: encounter[0].patients.PT_Cell_Phone ? encounter[0].patients.PT_Cell_Phone.replace(/\D+/g, '')
				.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3') : null,
			PT_Home_Address_Line_1: encounter[0].patients.PT_Home_Address_Line_1 ? encounter[0].patients.PT_Home_Address_Line_1 : null,
			PT_Home_Address_Line_2: encounter[0].patients.PT_Home_Address_Line_2 ? encounter[0].patients.PT_Home_Address_Line_2 : null,
			PT_Home_State: encounter[0].patients.PT_Home_State,
			PT_Home_City: encounter[0].patients.PT_Home_City,
			PT_Home_Zip: encounter[0].patients.PT_Home_Zip,
			PT_DOB: encounter[0].patients.PT_DOB ? moment(new Date(encounter[0].patients.PT_DOB)).format('M/D/YYYY') : null,
			PT_Age_Now: encounter[0].patients.PT_DOB ? moment(new Date()).format('YYYY') - moment(new Date(encounter[0].patients.PT_DOB)).format('YYYY') : null,
			PT_Gender: encounter[0].patients.PT_Gender ? encounter[0].patients.PT_Gender[0] : null,
			PT_HS_MRN: encounter[0].PT_HS_MRN ? encounter[0].PT_HS_MRN : null,
			Encounter_Chief_Complaint_PT: encounter[0].Encounter_Chief_Complaint_PT ? encounter[0].Encounter_Chief_Complaint_PT : null,
			Encounter_UID: encounter[0].Encounter_UID ? encounter[0].Encounter_UID : null,
			Encounter_End_Date: encounter[0].Encounter_Close_Date ? moment(encounter[0].Encounter_Close_Date).format('M/D/YYYY') : null,
			Encounter_End_Time: encounter[0].Encounter_Close_Date ? moment(encounter[0].Encounter_Close_Date).format('HH:mm') : null,
			PRO_Patient_Instructions: encounter[0].charts.length > 0 ? encounter[0].charts[0].PRO_Patient_Instructions : null,
			PRO_Plan_Description: encounter[0].charts.length > 0 ? encounter[0].charts[0].PRO_Plan_Description : null,
			Prescriptions: encounter[0].order.filter(ele => ele.Order_Type == 'Prescription Order'),
			Lab_Orders: encounter[0].order.filter(ele => ele.Order_Type == 'Lab Order'),
			Imaging_Orders: encounter[0].order.filter(ele => ele.Order_Type == 'Image Order'),
			Referrals: encounter[0].order.filter(ele => ele.Order_Type == 'Referral Order'),
			Other: encounter[0].order.filter(ele => ele.Order_Type == 'Other Order'),
			Encounter_Followup_Status: followupEncounter ? encounter[0].Encounter_Followup_Status : null,
			Followup_Time: followupEncounter ? `${followupEncounter.Encounter_Start_Time}${followupEncounter.Encounter_Start_Time_Zone}` : null,
			Followup_Date: followupEncounter ? moment(new Date(followupEncounter.Encounter_Date)).format('M/D/YYYY') : null,
			Followup_Day: followupEncounter ? moment(new Date(followupEncounter.Encounter_Date)).format('dddd') : null,
			Followup_Location: followupEncounter ? followupEncounter.Encounter_Location_Type : null,
			Chart_is_Signed: encounter[0].charts.length > 0 ? encounter[0].charts[0].Chart_is_Signed : null,
			Diagnosis: encounter[0].charts.length > 0 ? encounter[0].charts[0].PRO_Diagnosis.length > 0 ? encounter[0].charts[0].PRO_Diagnosis[0].PRO_Diagnosis_Note : 'None' : null,
			PRO_Diagnosis_ICD_Code: encounter[0].charts.length > 0 ? encounter[0].charts[0].PRO_Diagnosis_ICD_Code : null,
			PRO_Private_Note: encounter[0].charts[0].PRO_Private_Note,
			PRO_Addendums: encounter[0].charts.length > 0 ? encounter[0].charts[0].PRO_Addendums : null,
			order: encounter[0].order,
			all_url: all_url
		}
		let template = $.templates('./download/summary.html')

		// send chart data to the HTML template
		let html = template.render(summaryData)
		const opts = {
			headless: true,
			timeout: 15000,
			args: ['--no-sandbox']
		}
		const browser = await puppeteer.launch(opts)
		const page = await browser.newPage()
		await page.setContent(html)
		const pdf = await page.pdf({
			format: 'A4',
			printBackground: true,
			displayHeaderFooter: true,
			footerTemplate: `<div  style="width:100%; font-size: 11px !important;  padding:0px 60px 25px 60px !important; page-break-after: always; font-family: 'Roboto', sans-serif;">
      <div style="border-top:1px solid #ddd;">
       <div style = "float: left; margin:0px 0px;  ">
       <div >${summaryData.PT_First_Name} ${summaryData.PT_Last_Name} &nbsp; DOB: <span> </span>${summaryData.PT_DOB}</div>
       <div >${summaryData.Encounter_UID}</div>
     </div>
     <div style = "float: right; margin:0px 0px; ">
     <div class=\"page-footer\" style=\"width:100%; text-align:right; \">Page <span class=\"pageNumber\" "></span> of <span class=\"totalPages\"></span></div>
     </div>
     </div>
     </div>`,
			margin: {
				bottom: '30mm'
			}

		})
		await browser.close()
		const imagename = `summary_${Date.now()}.pdf`
		const params = {
			Bucket: process.env.AWS_BUCKET,
			Key: imagename,
			Body: pdf,
			ContentType: 'application/pdf',
			ACL: 'public-read'
		}

		// upload PDF to the AWS bucket
		s3.upload(params, function (s3Err, data) {
			if (s3Err) throw s3Err
			res.send({ code: 1, status: 'success', data: data.Location })
		})
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS047' })
	}
})


// route for get full chart pdf with encounter UID
router.get('/get-full-chart/:Encounter_UID', authorize([Role.Provider, Role.Patient]), async (req, res) => {
	try {
		let Encounter_UID = req.params.Encounter_UID
		if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required!', Error_Code: model.Errors.Encounter_UID_required() })
		
		// Find the encounter details with the encounter UID
		let encounter = await model.Encounter.getSummaryByEncounter_UID(Encounter_UID)
		if (encounter.length == 0) return res.send({ code: 0, status: 'failed', message: `Encounter with id '${Encounter_UID}' not found!`, Error_Code: model.Errors.Encounter_not_found() })
		
		// Check followup is present for this encounter
		let followupEncounter = await model.Encounter.getFollowupEncounterByEncounter_UID(encounter[0].Encounter_UID)
		let arr = []
		encounter[0].charts[0].PRO_Addendums.map(ele => {
			ele.signDate = ele.Addendum_Sign_Date ? moment(ele.Addendum_Sign_Date).format('D/M/YYYY') : null
			ele.signTime = ele.Addendum_Sign_Date ? moment.tz(ele.Addendum_Sign_Date, process.env.CURRENT_TIMEZONE).format('HH:mm') : null
			ele.PRO_Addendum_Attachment.forEach(item => {
				item['key'] = 'Chart ' + '/ ' + 'Addendum'
				arr.push(item)
			})
		})
		encounter[0].PT_History.PT_Medical_Directive_Attachment.map(ele => {
			ele['key'] = 'Recent Medical History ' + '/ ' + 'Medical Directive'
			arr.push(ele)
		})
		encounter[0].PT_History.PT_Medication_Attachment.map(ele => {
			ele['key'] = 'Recent Medical History ' + '/ ' + 'Medication'
			arr.push(ele)
		})
		encounter[0].PT_History.PT_Medical_History_Attachment.map(ele => {
			ele['key'] = 'Recent Medical History ' + '/ ' + 'Medical History'
			arr.push(ele)
		})
		let all_url = await addendumArray(arr)
		let fullChartData = {
			chart_title: encounter[0].Encounter_Type == 'New Visit' ? 'New Consult' : 'Followup Report',
			PRO_First_Name: encounter[0].providers.PRO_First_Name,
			PRO_Last_Name: encounter[0].providers.PRO_Last_Name,
			PRO_Office_Address_Line_1: encounter[0].providers.PRO_Office_Address_Line_1,
			PRO_Office_Address_Line_2: encounter[0].providers.PRO_Office_Address_Line_2,
			PRO_Office_State: encounter[0].providers.PRO_Office_State,
			PRO_Office_Zip: encounter[0].providers.PRO_Office_Zip,
			PRO_Office_Phone: encounter[0].providers.PRO_Office_Phone ? encounter[0].providers.PRO_Office_Phone.replace(/\D+/g, '')
				.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3') : null,
			Encounter_Day: moment(new Date(encounter[0].Encounter_Date)).format('dddd'),
			Encounter_Date: moment(new Date(encounter[0].Encounter_Date)).format('M/D/YYYY'),
			Encounter_Time: moment(encounter[0].Encounter_Time, 'hh:mm a').format('HH:mm'),
			Encounter_Location_Type: encounter[0].Encounter_Location_Type,
			PT_First_Name: encounter[0].patients.PT_First_Name,
			PT_Last_Name: encounter[0].patients.PT_Last_Name,
			PT_Cell_Phone: encounter[0].patients.PT_Cell_Phone ? encounter[0].patients.PT_Cell_Phone.replace(/\D+/g, '')
				.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3') : null,
			PT_Home_Address_Line_1: encounter[0].patients.PT_Home_Address_Line_1,
			PRO_Office_City: encounter[0].providers.PRO_Office_City,
			PT_Home_City: encounter[0].patients.PT_Home_City,
			PT_Home_Address_Line_2: encounter[0].patients.PT_Home_Address_Line_2,
			PT_Home_State: encounter[0].patients.PT_Home_State,
			PT_Home_Zip: encounter[0].patients.PT_Home_Zip,
			PT_DOB: moment(new Date(encounter[0].patients.PT_DOB)).format('M/D/YYYY'),
			PT_Age_Now: moment(new Date()).format('YYYY') - moment(new Date(encounter[0].patients.PT_DOB)).format('YYYY'),
			PT_Gender: encounter[0].patients.PT_Gender ? encounter[0].patients.PT_Gender[0] : null,
			PT_HS_MRN: encounter[0].PT_HS_MRN,
			Encounter_Chief_Complaint_PT: encounter[0].Encounter_Chief_Complaint_PT,
			Encounter_UID: encounter[0].Encounter_UID,
			Encounter_End_Date: encounter[0].Encounter_Close_Date ? moment(encounter[0].Encounter_Close_Date).format('M/D/YYYY') : null,
			Encounter_End_Time: encounter[0].Encounter_Close_Date ? moment(encounter[0].Encounter_Close_Date).format('HH:mm') : null,
			PRO_Patient_Instructions: encounter[0].charts[0].PRO_Patient_Instructions,
			PRO_Plan_Description: encounter[0].charts[0].PRO_Plan_Description,
			Prescriptions: encounter[0].order.filter(ele => ele.Order_Type == 'Prescription Order'),
			Lab_Orders: encounter[0].order.filter(ele => ele.Order_Type == 'Lab Order'),
			Imaging_Orders: encounter[0].order.filter(ele => ele.Order_Type == 'Image Order'),
			Referrals: encounter[0].order.filter(ele => ele.Order_Type == 'Referral Order'),
			Other: encounter[0].order.filter(ele => ele.Order_Type == 'Other Order'),
			Encounter_Followup_Status: followupEncounter ? encounter[0].Encounter_Followup_Status : null,
			Followup_Time: followupEncounter ? `${followupEncounter.Encounter_Start_Time}${followupEncounter.Encounter_Start_Time_Zone}` : null,
			Followup_Date: followupEncounter ? moment(new Date(followupEncounter.Encounter_Date)).format('M/D/YYYY') : null,
			Followup_Day: followupEncounter ? moment(new Date(followupEncounter.Encounter_Date)).format('dddd') : null,
			Followup_Location: followupEncounter ? followupEncounter.Encounter_Location_Type : null,
			Chart_is_Signed: encounter[0].charts[0].Chart_is_Signed,
			PRO_HPI: encounter[0].charts[0].PRO_HPI ? encounter[0].charts[0].PRO_HPI : 'None',
			PT_Allergies: encounter[0].PT_History.PT_Allergies.length > 0 ? encounter[0].PT_History.PT_Allergies : [],
			PT_Medication: encounter[0].PT_History.PT_Medication.length > 0 ? encounter[0].PT_History.PT_Medication : [],
			PT_Medication_Attachment: encounter[0].PT_History.PT_Medication_Attachment.length > 0 ? encounter[0].PT_History.PT_Medication_Attachment : [],
			PT_Medical_Directive: encounter[0].PT_History.PT_Medical_Directives ? encounter[0].PT_History.PT_Medical_Directives : 'None',
			PT_Past_Year_Medical_Changes_Note: encounter[0].PT_History.PT_Past_Year_Medical_Changes_Note ? encounter[0].PT_History.PT_Past_Year_Medical_Changes_Note : 'None',
			PT_Heart_Disease_Note: encounter[0].PT_History.PT_Heart_Disease_Note ? encounter[0].PT_History.PT_Heart_Disease_Note : 'None',
			PT_Hypertension_Note: encounter[0].PT_History.PT_Hypertension_Note ? encounter[0].PT_History.PT_Hypertension_Note : 'None',
			PT_Diabetes_Note: encounter[0].PT_History.PT_Diabetes_Note ? encounter[0].PT_History.PT_Diabetes_Note : 'None',
			PT_Cancer_Note: encounter[0].PT_History.PT_Cancer_Note ? encounter[0].PT_History.PT_Cancer_Note : 'None',
			PT_Immune_Diseases_Note: encounter[0].PT_History.PT_Immune_Diseases_Note ? encounter[0].PT_History.PT_Immune_Diseases_Note : 'None',
			PT_Neuro_Issues_Note: encounter[0].PT_History.PT_Neuro_Issues_Note ? encounter[0].PT_History.PT_Neuro_Issues_Note : 'None',
			PT_Other_Diseases_Note: encounter[0].PT_History.PT_Other_Diseases_Note ? encounter[0].PT_History.PT_Other_Diseases_Note : 'None',
			PT_Surgery_Note: encounter[0].PT_History.PT_Surgery_Note ? encounter[0].PT_History.PT_Surgery_Note : 'None',
			PT_Family_History_Info: encounter[0].PT_History.PT_Family_History_Info.length > 0 ? encounter[0].PT_History.PT_Family_History_Info : 'None',
			PT_Recreational_Drugs_Note: encounter[0].PT_History.PT_Recreational_Drugs_Note ? encounter[0].PT_History.PT_Recreational_Drugs_Note : 'None',
			PT_Tobacco_Usage_Note: encounter[0].PT_History.PT_Tobacco_Usage_Note ? encounter[0].PT_History.PT_Tobacco_Usage_Note : 'None',
			PT_Alcohol_Usage_Note: encounter[0].PT_History.PT_Alcohol_Usage_Note ? encounter[0].PT_History.PT_Alcohol_Usage_Note : 'None',
			PT_Occupation: encounter[0].PT_History.PT_Occupation ? encounter[0].PT_History.PT_Occupation : 'None',
			PT_Last_Menstrual_Cycle_Date: encounter[0].PT_History.PT_Last_Menstrual_Cycle_Date ? moment(new Date(encounter[0].PT_History.PT_Last_Menstrual_Cycle_Date)).format('M/D/YYYY') : 'None',
			PT_Number_Of_Pregnancies: encounter[0].PT_History.PT_Number_Of_Pregnancies ? encounter[0].PT_History.PT_Number_Of_Pregnancies : 0,
			PT_Number_Of_Deliveries: encounter[0].PT_History.PT_Number_Of_Deliveries ? encounter[0].PT_History.PT_Number_Of_Deliveries : 'None',
			PT_Pregnancy_Complications_Note: encounter[0].PT_History.PT_Pregnancy_Complications_Note ? encounter[0].PT_History.PT_Pregnancy_Complications_Note : 'None',
			Pregnant_Now: encounter[0].PT_History.Pregnant_Now ? encounter[0].PT_History.Pregnant_Now : 'No',
			Expected_Delivery_Date: encounter[0].PT_History.Expected_Delivery_Date ? moment(new Date(encounter[0].PT_History.Expected_Delivery_Date)).format('M/D/YYYY') : 'None',
			Nursing_Now: encounter[0].PT_History.Nursing_Now ? encounter[0].PT_History.Nursing_Now : 'No',
			Oral_Contraceptives_Info: encounter[0].PT_History.Oral_Contraceptives_Info ? encounter[0].PT_History.Oral_Contraceptives_Info : 'None',
			PT_Primary_Care_Provider: encounter[0].PT_History.PT_Primary_Care_Provider ? encounter[0].PT_History.PT_Primary_Care_Provider : 'None',
			PT_Organ_Donor: encounter[0].PT_History.PT_Organ_Donor ? encounter[0].PT_History.PT_Organ_Donor : 'No',
			PRO_Review_of_Systems: encounter[0].charts[0].PRO_Review_of_Systems ? encounter[0].charts[0].PRO_Review_of_Systems : 'None',
			PT_Height: encounter[0].patients.PT_Height ? `${encounter[0].patients.PT_Height} CM` : '-',
			PT_Height_Inches: encounter[0].patients.PT_Height ? toFeet(encounter[0].patients.PT_Height) : '-',
			PT_Weight: encounter[0].patients.PT_Weight ? `${encounter[0].patients.PT_Weight} KG` : '-',
			PT_Weight_Lbs: encounter[0].patients.PT_Weight ? `${kToLbs(encounter[0].patients.PT_Weight)}lbs` : '-',
			PT_BMI: encounter[0].patients.PT_BMI ? encounter[0].patients.PT_BMI : '-',
			PT_Blood_Glucose: encounter[0].patients.PT_Blood_Glucose ? encounter[0].patients.PT_Blood_Glucose : '-',
			PT_O2_Sat: encounter[0].patients.PT_O2_Sat ? `${encounter[0].patients.PT_O2_Sat}%` : '-',
			PT_Temp: encounter[0].patients.PT_Temp ? `${encounter[0].patients.PT_Temp}C` : '-',
			PT_Temp_Fahrenheit: encounter[0].patients.PT_Temp ? `${((encounter[0].patients.PT_Temp * 1.8) + 32).toFixed(1)}F` : '-',
			PT_Heart_Rt: encounter[0].patients.PT_Heart_Rt ? `${encounter[0].patients.PT_Heart_Rt} BPM` : '-',
			PT_Resp: encounter[0].patients.PT_Resp ? encounter[0].patients.PT_Resp : '-',
			Pressure: (encounter[0].patients.PT_Systolic_BP == 0 && encounter[0].patients.PT_Diastolic_BP == 0) ? '-' : `${encounter[0].patients.PT_Diastolic_BP}/${encounter[0].patients.PT_Systolic_BP}`,
			PRO_Exam_Notes: encounter[0].charts[0].PRO_Exam_Notes ? encounter[0].charts[0].PRO_Exam_Notes : 'None',
			PRO_Results_Note: encounter[0].charts[0].PRO_Results_Note ? encounter[0].charts[0].PRO_Results_Note : 'None',
			Diagnosis: encounter[0].charts[0].PRO_Diagnosis.length > 0 ? encounter[0].charts[0].PRO_Diagnosis[0].PRO_Diagnosis_Note : 'None',
			PRO_Diagnosis_ICD_Code: encounter[0].charts[0].PRO_Diagnosis_ICD_Code ? encounter[0].charts[0].PRO_Diagnosis_ICD_Code : [],
			PRO_Addendums: encounter[0].charts[0].PRO_Addendums,
			order: encounter[0].order,
			PRO_Private_Note: encounter[0].charts[0].PRO_Private_Note,
			all_url: all_url,
			arr: arr.filter(ele => ele.key == 'Recent Medical History / Medical Directive' || ele.key == 'Recent Medical History / Medical History')
		}
		let template = $.templates('./download/full_chart.html')

		// send chart data to the HTML template
		let html = template.render(fullChartData)
		const opts = {
			headless: true,
			timeout: 15000,
			args: ['--no-sandbox']
		}
		const browser = await puppeteer.launch(opts)
		const page = await browser.newPage()
		await page.setContent(html)
		const pdf = await page.pdf({
			// path: 'fullChart.pdf',
			format: 'A4',
			printBackground: true,
			displayHeaderFooter: true,
			footerTemplate: `<div  style="width:100%; font-size: 11px !important;  padding:0px 60px 25px 60px !important; page-break-after: always; font-family: 'Roboto', sans-serif;">
     <div style="border-top:1px solid #ddd;">
      <div style = "float: left; margin:0px 0px;  ">
      <div >${fullChartData.PT_First_Name} ${fullChartData.PT_Last_Name} &nbsp; DOB: <span> </span>${fullChartData.PT_DOB}</div>
      <div >${fullChartData.Encounter_UID}</div>
    </div>
    <div style = "float: right; margin:0px 0px; ">
    <div class=\"page-footer\" style=\"width:100%; text-align:right; \">Page <span class=\"pageNumber\" "></span> of <span class=\"totalPages\"></span></div>
    </div>
    </div>
    </div>`,
			margin: {
				bottom: '30mm'
			}
		})
		await browser.close()
		var imagename = `full_chart_${Date.now()}.pdf`
		const params = {
			Bucket: process.env.AWS_BUCKET,
			Key: imagename,
			Body: pdf,
			ContentType: 'application/pdf',
			ACL: 'public-read'
		}

	    // upload PDF to the AWS bucket
		s3.upload(params, function (s3Err, data) {
			if (s3Err) throw s3Err
			res.send({ code: 1, status: 'success', data: data.Location })
		})
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS048' })
	}
})

// function for get length in feet
function toFeet (n) {
	var realFeet = ((n * 0.393700) / 12)
	var feet = Math.floor(realFeet)
	var inches = Math.round((realFeet - feet) * 12)
	return `${feet}'${inches}"`
}


// function for change weight to killogram to Lbs
function kToLbs (pK) {
	var nearExact = pK / 0.45359237
	var lbs = Math.floor(nearExact)
	return lbs
}

// Get file urls for documents //
var getImageUrls = async (arr) => {
	return new Promise(async (resolve, reject) => {
		let newArray = []; let count = 0
		for (var i = 0; i < arr.length; i++) {
			let url = arr[i].Attachment_Name != null || arr[i].Attachment_Name != '' ? await serviceModule.getImage(arr[i].Attachment_Name) : null
			if (arr[i].Is_Deleted == false) {
				newArray.push({
					Attachment_Url: url,
					Attachment_Name: arr[i].Attachment_Name,
					key: arr[i].key
				})
			}
			count = count + 1
			if (count == arr.length) {
				resolve(newArray)
			}
		}
	})
}

// function for get addendum array with image url
async function addendumArray (array) {
	let arr = []
	return new Promise(async (resolve, reject) => {
		if (array.length != 0) {
			arr = await getImageUrls(array)
			resolve(arr)
		} else {
			resolve(arr)
		}
	})
}
module.exports = router
