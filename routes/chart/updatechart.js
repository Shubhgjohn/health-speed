/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const chartModule = require('../../models').Chart
const errorModule = require('../../models').Errors
const encounterModule = require('../../models').Encounter
const notificationModule = require('../../models').Notification
const patientHxModule = require('../../models').PatientHistory
const icd10Module = require('../../models').ICD10
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const logger = require('../../models/errorlog')
const moment = require('moment-timezone')
const fcm = require('../../_helpers/fcm')
const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_PRIVATE_KEY)

// route for update-chart with encounter UID
router.put('/update-chart/:Encounter_UID', authorize(Role.Provider), async (req, res) => {
	var { body: { data }, params: { Encounter_UID } } = req
	if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required', Error_Code: errorModule.Encounter_UID_required() })
	if (!data) return res.send({ code: 0, status: 'failed', message: 'PT_Username is required', Error_Code: errorModule.PT_Username_required() })
	req.body.PRO_UID = req.user.PRO_UID
	try {
		// Get encounter data from the encounter UID
		let encounter = await encounterModule.getEncounterByEncounter_UID(Encounter_UID)
		if (!encounter) return res.send({ code: 0, status: 'failed', message: `encounter with id '${Encounter_UID}' not found!`, Error_Code: errorModule.Encounter_not_found() })

		// Get chart data with encounter UID
		let chart = await chartModule.getChartData(Encounter_UID)

		// if chart is present in the system
		if (chart) {
			let d = new Date()
			var newYork = moment.tz(d, process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm:ss')
			let updatedAt = req.headers.host.includes('localhost') ? moment(Date.parse(newYork)).add(330, 'm').toDate() : moment(Date.parse(newYork)).toDate()
			if (data.PRO_Diagnosis_ICD_Code && data.PRO_Diagnosis_ICD_Code.length > 0) {
				if (data.PRO_Diagnosis_ICD_Code.length > chart.PRO_Diagnosis_ICD_Code.length) {
					let icd10 = data.PRO_Diagnosis_ICD_Code[data.PRO_Diagnosis_ICD_Code.length - 1]
					let ICDorder = await icd10Module.getICDOrderByCode(icd10.PRO_Diagnosis_ICD10_Code)
					icd10.PRO_Diagnosis_ICD10_Code_Order = ICDorder[0].PRO_Diagnosis_ICD10_Code_Order
					icd10.PRO_Diagnosis_ICD10_Code_Description = ICDorder[0].PRO_Diagnosis_ICD10_Code_Description
					if (icd10 == null) {
						res.send({
							code: 0,
							status: 'failed',
							message: 'ICD code can not be null'
						})
					}
					await chartModule.saveICD10Code(Encounter_UID, icd10, updatedAt)
				} else {
					let arr = []
					let count = 0
					data.PRO_Diagnosis_ICD_Code.forEach(async element => {
						let ICD = chart.PRO_Diagnosis_ICD_Code.find(ele => ele.PRO_Diagnosis_ICD10_Code == element.PRO_Diagnosis_ICD10_Code)
						arr.push(ICD)
						count = count + 1
						if (count == data.PRO_Diagnosis_ICD_Code.length) {
							data.PRO_Diagnosis_ICD_Code = arr
							await chartModule.updateChart(Encounter_UID, data, updatedAt)
						}
					})
				}
			} else {
				// Update chart with data and the encounter UID
				await chartModule.updateChart(Encounter_UID, data, updatedAt)
			}
			if (data.PRO_Diagnosis_ICD_Code) {
				let updatedChart = await chartModule.getChartData(Encounter_UID)
				let Primary_DX = updatedChart.PRO_Diagnosis_ICD_Code.length > 0 ? updatedChart.PRO_Diagnosis_ICD_Code[0].PRO_Diagnosis_ICD10_Code : null
				await encounterModule.updateEncounter(Encounter_UID, { Primary_DX: Primary_DX })
			}

			if (data.Chart_is_Signed) {
				let patientHistory = await patientHxModule.findPatientHistoryById(encounter.PT_HS_MRN)
				if (patientHistory.Updated_At.getTime() < encounter.PT_History.Updated_At.getTime()) {

					let message = {
						to: encounter.PT_Username,
						from: process.env.ADMIN_FROM_EMAIL,
						template_id: process.env.PATIENT_MEDICAL_HISTORY_UPDATE_TEMPLATE,
						dynamic_template_data: {
							url: `${process.env.API_HOST}/patient/main/editsuggest/${encounter.Encounter_UID}/history`
						}
					}
					// send mail to patient for suggestions
					await sgMail.send(message)
				}
			}
			// if the requested data have Instructions data then we have to send notification
			// if (data.PRO_Patient_Instructions || (data.PRO_Diagnosis && data.PRO_Diagnosis[0].PRO_Diagnosis_Note)) {
			// 	let PT_Token = await notificationModule.getToken('Patient', encounter.PT_Username)
			// 	let arr = []
			// 	PT_Token.map(ele => arr.push(ele.Device_Token))
			// 	if (arr.length != 0) {
			// 		let payload = {
			// 			registration_ids: arr,
			// 			notification: {
			// 				title: 'Visit Instructions Updated',
			// 				body: 'Checkout the changes made to your visit instructions.'
			// 			},

			// 			data: {
			// 				Encounter_UID: Encounter_UID,
			// 				PRO_UID: encounter.PRO_UID,
			// 				title: 'Discharge Instructions'
			// 			}
			// 		}

			// 		// send notification for the Instructions update
			// 		fcm.send(payload, function (err, response) {
			// 			if (err) {
			// 			} else console.log(response)
			// 		})
			// 	}
			// }
			res.send({
				code: 1,
				status: 'sucess',
				message: 'Chart update successfully'
			})
		} else {
			res.send({
				code: 0,
				status: 'failed',
				message: `No chart data found for encounter UID '${Encounter_UID}'`,
				Error_Code: errorModule.No_chart_found()
			})
		}
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS055' })
	}
})

module.exports = router
