/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const model = require('../../models')
const { auditUid } = require('../../utils/UID')
const auditType = require('../../auditType.json')
var ObjectId = require('mongodb').ObjectID
const errorModule = require('../../models').Errors
const patientModule = require('../../models').Patient
const providerModule = require('../../models').Provider
const serviceModule = require('../../models').CommonServices
const notificationModule = require('../../models').Notification
const moment = require('moment-timezone')
const fcm = require('../../_helpers/fcm')
const logger = require('../../models/errorlog')
var AWS = require('aws-sdk')
AWS.config.update({
	accessKeyId: process.env.AWS_ACCESS_KEY,
	secretAccessKey: process.env.AWS_SECRET_KEY,
	region: 'us-east-1'
})
var s3 = new AWS.S3()

// route for create and update addendum data
router.post('/create-update-addendum/:Encounter_UID', authorize(Role.Provider), async (req, res) => {
	var { body: { data }, params: { Encounter_UID } } = req
	if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required', Error_Code: errorModule.Encounter_UID_required() })
	if (!data) return res.send({ code: 0, status: 'failed', message: 'data is required', Error_Code: errorModule.data_required() })
	req.body.PRO_UID = req.user.PRO_UID
	try {

		// Check encounter with this encounter UID is available in the system or not
		let encounter = await model.Encounter.getEncounterByEncounter_UID(Encounter_UID)
		if (encounter) {

			// check patient of this encounter is exist in the syatem
			let patient = await patientModule.findById(encounter.PT_HS_MRN)
			// check provider of this encounter is exist in the syatem
			let provider = await providerModule.findById(encounter.PRO_UID)

			// Get chart data from the encounter UID
			let chart = await model.Chart.getChartData(Encounter_UID)
			if (!chart) return res.send({ code: 0, status: 'failed', message: `No chart is found for encounter '${Encounter_UID}'`, Error_Code: errorModule.No_chart_found() })
			
			// if chart is not signed for this encounter we can not create addendum for that return error
			if (!chart.Chart_is_Signed) {
				return res.send({
					code: 0,
					status: 'failed',
					message: 'You cannot create addendum for this encounter because chart is not signed yet.',
					Error_Code: 'HS038'
				})
			}
			if (chart) {
				if (!data._id) {
					let x = ObjectId()
					data._id = x
					let d = new Date()
					var newYork = moment.tz(d, process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm:ss')
					let updatedAt = req.headers.host.includes('localhost') ? moment(Date.parse(newYork)).add(330, 'm').toDate() : moment(Date.parse(newYork)).toDate()
					
					// Create addendum for this chart
					await model.Chart.createAddendum(Encounter_UID, data, updatedAt)
					await createAudit(req)
					res.send({
						code: 1,
						status: 'sucess',
						message: 'Addendum added successfully',
						_id: x
					})
				} else {

					// Get chart for this encounter UID
					let chartData = await model.Chart.getChartData(Encounter_UID)
					let addendum = chartData.PRO_Addendums.find(ele => ele._id == data._id)
					let ICD10_Code = addendum.ICD10_Code
					let PRO_Addendum_Attachment = addendum.PRO_Addendum_Attachment.length > 0 ? await serviceModule.getFileUrl(addendum.PRO_Addendum_Attachment) : []
					let jsonData = {
						PT_Profile_Picture: patient.PT_Profile_Picture ? await getImage(patient.PT_Profile_Picture) : '',
						PRO_Profile_Picture: provider.PRO_Profile_Picture ? await getImage(provider.PRO_Profile_Picture) : '',
						PRO_Gender: provider.PRO_Gender ? provider.PRO_Gender : '',
						PRO_First_Name: provider.PRO_First_Name ? provider.PRO_First_Name : '',
						PRO_Last_Name: provider.PRO_Last_Name ? provider.PRO_Last_Name : '',
						PT_First_Name: patient.PT_First_Name ? patient.PT_First_Name : '',
						PT_Last_Name: patient.PT_Last_Name ? patient.PT_Last_Name : '',
						Encounter_Date: encounter.Encounter_Date,
						Encounter_Time: encounter.Encounter_Time,
						Encounter_UID: encounter.Encounter_UID,
						Encounter_Chief_Complaint_PT: encounter.Encounter_Chief_Complaint_PT ? encounter.Encounter_Chief_Complaint_PT : '',
						Encounter_Chief_Complaint_PRO: encounter.Encounter_Chief_Complaint_PRO ? encounter.Encounter_Chief_Complaint_PRO : '',
						PRO_Addendum_Note: addendum.PRO_Addendum_Note ? addendum.PRO_Addendum_Note : '',
						ICD10_Code: JSON.stringify(ICD10_Code),
						PRO_Addendum_Attachment: JSON.stringify(PRO_Addendum_Attachment),
						title: 'Addendum Added'
					}
					let d = new Date()
					let newYork = moment.tz(d, process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm:ss')
					data.Addendum_Sign_Date = req.headers.host.includes('localhost') ? moment(Date.parse(newYork)).add(330, 'm').toDate() : moment(Date.parse(newYork)).toDate()
					// update addendum 
					await model.Chart.updateAddendum(Encounter_UID, data)
					if (data.Addendum_Is_Signed && data.Addendum_Is_Signed == true) {
						let PT_Token = await notificationModule.getToken('Patient', encounter.PT_Username)
						let arr = []
						PT_Token.map(ele => arr.push(ele.Device_Token))
						if (arr.length != 0) {
							var payload = { // this may vary according to the message type (single recipient, multicast, topic, et cetera)
								registration_ids: arr,
								notification: {
									title: 'Addendum Added',
									body: `Dr. ${req.user.PRO_Last_Name} has posted an addendum to your visit, best check it out!`
								},

								data: jsonData
							}

							// send notification for Addendum Added
							fcm.send(payload, function (err, response) {
								if (err) {
								} else console.log(response)
							})
						}
					}
					await createAudit(req, data._id)
					res.send({
						code: 1,
						status: 'sucess',
						message: 'Addendum Updated successfully'
					})
				}
			} else {
				res.send({
					code: 0,
					status: 'failed',
					message: 'No chart data found for this encounter',
					Error_Code: errorModule.No_chart_found()
				})
			}
		} else {
			res.send({
				code: 0,
				status: 'failed',
				message: `No encounter found with ${Encounter_UID}`,
				Error_Code: errorModule.Encounter_not_found()
			})
		}
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS039' })
	}
})


// function for create audit
async function createAudit (req, id) {
	const auditData = {
		Audit_UID: await auditUid(),
		Audit_Info: 'New- ' + await auditUid(),
		Audit_Type: auditType.add,
		Data_Point: id ? 'PRO/update addendum' : 'PRO/add addendum',
		PRO_First_Name: req.user.PRO_First_Name,
		PRO_Last_Name: req.user.PRO_Last_Name,
		PRO_UID: req.user.PRO_UID,
		IP_Address: await model.Audit.getIpAddress(req)
	}
	await model.Audit.create(auditData)
}


// function for get image URL from image key
let getImage = async (imageKey) => {
	if (imageKey == null || imageKey == '') {
		return ''
	} else {
		if (imageKey.includes('pdf')) {
			const url = s3.getSignedUrl('getObject', {
				Bucket: process.env.AWS_BUCKET,
				Key: imageKey,
				ResponseContentType: 'application/pdf',
				Expires: 60 * 60 * 24 * 15 // time in seconds: e.g. 60 * 5 = 5 mins
			})
			return url
		} else {
			const url = s3.getSignedUrl('getObject', {
				Bucket: process.env.AWS_BUCKET,
				Key: imageKey,
				Expires: 60 * 60 * 24 * 15 // time in seconds: e.g. 60 * 5 = 5 mins
			})
			return url
		}
	}
}

module.exports = router
