/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const chartModule = require('../../models').Chart
const patientModule = require('../../models').Patient
const providerModule = require('../../models').Provider
const errorModule = require('../../models').Errors
const encounterModule = require('../../models').Encounter
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const logger = require('../../models/errorlog')
const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_PRIVATE_KEY)
const momentTz = require('moment-timezone')

// route for create chart
router.post('/create', authorize(Role.Provider), async (req, res) => {
	var { body: { Encounter_UID, PT_Username } } = req
	if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required', Error_Code: errorModule.Encounter_UID_required() })
	if (!PT_Username) return res.send({ code: 0, status: 'failed', message: 'PT_Username is required', Error_Code: errorModule.PT_Username_required() })
	req.body.PRO_UID = req.user.PRO_UID
	try {

		// check encounter is available in the system from encounter UID
		let encounter = await encounterModule.getEncounterByEncounter_UID(Encounter_UID)

		// if encounter is not available
		if (!encounter) return res.send({ code: 0, status: 'failed', message: `encounter with id '${Encounter_UID}' not found!`, Error_Code: errorModule.Encounter_not_found() })
		
		// find encounter chart
		let chart = await chartModule.findEncounterChart(Encounter_UID, req.user.PRO_UID, PT_Username)
		// find patient with PT_Username
		let patient = await patientModule.findOneByEmail(PT_Username)
		if (!patient) return res.send({ code: 0, status: 'failed', message: `patient with email '${PT_Username}' not found!`, Error_Code: errorModule.patient_not_found() })
		
		// if chart is already created
		if (chart) {
			res.send({
				code: 0,
				status: 'failed',
				message: 'Chart is already created for this encounter',
				Error_Code: errorModule.Chart_already_created()
			})
		} else {
			req.body.PT_HS_MRN = encounter.PT_HS_MRN
			req.body.PRO_Chart_Access = { PRO_UID: req.user.PRO_UID }

			// create chart
			await chartModule.create(req.body)
			res.send({
				code: 1,
				status: 'sucess',
				message: 'Chart created successfully'
			})
		}
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS037' })
	}
})

// This service is for chart reminder of cron job
router.get('/chart-reminder', async (req, res) => {
	try {

		// Get all unsigned/false chart
		let filterData = await chartModule.getUnsignedChart()
		filterData.map(async ele => {
			// find provider from PRO_UID
			let provider = await providerModule.findById(ele.PRO_UID)
			let hours = Math.round(Math.abs(Date.parse(momentTz.tz(new Date(), process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm')) - Date.parse(momentTz.tz(ele.Updated_At, process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm'))) / 36e5)
			if (provider.PRO_Notifications == 'Yes') {
				if ((hours > 23 && hours < 25) || (hours > 47 && hours < 49)) {
					let msg = {
						from: process.env.ADMIN_FROM_EMAIL,
						to: provider.PRO_Username,
						template_id: process.env.Notify_Provider_On_Chart_Reminder,
						dynamic_template_data: {
							url: `${process.env.API_HOST}/provider/main/${ele.Encounter_UID}/chart`
						}
					}

					// send mail for send chart reminder
					sgMail.send(msg)
				}
			}
		})
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 0, status: 'failed', message: error.message })
	}
})

module.exports = router
