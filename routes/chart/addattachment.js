/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const chartModule = require('../../models').Chart
const errorModule = require('../../models').Errors
const serviceModule = require('../../models').CommonServices
const encounterModule = require('../../models').Encounter
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const logger = require('../../models/errorlog')

// route for upload attachement in chart
router.post('/add-attachment/:Encounter_UID', authorize(Role.Provider), async (req, res) => {
  var { body: { data }, params: { Encounter_UID } } = req
  if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required', Error_Code: errorModule.Encounter_UID_required() })
  if (!data) return res.send({ code: 0, status: 'failed', message: 'data is required', Error_Code: errorModule.data_required() })

  try {

    // Get encounter by encounter UID
    let encounter = await encounterModule.getEncounterByEncounter_UID(Encounter_UID)
    if (!encounter) return res.send({ code: 0, status: 'failed', message: `encounter with id '${Encounter_UID}' not found!`, Error_Code: errorModule.Encounter_not_found() })
    
    // Get chart data from encounter UID
    let chart = await chartModule.getChartData(Encounter_UID)

    // if chart is available
    if (chart) {
      let attachmentKey = Object.keys(req.body.data)[0]
      let attachmantData = {
        Attachment_Name: data[attachmentKey].Attachment_Name,
        Attachment_By: req.userType,
        Attachment_User: req.userType == 'Provider' ? req.user.PRO_UID : req.user.PT_HS_MRN
      }

      // upload attachment in chart
      await chartModule.uploadAttachment(Encounter_UID, attachmantData, attachmentKey)
      let updatedChart = await chartModule.getChartData(Encounter_UID)
      let updatedAttachmentArray = updatedChart[attachmentKey]
      let json = {}
      updatedAttachmentArray = updatedAttachmentArray.length > 0 ? await serviceModule.getFileUrl(updatedAttachmentArray) : []
      json[`${attachmentKey}`] = updatedAttachmentArray
      res.send({
        code: 1,
        status: 'sucess',
        data: json,
        message: 'Attachment upload successfully'
      })
    } else {
      let attachmentKey = Object.keys(req.body.data)[0]
      let attachmantData = {
        Attachment_Name: data[attachmentKey].Attachment_Name,
        Attachment_By: req.userType,
        Attachment_User: req.userType == 'Provider' ? req.user.PRO_UID : req.user.PT_HS_MRN
      }
      // eslint-disable-next-line no-redeclare
      var data = {}
      data.PRO_UID = req.user.PRO_UID
      data.Encounter_UID = Encounter_UID
      data[attachmentKey] = attachmantData

      // create chart with data
      let createdChart = await chartModule.create(data)
      let json = {}
      createdChart[attachmentKey] = createdChart[attachmentKey].length > 0 ? await serviceModule.getFileUrl(createdChart[attachmentKey]) : []
      json[attachmentKey] = createdChart[attachmentKey]
      res.send({
        code: 1,
        status: 'sucess',
        data: json,
        message: 'Attachment upload successfully'
      })
    }
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS031' })
  }
})

module.exports = router
