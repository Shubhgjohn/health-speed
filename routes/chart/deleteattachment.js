/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const model = require('../../models')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const logger = require('../../models/errorlog')

// route for delete-attachment in chart 
router.delete('/delete-attachment/:Encounter_UID/:_id', authorize(Role.Provider), async (req, res) => {
  var { params: { Encounter_UID, _id }, body: { key } } = req
  if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required', Error_Code: model.Errors.Encounter_UID_required() })
  if (!_id) return res.send({ code: 0, status: 'failed', message: '_id is required!', Error_Code: model.Errors._id_required() })
  if (!key) return res.send({ code: 0, status: 'failed', message: 'key is required!', Error_Code: model.Errors.key_required() })
  try {

    // Check encounter with this encounter UID is available in the system or not 
    let encounter = await model.Encounter.getEncounterByEncounter_UID(Encounter_UID)
    if (!encounter) return res.send({ code: 0, status: 'failed', message: `encounter with id '${Encounter_UID}' not found!`, Error_Code: model.Errors.Encounter_not_found() })
   
    // find encounter chart with encounter UID
    let chart = model.Chart.findEncounterChartById(Encounter_UID)
    if (chart) {
      switch (key) {
        case 'PRO_Review_of_Systems_Attachment':
          await model.Chart.deleteReviewofSystemsAttchment(Encounter_UID, _id)
          res.send({
            code: 1,
            status: 'sucess',
            message: 'Review of system attachment deleted successfully'
          })
          break
        case 'PRO_Exam_Notes_Attachment':
          await model.Chart.deleteExamNotesAttchment(Encounter_UID, _id)
          res.send({
            code: 1,
            status: 'sucess',
            message: 'Exam notes attachment deleted successfully'
          })
          break
        case 'PRO_Results_Note_Attachment':
          await model.Chart.deleteResultsNoteAttchment(Encounter_UID, _id)
          res.send({
            code: 1,
            status: 'sucess',
            message: 'Result note attachment deleted successfully'
          })
          break
        case 'PRO_Diagnosis_Attachment':
          await model.Chart.deleteDiagnosisAttchment(Encounter_UID, _id)
          res.send({
            code: 1,
            status: 'sucess',
            message: 'Diagnosis attachment deleted successfully'
          })
          break
        case 'PRO_Plan_Attachment':
          await model.Chart.deletePlanAttchment(Encounter_UID, _id)
          res.send({
            code: 1,
            status: 'sucess',
            message: 'plan attachment deleted successfully'
          })
          break
        case 'PRO_Addendum_Attachment':
          await model.Chart.deleteAddendumAttchment(Encounter_UID, _id, req.body.Addendum_id)
          res.send({
            code: 1,
            status: 'sucess',
            message: 'Addendum attachment deleted successfully'
          })
          break
        case 'PRO_HPI_Attachment':
          await model.Chart.deleteHPIAttchment(Encounter_UID, _id)
          res.send({
            code: 1,
            status: 'sucess',
            message: 'HPI attachment deleted successfully'
          })
          break

        default:
          return res.json({
            code: 0,
            status: 'failed',
            message: 'unsucessful attampt for delete attachment',
            Error_Code: 'HS045'
          })
      }
    } else {
      res.send({
        code: 0,
        status: 'failed',
        message: `No chart found for encounter UID ${Encounter_UID}`,
        Error_Code: model.Errors.No_chart_found()
      })
    }
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS046' })
  }
})

module.exports = router
