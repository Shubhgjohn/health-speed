/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const chartModule = require('../../models').Chart
const auditModule = require('../../models').Audit
const errorModule = require('../../models').Errors
const encounterModule = require('../../models').Encounter
const serviceModule = require('../../models').CommonServices
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const { auditUid } = require('../../utils/UID')
const auditType = require('../../auditType.json')
const logger = require('../../models/errorlog')

// route for delete-addendum from encounter UID
router.delete('/delete-addendum/:Encounter_UID', authorize(Role.Provider), async (req, res) => {
  var { params: { Encounter_UID } } = req
  if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required', Error_Code: errorModule.Encounter_UID_required() })
  
  // Check encounter with this encounter UID is available in the system
  let encounter = await encounterModule.getEncounterByEncounter_UID(Encounter_UID)
  
  // if not available
  if (!encounter) return res.send({ code: 0, status: 'failed', message: `encounter with id '${Encounter_UID}' not found!`, Error_Code: errorModule.Encounter_not_found() })
  if (!req.body._id) return res.send({ code: 0, status: 'failed', message: '_id is required!', Error_Code: errorModule._id_required() })
  try {

    // Get addendum data from addendum _id
    let addendum = await chartModule.findAddendume(req.body._id)
    if (addendum.length == 0) return res.send({ code: 0, status: 'failed', message: 'Addendum id is deleted or invalid!', Error_Code: 'HS040' })
    
    // Get chart data from encounter UID
    let chart = await chartModule.getChartData(Encounter_UID)

    // if chart is available in the system
    if (chart) {

      // delete addendum
      await chartModule.deleteAddendum(Encounter_UID, req.body._id)
      const auditData = {
        Audit_UID: await auditUid(),
        Audit_Info: 'New- ' + await auditUid(),
        Audit_Type: auditType.add,
        Data_Point: 'PRO/delete addendum',
        PRO_First_Name: req.user.PRO_First_Name,
        PRO_Last_Name: req.user.PRO_Last_Name,
        PRO_UID: req.user.PRO_UID,
        IP_Address: await auditModule.getIpAddress(req)
      }
      await auditModule.create(auditData)
      res.send({
        code: 1,
        status: 'sucess',
        message: 'Addendum is deleted successfully'
      })
    } else {
      res.send({
        code: 0,
        status: 'failed',
        message: `No chart find with for encounter UID ${Encounter_UID}`,
        Error_Code: errorModule.No_chart_found()
      })
    }
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS041' })
  }
})


// route for delete addendum attachment from encounter UID
router.delete('/delete-addendum-attachment/:Encounter_UID', authorize(Role.Provider), async (req, res) => {
  var { params: { Encounter_UID } } = req
  if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required', Error_Code: errorModule.Encounter_UID_required() })
  if (!req.body.Addendum_Id) return res.send({ code: 0, status: 'failed', message: 'Addendum_Id is required!', Error_Code: errorModule.Addendum_Id_required() })
  if (!req.body.Attachment_Id) return res.send({ code: 0, status: 'failed', message: 'Attachment_Id is required!', Error_Code: errorModule.Attachment_Id_required() })

  try {
    // check encounter with this encounter UID is available in the system
    let encounter = await encounterModule.getEncounterByEncounter_UID(Encounter_UID)
    if (!encounter) return res.send({ code: 0, status: 'failed', message: `encounter with id '${Encounter_UID}' not found!`, Error_Code: errorModule.Encounter_not_found() })
    
    // Check chart is available in system
    let chart = await chartModule.getChartData(Encounter_UID)

    // if chart is available
    if (chart) {
      await chartModule.deleteAddendumAttachment(Encounter_UID, req.body.Addendum_Id, req.body.Attachment_Id)
      const auditData = {
        Audit_UID: await auditUid(),
        Audit_Info: 'New- ' + await auditUid(),
        Audit_Type: auditType.add,
        Data_Point: 'PRO/delete addendum attachment',
        PRO_First_Name: req.user.PRO_First_Name,
        PRO_Last_Name: req.user.PRO_Last_Name,
        PRO_UID: req.user.PRO_UID,
        IP_Address: await auditModule.getIpAddress(req)
      }
      await auditModule.create(auditData)

      // Get updated chart data
      let updatedChart = await chartModule.getChartData(Encounter_UID)
      updatedChart = updatedChart.PRO_Addendums.find(ele => ele._id == req.body.Addendum_Id)
      let addendumAttachment = updatedChart.PRO_Addendum_Attachment.length > 0 ? await serviceModule.getFileUrl(updatedChart.PRO_Addendum_Attachment) : []
      res.send({
        code: 1,
        status: 'sucess',
        message: 'Addendum Attachment is deleted successfully',
        data: addendumAttachment
      })
    } else {
      res.send({
        code: 0,
        status: 'failed',
        message: `No chart find with for encounter UID ${Encounter_UID}`,
        Error_Code: errorModule.No_chart_found()
      })
    }
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.send({ code: 0, status: 'failed', message: error.message, Error_Code: 'HS043' })
  }
})

module.exports = router
