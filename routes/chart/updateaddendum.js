/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const chartModule = require('../../models').Chart
const auditModule = require('../../models').Audit
const errorModule = require('../../models').Error
const patientModule = require('../../models').Patient
const providerModule = require('../../models').Provider
const encounterModule = require('../../models').Encounter
const serviceModule = require('../../models').CommonServices
const notificationModule = require('../../models').Notification
const authorize = require('../../_helpers/authorize')
const logger = require('../../models/errorlog')
const Role = require('../../_helpers/role')
const { auditUid } = require('../../utils/UID')
const auditType = require('../../auditType.json')
const fcm = require('../../_helpers/fcm')
var AWS = require('aws-sdk')
AWS.config.update({
	accessKeyId: process.env.AWS_ACCESS_KEY,
	secretAccessKey: process.env.AWS_SECRET_KEY,
	region: 'us-east-1'
})
var s3 = new AWS.S3()


// route for sign addendum
router.put('/sign-addendum/:Encounter_UID', authorize(Role.Provider), async (req, res) => {
	var { params: { Encounter_UID } } = req
	if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required', Error_Code: errorModule.Encounter_UID_required() })
	if (!req.body.Addendum_Id) return res.send({ code: 0, status: 'failed', message: 'Addendum_Id is required!', Error_Code: errorModule.Addendum_Id_required() })
	try {

		// Check encounter with this encounter UID is available in the system
		let encounter = await encounterModule.getEncounterByEncounter_UID(Encounter_UID)
		if (!encounter) return res.send({ code: 0, status: 'failed', message: `encounter with id '${Encounter_UID}' not found!`, Error_Code: errorModule.Encounter_not_found() })
		
		// Get the chart data the encounter UID
		let chart = await chartModule.getChartData(Encounter_UID)

		// if chart is available
		if (chart) {
			await chartModule.signAddendum(Encounter_UID, req.body.Addendum_Id)

			// find encounter patient details
			let patient = await patientModule.findById(encounter.PT_HS_MRN)
			// find encounter provider details
			let provider = await providerModule.findById(encounter.PRO_UID)
			let addendum = chart.PRO_Addendums.find(ele => ele._id == req.body.Addendum_Id)
			let ICD10_Code = addendum.ICD10_Code
			let PRO_Addendum_Attachment = addendum.PRO_Addendum_Attachment.length > 0 ? await serviceModule.getFileUrl(addendum.PRO_Addendum_Attachment) : []
			let jsonData = {
				PT_Profile_Picture: patient.PT_Profile_Picture ? await getImage(patient.PT_Profile_Picture) : '',
				PRO_Profile_Picture: provider.PRO_Profile_Picture ? await getImage(provider.PRO_Profile_Picture) : '',
				PRO_Gender: provider.PRO_Gender ? provider.PRO_Gender : '',
				PRO_First_Name: provider.PRO_First_Name ? provider.PRO_First_Name : '',
				PRO_Last_Name: provider.PRO_Last_Name ? provider.PRO_Last_Name : '',
				PT_First_Name: patient.PT_First_Name ? patient.PT_First_Name : '',
				PT_Last_Name: patient.PT_Last_Name ? patient.PT_Last_Name : '',
				Encounter_Date: encounter.Encounter_Date,
				Encounter_Time: encounter.Encounter_Time,
				Encounter_UID: encounter.Encounter_UID,
				Encounter_Chief_Complaint_PT: encounter.Encounter_Chief_Complaint_PT ? encounter.Encounter_Chief_Complaint_PT : '',
				Encounter_Chief_Complaint_PRO: encounter.Encounter_Chief_Complaint_PRO ? encounter.Encounter_Chief_Complaint_PRO : '',
				PRO_Addendum_Note: addendum.PRO_Addendum_Note ? addendum.PRO_Addendum_Note : '',
				ICD10_Code: JSON.stringify(ICD10_Code),
				PRO_Addendum_Attachment: JSON.stringify(PRO_Addendum_Attachment),
				title: 'Addendum Added'
			}
			let PT_Token = await notificationModule.getToken('Patient', encounter.PT_Username)
			let arr = []
			PT_Token.map(ele => arr.push(ele.Device_Token))
			if (arr.length != 0) {
				var payload = { // this may vary according to the message type (single recipient, multicast, topic, et cetera)
					registration_ids: arr,
					notification: {
						title: 'Addendum Added',
						body: `Dr. ${req.user.PRO_Last_Name} has posted an addendum to your visit, best check it out!`
					},

					data: jsonData
				}

				// Send notification for sign addendum
				fcm.send(payload, function (err, response) {
					if (err) {
					} else console.log(response)
				})
			}
			const auditData = {
				Audit_UID: await auditUid(),
				Audit_Info: 'New- ' + await auditUid(),
				Audit_Type: auditType.add,
				Data_Point: 'PRO/sign addendum attachment',
				PRO_First_Name: req.user.PRO_First_Name,
				PRO_Last_Name: req.user.PRO_Last_Name,
				PRO_UID: req.user.PRO_UID,
				IP_Address: await auditModule.getIpAddress(req)
			}
			await auditModule.create(auditData)

			res.send({
				code: 1,
				status: 'sucess',
				message: 'Addendum is signed successfully'
			})
		} else {
			res.send({
				code: 0,
				status: 'failed',
				message: `No chart find with for encounter UID ${Encounter_UID}`,
				Error_Code: errorModule.No_chart_found()
			})
		}
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS054' })
	}
})


// function for get image URL from the image key
let getImage = async (imageKey) => {
	if (imageKey == null || imageKey == '') {
		return ''
	} else {
		if (imageKey.includes('pdf')) {
			const url = s3.getSignedUrl('getObject', {
				Bucket: process.env.AWS_BUCKET,
				Key: imageKey,
				ResponseContentType: 'application/pdf',
				Expires: 60 * 60 * 24 * 15 // time in seconds: e.g. 60 * 5 = 5 mins
			})
			return url
		} else {
			const url = s3.getSignedUrl('getObject', {
				Bucket: process.env.AWS_BUCKET,
				Key: imageKey,
				Expires: 60 * 60 * 24 * 15 // time in seconds: e.g. 60 * 5 = 5 mins
			})
			return url
		}
	}
}

module.exports = router
