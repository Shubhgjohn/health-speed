/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const chartModule = require('../../models').Chart
const auditModule = require('../../models').Audit
const errorModule = require('../../models').Errors
const encounterModule = require('../../models').Encounter
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const logger = require('../../models/errorlog')
const { auditUid } = require('../../utils/UID')
const auditType = require('../../auditType.json')

// route for create Add ICD10 Code //
router.post('/add-icd-code', authorize(Role.Provider), async (req, res) => {
  try {
    const { body: { Encounter_UID, ICD10_Code, ICD10_Code_Order, _id } } = req
    if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required', Error_Code: errorModule.Encounter_UID_required() })
    if (!ICD10_Code) return res.send({ code: 0, status: 'failed', message: 'ICD10_Code is required', Error_Code: errorModule.ICD10_Code_required() })
    if (!ICD10_Code_Order) return res.send({ code: 0, status: 'failed', message: 'ICD10_Code_Order is required!', Error_Code: errorModule.ICD10_Code_Order_required() })
    if (!_id) return res.send({ code: 0, status: 'failed', message: '_id is required!', Error_Code: errorModule._id_required() })
    
    // Get encounter data with the encounter UID
    let encounter = await encounterModule.getEncounterByEncounter_UID(Encounter_UID)
    if (!encounter) return res.send({ code: 0, status: 'failed', message: `encounter with id '${Encounter_UID}' not found!`, Error_Code: errorModule.Encounter_not_found() })
    
    // Get chart data with the encounter UID
    let chart = await chartModule.getChartData(Encounter_UID)
    if (chart) {
      let ICD10Code = {
        PRO_Diagnosis_ICD10_Code: ICD10_Code,
        PRO_Diagnosis_ICD10_Code_Order: ICD10_Code_Order
      }
      await chartModule.addICD10Code(Encounter_UID, _id, ICD10Code)

      // Get updated chart data the encounter UID
      let updatedChart = await chartModule.getChartData(Encounter_UID)
      updatedChart = updatedChart.PRO_Addendums.find(ele => ele._id == req.body._id)
      let ICD10_Code_Data = updatedChart.ICD10_Code.length > 0 ? updatedChart.ICD10_Code : []
      const auditData = {
        Audit_UID: await auditUid(),
        Audit_Info: 'New- ' + await auditUid(),
        Audit_Type: auditType.add,
        Data_Point: 'PRO/add ICD10 Code',
        PRO_First_Name: req.user.PRO_First_Name,
        PRO_Last_Name: req.user.PRO_Last_Name,
        PRO_UID: req.user.PRO_UID,
        IP_Address: await auditModule.getIpAddress(req)
      }
      await auditModule.create(auditData)
      res.send({
        code: 1,
        status: 'sucess',
        data: ICD10_Code_Data
      })
    } else {
      res.send({
        code: 0,
        status: 'failed',
        message: `No chart find for encounter UID ${Encounter_UID}`,
        Error_Code: errorModule.No_chart_found()
      })
    }
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS051' })
  }
})


// route for delete ICD code with the encounter UID
router.delete('/delete-icd-code/:Encounter_UID', authorize(Role.Provider), async (req, res) => {
  const { params: { Encounter_UID } } = req
  if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required', Error_Code: errorModule.Encounter_UID_required() })
  if (!req.body.Addendum_Id) return res.send({ code: 0, status: 'failed', message: 'Addendum_Id is required!', Error_Code: errorModule.Addendum_Id_required() })
  if (!req.body.ICD10_Id) return res.send({ code: 0, status: 'failed', message: 'ICD10_Id is required!', Error_Code: errorModule.ICD10_Id_required() })
  try {

    // Check encounter with this encounter UID is available in the system or not
    let encounter = await encounterModule.getEncounterByEncounter_UID(Encounter_UID)
    if (!encounter) return res.send({ code: 0, status: 'failed', message: `encounter with id '${Encounter_UID}' not found!`, Error_Code: errorModule.Encounter_not_found() })
   
   // function for get the chart data the encounter UID
    let chart = await chartModule.getChartData(Encounter_UID)
    if (chart) {

      // delete ICD code
      await chartModule.deleteICD10Code(req.body.Addendum_Id, req.body.ICD10_Id)

      // Get updated chart data the encounter UID
      let updatedChart = await chartModule.getChartData(Encounter_UID)
      updatedChart = updatedChart.PRO_Addendums.find(ele => ele._id == req.body.Addendum_Id)
      let ICD10_Code_Data = updatedChart.ICD10_Code.length > 0 ? updatedChart.ICD10_Code : []
      const auditData = {
        Audit_UID: await auditUid(),
        Audit_Info: 'New- ' + await auditUid(),
        Audit_Type: auditType.add,
        Data_Point: 'PRO/delete ICD10 Code',
        PRO_First_Name: req.user.PRO_First_Name,
        PRO_Last_Name: req.user.PRO_Last_Name,
        PRO_UID: req.user.PRO_UID,
        IP_Address: await auditModule.getIpAddress(req)
      }
      await auditModule.create(auditData)
      res.send({
        code: 1,
        status: 'sucess',
        message: 'ICD10_Code is deleted successfully',
        data: ICD10_Code_Data
      })
    } else {
      res.send({
        code: 0,
        status: 'failed',
        message: `No chart find with for encounter UID ${Encounter_UID}`,
        Error_Code: errorModule.No_chart_found()
      })
    }
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.send({ code: 0, status: 'failed', message: error.message, Error_Code: 'HS053' })
  }
})
module.exports = router
