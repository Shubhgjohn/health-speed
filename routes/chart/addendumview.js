/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const model = require('../../models')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const logger = require('../../models/errorlog')

// route for view-addendum by addendum id
router.get('/view-addendum/:Encounter_UID/:_id', authorize(Role.Patient), async (req, res) => {
	var { params: { _id, Encounter_UID } } = req
	if (!_id) return res.send({ code: 0, status: 'failed', message: '_id is required', Error_Code: model.Errors._id_required() })
	if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required', Error_Code: model.Errors.Encounter_UID_required() })
	try {

		// Get encounter data from encounter UID
		let encounter = await model.Encounter.getEncounterByEncounter_UID(Encounter_UID)
		if (!encounter) return res.send({ code: 0, status: 'failed', message: `encounter with id '${Encounter_UID}' not found!`, Error_Code: model.Errors.Encounter_not_found() })
		
		// Get chart data from encounter UID
		let chart = await model.Chart.getChartData(Encounter_UID)

		// if chart is available in the system
		if (chart) {
			let data = {
				Addendum_Id: _id,
				User: req.user.PT_HS_MRN || req.user.PRO_UID
			}
			await model.Chart.viewAddendum(Encounter_UID, data)
			res.send({
				code: 1,
				status: 'sucess'
			})
		} else {
			res.send({
				code: 0,
				status: 'failed',
				message: `No chart find for encounter UID ${Encounter_UID}`,
				Error_Code: model.Errors.No_chart_found()
			})
		}
	} catch (error) {
		logger.error(error.message)
		logger.fatal(error)
		return res.send({ code: 2, status: 'failed', message: error.message, Error_Code: 'HS033' })
	}
})

module.exports = router
