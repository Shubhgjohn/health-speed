/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const model = require('../../models')
const { auditUid } = require('../../utils/UID')
var auditType = require('../../auditType.json')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
const logger = require('../../models/errorlog')


// service for the add provider speciality
router.post('/add-speciality', async (req, res) => {
  const { PRO_Specialty } = req.body
  try {
    if (!PRO_Specialty) return res.send({ code: 0, status: 'failed', message: 'PRO_Specialty is required ' })
    await model.Specialty.create({ PRO_Specialty: PRO_Specialty })
    return res.send({ code: 1, status: 'success', message: 'added success' })
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.send({
      code: 2,
      status: 'failed',
      message: error.message,
      Error_Code: 'HS312'
    })
  }
})

// service for get speciality list
router.get('/get-specialitylist', authorize([Role.Provider, Role.Admin]), async (req, res) => {
  try {
    let specialitylist = await model.Specialty.find()
    var auditData = {
      Audit_UID: await auditUid(),
      Audit_Type: auditType.view,
      Audit_Info: 'New- ' + await auditUid(),
      Data_Point: 'PRO/Get speciality list',
      IP_Address: await model.Audit.getIpAddress(req)
    }
    await model.Audit.create(auditData)
    return res.send({ code: 1, status: 'success', data: specialitylist })
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.send({
      code: 2,
      status: 'failed',
      message: error.message,
      Error_Code: 'HS313'
    })
  }
})

module.exports = router
