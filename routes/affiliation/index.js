/* eslint-disable camelcase */
const Router = require('express').Router
const router = new Router()
const affiliationModule = require('../../models').Affiliation
const logger = require('../../models/errorlog')

// service for the add affiliation
router.post('/add-affiliation', async (req, res) => {
  const { PRO_Affiliations } = req.body
  if (!PRO_Affiliations) return res.send({ code: 0, status: 'failed', message: 'PRO_Affiliations is required ' })
  try {
    await affiliationModule.create({ PRO_Affiliations: PRO_Affiliations })
    return res.send({ code: 1, status: 'success', message: 'added success' })
  } catch (error) {
    logger.error(error.message)
    logger.fatal(error)
    return res.status(422).send({ code: 422, status: 'failed', message: error.message })
  }
})

module.exports = router
