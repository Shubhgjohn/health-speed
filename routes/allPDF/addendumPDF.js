/* eslint-disable no-useless-escape */
/* eslint-disable eqeqeq */
/* eslint-disable no-async-promise-executor */
/* eslint-disable camelcase */
/* eslint-disable prefer-const */
const Router = require('express').Router
const router = new Router()
const model = require('../../models')
const $ = require('jsrender')
const moment = require('moment')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
var AWS = require('aws-sdk')
const puppeteer = require('puppeteer')
AWS.config.update({
	accessKeyId: process.env.AWS_ACCESS_KEY,
	secretAccessKey: process.env.AWS_SECRET_KEY,
	region: 'us-east-1'
})
var s3 = new AWS.S3()

//service for the addendum pdf from encounter id
router.get('/addendum-pdf/:Encounter_UID', authorize([Role.Provider, Role.Patient]), async (req, res) => {
	try {
		let Encounter_UID = req.params.Encounter_UID
		if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required!', Error_Code: model.Errors.Encounter_UID_required() })
		
		// find encounter summery from encounter id
		let encounter = await model.Encounter.getSummaryByEncounter_UID(Encounter_UID)
		if (encounter.length == 0) return res.send({ code: 0, status: 'failed', message: `Encounter with id '${Encounter_UID}' not found!`, Error_Code: model.Errors.Encounter_not_found() })
		let arr = []
		encounter[0].charts[0].PRO_Addendums.map(ele => {
			ele.signDate = ele.Addendum_Sign_Date ? moment.tz(ele.Addendum_Sign_Date, process.env.CURRENT_TIMEZONE).format('D/M/YYYY') : null
			ele.signTime = ele.Addendum_Sign_Date ? moment.tz(ele.Addendum_Sign_Date, process.env.CURRENT_TIMEZONE).format('HH:mm') : null
			ele.PRO_Addendum_Attachment.forEach(item => {
				arr.push(item)
			})
		})
		let all_url = arr.length > 0 ? await addendumArray(arr) : []
		let addendumData = {
			PRO_First_Name: encounter[0].providers.PRO_First_Name,
			PRO_Last_Name: encounter[0].providers.PRO_Last_Name,
			PRO_Office_Address_Line_1: encounter[0].providers.PRO_Office_Address_Line_1,
			PRO_Office_Address_Line_2: encounter[0].providers.PRO_Office_Address_Line_2,
			PRO_Office_State: encounter[0].providers.PRO_Office_State,
			PRO_Office_Zip: encounter[0].providers.PRO_Office_Zip,
			PRO_Office_City: encounter[0].providers.PRO_Office_City,
			PRO_Office_Phone: encounter[0].providers.PRO_Office_Phone ? encounter[0].providers.PRO_Office_Phone.replace(/\D+/g, '')
				.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3') : null,
			Encounter_Day: moment(new Date(encounter[0].Encounter_Date)).format('dddd'),
			Encounter_Date: moment(new Date(encounter[0].Encounter_Date)).format('M/D/YYYY'),
			Encounter_Time: moment(encounter[0].Encounter_Time, 'hh:mm a').format('HH:mm'),
			Encounter_Location_Type: encounter[0].Encounter_Location_Type,
			PT_First_Name: encounter[0].patients.PT_First_Name,
			PT_Last_Name: encounter[0].patients.PT_Last_Name,
			PT_Cell_Phone: encounter[0].patients.PT_Cell_Phone ? encounter[0].patients.PT_Cell_Phone.replace(/\D+/g, '')
				.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3') : null,
			PT_Home_Address_Line_1: encounter[0].patients.PT_Home_Address_Line_1,
			PT_Home_Address_Line_2: encounter[0].patients.PT_Home_Address_Line_2,
			PT_Home_State: encounter[0].patients.PT_Home_State,
			PT_Home_City: encounter[0].patients.PT_Home_City,
			PT_Home_Zip: encounter[0].patients.PT_Home_Zip,
			PT_DOB: moment(new Date(encounter[0].patients.PT_DOB)).format('M/D/YYYY'),
			PT_Age_Now: moment(new Date()).format('YYYY') - moment(new Date(encounter[0].patients.PT_DOB)).format('YYYY'),
			PT_Gender: encounter[0].patients.PT_Gender ? encounter[0].patients.PT_Gender[0] : null,
			PT_HS_MRN: encounter[0].PT_HS_MRN,
			Encounter_UID: encounter[0].Encounter_UID,
			Encounter_End_Date: encounter[0].Encounter_Close_Date ? moment(encounter[0].Encounter_Close_Date).format('M/D/YYYY') : null,
			Encounter_End_Time: encounter[0].Encounter_Close_Date ? moment(encounter[0].Encounter_Close_Date).format('HH:mm') : null,
			PRO_Addendums: encounter[0].charts[0].PRO_Addendums,
			all_url: all_url
		}

		let template = $.templates('./download/addendum.html')

		// send data to html template
		let html = template.render(addendumData)
		const opts = {
			headless: true,
			timeout: 15000,
			args: ['--no-sandbox']
		}
		const browser = await puppeteer.launch(opts)
		const page = await browser.newPage()
		await page.setContent(html)

		const pdf = await page.pdf({
			format: 'A4',
			printBackground: true,
			displayHeaderFooter: true,
			footerTemplate: `<div  style="width:100%; font-size: 11px !important;  padding:0px 60px 25px 60px !important; page-break-after: always; font-family: 'Roboto', sans-serif;">
     
      <div style = "float: left; margin:0px 0px;">
      <div >${addendumData.PT_First_Name} ${addendumData.PT_Last_Name} &nbsp; DOB: <span> </span>${addendumData.PT_DOB}</div>
      <div >${addendumData.Encounter_UID}</div>
    </div>
    <div style = "float: right; margin:0px 0px;">
    <div class=\"page-footer\" style=\"width:100%; text-align:right; \">Page <span class=\"pageNumber\" "></span> of <span class=\"totalPages\"></span></div>
    </div></div>`,
			margin: {
				bottom: '30mm'
			}
		})
		await browser.close()
		var imagename = `addendum_${Date.now()}.pdf`
		const params = {
			Bucket: process.env.AWS_BUCKET,
			Key: imagename,
			Body: pdf,
			ContentType: 'application/pdf',
			ACL: 'public-read'
		}

		// upload image to AWS bucket
		s3.upload(params, function (s3Err, data) {
			if (s3Err) throw s3Err
			res.send({ code: 1, status: 'success', data: data.Location })
		})
	} catch (err) {
		res.send({ code: 2, status: 'failed', message: err.message, Error_Code: 'HS005' })
	}
})

// Get file urls for documents //
var getImageUrls = async (arr) => {
	return new Promise(async (resolve, reject) => {
		let newArray = []; let count = 0
		for (var i = 0; i < arr.length; i++) {
			let url = arr[i].Attachment_Name != null || arr[i].Attachment_Name != '' ? await model.CommonServices.getImage(arr[i].Attachment_Name) : null
			if (arr[i].Is_Deleted == false) {
				newArray.push({
					Attachment_Url: url,
					key: 'Chart' + '/' + 'PRO Addendum',
					Attachment_Name: arr[i].Attachment_Name
				})
			}
			count = count + 1
			if (count == arr.length) {
				resolve(newArray)
			}
		}
	})
}

// function for get addendum array with image urls
async function addendumArray (array) {
	return new Promise(async (resolve, reject) => {
		let arr = await getImageUrls(array)
		resolve(arr)
	})
}

module.exports = router
