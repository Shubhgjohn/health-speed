/* eslint-disable camelcase */
const express = require('express')
const app = express()
const addendumPDF = require('./addendumPDF')
const patient_receipt = require('./patient-receipt')
const provider_receipt = require('./provider-receipt')
const single_addendum = require('./singleaddendum')


app.use(addendumPDF)
app.use(patient_receipt)
app.use(provider_receipt)
app.use(single_addendum)

module.exports = app
