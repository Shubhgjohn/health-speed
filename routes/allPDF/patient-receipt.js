/* eslint-disable no-useless-escape */
/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
/* eslint-disable prefer-const */
const Router = require('express').Router
const router = new Router()
const model = require('../../models')
const $ = require('jsrender')
const moment = require('moment')
const authorize = require('../../_helpers/authorize')
const Role = require('../../_helpers/role')
var AWS = require('aws-sdk')
const puppeteer = require('puppeteer')
AWS.config.update({
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET_KEY,
  region: 'us-east-1'
})
var s3 = new AWS.S3()

// service for get payment reciept for patient
router.get('/patient-receipt-pdf/:Encounter_UID', authorize(Role.Patient), async (req, res) => {
  try {
    let Encounter_UID = req.params.Encounter_UID
    if (!Encounter_UID) return res.send({ code: 0, status: 'failed', message: 'Encounter_UID is required!', Error_Code: model.Errors.Encounter_UID_required() })
    
    // get encounter summery from encounter id
    let encounter = await model.Encounter.getSummaryByEncounter_UID(Encounter_UID)
    if (encounter.length == 0) return res.send({ code: 0, status: 'failed', message: `Encounter with id '${Encounter_UID}' not found!`, Error_Code: model.Errors.Encounter_not_found() })
   
   // get transaction from encounter id
    let transactions = await model.Transactions.getTransactionByEncounterId(Encounter_UID)
    let charge = transactions ? await model.Transactions.getChargeDetails(transactions.Charge_Id) : null
    let patient_receipt = {
      PRO_First_Name: encounter[0].providers.PRO_First_Name,
      PRO_Last_Name: encounter[0].providers.PRO_Last_Name,
      PRO_Profile_Picture: encounter[0].providers.PRO_Profile_Picture ? await model.CommonServices.getImage(encounter[0].providers.PRO_Profile_Picture) : null,
      PRO_Office_Address_Line_1: encounter[0].providers.PRO_Office_Address_Line_1,
      PRO_Office_Address_Line_2: encounter[0].providers.PRO_Office_Address_Line_2,
      PRO_Office_State: encounter[0].providers.PRO_Office_State,
      PRO_Office_Zip: encounter[0].providers.PRO_Office_Zip,
      PRO_Office_Phone: encounter[0].providers.PRO_Office_Phone ? encounter[0].providers.PRO_Office_Phone.replace(/\D+/g, '')
        .replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3') : null,
      PRO_Office_City: encounter[0].providers.PRO_Office_City,
      PT_Home_City: encounter[0].patients.PT_Home_City,
      Encounter_Day: moment(new Date(encounter[0].Encounter_Date)).format('dddd'),
      Encounter_Date: moment(new Date(encounter[0].Encounter_Date)).format('M/D/YYYY'),
      Encounter_Time: moment(encounter[0].Encounter_Time, 'hh:mm a').format('HH:mm'),
      PT_First_Name: encounter[0].patients.PT_First_Name,
      PT_Last_Name: encounter[0].patients.PT_Last_Name,
      PT_Cell_Phone: encounter[0].patients.PT_Cell_Phone ? encounter[0].patients.PT_Cell_Phone.replace(/\D+/g, '')
        .replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3') : null,
      PT_Home_Address_Line_1: encounter[0].patients.PT_Home_Address_Line_1,
      PT_Home_Address_Line_2: encounter[0].patients.PT_Home_Address_Line_2,
      PT_Home_State: encounter[0].patients.PT_Home_State,
      PT_Home_Zip: encounter[0].patients.PT_Home_Zip,
      PT_DOB: moment(new Date(encounter[0].patients.PT_DOB)).format('M/D/YY'),
      PT_Age_Now: moment(new Date()).format('YYYY') - moment(new Date(encounter[0].patients.PT_DOB)).format('YYYY'),
      PT_Gender: encounter[0].patients.PT_Gender ? encounter[0].patients.PT_Gender[0] : null,
      PT_HS_MRN: encounter[0].PT_HS_MRN,
      Encounter_UID: encounter[0].Encounter_UID,
      Encounter_Cost: encounter[0].Encounter_Cost,
      Encounter_Type: encounter[0].Encounter_Type == 'New Visit' ? 'new' : 'followup',
      card: charge ? charge.source.last4 : null,
      payment_date: transactions ? moment(transactions.Payment_Date).format('M/D/YYYY') : null
    }
    let template = $.templates('./download/patient-receipt.html')

    // send receipt data to HTML template
    let html = template.render(patient_receipt)
    const opts = {
      headless: true,
      timeout: 15000,
      args: ['--no-sandbox']
    }
    const browser = await puppeteer.launch(opts)
    const page = await browser.newPage()
    await page.setContent(html)
    let pdf = await page.pdf({
      format: 'A4',
      printBackground: true,
      displayHeaderFooter: true,
      footerTemplate: `<div  style="width:100%; font-size: 11px !important;  padding:0px 70px 25px 70px !important; page-break-after: always; font-family: 'Roboto', sans-serif;">
     
      <div style = "float: left; margin:0px 0px;">
      <div >${patient_receipt.PT_First_Name} ${patient_receipt.PT_Last_Name} &nbsp; DOB: <span> </span>${patient_receipt.PT_DOB}</div>
      <div >${patient_receipt.Encounter_UID}</div>
    </div>
    <div style = "float: right; margin:0px 0px;">
    <div class=\"page-footer\" style=\"width:100%; text-align:right; \">Page <span class=\"pageNumber\" "></span> of <span class=\"totalPages\"></span></div>
    <div>For assistance: <u style="color: blue; font-family: 'Roboto', sans-serif;">help@healthspeed.io</u></div>
    </div></div>`,
      margin: {
        bottom: '30mm'
      }
    })
    await browser.close()
    var imagename = `patient-receipt_${Date.now()}.pdf`
    const params = {
      Bucket: process.env.AWS_BUCKET,
      Key: imagename,
      Body: pdf,
      ContentType: 'application/pdf',
      ACL: 'public-read'
    }

    //upload image to AWS bucket
    s3.upload(params, function (s3Err, data) {
      if (s3Err) throw s3Err
      res.send({ code: 1, status: 'success', data: data.Location })
    })
  } catch (err) {
    res.send({ code: 2, status: 'failed', message: err.message, Error_Code: 'HS008' })
  }
})

module.exports = router
