const FCM = require('fcm-node')
const serverKey = require('../privateKey.json')
const fcm = new FCM(serverKey)

module.exports = fcm
