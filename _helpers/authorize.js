/* eslint-disable eqeqeq */
/* eslint-disable no-unreachable */
const secret = process.env.JWT_SECRET
const expressJwt = require('express-jwt')
var model = require('../models')
module.exports = authorize

function authorize (roles = []) {
	// roles param can be a single role string (e.g. Role.User or 'User')
	// or an array of roles (e.g. [Role.Admin, Role.User] or ['Admin', 'User'])
	if (typeof roles == 'string') {
		roles = [roles]
	}
	return [
		// authenticate JWT token and attach user to request object (req.user)
		expressJwt({ secret }),

		// authorize based on user role
		(req, res, next) => {
			if (roles.length && !roles.includes(req.user.roles)) {
				// user's role is not authorized
				return res.status(401).json({ message: 'Unauthorized' })
			}
			switch (req.user.roles) {
			case 'Admin':
				model.Admin.findAdminByUsername(req.user.AP_Username).then(
					user => {
						if (user) {
							req.user = user
							req.userType = 'Admin'
							// authentication and authorization successful
							next()
						} else {
							return res
								.status(404)
								.json({ message: 'User Not Found' })
						}
					}
				)
				break
			case 'Patient':
				model.Patient.findOneByEmail(req.user.PT_Username).then(
					user => {
						if (user) {
							req.user = user
							req.userType = 'Patient'
							// authentication and authorization successful
							next()
						} else {
							return res
								.status(404)
								.json({ message: 'User Not Found' })
						}
					}
				)
				break
			case 'Provider':
				model.Provider.findOneByEmail(req.user.PRO_Username).then(
					user => {
						if (user) {
							req.user = user
							req.userType = 'Provider'
							// authentication and authorization successful
							next()
						} else {
							return res
								.status(404)
								.json({ message: 'User Not Found' })
						}
					}
				)
				break

			default:
				return res.status(404).json({ message: 'User Not Found' })
				break
			}
		}
	]
}
