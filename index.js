/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
/* eslint-disable handle-callback-err */
/* eslint-disable prefer-const */
require('dotenv').config()
const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const swaggerUi = require('swagger-ui-express')
const swaggerDocument = require('./swagger.json')
const cors = require('cors')
const app = express()
const mongoose = require('mongoose')
mongoose.Promise = global.Promise
const fs = require('fs')
var socketEvents = require('./socketEvents')
const compression = require('compression')

// Require all routes //
const providerRoute = require('./routes/provider')
const adminRoute = require('./routes/admin')
const auditRoute = require('./routes/audit-logs')
const affiliationRoute = require('./routes/affiliation')
const specialityRoute = require('./routes/speciality')
const patientRoute = require('./routes/patient')
const encounterRoute = require('./routes/encounter')
const availabilityRoute = require('./routes/availability')
const orderRoute = require('./routes/order')
const chartRoute = require('./routes/chart')
const icd10Route = require('./routes/ICD10')
const allPDF = require('./routes/allPDF')
const notificationRoute = require('./routes/notification')
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument)) // swagger
app.all(['*index.js*', '*_helpers/**', '*models/**', '*package.json*', '*bower.json*', '*README.md*', '*Public/**'], function (req, res, next) {
	res.send({ auth: false })
})
app.use(compression()) // use compression
app.use(cors())
app.use(express.static(__dirname))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(morgan('dev'))


app.get('/', (req, res) => {
	res.send('welcome to healthspeed')
})
// Connecting to the database //
mongoose
	.connect(process.env.MONGOURL, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		useFindAndModify:false,
		useCreateIndex:true
	})
	.then(() => {
		console.log('Successfully connected to the database')
	})
	.catch(err => {
		console.log('Could not connect to the database. Exiting now...', err)
		process.exit()
	})

// Routes //
app.use('/provider', providerRoute)
app.use('/admin', adminRoute)
app.use('/audit', auditRoute)
app.use('/affiliation', affiliationRoute)
app.use('/speciality', specialityRoute)
app.use('/ICD10', icd10Route)
app.use('/patient', patientRoute)
app.use('/encounter', encounterRoute)
app.use('/availability', availabilityRoute)
app.use('/order', orderRoute)
app.use('/chart', chartRoute)
app.use('/pdf', allPDF)
app.use('/notification', notificationRoute)

app.get('/.well-known/apple-app-site-association', (req, res) => {
	fs.readFile('apple-app-site-association', 'utf8', (err, data) => {
		if (err) return res.send(err)
		let obj = JSON.parse(data)
		res.send(obj)
	})
})

// Listen the server on port 8081 //
const server = app.listen(process.env.PORT || 8081, () => {
	const host = server.address().address
	const port = server.address().port
	console.log('App is ready at http://%s:%s', host, port)
})
const io = require('socket.io').listen(server)

socketEvents(io)
