const Joi = require('joi')

module.exports = {
  updatePatientMail () {
    const emailSchema = Joi.object().keys({
      PT_Username: Joi.string()
    })
    return emailSchema
  },

  updatePatientPassword () {
    const passordSchema = Joi.object().keys({
      PT_Password: Joi.string()
    })
    return passordSchema
  }
}
