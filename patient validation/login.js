const Joi = require('joi')

module.exports = {
  login () {
    const contactSchema = Joi.object().keys({
      PT_Username: Joi.string().required(),
      PT_Password: Joi.string().required()
    })
    return contactSchema
  }
}
