const Joi = require('joi')

module.exports = {
  addMinor () {
    const minorSchema = Joi.object().keys({
      PT_First_Name: Joi.string(),
      PT_Last_Name: Joi.string(),
      PT_DOB: Joi.date(),
      PT_Gender: Joi.string().only('Male', 'Female'),
      PT_Profile_Picture: Joi.string().allow(null, ''),
      PT_Organ_Donor: Joi.string(),
      PT_Height: Joi.number(),
      PT_Weight: Joi.number(),
      PT_Is_Minor: Joi.string()
    })
    return minorSchema
  }
}
