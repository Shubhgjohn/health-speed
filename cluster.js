/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
require('dotenv').config()

const cluster = require('cluster')
const numCPUs = require('os').cpus().length
const scheduler = require('./cronjob')

if (cluster.isMaster) {
	if (process.env.ENV_MODE == 'PRODUCTION') {
		scheduler.cronScheduler()
	}
	for (var i = 0; i < numCPUs; i++) {
		cluster.fork()
	}
	cluster.on('exit', function (worker, code, signal) {
		console.log('worker ' + worker.process.pid + ' died')
	})
} else {
	// change this line to Your Node.js app entry point.
	require('./index.js')
}
