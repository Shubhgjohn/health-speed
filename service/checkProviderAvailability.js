/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
module.exports = {
  async checkProviderAvailability (availabilityArray, Encounter_Location_Type, isAvailable) {
    let Available = true
    if (Encounter_Location_Type == 'Office') {
      for (let index = 0; index < availabilityArray.length; index++) {
        const ele = availabilityArray[index]
        if (ele.Is_Office_Availability && ele.Is_Office_Available == true) {
          if (index == availabilityArray.length - 1) {
            const data = Available ? 'Available' : 'Not_Available'
            return data
          }
        } else {
          Available = false
          if (index == availabilityArray.length - 1) {
            const data = Available ? 'Available' : 'Not_Available'
            return data
          }
        }
      }
    } else {
      for (let index = 0; index < availabilityArray.length; index++) {
        const ele = availabilityArray[index]
        if (ele.Is_Home_Availability == true && ele.Is_Home_Available == true) {
          if (index == availabilityArray.length - 1) {
            const data = Available ? 'Available' : 'Not_Available'
            return data
          }
        } else {
          Available = false
          if (index == availabilityArray.length - 1) {
            const data = Available ? 'Available' : 'Not_Available'
            return data
          }
        }
      }
    }
  }
}
