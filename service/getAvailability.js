/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
let moment = require('moment')

// var available = []
// let homeAvailable = []

var getTravelTimeArray = async (provider, array, availability) => {
    let travelTime = provider.PRO_Home_Visit_Buffer / 60
    var count = 0; var newArr = []
  
    for (var i = 0; i < array.length; i++) {
      if (array[i].Encounter_Location_Type == 'Home') {
        for (var j = 0; j < 2; j++) {
          if (j == 0) {
            var Start_Time = moment(`${array[i].Encounter_Start_Time} ${array[i].Encounter_Start_Time_Zone}`, 'hh:mm A').format('HH.mm')
            Start_Time = Start_Time.includes(30) ? parseFloat(Start_Time) + 0.20 : parseFloat(Start_Time)
            Start_Time = Start_Time - travelTime
            Start_Time = Start_Time < 0 ? 0 : Start_Time
            Start_Time = Start_Time % 1 != 0 ? `${parseInt(Start_Time)}:30` : `${parseInt(Start_Time)}:00`
            Start_Time = moment(Start_Time, 'hh:mm').format('LT')
            if (Start_Time == '12:00 AM') {
              Start_Time = '0:00 AM'
            }
            if (Start_Time == '12:30 AM') {
              Start_Time = '0:30 AM'
            }
            newArr.push({
              Travel_Start_Time: Start_Time.split(' ')[0],
              Travel_Start_Time_Zone: Start_Time.split(' ')[1].toLowerCase(),
              Travel_End_Time: (array[i].Encounter_Start_Time == '12:00' && array[i].Encounter_Start_Time_Zone == 'am') ? '0:00' : array[i].Encounter_Start_Time,
              Travel_End_Time_Zone: array[i].Encounter_Start_Time_Zone
            })
          } else {
            var End_Time = moment(`${array[i].Encounter_End_Time} ${array[i].Encounter_End_Time_Zone}`, 'hh:mm A').format('HH.mm')
            End_Time = End_Time.includes(30) ? parseFloat(End_Time) + 0.20 : parseFloat(End_Time)
            End_Time = End_Time + travelTime
            End_Time = End_Time > 24 ? 24 : End_Time
            End_Time = End_Time % 1 != 0 ? `${parseInt(End_Time)}:30` : `${parseInt(End_Time)}:00`
            End_Time = moment(End_Time, 'hh:mm').format('LT')
            if (End_Time == '12:00 AM') {
              End_Time = '0:00 AM'
            }
            if (End_Time == '12:30 AM') {
              End_Time = '0:30 AM'
            }
            newArr.push({
              Travel_Start_Time: array[i].Encounter_End_Time,
              Travel_Start_Time_Zone: array[i].Encounter_End_Time_Zone,
              Travel_End_Time: End_Time.split(' ')[0],
              Travel_End_Time_Zone: End_Time.split(' ')[1].toLowerCase()
            })
          }
        }
        count = count + 1
        if (count == array.length) {
          return newArr
        }
      } else {
        count = count + 1
  
        if (count == array.length) {
          return newArr
        }
      }
    }
  }
// function for get availability slot array
let getAvailableSlotArray = async  (slotData, status, arr)=> {
    if (status || slotData.length == 1) {
      return arr
    }
    let startStatus = slotData.find(ele => ele.Is_Office_Available == true)
    if (!startStatus) {
      return arr
    }
    let index = await getIndex(slotData, startStatus.Slot_Format)
    slotData = slotData.slice(index, 48)
   return  await  nextFunctionOffice(slotData, arr)
  }
  
  
  // function for get slot array //
  let nextFunctionOffice = async (slotData, arr) => {
    let nextSlot = slotData.find(ele => ele.Is_Office_Available != true)
    let stat = !nextSlot ? true : false
    if (!nextSlot) {
      nextSlot = slotData[slotData.length - 1]
    }
    let index = await getIndex(slotData, nextSlot.Slot_Format)
    index = index + 1
    let data = slotData.slice(0, index)
    if (data.length > 0) {
       arr.push(data)
    }
    let nextArray = slotData.slice(index, slotData.length)
  return  await getAvailableSlotArray(nextArray, stat, arr)
 }
  
  
  
  
  let getAvailableSlotHomeArray = async (slotData, status, home) => {
    if (status || slotData.length == 1) {
      return home
    }
    let startStatus = slotData.find(ele => ele.Is_Home_Available == true)
    if (!startStatus) {
      return home
    }
    let index = await getIndex(slotData, startStatus.Slot_Format)
    slotData = slotData.slice(index, 48)
    return await nextFunctionHome(slotData, home)
  }
  
  // // function for get slot array //
  
  let nextFunctionHome = async (slotData, home) => {
    let nextSlot = slotData.find(ele => ele.Is_Home_Available != true)
    let stat = !nextSlot ? true : false
    if (!nextSlot) {
      nextSlot = slotData[slotData.length - 1]
    }
    let index = await getIndex(slotData, nextSlot.Slot_Format)
    index = index + 1
    let data = slotData.slice(0, index)
    if (data.length > 0) {
      await home.push(data)
    }
    let nextArray = slotData.slice(index, slotData.length)
    return await getAvailableSlotHomeArray(nextArray, stat, home)
  }

  // function for the get index of the given value from array
let getIndex = (array, value) => {
    let index = -1
    array.find(function (item, i) {
      if (item.Slot_Format == value) {
        index = i
        return i
      }
    })
    return index
  }
module.exports = { getTravelTimeArray , getAvailableSlotArray , getAvailableSlotHomeArray }
  