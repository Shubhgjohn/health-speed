const Joi = require('joi')

module.exports = {
  updateEmail () {
    const settingSchema = Joi.object().keys({
      PRO_Username: Joi.string()
    })
    return settingSchema
  },

  updatePassword () {
    const settingSchema = Joi.object().keys({
      PRO_Password: Joi.string()
    })
    return settingSchema
  }
}
