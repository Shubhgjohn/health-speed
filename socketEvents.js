exports = module.exports = function (io) {
  io.on('connection', socket => {
    console.log('user connected')
    socket.on('join', function (encounterId) {
      console.log('join', encounterId)
      socket.join(encounterId)
      console.log(encounterId + ' : has joined the chat ')
    })
    socket.on('leave', encounterId => {
      socket.leave(encounterId)
      console.log('left ' + encounterId)
    })
    socket.on('messagedetection', function (encounterId) {
      console.log('messagedetection', encounterId)
      socket.broadcast.to(encounterId).emit('update messages', encounterId)
    })
    socket.on('disconnect', function () {
      socket.broadcast.emit('userdisconnect', ' user has left')
    })
  })
}
