const BaseModel = require('../base')
const mongoose = require('mongoose')
const Schema = mongoose.Schema
var autopopulate = require('mongoose-autopopulate')

const OTHER_COLLECTION = 'other'
const OTHER_SCHEMA = new Schema({
  Current_Month: { type: String, default: null },
  Current_Date: { type: String, default: null },
  Current_Year: Number,
  Current_Time: Date,
  Currently_Selected_Date: Date,
  Month_name: { type: String, max: 3 },
  Last_2_digits_of_the_year: Number,
  Day_Month_Date: Date,
  Day_of_Week: { type: String, default: null },
  Numeral_Day: { type: Number, min: 1, max: 31 },
  Characters_Remaining: { type: String, min: 5, max: 300 },
  HS_Support_Number: { type: String, min: 10, max: 10 },
  HS_Support_Email: { type: String, min: 5, max: 140 },
  HS_Address: { type: String, min: 5, max: 300 },
  PT_Privacy_Policy: { type: String, default: null },
  PT_Privacy_Policy_PDF_Name: { type: String, default: null },
  PT_Privacy_Policy_PDF_Url: { type: String, default: null },
  PRO_Privacy_Policy: { type: String, default: null },
  PRO_Privacy_Policy_PDF_Name: { type: String, default: null },
  PRO_Privacy_Policy_PDF_Url: { type: String, default: null },
  PT_Terms_and_Conditions_PDF_Name: { type: String, default: null },
  PT_Terms_and_Conditions_PDF_Url: { type: String, default: null },
  PT_Terms_and_Conditions: { type: String, default: null },
  PRO_Terms_and_Conditions_PDF_Name: { type: String, default: null },
  PRO_Terms_and_Conditions_PDF_Url: { type: String, default: null },
  PRO_Terms_and_Conditions: { type: String, default: null },
  HS_Notification_Emails: { type: Array, min: 1 },
  HS_911_Disclaimer: { type: String, min: 5, max: 400 },
  Chat_Date: Date,
  Name_of_Image: { type: String, default: null },
  Image_Number: Number,
  Total_Number_of_Images: Number,
  Name_of_PDF: { type: String, default: null },
  PDF_Number: Number,
  Total_Number_of_PDF: Number,
  Paid_to_HealthSpeed: Number,
  Paid_to_Provider: Number,
  Attachment_Name: { type: String, default: null },
  PRO_Office_Office_Hours_New_Visit_Rate_AVG: { type: Number, min: 25, max: 2000 },
  PRO_Office_Office_Hours_Followup_Rate_AVG: { type: Number, min: 20, max: 2000 },
  PRO_Office_Off_Hours_New_Visit_Rate_AVG: { type: Number, min: 25, max: 2000 },
  PRO_Office_Off_Hours_Followup_Rate_AVG: { type: Number, min: 20, max: 2000 },
  PRO_Home_Office_Hours_New_Visit_Rate_AVG: { type: Number, min: 25, max: 2000 },
  PRO_Home_Office_Hours_Followup_Rate_AVG: { type: Number, min: 20, max: 2000 },
  PRO_Home_Off_Hours_New_Visit_Rate_AVG: { type: Number, min: 25, max: 2000 },
  PRO_Home_Off_Hours_Followup_Rate_AVG: { type: Number, min: 20, max: 2000 },
  PRO_Office_Office_Hours_New_Visit_Min: Number,
  PRO_Office_Office_Hours_Followup_Min: Number,
  PRO_Office_Off_Hours_New_Visit_Min: Number,
  PRO_Office_Off_Hours_Followup_Min: Number,
  PRO_Home_Office_Hours_New_Visit_Min: Number,
  PRO_Home_Office_Hours_Followup_Min: Number,
  PRO_Home_Off_Hours_New_Visit_Min: Number,
  PRO_Home_Off_Hours_Followup_Min: Number,

  Created_At: { type: Date, default: new Date() },
  Updated_At: { type: Date, default: new Date() },
  Deleted_At: { type: Date, default: null }
})
OTHER_SCHEMA.plugin(autopopulate)

var MODEL = mongoose.model(OTHER_COLLECTION, OTHER_SCHEMA)

// Note: Do not extend this class, only BaseModel is allow to be extended from.
// because more than 2 levels inheritance could lead to tight-coupling design and make everything more complicated
class OtherModel extends BaseModel {
  constructor () {
    super(MODEL, OTHER_COLLECTION, OTHER_SCHEMA)
  }
}

module.exports = OtherModel
