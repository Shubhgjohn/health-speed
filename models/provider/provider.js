/* eslint-disable eqeqeq */
/* eslint-disable dot-notation */
/* eslint-disable camelcase */
const BaseModel = require('../base')
const mongoose = require('mongoose')
var ObjectId = require('mongodb').ObjectID
const Schema = mongoose.Schema

const PROVIDER_COLLECTION = 'provider'

function omitPrivate (doc, obj) {
  delete obj.__v
  delete obj.id
  return obj
}
var options = { toJSON: { transform: omitPrivate } }
const Deposite_schema = new Schema({
  PRO_Account_Number: { type: Number },
  PRO_Account_Short_Number: { type: String },
  PRO_Routing_Number: { type: String }
})

// Provider's Special rate schema //
const SPECIAL_RATE_SCHEMA = new Schema({
  PRO_Special_Rate_Date: { type: String },
  PRO_Home_New_Visit_Special_Hourly_Rate: { type: Number, min: 25, max: 2000 },
  PRO_Home_New_Visit_Special_Hourly_Rate_Min: { type: Number, default: 25 },
  PRO_Home_New_Visit_Special_Hourly_Rate_MAX: { type: Number, default: 2000 },

  PRO_Home_Followup_Special_Hourly_Rate: { type: Number, min: 20, max: 2000 },
  PRO_Home_Followup_Special_Hourly_Rate_Min: { type: Number, default: 20 },
  PRO_Home_Followup_Special_Hourly_Rate_MAX: { type: Number, default: 2000 },

  PRO_Home_New_Visit_Special_Off_Hourly_Rate: { type: Number, min: 25, max: 2000 },
  PRO_Home_New_Visit_Special_Off_Hourly_Rate_Min: { type: Number, default: 25 },
  PRO_Home_New_Visit_Special_Off_Hourly_Rate_MAX: { type: Number, default: 2000 },

  PRO_Home_Followup_Special_Off_Hourly_Rate: { type: Number, min: 20, max: 2000 },
  PRO_Home_Followup_Special_Off_Hourly_Rate_Min: { type: Number, default: 20 },
  PRO_Home_Followup_Special_Off_Hourly_Rate_MAX: { type: Number, default: 2000 },

  PRO_Office_New_Visit_Special_Hourly_Rate: { type: Number, min: 25, max: 2000 },
  PRO_Office_New_Visit_Special_Hourly_Rate_Min: { type: Number, default: 25 },
  PRO_Office_New_Visit_Special_Hourly_Rate_MAX: { type: Number, default: 2000 },

  PRO_Office_Followup_Special_Hourly_Rate: { type: Number, min: 20, max: 2000 },
  PRO_Office_Followup_Special_Hourly_Rate_Min: { type: Number, default: 20 },
  PRO_Office_Followup_Special_Hourly_Rate_MAX: { type: Number, default: 2000 },

  PRO_Office_New_Visit_Special_Off_Hourly_Rate: { type: Number, min: 25, max: 2000 },
  PRO_Office_New_Visit_Special_Off_Hourly_RateMin: { type: Number, default: 25 },
  PRO_Office_New_Visit_Special_Off_Hourly_RateMAX: { type: Number, default: 2000 },

  PRO_Office_Followup_Special_Off_Hourly_Rate: { type: Number, min: 20, max: 2000 },
  PRO_Office_Followup_Special_Off_Hourly_Rate_Min: { type: Number, default: 20 },
  PRO_Office_Followup_Special_Off_Hourly_Rate_MAX: { type: Number, default: 2000 },

  Created_At: { type: Date, default: new Date() },
  Updated_At: { type: Date, default: new Date() },
  Deleted_At: { type: Date, default: null },
  Is_Deleted: { type: Boolean, default: false }
}, options)
// Provider's Speciality Schema //
const speciality = new Schema({
  PRO_Speciality: { type: String, minlength: 2, maxlength: 40 },
  PRO_Primary_Speciality: { type: Boolean, default: false },
  Is_Deleted: { type: Boolean, default: false }
}, options)

const affliation = new Schema({
  PRO_Affiliations: { type: String, minlength: 2, maxlength: 98 },
  Attachment_Url: { type: String, default: null },
  Is_Deleted: { type: Boolean, default: false }
}, options)

const educations = new Schema({
  PRO_Education: { type: String, minlength: 2, maxlength: 98 },
  Attachment_Url: { type: String, default: null },
  Is_Deleted: { type: Boolean, default: false }
}, options)

const experiences = new Schema({
  PRO_Experience: { type: String, minlength: 2, maxlength: 98 },
  Attachment_Url: { type: String, default: null },
  Is_Deleted: { type: Boolean, default: false }
}, options)

const actions = new Schema({
  PRO_Actions_Against: { type: String, minlength: 2, maxlength: 98 },
  Attachment_Url: { type: String, default: null },
  Is_Deleted: { type: Boolean, default: false }
}, options)

const curriculmVitaeSchema = new Schema({
  Attachment_Name: String,
  Attachment_Url: { type: String, default: null },
  Is_Deleted: { type: Boolean, default: false }
})
const stateMedicalLicenceSchema = new Schema({
  Attachment_Name: String,
  Attachment_Url: { type: String, default: null },
  Is_Deleted: { type: Boolean, default: false }
})
const deaCertificateSchema = new Schema({
  Attachment_Name: String,
  Attachment_Url: { type: String, default: null },
  Is_Deleted: { type: Boolean, default: false }
})
const boardCertificateSchema = new Schema({
  Attachment_Name: String,
  Attachment_Url: { type: String, default: null },
  Is_Deleted: { type: Boolean, default: false }
})
const professionalLiabilitySchema = new Schema({
  Attachment_Name: String,
  Attachment_Url: { type: String, default: null },
  Is_Deleted: { type: Boolean, default: false }
})

// Provider Schema //
const PROVIDER_SCHEMA = new Schema({
  PRO_Username: { type: String, minlength: 5, maxlength: 140, default: null },
  PRO_Password: { type: String, minlength: 6, maxlength: 100, default: null },
  PRO_First_Name: { type: String, minlength: 1, maxlength: 50, default: null },
  PRO_Last_Name: { type: String, minlength: 1, maxlength: 50, default: null },
  PRO_Gender: { type: String, default: null },
  PRO_Fax_Number: { type: String, default: null },
  PRO_Home_Address_Line_1: { type: String, minlength: 3, maxlength: 140, default: null },
  PRO_Home_Address_Line_2: { type: String, minlength: 3, maxlength: 140, default: null },
  PRO_Home_Address_Latitude: { type: String, default: null },
  PRO_Home_Address_Longitude: { type: String, default: null },
  PRO_Office_Address_Latitude: { type: String, default: null },
  PRO_Office_Address_Longitude: { type: String, default: null },
  PRO_Home_City: { type: String, minlength: 1, maxlength: 140, default: null },
  PRO_Home_State: { type: String, default: null },
  PRO_Home_Zip: { type: String, minlength: 5, maxlength: 5, default: null },
  PRO_Home_Phone: { type: String, default: null },
  PRO_Office_Address_Line_1: { type: String, minlength: 3, maxlength: 140, default: null },
  PRO_Office_Address_Line_2: { type: String, minlength: 3, maxlength: 140, default: null },
  PRO_Office_City: { type: String, minlength: 1, maxlength: 140, default: null },
  PRO_Office_State: { type: String, default: null },
  PRO_Office_Zip: { type: String, minlength: 5, maxlength: 5, default: null },
  PRO_Office_Phone: { type: String, default: null },
  PRO_Profile_Picture: { type: String, default: null },
  PRO_Profile_Picture_Key: { type: String, default: null },
  PRO_Default_Profile_Picture: { type: String, default: null },
  PRO_Home_Rate_Status: { type: String, default: 'New' },
  PRO_Office_Rate_Status: { type: String, default: 'New' },
  PRO_Billing_Status: { type: String, default: 'New' },
  PRO_Profile_Status: { type: String, default: 'New' },
  PRO_Account_Status: { type: String, default: 'New' },
  PRO_UID: { type: String, default: null },
  PRO_DOB: { type: Date, default: null },
  PRO_Attestation_QA: { type: String, default: null },
  PRO_Attestation_QB: { type: String, default: null },
  PRO_Attestation_QC: { type: String, default: null },
  PRO_Attestation_QD: { type: String, default: null },
  PRO_Attestation_QE: { type: String, default: null },
  PRO_Attestation_QF: { type: String, default: null },
  PRO_Attestation_QG: { type: String, default: null },
  PRO_Attestation_QH: { type: String, default: null },
  PRO_Attestation_QI: { type: String, default: null },
  PRO_Attestation_QJ: { type: String, default: null },
  PRO_Attestation_QK: { type: String, default: null },
  PRO_Attestation_QL: { type: String, default: null },
  PRO_AQ_Complete: { type: Boolean, default: false },
  PRO_Bio: { type: String, minlength: 100, maxlength: 600, default: null },
  PRO_Speciality: [speciality],
  PT_Age_Min: { type: Number, default: 1 },
  PT_Age_Max: { type: Number, default: 90 },
  PRO_Affliation: [affliation],
  PRO_Actions: [actions],
  PRO_Educations: [educations],
  PRO_Experiences: [experiences],
  PRO_Govt_ID_Type: { type: String, default: null },
  PRO_Govt_ID_Front: { type: String, default: null },
  PRO_Govt_ID_Front_Key: { type: String, default: null },
  PRO_Govt_ID_Back: { type: String, default: null },
  PRO_Govt_ID_Back_Key: { type: String, default: null },
  PRO_Driving_Licence_Front: { type: String, default: null },
  PRO_Driving_Licence_Back: { type: String, default: null },
  PRO_State_ID_Front: { type: String, default: null },
  PRO_State_ID_Back: { type: String, default: null },
  PRO_Passport_Front: { type: String, default: null },
  PRO_Notifications: { type: String, default: 'Yes' },
  PRO_Biometric_Login: { type: String, default: null },
  PRO_Home_Availability_Period: { type: Number, min: 1, max: 24, default: 4 },
  PRO_Office_Availability_Period: { type: Number, min: 1, max: 24, default: 4 },
  PRO_Number_of_Unsigned_Docs: { type: Number, default: 0 },
  PRO_Number_of_Complete_Charts: { type: Number, default: 0 },
  PRO_Home_Visit_Buffer: { type: Number, default: 30 },
  PRO_Office_Hours: { type: String, default: null },
  PRO_Earnings_to_Date: { type: Number, default: 0 },
  PRO_Monthly_Earnings: { type: Number, default: null },
  PRO_Home_Monthly_Earnings: { type: Number, default: null },
  PRO_Office_Monthly_Earnings: { type: Number, default: null },
  PRO_Monthly_Home_Visits: { type: Number, default: null },
  PRO_Monthly_Total_Visits: { type: Number, default: null },
  PRO_Monthly_Office_Visits: { type: Number, default: null },
  Pro_Office_Hours_Time: { type: Object, default: [{ id: 1, day: 'Sun', fromTime: { time: '6', zone: 'am' }, toTime: { time: '7', zone: 'pm' } }, { id: 2, day: 'Mon', fromTime: { time: '6', zone: 'am' }, toTime: { time: '7', zone: 'pm' } }, { id: 3, day: 'Tues', fromTime: { time: '6', zone: 'am' }, toTime: { time: '7', zone: 'pm' } }, { id: 4, day: 'Wed', fromTime: { time: '6', zone: 'am' }, toTime: { time: '7', zone: 'pm' } }, { id: 5, day: 'Thur', fromTime: { time: '6', zone: 'am' }, toTime: { time: '7', zone: 'pm' } }, { id: 6, day: 'Fri', fromTime: { time: '6', zone: 'am' }, toTime: { time: '7', zone: 'pm' } }, { id: 7, day: 'Sat', fromTime: { time: '6', zone: 'am' }, toTime: { time: '7', zone: 'pm' } }] },
  Pro_Office_Hours_To_Time: { type: String, default: null },
  Pro_Office_Hours_From_Time: { type: String, default: null },
  PRO_Miles_to_Travel: { type: Number, min: 1, max: 60, default: 20 },
  PRO_Notice_for_Home_Visit: { type: Number, default: 4 },
  PRO_Notice_for_Office_Visit: { type: Number, default: 4 },
  PRO_Office_Visit_Buffer: { type: Number, default: 0 },
  PRO_Special_Rate: [SPECIAL_RATE_SCHEMA],
  PRO_Deposite: [Deposite_schema],
  PRO_CV: [curriculmVitaeSchema],
  PRO_State_Medical_License: [stateMedicalLicenceSchema],
  PRO_DEA_Cert: [deaCertificateSchema],
  PRO_Board_Cert: [boardCertificateSchema],
  PRO_Liability_Cert: [professionalLiabilitySchema],
  PRO_Private_Note: { type: String, minlength: 0, max: 2000, default: null },
  PRO_Home_Office_Hours_New_Visit_Rate: { type: Number, min: 0, max: 2000, default: 0 },
  PRO_Home_Office_Hours_Followup_Rate: { type: Number, min: 0, max: 2000, default: 0 },
  PRO_Home_Off_Hours_New_Visit_Rate: { type: Number, min: 0, max: 2000, default: 0 },
  PRO_Home_Off_Hours_Followup_Rate: { type: Number, min: 0, max: 2000, default: 0 },

  PRO_Office_Office_Hours_New_Visit_Rate: { type: Number, min: 0, max: 2000, default: 0 },
  PRO_Office_Office_Hours_Followup_Rate: { type: Number, min: 0, max: 2000, default: 0 },
  PRO_Office_Off_Hours_New_Visit_Rate: { type: Number, min: 0, max: 2000, default: 0 },
  PRO_Office_Off_Hours_Followup_Rate: { type: Number, min: 0, max: 2000, default: 0 },

  PRO_Home_Office_Hours_New_Visit_Rate_Min: { type: Number, default: 25 },
  PRO_Home_Office_Hours_New_Visit_Rate_MAX: { type: Number, default: 2000 },

  PRO_Home_Office_Hours_Followup_Rate_Min: { type: Number, default: 20 },
  PRO_Home_Office_Hours_Followup_Rate_MAX: { type: Number, default: 2000 },

  PRO_Home_Off_Hours_New_Visit_Rate_Min: { type: Number, default: 25 },
  PRO_Home_Off_Hours_New_Visit_Rate_MAX: { type: Number, default: 2000 },

  PRO_Home_Off_Hours_Followup_Rate_Min: { type: Number, default: 20 },
  PRO_Home_Off_Hours_Followup_Rate_MAX: { type: Number, default: 2000 },

  PRO_Office_Office_Hours_New_Visit_Rate_Min: { type: Number, default: 25 },
  PRO_Office_Office_Hours_New_Visit_Rate_MAX: { type: Number, default: 2000 },

  PRO_Office_Office_Hours_Followup_Rate_Min: { type: Number, default: 20 },
  PRO_Office_Office_Hours_Followup_Rate_MAX: { type: Number, default: 2000 },

  PRO_Office_Off_Hours_New_Visit_Rate_Min: { type: Number, default: 25 },
  PRO_Office_Off_Hours_New_Visit_Rate_MAX: { type: Number, default: 2000 },

  PRO_Office_Off_Hours_Followup_Rate_Min: { type: Number, default: 20 },
  PRO_Office_Off_Hours_Followup_Rate_MAX: { type: Number, default: 2000 },

  PRO_Last_LogIn: { type: Date, default: null },
  PRO_Private_Key: { type: String, default: null },
  stripeCustomerId: { type: String, default: null },
  StripeAccountId: { type: String, default: null },
  Created_At: { type: Date, default: Date.now },
  Updated_At: { type: Date, default: Date.now },
  Deleted_At: { type: Date, default: null }
}, options)

var MODEL = mongoose.model(PROVIDER_COLLECTION, PROVIDER_SCHEMA)
// Note: Do not extend this class, only BaseModel is allow to be extended from.
// because more than 2 levels inheritance could lead to tight-coupling design and make everything more complicated
class ProviderModel extends BaseModel {
  constructor () {
    super(MODEL, PROVIDER_COLLECTION, PROVIDER_SCHEMA)
  }

  // Find one provider by provider email //
  findOneByEmail (email) {
    return this.model.findOne({ PRO_Username: email })
  }

  // Find provider by PRO_UID //
  findById (id) {
    return this.model.findOne({ PRO_UID: id })
  }

  // Update provider by PRO_Username(email) //
  updateByEmail (email, data) {
    return this.model.updateOne(
      { PRO_Username: email },
      {
        $set: {
          PRO_Password: data,
          Updated_At: Date.now()
        }
      }
    )
  }

  // Update provider by PRO_UID
  updateById (id, data) {
    return this.model.findOneAndUpdate({ PRO_UID: id }, { $set: data, Updated_At: new Date() }, { new: true }, (err, doc) => {
      if (err) {
        console.log('Something wrong when updating data!')
      }
    })
  }

  // Update provider last login //
  updateLastLogin (email) {
    return this.model.updateOne(
      { PRO_Username: email },
      { $set: { PRO_Last_LogIn: Date.now(), Updated_At: new Date() } }
    )
  }

  // Save provider special rate provider PRO_UID //
  saveSpecialRate (id, data) {
    return this.model.updateOne(
      { PRO_UID: id },
      { $push: { PRO_Special_Rate: data } },
      { $set: { Updated_At: new Date() } }
    )
  }

  // Remove provider special rate provider PRO_UID //
  delteSpecialRate (id, specialRateId) {
    return this.model.updateOne(
      { PRO_UID: id },
      { $pull: { PRO_Special_Rate: { _id: ObjectId(specialRateId) } } }
    )
  }

  // Get Provider special rate by provider PRO_UID //
  getSpecialRateById (id) {
    return this.model.findOne({ PRO_UID: id }, { PRO_Special_Rate: 1 })
  }

  getSpecialRatesByDate (id, date) {
    return this.model.aggregate([{ $match: { PRO_UID: id } },
      { $unwind: '$PRO_Special_Rate' },
      {
        $match: {
          'PRO_Special_Rate.PRO_Special_Rate_Date': date
        }
      },
      {
        $group:
      {
        _id: '$PRO_Special_Rate._id',
        special_rate:
        {
          $push: {
            date: '$PRO_Special_Rate.PRO_Special_Rate_Date',
            PRO_Home_Followup_Special_Hourly_Rate: '$PRO_Special_Rate.PRO_Home_Followup_Special_Hourly_Rate',
            PRO_Home_Followup_Special_Off_Hourly_Rate: '$PRO_Special_Rate.PRO_Home_Followup_Special_Off_Hourly_Rate',
            PRO_Home_New_Visit_Special_Hourly_Rate: '$PRO_Special_Rate.PRO_Home_New_Visit_Special_Hourly_Rate',
            PRO_Home_New_Visit_Special_Off_Hourly_Rate: '$PRO_Special_Rate.PRO_Home_New_Visit_Special_Off_Hourly_Rate',
            PRO_Office_Followup_Special_Hourly_Rate: '$PRO_Special_Rate.PRO_Office_Followup_Special_Hourly_Rate',
            PRO_Office_Followup_Special_Off_Hourly_Rate: '$PRO_Special_Rate.PRO_Office_Followup_Special_Off_Hourly_Rate',
            PRO_Office_New_Visit_Special_Hourly_Rate: '$PRO_Special_Rate.PRO_Office_New_Visit_Special_Hourly_Rate',
            PRO_Office_New_Visit_Special_Off_Hourly_Rate: '$PRO_Special_Rate.PRO_Office_New_Visit_Special_Off_Hourly_Rate'
          }
        }

      }
      },
      { $unwind: '$special_rate' }
    ])
  }

  // Save provider bank account details //
  saveAccountDetail (id, data) {
    return this.model.updateOne(
      { PRO_UID: id },
      { $push: { PRO_Deposite: data } },
      { multi: true }
    )
  }

  // Find provider special rate by id //
  findSpecialRateById (proId) {
    return this.model.aggregate([
      { $unwind: '$PRO_Special_Rate' },
      { $match: { 'PRO_Special_Rate._id': ObjectId(proId) } },
      { $project: { PRO_Special_Rate: '$PRO_Special_Rate' } }
    ])
  }

  // Update provider special rate by id //
  updateSpecialDataById (specialId, data) {
    return this.model.updateOne(
      { 'PRO_Special_Rate._id': ObjectId(specialId) },
      {
        $set: {
          'PRO_Special_Rate.$.PRO_Special_Rate_Date': data.PRO_Special_Rate_Date,
          'PRO_Special_Rate.$.PRO_Home_New_Visit_Special_Hourly_Rate':
            data.PRO_Home_New_Visit_Special_Hourly_Rate,
          'PRO_Special_Rate.$.PRO_Home_Followup_Special_Hourly_Rate':
            data.PRO_Home_Followup_Special_Hourly_Rate,
          'PRO_Special_Rate.$.PRO_Home_New_Visit_Special_Off_Hourly_Rate':
            data.PRO_Home_New_Visit_Special_Off_Hourly_Rate,
          'PRO_Special_Rate.$.PRO_Home_Followup_Special_Off_Hourly_Rate':
            data.PRO_Home_Followup_Special_Off_Hourly_Rate,
          'PRO_Special_Rate.$.PRO_Office_New_Visit_Special_Hourly_Rate':
            data.PRO_Office_New_Visit_Special_Hourly_Rate,
          'PRO_Special_Rate.$.PRO_Office_Followup_Special_Hourly_Rate':
            data.PRO_Office_Followup_Special_Hourly_Rate,
          'PRO_Special_Rate.$.PRO_Office_New_Visit_Special_Off_Hourly_Rate':
            data.PRO_Office_New_Visit_Special_Off_Hourly_Rate,
          'PRO_Special_Rate.$.PRO_Office_Followup_Special_Off_Hourly_Rate':
            data.PRO_Office_Followup_Special_Off_Hourly_Rate,
          Updated_At: new Date()
        }
      },
      { multi: true }
    )
  }

  // Add provider speciality by PRO_UID //
  addSpeciality (id, data) {
    return this.model.updateOne(
      { PRO_UID: id },
      { $push: { PRO_Speciality: data } },
      { multi: true }
    )
  }

  addFieldData (id, data, key) {
    var json = { key: data }
    json[`${key}`] = data
    delete json.key
    return this.model.updateOne(
      { PRO_UID: id },
      { $push: json },
      { multi: true }
    )
  }

  updateArrayElement (id, key, data) {
    const getStr = `${key}._id`
    const setStr = `${key}.$`
    const getJson = { key: id }
    getJson[`${getStr}`] = getJson.key
    delete getJson.key
    const setJson = { key: data }
    setJson[`${setStr}`] = setJson.key
    delete setJson.key
    return this.model.update(getJson, {
      $set: setJson
    }, { multi: true })
  }

  // delete speciality by id
  deleteSpeciality (id, specialRateId) {
    return this.model.updateOne(
      { PRO_UID: id },
      { $pull: { PRO_Speciality: { _id: ObjectId(specialRateId) } } }
    )
  }

  // Find speciality by id //
  findSpecilityById (id) {
    return this.model.aggregate([
      { $unwind: '$PRO_Speciality' },
      { $match: { 'PRO_Speciality._id': ObjectId(id) } },
      { $project: { Speciality: '$PRO_Speciality.PRO_Speciality', PRO_Primary_Speciality: '$PRO_Speciality.PRO_Primary_Speciality' } }
    ])
  }

  CreatePrimarySpeciality (PRO_UID) {
    return this.model.updateOne(
      { PRO_UID: PRO_UID },
      { $set: { 'PRO_Speciality.0.PRO_Primary_Speciality': true, Updated_At: new Date() } },
      { multi: true }
    )
  }

  // Update provider speciality by id //
  updateSpecialityById (specialityId, data) {
    return this.model.updateOne(
      { 'PRO_Speciality._id': ObjectId(specialityId) },
      { $set: { 'PRO_Speciality.$.PRO_Speciality': data.PRO_Speciality, Updated_At: new Date() } },
      { multi: true }
    )
  }

  // Add provider Affiliation by PRO_UID //
  addAffiliation (id, data) {
    return this.model.updateOne(
      { PRO_UID: id },
      { $push: { PRO_Affliation: data } },
      { multi: true }
    )
  }

  // delete Affiliation by id
  deleteAffiliationById (id, affiliationId) {
    return this.model.updateOne(
      { PRO_UID: id },
      { $pull: { PRO_Affliation: { _id: ObjectId(affiliationId) } } }
    )
  }

  // find affliation by id //
  findAffliationById (id) {
    return this.model.aggregate([
      { $unwind: '$PRO_Affliation' },
      { $match: { 'PRO_Affliation._id': ObjectId(id), 'PRO_Affliation.Is_Deleted': false } },
      { $project: { _id: 0, affliation: '$PRO_Affliation.PRO_Affiliations' } }
    ])
  }

  // Update provider Affiliation  id //
  updateAffiliationById (affiliationId, data) {
    return this.model.updateOne(
      { 'PRO_Affliation._id': ObjectId(affiliationId) },
      { $set: { 'PRO_Affliation.$.PRO_Affiliations': data.PRO_Affiliations, Updated_At: new Date() } },
      { multi: true }
    )
  }

  /* Education */
  // Add provider Education by PRO_UID //
  addEducation (id, data) {
    return this.model.updateOne(
      { PRO_UID: id },
      { $push: { PRO_Educations: data } },
      { multi: true }
    )
  }

  // delete Education by id
  deleteEducationById (id, educationId) {
    return this.model.updateOne(
      { PRO_UID: id },
      { $pull: { PRO_Educations: { _id: ObjectId(educationId) } } }
    )
  }

  // find provider education by id //
  findEducationById (id) {
    return this.model.aggregate([
      { $unwind: '$PRO_Educations' },
      { $match: { 'PRO_Educations._id': ObjectId(id), 'PRO_Educations.Is_Deleted': false } },
      { $project: { _id: 0, education: '$PRO_Educations.PRO_Education' } }
    ])
  }

  // Update provider Education  id //
  updateEducationById (educationId, data) {
    return this.model.updateOne(
      { 'PRO_Educations._id': ObjectId(educationId) },
      { $set: { 'PRO_Educations.$.PRO_Education': data.PRO_Education, Updated_At: new Date() } },
      { multi: true }
    )
  }

  /* Experience */
  // Add provider Experience by PRO_UID //
  addExperience (id, data) {
    return this.model.updateOne(
      { PRO_UID: id },
      { $push: { PRO_Experiences: data } },
      { multi: true }
    )
  }

  // delete Experience by id
  deleteExperienceById (id, experienceId) {
    return this.model.updateOne(
      { PRO_UID: id },
      { $pull: { PRO_Experiences: { _id: ObjectId(experienceId) } } }
    )
  }

  // find provider experience by id //
  findExperienceById (id) {
    return this.model.aggregate([
      { $unwind: '$PRO_Experiences' },
      { $match: { 'PRO_Experiences._id': ObjectId(id), 'PRO_Experiences.Is_Deleted': false } },
      { $project: { _id: 0, experience: '$PRO_Experiences.PRO_Experience' } }
    ])
  }

  // Update provider Experience  id //
  updateExperienceById (experienceId, data) {
    return this.model.updateOne(
      { 'PRO_Experiences._id': ObjectId(experienceId) },
      { $set: { 'PRO_Experiences.$.PRO_Experience': data.PRO_Experience, Updated_At: new Date() } },
      { multi: true }
    )
  }

  /* Action */
  // Add Action Education by PRO_UID //
  addAction (id, data) {
    return this.model.updateOne(
      { PRO_UID: id },
      { $push: { PRO_Actions: data } },
      { multi: true }
    )
  }

  // delete Action by id
  deleteActionById (id, actionId) {
    return this.model.updateOne(
      { PRO_UID: id },
      { $pull: { PRO_Actions: { _id: ObjectId(actionId) } } }
    )
  }

  // find action by id //
  findActionById (id) {
    return this.model.aggregate([
      { $unwind: '$PRO_Actions' },
      { $match: { 'PRO_Actions._id': ObjectId(id), 'PRO_Actions.Is_Deleted': false } },
      { $project: { actions: '$PRO_Actions.PRO_Actions_Against' } }
    ])
  }

  // Update provider Action  id //
  updateActionById (actionId, data) {
    return this.model.updateOne(
      { 'PRO_Actions._id': ObjectId(actionId) },
      { $set: { 'PRO_Actions.$.PRO_Actions_Against': data.PRO_Actions_Against, Updated_At: new Date() } },
      { multi: true }
    )
  }

  // Get provider list with search funcationality //
  async getProviderList (data) {
    var countData = {}
    var orQuery = {}
    var andQuery = {}
    orQuery['$or'] = []
    andQuery['$and'] = []
    if (data.search_text) {
      orQuery['$or'].push({ PRO_Username: { $regex: data.search_text, $options: 'i' } })
      orQuery['$or'].push({ 'PRO_Speciality.PRO_Speciality': { $regex: data.search_text, $options: 'i' } })
      orQuery['$or'].push({ PRO_Account_Status: { $regex: data.search_text, $options: 'i' } })
      orQuery['$or'].push({ PRO_Home_Phone: { $regex: data.search_text, $options: 'i' } })
      if (data.search_text.indexOf(' ') >= 0) {
        const split = data.search_text.split(' ')
        andQuery['$and'].push({ PRO_First_Name: { $regex: split[0], $options: 'i' } })
        andQuery['$and'].push({ PRO_Last_Name: { $regex: split[1], $options: 'i' } })
      } else {
        orQuery['$or'].push({ PRO_First_Name: { $regex: data.search_text, $options: 'i' } })
        orQuery['$or'].push({ PRO_Last_Name: { $regex: data.search_text, $options: 'i' } })
      }
    }
    if (andQuery['$and'].length != 0) orQuery['$or'].push(andQuery)
    var match = {}
    if (orQuery['$or'].length != 0) match = orQuery
    const count = await this.model.aggregate([
      {
        $match: match
      },
      {
        $count: 'PRO_Username'
      }
    ])
    var provider = await this.model.aggregate([
      {
        $match: match
      },
      {
        $project: {
          _id: 0,
          Signup: '$Created_At',
          Speciality: {
            $arrayElemAt: [{
              $filter: {
                input: '$PRO_Speciality',
                as: 'item',
                cond: { $ne: ['$$item.PRO_Primary_Speciality', false] }
              }
            }, 0]
          },
          PRO_First_Name: '$PRO_First_Name',
          PRO_Last_Name: '$PRO_Last_Name',
          Email: '$PRO_Username',
          Phone: '$PRO_Home_Phone',
          Status: '$PRO_Account_Status',
          Last_Active: '$PRO_Last_LogIn',
          PRO_UID: '$PRO_UID',
          PRO_Number_of_Unsigned_Docs: 1,
          PRO_Number_of_Complete_Charts: 1,
          PRO_Earnings_to_Date: 1
        }
      },
      {
        $project: {
          Signup: '$Signup',
          Speciality: '$Speciality.PRO_Speciality',
          PRO_First_Name: '$PRO_First_Name',
          PRO_Last_Name: '$PRO_Last_Name',
          Email: '$Email',
          Phone: '$Phone',
          Status: '$Status',
          Last_Active: '$Last_Active',
          PRO_UID: '$PRO_UID',
          PRO_Number_of_Unsigned_Docs: '$PRO_Number_of_Unsigned_Docs',
          PRO_Number_of_Complete_Charts: '$PRO_Number_of_Complete_Charts',
          PRO_Earnings_to_Date: '$PRO_Earnings_to_Date'
        }
      },
      {
        $sort: data.column == 'signup' ? { Signup: data.order == 'desc' ? -1 : 1 } : data.column == 'status' ? { Status: data.order == 'desc' ? -1 : 1 } : data.column == 'speciality' ? { Speciality: data.order == 'desc' ? -1 : 1 } : data.column == 'name' ? { PRO_First_Name: data.order == 'desc' ? -1 : 1 } : data.column == 'email' ? { Email: data.order == 'desc' ? -1 : 1 } : data.column == 'phone' ? { Phone: data.order == 'desc' ? -1 : 1 } : data.column == 'last_active' ? { Last_Active: data.order == 'desc' ? -1 : 1 } : data.column == 'Unsigned_Docs' ? { PRO_Number_of_Unsigned_Docs: data.order == 'desc' ? -1 : 1 } : data.column == 'Complete_Charts' ? { PRO_Number_of_Complete_Charts: data.order == 'desc' ? -1 : 1 } : data.column == 'PRO_Earnings' ? { PRO_Earnings_to_Date: data.order == 'desc' ? -1 : 1 } : { Signup: data.order == 'asc' ? 1 : -1 }
      },
      { $skip: data.skip != 0 ? data.skip : 0 }, { $limit: data.limit != 0 ? data.limit : 10 }
    ])
    countData.total = count.length > 0 ? count[0].PRO_Username : 0
    countData.result = provider
    return countData
  }

  // Export provider csv //
  exportProviderCSV (data) {
    var orQuery = {}
    var andQuery = {}
    orQuery['$or'] = []
    andQuery['$and'] = []
    if (data.search_text) {
      orQuery['$or'].push({ PRO_Username: { $regex: data.search_text, $options: 'i' } })
      orQuery['$or'].push({ 'PRO_Speciality.PRO_Speciality': { $regex: data.search_text, $options: 'i' } })
      orQuery['$or'].push({ PRO_Account_Status: { $regex: data.search_text, $options: 'i' } })
      orQuery['$or'].push({ PRO_Home_Phone: { $regex: data.search_text, $options: 'i' } })
      if (data.search_text.indexOf(' ') >= 0) {
        const split = data.search_text.split(' ')
        andQuery['$and'].push({ PRO_First_Name: { $regex: split[0], $options: 'i' } })
        andQuery['$and'].push({ PRO_Last_Name: { $regex: split[1], $options: 'i' } })
      } else {
        orQuery['$or'].push({ PRO_First_Name: { $regex: data.search_text, $options: 'i' } })
        orQuery['$or'].push({ PRO_Last_Name: { $regex: data.search_text, $options: 'i' } })
      }
    }
    if (andQuery['$and'].length != 0) orQuery['$or'].push(andQuery)
    var match = {}
    if (orQuery['$or'].length != 0) match = orQuery
    return this.model.aggregate([
      {
        $match: match
      },
      {
        $project: {
          _id: 0,
          Signup: '$Created_At',
          Speciality: {
            $arrayElemAt: [{
              $filter: {
                input: '$PRO_Speciality',
                as: 'item',
                cond: { $ne: ['$$item.PRO_Primary_Speciality', false] }
              }
            }, 0]
          },
          PRO_First_Name: '$PRO_First_Name',
          PRO_Last_Name: '$PRO_Last_Name',
          Email: '$PRO_Username',
          Phone: '$PRO_Home_Phone',
          Status: '$PRO_Account_Status',
          Last_Active: '$PRO_Last_LogIn',
          PRO_UID: '$PRO_UID',
          PRO_Number_of_Unsigned_Docs: 1,
          PRO_Number_of_Complete_Charts: 1,
          PRO_Earnings_to_Date: 1
        }
      },
      {
        $project: {
          Signup: '$Signup',
          Speciality: '$Speciality.PRO_Speciality',
          PRO_First_Name: '$PRO_First_Name',
          PRO_Last_Name: '$PRO_Last_Name',
          Email: '$Email',
          Phone: '$Phone',
          Status: '$Status',
          Last_Active: '$Last_Active',
          PRO_UID: '$PRO_UID',
          PRO_Number_of_Unsigned_Docs: '$PRO_Number_of_Unsigned_Docs',
          PRO_Number_of_Complete_Charts: '$PRO_Number_of_Complete_Charts',
          PRO_Earnings_to_Date: '$PRO_Earnings_to_Date'
        }
      }
    ])
  }

  // Get provider by PRO_UID //
  async getProviderByPRO_UID (id) {
    return this.model.findOne({ PRO_UID: id })
  }

  // Get available provider list //
  getAvailableProviders () {
    return this.model.aggregate([
      {
        $project: {
          _id: 0,
          Speciality: {
            $arrayElemAt: [{
              $filter: {
                input: '$PRO_Speciality',
                as: 'item',
                cond: { $ne: ['$$item.PRO_Primary_Speciality', false] }
              }
            }, 0]
          },
          PRO_First_Name: '$PRO_First_Name',
          PRO_Last_Name: '$PRO_Last_Name',
          PRO_Profile_Picture: '$PRO_Profile_Picture',
          PRO_Office_City: '$PRO_Office_City',
          PRO_Office_State: '$PRO_Office_State',
          home_rate: '$PRO_Home_Office_Hours_New_Visit_Rate',
          office_rate: '$PRO_Office_Office_Hours_New_Visit_Rate'
        }
      },
      {
        $project: {
          Speciality: '$Speciality.PRO_Speciality',
          PRO_First_Name: '$PRO_First_Name',
          PRO_Last_Name: '$PRO_Last_Name',
          PRO_Profile_Picture: '$PRO_Profile_Picture',
          PRO_Office_City: '$PRO_Office_City',
          PRO_Office_State: '$PRO_Office_State',
          home_rate: '$PRO_Home_Office_Hours_New_Visit_Rate',
          office_rate: '$PRO_Office_Office_Hours_New_Visit_Rate'
        }
      }
    ])
  }

  // provider for listing in audit logs //
  providerForAuditLogs (search) {
    if (search) {
      var orFilter = {}
      var provider = {}
      orFilter['$or'] = []
      provider['$and'] = []
      if (search.indexOf(' ') >= 0) {
        const split = search.split(' ')
        if (split.length > 2) return []
        provider['$and'].push({ PRO_First_Name: { $regex: split[0], $options: 'i' } })
        provider['$and'].push({ PRO_Last_Name: { $regex: split[1], $options: 'i' } })
      } else {
        // orFilter['$or'].push({ PRO_UID: { $regex: search, $options: 'i' } })
        orFilter['$or'].push({ PRO_First_Name: { $regex: search, $options: 'i' } })
        orFilter['$or'].push({ PRO_Last_Name: { $regex: search, $options: 'i' } })
      }
      var match = {}
      if (provider['$and'].length != 0) match = provider
      if (orFilter['$or'].length != 0) match = orFilter
      return this.model.aggregate([{ $match: match }, {
        $project: {
          _id: 0,
          Speciality: {
            $arrayElemAt: [{
              $filter: {
                input: '$PRO_Speciality',
                as: 'item',
                cond: { $ne: ['$$item.PRO_Primary_Speciality', false] }
              }
            }, 0]
          },
          PRO_First_Name: '$PRO_First_Name',
          PRO_Last_Name: '$PRO_Last_Name',
          PRO_UID: '$PRO_UID'
        }
      },
      { $project: { Speciality: '$Speciality.PRO_Speciality', PRO_First_Name: '$PRO_First_Name', PRO_Last_Name: '$PRO_Last_Name', PRO_UID: '$PRO_UID' } }])
    }
    return this.model.aggregate([
      {
        $project: {
          _id: 0,
          Speciality: {
            $arrayElemAt: [{
              $filter: {
                input: '$PRO_Speciality',
                as: 'item',
                cond: { $ne: ['$$item.PRO_Primary_Speciality', false] }
              }
            }, 0]
          },
          PRO_First_Name: '$PRO_First_Name',
          PRO_Last_Name: '$PRO_Last_Name',
          PRO_UID: '$PRO_UID'
        }
      },
      { $project: { Speciality: '$Speciality.PRO_Speciality', PRO_First_Name: '$PRO_First_Name', PRO_Last_Name: '$PRO_Last_Name', PRO_UID: '$PRO_UID' } }
    ])
  }

  // get active provider in last 30 Days //
  async getLastThirtyDaysActiveUser (date) {
    var data = {}
    const lastActive = await this.model.find({ PRO_Account_Status: 'Active', PRO_Last_LogIn: { $gte: date } }).count()
    const totalActive = await this.model.find({ PRO_Account_Status: 'Active' }).count()
    data.lastActive = lastActive
    data.totalActive = totalActive
    return data
  }

  getProviderName (PRO_UID) {
    return this.model.findOne({ PRO_UID: PRO_UID }, { PRO_First_Name: 1, PRO_Last_Name: 1 })
  }

  // get available physician //
  async getAbailablePhysician (PT_Age_Now) {
    return this.model.aggregate([
      {
        $match: {
          $and: [{ PT_Age_Min: { $lte: PT_Age_Now } }, { PT_Age_Max: { $gte: PT_Age_Now } }, { PRO_Account_Status: 'Active' }],
          $or: [{ PRO_Home_Rate_Status: 'Complete' }, { PRO_Office_Rate_Status: 'Complete' }]
        }
      },
      {
        $project: {
          _id: 0,
          PRO_Username: '$PRO_Username',
          PRO_First_Name: '$PRO_First_Name',
          PRO_Last_Name: '$PRO_Last_Name',
          PRO_Office_Address_Line_1: '$PRO_Office_Address_Line_1',
          PRO_Office_Address_Line_2: '$PRO_Office_Address_Line_2',
          PRO_Home_Address_Line_1: '$PRO_Home_Address_Line_1',
          PRO_Home_Address_Line_2: '$PRO_Home_Address_Line_2',
          PRO_Office_Zip: '$PRO_Office_Zip',
          PRO_Home_City: '$PRO_Home_City',
          PRO_Home_State: '$PRO_Home_State',
          PRO_Home_Zip: '$PRO_Home_Zip',
          PRO_UID: '$PRO_UID',
          PRO_Profile_Picture: '$PRO_Profile_Picture',
          PRO_Office_Address_Latitude: '$PRO_Office_Address_Latitude',
          PRO_Office_Address_Longitude: '$PRO_Office_Address_Longitude',
          PRO_Bio: '$PRO_Bio',
          PRO_Office_City: '$PRO_Office_City',
          PRO_Office_State: '$PRO_Office_State',
          PRO_Home_Office_Hours_New_Visit_Rate: '$PRO_Home_Office_Hours_New_Visit_Rate',
          PRO_Office_Office_Hours_New_Visit_Rate: '$PRO_Office_Office_Hours_New_Visit_Rate',
          PRO_Office_Off_Hours_New_Visit_Rate: '$PRO_Office_Off_Hours_New_Visit_Rate',
          PRO_Home_Off_Hours_New_Visit_Rate: '$PRO_Home_Off_Hours_New_Visit_Rate',

          PRO_Miles_to_Travel: '$PRO_Miles_to_Travel',
          PRO_Home_Visit_Buffer: '$PRO_Home_Visit_Buffer',
          PRO_Board_Cert: '$PRO_Board_Cert',
          PRO_Educations: '$PRO_Educations.PRO_Education',
          PRO_Affliation: '$PRO_Affliation',
          PRO_Experiences: '$PRO_Experiences',
          PRO_Actions: '$PRO_Actions.PRO_Actions_Against',
          PRO_Speciality: '$PRO_Speciality',
          PRO_Special_Rate: '$PRO_Special_Rate',
          Pro_Office_Hours_Time: '$Pro_Office_Hours_Time',
          PRO_Notice_for_Home_Visit: '$PRO_Notice_for_Home_Visit',
          PRO_Notice_for_Office_Visit: '$PRO_Notice_for_Office_Visit',
          PRO_Office_Rate_Status: '$PRO_Office_Rate_Status',
          PRO_Home_Rate_Status: '$PRO_Home_Rate_Status',
          PRO_Office_Visit_Buffer: '$PRO_Office_Visit_Buffer',
          PRO_Home_Office_Hours_Followup_Rate: '$PRO_Home_Office_Hours_Followup_Rate',
          PRO_Office_Office_Hours_Followup_Rate: '$PRO_Office_Office_Hours_Followup_Rate',
          PRO_Home_Off_Hours_Followup_Rate: '$PRO_Home_Off_Hours_Followup_Rate',
          PRO_Office_Off_Hours_Followup_Rate: '$PRO_Office_Off_Hours_Followup_Rate'
        }
      }
    ])
  }

  getActiveProvider () {
    return this.model.find({ PRO_Account_Status: 'Active' })
  }
}

module.exports = ProviderModel
