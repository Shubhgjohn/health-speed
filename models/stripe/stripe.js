/* eslint-disable camelcase */
const stripe = require('stripe')(process.env.SECRET_KEY)
// stripe.setMaxNetworkRetries(2)
class StripeModel {
// Creater stripe customer //
  createCutomer (email) {
    return stripe.customers.create(
      {
        description: `Customer for ${email}`,
        email: email
      })
  }

  // create stripe account //
  createStripeAccount (email) {
    return stripe.accounts.create(
      {
        type: 'custom',
        country: 'US',
        email: email,
        requested_capabilities: [
          'card_payments',
          'transfers'
        ]
      }
    )
  }

  // Create customer bank account //
  createBankAccount (customerId, name, data) {
    return stripe.customers.createSource(
      customerId,
      {
        source: {
          object: 'bank_account',
          country: 'US',
          currency: 'USD',
          account_holder_name: name,
          routing_number: data.PRO_Routing_Number,
          account_number: data.PRO_Account_Number,
          account_holder_type: 'individual'
        }
      })
  }

  // Get Customer bank account details //
  getBankAccount (accountId) {
    return stripe.accounts.retrieve(
      accountId
    )
  }

  getCardList (customerId) {
    return stripe.customers.listSources(
      customerId,
      { object: 'card' }
    )
  }

  getCardByCardId (customerId, cardID) {
    return stripe.customers.retrieveSource(
      customerId,
      cardID
    )
  }

  // Delete customer bank account //
  deleteBankAccount (customerId, accountId) {
    return stripe.customers.deleteSource(customerId, accountId)
  }

  deleteCreditCard (customerId, cardID) {
    return stripe.customers.deleteSource(
      customerId,
      cardID
    )
  }

  makeDefault (customerId, cardId) {
    return stripe.customers.update(
      customerId, { default_source: cardId }

    )
  }

  updateCardDetail (customerId, cardId, data) {
    return stripe.customers.updateSource(
      customerId,
      cardId,
      data
    )
  }

  createCard (customerId, token) {
    return stripe.customers.createSource(
      customerId,
      { source: token }
    )
  }

  craeteAccount (user, body, ip, businessData) {
    const business_profile = body.PRO_Entity_TYPE !== 'individual' ? { url: body.PRO_WebAddress } : { product_description: 'Medical services' }
    return stripe.accounts.create({
      type: 'custom',
      country: 'US',
      email: user.PRO_Username,
      business_type: body.PRO_Entity_TYPE,
      business_profile: business_profile,
      [body.PRO_Entity_TYPE]: businessData,
      requested_capabilities: ['transfers'],
      external_account: {
        object: 'bank_account',
        country: 'US',
        currency: 'USD',
        account_holder_name: user.PRO_First_Name,
        routing_number: body.PRO_Routing_Number,
        account_number: body.PRO_Account_Number,
        account_holder_type: body.PRO_Entity_TYPE
      },
      tos_acceptance: {
        date: Math.floor(Date.now() / 1000),
        ip: ip // Assumes you're not using a proxy
      }
    })
  }

  updateAccount (user, body, ip, businessData) {
    return stripe.accounts.update(
      user.StripeAccountId,
      {
        business_type: body.PRO_Entity_TYPE,
        [body.PRO_Entity_TYPE]: businessData,
        external_account: {
          object: 'bank_account',
          country: 'US',
          currency: 'USD',
          account_holder_name: user.PRO_First_Name,
          routing_number: body.PRO_Routing_Number,
          account_number: body.PRO_Account_Number,
          account_holder_type: body.PRO_Entity_TYPE
        },
        tos_acceptance: {
          date: Math.floor(Date.now() / 1000),
          ip: ip // Assumes you're not using a proxy
        }
      }
    )
  }

  deleteAccount (accountId) {
    return stripe.accounts.del(
      accountId
    )
  }
}
module.exports = StripeModel
