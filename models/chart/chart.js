/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const BaseModel = require('../base')
const mongoose = require('mongoose')
const moment = require('moment-timezone')
const Schema = mongoose.Schema
var autopopulate = require('mongoose-autopopulate')
var ObjectId = require('mongodb').ObjectID
function omitPrivate (doc, obj) {
	delete obj.__v
	delete obj.id
	delete obj._id
	return obj
}
var options = { toJSON: { transform: omitPrivate } }

const CHART_COLLECTION = 'chart'

const DiagnosisSchema = new Schema({
	PRO_Diagnosis_Note: { type: String, default: null },
	Is_Deleted: { type: Boolean, default: false }
})

const PRO_Diagnosis_ICD_Code = new Schema({
	PRO_Diagnosis_ICD10_Code: { type: String, default: null },
	PRO_Diagnosis_ICD10_Code_Order: { type: Number, default: 0 },
	PRO_Diagnosis_ICD10_Code_Description: { type: String },
	Is_Deleted: { type: Boolean, default: false }
})

const PRO_Diagnosis_Attchment = new Schema({
	Attachment_Name: { type: String, default: null },
	Attachment_Url: { type: String, default: null },
	Attachment_By: { type: String, default: null },
	Attachment_User: { type: String, default: null },
	Is_Deleted: { type: Boolean, default: false }
})

const PRO_HPI_Attchment = new Schema({
	Attachment_Name: { type: String, default: null },
	Attachment_Url: { type: String, default: null },
	Attachment_By: { type: String, default: null },
	Attachment_User: { type: String, default: null },
	Is_Deleted: { type: Boolean, default: false }
})
const PRO_Review_of_Systems_Attchment = new Schema({
	Attachment_Name: { type: String, default: null },
	Attachment_Url: { type: String, default: null },
	Attachment_By: { type: String, default: null },
	Attachment_User: { type: String, default: null },
	Is_Deleted: { type: Boolean, default: false }
})

const PRO_Exam_Attchment = new Schema({
	Attachment_Name: { type: String, default: null },
	Attachment_Url: { type: String, default: null },
	Attachment_By: { type: String, default: null },
	Attachment_User: { type: String, default: null },
	Is_Deleted: { type: Boolean, default: false }
})

const PRO_Results_Note_Attchment = new Schema({
	Attachment_Name: { type: String, default: null },
	Attachment_Url: { type: String, default: null },
	Attachment_By: { type: String, default: null },
	Attachment_User: { type: String, default: null },
	Is_Deleted: { type: Boolean, default: false }
})
const PRO_Plan_Attchment = new Schema({
	Attachment_Name: { type: String, default: null },
	Attachment_Url: { type: String, default: null },
	Attachment_By: { type: String, default: null },
	Attachment_User: { type: String, default: null },
	Is_Deleted: { type: Boolean, default: false }
})

const PRO_Addendum_Attachment = new Schema({
	Attachment_Name: { type: String, default: null },
	Attachment_Url: { type: String, default: null },
	Attachment_By: { type: String, default: null },
	Attachment_User: { type: String, default: null },
	Is_Deleted: { type: Boolean, default: false }
})

const PRO_ICD10 = new Schema({
	PRO_Diagnosis_ICD10_Code: { type: String, default: null },
	PRO_Diagnosis_ICD10_Code_Order: { type: Number, default: 0 },
	PRO_Diagnosis_ICD10_Code_Description: { type: String }
})

const Chart_Access = new Schema({
	PRO_UID: { type: String, default: null },
	Access_Status: { type: Boolean, default: false }
})

const PRO_Addendum = new Schema({
	PRO_Addendum_Note: { type: String, default: null },
	ICD10_Code: [PRO_ICD10],
	PRO_Addendum_Attachment: [PRO_Addendum_Attachment],
	Addendum_Is_Signed: { type: Boolean, default: false },
	Addendum_Sign_Date: { type: Date, default: null },
	Is_Deleted: { type: Boolean, default: false },
	Created_At: { type: Date, default: Date.now },
	Updated_At: { type: Date, default: Date.now }
})

const AddendumView = new Schema({
	Addendum_Id: { type: String, default: null },
	User: { type: String, default: null }
})

const CHART_SCHEMA = new Schema({
	PRO_Patient_Instructions: { type: String, default: null },
	PRO_Private_Note: { type: String, minlength: 0, max: 2000, default: null },
	PRO_HPI: { type: String, default: null },
	PRO_HPI_Attchment: [PRO_HPI_Attchment],
	PRO_Review_of_Systems: { type: String, default: null },
	PRO_Review_of_Systems_Attchment: [PRO_Review_of_Systems_Attchment],
	Vital_Signs_Last_Modified_Timestamp: { type: Date, default: null },
	PRO_Exam_Notes: { type: String, default: null },
	PRO_Exam_Notes_Attchment: [PRO_Exam_Attchment],
	PRO_Results_Note: { type: String, default: null },
	PRO_Results_Note_Attachment: [PRO_Results_Note_Attchment],
	PRO_Diagnosis: [DiagnosisSchema],
	PRO_Diagnosis_Attchment: [PRO_Diagnosis_Attchment],
	PRO_Diagnosis_ICD_Code: [PRO_Diagnosis_ICD_Code],
	PRO_Plan_Description: { type: String, default: null },
	PRO_Plan_Attchment: [PRO_Plan_Attchment],
	PRO_Addendum_Note: { type: String, default: null },
	PRO_Addendums: [PRO_Addendum],
	PRO_Addendum_Attachment: [PRO_Addendum_Attachment],
	PRO_Addendum_Edited_Timestamp: { type: Date, default: new Date() },
	PRO_Addendum_Completed_Timestamp: { type: Date, default: new Date() },
	Addendum_Is_Signed: { type: Boolean, default: false },
	Discharge_Instructions_Status: { type: String, default: 'Draft' },
	PRO_Diagnosis_ICD10_Code: { type: String, default: null },
	PRO_Diagnosis_ICD10_Code_Order: { type: Number, default: 0 },
	PRO_UID: { type: String, default: null },
	Chart_is_Signed: { type: Boolean, default: false },
	PT_Username: { type: String, min: 5, max: 140 },
	PT_HS_MRN: String,
	Encounter_UID: { type: String, default: null },
	Encounter_Number_of_Attachments: { type: Number, default: 0 },
	PRO_Number_of_Complete_Charts: { type: Number, default: 0 },
	PRO_Number_of_Unsigned_Docs: { type: Number, default: 0 },
	Notification_Time: { type: Date, default: null },
	PRO_Chart_Access: [Chart_Access],
	PRO_Viewed_Time: { type: Date, default: new Date() },
	PT_Viewed_Time: { type: Date, default: new Date() },
	Discharge_Instructions_Viewed: { type: [String], default: [] },
	Chart_Viewed: { type: [String], default: [] },
	Addendum_Viewed_By: [AddendumView],
	Created_At: { type: Date, default: new Date() },
	Updated_At: { type: Date, default: new Date() },
	Deleted_At: { type: Date, default: null }
}, options)
CHART_SCHEMA.plugin(autopopulate)

var MODEL = mongoose.model(CHART_COLLECTION, CHART_SCHEMA)

// Note: Do not extend this class, only BaseModel is allow to be extended from.
// because more than 2 levels inheritance could lead to tight-coupling design and make everything more complicated
class ChartModel extends BaseModel {
	constructor () {
		super(MODEL, CHART_COLLECTION, CHART_SCHEMA)
	}

	saveICD10Code (Encounter_UID, data) {
		return this.model.findOneAndUpdate({
			Encounter_UID: Encounter_UID
		},
		{
			$push: { PRO_Diagnosis_ICD_Code: data }
		})
	}

	findChartByPT_HS_MRN (PT_HS_MRN) {
		return this.model.find({ PT_HS_MRN: PT_HS_MRN })
	}

	findChartByEncounterUID (Encounter_UID) {
		return this.model.find({ Encounter_UID: Encounter_UID })
	}

	deniedChartAccess (Encounter_UID, PRO_UID) {
		return this.model.findOneAndUpdate({ Encounter_UID: Encounter_UID }, { $pull: { PRO_Chart_Access: { PRO_UID: PRO_UID } } }, { $set: { Updated_At: new Date() } })
	}

	findEncounterChart (Encounter_UID, PRO_UID) {
		return this.model.findOne({ Encounter_UID: Encounter_UID, PRO_UID: PRO_UID })
	}

	findEncounterChartById (Encounter_UID, PRO_UID, PT_HS_MRN) {
		return this.model.findOne({ Encounter_UID: Encounter_UID, PRO_UID: PRO_UID, PT_HS_MRN: PT_HS_MRN })
	}

	findAddendume (id) {
		return this.model.find({ 'PRO_Addendums._id': ObjectId(id) })
	}

	getChartData (Encounter_UID) {
		return this.model.findOne({ Encounter_UID: Encounter_UID })
	}

	updateChart (Encounter_UID, data, updatedAt) {
		return this.model.updateOne({ Encounter_UID: Encounter_UID }, { $set: data, Updated_At: updatedAt }, { multi: true }, (err, doc) => {
			if (err) {
				console.log('Something wrong when updating data!')
			}
		})
	}

	UpdatePRO_Access_Chart (Encounter_UID, data) {
		return this.model.updateOne({ Encounter_UID: Encounter_UID },
			{ $push: { PRO_Chart_Access: data } },
			{ $set: { Updated_At: new Date() } }
		)
	}

	ChartAccess (Encounter_UID, PRO_UID) {
		return this.model.updateOne({ Encounter_UID: Encounter_UID, 'PRO_Chart_Access.PRO_UID': PRO_UID }, { $set: { 'PRO_Chart_Access.$.Access_Status': true } }, { new: true })
	}

	createAddendum (Encounter_UID, data, updatedAt) {
		return this.model.findOneAndUpdate(
			{ Encounter_UID: Encounter_UID },
			{ $push: { PRO_Addendums: data } },
			{ $set: { Updated_At: updatedAt } }
		)
	}

	updateAddendum (Encounter_UID, value) {
		return this.model.updateOne({ Encounter_UID: Encounter_UID, 'PRO_Addendums._id': ObjectId(value._id) },
			{
				$set: {
					'PRO_Addendums.$.PRO_Addendum_Note': value.PRO_Addendum_Note,
					'PRO_Addendums.$.Addendum_Is_Signed': value.Addendum_Is_Signed,
					'PRO_Addendums.$.Addendum_Sign_Date': value.Addendum_Sign_Date,
					'PRO_Addendums.$.Updated_At': moment(Date.parse(moment.tz(new Date(), process.env.CURRENT_TIMEZONE).format('YYYY-MM-DDTHH:mm:ss'))).toDate()
				}
			})
	}

	updatePatientEmail (oldEmail, newEmail) {
		let data = {
			PT_Username: newEmail,
			Updated_At: new Date()
		}
		return this.model.updateMany({ PT_Username: oldEmail },
			{ $set: data }, { multi: true })
	}

	getProviderChart (PRO_UID) {
		return this.model.aggregate([
			{
				$match: { PRO_UID: PRO_UID }
			},
			{
				$lookup: {
					from: 'encounters',
					localField: 'Encounter_UID',
					foreignField: 'Encounter_UID',
					as: 'encounter'
				}
			},
			{ $unwind: '$encounter' },
			{
				$lookup: {
					from: 'patients',
					localField: 'PT_HS_MRN',
					foreignField: 'PT_HS_MRN',
					as: 'patient'
				}
			},
			{ $unwind: '$patient' },
			{
				$project: {
					Encounter_Time: '$encounter.Encounter_Time',
					Encounter_Date: '$encounter.Encounter_Date',
					Encounter_Type: '$encounter.Encounter_Type',
					Encounter_Chief_Complaint_PRO: '$encounter.Encounter_Chief_Complaint_PRO',
					Encounter_UID: '$encounter.Encounter_UID',
					Encounter_Status: '$encounter.Encounter_Status',
					PRO_UID: '$encounter.PRO_UID',
					Primary_DX: '$encounter.Primary_DX',
					Addendum_Is_Signed: 1,
					PRO_Addendum_Attchment: 1,
					PT_First_Name: '$patient.PT_First_Name',
					PT_Last_Name: '$patient.PT_Last_Name',
					PT_Age_Now: '$patient.PT_Age_Now',
					PT_Gender: '$patient.PT_Gender',
					PT_DOB: '$patient.PT_DOB',
					PRO_Addendum_Note: 1,
					Discharge_Instructions_Status: 1,
					Chart_is_Signed: 1,
					PT_Profile_Picture: '$patient.PT_Profile_Picture',
					PRO_Addendums: 1,
					_id: 0
				}
			}
		])
	}

	uploadAddendumAttachment (Encounter_UID, id, data) {
		return this.model.updateOne({ Encounter_UID: Encounter_UID, 'PRO_Addendums._id': ObjectId(id) },
			{
				$push: {
					'PRO_Addendums.$.PRO_Addendum_Attachment': data
				}
			})
	}

	viewDischargeInstruction (Encounter_UID, userId) {
		return this.model.updateOne({ Encounter_UID: Encounter_UID },
			{
				$push: {
					Discharge_Instructions_Viewed: userId
				}
			})
	}

	viewChartData (Encounter_UID, userId) {
		return this.model.updateOne({ Encounter_UID: Encounter_UID },
			{
				$push: {
					Chart_Viewed: userId
				}
			})
	}

	viewAddendum (Encounter_UID, data) {
		return this.model.updateOne({ Encounter_UID: Encounter_UID },
			{
				$push: {
					Addendum_Viewed_By: data
				}
			})
	}

	uploadAttachment (Encounter_UID, data, attachmentKey) {
		var json = { josnData: data }
		json[`${attachmentKey}`] = data
		delete json.josnData
		return this.model.findOneAndUpdate(
			{ Encounter_UID: Encounter_UID },
			{ $push: json },
			{ $set: { Updated_At: new Date() } }
		)
	}

	deleteAddendumAttchment (Encounter_UID, _id, Addendum_Id) {
		return this.model.updateOne(
			{ Encounter_UID: Encounter_UID, PRO_Addendums: ObjectId(Addendum_Id) },
			{ $pull: { 'PRO_Addendums.$.PRO_Addendum_Attachment': { _id: ObjectId(_id) } } },
			{ $set: { Updated_At: new Date() } }
		)
	}

	deleteReviewofSystemsAttchment (Encounter_UID, _id) {
		return this.model.updateOne(
			{ Encounter_UID: Encounter_UID },
			{ $pull: { PRO_Review_of_Systems_Attchment: { _id: ObjectId(_id) } } },
			{ $set: { Updated_At: new Date() } }
		)
	}

	deleteExamNotesAttchment (Encounter_UID, _id) {
		return this.model.updateOne(
			{ Encounter_UID: Encounter_UID },
			{ $pull: { PRO_Exam_Notes_Attchment: { _id: ObjectId(_id) } } },
			{ $set: { Updated_At: new Date() } }
		)
	}

	deleteResultsNoteAttchment (Encounter_UID, _id) {
		return this.model.updateOne(
			{ Encounter_UID: Encounter_UID },
			{ $pull: { PRO_Review_of_Systems_Attchment: { _id: ObjectId(_id) } } },
			{ $set: { Updated_At: new Date() } }
		)
	}

	deleteDiagnosisAttchment (Encounter_UID, _id) {
		return this.model.updateOne(
			{ Encounter_UID: Encounter_UID },
			{ $pull: { PRO_Diagnosis_Attchment: { _id: ObjectId(_id) } } },
			{ $set: { Updated_At: new Date() } }
		)
	}

	deletePlanAttchment (Encounter_UID, _id) {
		return this.model.updateOne(
			{ Encounter_UID: Encounter_UID },
			{ $pull: { PRO_Plan_Attchment: { _id: ObjectId(_id) } } },
			{ $set: { Updated_At: new Date() } }
		)
	}

	deleteHPIAttchment (Encounter_UID, _id) {
		return this.model.updateOne(
			{ Encounter_UID: Encounter_UID },
			{ $pull: { PRO_HPI_Attchment: { _id: ObjectId(_id) } } },
			{ $set: { Updated_At: new Date() } }
		)
	}

	deleteAddendum (Encounter_UID, id) {
		return this.model.updateOne(
			{ Encounter_UID: Encounter_UID },
			{ $pull: { PRO_Addendums: { _id: ObjectId(id) } } },
			{ $set: { Updated_At: new Date() } }
		)
	}

	deleteAddendumAttachment (Encounter_UID, Addendum_Id, Attachment_Id) {
		return this.model.updateOne({ Encounter_UID: Encounter_UID, 'PRO_Addendums._id': ObjectId(Addendum_Id) },
			{
				$pull: {
					'PRO_Addendums.$.PRO_Addendum_Attachment': { _id: ObjectId(Attachment_Id) }
				}
			}, { $set: { Updated_At: new Date() } }
		)
	}

	signAddendum (Encounter_UID, Addendum_Id) {
		return this.model.updateOne({ Encounter_UID: Encounter_UID, 'PRO_Addendums._id': ObjectId(Addendum_Id) },
			{ $set: { 'PRO_Addendums.$.Addendum_Is_Signed': true, 'PRO_Addendums.$.Addendum_Sign_Date': new Date() } }, { new: true }
		)
	}

	deleteICD10Code (Addendum_Id, ICD10_Id) {
		return this.model.updateOne({ 'PRO_Addendums._id': ObjectId(Addendum_Id) },
			{
				$pull: {
					'PRO_Addendums.$.ICD10_Code': { _id: ObjectId(ICD10_Id) }
				}
			}, { $set: { Updated_At: new Date() } }
		)
	}

	updateEncounterChartById (id, data) {
		return this.model.updateOne({ Encounter_UID: id }, { $set: data })
	}

	addICD10Code (Encounter_UID, id, data) {
		return this.model.updateOne({ Encounter_UID: Encounter_UID, 'PRO_Addendums._id': ObjectId(id) },
			{
				$push: {
					'PRO_Addendums.$.ICD10_Code': data
				}
			})
	}

	getProviderUnsignedDocCount (PRO_UID) {
		return this.model.find({ PRO_UID: PRO_UID, Chart_is_Signed: false }).count()
	}

	getProviderSignedDocCount (PRO_UID) {
		return this.model.find({ PRO_UID: PRO_UID, Chart_is_Signed: true }).count()
	}

	getPatientSignedDocCount (PT_Username) {
		return this.model.find({ PT_Username: PT_Username, Chart_is_Signed: false }).count()
	}

	getUnsignedChart () {
		return this.model.find({ Chart_is_Signed: false })
	}
}

module.exports = ChartModel
