/* eslint-disable camelcase */
const BaseModel = require('../base')
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const TRANSACTION_COLLECTION = 'transaction'

const TRANSACTION_SCHEMA = new Schema({
  Encounter_UID: { type: String, default: null },
  Payment_Type: { type: String, default: null },
  Payment_Date: Date,
  Amount: Number,
  receipt_url: { type: String, default: null },
  Transaction_Id: { type: String, default: null },
  Charge_Id: { type: String, default: null },
  payment_method_details: JSON,
  Transaction_Status: { type: String, default: null },
  Payment_By: { type: String, default: null },
  PRO_UID: { type: String, default: null },
  Created_At: { type: Date, default: Date.now },
  Updated_At: { type: Date, default: Date.now },
  Deleted_At: { type: Date, default: null }
})

var MODEL = mongoose.model(TRANSACTION_COLLECTION, TRANSACTION_SCHEMA)

const stripe = require('stripe')(process.env.SECRET_KEY)

class TRANSACTION_MODEL extends BaseModel {
  constructor () {
    super(MODEL, TRANSACTION_COLLECTION, TRANSACTION_SCHEMA)
  }

  async getLastThirtyDaysTransaction (date) {
    var data = {}
    const pastTransaction = await this.model.aggregate([
      {
        $match: {
          Created_At: { $gte: date },
          Transaction_Status: 'Paid'
        }
      },
      {
        $group: {
          _id: { Transaction_Status: '$Transaction_Status' },
          totalAmount: { $sum: '$Amount' }
        }
      }
    ])
    const totalTransaction = await this.model.aggregate([
      {
        $match: {
          Transaction_Status: 'Paid'
        }
      },
      {
        $group: {
          _id: { Transaction_Status: '$Transaction_Status' },
          totalAmount: { $sum: '$Amount' }
        }
      }
    ])
    data.pastTransaction = pastTransaction.length > 0 ? pastTransaction[0].totalAmount : 0
    data.totalTransaction = totalTransaction.length > 0 ? totalTransaction[0].totalAmount : 0
    return data
  }

  getTransactionByEncounterId (Encounter_UID) {
    return this.model.findOne({ Encounter_UID: Encounter_UID, Transaction_Status: 'Paid' })
  }

  getTransactionDate (Encounter_UID) {
    return this.model.findOne({ Encounter_UID: Encounter_UID}, { Payment_Date : 1 })
  }
  getChargeDetails (ch_id) {
    return stripe.charges.retrieve(
      ch_id
    )
  }

  getTransactionDetails (txn_id) {
    return stripe.balanceTransactions.retrieve(
      txn_id
    )
  }

  getMonthData (PRO_UID, first_date, last_date) {
    return this.model.aggregate([
      {
        $match: { PRO_UID: PRO_UID, Transaction_Status: 'Paid', $and: [{ Created_At: { $gte: first_date } }, { Created_At: { $lte: last_date } }] }
      },
      {
        $lookup: {
          from: 'encounters',
          localField: 'Encounter_UID',
          foreignField: 'Encounter_UID',
          as: 'encounter'
        }
      },
      { $unwind: '$encounter' }
    ])
  }

  getTotalEarning (PRO_UID) {
    return this.model.aggregate([
      {
        $match: {
          PRO_UID: PRO_UID, Transaction_Status: 'Paid'
        }
      },
      {
        $lookup: {
          from: 'encounters',
          localField: 'Encounter_UID',
          foreignField: 'Encounter_UID',
          as: 'encounter'
        }
      }
    ])
  }

  getFailedTransaction() {
      return this.model.aggregate([
        {
          $match: {
            Transaction_Status: 'Failed'
          }
        },
        {
          $lookup: {
            from: 'encounters',
            localField: 'Encounter_UID',
            foreignField: 'Encounter_UID',
            as: 'encounter'
          }
        },
        { $unwind: '$encounter' },
        {
          $project: {
            Transaction_Status: 1,
            Encounter_UID: 1,
            PT_HS_MRN: '$encounter.PT_HS_MRN',
            PT_Username: '$encounter.PT_Username',
            Encounter_Transaction_Status: '$encounter.Encounter_Transaction_Status',
            Created_At : 1,
            Updated_At : 1,
            Encounter_Payment_Failure_Mail: '$encounter.Encounter_Payment_Failure_Mail'
          }
        }
      ])
  }
}

module.exports = TRANSACTION_MODEL
