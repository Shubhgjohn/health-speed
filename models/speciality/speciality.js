const BaseModel = require('../base')
const mongoose = require('mongoose')
const Schema = mongoose.Schema
var autopopulate = require('mongoose-autopopulate')

const SPECIALITY_COLLECTION = 'speciality'
function omitPrivate (doc, obj) {
  delete obj.Created_At
  delete obj.__v
  delete obj.id
  delete obj.Deleted_At
  delete obj.Updated_At
  return obj
}
var options = { toJSON: { transform: omitPrivate } }
const SPECIALITY_SCHEMA = new Schema(
  {
    PRO_Specialty: { type: String },
    Is_Deleted: { type: Boolean, default: false },
    Created_At: { type: Date, default: new Date() },
    Updated_At: { type: Date, default: new Date() },
    Deleted_At: { type: Date, default: null }
  },
  options
)
SPECIALITY_SCHEMA.plugin(autopopulate)
var MODEL = mongoose.model(SPECIALITY_COLLECTION, SPECIALITY_SCHEMA)

// Note: Do not extend this class, only BaseModel is allow to be extended from.
// because more than 2 levels inheritance could lead to tight-coupling design and make everything more complicated
class SpecialityModel extends BaseModel {
  constructor () {
    super(MODEL, SPECIALITY_COLLECTION, SPECIALITY_SCHEMA)
  }
}

module.exports = SpecialityModel
