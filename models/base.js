/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
/* eslint-disable new-cap */
var mongoose = require('mongoose')

class BaseModel {
  constructor (model = null, collection = null, schema = {}) {
    this.schema = schema
    this.collection = collection
    this.subscriberList = []
    this.parentCollectionId = null
    this.model = model

    if (!collection) throw new Error('No `collection` parameter provided in constructor.')
  }

  // Save(create) data(collection)
  create (data) {
    return new this.model(data).save()
  }

  insert (data) {
    return this.model.insertMany(data)
  }

  // Get last inserted data into collection by id //
  lastInserted () {
    return this.model.find({}).sort({ _id: -1 }).limit(1)
  }

  batchCreate (dataArray) {

  }

  // Find data by id from collection //
  findById (id) {
    return this.model.find({ _id: id, is_Deleted: false })
  }

  // For get all records from collection //
  find () {
    return this.model.find()
  }

  findOrCreateById (id, data) {

  }

  // Find data by user name //
  findOneByName (name) {
    return this.model.findOne({ name: name })
  }

  // Update user record by id //
  updateById (id, data) {
    id = mongoose.Types.ObjectId(id)
    var q = this.model.updateOne({ _id: id }, { $set: data })
    return q
  }

  // Find user by user email //
  findOneByEmail (email) {
    return this.model.findOne({ email: email })
  }

  updateByEmail (email, data) {

  }

  // delete one user by id //
  deleteById (id) {
    return this.model.remove({ _id: id })
  }

  hardDeleteById (id) {

  }

  subscribe (onNext, onError) {

  }

  unsubscribe () {

  }

  getIpAddress (req) {
    const forwarded = req.headers['x-forwarded-for']
    const ip = forwarded ? forwarded.split(/, /)[0] : req.connection.remoteAddress
    return ip
  }
}

module.exports = BaseModel
