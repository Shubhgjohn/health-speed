/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const BaseModel = require('../base')
const mongoose = require('mongoose')
const Schema = mongoose.Schema

var autopopulate = require('mongoose-autopopulate')
function omitPrivate (doc, obj) {
	delete obj.Created_At
	delete obj.__v
	delete obj.id
	delete obj._id
	return obj
}
var options = { toJSON: { transform: omitPrivate } }

const ORDER_COLLECTION = 'order'
const ORDER_SCHEMA = new Schema({
	PRO_UID: { type: String, default: null },
	Encounter_UID: { type: String, default: null },
	PT_Username: { type: String, min: 5, max: 140 },
	PT_HS_MRN: { type: String, default: null },
	Order_UID: { type: String, default: null },
	Days_to_Order_Reminder: Number,
	Order_ICD10_Code: { type: [String], default: null },
	Order_Complete: { type: Boolean, default: false },
	Order_Incomplete: { type: Boolean, default: true },
	Order_Text: { type: String, min: 1, max: 2000 },
	Reminder_Start_Date: Date,
	Reminder_Date: Date,
	Order_Reminder: { type: String, default: 'Unchecked' },
	Order_Stat: { type: String, default: 'Unchecked' },
	Order_Created_Time: { type: String, default: null },
	Order_Type: { type: String, default: null },
	Order_Created_Date: { type: String, default: null },
	Order_Date_Delivered: Date,
	Order_Date_Marked_Complete: { type: Date, default: null },
	Order_Status: { type: String, default: 'Draft' },
	Order_Review_Status: { type: String, default: null },
	Order_As_Addendum: { type: Boolean, default: false },
	PRO_Viewed_Time: { type: Date, default: new Date() },
	PT_Viewed_Time: { type: Date, default: new Date() },
	Order_Viewed_By: { type: [String], default: [] },
	Created_At: { type: Date, default: new Date() },
	Updated_At: { type: Date, default: new Date() },
	Deleted_At: { type: Date, default: null }
}, options)
ORDER_SCHEMA.plugin(autopopulate)
var MODEL = mongoose.model(ORDER_COLLECTION, ORDER_SCHEMA)

// Note: Do not extend this class, only BaseModel is allow to be extended from.
// because more than 2 levels inheritance could lead to tight-coupling design and make everything more complicated
class OrderModel extends BaseModel {
	constructor () {
		super(MODEL, ORDER_COLLECTION, ORDER_SCHEMA)
	}

	findEncounterOrders (Encounter_UID, PRO_UID, PT_Username) {
		return this.model.find({ Encounter_UID: Encounter_UID, PRO_UID: PRO_UID, PT_Username: PT_Username })
	}

	findEncounterOrdersById (Encounter_UID, PRO_UID, PT_HS_MRN) {
		return this.model.find({ Encounter_UID: Encounter_UID, PRO_UID: PRO_UID, PT_HS_MRN: PT_HS_MRN })
	}

	getOrderByEncounterId (Encounter_UID) {
		return this.model.find({ Encounter_UID: Encounter_UID })
	}

	completeOrders (Encounter_UID) {
		return this.model.updateOne({ Encounter_UID: Encounter_UID },
			{ $set: { Order_Status: 'Delivered', Order_Date_Delivered: new Date(), Updated_At: new Date() } }, { multi: true })
	}

	getAllProviderOrders (PRO_UID) {
		return this.model.aggregate([
			{
				$match: { PRO_UID: PRO_UID, Order_Review_Status: { $ne: 'Cancelled' } }
			},
			{
				$lookup: {
					from: 'patients',
					localField: 'PT_HS_MRN',
					foreignField: 'PT_HS_MRN',
					as: 'patients'
				}
			},
			{ $unwind: '$patients' },
			{
				$lookup: {
					from: 'encounters',
					localField: 'Encounter_UID',
					foreignField: 'Encounter_UID',
					as: 'encounters'
				}
			},
			{ $unwind: '$encounters' },
			{
				$lookup: {
					from: 'charts',
					localField: 'Encounter_UID',
					foreignField: 'Encounter_UID',
					as: 'chart'
				}
			},
			{ $unwind: '$chart' },
			{
				$project: {
					Order_Type: 1,
					Order_Text: 1,
					Order_Reminder: 1,
					Order_Review_Status: 1,
					Order_Complete: 1,
					Order_UID: 1,
					Order_Stat: 1,
					Order_Status: 1,
					Order_As_Addendum: 1,
					Reminder_Date: 1,
					Created_At: 1,
					Encounter_Type: '$encounters.Encounter_Type',
					Encounter_Date: '$encounters.Encounter_Date',
					Encounter_UID: '$encounters.Encounter_UID',
					Encounter_Time: '$encounters.Encounter_Time',
					Encounter_Chief_Complaint_PRO: '$encounters.Encounter_Chief_Complaint_PRO',
					PT_First_Name: '$patients.PT_First_Name',
					PT_Last_Name: '$patients.PT_Last_Name',
					PT_Age_Now: '$patients.PT_Age_Now',
					PT_Gender: '$patients.PT_Gender',
					PT_DOB: '$patients.PT_DOB',
					PT_Profile_Picture: '$patients.PT_Profile_Picture',
					PT_Username: '$patients.PT_Username',
					PT_HS_MRN: '$patients.PT_HS_MRN',
					Chart_is_Signed: '$chart.Chart_is_Signed',
					_id: 0

				}
			}
		])
	}

	getPatientOrder (PT_HS_MRN) {
		return this.model.aggregate([
			{
				$match: { PT_HS_MRN: PT_HS_MRN }
			},
			{
				$lookup: {
					from: 'encounters',
					localField: 'Encounter_UID',
					foreignField: 'Encounter_UID',
					as: 'encounter'
				}
			},
			{ $unwind: '$encounter' },
			{
				$lookup: {
					from: 'charts',
					localField: 'Encounter_UID',
					foreignField: 'Encounter_UID',
					as: 'chart'
				}
			},
			{ $unwind: '$chart' },
			{
				$project: {
					Encounter_Time: '$encounter.Encounter_Time',
					Encounter_Date: '$encounter.Encounter_Date',
					PRO_UID: '$encounter.PRO_UID',
					Order_Created_Date: 1,
					Order_Created_Time: 1,
					Order_Type: 1,
					Order_Text: 1,
					Order_Reminder: 1,
					Order_Review_Status: 1,
					Order_Complete: 1,
					Order_Date_Marked_Complete: 1,
					Order_UID: 1,
					Order_Status: 1,
					Encounter_Provider: '$encounter.Encounter_Provider',
					Encounter_UID: '$encounter.Encounter_UID',
					Encounter_Type: '$encounter.Encounter_Type',
					Encounter_Chief_Complaint_PRO: '$encounter.Encounter_Chief_Complaint_PRO',
					Encounter_Chief_Complaint_PT: '$encounter.Encounter_Chief_Complaint_PT',
					Chart_is_Signed: '$chart.Chart_is_Signed',
					Reminder_Start_Date: 1,
					Reminder_Date: 1
				}
			}
		])
	}

	async getPatientOrderByEmail (data) {
		var countData = {}
		const count = await this.model.aggregate([
			{
				$match: { PT_HS_MRN: data.PT_HS_MRN }
			},
			{
				$count: 'Order_UID'
			}
		])
		const order = await this.model.aggregate([
			{
				$match: { PT_HS_MRN: data.PT_HS_MRN }
			},
			{
				$lookup: {
					from: 'providers',
					localField: 'PRO_UID',
					foreignField: 'PRO_UID',
					as: 'provider'
				}
			}
		])
		countData.total = count.length > 0 ? count[0].Order_UID : 0
		countData.result = order
		return countData
	}

	// Get all order for patient with encounter //
	async getOrderByEmail (email) {
		return this.model.aggregate([
			{
				$match: { PT_Username: email }
			},
			{
				$lookup: {
					from: 'encounters',
					localField: 'Encounter_UID',
					foreignField: 'Encounter_UID',
					as: 'encounter'
				}
			},
			{
				$unwind: '$encounter'
			}
		])
	}

	deleteOrder (id) {
		return this.model.remove({ Order_UID: id })
	}

	getOrderById (id) {
		return this.model.find({ Order_UID: id })
	}

	findOrderById (Order_UID) {
		return this.model.find({ Order_UID: Order_UID })
	}

	viewOrder (Order_UID, user) {
		return this.model.updateOne({ Order_UID: Order_UID },
			{
				$push: {
					Order_Viewed_By: user
				}
			})
	}

	completeOrder (Order_UID, data) {
		return this.model.updateOne({ Order_UID: Order_UID }, {
			$set: {
				Order_Complete: data.Order_Complete,
				Order_Incomplete: !data.Order_Complete,
				Order_Date_Marked_Complete: data.Order_Date_Marked_Complete,
				Updated_At: data.Order_Date_Marked_Complete
			}
		}, { new: true }, (err, doc) => {
			if (err) {
				console.log('Something wrong when updating data!')
			}
		})
	}

	updatePatientEmail (oldEmail, newEmail) {
		let data = {
			PT_Username: newEmail,
			Updated_At: new Date()
		}
		return this.model.updateMany({ PT_Username: oldEmail },
			{ $set: data }, { multi: true })
	}

	updateOrderById (Order_UID, data) {
		return this.model.findOneAndUpdate({ Order_UID: Order_UID }, { $set: data, Updated_At: new Date() }, { new: true }, (err, doc) => {
			if (err) {
				console.log('Something wrong when updating data!')
			}
		})
	}

	updateAllOrders (Encounter_UID, data) {
		return this.model.updateMany({ Encounter_UID: Encounter_UID }, { $set: data, Updated_At: new Date() }, { multi: true }, (err, doc) => {
			if (err) {
				console.log('Something wrong when updating data!')
			}
		})
	}

	getOrderAllDetails (Order_UID) {
		return this.model.aggregate([
			{
				$match: { Order_UID: Order_UID }
			},
			{
				$lookup: {
					from: 'patients',
					localField: 'PT_HS_MRN',
					foreignField: 'PT_HS_MRN',
					as: 'patient'
				}
			},
			{ $unwind: '$patient' },
			{
				$lookup: {
					from: 'providers',
					localField: 'PRO_UID',
					foreignField: 'PRO_UID',
					as: 'provider'
				}
			},
			{ $unwind: '$provider' },
			{
				$project: {
					Order_UID: 1,
					Order_Stat: 1,
					PRO_First_Name: '$provider.PRO_First_Name',
					PRO_Last_Name: '$provider.PRO_Last_Name',
					Order_Created_Date: 1,
					Order_Type: 1,
					Order_Text: 1,
					PRO_UID: 1,
					Encounter_UID: 1,
					Order_ICD10_Code: 1,
					Order_Review_Status: 1,
					Order_Complete: 1,
					Order_As_Addendum: 1,
					Order_Date_Delivered: 1,
					Order_Date_Marked_Complete: 1,
					PRO_Office_Address_Line_1: '$provider.PRO_Office_Address_Line_1',
					PRO_Office_Address_Line_2: '$provider.PRO_Office_Address_Line_2',
					PRO_Office_Phone: '$provider.PRO_Office_Phone',
					PRO_Office_State: '$provider.PRO_Office_State',
					PRO_Office_City: '$provider.PRO_Office_City',
					PRO_Office_Zip: '$provider.PRO_Office_Zip',
					PT_First_Name: '$patient.PT_First_Name',
					PT_Last_Name: '$patient.PT_Last_Name',
					PT_Home_Address_Line_1: '$patient.PT_Home_Address_Line_1',
					PT_Home_Address_Line_2: '$patient.PT_Home_Address_Line_2',
					PT_Home_City: '$patient.PT_Home_City',
					PT_Home_State: '$patient.PT_Home_State',
					PT_Cell_Phone: '$patient.PT_Cell_Phone',
					PT_Home_Zip: '$patient.PT_Home_Zip',
					PT_DOB: '$patient.PT_DOB',
					PT_Age_Now: '$patient.PT_Age_Now',
					PT_Gender: '$patient.PT_Gender',
					PT_HS_MRN: '$patient.PT_HS_MRN',
					Created_At: 1,
					_id: 0

				}
			}
		])
	}
}

module.exports = OrderModel
