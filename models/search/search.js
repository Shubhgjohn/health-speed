const BaseModel = require('../base')
const mongoose = require('mongoose')
const Schema = mongoose.Schema
var autopopulate = require('mongoose-autopopulate')

const SEARCH_COLLECTION = 'search'
const SEARCH_SCHEMA = new Schema({
  PRO_Search_Contact: { type: String, min: 1, max: 140 },
  PRO_Search_Encounters: { type: String, min: 1, max: 140 },
  PRO_Search_Orders: { type: String, min: 1, max: 140 },
  Created_At: { type: Date, default: new Date() },
  Updated_At: { type: Date, default: new Date() },
  Deleted_At: { type: Date, default: null }
})
SEARCH_SCHEMA.plugin(autopopulate)
var MODEL = mongoose.model(SEARCH_COLLECTION, SEARCH_SCHEMA)

// Note: Do not extend this class, only BaseModel is allow to be extended from.
// because more than 2 levels inheritance could lead to tight-coupling design and make everything more complicated
class SearchModel extends BaseModel {
  constructor () {
    super(MODEL, SEARCH_COLLECTION, SEARCH_SCHEMA)
  }
}

module.exports = SearchModel
