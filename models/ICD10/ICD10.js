const BaseModel = require('../base')
const mongoose = require('mongoose')
const Schema = mongoose.Schema
var autopopulate = require('mongoose-autopopulate')

const ICD10_COLLECTION = 'ICD10'
function omitPrivate (doc, obj) {
  delete obj.Created_At
  delete obj.__v
  delete obj.id
  delete obj.Deleted_At
  delete obj.Updated_At
  return obj
}
var options = { toJSON: { transform: omitPrivate } }
const ICD10_SCHEMA = new Schema(
  {
    PRO_Diagnosis_ICD10_Code: { type: String },
    PRO_Diagnosis_ICD10_Code_Order: { type: String },
    PRO_Diagnosis_ICD10_Code_Description: { type: String },
    Created_At: { type: Date, default: new Date() },
    Updated_At: { type: Date, default: new Date() },
    Deleted_At: { type: Date, default: null }
  },
  options
)
ICD10_SCHEMA.plugin(autopopulate)
var MODEL = mongoose.model(ICD10_COLLECTION, ICD10_SCHEMA)

// Note: Do not extend this class, only BaseModel is allow to be extended from.
// because more than 2 levels inheritance could lead to tight-coupling design and make everything more complicated
class ICD10Model extends BaseModel {
  constructor () {
    super(MODEL, ICD10_COLLECTION, ICD10_SCHEMA)
  }

  getICDOrderByCode (code) {
    return this.model.aggregate([
      {
        $match: {
          PRO_Diagnosis_ICD10_Code: code
        }
      },
      {
        $project: {
          PRO_Diagnosis_ICD10_Code_Order: 1,
          PRO_Diagnosis_ICD10_Code_Description: 1
        }
      }
    ])
  }

  getICDCode (searchText) {
    return this.model.find({ PRO_Diagnosis_ICD10_Code: { $regex: searchText, $options: 'i' } })
  }
}

module.exports = ICD10Model
