/* eslint-disable camelcase */
/* eslint-disable new-cap */
const AdminModel = require('./admin/admin')
const ProviderModel = require('./provider/provider')
const AuditModel = require('./audit/audit')
const ChartModel = require('./chart/chart')
const EncounterModel = require('./encounter/encounter')
const OrderModel = require('./order/order')
const OtherModel = require('./other/other')
const PatientModel = require('./patient/patient')
const PatientHistoryModel = require('./patient_history/patient_history')
const PatientProblemModel = require('./patient_problem/patient_problem')
const SearchModel = require('./search/search')
const StripeModel = require('./stripe/stripe')
const SpecialityModel = require('./speciality/speciality')
const AffiliationModel = require('./affiliation/affiliation')
const AvailabilityModel = require('./availability/availability')
const ICD10Model = require('./ICD10/ICD10')
const NotificationModel = require('./pushNotification/pushNotification')
const MessageModel = require('./message/message')
const providerAvailability = require('./provideravailability/availability')
const transactionModel = require('./transactions/transaction')
const Error_Model = require('./Error_Code/Error_Code')
const Service_Model = require('./service/service')
const New_ICD_Model = require('./new_ICD/ICD')

module.exports = {
	Admin: new AdminModel(),
	Provider: new ProviderModel(),
	Audit: new AuditModel(),
	Chart: new ChartModel(),
	Encounter: new EncounterModel(),
	Order: new OrderModel(),
	Other: new OtherModel(),
	Patient: new PatientModel(),
	PatientHistory: new PatientHistoryModel(),
	PatientProblem: new PatientProblemModel(),
	Search: new SearchModel(),
	Stripe: new StripeModel(),
	Specialty: new SpecialityModel(),
	Affiliation: new AffiliationModel(),
	Availability: new AvailabilityModel(),
	ICD10: new ICD10Model(),
	Notification: new NotificationModel(),
	Message: new MessageModel(),
	ProviderAvailability: new providerAvailability(),
	Transactions: new transactionModel(),
	Errors: new Error_Model(),
	CommonServices: new Service_Model(),
	NewICD: new New_ICD_Model()

}
