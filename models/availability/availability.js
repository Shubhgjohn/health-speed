/* eslint-disable camelcase */
const BaseModel = require('../base')
const mongoose = require('mongoose')
const Schema = mongoose.Schema
var autopopulate = require('mongoose-autopopulate')
function omitPrivate (doc, obj) {
  delete obj.Created_At
  delete obj.__v
  delete obj.id
  delete obj._id
  delete obj.AP_Password
  return obj
}
var options = { toJSON: { transform: omitPrivate } }
const AVAILABILITY_COLLECTION = 'provider_availability'

const Slot_Model = new Schema({
  Slot_Time: { type: String },
  Slot_Format: { type: Number },
  Slot_Time_Zone: String,
  Is_Home_Available: { type: Boolean, default: true },
  Is_Office_Available: { type: Boolean, default: true },
  Is_Travel_Time: { type: Boolean, default: false }
}, options)

const AVAILABILITY_SCHEMA = new Schema({
  PRO_UID: String,
  PRO_Availability_Date: String,
  PRO_Availability_Day: String,
  PRO_Availability_From_Time: String,
  PRO_Availability_From_Time_Zone: String,
  PRO_Availability_To_Time: String,
  PRO_Availability_To_Time_Zone: String,
  PRO_Availability_Type: { type: String, default: 'Office' },
  PRO_Availability_Set_By: String,
  PRO_Slots: [Slot_Model],
  Created_At: { type: Date, default: new Date() },
  Updated_At: { type: Date, default: new Date() },
  Deleted_At: { type: Date, default: null }
}, options)
AVAILABILITY_SCHEMA.plugin(autopopulate)

var MODEL = mongoose.model(AVAILABILITY_COLLECTION, AVAILABILITY_SCHEMA)

// Note: Do not extend this class, only BaseModel is allow to be extended from.
// because more than 2 levels inheritance could lead to tight-coupling design and make everything more complicated
class AvailabilityModel extends BaseModel {
  constructor () {
    super(MODEL, AVAILABILITY_COLLECTION, AVAILABILITY_SCHEMA)
  }

  getProviderAvailability (PRO_UID, date) {
    return this.model.findOne({ PRO_UID: PRO_UID, PRO_Availability_Date: date })
  }

  getProviderAllAvailability (PRO_UID) {
    return this.model.find({ PRO_UID: PRO_UID }, { PRO_Availability_Set_By: 1, PRO_Availability_Date: 1, PRO_Availability_Type: 1 })
  }

  getNextDaysAvailabilityByDayOrWeeks (from_date, to_date) {
    return this.model.aggregate([
      {
        $match: {
          $and: [{ PRO_Availability_Date: { $gte: from_date } }, { PRO_Availability_Date: { $lte: to_date } }]
        }
      },
      {
        $project: {
          _id: 0, PRO_Availability_Date: '$PRO_Availability_Date', PRO_Slots: '$PRO_Slots', PRO_UID: '$PRO_UID'
        }
      },
      {
        $sort: { PRO_Availability_Date: 1 }
      }
    ])
  }

  updateAvailability (PRO_UID, data) {
    return this.model.updateOne({ PRO_UID: PRO_UID, PRO_Availability_Date: data.PRO_Availability_Date }, data, { new: true }, (err, doc) => {
      if (err) {
        console.log('Something wrong when updating data!')
      }
    })
  }

  updateAvailabilityByTime (PRO_UID, date, data) {
    return this.model.updateOne({ PRO_UID: PRO_UID, PRO_Availability_Date: date }, { $set: { PRO_Slots: data } }, { new: true }, (err, doc) => {
      if (err) {
        console.log('Something wrong when updating data!')
      }
    })
  }

  getBetweenRange (PRO_UID, date, time, zone) {
    return this.model.aggregate([
      {
        $match: { PRO_UID: PRO_UID, PRO_Availability_Date: date }
      },
      {
        $unwind: '$PRO_Slots'
      },
      {
        $match: { 'PRO_Slots.Slot_Time': time, 'PRO_Slots.Slot_Time_Zone': zone }
      }
    ])
  }

  deleteAvailability (PRO_UID) {
    return this.model.remove({ PRO_UID: PRO_UID })
  }
  deleteDayAvailability (PRO_UID, date) {
    return this.model.remove({ PRO_UID: PRO_UID , PRO_Availability_Date: date})
  } 
  updateSlot (id, date, time) {
    return this.model.updateOne(
      { PRO_UID: id, PRO_Availability_Date: date, 'PRO_Slots.Slot_Format': time },
      {
        $set: {
          'PRO_Slots.$.Is_Office_Available': true,
          'PRO_Slots.$.Is_Home_Available': true,
          'PRO_Slots.$.Is_Travel_Time': false
        }
      }
    )
  }

  updateAvailableSlot (id, date, time) {
    return this.model.updateOne(
      { PRO_UID: id, PRO_Availability_Date: date, 'PRO_Slots.Slot_Format': time },
      {
        $set: {
          'PRO_Slots.$.Is_Office_Available': false,
          'PRO_Slots.$.Is_Home_Available': false,
          'PRO_Slots.$.Is_Travel_Time': true
        }
      }
    )
  }

  getAllAvailability (PRO_UID, sDate, eDate) {
    return this.model.aggregate([
      {
        $match: {
          PRO_UID: { $in: PRO_UID }
        }
      },
      {
        $project: {
          _id: 0,
          date: {
            $dateFromString: {
              dateString: '$PRO_Availability_Date',
              format: '%m-%d-%Y'
            }
          },
          PRO_Availability_Date: '$PRO_Availability_Date',
          PRO_Slots: '$PRO_Slots',
          PRO_UID: '$PRO_UID',
          PRO_Availability_Type: 1,
          PRO_Availability_From_Time: 1,
          PRO_Availability_From_Time_Zone: 1,
          PRO_Availability_To_Time: 1,
          PRO_Availability_To_Time_Zone: 1
        }
      },
      {
        $match: {
          $and: [{ date: { $gte: sDate } }, { date: { $lte: eDate } }]
        }
      },
      {
        $sort: { date: 1 }
      }
    ])
  }

  getProviderAvailabilityById (PRO_UID, sDate, eDate) {
    return this.model.aggregate([
      {
        $match: {
          PRO_UID: PRO_UID
        }
      },
      {
        $project: {
          _id: 0,
          date: {
            $dateFromString: {
              dateString: '$PRO_Availability_Date',
              format: '%m-%d-%Y'
            }
          },
          PRO_Availability_Type: 1,
          PRO_Availability_Date: '$PRO_Availability_Date',
          PRO_Slots: '$PRO_Slots',
          PRO_UID: '$PRO_UID'
        }
      },
      {
        $match: {
          $and: [{ date: { $gte: sDate } }, { date: { $lte: eDate } }]
        }
      },
      {
        $sort: { date: 1 }
      }
    ])
  }
}

module.exports = AvailabilityModel
