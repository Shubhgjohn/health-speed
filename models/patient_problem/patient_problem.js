/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const BaseModel = require('../base')
const mongoose = require('mongoose')
const Schema = mongoose.Schema
var autopopulate = require('mongoose-autopopulate')
function omitPrivate (doc, obj) {
  delete obj.__v
  delete obj.id
  delete obj._id
  return obj
}
var options = { toJSON: { transform: omitPrivate } }
const PATIENT_PROBLEM_COLLECTION = 'patient_problem'
const PATIENT_PROBLEM_SCHEMA = new Schema({
  PT_PRO_UID: String,
  PT_Problem_Created_By: { type: String, default: null },
  PT_Problem_Created_Timestamp: { type: String, default: null },
  PT_Problem_Last_Modified_By: { type: String, default: null },
  PT_Problem_Last_Modified_Timestamp: { type: Date, default: null },
  PT_Problem_Description: { type: String, min: 2, max: 140 },
  PT_Problem_Resolved_By: { type: String, default: null },
  PT_Problem_Resolved_Timestamp: { type: Date, default: null },
  PT_Problem_List_Order: Number,
  PT_Problem_Status: { type: String, default: 'Active' },
  PT_Username: { type: String, default: null },
  PT_HS_MRN: { type: String, default: null },
  Problem_Index: { type: Number, default: 0 },
  PRO_UID: { type: String, default: null },
  Created_At: { type: Date, default: new Date() },
  Updated_At: { type: Date, default: new Date() },
  Deleted_At: { type: Date, default: null }
}, options)
PATIENT_PROBLEM_SCHEMA.plugin(autopopulate)
var MODEL = mongoose.model(PATIENT_PROBLEM_COLLECTION, PATIENT_PROBLEM_SCHEMA)

// Note: Do not extend this class, only BaseModel is allow to be extended from.
// because more than 2 levels inheritance could lead to tight-coupling design and make everything more complicated
class PatientProblemModel extends BaseModel {
  constructor () {
    super(MODEL, PATIENT_PROBLEM_COLLECTION, PATIENT_PROBLEM_SCHEMA)
  }

  findPatientProblem (PT_Username) {
    return this.model.find({ PT_Username: PT_Username })
  }

  findPatientProblemByPT_Id (PT_HS_MRN) {
    return this.model.aggregate([
      { $match: { PT_HS_MRN: PT_HS_MRN } },
      {
        $lookup: {
          from: 'providers',
          localField: 'PRO_UID',
          foreignField: 'PRO_UID',
          as: 'provider'
        }
      },
      { $unwind: '$provider' },
      {
        $project:
         {
           PT_Problem_Created_First_Name: '$provider.PRO_First_Name',
           PT_Problem_Created_Last_Name: '$provider.PRO_Last_Name',
           PT_Problem_Created_Timestamp: 1,
           PT_Problem_Last_Modified_By: 1,
           PT_Problem_Resolved_By: 1,
           PT_Problem_Status: 1,
           PT_Username: 1,
           PRO_UID: 1,
           PT_PRO_UID: 1,
           PT_Problem_Last_Modified_Timestamp: 1,
           PT_Problem_Description: 1,
           PT_Problem_Resolved_Timestamp: 1,
           Problem_Index: 1
         }
      }
    ])
  }

  findPatientProblemByID (PT_PRO_UID) {
    return this.model.findOne({ PT_PRO_UID: PT_PRO_UID })
  }

  updatePatientProblem (PT_PRO_UID, data) {
    return this.model.updateOne({ PT_PRO_UID: PT_PRO_UID }, data, { new: true }, (err, doc) => {
      if (err) {
        console.log('Something wrong when updating data!')
      }
    })
  }

  updatePatientEmail (oldEmail, newEmail) {
    let data = {
      PT_Username: newEmail,
      Updated_At: new Date()
    }
    return this.model.updateMany({ PT_Username: oldEmail },
      { $set: data }, { multi: true })
  }

  lastInserted () {
    return this.model.find({}).sort({ Problem_Index: -1 }).limit(1)
  }
}

module.exports = PatientProblemModel
