/* eslint-disable eqeqeq */
/* eslint-disable dot-notation */
/* eslint-disable camelcase */
const BaseModel = require('../base')
const mongoose = require('mongoose')
const Schema = mongoose.Schema
var autopopulate = require('mongoose-autopopulate')
function omitPrivate (doc, obj) {
  delete obj.Created_At
  delete obj.__v
  delete obj.id
  delete obj._id
  delete obj.AP_Password
  return obj
}
var options = { toJSON: { transform: omitPrivate } }
const ADMIN_COLLECTION = 'admin'
const ADMIN_SCHEMA = new Schema({
  AP_Username: String,
  AP_First_Name: String,
  AP_Last_Name: String,
  AP_Password: { type: String, min: 10, max: 45 },
  AP_Provider_User_Search: { type: String, minlength: 1, maxlength: 140 },
  AP_Admin_User_Search: { type: String, minlength: 1, maxlength: 140 },
  AP_Patient_User_Search: { type: String, minlength: 1, maxlength: 140 },
  AP_Phone_Number: { type: String, default: null },
  AP_Audit_Log_Search: { type: String, minlength: 1, maxlength: 140 },
  AP_Audit_Providers_Search: { type: String, minlength: 1, maxlength: 140 },
  AP_Audit_Patients_Search: { type: String, minlength: 1, maxlength: 140 },
  Total_Number_of_Active_Providers: { type: Number, min: 0 },
  Total_Number_of_Active_Providers_in_Past_30_Days: { type: Number, min: 0 },
  Total_Number_of_Active_Patients: { type: Number, min: 0 },
  Total_Number_of_Active_Patients_in_Past_30_Days: { type: Number, min: 0 },
  Total_Number_of_Encounters: { type: Number, min: 0 },
  Total_Number_of_Encounters_in_Past_30_Days: { type: Number, min: 0 },
  Total_Amount_of_Transactions_Processed: { type: Number, min: 0 },
  Total_Amount_of_Transactions_Processed_in_Past_30_Days: { type: Number, min: 0 },
  PRO_Home_Office_Hours_New_Visit_Min: { type: Number, default: null },
  PRO_Home_Office_Hours_Followup_Min: { type: Number, default: null },
  PRO_Home_Off_Hours_New_Visit_Min: { type: Number, default: null },
  PRO_Home_Off_Hours_Followup_Min: { type: Number, default: null },
  PRO_Office_Office_Hours_New_Visit_Min: { type: Number, default: null },
  PRO_Office_Office_Hours_Followup_Min: { type: Number, default: null },
  PRO_Office_Off_Hours_New_Visit_Min: { type: Number, default: null },
  PRO_Office_Off_Hours_Followup_Min: { type: Number, default: null },
  Audit_Start_Date_Filter_Option: Date,
  Audit_End_Date_Filter_Option: Date,
  Audit_Type_Add_Filter_Option: Boolean,
  Audit_Type_Change_Filter_Option: Boolean,
  Audit_Type_Remove_Filter_Option: Boolean,
  Audit_Type_Login_Filter_Option: Boolean,
  Audit_Type_Logout_Filter_Option: Boolean,
  Audit_Type_Confirm_Filter_Option: Boolean,
  Audit_Type_View_Filter_Option: Boolean,
  Bulk_Availability_Span_Start_Date: { type: Date, min: new Date() + (60 * 60 * 24 * 1000), max: new Date() + (60 * 60 * 24 * 365 * 1000) },
  Bulk_Availability_Span_End_Date: { type: Date, min: new Date() + (60 * 60 * 24 * 2 * 1000), max: new Date() + (60 * 60 * 24 * 365 * 1000) },
  Bulk_Availability_Update_Trigger: { type: String, default: 'Unchecked' },
  Bulk_Availability_Type_of_Encounters: { type: String, default: 'Office' },
  AP_Status: { type: String, default: 'Active' },
  AP_LastLogin: { type: Date, default: null },
  Created_At: { type: Date, default: new Date() },
  Updated_At: { type: Date, default: new Date() },
  Deleted_At: { type: Date, default: null },
  is_Deleted: { type: Boolean, default: false }
}, options)
ADMIN_SCHEMA.plugin(autopopulate)

var MODEL = mongoose.model(ADMIN_COLLECTION, ADMIN_SCHEMA)

// Note: Do not extend this class, only BaseModel is allow to be extended from.
// because more than 2 levels inheritance could lead to tight-coupling design and make everything more complicated
class AdminModel extends BaseModel {
  constructor () {
    super(MODEL, ADMIN_COLLECTION, ADMIN_SCHEMA)
  }

  // Get admin user by Username(email) //
  findAdminByUsername (email) {
    return this.model.findOne({ AP_Username: email, is_Deleted: false })
  }

  // Update admin last login time //
  updateAgentLastLogin (email) {
    return this.model.updateOne({ AP_Username: email }, { $set: { AP_LastLogin: Date.now() } })
  }

  // Update admin password at reset time //
  updateAP_PasswordById (id, data) {
    return this.model.updateOne({ _id: id }, { $set: { AP_Password: data.AP_Password, Updated_At: data.Updated_At } })
  }

  // Get the list of admin(!logged in) //
  async getNotLoggedinAdmin (email, data) {
    var countData = {}
    var orQuery = {}
    var andQuery = {}
    orQuery['$or'] = []
    andQuery['$and'] = []
    if (data.search_text) {
      orQuery['$or'].push({ AP_Username: { $regex: data.search_text, $options: 'i' } })
      if (data.search_text.indexOf(' ') >= 0) {
        const split = data.search_text.split(' ')
        andQuery['$and'].push({ AP_First_Name: { $regex: split[0], $options: 'i' } })
        andQuery['$and'].push({ AP_Last_Name: { $regex: split[1], $options: 'i' } })
      } else {
        orQuery['$or'].push({ AP_First_Name: { $regex: data.search_text, $options: 'i' } })
        orQuery['$or'].push({ AP_Last_Name: { $regex: data.search_text, $options: 'i' } })
      }
    }
    if (andQuery['$and'].length != 0) orQuery['$or'].push(andQuery)
    var match = {}
    if (orQuery['$or'].length != 0) match = { AP_Username: { $ne: email }, is_Deleted: false, $or: orQuery['$or'] }
    else match = { AP_Username: { $ne: email }, is_Deleted: false }
    var count = await this.model.aggregate([
      {
        $match: match
      },
      {
        $count: 'AP_Username'
      }
    ])
    var admin = await this.model.aggregate([
      {
        $match: match
      },
      {
        $project: {
          _id: 0,
          AP_Id: '$_id',
          AP_First_Name: '$AP_First_Name',
          AP_Last_Name: '$AP_Last_Name',
          AP_Username: '$AP_Username',
          AP_LastLogin: '$AP_LastLogin'
        }
      },
      { $skip: data.skip ? data.skip : 0 }, { $limit: data.limit ? data.limit : 10 }])
    countData.total = count.length > 0 ? count[0].AP_Username : 0
    countData.result = admin
    return countData
  }
}

module.exports = AdminModel
