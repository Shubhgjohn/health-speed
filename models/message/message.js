/* eslint-disable camelcase */
const BaseModel = require('../base')
const mongoose = require('mongoose')
const Schema = mongoose.Schema
var autopopulate = require('mongoose-autopopulate')

const MESSAGE_COLLECTION = 'message'
const MESSAGE_SCHEMA = new Schema({
  Encounter_UID: { type: String, default: null },
  body: { type: String, default: null },
  sentBy: { type: String, default: null },
  Created_At: { type: Date },
  Updated_At: { type: Date, default: new Date() },
  Deleted_At: { type: Date, default: null }
})
MESSAGE_SCHEMA.plugin(autopopulate)

var MODEL = mongoose.model(MESSAGE_COLLECTION, MESSAGE_SCHEMA)

// Note: Do not extend this class, only BaseModel is allow to be extended from.
// because more than 2 levels inheritance could lead to tight-coupling design and make everything more complicated
class OtherModel extends BaseModel {
  constructor () {
    super(MODEL, MESSAGE_COLLECTION, MESSAGE_SCHEMA)
  }

  getEncounterMessages (Encounter_UID) {
    return this.model.find({ Encounter_UID: Encounter_UID })
  }
}

module.exports = OtherModel
