/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const BaseModel = require('../base')
const mongoose = require('mongoose')
const Schema = mongoose.Schema
var autopopulate = require('mongoose-autopopulate')
var ObjectId = require('mongodb').ObjectID
function omitPrivate (doc, obj) {
  delete obj.Created_At
  delete obj.__v
  delete obj._id
  return obj
}
var options = { toJSON: { transform: omitPrivate } }

const PATIENT_HISTORY_COLLECTION = 'patient_history'

const PT_Lab_And_Image = new Schema({
  Attachment_Name: { type: String, default: null },
  Attachment_Url: { type: String, default: null },
  Attachment_By: { type: String, default: null },
  Attachment_User: { type: String, default: null },
  Is_Deleted: { type: Boolean, default: false }
})

const PT_Medical_Directive_Attachment = new Schema({
  Attachment_Name: { type: String, default: null },
  Attachment_Url: { type: String, default: null },
  Attachment_By: { type: String, default: null },
  Attachment_User: { type: String, default: null },
  Is_Deleted: { type: Boolean, default: false }
})

const PT_Medication = new Schema({
  PT_Medication_Name: String,
  Is_Deleted: { type: Boolean, default: false }
})

const PT_Medication_Attachment = new Schema({
  Attachment_Name: { type: String, default: null },
  Attachment_Url: { type: String, default: null },
  Attachment_By: { type: String, default: null },
  Attachment_User: { type: String, default: null },
  Is_Deleted: { type: Boolean, default: false }
})

const PT_Medical_History_Attachment = new Schema({
  Attachment_Name: { type: String, default: null },
  Attachment_Url: { type: String, default: null },
  Attachment_By: { type: String, default: null },
  Attachment_User: { type: String, default: null },
  Is_Deleted: { type: Boolean, default: false }
})

const PATIENT_HISTORY_SCHEMA = new Schema({
  PT_Username: { type: String, min: 5, max: 140 },
  PT_HS_MRN: { type: String, min: 5, max: 140 },
  PT_Primary_Care_Provider_Toggle: { type: String, default: 'No' },
  PRO_UID: { type: String, default: null },
  PT_Primary_Care_Provider: { type: String, min: 2, max: 140 },
  PT_Allergies_Toggle: { type: String, default: 'No' },
  PT_Allergies: { type: [String], default: [] },
  PT_Medication_Attachment: [PT_Medication_Attachment],
  PT_Medications_Toggle: { type: String, default: 'No' },
  PT_Medication: { type: [PT_Medication], default: [] },
  PT_Past_Year_Medical_Changes: { type: String, default: 'No' },
  PT_Past_Year_Medical_Changes_Note: { type: String, min: 0, max: 500, default: null },
  PT_Heart_Disease: { type: String, default: 'No' },
  PT_Heart_Disease_Note: { type: String, min: 0, max: 500, default: null },
  PT_Hypertension: { type: String, default: 'No' },
  PT_Hypertension_Note: { type: String, min: 0, max: 500, default: null },
  PT_Diabetes: { type: String, default: 'No' },
  PT_Diabetes_Note: { type: String, min: 0, max: 500, default: null },
  PT_Cancer: { type: String, default: 'No' },
  PT_Cancer_Note: { type: String, min: 0, max: 500, default: null },
  PT_Immune_Diseases: { type: String, default: 'No' },
  PT_Immune_Diseases_Note: { type: String, min: 0, max: 500, default: null },
  PT_Surgery: { type: String, default: 'No' },
  PT_Surgery_Note: { type: String, min: 0, max: 500, default: null },
  PT_Neuro_Issues: { type: String, default: 'No' },
  PT_Neuro_Issues_Note: { type: String, min: 0, max: 500, default: null },
  PT_Other_Diseases: { type: String, default: 'No' },
  PT_Other_Diseases_Note: { type: String, min: 0, max: 500, default: null },
  PT_Family_History_Info: { type: [String], default: [] },
  PT_Lab_and_Image_Reports_Toggle: { type: String, default: 'No' },
  PT_Lab_and_Image_Reports: [PT_Lab_And_Image],
  PT_Medical_Directives_Toggle: { type: String, default: 'No' },
  PT_Medical_Directive: String,
  PT_Medical_Directive_Attachment: [PT_Medical_Directive_Attachment],
  PT_Recreational_Drugs: { type: String, default: 'No' },
  PT_Recreational_Drugs_Note: { type: String, min: 0, max: 500, default: null },
  PT_Tobacco_Usage: { type: String, default: 'No' },
  PT_Tobacco_Usage_Note: { type: String, min: 0, max: 500, default: null },
  PT_Alcohol_Usage: { type: String, default: 'No' },
  PT_Alcohol_Usage_Note: { type: String, min: 0, max: 500, default: null },
  PT_Occupation: { type: String, default: null },
  PT_Last_Menstrual_Cycle_Date: { type: String, default: null },
  Pregnancy_History: { type: String, default: 'No' },
  PT_Number_Of_Pregnancies: { type: Number, default: 0 },
  PT_Number_Of_Deliveries: { type: Number, default: 0 },
  PT_Pregnancy_Complications: { type: String, default: 'No' },
  PT_Pregnancy_Complications_Note: { type: String, min: 0, max: 500, default: null },
  Pregnant_Now: { type: String, default: 'No' },
  Expected_Delivery_Date: { type: String, default: null },
  Nursing_Now: { type: String, default: 'No' },
  Oral_Contraceptives: { type: String, default: 'No' },
  Oral_Contraceptives_Info: { type: String, min: 0, max: 500, default: null },
  PT_Primary_Care_Physician_Toggle: { type: String, default: 'No' },
  PT_Hx_About_Confirmation: { type: String, default: 'Unchecked' },
  PT_Hx_General_Confirmation: { type: String, default: 'Unchecked' },
  PT_Hx_Medications_Confirmation: { type: String, default: 'Unchecked' },
  PT_Hx_Other_Confirmation: { type: String, default: 'Unchecked' },
  PT_Hx_OB_Confirmation: { type: String, default: 'Unchecked' },
  PT_Organ_Donor: { type: String, default: 'No' },
  PT_Medical_History_Attachment: [PT_Medical_History_Attachment],
  Created_At: { type: Date, default: new Date() },
  Updated_At: { type: Date, default: new Date() },
  Deleted_At: { type: Date, default: null }
}, options)
PATIENT_HISTORY_SCHEMA.plugin(autopopulate)
var MODEL = mongoose.model(PATIENT_HISTORY_COLLECTION, PATIENT_HISTORY_SCHEMA)

// Note: Do not extend this class, only BaseModel is allow to be extended from.
// because more than 2 levels inheritance could lead to tight-coupling design and make everything more complicated
class PatientHistoryModel extends BaseModel {
  constructor () {
    super(MODEL, PATIENT_HISTORY_COLLECTION, PATIENT_HISTORY_SCHEMA)
  }

  findByUserId (id) {
    return this.model.findOne({ PT_HS_MRN: id })
  }

  findByUsername (PT_Username) {
    return this.model.findOne({ PT_Username: PT_Username })
  }

  findPatientHistoryById (PT_HS_MRN) {
    return this.model.findOne({ PT_HS_MRN: PT_HS_MRN })
  }

  findUpdatedHistory (PT_Username, key) {
    var json = { josnData: 1 }
    json[`${key}`] = 1
    delete json.josnData
    json._id = 0
    return this.model.findOne({ PT_Username: PT_Username }, json)
  }

  updateByUsername (PT_HS_MRN, data) {
    return this.model.updateOne({ PT_HS_MRN: PT_HS_MRN }, { $set: data }, { new: true }).exec()
  }

  updateById (id, data) {
    data.Updated_At = new Date()
    return this.model.updateOne({ PT_HS_MRN: id }, data, { new: true }).exec()
  }

  updatePatientEmail (oldEmail, newEmail) {
    let data = {
      PT_Username: newEmail,
      Updated_At: new Date()
    }
    return this.model.updateMany({ PT_Username: oldEmail },
      { $set: data }, { multi: true })
  }

  updatePatientAttachment (PT_Username, attachment, key) {
    var json = { key: attachment[key] }
    json[`${key}`] = attachment[key]
    delete json.key
    return this.model.updateOne(
      { PT_Username: PT_Username },
      { $push: json },
      { $set: { Updated_At: new Date() } }
    )
  }

  getPatientMedicalHistory (PT_HS_MRN) {
    return this.model.aggregate([
      {
        $match: { PT_HS_MRN: PT_HS_MRN }
      },
      {
        $project: {
          PT_Heart_Disease: '$PT_Heart_Disease',
          PT_Hypertension: '$PT_Hypertension',
          PT_Diabetes: '$PT_Diabetes',
          PT_Cancer: '$PT_Cancer',
          PT_Immune_Diseases: '$PT_Immune_Diseases',
          PT_Surgery: '$PT_Surgery',
          PT_Neuro_Issues: '$PT_Neuro_Issues',
          PT_Other_Diseases: '$PT_Other_Diseases'
        }
      }
    ])
  }

  updatePatientHistory (PT_Username, data) {
    return this.model.findOneAndUpdate({ PT_Username: PT_Username }, data, { new: true }, (err, doc) => {
      if (err) {
        console.log('Something wrong when updating data!')
      }
    })
  }

  getPatientSocialHistory (PT_HS_MRN) {
    return this.model.aggregate([
      {
        $match: { PT_HS_MRN: PT_HS_MRN }
      },
      {
        $project: {
          PT_Tobacco_Usage: '$PT_Tobacco_Usage',
          PT_Recreational_Drugs: '$PT_Recreational_Drugs',
          PT_Alcohol_Usage: '$PT_Alcohol_Usage'
        }
      }
    ])
  }

  findMedicationAttachment (id) {
    return this.model.aggregate([
      { $unwind: '$PT_Medication_Attachment' },
      { $match: { 'PT_Medication_Attachment._id': ObjectId(id) } },
      { $project: { PT_Medication_Attachment: 1 } }
    ])
  }

  findMedicalHistoryAttchment (id) {
    return this.model.aggregate([
      { $unwind: '$PT_Medical_History_Attachment' },
      { $match: { 'PT_Medical_History_Attachment._id': ObjectId(id) } },
      { $project: { PT_Medical_History_Attachment: 1 } }
    ])
  }

  findMedicalDirectiveAttachment (id) {
    return this.model.aggregate([
      { $unwind: '$PT_Medical_Directive_Attachment' },
      { $match: { 'PT_Medical_Directive_Attachment._id': ObjectId(id) } },
      { $project: { PT_Medical_Directive_Attachment: 1 } }

    ])
  }

  findLabAndImageAttachment (id) {
    return this.model.aggregate([
      { $unwind: '$PT_Lab_and_Image_Reports' },
      { $match: { 'PT_Lab_and_Image_Reports._id': ObjectId(id) } },
      { $project: { PT_Lab_and_Image_Reports: 1 } }

    ])
  }

  deleteMedicationAttachment (id) {
    return this.model.updateOne(
      { 'PT_Medication_Attachment._id': ObjectId(id) },
      {
        $pull: {
          PT_Medication_Attachment: { _id: ObjectId(id) }
        }
      }
    )
  }

  deleteMedicalHistoryAttachment (id) {
    return this.model.updateOne(
      { 'PT_Medical_History_Attachment._id': ObjectId(id) },
      {
        $pull: {
          PT_Medical_History_Attachment: { _id: ObjectId(id) }
        }
      }
    )
  }

  deleteMedicalDirectiveAttachment (id) {
    return this.model.updateOne(
      { 'PT_Medical_Directive_Attachment._id': ObjectId(id) },
      {
        $pull: {
          PT_Medical_Directive_Attachment: { _id: ObjectId(id) }
        }
      }
    )
  }

  deleteLabAndImageAttachment (id) {
    return this.model.updateOne(
      { 'PT_Lab_and_Image_Reports._id': ObjectId(id) },
      {
        $pull: {
          PT_Lab_and_Image_Reports: { _id: ObjectId(id) }
        }
      }
    )
  }

  addMedication (PT_HS_MRN, data) {
    return this.model.updateOne(
      { PT_HS_MRN: PT_HS_MRN },
      { $push: { PT_Medication: data } },
      { multi: true }
    )
  }

  updateMedicationById (medicationId, data) {
    return this.model.updateOne(
      { 'PT_Medication._id': ObjectId(medicationId) },
      { $set: { 'PT_Medication.$.PT_Medication_Name': data.PT_Medication_Name, Updated_At: new Date() } }
    )
  }

  deleteMedicationById (medicationId) {
    return this.model.updateOne(
      { 'PT_Medication._id': ObjectId(medicationId) },
      { $pull: { PT_Medication: { _id: ObjectId(medicationId) } } },
      { $set: { Updated_At: new Date() } }
    )
  }

  findMedicationById (medicationId) {
    return this.model.findOne({ 'PT_Medication._id': ObjectId(medicationId) })
  }

  uploadMedicationAttachment (PT_HS_MRN, data) {
    return this.model.findOneAndUpdate(
      { PT_HS_MRN: PT_HS_MRN },
      { $push: { PT_Medication_Attachment: data } },
      { $set: { Updated_At: new Date() } }
    )
  }

  uploadMedicalDirectiveAttachment (PT_HS_MRN, data) {
    return this.model.findOneAndUpdate(
      { PT_HS_MRN: PT_HS_MRN },
      { $push: { PT_Medical_Directive_Attachment: data } },
      { $set: { Updated_At: new Date() } }
    )
  }

  uploadMedicalHistoryAttachment (PT_HS_MRN, data) {
    return this.model.findOneAndUpdate(
      { PT_HS_MRN: PT_HS_MRN },
      { $push: { PT_Medical_History_Attachment: data } },
      { $set: { Updated_At: new Date() } }
    )
  }

  uploadLabAndImageReportAttachment (PT_HS_MRN, data) {
    return this.model.findOneAndUpdate(
      { PT_HS_MRN: PT_HS_MRN },
      { $push: { PT_Lab_and_Image_Reports: data } },
      { $set: { Updated_At: new Date() } }
    )
  }
}

module.exports = PatientHistoryModel
