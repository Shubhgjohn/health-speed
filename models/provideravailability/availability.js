/* eslint-disable camelcase */
const BaseModel = require('../base')
const mongoose = require('mongoose')
const Schema = mongoose.Schema
var autopopulate = require('mongoose-autopopulate')
function omitPrivate (doc, obj) {
  delete obj.Created_At
  delete obj.__v
  delete obj.id
  delete obj._id
  delete obj.AP_Password
  return obj
}
var options = { toJSON: { transform: omitPrivate } }
const AVAILABILITY_COLLECTION = 'availability'

const Slot_Array = new Schema({
  Slot_Time: { type: String, default: null },
  Slot_Format: { type: Number, default: 0 },
  Slot_Time_Zone: { type: String, default: null },
  Is_Home_Availability: { type: Boolean, default: false },
  Is_Office_Availability: { type: Boolean, default: false },
  Is_Home_Available: { type: Boolean, default: true },
  Is_Office_Available: { type: Boolean, default: true }
}, options)

const AVAILABILITY_SCHEMA = new Schema({
  PRO_UID: { type: String, default: null },
  PRO_Availability_Date: { type: String, default: null },
  PRO_Availability_Day: { type: String, default: null },
  PRO_Availability_Set_By: { type: String, default: null },
  PRO_Slots: [Slot_Array],
  Created_At: { type: Date, default: new Date() },
  Updated_At: { type: Date, default: new Date() },
  Deleted_At: { type: Date, default: null }
}, options)
AVAILABILITY_SCHEMA.plugin(autopopulate)

var MODEL = mongoose.model(AVAILABILITY_COLLECTION, AVAILABILITY_SCHEMA)

// Note: Do not extend this class, only BaseModel is allow to be extended from.
// because more than 2 levels inheritance could lead to tight-coupling design and make everything more complicated
class AvailabilityModel extends BaseModel {
  constructor () {
    super(MODEL, AVAILABILITY_COLLECTION, AVAILABILITY_SCHEMA)
  }

  getProviderAvailability (PRO_UID, date) {
    return this.model.findOne({ PRO_UID: PRO_UID, PRO_Availability_Date: date })
  }

  getProviderAllAvailability (PRO_UID) {
    return this.model.find({ PRO_UID: PRO_UID }, { PRO_Availability_Set_By: 1, PRO_Availability_Date: 1, PRO_Availability_Type: 1 })
  }

  updateAvailability (PRO_UID, date, slot) {
    return this.model.updateOne({ PRO_UID: PRO_UID, PRO_Availability_Date: date }, { $set: { PRO_Slots: slot } })
  }

  deleteAvailability (PRO_UID) {
    return this.model.remove({ PRO_UID: PRO_UID })
  }

  deleteDayAvailability (PRO_UID, date) {
    return this.model.remove({ PRO_UID: PRO_UID , PRO_Availability_Date: date})
  } 

  getAllAvailability (PRO_UID, sDate, eDate) {
    return this.model.aggregate([
      {
        $match: {
          PRO_UID: { $in: PRO_UID }
        }
      },
      {
        $project: {
          _id: 0,
          date: {
            $dateFromString: {
              dateString: '$PRO_Availability_Date',
              format: '%m-%d-%Y'
            }
          },
          PRO_Availability_Date: '$PRO_Availability_Date',
          PRO_Slots: '$PRO_Slots',
          PRO_UID: '$PRO_UID'
        }
      },
      {
        $match: {
          $and: [{ date: { $gte: sDate } }, { date: { $lte: eDate } }]
        }
      },
      {
        $sort: { date: 1 }
      }
    ])
  }
}

module.exports = AvailabilityModel
