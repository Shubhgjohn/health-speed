/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
/* eslint-disable no-useless-escape */
/* eslint-disable dot-notation */
const BaseModel = require('../base')
const mongoose = require('mongoose')
const Schema = mongoose.Schema
var autopopulate = require('mongoose-autopopulate')
function omitPrivate (doc, obj) {
	delete obj.Created_At
	delete obj.__v
	delete obj.id
	delete obj._id
	return obj
}
var options = { toJSON: { transform: omitPrivate } }
const AUDIT_COLLECTION = 'audit'
const AUDIT_SCHEMA = new Schema({
	Audit_UID: String,
	Audit_Type: String,
	Audit_Info: String,
	PRO_First_Name: { type: String, default: null },
	PRO_Last_Name: { type: String, default: null },
	PRO_UID: { type: String, default: null },
	PT_First_Name: { type: String, default: null },
	PT_Last_Name: { type: String, default: null },
	PT_HSID: { type: String, default: null },
	Data_Point: { type: String, default: null },
	Old_Value: { type: Object, default: null },
	New_Value: { type: Object, default: null },
	IP_Address: String,
	Field_Name: String,
	Page_Name: { type: Object, default: null },
	Audit_Timestamp: { type: Date, default: new Date() },
	Created_At: { type: Date, default: new Date() },
	Updated_At: { type: Date, default: null },
	Deleted_At: { type: Date, default: null }
}, options)
AUDIT_SCHEMA.plugin(autopopulate)

var MODEL = mongoose.model(AUDIT_COLLECTION, AUDIT_SCHEMA)

// Note: Do not extend this class, only BaseModel is allow to be extended from.
// because more than 2 levels inheritance could lead to tight-coupling design or make everything more complicated
class AuditModel extends BaseModel {
	constructor () {
		super(MODEL, AUDIT_COLLECTION, AUDIT_SCHEMA)
	}

	// Get the list of audits with all filters and search funcationality //
	async getAuditLogs (data) {
		var andQuery = {}
		var orQuery = {}
		var patient = {}
		var provider = {}
		var orFilter = {}
		orFilter['$or'] = []
		orQuery['$or'] = []
		andQuery['$and'] = []
		patient['$and'] = []
		provider['$and'] = []
		var countData = {}
		if (data.start_date && data.end_date) {
			andQuery['$and'].push({ Audit_Timestamp: { $gte: new Date(data.start_date), $lte: new Date(data.end_date) } })
		}
		if (data.provider) {
			orFilter['$or'].push({ PRO_UID: data.provider })
		}
		if (data.patient) {
			orFilter['$or'].push({ PT_HSID: data.patient })
		}
		if (data.audit_type.length > 0) {
			andQuery['$and'].push({ Audit_Type: { $in: data.audit_type } })
		}
		if (orFilter['$or'].length != 0)andQuery['$and'].push(orFilter)
		if (data.search_text) {
			if (data.search_text.indexOf(' ') >= 0) {
				const split = data.search_text.split(' ')
				provider['$and'].push({ PRO_First_Name: { $regex: split[0], $options: 'i' } })
				provider['$and'].push({ PRO_Last_Name: { $regex: split[1], $options: 'i' } })
				patient['$and'].push({ PT_First_Name: { $regex: split[0], $options: 'i' } })
				patient['$and'].push({ PT_Last_Name: { $regex: split[1], $options: 'i' } })
			} else andQuery['$and'].push({ $text: { $search: `\"${data.search_text}\"` } })
		}
		if (patient['$and'].length != 0) orQuery['$or'].push(patient)
		var match = {}
		if (provider['$and'].length != 0)orQuery['$or'].push(provider)
		if (orQuery['$or'].length != 0) andQuery['$and'].push(orQuery)
		if (andQuery['$and'].length != 0) match = andQuery
		var count = await this.model.aggregate([
			{
				$match: match
			},
			{
				$count: 'Audit_UID'
			}
		])
		var audit = await this.model.aggregate([
			{
				$match: match
			},
			{
				$project: {
					_id: 0,
					Timestamp: '$Audit_Timestamp',
					Auditinfo: '$Audit_Info',
					PRO_First_Name: '$PRO_First_Name',
					PRO_Last_Name: '$PRO_Last_Name',
					PRO_UID: '$PRO_UID',
					Audit_Type: '$Audit_Type',
					Audit_UID: '$Audit_UID',
					PT_First_Name: '$PT_First_Name',
					PT_Last_Name: '$PT_Last_Name',
					PT_HSID: '$PT_HSID',
					Datapoint: '$Data_Point',
					Oldvalue: '$Old_Value',
					Newvalue: '$New_Value',
					IPaddress: '$IP_Address',
					Field_Name: 1
				}
			},
			{
				$sort: data.column == 'signup' ? { Timestamp: data.order == 'desc' ? -1 : 1 } : data.column == 'audit_info' ? { Audit_UID: data.order == 'desc' ? -1 : 1 } : data.column == 'provider' ? { PRO_First_Name: data.order == 'desc' ? -1 : 1 } : data.column == 'patient' ? { PT_First_Name: data.order == 'desc' ? -1 : 1 } : data.column == 'data_point' ? { Datapoint: data.order == 'desc' ? -1 : 1 } : data.column == 'ip_address' ? { IPaddress: data.order == 'desc' ? -1 : 1 } : { Timestamp: data.order == 'asc' ? 1 : -1 }
			},
			{ $skip: data.skip != 0 ? data.skip : 0 }, { $limit: data.limit != 0 ? data.limit : 10 }
		])
		countData.total = count.length > 0 ? count[0].Audit_UID : 0
		countData.result = audit
		return countData
	}

	// Get the audit by id //
	getAuditById (id) {
		return this.model.find({ $or: [{ 'New_Value.AP_Username': id }, { 'New_Value._id': id }] })
	}

	// Get the all audits for export csv file //
	getExportCSV (data) {
		var andQuery = {}
		var orQuery = {}
		var patient = {}
		var provider = {}
		var orFilter = {}
		orFilter['$or'] = []
		orQuery['$or'] = []
		andQuery['$and'] = []
		patient['$and'] = []
		provider['$and'] = []
		if (data.start_date && data.end_date) {
			andQuery['$and'].push({ Audit_Timestamp: { $gte: new Date(data.start_date), $lte: new Date(data.end_date) } })
		}
		if (data.provider) {
			orFilter['$or'].push({ PRO_UID: data.provider })
		}
		if (data.patient) {
			orFilter['$or'].push({ PT_HSID: data.patient })
		}
		if (data.audit_type.length > 0) {
			andQuery['$and'].push({ Audit_Type: { $in: data.audit_type } })
		}
		if (orFilter['$or'].length != 0)andQuery['$and'].push(orFilter)
		if (data.search_text) {
			if (data.search_text.indexOf(' ') >= 0) {
				const split = data.search_text.split(' ')
				provider['$and'].push({ PRO_First_Name: { $regex: split[0], $options: 'i' } })
				provider['$and'].push({ PRO_Last_Name: { $regex: split[1], $options: 'i' } })
				patient['$and'].push({ PT_First_Name: { $regex: split[0], $options: 'i' } })
				patient['$and'].push({ PT_Last_Name: { $regex: split[1], $options: 'i' } })
			} else andQuery['$and'].push({ $text: { $search: `\"${data.search_text}\"` } })
		}
		if (patient['$and'].length != 0) orQuery['$or'].push(patient)
		var match = {}
		if (provider['$and'].length != 0)orQuery['$or'].push(provider)
		if (orQuery['$or'].length != 0) andQuery['$and'].push(orQuery)
		if (andQuery['$and'].length != 0) match = andQuery
		return this.model.aggregate([
			{
				$match: match
			},
			{
				$project: {
					_id: 0,
					Timestamp: '$Audit_Timestamp',
					Auditinfo: '$Audit_Info',
					PRO_First_Name: '$PRO_First_Name',
					PRO_Last_Name: '$PRO_Last_Name',
					PRO_UID: '$PRO_UID',
					Audit_Type: '$Audit_Type',
					Audit_UID: '$Audit_UID',
					PT_First_Name: '$PT_First_Name',
					PT_Last_Name: '$PT_Last_Name',
					PT_HSID: '$PT_HSID',
					Datapoint: '$Data_Point',
					Oldvalue: '$Old_Value',
					Newvalue: '$New_Value',
					IPaddress: '$IP_Address'
				}
			}
		])
	}

	getAuditByAuditId (Audit_UID) {
		return this.model.find({ Audit_UID: Audit_UID })
	}

	getAuditBetweenTime (fromTime, toTime) {
		return this.model.aggregate([
			{
				$match: {
					$and: [{ Audit_Timestamp: { $gte: fromTime } }, { Audit_Timestamp: { $lte: toTime } }, { Data_Point: 'PRO/Update Provider' }]
				}
			}
		])
	}
}

module.exports = AuditModel
