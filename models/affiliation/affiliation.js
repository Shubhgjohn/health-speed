const BaseModel = require('../base')
const mongoose = require('mongoose')
const Schema = mongoose.Schema
var autopopulate = require('mongoose-autopopulate')

function omitPrivate (doc, obj) {
  delete obj.Created_At
  delete obj.__v
  delete obj.id
  delete obj.Deleted_At
  delete obj.Updated_At
  return obj
}
var options = { toJSON: { transform: omitPrivate } }
const AFFILIATION_COLLECTION = 'affiliation'
const AFFILIATION_SCHEMA = new Schema(
  {
    PRO_Affiliations: { type: String },
    Is_Deleted: { type: Boolean, default: false },
    Created_At: { type: Date, default: new Date() },
    Updated_At: { type: Date, default: new Date() },
    Deleted_At: { type: Date, default: null }
  },
  options
)
AFFILIATION_SCHEMA.plugin(autopopulate)
var MODEL = mongoose.model(AFFILIATION_COLLECTION, AFFILIATION_SCHEMA)

// Note: Do not extend this class, only BaseModel is allow to be extended from.
// because more than 2 levels inheritance could lead to tight-coupling design and make everything more complicated
class AffiliationModel extends BaseModel {
  constructor () {
    super(MODEL, AFFILIATION_COLLECTION, AFFILIATION_SCHEMA)
  }
}
module.exports = AffiliationModel
