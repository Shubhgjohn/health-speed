/* eslint-disable no-unneeded-ternary */
/* eslint-disable prefer-const */
/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
var aws = require('aws-sdk')
aws.config.update({
  secretAccessKey: process.env.AWS_SECRET_KEY,
  accessKeyId: process.env.AWS_ACCESS_KEY,
  region: 'us-east-1'
})

var s3 = new aws.S3()

class Service_Model {
  async getImage (imageKey) {
    if (imageKey == null || imageKey == '') {
      return null
    } else {
      if (imageKey.includes('pdf')) {
        const url = s3.getSignedUrl('getObject', {
          Bucket: process.env.AWS_BUCKET,
          Key: imageKey,
          ResponseContentType: 'application/pdf',
          Expires: 60 * 60 * 24 * 15 // time in seconds: e.g. 60 * 5 = 5 mins
        })
        return url
      } else {
        const url = s3.getSignedUrl('getObject', {
          Bucket: process.env.AWS_BUCKET,
          Key: imageKey,
          Expires: 60 * 60 * 24 * 15 // time in seconds: e.g. 60 * 5 = 5 mins
        })
        return url
      }
    }
  }

  async getImageUrls (arr, user) {
    let newArray = []; let count = 0
    for (var i = 0; i < arr.length; i++) {
      let url = arr[i].Attachment_Name != null || arr[i].Attachment_Name != '' ? await getImageUrl(arr[i].Attachment_Name) : null
      newArray.push({
        Attachment_Url: url,
        Is_Deleted: arr[i].Is_Deleted,
        _id: arr[i]._id,
        Attachment_Name: arr[i].Attachment_Name,
        Attachment_By: arr[i].Attachment_By,
        Attachment_User: arr[i].Attachment_User,
        Delete_Attachment: (arr[i].Attachment_User == user.PRO_UID || arr[i].Attachment_By == 'Patient') ? true : false
      })
      count = count + 1
      if (count == arr.length) {
        return newArray
      }
    }
  }

  async getFileUrl (arr) {
    let newArray = []; let count = 0
    for (var i = 0; i < arr.length; i++) {
      let url = arr[i].Attachment_Name != null || arr[i].Attachment_Name != '' ? arr[i].Attachment_Name != undefined ? await getImageUrl(arr[i].Attachment_Name) : null : null
      newArray.push({
        Attachment_Url: url,
        Is_Deleted: arr[i].Is_Deleted,
        _id: arr[i]._id,
        Attachment_Name: arr[i].Attachment_Name,
        Attachment_By: arr[i].Attachment_By,
        Attachment_User: arr[i].Attachment_User
      })
      count = count + 1
      if (count == arr.length) {
        return newArray
      }
    }
  }

  async getChanges (Encounter_History, patientHistory, PRO_UID) {
    let patientEncounterHistory = Encounter_History
    var jsonObject1 = patientEncounterHistory
    var jsonObject2 = patientHistory
    let differanceArray = {}

    delete jsonObject1._id
    delete jsonObject2._id
    delete jsonObject1.Created_At
    delete jsonObject2.Created_At
    delete jsonObject1.Updated_At
    delete jsonObject2.Updated_At
    delete jsonObject1.__v
    delete jsonObject2.__v
    var keys = Object.keys(jsonObject1)
    let count = 0
    for (var i = 0; i < keys.length; i++) {
      var key = keys[i]
      if (key != 'PT_HX_Last_Updated_About_Section' &&
          key != 'PT_General_Update_Toggle' &&
          key != 'PT_HX_Last_Updated_General_Section' &&
          key != 'PT_Medicaitons_Update_Toggle' &&
          key != 'PT_HX_Last_Updated_Medications_Section' &&
          key != 'PT_Other_Update_Toggle' &&
          key != 'PT_HX_Last_Updated_Other_Section' &&
          key != 'PT_OBGYN_Update_Toggle' &&
          key != 'PT_HX_Last_Updated_OB_Section' &&
        key != 'PT_HX_Last_Updated_OB_Section' && key != 'PT_Cell_Phone' && key != 'PT_About_Update_Toggle' && key != 'PRO_UID' && key != 'PT_Username' && key != 'PT_Organ_Donor' && key != 'PT_First_Name' && key != 'PT_Profile_Picture' && !key.includes('Confirmation')) {
        if (jsonObject1[key] != null && typeof (jsonObject1[key]) == 'object') {
          differanceArray[key] = {
            oldValue: jsonObject1[key] == null || jsonObject1[key] == undefined ? jsonObject1[key] : typeof (jsonObject1[key]) == 'number' ? `${jsonObject1[key]}` : typeof (jsonObject1[key]) == 'object' ? await ImageUrls(jsonObject1[key], PRO_UID) : jsonObject1[key],
            newValue: jsonObject2[key] == null || jsonObject2[key] == undefined ? jsonObject2[key] : typeof (jsonObject2[key]) == 'number' ? `${jsonObject2[key]}` : typeof (jsonObject2[key]) == 'object' ? await ImageUrls(jsonObject2[key], PRO_UID) : jsonObject2[key]
          }
          if (jsonObject1[key] != null && jsonObject1[key] != undefined && jsonObject1[key] && typeof (jsonObject1[key]) == 'object') {
            count = jsonObject1[key].length != jsonObject2[key].length ? count + 1 : count
          }
        } else {
          if (jsonObject1[key] != jsonObject2[key]) {
            differanceArray[key] = {
              oldValue: jsonObject1[key] == null || jsonObject1[key] == undefined ? jsonObject1[key] : typeof (jsonObject1[key]) == 'number' ? `${jsonObject1[key]}` : typeof (jsonObject1[key]) == 'object' ? await ImageUrls(jsonObject1[key], PRO_UID) : jsonObject1[key],
              newValue: jsonObject2[key] == null || jsonObject2[key] == undefined ? jsonObject2[key] : typeof (jsonObject2[key]) == 'number' ? `${jsonObject2[key]}` : typeof (jsonObject2[key]) == 'object' ? await ImageUrls(jsonObject2[key], PRO_UID) : jsonObject2[key]
            }
            if (differanceArray[key].newValue) {
              count = count + 1
            }
          } else {
            differanceArray[key] = {
              oldValue: jsonObject1[key] == null || jsonObject1[key] == undefined ? jsonObject1[key] : typeof (jsonObject1[key]) == 'number' ? `${jsonObject1[key]}` : typeof (jsonObject1[key]) == 'object' ? await ImageUrls(jsonObject1[key], PRO_UID) : jsonObject1[key],
              newValue: jsonObject1[key] == null || jsonObject1[key] == undefined ? jsonObject1[key] : typeof (jsonObject1[key]) == 'number' ? `${jsonObject1[key]}` : typeof (jsonObject1[key]) == 'object' ? await ImageUrls(jsonObject1[key], PRO_UID) : jsonObject1[key]
            }
          }
        }
      }
      if (i == keys.length - 1) {
        let differance = count != 0 ? true : false
        return differance
      }
    }
  }
}

let getImageUrl = async (imageKey) => {
  if (imageKey == null || imageKey == '') {
    return null
  } else {
    if (imageKey.includes('pdf')) {
      const url = s3.getSignedUrl('getObject', {
        Bucket: process.env.AWS_BUCKET,
        Key: imageKey,
        ResponseContentType: 'application/pdf',
        Expires: 60 * 60 * 24 * 15 // time in seconds: e.g. 60 * 5 = 5 mins
      })
      return url
    } else {
      const url = s3.getSignedUrl('getObject', {
        Bucket: process.env.AWS_BUCKET,
        Key: imageKey,
        Expires: 60 * 60 * 24 * 15 // time in seconds: e.g. 60 * 5 = 5 mins
      })
      return url
    }
  }
}

async function ImageUrls (arr, PRO_UID) {
  let newArray = []; let count = 0
  for (var i = 0; i < arr.length; i++) {
    let url = arr[i].Attachment_Name != null || arr[i].Attachment_Name != '' ? await getImageUrl(arr[i].Attachment_Name) : null
    newArray.push({
      Attachment_Url: url,
      Is_Deleted: arr[i].Is_Deleted,
      _id: arr[i]._id,
      Attachment_Name: arr[i].Attachment_Name,
      Attachment_By: arr[i].Attachment_By,
      Attachment_User: arr[i].Attachment_User,
      Delete_Attachment: (arr[i].Attachment_User == PRO_UID || arr[i].Attachment_By == 'Patient') ? true : false
    })
    count = count + 1
    if (count == arr.length) {
      return newArray
    }
  }
}

module.exports = Service_Model
