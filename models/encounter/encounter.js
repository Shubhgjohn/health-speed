/* eslint-disable no-mixed-spaces-and-tabs */
/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
const BaseModel = require('../base')
const mongoose = require('mongoose')
const Schema = mongoose.Schema
var autopopulate = require('mongoose-autopopulate')
var ObjectId = require('mongodb').ObjectID
function omitPrivate (doc, obj) {
	delete obj.Created_At
	delete obj.__v
	delete obj.id
	delete obj._id
	return obj
}
var options = { toJSON: { transform: omitPrivate } }
const ENCOUNTER_COLLECTION = 'encounter'
const Encounter_Access = new Schema({
	PRO_UID: { type: String, default: null },
	Access_Status: { type: String, default: null }
})

const ENCOUNTER_SCHEMA = new Schema({
	Encounter_Location_Type: { type: String, default: null },
	Encounter_Type: { type: String, default: null },
	Encounter_Time: { type: String, default: null },
	Encounter_Date: { type: String, default: null },
	Encounter_Chief_Complaint_PT: { type: String, maxlength: 140, default: null },
	Encounter_PT_Chief_Complaint_More: { type: String, maxlength: 140, default: null },
	Encounter_Chief_Complaint_PRO: { type: String, maxlength: 140, default: null },
	Encounter_Provider: { type: String, default: null },
	Encounter_Cost: { type: Number, default: null },
	Encounter_Transaction_Status: { type: String, default: 'Unpaid' },
	Encounter_Start_Time: {
		type: String,
		default: null,
		validate: {
			validator: function (v) {
				return !v.match(/[a-z]/i)
			},
			message: props => `${props.value} is not a valid String!`
		}
	},
	Encounter_Start_Time_Zone: { type: String, default: null },
	Encounter_End_Time: {
		type: String,
		default: null,
		validate: {
			validator: function (v) {
				return !v.match(/[a-z]/i)
			},
			message: props => `${props.value} is not a valid String!`
		}
	},
	Encounter_End_Time_Zone: { type: String, default: null },
	Exam_Start_Time: { type: String, default: null },
	Exam_End_Time: { type: String, default: null },
	Encounter_Status: { type: String, default: null },
	Payment_Method: { type: String, default: null },
	Encounter_Followup_Status: { type: String, default: null },
	Followup_Decline_Acknowledged: { type: Boolean, default: false },
	Encounter_Has_Followup: { type: Boolean, default: false },
	Parent_Encounter: { type: String, default: null },
	Encounter_Reschedule_Status: { type: String, default: null },
	Primary_DX: { type: String, default: null },
	Encounter_Access: { type: Boolean, default: false },
	Encounter_Access_Provider: [Encounter_Access],
	Encounter_UID: { type: String, default: null },
	Encounter_Followup_Limit: { type: Number, default: 7 },
	Cancellation_Reason: { type: String, default: null },
	Encounter_Close_Date: { type: Date, default: null },
	Encounter_Cancelled_By: { type: String, default: null },
	Encounter_Cancelled_User: { type: String, default: null },
	Cancel_and_Reschedule_Encounter_Now: { type: String, default: 'Unchecked' },
	Reschedule_Request_Date: { type: String, default: null },
	Reschedule_Request_Time: { type: String, default: null },
	Get_Ready_Info_Status_About: { type: String, default: 'Active' },
	Get_Ready_Info_Status_General: { type: String, default: 'Active' },
	Get_Ready_Info_Status_Medications: { type: String, default: 'Active' },
	Get_Ready_Info_Status_Other: { type: String, default: 'Active' },
	Get_Ready_Info_Status_OBGYN: { type: String, default: 'Active' },
	PRO_UID: { type: String, default: null },
	PT_Username: { type: String, minlength: 5, maxlength: 140 },
	PT_HS_MRN: { type: String, default: null },
	First_Notification: { type: Boolean, default: false },
	Second_Notification: { type: Boolean, default: false },
	PMH_Notification: { type: Boolean, default: false },
	Payment_Card_Id: { type: String, default: null },
	Transaction_Status: { type: String, default: 'Unpaid' },
	Followup_Created_By: { type: String, default: null },
	Reschedule_Requested_By: { type: String, default: null },
	Reschedule_Requested_Time: { type: Date, default: null },
	Followup_Requested_Time: { type: Date, default: null },
	PT_History: { type: Object, default: null },
	PRO_Viewed_Time: { type: Date, default: new Date() },
	PT_Viewed_Time: { type: Date, default: new Date() },
	Encounter_Viewed_By: [],
	Encounter_Travel_Time: { type: String, default: null },
	Encounter_Payment_Failure_Mail: { type: Boolean, default: false },
	Created_At: { type: Date, default: new Date() },
	Updated_At: { type: Date, default: new Date() },
	Deleted_At: { type: Date, default: null },
	Is_Deleted: { type: Boolean, default: false }
}, options)
ENCOUNTER_SCHEMA.plugin(autopopulate)

var MODEL = mongoose.model(ENCOUNTER_COLLECTION, ENCOUNTER_SCHEMA)

// Note: Do not extend this class, only BaseModel is allow to be extended from.
// because more than 2 levels inheritance could lead to tight-coupling design and make everything more complicated
class EncounterModel extends BaseModel {
	constructor () {
		super(MODEL, ENCOUNTER_COLLECTION, ENCOUNTER_SCHEMA)
	}

	getProviderEncounters (PRO_UID, date) {
		return this.model.aggregate([
			{ $match: { PRO_UID: PRO_UID, Encounter_Date: date, Encounter_Status: { $nin: ['Cancelled', 'Abandoned'] } } },
			{
				$lookup:
        {
        	from: 'patients',
        	localField: 'PT_HS_MRN',
        	foreignField: 'PT_HS_MRN',
        	as: 'patient'
        }
			}
		])
	}

	getEncounterForDate (PRO_UID, date) {
		return this.model.findOne({ PRO_UID: PRO_UID, Encounter_Date: date })
	}

	getEncounterForDateAndTime (PRO_UID, date) {
		return this.model.aggregate([
			{
				$match: {
					PRO_UID: PRO_UID, Encounter_Date: date
				}
			},
			{
				$project: {
					_id: 0, Encounter_Date: '$Encounter_Date', Encounter_Time: '$Encounter_Time', Encounter_End_Time: '$Encounter_End_Time', Encounter_End_Time_Zone: '$Encounter_End_Time_Zone', Encounter_Location_Type: '$Encounter_Location_Type'
				}
			}
		])
	}

	getEncounterPatients (PRO_UID) {
		return this.model.aggregate([
			{ $match: { PRO_UID: PRO_UID, Encounter_Status: { $nin: ['Cancelled', 'Abandoned'] } } },
			{
				$lookup:
        {
        	from: 'patients',
        	localField: 'PT_HS_MRN',
        	foreignField: 'PT_HS_MRN',
        	as: 'patient'
        }
			},
			{ $unwind: '$patient' }
		])
	}

	getProviderTodaysEncounter (PRO_UID, date) {
		return this.model.aggregate([
			{ $match: { PRO_UID: PRO_UID, Encounter_Date: date } },
			{
				$lookup:
        {
        	from: 'patients',
        	localField: 'PT_HS_MRN',
        	foreignField: 'PT_HS_MRN',
        	as: 'patient'
        }
			}
		])
	}

	getParentEncounter (Encounter_UID) {
		return this.model.find({ Parent_Encounter: Encounter_UID })
	}

	findEncounter (id) {
		return this.model.aggregate([
			{ $match: { Encounter_UID: id } },
			{
				$lookup:
        {
        	from: 'patients',
        	localField: 'PT_HS_MRN',
        	foreignField: 'PT_HS_MRN',
        	as: 'patient'
        }
			},
			{ $unwind: '$patient' },
			{
				$lookup:
        {
        	from: 'providers',
        	localField: 'PRO_UID',
        	foreignField: 'PRO_UID',
        	as: 'provider'
        }
			},
			{ $unwind: '$provider' }
		])
	}

	getPatientProviderEncounter (PRO_UID, PT_HS_MRN) {
		return this.model.aggregate([
			{ $match: { PRO_UID: PRO_UID, PT_HS_MRN: PT_HS_MRN } },
			{
				$lookup:
        {
        	from: 'patients',
        	localField: 'PT_HS_MRN',
        	foreignField: 'PT_HS_MRN',
        	as: 'patient'
        }
			},
			{ $unwind: '$patient' },
			{
				$lookup:
        {
        	from: 'providers',
        	localField: 'PRO_UID',
        	foreignField: 'PRO_UID',
        	as: 'provider'
        }
			},
			{ $unwind: '$provider' }
		])
	}

	getPatientsAllEncounter (PT_HS_MRN) {
		return this.model.aggregate([
			{
				$match: { PT_HS_MRN: PT_HS_MRN }
			},
			{
				$lookup: {
					from: 'providers',
					localField: 'PRO_UID',
					foreignField: 'PRO_UID',
					as: 'providers'
				}
			},
			{
				$lookup: {
					from: 'charts',
					localField: 'Encounter_UID',
					foreignField: 'Encounter_UID',
					as: 'charts'
				}
			},
			{ $unwind: '$charts' },
			{ $unwind: '$providers' },
			{
				$project: {
					Encounter_UID: 1,
					PRO_UID: 1,
					Encounter_Chief_Complaint_PRO: 1,
					Encounter_Type: 1,
					Encounter_Location_Type: 1,
					Encounter_Time: 1,
					Encounter_End_Time: 1,
					Encounter_End_Time_Zone: 1,
					Encounter_Date: 1,
					Encounter_Status: 1,
					Encounter_Provider: 1,
					Primary_DX: 1,
					Encounter_Access: 1,
					Encounter_Access_Provider: 1,
					PRO_Profile_Picture: '$providers.PRO_Profile_Picture',
					Encounter_Cancelled_User: 1,
					Encounter_Cancelled_By: 1,
					Chart_is_Signed: '$charts.Chart_is_Signed',
					Addendum_Is_Signed: '$charts.Addendum_Is_Signed',
					Encounter_Number_of_Attachments: '$charts.Encounter_Number_of_Attachments',
					Encounter_Followup_Limit: 1
				}
			}
		])
	}

	// get encounter in last 30 Days //
	async getLastThirtyDaysEncounter (date) {
		var data = {}
		const pastEncounter = await this.model.find({ Created_At: { $gte: date } }).count()
		const totalEncounter = await this.model.find({}).count()
		data.pastEncounter = pastEncounter
		data.totalEncounter = totalEncounter
		return data
	}

	// get provider encounter //
	async getProviderEncounterById (data) {
		var countData = {}
		var count = await this.model.aggregate([
			{
				$match: { PRO_UID: data.PRO_UID }
			},
			{
				$count: 'Encounter_UID'
			}
		])
		var encounter = await this.model.aggregate([
			{
				$match: { PRO_UID: data.PRO_UID }
			},
			{
				$lookup: {
					from: 'patients',
					localField: 'PT_HS_MRN',
					foreignField: 'PT_HS_MRN',
					as: 'provider_encounter'
				}
			},
			{
				$lookup: {
					from: 'charts',
					localField: 'Encounter_UID',
					foreignField: 'Encounter_UID',
					as: 'chart'
				}
			}
		])
		countData.total = count.length > 0 ? count[0].Encounter_UID : 0
		countData.result = encounter
		return countData
	}

	// Get patient encounter //
	async getPatientEncounterByEmail (data) {
		var countData = {}
		const count = await this.model.aggregate([
			{
				$match: { PT_HS_MRN: data.PT_HSID }
			},
			{
				$count: 'Encounter_UID'
			}
		])
		const encounter = await this.model.aggregate([
			{
				$match: { PT_HS_MRN: data.PT_HSID }
			},
			{
				$lookup: {
					from: 'providers',
					localField: 'PRO_UID',
					foreignField: 'PRO_UID',
					as: 'patient_encounter'
				}
			},
			{
				$lookup: {
					from: 'charts',
					localField: 'Encounter_UID',
					foreignField: 'Encounter_UID',
					as: 'chart'
				}
			}
		])
		countData.total = count.length > 0 ? count[0].Encounter_UID : 0
		countData.result = encounter
		return countData
	}

	findScheduledEncounter (id) {
		return this.model.aggregate([
			{
				$match: { PRO_UID: id }
			},
			{
				$group:
        {
        	_id: '_id',
        	encounter:
          {
          	$push: {
          		Encounter_UID: '$Encounter_UID',
          		Encounter_Location_Type: '$Encounter_Location_Type',
          		Encounter_Date: '$Encounter_Date',
          		Encounter_Start_Time: '$Encounter_Start_Time',
          		Encounter_Start_Time_Zone: '$Encounter_Start_Time_Zone',
          		Encounter_End_Time: '$Encounter_End_Time',
          		Encounter_End_Time_Zone: '$Encounter_End_Time_Zone',
          		Encounter_startComplete_Time: { $concat: ['$Encounter_Start_Time', ' ', '$Encounter_Start_Time_Zone'] },
          		Encounter_endComplete_Time: { $concat: ['$Encounter_End_Time', ' ', '$Encounter_End_Time_Zone'] }
          	}
          }
        }
			}
		])
	}

	updateEncounterTravelTime (id, encounterTravel) {
		return this.model.updateOne({ Encounter_UID: id }, { $set: { Encounter_Travel_Time: encounterTravel } })
	}

	// -----------------------------//
	async upcomming_pastVisit (PT_Username, PRO_UID) {
		return this.model.find({ PT_Username: PT_Username, PRO_UID: PRO_UID })
	}

	getEncounterByEncounter_UID (Encounter_UID) {
		return this.model.findOne({ Encounter_UID: Encounter_UID })
	}

	getFollowupEncounterByEncounter_UID (Encounter_UID) {
		return this.model.findOne({ Parent_Encounter: Encounter_UID })
	}

	getfollowUpEncounter (Encounter_UID) {
		return this.model.aggregate([
			{
				$match: {
					Parent_Encounter: Encounter_UID
				}
			},
			{
				$lookup: {
					from: 'patients',
					localField: 'PT_HS_MRN',
					foreignField: 'PT_HS_MRN',
					as: 'patients'
				}
			},
			{
				$lookup: {
					from: 'providers',
					localField: 'PRO_UID',
					foreignField: 'PRO_UID',
					as: 'providers'
				}
			},
			{
				$lookup: {
					from: 'charts',
					localField: 'Encounter_UID',
					foreignField: 'Encounter_UID',
					as: 'charts'
				}
			},
			{
				$lookup: {
					from: 'orders',
					localField: 'Encounter_UID',
					foreignField: 'Encounter_UID',
					as: 'order'
				}
			},
			{
				$unwind: '$providers'
			},
			{
				$unwind: '$patients'
			}
		])
	}

	updateEncounter (Encounter_UID, data) {
		return this.model.findOneAndUpdate({ Encounter_UID: Encounter_UID }, { $set: data, Updated_At: new Date() }, { new: true }, (err, doc) => {
			if (err) {
				console.log('Something wrong when updating data!')
			}
		})
	}

	updatePatientHistoryProvider (Encounter_UID, PRO_UID) {
		return this.model.updateOne({ Encounter_UID: Encounter_UID }, { $set: { 'PT_History.PRO_UID': PRO_UID } })
	}

	deniedChartAccess (Encounter_UID, PRO_UID) {
		return this.model.findOneAndUpdate({ Encounter_UID: Encounter_UID }, { $pull: { Encounter_Access_Provider: { PRO_UID: PRO_UID } } }, { $set: { Updated_At: new Date() } })
	}

	shareChart (Encounter_UID, PRO_UID) {
		return this.model.updateOne(
			{ Encounter_UID: Encounter_UID, 'Encounter_Access_Provider.PRO_UID': PRO_UID },
			{ $set: { 'Encounter_Access_Provider.$.Access_Status': 'Granted', Updated_At: new Date() } }
		)
	}

	shareMultiChart (Encounter_UID, PRO_UID) {
		return this.model.updateMany(
			{ Encounter_UID: { $in: Encounter_UID }, 'Encounter_Access_Provider.PRO_UID': PRO_UID },
			{ $set: { 'Encounter_Access_Provider.$.Access_Status': 'Granted', Updated_At: new Date() } }
		)
	}

	updateEncounterAcesss (Encounter_UID, data) {
		return this.model.updateOne(
			{ Encounter_UID: Encounter_UID },
			{ $push: { Encounter_Access_Provider: data } },
			{ $set: { Updated_At: new Date() } }
		)
	}

	shareAllPastChart (Encounter_UID, data) {
		return this.model.updateMany(
			{ Encounter_UID: { $in: Encounter_UID } },
			{ $push: { Encounter_Access_Provider: data } },
			{ $set: { Updated_At: new Date() } }
		)
	}

	updatePatientEmail (oldEmail, newEmail) {
		let data = {
			PT_Username: newEmail,
			Updated_At: new Date()
		}
		return this.model.updateMany({ PT_Username: oldEmail },
			{ $set: data }, { multi: true })
	}

	getProvidersAllEncounter (PRO_UID) {
		return this.model.aggregate([
			{
				$match: { PRO_UID: PRO_UID }
			},
			{
				$lookup: {
					from: 'patients',
					localField: 'PT_HS_MRN',
					foreignField: 'PT_HS_MRN',
					as: 'patient'
				}
			},
			{ $unwind: '$patient' },
			{
				$project: {
					Encounter_UID: 1,
					PRO_UID: 1,
					Encounter_Chief_Complaint_PRO: 1,
					Encounter_Type: 1,
					Encounter_Location_Type: 1,
					Encounter_Time: 1,
					Encounter_Date: 1,
					Encounter_Start_Time: 1,
					Encounter_Start_Time_Zone: 1,
					Encounter_Status: 1,
					Encounter_Provider: 1,
					Primary_DX: 1,
					Encounter_Reschedule_Status: 1,
					Encounter_Access_Provider: 1,
					Cancellation_Reason: 1,
					PT_HS_MRN: 1,
					PT_First_Name: '$patient.PT_First_Name',
					PT_Last_Name: '$patient.PT_Last_Name',
					PT_Age_Now: '$patient.PT_Age_Now',
					PT_Gender: '$patient.PT_Gender',
					PT_DOB: '$patient.PT_DOB',
					PT_Profile_Picture: '$patient.PT_Profile_Picture'
				}
			}
		])
	}

	async getPatientEncounter (email) {
		return this.model.aggregate([
			{
				$match: { PT_Username: email }
			},
			{
				$lookup: {
					from: 'patients',
					localField: 'PT_HS_MRN',
					foreignField: 'PT_HS_MRN',
					as: 'patient'
				}
			},
			{
				$lookup: {
					from: 'providers',
					localField: 'PRO_UID',
					foreignField: 'PRO_UID',
					as: 'provider'
				}
			},
			{
				$lookup: {
					from: 'charts',
					localField: 'Encounter_UID',
					foreignField: 'Encounter_UID',
					as: 'chart'
				}
			},
			{
				$unwind: '$provider'
			},
			{
				$unwind: '$patient'
			}
		])
	}

	async getPatientTransactionStatus (email) {
		return this.model.aggregate([
			{
				$match: { PT_Username: email, Encounter_Status: { $nin: ['Cancelled', 'Abandoned'] } }
			},
			{
				$lookup: {
					from: 'providers',
					localField: 'PRO_UID',
					foreignField: 'PRO_UID',
					as: 'provider'
				}
			},
			{
				$lookup: {
					from: 'patients',
					localField: 'PT_HS_MRN',
					foreignField: 'PT_HS_MRN',
					as: 'patients'
				}
			},
			{
				$lookup: {
					from: 'charts',
					localField: 'Encounter_UID',
					foreignField: 'Encounter_UID',
					as: 'chart'
				}
			},
			{
				$unwind: '$provider'
			},
			{
				$unwind: '$patients'
			}
		])
	}

	getPatientEncounterByUserID (user_id) {
		return this.model.aggregate([
			{
				$match: {
					PT_HS_MRN: { $in: user_id }
				}
			},
			{
				$lookup: {
					from: 'patients',
					localField: 'PT_HS_MRN',
					foreignField: 'PT_HS_MRN',
					as: 'patients'
				}
			},
			{
				$lookup: {
					from: 'providers',
					localField: 'PRO_UID',
					foreignField: 'PRO_UID',
					as: 'providers'
				}
			},
			{
				$lookup: {
					from: 'charts',
					localField: 'Encounter_UID',
					foreignField: 'Encounter_UID',
					as: 'charts'
				}
			},
			{
				$lookup: {
					from: 'orders',
					localField: 'Encounter_UID',
					foreignField: 'Encounter_UID',
					as: 'order'
				}
			},
			{
				$unwind: '$providers'
			},
			{
				$unwind: '$patients'
			},
			{
				$project: {
					Encounter_UID: 1,
					Encounter_Chief_Complaint_PRO: 1,
					Encounter_Chief_Complaint_PT: 1,
					Encounter_Type: 1,
					Encounter_Location_Type: 1,
					Encounter_Time: 1,
					Encounter_Date: 1,
					Encounter_Start_Time: 1,
					Encounter_End_Time: 1,
					Encounter_End_Time_Zone: 1,
					Encounter_Start_Time_Zone: 1,
					Encounter_Status: 1,
					Encounter_Provider: 1,
					Primary_DX: 1,
					Payment_Card_Id: 1,
					Parent_Encounter: 1,
					Encounter_Reschedule_Status: 1,
					Reschedule_Request_Date: 1,
					Reschedule_Request_Time: 1,
					Encounter_Access_Provider: 1,
					Encounter_Followup_Status: 1,
					Followup_Created_By: 1,
					Encounter_Cancelled_By: 1,
					Cancellation_Reason: 1,
					Reschedule_Requested_By: 1,
					Transaction_Status: 1,
					Encounter_Cost: 1,
					PT_History: 1,
					PT_First_Name: '$patients.PT_First_Name',
					PT_Last_Name: '$patients.PT_Last_Name',
					PT_HS_MRN: '$patients.PT_HS_MRN',
					PT_Username: '$patients.PT_Username',
					PT_Profile_Picture: '$patients.PT_Profile_Picture',
					PT_Home_Address_Line_1: '$patients.PT_Home_Address_Line_1',
					PT_Home_Address_Line_2: '$patients.PT_Home_Address_Line_2',
					PT_Home_Address_Latitude: '$patients.PT_Home_Address_Latitude',
					PT_Home_Address_Longitude: '$patients.PT_Home_Address_Longitude',
					PT_Home_City: '$patients.PT_Home_City',
					PT_Home_State: '$patients.PT_Home_State',
					PT_Home_Zip: '$patients.PT_Home_Zip',
					PT_DOB: '$patients.PT_DOB',
					PT_Share_Records: '$patients.PT_Share_Records',
					PT_Credit_Card_Type: '$patients.PT_Credit_Card_Type',
					PT_Credit_Card_Short_Number: '$patients.PT_Credit_Card_Short_Number',
					PRO_First_Name: '$providers.PRO_First_Name',
					PRO_Last_Name: '$providers.PRO_Last_Name',
					PRO_Profile_Picture: '$providers.PRO_Profile_Picture',
					PRO_Gender: '$providers.PRO_Gender',
					PRO_UID: '$providers.PRO_UID',
					Pro_Office_Hours_Time: '$providers.Pro_Office_Hours_Time',
					PRO_Office_Address_Line_1: '$providers.PRO_Office_Address_Line_1',
					PRO_Office_Address_Line_2: '$providers.PRO_Office_Address_Line_2',
					PRO_Office_Address_Latitude: '$providers.PRO_Office_Address_Latitude',
					PRO_Office_Address_Longitude: '$providers.PRO_Office_Address_Longitude',
					PRO_Office_City: '$providers.PRO_Office_City',
					PRO_Office_State: '$providers.PRO_Office_State',
					PRO_Office_Zip: '$providers.PRO_Office_Zip',
					Discharge_Instructions_Status: '$charts.Discharge_Instructions_Status',
					Discharge_Instructions_Viewed: '$charts.Discharge_Instructions_Viewed',
					Chart_is_Signed: '$charts.Chart_is_Signed',
					Chart_Viewed: '$charts.Chart_Viewed',
					PRO_Addendums: '$charts.PRO_Addendums',
					Addendum_Viewed_By: '$charts.Addendum_Viewed_By',
					order: '$order'
				}
			}
		])
	}

	getSummaryByEncounter_UID (EN_UID) {
		return this.model.aggregate([
			{
				$match: {
					Encounter_UID: EN_UID
				}
			},
			{
				$lookup: {
					from: 'patients',
					localField: 'PT_HS_MRN',
					foreignField: 'PT_HS_MRN',
					as: 'patients'
				}
			},
			{
				$lookup: {
					from: 'providers',
					localField: 'PRO_UID',
					foreignField: 'PRO_UID',
					as: 'providers'
				}
			},
			{
				$lookup: {
					from: 'charts',
					localField: 'Encounter_UID',
					foreignField: 'Encounter_UID',
					as: 'charts'
				}
			},
			{
				$lookup: {
					from: 'orders',
					localField: 'Encounter_UID',
					foreignField: 'Encounter_UID',
					as: 'order'
				}
			},
			{
				$unwind: '$providers'
			},
			{
				$unwind: '$patients'
			}
		])
	}

	getEncounters (PRO_UID) {
		return this.model.aggregate([
			{
				$match: {
					PRO_UID: PRO_UID
				}
			},
			{
				$lookup: {
					from: 'patients',
					localField: 'PT_HS_MRN',
					foreignField: 'PT_HS_MRN',
					as: 'patients'
				}
			},
			{
				$lookup: {
					from: 'providers',
					localField: 'PRO_UID',
					foreignField: 'PRO_UID',
					as: 'providers'
				}
			},
			{
				$lookup: {
					from: 'charts',
					localField: 'Encounter_UID',
					foreignField: 'Encounter_UID',
					as: 'charts'
				}
			},
                        	{
				$lookup: {
					from: 'transactions',
					localField: 'Encounter_UID',
					foreignField: 'Encounter_UID',
					as: 'transaction'
				}
			},
			{
				$unwind: '$providers'
			},
			{
				$unwind: '$patients'
			},
			{
				$unwind: '$charts'
			},
            {
				$unwind: '$transaction'
			},
			{
				$project: {
					Encounter_UID: 1,
					Encounter_Chief_Complaint_PRO: 1,
					Encounter_Type: 1,
					Encounter_Location_Type: 1,
					Encounter_Time: 1,
					Encounter_Date: 1,
					Encounter_Cost: 1,
					Encounter_Transaction_Status: '$transaction.Transaction_Status',
					PT_First_Name: '$patients.PT_First_Name',
					PT_Last_Name: '$patients.PT_Last_Name',
					PT_Age_Now: '$patients.PT_Age_Now',
					PT_Gender: '$patients.PT_Gender',
					Chart_is_Signed: '$charts.Chart_is_Signed',
                                    // transaction:'$transaction.Transaction_Id',
					_id: 0
				}
			}
		])
	}

	getPatientsEncounter (PT_Username) {
		return this.model.find({ PT_Username: PT_Username })
	}

	getPatientsEncounterById (PT_HS_MRN) {
		return this.model.aggregate([
			{
				$match: { PT_HS_MRN: PT_HS_MRN }
			},
			{
				$project: {
					Encounter_UID: 1,
					Encounter_Date: 1,
					Encounter_Time: 1
				}
			}
		])
	}

	updateMedicalDirectiveAttchment (Encounter_UID, data) {
		return this.model.updateOne(
			{ Encounter_UID: Encounter_UID },
			{ $push: { 'PT_History.PT_Medical_Directive_Attachment': data } },
			{ $set: { Updated_At: new Date(), 'PT_History.Updated_At': new Date() } }
		)
	}

	setMedicalDirectiveAttchment (Encounter_UID, data) {
		return this.model.updateOne(
			{ Encounter_UID: Encounter_UID },
			{ $set: { 'PT_History.PT_Medical_Directive_Attachment': data } },
			{ $set: { Updated_At: new Date(), 'PT_History.Updated_At': new Date() } }
		)
	}

	updateMedicationAttachment (Encounter_UID, data) {
		return this.model.updateOne(
			{ Encounter_UID: Encounter_UID },
			{ $push: { 'PT_History.PT_Medication_Attachment': data } },
			{ $set: { Updated_At: new Date(), 'PT_History.Updated_At': new Date() } }
		)
	}

	setMedicationAttachment (Encounter_UID, data) {
		return this.model.updateOne(
			{ Encounter_UID: Encounter_UID },
			{ $set: { 'PT_History.PT_Medication_Attachment': data } },
			{ $set: { Updated_At: new Date(), 'PT_History.Updated_At': new Date() } }
		)
	}

	updateMedication (Encounter_UID, data) {
		return this.model.updateOne(
			{ Encounter_UID: Encounter_UID },
			{ $push: { 'PT_History.PT_Medication': data } },
			{ $set: { Updated_At: new Date(), 'PT_History.Updated_At': new Date() } }
		)
	}

	updateMedicalHistoryAttachment (Encounter_UID, data) {
		return this.model.updateOne(
			{ Encounter_UID: Encounter_UID },
			{ $push: { 'PT_History.PT_Medical_History_Attachment': data } },
			{ $set: { Updated_At: new Date(), 'PT_History.Updated_At': new Date() } }
		)
	}

	setMedicalHistoryAttachment (Encounter_UID, data) {
		return this.model.updateOne(
			{ Encounter_UID: Encounter_UID },
			{ $set: { 'PT_History.PT_Medical_History_Attachment': data } },
			{ $set: { Updated_At: new Date(), 'PT_History.Updated_At': new Date() } }
		)
	}

	findMedicalHistoryAttachment (Encounter_UID, id) {
		return this.model.findOne(
			{
				Encounter_UID: Encounter_UID,
			 'PT_History.PT_Medical_History_Attachment._id': ObjectId(id)
			},
			 { 'PT_History.PT_Medical_History_Attachment': 1 })
	}

	findMedicalDirectiveAttachment (Encounter_UID, id) {
		return this.model.findOne(
			{
				Encounter_UID: Encounter_UID,
			 'PT_History.PT_Medical_Directive_Attachment._id': ObjectId(id)
			},
			 { 'PT_History.PT_Medical_Directive_Attachment': 1 })
	}

	findMedicationAttachment (Encounter_UID, id) {
		return this.model.findOne(
			{
				Encounter_UID: Encounter_UID,
			 'PT_History.PT_Medication_Attachment._id': ObjectId(id)
			},
			 { 'PT_History.PT_Medication_Attachment': 1 })
	}

	updateLabAttachment (Encounter_UID, data) {
		return this.model.updateOne(
			{ Encounter_UID: Encounter_UID },
			{ $push: { 'PT_History.PT_Lab_and_Image_Reports': data } },
			{ $set: { Updated_At: new Date(), 'PT_History.Updated_At': new Date() } }
		)
	}

	getFollowup (PRO_UID) {
		return this.model.aggregate([
			{
				$match: {
					PRO_UID: PRO_UID, Encounter_Type: 'Followup Visit'
				}
			},
			{
				$lookup: {
					from: 'encounters',
					localField: 'Parent_Encounter',
					foreignField: 'Encounter_UID',
					as: 'parent_encounter'
				}
			},
			{ $unwind: '$parent_encounter' },
			{
				$project: {
					Encounter_UID: 1,
					Encounter_Chief_Complaint_PRO: 1,
					Encounter_Type: 1,
					Encounter_Location_Type: 1,
					Encounter_Time: 1,
					Encounter_Date: 1,
					Encounter_Start_Time: 1,
					Encounter_Start_Time_Zone: 1,
					Encounter_Status: 1,
					Encounter_Followup_Status: '$parent_encounter.Encounter_Followup_Status',
					Encounter_Provider: 1,
					Parent_Encounter: 1,
					Encounter_Has_Followup: '$parent_encounter.Encounter_Has_Followup'
				}
			}
		])
	}

	getEncounterFollowup (Encounter_UID) {
		return this.model.findOne({ Parent_Encounter: Encounter_UID, Encounter_Status: { $ne: 'Cancelled' } })
	}

	removeFollowupEncounter (Encounter_UID) {
		return this.model.deleteMany({ Parent_Encounter: Encounter_UID, Encounter_Status: { $ne: 'Cancelled' } })
	}

	getEncounter (PRO_UID) {
		return this.model.aggregate([
			{
				$match: {
					PRO_UID: PRO_UID, Encounter_Status: { $ne: 'Abandoned' }
				}
			},
			{
				$lookup: {
					from: 'patients',
					localField: 'PT_HS_MRN',
					foreignField: 'PT_HS_MRN',
					as: 'patients'
				}
			},
			{
				$unwind: '$patients'
			}
		])
	}

	updatePatientMedicalHistory (Encounter_UID, PT_History) {
		return this.model.updateOne(
			{ Encounter_UID: Encounter_UID },
			{
				$set: {
					PT_History: PT_History
				}
			}
		)
	}

	updateEncounterPatientHistory (Encounter_UID, PT_History) {
		var json = {}
		let updateKey = Object.keys(PT_History)
		for (let i = 0; i < updateKey.length; i++) {
			json[`PT_History.${updateKey[i]}`] = PT_History[updateKey[i]]
		}
		json['PT_History.Updated_At'] = new Date()
		json.Updated_At = new Date()
		return this.model.updateOne(
			{ Encounter_UID: Encounter_UID },
			{
				$set: json
			}
		)
	}

	updateEncounterPatientAttachment (Encounter_UID, attachment, key) {
		var json = { key: attachment[key] }
		json[`PT_History.${key}`] = attachment[key]
		delete json.key
		return this.model.updateOne(
			{ Encounter_UID: Encounter_UID },
			{ $push: json },
			{ $set: { Updated_At: new Date(), 'PT_History.Updated_At': new Date() } }
		)
	}

	getPatientMedication (Encounter_UID, id) {
		return this.model.find({
			Encounter_UID: Encounter_UID,
			'PT_History.PT_Medication._id': id
		})
	}

	updatePatientMedication (Encounter_UID, data, id) {
		return this.model.updateOne({
			Encounter_UID: Encounter_UID,
			'PT_History.PT_Medication._id': id
		},
		{ $set: { 'PT_History.PT_Medication.$.PT_Medication_Name': data, 'PT_History.Updated_At': new Date() } }
		)
	}

	async updateAllPatientComingEncounter (encounter_ids, data) {
		let newData = JSON.parse(JSON.stringify(data))
		delete newData._id

		return this.model.update(
			{ Encounter_UID: { $in: encounter_ids } },
			{ $set: { PT_History: data } }, { multi: true }
		)
	}

	deletePatientMedication (Encounter_UID, id) {
		return this.model.updateOne(
			{
				Encounter_UID: Encounter_UID,
				'PT_History.PT_Medication._id': id
			},
			{ $pull: { 'PT_History.PT_Medication': { _id: id } } },
			{ $set: { Updated_At: new Date() } }
		)
	}

	addendumReminder () {
		return this.model.aggregate([
			{
				$match: {
					Encounter_Status: 'Active'
				}
			},
			{
				$lookup: {
					from: 'charts',
					localField: 'Encounter_UID',
					foreignField: 'Encounter_UID',
					as: 'chart'
				}
			},
			{
				$unwind: '$chart'
			},
			{
				$project:{
   					Encounter_Type: 1,
   					Encounter_Date: 1,
   					Encounter_Time: 1,
   					Encounter_UID: 1,
   					Encounter_Chief_Complaint_PT: 1,
					Encounter_Chief_Complaint_PRO: 1,
					Chart_is_Signed: '$chart.Chart_is_Signed',
					PRO_Addendums: '$chart.PRO_Addendums',
					PRO_UID:1, 
					PT_HS_MRN:1
				}
			}
		])
	}

	beforeEncouterDate () {
		return this.model.find({ Encounter_Status: 'Scheduled' })
	}

	getFollowupRequestedEncounter () {
		return this.model.aggregate([
			{ $match: { Encounter_Followup_Status: 'Requested' } },
			{
				$project: {
					Encounter_UID: 1,
					Encounter_Date: 1,
					Encounter_Time: 1,
					Followup_Requested_Time: 1,
					PRO_UID: 1
				}
			}
		])
	}

	getEncounterRescheduleRequested () {
		return this.model.aggregate([
			{ $match: { Encounter_Reschedule_Status: 'Requested' } },
			{
				$project: {
					Encounter_UID: 1,
					Reschedule_Requested_Time: 1,
					PRO_UID: 1
				}
			}
		])
	}

	getActiveEncounter () {
		return this.model.aggregate([
			{ $match: { Encounter_Status: 'Active' } },
			{
				$lookup:
        {
        	from: 'charts',
        	localField: 'Encounter_UID',
        	foreignField: 'Encounter_UID',
        	as: 'chart'
        }
			},
			{
				$unwind: '$chart'
			},
			{ $match: { 'chart.Discharge_Instructions_Status': 'Draft', 'chart.Chart_is_Signed': false } },
			{
				$project: {
					Encounter_UID: 1,
					Encounter_Date: 1,
					Encounter_Time: 1,
					PRO_UID: 1
				}
			}
		])
	}

	getScheduledEncounter () {
		return this.model.aggregate([
			{ $match: { Encounter_Status: 'Scheduled' } },
			{
				$project: {
					Encounter_UID: 1,
					Encounter_Date: 1,
					Encounter_Time: 1,
					PRO_UID: 1
				}
			}
		])
	}
}

module.exports = EncounterModel
