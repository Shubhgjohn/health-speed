/* eslint-disable camelcase */
class Error_Model {
  Access_Denied () {
    return 'HS003'
  }

  Encounter_UID_required () {
    return 'HS001'
  }

  Encounter_not_found () {
    return 'HS002'
  }

  pdf_error () {
    return 'HS004'
  }

  PRO_Availability_From_Date () {
    return 'HS010'
  }

  PRO_Availability_To_Date () {
    return 'HS011'
  }

  date_required () {
    return 'HS019'
  }

  No_availability_available () {
    return 'HS020'
  }

  year_required () {
    return 'HS023'
  }

  PRO_UID_required () {
    return 'HS025'
  }

  Attachment_Name_required () {
    return 'HS026'
  }

  Addendum_Id_required () {
    return 'HS027'
  }

  No_chart_found () {
    return 'HS028'
  }

  data_required () {
    return 'HS030'
  }

  _id_required () {
    return 'HS032'
  }

  PT_Username_required () {
    return 'HS034'
  }

  patient_not_found () {
    return 'HS035'
  }

  Chart_already_created () {
    return 'HS036'
  }

  Attachment_Id_required () {
    return 'HS042'
  }

  key_required () {
    return 'HS044'
  }

  ICD10_Code_required () {
    return 'HS049'
  }

  ICD10_Code_Order_required () {
    return 'HS050'
  }

  ICD10_Id_required () {
    return 'HS052'
  }

  Encounter_Location_Type_required () {
    return 'HS059'
  }

  Encounter_Type_required () {
    return 'HS060'
  }

  Encounter_Date_required () {
    return 'HS061'
  }

  Encounter_Chief_Complaint_PT_required () {
    return 'HS062'
  }

  Encounter_Chief_Complaint_PRO_required () {
    return 'HS063'
  }

  Encounter_Cost_required () {
    return 'HS064'
  }

  Encounter_Start_Time_required () {
    return 'HS065'
  }

  Encounter_End_Time_required () {
    return 'HS066'
  }

  Encounter_Start_Time_Zone_required () {
    return 'HS067'
  }

  Encounter_End_Time_Zone_required () {
    return 'HS068'
  }

  Encounter_End_Time_less_than_Encounter_Start_Time () {
    return 'HS071'
  }

  You_can_not_set_encounter () {
    return 'HS072'
  }

  you_can_not_schedule () {
    return 'HS073'
  }

  patient_can_not_accept () {
    return 'HS079'
  }

  provider_can_not_accept () {
    return 'HS080'
  }

  PT_HS_MRN_required () {
    return 'HS092'
  }

  body_required () {
    return 'HS095'
  }

  Reschedule_Request_Date_required () {
    return 'HS101'
  }

  Reschedule_Request_Time_required () {
    return 'HS102'
  }

  Encounter_Reschedule_Status_required () {
    return 'HS103'
  }

  provider_not_found () {
    return 'HS109'
  }

  Order_UID_required () {
    return 'HS116'
  }

  No_order_found () {
    return 'HS118'
  }

  PT_Medication_Name_required () {
    return 'HS124'
  }

  Medication_invalid () {
    return 'HS125'
  }

  id_required () {
    return 'HS127'
  }

  enter_your_email () {
    return 'HS142'
  }

  did_not_find_record () {
    return 'HS143'
  }

  Payment_Card_Id_required () {
    return 'HS145'
  }

  enter_your_password () {
    return 'HS153'
  }

  account_banned () {
    return 'HS154'
  }

  entries_do_not_match () {
    return 'HS155'
  }

  patient_valid_email () {
    return 'HS157'
  }

  make_sure_criteria () {
    return 'HS161'
  }

  PT_New_Password_required () {
    return 'HS162'
  }

  username_already_associated () {
    return 'HS173'
  }

  no_stripe_account () {
    return 'HS150'
  }

  cardId_required () {
    return 'HS179'
  }

  enter_provider_email () {
    return 'HS191'
  }

  Provider_not_available () {
    return 'HS204'
  }

  special_Rate_id_required () {
    return 'HS216'
  }

  special_rate_not_in_system () {
    return 'HS217'
  }

  PRO_Speciality_required () {
    return 'HS223'
  }

  Speciality_Id_required () {
    return 'HS225'
  }

  Speciality_invalid () {
    return 'HS226'
  }

  PRO_Affiliations_required () {
    return 'HS229'
  }

  affliation_Id_required () {
    return 'HS231'
  }

  affliation_invalid () {
    return 'HS232'
  }

  PRO_Education_required () {
    return 'HS235'
  }

  Education_Id_required () {
    return 'HS237'
  }

  education_invalid () {
    return 'HS238'
  }

  PRO_Experience_required () {
    return 'HS241'
  }

  Experience_Id_required () {
    return 'HS243'
  }

  experience_invalid () {
    return 'HS244'
  }

  PRO_Actions_Against_required () {
    return 'HS247'
  }

  Action_Id_required () {
    return 'HS249'
  }

  acrtion_invalid () {
    return 'HS250'
  }

  can_not_set_past_time () {
    return 'HS262'
  }
}

module.exports = Error_Model
