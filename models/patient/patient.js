/* eslint-disable dot-notation */
/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
const BaseModel = require('../base')
const mongoose = require('mongoose')
const Schema = mongoose.Schema
var autopopulate = require('mongoose-autopopulate')

const PATIENT_COLLECTION = 'patient'
const PATIENT_SCHEMA = new Schema({
  PT_Username: { type: String, minlength: 5, maxlength: 500, default: null },
  PT_Password: { type: String, minlength: 6, maxlength: 500, default: null },
  PT_First_Name: { type: String, minlength: 1, maxlength: 50, default: null },
  PT_Last_Name: { type: String, minlength: 1, maxlength: 50, default: null },
  PT_Cell_Phone: { type: String, minlength: 10, maxlength: 10, default: null },
  PT_Home_Address_Line_1: { type: String, minlength: 3, maxlength: 140, default: null },
  PT_Home_Address_Line_2: { type: String, default: null },
  PT_Home_Address_Latitude: { type: String, default: null },
  PT_Home_Address_Longitude: { type: String, default: null },
  PT_Home_City: { type: String, minlength: 1, maxlength: 140, default: null },
  PT_Home_State: { type: String, default: null },
  PT_Home_Zip: { type: String, minlength: 5, maxlength: 5, default: null },
  PT_DOB: { type: Date, minlength: 1 / 1 / 1900, maxlength: new Date(), default: null },
  PT_Age_Now: { type: Number, default: 0 },
  PT_Age_at_Date: { type: Date, default: null },
  PT_Gender: { type: String, default: null },
  PT_Terms_Agreement: { type: String, default: null },
  PT_Account_Status: { type: String, default: 'Active' },
  PT_Profile_Picture: { type: String, default: null },
  PT_Profile_Picture_Key: { type: String, default: null },
  PT_Default_Profile_Picture: { type: String, default: null },
  PT_Govt_ID_Type: { type: String, default: null },
  PT_Govt_ID_Front: { type: String, default: null },
  PT_Govt_ID_Front_Key: { type: String, default: null },
  PT_Govt_ID_Back: { type: String, default: null },
  PT_Govt_ID_Back_Key: { type: String, default: null },
  PT_Guardian_Name: { type: String, minlength: 2, maxlength: 100, default: null },
  PT_Height: { type: Number, default: 0 },
  PT_Weight: { type: Number, default: 0 },
  PT_Medical_Directive: { type: String, default: null },
  PT_Organ_Donor: { type: String, default: 'No' },
  PT_Height_Feet_Or_Inches: { type: Number, default: 0 },
  PT_Weight_lbs: { type: Number, default: 0 },
  PT_BMI: { type: Number, default: 0 },
  PT_O2_Sat: { type: Number, default: 0 },
  PT_Temp: { type: Number, default: 0 },
  PT_Temp_Farenheit: { type: Number, default: 0 },
  PT_Heart_Rt: { type: Number, default: 0 },
  PT_Resp: { type: Number, default: 0 },
  PT_Systolic_BP: { type: Number, default: 0 },
  PT_Diastolic_BP: { type: Number, default: 0 },
  PT_Blood_Pressure: { type: Number, default: 0 },
  PT_Blood_Glucose: { type: Number, min: 0, max: 4, default: 0 },
  PT_Emergency_Contact_Name: { type: String, minlength: 1, maxlength: 100, default: null },
  PT_Emergency_Contact_Phone: { type: String, minlength: 10, maxlength: 10, default: null },
  PT_HS_MRN: { type: String, default: null },
  PT_Get_Ready_Info_Updated_Timestamp: { type: Date, default: null },
  PT_Share_Records: { type: String, default: 'No' },
  PT_Is_Minor: { type: String, default: 'No' },
  PT_Last_LogIn: { type: Date, default: null },
  PT_Biometric_Login: String,
  PT_Email_Notification: { type: String, default: 'Enabled' },
  PRO_UID: String,
  stripeCustomerId: String,
  PT_Total_Number_of_Encounters: { type: Number, default: 0 },
  PT_Total_Amount_Paid: { type: Number, default: 0 },
  PT_About_Update_Toggle: { type: String, default: 'Unchecked' },
  PT_HX_Last_Updated_About_Section: { type: String, default: null },
  PT_General_Update_Toggle: { type: String, default: 'Unchecked' },
  PT_HX_Last_Updated_General_Section: { type: String, default: null },
  PT_Medicaitons_Update_Toggle: { type: String, default: 'Unchecked' },
  PT_HX_Last_Updated_Medications_Section: { type: String, default: null },
  PT_Other_Update_Toggle: { type: String, default: 'Unchecked' },
  PT_HX_Last_Updated_Other_Section: { type: String, default: null },
  PT_OBGYN_Update_Toggle: { type: String, default: 'Unchecked' },
  PT_HX_Last_Updated_OB_Section: { type: String, default: null },
  Created_At: { type: Date, default: new Date() },
  Updated_At: { type: Date, default: new Date() },
  Deleted_At: { type: Date, default: null },
  Is_Deleted: { type: Boolean, default: false }
})
PATIENT_SCHEMA.plugin(autopopulate)
var MODEL = mongoose.model(PATIENT_COLLECTION, PATIENT_SCHEMA)

// Note: Do not extend this class, only BaseModel is allow to be extended from.
// because more than 2 levels inheritance could lead to tight-coupling design and make everything more complicated
class PatientModel extends BaseModel {
  constructor () {
    super(MODEL, PATIENT_COLLECTION, PATIENT_SCHEMA)
  }

  // find patient by id //
  findById (id) {
    return this.model.findOne({ PT_HS_MRN: id })
  }

  findMinorIDByUserId (id) {
    return this.model.aggregate([
      {
        $match: {
          PT_Guardian_Name: id
        }
      },
      {
        $project: {
          PT_HS_MRN: 1, _id: 0
        }
      }
    ])
  }

  getPatientByHSId (id) {
    return this.model.aggregate([
      { $match: { PT_HS_MRN: id } }
    ])
  }

  // find patient by email //
  findOneByEmail (email) {
    return this.model.findOne({ PT_Username: email })
  }

  // Get patient last active time //
  updateLastLogin (email) {
    return this.model.updateOne(
      { PT_Username: email },
      { $set: { PT_Last_LogIn: Date.now() } }
    )
  }

  // update patient password by email //
  updatePasswordByEmail (email, data) {
    return this.model.updateOne(
      { PT_Username: email },
      {
        $set: {
          PT_Password: data
        }
      }
    )
  }

  // Get patient list with sorting,pagination and search funcationality //
  async getPatientList (data) {
    const countData = {}
    var orQuery = {}
    var andQuery = {}
    orQuery.$or = []
    andQuery.$and = []
    if (data.search_text) {
      orQuery.$or.push({ PT_Username: { $regex: data.search_text, $options: 'i' } })
      orQuery.$or.push({ PT_Gender: { $regex: data.search_text, $options: 'i' } })
      orQuery.$or.push({ PT_Cell_Phone: { $regex: data.search_text, $options: 'i' } })
      if (data.search_text.indexOf(' ') >= 0) {
        const split = data.search_text.split(' ')
        andQuery.$and.push({ PT_First_Name: { $regex: split[0], $options: 'i' } })
        andQuery.$and.push({ PT_Last_Name: { $regex: split[1], $options: 'i' } })
      } else {
        orQuery.$or.push({ PT_First_Name: { $regex: data.search_text, $options: 'i' } })
        orQuery.$or.push({ PT_Last_Name: { $regex: data.search_text, $options: 'i' } })
      }
      orQuery.$or.push({ PT_HS_MRN: { $regex: data.search_text, $options: 'i' } })
      orQuery.$or.push({ PT_Account_Status: { $regex: data.search_text, $options: 'i' } })
    }
    // andQuery.$and.push({ PT_Is_Minor: 'No' })
    if (andQuery.$and.length != 0) orQuery.$or.push(andQuery)
    var match = {}
    if (orQuery.$or.length != 0) match = orQuery
    const count = await this.model.aggregate([
      {
        $match: match
      },
      {
        $count: 'PT_Username'
      }
    ])
    const patient = await this.model.aggregate([
      {
        $match: match
      },
      {
        $project: {
          _id: 0,
          Signup: '$Created_At',
          HSID: '$PT_HS_MRN',
          PT_First_Name: '$PT_First_Name',
          PT_Last_Name: '$PT_Last_Name',
          PT_DOB: 1,
          PT_Gender: '$PT_Gender',
          Email: '$PT_Username',
          Phone: '$PT_Cell_Phone',
          lastActive: '$PT_Last_LogIn',
          Status: '$PT_Account_Status',
          PT_Total_Number_of_Encounters: '$PT_Total_Number_of_Encounters',
          PT_Total_Amount_Paid: '$PT_Total_Amount_Paid'
        }
      },
      {
        $sort: data.column == 'signup' ? { Signup: data.order == 'desc' ? -1 : 1 } : data.column == 'HSID' ? { HSID: data.order == 'desc' ? -1 : 1 } : data.column == 'name' ? { PT_First_Name: data.order == 'desc' ? -1 : 1 } : data.column == 'demo' ? { PT_DOB: data.order == 'desc' ? -1 : 1 } : data.column == 'email' ? { Email: data.order == 'desc' ? -1 : 1 } : data.column == 'phone' ? { Phone: data.order == 'desc' ? -1 : 1 } : data.column == 'status' ? { Status: data.order == 'desc' ? -1 : 1 } : data.column == 'last_active' ? { lastActive: data.order == 'desc' ? -1 : 1 } : data.column == 'Total_Encounters' ? { PT_Total_Number_of_Encounters: data.order == 'desc' ? -1 : 1 } : data.column == 'Amount_Paid' ? { PT_Total_Amount_Paid: data.order == 'desc' ? -1 : 1 } : { Signup: data.order == 'asc' ? 1 : -1 }
      },
      { $skip: data.skip != 0 ? data.skip : 0 }, { $limit: data.limit != 0 ? data.limit : 10 }
    ])
    countData.total = count.length > 0 ? count[0].PT_Username : 0
    countData.result = patient
    return countData
  }

  // Export patient CSV //
  exportPatientCSV (data) {
    var orQuery = {}
    var andQuery = {}
    orQuery.$or = []
    andQuery.$and = []
    if (data.search_text) {
      orQuery.$or.push({ PT_Username: { $regex: data.search_text, $options: 'i' } })
      orQuery.$or.push({ PT_Gender: { $regex: data.search_text, $options: 'i' } })
      orQuery.$or.push({ PT_Cell_Phone: { $regex: data.search_text, $options: 'i' } })
      if (data.search_text.indexOf(' ') >= 0) {
        const split = data.search_text.split(' ')
        andQuery.$and.push({ PT_First_Name: { $regex: split[0], $options: 'i' } })
        andQuery.$and.push({ PT_Last_Name: { $regex: split[1], $options: 'i' } })
      } else {
        orQuery.$or.push({ PT_First_Name: { $regex: data.search_text, $options: 'i' } })
        orQuery.$or.push({ PT_Last_Name: { $regex: data.search_text, $options: 'i' } })
      }
      orQuery.$or.push({ PT_HS_MRN: { $regex: data.search_text, $options: 'i' } })
      orQuery.$or.push({ PT_Account_Status: { $regex: data.search_text, $options: 'i' } })
    }
    if (andQuery.$and.length != 0) orQuery.$or.push(andQuery)
    var match = {}
    if (orQuery.$or.length != 0) match = orQuery
    return this.model.aggregate([
      {
        $match: match
      },
      {
        $project: {
          _id: 0,
          Signup: '$Created_At',
          HSID: '$PT_HS_MRN',
          PT_First_Name: '$PT_First_Name',
          PT_Last_Name: '$PT_Last_Name',
          PT_Age_Now: '$PT_Age_Now',
          PT_Gender: '$PT_Gender',
          Email: '$PT_Username',
          Phone: '$PT_Cell_Phone',
          lastActive: '$PT_Last_LogIn',
          Status: '$PT_Account_Status',
          PT_Total_Number_of_Encounters: '$PT_Total_Number_of_Encounters',
          PT_Total_Amount_Paid: '$PT_Total_Amount_Paid'
        }
      }
    ])
  }

  // Get patient by id //
  getPatientById (id) {
    return this.model.aggregate([
      {
        $match: { PT_HS_MRN: id }
      },
      {
        $project: {
          _id: 0,
          PT_Profile_Picture: '$PT_Profile_Picture',
          PT_Govt_ID_Front: '$PT_Govt_ID_Front',
          PT_Govt_ID_Back: '$PT_Govt_ID_Back',
          PT_Govt_ID_Type: '$PT_Govt_ID_Type',
          PT_First_Name: '$PT_First_Name',
          PT_Last_Name: '$PT_Last_Name',
          PT_DOB: '$PT_DOB',
          PT_Cell_Phone: '$PT_Cell_Phone',
          PT_Username: '$PT_Username',
          PT_Home_Address_Line_1: '$PT_Home_Address_Line_1',
          PT_Home_Address_Line_2: '$PT_Home_Address_Line_2',
          PT_Home_City: '$PT_Home_City',
          PT_Home_State: '$PT_Home_State',
          PT_Home_Zip: '$PT_Home_Zip',
          PT_Gender: '$PT_Gender',
          PT_Organ_Donor: '$PT_Organ_Donor',
          PT_Height: '$PT_Height',
          PT_Weight: '$PT_Weight',
          PT_Account_Status: '$PT_Account_Status',
          PT_Is_Minor: 1
        }
      }

    ])
  }

  // update patient by id //
  updatePatientById (PT_HS_MRN, data) {
    data.Updated_At = Date.now()
    return this.model.updateOne({ PT_HS_MRN: PT_HS_MRN }, { $set: data }, { new: true })
  }

  updateMinorById (PT_HS_MRN, data) {
    data.Updated_At = Date.now()
    return this.model.updateMany({ PT_Guardian_Name: PT_HS_MRN }, { $set: data }, { new: true })
  }

  updatePatientByUsername (email, data) {
    return this.model.updateOne({ PT_Username: email }, data, { new: true }).exec()
  }

  // Patient for listing in audit logs //
  patientForAuditLog (search) {
    if (search) {
      var orFilter = {}
      var patient = {}
      orFilter['$or'] = []
      patient['$and'] = []

      if (search.indexOf(' ') >= 0) {
        const split = search.split(' ')
        if (split.length > 2) return []
        patient['$and'].push({ PT_First_Name: { $regex: split[0], $options: 'i' } })
        patient['$and'].push({ PT_Last_Name: { $regex: split[1], $options: 'i' } })
      } else {
        // orFilter['$or'].push({ PT_HS_MRN: { $regex: search, $options: 'i' } })
        orFilter['$or'].push({ PT_First_Name: { $regex: search, $options: 'i' } })
        orFilter['$or'].push({ PT_Last_Name: { $regex: search, $options: 'i' } })
      }
      var match = {}
      if (patient['$and'].length != 0) match = patient
      if (orFilter['$or'].length != 0) match = orFilter
      return this.model.aggregate([{ $match: match }, {
        $project: {
          _id: 0, HSID: '$PT_HS_MRN', PT_First_Name: '$PT_First_Name', PT_Last_Name: '$PT_Last_Name', PT_DOB: '$PT_DOB'
        }
      }])
    }
    return this.model.aggregate([
      {
        $project: {
          _id: 0, HSID: '$PT_HS_MRN', PT_First_Name: '$PT_First_Name', PT_Last_Name: '$PT_Last_Name', PT_DOB: '$PT_DOB'
        }
      }
    ])
  }

  // get active patient in last 30 Days //
  async getLastThirtyDaysActiveUser (date) {
    var data = {}
    const lastActive = await this.model.find({ PT_Account_Status: 'Active', PT_Last_LogIn: { $gte: date } }).count()
    const totalActive = await this.model.find({ PT_Account_Status: 'Active' }).count()
    data.lastActive = lastActive
    data.totalActive = totalActive
    return data
  }

  // function for get patient info //
  async getPatientInfo (PT_HS_MRN) {
    return this.model.aggregate([
      {
        $match: {
          PT_HS_MRN: PT_HS_MRN
        }
      },
      {
        $lookup: {
          from: 'patient_histories',
          localField: 'PT_HS_MRN',
          foreignField: 'PT_HS_MRN',
          as: 'data'
        }
      },
      { $unwind: '$data' }
    ])
  }

  updatePatientEmail (oldEmail, newEmail) {
    let data = {
      PT_Username: newEmail,
      Updated_At: new Date()
    }
    return this.model.updateMany({ PT_Username: oldEmail },
      { $set: data }, { multi: true })
  }

  getPatientAllMinors (id) {
    return this.model.aggregate([
      {
        $match: {
          PT_Guardian_Name: id
        }
      },
      {
        $project: {
          _id: 0, PT_HS_MRN: 1, PT_First_Name: 1, PT_Last_Name: 1, PT_Profile_Picture: 1, PT_Is_Minor: 1, PT_HX_Last_Updated_About_Section: 1, Updated_At: 1, Created_At: 1
        }
      }
    ])
  }
}

module.exports = PatientModel
