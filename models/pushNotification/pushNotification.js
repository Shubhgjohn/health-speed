const BaseModel = require('../base')
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const NOTIFICATION_COLLECTION = 'notification'

const NOTIFICATION_SCHEMA = new Schema({
  Device_Token: String,
  Device_Id: String,
  Role: String,
  Email_ID: String,
  Created_At: { type: Date, default: Date.now },
  Updated_At: { type: Date, default: Date.now },
  Deleted_At: { type: Date, default: null }
})

var MODEL = mongoose.model(NOTIFICATION_COLLECTION, NOTIFICATION_SCHEMA)

class NOTIFICATION_MODEL extends BaseModel {
  constructor () {
    super(MODEL, NOTIFICATION_COLLECTION, NOTIFICATION_SCHEMA)
  }

  getToken (role, email) {
    return this.model.find({ Role: role, Email_ID: email })
  }

  removeToken (data) {
    return this.model.remove({ Device_Id: data.Device_Id, Role: data.Role })
  }
}

module.exports = NOTIFICATION_MODEL
