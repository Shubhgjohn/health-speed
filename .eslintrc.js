module.exports = {
  extends: 'standard',
  env: {
    node: true,
    commonjs: true,
  },
  parserOptions: {
    ecmaVersion: 2017,
  },
  rules: {
    indent: ['error', 'tab'],
    indent: [2, 'tab'],
    'no-tabs': 0,
    'linebreak-style': ['error', 'unix'],
    quotes: ['error', 'single'],
    semi: ['error', 'never'],
  },
};
